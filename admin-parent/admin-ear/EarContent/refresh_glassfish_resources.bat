IF "%GLASSFISH_HOME%" == "" GOTO NOHOME
:YESHOME
call %GLASSFISH_HOME%\bin\asadmin delete-jms-resource jms/PptUpdateQueue
call %GLASSFISH_HOME%\bin\asadmin delete-jms-resource jms/PalmsConnectionFactory
call %GLASSFISH_HOME%\bin\asadmin delete-jdbc-resource jdbc/pdinh-ms
call %GLASSFISH_HOME%\bin\asadmin delete-jdbc-connection-pool MsSql_pdinh_rootPool
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource custom/tlt_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource custom/abyd_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource application/config_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource application/admin-timer/config_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource application/admin-rest/config_properties

@cd /D %~dp0
call %GLASSFISH_HOME%\bin\asadmin add-resources sun-resources.xml

GOTO END
:NOHOME
@ECHO The GLASSFISH_HOME environment variable was NOT detected
GOTO END
:END
pause
