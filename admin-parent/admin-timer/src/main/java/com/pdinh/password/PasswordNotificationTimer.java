package com.pdinh.password;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.PasswordPolicyUserStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailTextDao;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;

@Startup
@Singleton
public class PasswordNotificationTimer {
	private static final Logger log = LoggerFactory.getLogger(PasswordNotificationTimer.class);
	@Resource(name = "custom/tlt_properties")
	private Properties tltProps;
	
	@Resource
	TimerService timerService;
	
	@EJB
	private PasswordPolicyUserStatusDao statusDao;
	
	@EJB
	private MailTemplatesServiceLocalImpl mailTemplateService;
	
	@EJB
	ScheduledEmailTextDao scheduledEmailTextDao;
	
	private List<PasswordPolicyUserStatus> statusList;
	
	@Resource(name = "application/admin-timer/config_properties")
	private Properties configProperties;
	
	
	private long passwordNotificationIntervalMillis;
    /**
     * Default constructor. 
     */
    public PasswordNotificationTimer() {
        // TODO Auto-generated constructor stub
    }
    
    @PostConstruct
	private void init(){
    	passwordNotificationIntervalMillis = Long.valueOf(configProperties.getProperty("passwordNotificationTimerIntervalMillis", "0"));
		if (passwordNotificationIntervalMillis > 0){
			//setTimer(passwordNotificationIntervalMillis);
		}
	}
    
    public void setTimer(long intervalDuration) {
        log.info("Setting a programmatic timeout for " +
                intervalDuration + " milliseconds from now.");
        TimerConfig config = new TimerConfig();
        config.setPersistent(false);
        
        HashSet<Timer> timerList = (HashSet<Timer>) timerService.getTimers();
        for (Timer timer : timerList){
        	timer.cancel();
        }
        Timer timer = timerService.createIntervalTimer(intervalDuration, intervalDuration, 
                config);
    }
    
    @Timeout
	public void sendPasswordNotifications(Timer timer){
		log.debug("Password Notification Timer Expired..  checking for notifications");
		statusList = statusDao.findAllExpiringPasswords();
		for (PasswordPolicyUserStatus status : statusList){
			log.debug("Found Status for user: {}, last change time: {}", status.getUser().getUsername(), status.getLastChangeTime());
			User user = status.getUser();
			if (user.getActive() == 1){
				Company company = user.getCompany();
				ScheduledEmailDef def = status.getPolicy().getScheduledEmailDef();
				List<ScheduledEmailText> texts = def.getScheduledEmailTexts();
				ScheduledEmailText theText = null;
				String langPref = "";
				int textid = 0;
				//if the user's lang pref is not set, use the company default lang
				if (!(user.getLangPref() == null)){
					langPref = user.getLangPref();
				}else if (!(company.getLangPref() == null)){
					langPref = company.getLangPref();
				}else {
					//system lang default.. change to jndi property
					langPref = "en";
				}
				for (ScheduledEmailText text : texts){
					if (text.getLanguage().getCode().equalsIgnoreCase(langPref)){
						theText = text;
						break;
					}
				}
				if (theText == null){
					log.debug("User lang not found.  Sending english");
					for (ScheduledEmailText text : texts){
						if (text.getLanguage().getCode().equalsIgnoreCase("en")){
							theText = text;
							break;
						}
					}
				}
				if (theText == null){
					log.error("failed to find any appropriate language for email");
				}else {
					ParticipantObj p = new ParticipantObj();
					p.setEmail(user.getEmail());
					p.setFirstName(user.getFirstname());
					p.setLastName(user.getLastname());
					p.setUserId(user.getUsersId());
					p.setUserName(user.getUsername());
					p.setOptional1(user.getOptional1());
					p.setOptional2(user.getOptional2());
					p.setOptional3(user.getOptional3());
					p.setPassword(user.getPassword());
					p.setSupervisorName(user.getSupervisorName());
					p.setSupervisorEmail(user.getSupervisorEmail());
					/*
					 * Modify to use getTemplate method here.  build list of EmailEventObjs and send through sendBatchEmail method
					 */
					mailTemplateService.sendTemplate(theText.getId(), p, null, company, tltProps.getProperty("tltParticipantExperienceUrl", "https://www.ninthhouse.net/assessment"));
				}
				status.setNotificationSent(true);
				status.setAboutToExpire(true);
				statusDao.update(status);
			}else{
				log.debug("User is inactive. Do not send email");
				
				status.setAboutToExpire(true);
				statusDao.update(status);
			}
			
			
		}
	}



	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	public Properties getTltProps() {
		return tltProps;
	}

	public void setTltProps(Properties tltProps) {
		this.tltProps = tltProps;
	}

}