package com.pdinh.password;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.PasswordPolicyUserStatusDao;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;

@Startup
@Singleton
public class PasswordExpirationTimer {
	private static final Logger log = LoggerFactory.getLogger(PasswordExpirationTimer.class);
	
	@Resource
	TimerService timerService;
	
	@EJB
	private PasswordPolicyUserStatusDao statusDao;
	
	@Resource(name = "application/admin-timer/config_properties")
	private Properties configProperties;
	
	private long passwordExpirationTimerIntervalMillis;
	
	private long passwordExpirationTimerInitialDelay;
	
	@PostConstruct
	private void init(){
		passwordExpirationTimerIntervalMillis = Long.valueOf(configProperties.getProperty("passwordExpirationTimerIntervalMillis", "0"));
		if (passwordExpirationTimerIntervalMillis > 0){
			//setTimer(passwordExpirationTimerIntervalMillis);
		}
	}
	
	public void setTimer(long intervalDuration) {
        log.info("Setting a programmatic timeout for " +
                intervalDuration + " milliseconds from now.");
        TimerConfig config = new TimerConfig();
        config.setPersistent(false);
        
        HashSet<Timer> timerList = (HashSet<Timer>) timerService.getTimers();
        for (Timer timer : timerList){
        	timer.cancel();
        }
        //
        passwordExpirationTimerInitialDelay = Long.valueOf(configProperties.getProperty("passwordExpirationTimerInitialDelay", "1800000"));
        log.debug("Password Expiration Timer Initial Delay: {}", passwordExpirationTimerInitialDelay);
        Timer timer = timerService.createIntervalTimer(passwordExpirationTimerInitialDelay, intervalDuration, 
                config);
    }
	
	@Timeout
	public void doPasswordExpiration(Timer timer){
		log.debug("Password Expiration Timer Expired..  checking for expirations");
		List<PasswordPolicyUserStatus> statusList = statusDao.findAllExpiredPasswords();
		for (PasswordPolicyUserStatus status : statusList){
			log.debug("Found Expired Password for user: {}, last change time: {}", status.getUser().getUsername(), status.getLastChangeTime());
			status.setExpired(true);
			status.setChangeRequired(true);
			statusDao.update(status);
		}
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}


	public long getPasswordExpirationTimerIntervalMillis() {
		return passwordExpirationTimerIntervalMillis;
	}

	public void setPasswordExpirationTimerIntervalMillis(
			long passwordExpirationTimerIntervalMillis) {
		this.passwordExpirationTimerIntervalMillis = passwordExpirationTimerIntervalMillis;
	}

	public long getPasswordExpirationTimerInitialDelay() {
		return passwordExpirationTimerInitialDelay;
	}

	public void setPasswordExpirationTimerInitialDelay(
			long passwordExpirationTimerInitialDelay) {
		this.passwordExpirationTimerInitialDelay = passwordExpirationTimerInitialDelay;
	}
}
