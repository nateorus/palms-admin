package com.pdinh.mail;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleToProcessDao;
import com.pdinh.persistence.ms.entity.EmailRuleToProcess;

@Startup
@Singleton
public class EmailRuleProcessor {
	
	private static final Logger log = LoggerFactory.getLogger(EmailRuleProcessor.class);
	@Resource
	TimerService timerService;
	
	@Resource(name = "application/admin-timer/config_properties")
	private Properties configProperties;
	
	@EJB
	private EmailRuleToProcessDao emailRuleToProcessDao;

	
	@EJB
	private MailRuleProcessorServiceLocal mailRuleProcessorService;
	
	private long emailRuleTimerIntervalMillis;
	private long emailRuleTimerInitialDelay;
	
	@PostConstruct
	private void init(){
		//Default value for Timer delay is one day or 86400000 milliseconds
		emailRuleTimerIntervalMillis = Long.valueOf(configProperties.getProperty("emailRuleTimerIntervalMillis", "86400000"));
		emailRuleTimerInitialDelay = Long.valueOf(configProperties.getProperty("emailRuleTimerInitialDelay", "0"));
		log.debug("Email Rule Timer Initial Delay: {}, Interval: {}", emailRuleTimerInitialDelay, emailRuleTimerIntervalMillis);
		if (emailRuleTimerIntervalMillis > 0){
			log.debug("Setting Timer...");
			setTimer(emailRuleTimerIntervalMillis);
		}
	}
	
	public void setTimer(long intervalDuration) {
        log.info("Setting a programmatic timeout for every " +
                intervalDuration + " milliseconds ");
        TimerConfig config = new TimerConfig();
        config.setPersistent(true);
        
        //Cleanup possible leftover timers from a bad deploy
        HashSet<Timer> timerList = (HashSet<Timer>) timerService.getTimers();
        for (Timer timer : timerList){
        	timer.cancel();
        }
        //Calculate the number of milliseconds until next hour change
        if (this.emailRuleTimerInitialDelay == 0){
	        Calendar c = Calendar.getInstance();
	        Date now = new Date();
	        c.setTime(now);
	        //c.add(Calendar.DAY_OF_MONTH, 1);
	        c.add(Calendar.HOUR_OF_DAY, 1);
	        
	        c.set(Calendar.MINUTE, 0);
	        c.set(Calendar.SECOND, 0);
	        c.set(Calendar.MILLISECOND, 0);
	        this.emailRuleTimerInitialDelay = (c.getTimeInMillis()-now.getTime());
	        //add 1 minute so rule will fire one minute after the hour to 
	        //make sure rules set to the hour will be in the past.
	        this.emailRuleTimerInitialDelay = this.emailRuleTimerInitialDelay + 60000;
	        log.info("Setting Email Rule Timer to fire at top of the hour... aka {} milliseconds from now", emailRuleTimerInitialDelay);
        }
       

        Timer timer = timerService.createIntervalTimer(emailRuleTimerInitialDelay, intervalDuration, 
                config);
        log.debug("Created Timer.  Time remaining: {}", timer.getTimeRemaining());
    }
	
	
	@Timeout
	public void doEmailRuleProcessing(Timer timer){
		log.debug("Email Rule Processor Expiration Timer Expired..  checking for emails to send");
		
		
		List<EmailRuleToProcess> rules = emailRuleToProcessDao.findAll();
				
		for (EmailRuleToProcess rule: rules){
			mailRuleProcessorService.processRule(rule, false);
			
			
		}
		
	}

	public long getEmailRuleTimerInitialDelay() {
		return emailRuleTimerInitialDelay;
	}

	public void setEmailRuleTimerInitialDelay(long emailRuleTimerInitialDelay) {
		this.emailRuleTimerInitialDelay = emailRuleTimerInitialDelay;
	}
	
	

}
