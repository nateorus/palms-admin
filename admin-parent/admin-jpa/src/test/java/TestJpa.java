import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.pdinh.persistence.ms.entity.Company;


public class TestJpa {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		/*
		List list = em.createQuery("Select r from ReportType r").getResultList();
		
		for(Object o : list) {
			ReportType rt = (ReportType)o;
			if(rt.getTargetLevelGroup() != null) {
				System.out.println(rt.getTargetLevelGroup().getName());
			}

		}*/
		 
	List list2 = em.createQuery("Select c from Company c").getResultList();
		System.out.println(list2.size());
		for(Object o : list2) {
			Company c = (Company)o;
			System.out.println(c.getCoName());

		}
		 
		
		/*
		List list3 = em.createQuery("Select c from CompanyProjectType c").getResultList();
		
		for(Object o : list3) {
			CompanyProjectType c3 = (CompanyProjectType)o;
			System.out.println(c3.getCompany().getCoName());

		}*/
	}

}
