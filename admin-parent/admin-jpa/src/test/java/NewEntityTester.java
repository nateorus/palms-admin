/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/*
 * Testbed to test content of new entities.  Doesn't do much except call them;
 * it's up to the user to do the validation
 */
public class NewEntityTester
{
    public static void main(String[] args)
    {
        // Get the entity manager
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();

/*
		// Test TargetLevelType
		TargetLevelType tlt = em.find(TargetLevelType.class, 1);
		System.out.println("TLvlT:  ID=" + tlt.getTargetLevelTypeId() + ", code=" + tlt.getCode() + ", Name=" + tlt.getName());
*/

/*
		// Test TargetLevelGroup
		TargetLevelGroup tlg = em.find(TargetLevelGroup.class, 1);
		System.out.println("TLvlG:  ID=" + tlg.getTargetLevelGroupId() + ", Name=" + tlg.getName());
		for (TargetLevelGroupType typ : tlg.getTargetLevelGroupTypes())
		{
			System.out.println("  type:  id=" + typ.getTargetLevelType().getTargetLevelTypeId() + ", seq=" + typ.getSequenceOrder() + ", name=" + typ.getTargetLevelType().getName());
		}
*/
		
/*
		// Test TargetLevelGroupType
		TargetLevelGroupType tlgt = em.find(TargetLevelGroupType.class, 2);
		System.out.println("TLvlGT:  ID=" + tlgt.getTargetLevelGroupTypeId() + "seq=" + tlgt.getSequenceOrder() +
				": Group=" + tlgt.getTargetLevelGroup().getTargetLevelGroupId() + "/" +  tlgt.getTargetLevelGroup().getName() + 
				", Type=" + tlgt.getTargetLevelType().getTargetLevelTypeId() + "/" + tlgt.getTargetLevelType().getName());
*/

/*
		// Test CompanyProjectType - by company_project_type_id
		CompanyProjectType cpt = em.find(CompanyProjectType.class, 2);
		System.out.println("cpt:  ID=" + cpt.getCompanyProjectTypeId() + " - company=" + cpt.getCompany().getCoName() + ", projType=" + cpt.getProjectType().getName());
*/

/*
		// Test Company code update
		Company c = em.find(Company.class, 751);
		System.out.println("c:  name=" + c.getCoName());
		for (CompanyProjectType typ : c.getCompanyProjectTypes())
		{
			System.out.println("  type:  id=" + typ.getCompanyProjectTypeId() + ", typeName=" + typ.getProjectType().getName());
		}
*/

/*
		// Test ProjectType code update
		ProjectType pt = em.find(ProjectType.class, 1);
		//ProjectType pt = em.find(ProjectType.class, 3);
		//ProjectType pt = em.find(ProjectType.class, 5);
		System.out.println("pt:  name=" + pt.getName() + ".  tlVis=" + pt.getTargetLevelVisible() + ", urlVis=" + pt.getUrlGeneratorVisible());
*/
		
		// clean up
		emf.getCache().evictAll();
		em.close();
		emf.close();

    }
}
