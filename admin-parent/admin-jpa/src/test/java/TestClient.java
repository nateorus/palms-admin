import javax.persistence.EntityManager;

import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.persistence.ms.entity.UserCoursePrefs;

public class TestClient {
    
    /**
     * @param args
     */
    public static void main(String[] args) {

        // connect to db
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		String output;

		Company company;
		Course course;
		User user;
		Language language;
		Project project;
		ProjectUser projectUser;
		Position position;
		UserCoursePrefs ucp;
		
		long startTime, endTime;

		Iterator<Company> ci = null;
		Iterator<ProjectUser> pui = null;
		Iterator<Project> cpi = null;
		Iterator<User> ui = null;
		Iterator<Position> posi = null;
		Iterator<Language> li = null;
		Iterator<UserCoursePrefs> ucpi = null;

		// ============================================================================
/*
		System.out.println( "*********************" );
		System.out.println( "Company" );

		List<Company> list = 
		    em.createQuery( "SELECT c from Company c",Company.class).getResultList();
		ci = list.iterator();
		while (ci.hasNext()) {
			company = ci.next();
			output = "   id: " + company.getCompanyId() + " name: " + company.getCoName();
			System.out.println( output );
		}
*/
/*
		company = em.find(Company.class, 751);
		output = "id: " + company.getCompanyId() + " city: " + company.getCoCity() + " name:" + company.getCoName();
		System.out.println( output );
		Query listq = em.createNamedQuery("findDistinctProjectTypes");
		listq.setParameter("id", company.getCompanyId());
		List<String> typelist = (List<String>) listq.getResultList();
    	Iterator<String> tli = typelist.iterator();
		while (tli.hasNext()) {
			System.out.println( " type: " + tli.next());
		}
*/		
		
/*		
		company = em.find(Company.class, 1);
		output = "id: " + company.getCompanyId() + " city: " + company.getCoCity() + " name:" + company.getCoName();
		System.out.println( output );
		company = em.find(Company.class, 4);
		output = "id: " + company.getCompanyId() + " city: " + company.getCoCity() + " name:" + company.getCoName();
		System.out.println( output );
		company = em.find(Company.class, 5);
		output = "id: " + company.getCompanyId() + " city: " + company.getCoCity() + " name:" + company.getCoName();
		System.out.println( output );
		
		System.out.println( "** Company Projects for Company 751" );

		TypedQuery<Company> query = em.createNamedQuery("CompanyBatchProjects",Company.class);
    	
		query.setParameter("id", 751);
		
		startTime = System.currentTimeMillis();
		company = query.getSingleResult();
		output = "id: " + company.getCompanyId() + " city: " + company.getCoCity() + " name:" + company.getCoName();
		System.out.println( output );

		// projects for company
		cpi = company.getProjects().iterator();
		while (cpi.hasNext()) {
			project = cpi.next();
			output = "   id: " + project.getProjectId() + " name: " + project.getName() + " code:" + project.getProjectCode();
			//System.out.println( output );
		}
		System.out.println( "count: " + company.getProjects().size() );

		// users for company
		ui = company.getUsers().iterator();
		while (ui.hasNext()) {
			user = ui.next();
			output = "   id: " + user.getUsersId() + " name: " + user.getUsername() + " company: " + user.getCompany().getCoName();
			//System.out.println( output );
		}
		System.out.println( "count: " + company.getUsers().size() );

		endTime = System.currentTimeMillis();
		
		System.out.println( "TIME1: " + (endTime - startTime));

		// flush
		emf.getCache().evictAll();

*/
/*
		// ============================================================================
		System.out.println( "*********************" );
		System.out.println( "Project" );
		startTime = System.currentTimeMillis();
		
    	//TypedQuery<Project> projectquery = em.createNamedQuery("ProjectBatchUsers",Project.class);
		//projectquery.setParameter("id", 31);
		//project = projectquery.getSingleResult();
		
		project = em.find(Project.class, 31);

		output = "    project: " + project.getName();
		System.out.println( output );

		// metadata for project
		pmi = project.getProjectMetadata().iterator();
		System.out.println( "metadata count: " + project.getProjectMetadata().size() );
		while (pmi.hasNext()) {
			pm = pmi.next();
			output = "    name: " + pm.getMetaName()+ " value: " + pm.getMetaValue();
			System.out.println( output );
		}
		
		// users for project
		pui = project.getProjectUsers().iterator();
		while (pui.hasNext()) {
			projectUser = pui.next();
			output = "   id: " + projectUser.getUser().getUsersId() + " name: " + projectUser.getUser().getUsername() + " company: " + projectUser.getUser().getCompany().getCoName();
			//System.out.println( output );
		}

		
		System.out.println( "** Courses for Project #31" );

		// courses for project
		Iterator<Course> pci = project.getCourses().iterator();
		while (pci.hasNext()) {
			course = pci.next();
			output = "   id: " + course.getCourse() + " title: " + course.getTitle();
			//System.out.println( output );

		}
		
		endTime = System.currentTimeMillis();
		
		System.out.println( "TIME2: " + (endTime - startTime));
		// flush
		emf.getCache().evictAll();
*/
/*
		// ============================================================================
		System.out.println( "*********************" );
		System.out.println( "Project 32" );
		startTime = System.currentTimeMillis();
		
    	TypedQuery<Project> projectquery = em.createNamedQuery("getProjectBatchUsersPositions",Project.class);
		projectquery.setParameter("id", 32);
		project = projectquery.getSingleResult();
		
		//project = em.find(Project.class, 32);

		output = "    project: " + project.getName();
		System.out.println( output );

		// metadata for project
		pmi = project.getProjectMetadata().iterator();
		System.out.println( "metadata count: " + project.getProjectMetadata().size() );
		while (pmi.hasNext()) {
			pm = pmi.next();
			output = "    name: " + pm.getMetaName()+ " value: " + pm.getMetaValue();
			System.out.println( output );
		}
		
		System.out.println( "** Courses for Project" );

		// courses for project
		Iterator<Course> pci = project.getCourses().iterator();
		while (pci.hasNext()) {
			course = pci.next();
			output = "  course id: " + course.getCourse() + " title: " + course.getTitle();
			System.out.println( output );

		}
		
		System.out.println( "** Users for Project" );
		pui = project.getProjectUsers().iterator();
		while (pui.hasNext()) {
			projectUser = pui.next();
			user = projectUser.getUser();
			output = "   user id: " + user.getUsersId() + " name: " + user.getUsername() + " company: " + user.getCompany().getCoName();

			System.out.println( output );
			
			// position for user
			posi = user.getPositions().iterator();
			while (posi.hasNext()) {
			    position = posi.next();
			    output = "      course code: " + position.getCourse().getAbbv() + " score: " + position.getScore() + " status: " + position.getCompletionStatus() + " percent: " + position.getPercentTaken();
			    System.out.println( output );
			}

		}

		
		endTime = System.currentTimeMillis();
		
		System.out.println( "TIME: " + (endTime - startTime));
*/
/*
		
		// flush
		emf.getCache().evictAll();


 		// ============================================================================
		System.out.println( "*********************" );
		System.out.println( "Course" );

		course = em.find(Course.class, 1);
		output = "id: " + course.getCourse() + " title: " + course.getTitle();
		System.out.println( output );
		course = em.find(Course.class, 2);
		output = "id: " + course.getCourse() + " title: " + course.getTitle();
		System.out.println( output );
		course = em.find(Course.class, 3);
		output = "id: " + course.getCourse() + " title: " + course.getTitle();
		System.out.println( output );

		

		System.out.println( "*********************" );
		System.out.println( "User" );

		user = em.find(User.class, 1);
		output = "id: " + user.getUsersId() + " name: " + user.getUsername() + " company: " + user.getCompany().getCoName();
		System.out.println( output );
		user = em.find(User.class, 3);
		output = "id: " + user.getUsersId() + " name: " + user.getUsername() + " company: " + user.getCompany().getCoName();
		System.out.println( output );
		user = em.find(User.class, 5);
		output = "id: " + user.getUsersId() + " name: " + user.getUsername() + " company: " + user.getCompany().getCoName();
		System.out.println( output );


		System.out.println( "*********************" );
		System.out.println( "Language" );

		language = em.find(Language.class, 1);
		output = "code: " + language.getCode() + " name: " + language.getName();
		System.out.println( output );

		// specific language by code

		TypedQuery<Language> lq = em.createNamedQuery("LanguageFindByCode",Language.class);
    	
		lq.setParameter("code", "ja");
		
		language = lq.getSingleResult();
		output = "Language id:" + language.getId()+ " code:" + language.getCode() + " name:" + language.getName();
		System.out.println( output );

		// languages by course
		System.out.println( "languages by course 'sl'");
		TypedQuery<Language> lcquery = em.createNamedQuery("LanguagesByCourseAbbr",Language.class);
		lcquery.setParameter("abbv", "sl");
    	List<Language> lclist = lcquery.getResultList();
    	li = lclist.iterator();
		while (li.hasNext()) {
			language = li.next();
			System.out.println( " code: " + language.getCode() + " name: " + language.getName() );
		}
		System.out.println( "languages by course 'ss'");
		lcquery.setParameter("abbv", "ss");
    	lclist = lcquery.getResultList();
    	li = lclist.iterator();
		while (li.hasNext()) {
			language = li.next();
			System.out.println( " code: " + language.getCode() + " name: " + language.getName() );
		}
		System.out.println( "languages by course 'in'");
		lcquery.setParameter("abbv", "in");
    	lclist = lcquery.getResultList();
    	li = lclist.iterator();
		while (li.hasNext()) {
			language = li.next();
			System.out.println( " code: " + language.getCode() + " name: " + language.getName() );
		}
		
		// see if language is associated with any course

    	TypedQuery<Long> langquery = em.createNamedQuery("LanguageCourseReferenceCountByCode",Long.class);

    	langquery.setParameter("code", "en");
		long num = langquery.getSingleResult().longValue();
		System.out.println( "count: " + num ); // should be "1"
		
		langquery.setParameter("code", "pl");
		num = langquery.getSingleResult().longValue();
		System.out.println( "count: " + num ); // should be "0"

		// unused languages by course
		System.out.println( "unused languages by course 'sl'");
		TypedQuery<Language> lquery = em.createNamedQuery("LanguagesFindUnusedByCourseAbbv",Language.class);
		lquery.setParameter("abbv", "sl");
    	List<Language> llist = lquery.getResultList();
    	System.out.println( " size:" + llist.size());
    	li = llist.iterator();
		while (li.hasNext()) {
			language = li.next();
			System.out.println( " code: " + language.getCode() );
		}
*/
/*
		System.out.println( "users in company 751 project 2");
		TypedQuery<User> lquery = em.createNamedQuery("findUsersInProjectByProjectId",User.class);
		//lquery.setParameter("companyId", 751);
		lquery.setParameter("projectId", 2);
    	List<User> llist = lquery.getResultList();
    	System.out.println( " size:" + llist.size());
    	ui = llist.iterator();
		while (ui.hasNext()) {
			user = ui.next();
			System.out.println( "userId: " + user.getUsersId() + " email: " + user.getEmail());
		}

		System.out.println( "users in company 751 not in project 2");
		lquery = em.createNamedQuery("findUsersNotInProjectByProjectId",User.class);
		lquery.setParameter("companyId", 751);
		lquery.setParameter("projectId", 2);
    	llist = lquery.getResultList();
    	System.out.println( " size:" + llist.size());
    	ui = llist.iterator();
		while (ui.hasNext()) {
			user = ui.next();
			System.out.println( "userId: " + user.getUsersId() + " email: " + user.getEmail() + " company: " + user.getCompany().getCoName());
		}
		
*/  		
/*
		// ============================================================================
		System.out.println( "*********************" );
		System.out.println( "User Prefs" );
		
    	TypedQuery<Language> prefsquery = em.createNamedQuery("getLanguagePrefByUserAndCourse",Language.class);
		prefsquery.setParameter("courseId", 46);
		prefsquery.setParameter("userId", 572628);
		language = prefsquery.getSingleResult();
    	System.out.println( "  code:" + language.getCode());

    	
		// languages prefs by user
		System.out.println( "languages prefs for user");
		prefsquery = em.createNamedQuery("getLanguagePrefsByUserX",Language.class);
		prefsquery.setParameter("userId", 572628);
    	List<Language> lclist = prefsquery.getResultList();
    	li = lclist.iterator();
		while (li.hasNext()) {
			language = li.next();
			System.out.println( " code: " + language.getCode() + " name: " + language.getName() );
		}
		
		// course prefs by user
		System.out.println( "course prefs for user");
		TypedQuery<UserCoursePrefs> ucpquery = em.createNamedQuery("getLanguagePrefsByUser",UserCoursePrefs.class);
		ucpquery.setParameter("userId", 572628);
    	List<UserCoursePrefs> ucplist = ucpquery.getResultList();
    	ucpi = ucplist.iterator();
		while (ucpi.hasNext()) {
			ucp = ucpi.next();
			System.out.println( " course: " + ucp.getCourse().getAbbv() + " code: " + ucp.getLanguage().getCode() + " name: " + ucp.getLanguage().getName() );
		}
*/
		System.out.println("start ");
        TypedQuery<ProjectUser> query = 
            em.createNamedQuery("getCompanyUserProjects",ProjectUser.class);
        query.setParameter("id", 1);
        List<ProjectUser> tmp = query.getResultList();
        System.out.println(" size = " + tmp.size());

		
		// flush
		emf.getCache().evictAll();
		
		//disconnect
		em.close();
		emf.close();
        
    }
    
}

