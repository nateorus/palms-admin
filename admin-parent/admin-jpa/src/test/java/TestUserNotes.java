import javax.persistence.EntityManager;

import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.UserNote;

public class TestUserNotes {
    
    /**
     * @param args
     */
    public static void main(String[] args) {

        // connect to db
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		String output;

		TypedQuery<UserNote> pnquery = em.createNamedQuery("getNotesByUser",UserNote.class);
		pnquery.setParameter("userId", 45);
    	List<UserNote> unlist = (List<UserNote>) pnquery.getResultList();
    	UserNote un;
    	Iterator<UserNote> iterator = unlist.iterator();
		while (iterator.hasNext()) {
			un = iterator.next();
			output =  " name: '" + un.getNote() + "'";
			System.out.println(output);
		}


		
		// flush
		emf.getCache().evictAll();
		
		//disconnect
		em.close();
		emf.close();
        
    }
    
}

