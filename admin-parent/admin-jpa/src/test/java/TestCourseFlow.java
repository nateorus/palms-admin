import javax.persistence.EntityManager;

import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseFlowType;
import com.pdinh.persistence.ms.entity.CourseGroupTemplate;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.User;

public class TestCourseFlow {
    
    /**
     * @param args
     */
    public static void main(String[] args) {

        // connect to db
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		String output;

		Company company;
		Course course;
		CourseFlowType cft;
		CourseGroupTemplate cgt;
		User user;
		Language language;
		Project project;
		ProjectCourse projectCourse;
		ProjectUser projectUser;
		Position position;
		
		long startTime, endTime;

		Iterator<ProjectUser> pui = null;
		Iterator<Project> cpi = null;
		Iterator<User> ui = null;
		Iterator<Position> posi = null;
		Iterator<Language> li = null;
    	Iterator<CourseFlowType> cfti = null;
    	Iterator<CourseGroupTemplate> cgti = null;
    	Iterator<ProjectCourse> pci = null;

		System.out.println( "*** CourseFlowType ");
		TypedQuery<CourseFlowType> cftq = em.createNamedQuery("findAllCourseFlowType",CourseFlowType.class);
		List<CourseFlowType> cftlist = (List<CourseFlowType>) cftq.getResultList();
    	cfti = cftlist.iterator();
		while (cfti.hasNext()) {
			cft = cfti.next();
			output =  " name: '" + cft.getName() + "' desc: '" + cft.getDescription() + "'";
			System.out.println(output);
		}
/*
		System.out.println( "*** CourseGroupTemplate ");
		TypedQuery<CourseGroupTemplate> cgtq = em.createNamedQuery("findAllCourseGroupTemplatByProjectTypeId",CourseGroupTemplate.class);
		cgtq.setParameter("projectTypeId", 2);
		List<CourseGroupTemplate> cgtlist = (List<CourseGroupTemplate>) cgtq.getResultList();
    	cgti = cgtlist.iterator();
		while (cgti.hasNext()) {
			cgt = cgti.next();
			output =  "cft seq: " + cgt.getCourseFlowType().getSequence() + " seq: " + cgt.getSequence() + " group: '" + cgt.getCourseFlowType().getName() + "' course: '" + cgt.getCourse().getTitle() + "'";
			System.out.println(output);
		}
*/
		System.out.println( "*** ProjectCourse ");
		TypedQuery<ProjectCourse> pcq = em.createNamedQuery("getProjectCourseByProjectId",ProjectCourse.class);
		pcq.setParameter("projectId", 73);
		List<ProjectCourse> pclist = (List<ProjectCourse>) pcq.getResultList();
    	pci = pclist.iterator();
		while (pci.hasNext()) {
			projectCourse = pci.next();
			output =  "cft seq: " + projectCourse.getCourseFlowType().getSequence() 
			+ " seq: " + projectCourse.getSequence() 
			+ " group: '" + projectCourse.getCourseFlowType().getName() 
			+ "' course: '" + projectCourse.getCourse().getAbbv()
			+ "' id: " + projectCourse.getCourse().getCourse()
			+ "' sel: " + projectCourse.getSelected();
			System.out.println(output);
		}
	
		

		// flush
		emf.getCache().evictAll();
		
		//disconnect
		em.close();
		emf.close();
        
    }
    
}

