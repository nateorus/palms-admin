import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.pdinh.persistence.ms.entity.ContentToken;


public class TestCreateContentToken {
	public static void main(String[] args) {
    // connect to db
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		ContentToken contentToken = new ContentToken();
		
		contentToken.setCompany_id(1501);
		contentToken.setUsers_id(371936);
		contentToken.setContent_id("vedge");
		contentToken.setToken(UUID.randomUUID().toString());
		contentToken.setTimestamp(new Timestamp(System.currentTimeMillis()));
		
		em.persist(contentToken);
		System.out.println(contentToken.getToken().toUpperCase());

		em.getTransaction().commit();
		//disconnect
		em.close();
		emf.close();
        
    }
	

}
