import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;


public class TestGetCompanies {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Iterator<Company> ci = null;
		Iterator<ScheduledEmailDef> sedi = null;
		Company company;
		ScheduledEmailDef sed;
		String output;
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		TypedQuery<ScheduledEmailDef> query = em.createNamedQuery("findAllTemplatesByCompanyProductType",ScheduledEmailDef.class);
	    query.setParameter("companyId", 1);
	    List<ScheduledEmailDef> list = query.getResultList();
	    System.out.println( "global, count = " + list.size() );
		sedi = list.iterator();
		while (sedi.hasNext()) {
			sed = sedi.next();
			output = "   id: " + sed.getId() + " title: " + sed.getTitle() + " isTemplate: " + sed.getIsTemplate();
			System.out.println( output );
		}

		
		TypedQuery<ScheduledEmailDef> query2 = em.createNamedQuery("findAllTemplatesByCompany",ScheduledEmailDef.class);
	    query2.setParameter("companyId", 1);
	    List<ScheduledEmailDef> list2 = query2.getResultList();
	    System.out.println( "company, count = " + list2.size() );
		sedi = list2.iterator();
		while (sedi.hasNext()) {
			sed = sedi.next();
			output = "   id: " + sed.getId() + " title: " + sed.getTitle() + " isTemplate: " + sed.getIsTemplate();
			System.out.println( output );
		}


/*
		TypedQuery<Company> q = em.createQuery("SELECT c from Company c",Company.class);
		q.setHint("eclipselink.batch.type", "EXISTS");
		q.setHint("eclipselink.batch", "c.projects");
		q.setHint("eclipselink.batch", "c.users");

		List<Company> list = q.getResultList();
		ci = list.iterator();
		while (ci.hasNext()) {
			company = ci.next();
			output = "   id: " + company.getCompanyId() + " name: " + company.getCoName() + " project count: " + company.getProjectCount() + " participant count: " + company.getUsersCount();
			//output = "   id: " + company.getCompanyId() + " name: " + company.getCoName() + " projectCount: " + company.getProjects().size();
			//output = "   id: " + company.getCompanyId() + " name: " + company.getCoName() + " participantCount: " + company.getUsers().size();
			//output = "   id: " + company.getCompanyId() + " name: " + company.getCoName() + " projectCount: " + company.getProjects().size() + " participantCount: " + company.getUsers().size();
			System.out.println( output );
		}
*/

		// flush
		emf.getCache().evictAll();
		
		//disconnect
		em.close();
		emf.close();

	}

}
