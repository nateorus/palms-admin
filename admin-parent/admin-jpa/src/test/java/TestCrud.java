import javax.persistence.EntityManager;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.UUID;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.User;

public class TestCrud {
    
    /**
     * @param args
     */
    public static void main(String[] args) {

        // connect to db
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PDINHServer-ms-jpa");
		EntityManager em = emf.createEntityManager();
		
		String output;

		Company company;
		Course course;
		User user;
		Language language;
		Project project;
		ProjectUser projectUser;
		Position position;
		
		long startTime, endTime;

		Iterator<ProjectUser> pui = null;
		Iterator<Project> cpi = null;
		Iterator<User> ui = null;
		Iterator<Position> posi = null;
		Iterator<Language> li = null;


		// ============================================================================
/*
System.out.println( "** User Add" );

UUID guid = UUID.randomUUID();
String rowguid = guid.toString();
guid = UUID.randomUUID();
String rowguid51 = guid.toString();
Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());

company = em.find(Company.class, 1);

em.getTransaction().begin();

user = new User();
user.setCompany(company);
user.setFirstname("Chris");
user.setLastname("Harrington");
user.setEmail("charring@pdinh.com");
user.setType(0);
user.setUsername("charring");
user.setGender((short) 0);
user.setEdlevel((short) 0);
user.setFuncarea(0);
user.setWorkenv(0);
user.setJobclass(0);
user.setTiPosition((short) 0);
user.setTimeorg(0);
user.setAgegrp(0);
user.setStartdate(sqlDate);
user.setCosubid(-1);
user.setActive((short) 1);
user.setWorkwatch((short) 0);
user.setAccessMode((short) 1);
user.setEmpstatus(0);
user.setCcTourLaunched((short) 0);
user.setAdminHomePage(0);
user.setLaunchDemo(0);
user.setRowguid(rowguid);
user.setRowguid51(rowguid51);
			
		em.persist(user);
		
		em.getTransaction().commit();

*/
/*
		System.out.println( "** User get" );

		TypedQuery<User> userquery = em.createNamedQuery("getUserByUsername",User.class);
    	
		userquery.setParameter("username", "charring");
		
		user = userquery.getSingleResult();
		output = "id: " + user.getUsersId() + " name: " + user.getUsername() + " company:" + user.getCompany().getCoName();
		System.out.println( output );

		System.out.println( "** User remove" );
		em.getTransaction().begin();
		em.remove(user);
		em.getTransaction().commit();
*/
/*
		// ============================================================================
		UUID guid = UUID.randomUUID();
		String rowguid = guid.toString();

        System.out.println( "** Company Add" );

        em.getTransaction().begin();

        company = new Company();
        company.setCoName("cchco");
        company.setCoCity("Pittsburgh");
        company.setPermitallprog(0);
        company.setLpoolflag(0);
        company.setConvtype(0);
        company.setAdmininstall((short) 0);
        company.setIadviceFlag((short) 0);
        company.setUseSsl((short) 0);
        company.setShowReporting((short) 0);
        company.setShowMemberDetails((short) 0);
        company.setShowMultirater((short) 0);
        company.setRowguid(rowguid);

        em.persist(company);
        
        em.getTransaction().commit();
*/

/*
		// ============================================================================
        System.out.println( "** Language Add" );

		UUID guid = UUID.randomUUID();
		String rowguid = guid.toString();


        em.getTransaction().begin();

        language = new Language();
        language.setListOrder(20);
        language.setCode("cchlang");
        language.setName("cchname");
        language.setTransName("cchtrans");
        language.setTransNameUnicode("utfname");

        em.persist(language);
        
        em.getTransaction().commit();
*/
/*		
		System.out.println( "** Language get" );

		TypedQuery<Language> langquery = em.createNamedQuery("getLanguageByCode",Language.class);
    	
		langquery.setParameter("code", "cchlang");
		
		language = langquery.getSingleResult();
		output = "id: " + language.getId() + " name: " + language.getName();
		System.out.println( output );

		System.out.println( "** Language remove" );
		em.getTransaction().begin();
		em.remove(language);
		em.getTransaction().commit();
*/

		// ============================================================================
        System.out.println( "** Project Add" );

        UUID guid = UUID.randomUUID();
        String rowguid = guid.toString();
        guid = UUID.randomUUID();
        //Date sqlDate = java.sql.Date.valueOf("2011-07-20");
        Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());
        company = em.find(Company.class, 1);

        em.getTransaction().begin();
        project = new Project();
        project.setName("cchproj");
        project.setRowguid(rowguid);
        project.setCompany(company);
        project.setDateCreated(sqlDate);
        project.setActive(1);
        em.persist(project);
/*
        pm = new ProjectMetadata();
        ProjectMetatdataPK = new ProjectMetatdataPK();
        pm.setMetaName("cchmetaname1");
        pm.setMetaValue("cchmetavalue1");
        pm.setProject(project);
        em.persist(pm);

        pm = new ProjectMetadata();
        pm.setMetaName("cchmetaname2");
        pm.setMetaValue("cchmetavalue2");
        pm.setProject(project);
        em.persist(pm);
*/
        em.getTransaction().commit();

/*
		System.out.println( "** Project get" );

		TypedQuery<Project> prquery = em.createNamedQuery("getProjectByName",Project.class);
    	
		prquery.setParameter("name", "cchproj");
		try {
		    project = prquery.getSingleResult();

		    output = " Got Project, id: " + project.getProjectId() + " company: " + project.getCompany().getCoName();
		    System.out.println( output );

		    System.out.println( "** Project remove" );
		    em.getTransaction().begin();
		    em.remove(project);
		    em.getTransaction().commit();
		} catch (NoResultException nre) {
		    System.out.println( "Project not found" );
		}
*/
		
		// flush
		emf.getCache().evictAll();
		
		//disconnect
		em.close();
		emf.close();
        
    }
    
}

