package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the company_voucher_view database table.
 * 
 */
@Entity
@Table(name = "company_voucher_view")
@NamedQueries({
// @NamedQuery(name = "findVoucherForUser", query =
// "SELECT v FROM CompanyVoucher v WHERE v.company = :company"),
@NamedQuery(name = "findVoucherForCompany", query = "SELECT v FROM CompanyVoucher v WHERE v.company = :company") })
public class CompanyVoucher implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "company_voucher_id")
	private int companyVoucherId;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@Column(name = "voucher_id")
	private String voucherId;

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public int getCompanyVoucherId() {
		return companyVoucherId;
	}

	public void setCompanyVoucherId(int companyVoucherId) {
		this.companyVoucherId = companyVoucherId;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}