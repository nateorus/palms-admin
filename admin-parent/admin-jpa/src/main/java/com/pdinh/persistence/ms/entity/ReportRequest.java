package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;

@Entity
@Table(name = "report_request")
@NamedQueries({

	@NamedQuery(name = "getReportRequestBatchChildren", query = "Select rr from ReportRequest rr where rr.reportRequestId = :reportRequestId", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest")}),
			
	@NamedQuery(name = "getReportRequestByKey", query = "Select rr from ReportRequest rr where rr.reportRequestKey = :reportRequestKey", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest")}),
			
	@NamedQuery(name = "getReportRequestsByProjectId", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.project.projectId = :projectId AND "
			+ "rr.reportRequestStatus.code != 'DELETED' "
			+ "ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportRequestsByProjectId", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.project.projectId = :projectId AND "
			+ "rr.reportRequestStatus.code != 'DELETED'"),
			
	@NamedQuery(name = "getReportRequestsByCompanyId", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rr.reportRequestStatus.code != 'DELETED' AND "
			+ "rp.user.company.companyId = :companyId ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportRequestsByCompanyId", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rr.reportRequestStatus.code != 'DELETED' AND "
			+ "rp.user.company.companyId = :companyId"),
			
	@NamedQuery(name = "getReportRequestsByCompanyIdAndStatus", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.company.companyId = :companyId AND "
			+ "rr.reportRequestStatus.code IN :statuses ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportRequestsByCompanyIdAndStatus", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.company.companyId = :companyId AND "
			+ "rr.reportRequestStatus.code IN :statuses"),
			
	@NamedQuery(name = "getReportRequestsByProjectIdAndStatus", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.project.projectId = :projectId AND "
			+ "rr.reportRequestStatus.code IN :statuses ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportRequestsByProjectIdAndStatus", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.project.projectId = :projectId AND "
			+ "rr.reportRequestStatus.code IN :statuses"),
			
	@NamedQuery(name = "getIndividualRequestsByUserId", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.usersId = :userId AND "
			+ "rr.reportRequestStatus.code != 'DELETED' "
			+ "ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}), 
			
	@NamedQuery(name = "getCountIndividualRequestsByUserId", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.usersId = :userId AND "
			+ "rr.reportRequestStatus.code != 'DELETED'"), 
			
	@NamedQuery(name = "getIndividualRequestsByUserIdAndStatus", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.usersId = :userId AND "
			+ "rr.reportType.groupReport = false AND "
			+ "rr.reportRequestStatus.code IN :statuses ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountIndividualRequestsByUserIdAndStatus", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.usersId = :userId AND "
			+ "rr.reportType.groupReport = false AND "
			+ "rr.reportRequestStatus.code IN :statuses"),
			
	@NamedQuery(name = "getAllRequestsByUserIdAndStatus", query = "SELECT DISTINCT rr FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.usersId = :userId AND "
			+ "rr.reportRequestStatus.code IN :statuses ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountAllRequestsByUserIdAndStatus", query = "SELECT count(DISTINCT rr) FROM ReportRequest rr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rr.reportParticipants AND "
			+ "rp.user.usersId = :userId AND "
			+ "rr.reportRequestStatus.code IN :statuses"),
			
	@NamedQuery(name = "getRequestsByStatus", query = "SELECT rr FROM ReportRequest rr WHERE "
			+ "rr.reportRequestStatus.code IN :statuses ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getRequestsSubmittedBy", query = "SELECT rr FROM ReportRequest rr WHERE "
			+ "rr.reportGenerateRequest.subjectId = :subjectId AND "
			+ "rr.reportRequestStatus.code IN :statuses ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getByTypeSubmittedBy", query = "SELECT rr FROM ReportRequest rr WHERE "
			+ "rr.reportGenerateRequest.subjectId = :subjectId AND "
			+ "rr.reportType.code = :reportTypeCode AND "
			+ "rr.reportRequestStatus.code != 'DELETED' ORDER BY rr.statusUpdateTs DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")}),
			
	@NamedQuery(name = "getReportsByParent", query = "SELECT rr FROM ReportRequest rr WHERE "
			+ "rr.reportGenerateRequest.reportGenerateRequestId = :reportGenerateRequestId AND "
			+ "rr.reportRequestStatus.code != 'DELETED'", hints = { 
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.targetLevelType"),
			@QueryHint(name = "eclipselink.batch", value = "rr.language"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rr.reportRequestStatus")
	})	
})
public class ReportRequest implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	public static final int CREATE_TYPE_UPLOAD = 1;
	public static final int CREATE_TYPE_REQUEST = 2;
	public static final int CREATE_TYPE_AUTO = 3;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "report_request_id")
	private int reportRequestId;
	
	@ManyToOne
	@JoinColumn(name = "report_generate_request_id")
	private ReportGenerateRequest reportGenerateRequest;
	
	@OneToMany
	@JoinTable(name = "report_request_participant", joinColumns = { @JoinColumn(name = "report_request_id") }, inverseJoinColumns = { @JoinColumn(name = "report_participant_id") })
	private List<ReportParticipant> reportParticipants;
	
	@ManyToOne
	@JoinColumn(name = "report_type_id")
	private ReportType reportType;
	
	@ManyToOne
	@JoinColumn(name = "target_level_type_id")
	private TargetLevelType targetLevelType;
	
	@ManyToOne
	@JoinColumn(name="lang_id")
	private Language language;
	
	@Column(name = "report_id_name_option")
	private String reportIdNameOption;
	
	@Column(name = "report_title")
	private String reportTitle;
	
	@Column(name = "report_request_key")
	private String reportRequestKey;
	
	@Column(name = "content_type")
	private String contentType;
	
	@ManyToOne
	@JoinColumn(name = "report_request_status_id", referencedColumnName = "report_request_status_id")
	private ReportRequestStatus reportRequestStatus;
	
	@Column(name = "error_message")
	private String errorMessage;
	
	@Column(name = "error_code")
	private String errorCode;
	
	@Column(name = "status_update_ts")
	private Timestamp statusUpdateTs;
	
	@Column(name = "filesize")
	private long filesize;
	
	@Column(name = "process_start_time")
	private Timestamp processStartTime;
	
	@Column(name = "create_type")
	private int createType;

	public int getReportRequestId() {
		return reportRequestId;
	}

	public void setReportRequestId(int reportRequestId) {
		this.reportRequestId = reportRequestId;
	}

	public ReportGenerateRequest getReportGenerateRequest() {
		return reportGenerateRequest;
	}

	public void setReportGenerateRequest(ReportGenerateRequest reportGenerateRequest) {
		this.reportGenerateRequest = reportGenerateRequest;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

	public TargetLevelType getTargetLevelType() {
		return targetLevelType;
	}

	public void setTargetLevelType(TargetLevelType targetLevelType) {
		this.targetLevelType = targetLevelType;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getReportIdNameOption() {
		return reportIdNameOption;
	}

	public void setReportIdNameOption(String reportIdNameOption) {
		this.reportIdNameOption = reportIdNameOption;
	}

	public ReportRequestStatus getReportRequestStatus() {
		return reportRequestStatus;
	}

	public void setReportRequestStatus(ReportRequestStatus reportRequestStatus) {
		this.reportRequestStatus = reportRequestStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Timestamp getStatusUpdateTs() {
		return statusUpdateTs;
	}

	public void setStatusUpdateTs(Timestamp statusUpdateTs) {
		this.statusUpdateTs = statusUpdateTs;
	}

	public List<ReportParticipant> getReportParticipants() {
		return reportParticipants;
	}

	public void setReportParticipants(List<ReportParticipant> reportParticipants) {
		this.reportParticipants = reportParticipants;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public String getReportRequestKey() {
		return reportRequestKey;
	}

	public void setReportRequestKey(String reportRequestKey) {
		this.reportRequestKey = reportRequestKey;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public long getFilesize() {
		return filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public Timestamp getProcessStartTime() {
		return processStartTime;
	}

	public void setProcessStartTime(Timestamp processStartTime) {
		this.processStartTime = processStartTime;
	}

	public int getCreateType() {
		return createType;
	}

	public void setCreateType(int createType) {
		this.createType = createType;
	}
	
	
	
}
