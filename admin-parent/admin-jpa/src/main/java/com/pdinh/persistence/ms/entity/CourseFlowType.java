package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the course_flow_type database table.
 * 
 */
@Entity
@Table(name="course_flow_type")
@NamedQueries({
    @NamedQuery(
        name="findAllCourseFlowType",
        query="SELECT cft FROM CourseFlowType cft ORDER BY cft.sequence")
})
public class CourseFlowType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final int PREWORK = 1;
	public static final int ASSESSMENT_DAY = 2;
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_flow_type_id")
	private int courseFlowTypeId;

	@Column(name="sequence_order")
	private int sequence;

	private String description;

	private String name;
/*
	//bi-directional many-to-one association to CourseGroupTemplate
	@OneToMany(mappedBy="courseFlowType")
	private Set<CourseGroupTemplate> courseGroupTemplates;

	//bi-directional many-to-one association to ProjectCourse
	@OneToMany(mappedBy="courseFlowType")
	private Set<ProjectCourse> projectCourses;
*/
    public CourseFlowType() {
    }

	public int getCourseFlowTypeId() {
		return this.courseFlowTypeId;
	}

	public void setCourseFlowTypeId(int courseFlowTypeId) {
		this.courseFlowTypeId = courseFlowTypeId;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
/*
	public Set<CourseGroupTemplate> getCourseGroupTemplates() {
		return this.courseGroupTemplates;
	}

	public void setCourseGroupTemplates(Set<CourseGroupTemplate> courseGroupTemplates) {
		this.courseGroupTemplates = courseGroupTemplates;
	}
	
	public Set<ProjectCourse> getProjectCourses() {
		return this.projectCourses;
	}

	public void setProjectCourses(Set<ProjectCourse> projectCourses) {
		this.projectCourses = projectCourses;
	}
*/
}