package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.pdinh.persistence.ms.entity.Course;

/**
 * Entity implementation class for Entity: CourseScorecard
 * 
 * NOTE: This used to call out courses with scorecards: It is used in the
 * SynchronizerBean in Scorecard
 * 
 */
@Entity
@Table(name = "course_scorecard")
@NamedQueries({ @NamedQuery(name = "findCourseScorecardByCourse", query = "SELECT cs FROM CourseScorecard cs WHERE cs.course.course = :course") })
public class CourseScorecard implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_scorecard_id")
	private int courseScorecardId;

	// bi-directional many-to-one association to Course
	@OneToOne
	@JoinColumn(name = "course")
	private Course course;

	public CourseScorecard() {
	}

	public int getCsId() {
		return this.courseScorecardId;
	}

	public void setCsId(int value) {
		this.courseScorecardId = value;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
}