package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The persistent class for list of possible user doscument types
 * 
 */
@Entity
@Table(name="user_document_type")
@XmlRootElement(name="userDocumentType")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDocumentType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_document_type_id")
	@XmlAttribute
	private int userDocumentTypeId;
	
	@XmlElement
	private String name;

	public int getUserDocumentTypeId() {
		return userDocumentTypeId;
	}

	public void setUserDocumentTypeId(int userDocumentTypeId) {
		this.userDocumentTypeId = userDocumentTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
