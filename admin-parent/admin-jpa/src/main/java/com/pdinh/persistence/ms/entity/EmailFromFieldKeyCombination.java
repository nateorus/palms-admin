package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the report_type database table.
 * 
 */
@Entity
@Table(name="emailFromFieldKeyCombination")
@NamedQueries({
	@NamedQuery(name = "findKeyCombinationByKeyCode", query = "select kc from EmailFromFieldKeyCombination kc where kc.keyCode = :keyCode")
})
public class EmailFromFieldKeyCombination  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public static final String GSC = "GSC";
	
	@Id
	@Column(name = "id")
	private int keyCombinationId;

	@Column(name = "key_code")
	private String keyCode;

	@Column(name = "sender_name")
	private String senderName;
	
	@Column(name = "sender_email")
	private String senderEmail;
	
	public int getKeyCombinationId(){
		return this.keyCombinationId;
	}
	public void setKeyCombinationId(int keyCombinationId){
		this.keyCombinationId = keyCombinationId;
	}
	
	public String getKeyCode(){
		return this.keyCode;
	}
	public void setKeyCode(String keyCode){
		this.keyCode = keyCode;
	}
	
	public String getSenderName(){
		return this.senderName;
	}
	public void setSenderName(String senderName){
		this.senderName = senderName;
	}
	
	public String getSenderEmail(){
		return this.senderEmail;
	}
	public void setSenderEmail(String senderEmail){
		this.senderEmail = senderEmail;
	}
}
