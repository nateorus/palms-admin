package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PasswordPolicyUserStatus
 *
 */
@Entity
@Table(name="password_policy_user_status_view")
@NamedQueries({
	@NamedQuery(name="findAllExpiringPasswords", query="SELECT us FROM PasswordPolicyUserStatus us WHERE us.policy.termInDays > 0" +
			" and us.policy.notificationDaysPriorToExpiration > 0 and us.expired = 0 and us.notificationSent = 0 and us.notificationDate <= CURRENT_DATE"),
	@NamedQuery(name="findAllExpiredPasswords", query="SELECT us FROM PasswordPolicyUserStatus us WHERE us.policy.termInDays > 0 and us.expired = false and us.expirationDate <= CURRENT_DATE")	,
	@NamedQuery(name="findAllInCompanyWithPolicy", query="SELECT us FROM PasswordPolicyUserStatus us WHERE us.user.company.companyId = :companyId and us.user.type = :userType")
	
})

public class PasswordPolicyUserStatus implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "password_policy_user_status_id")
	private int passwordPolicyUserStatusId;
	
	@OneToOne
	@JoinColumn(name = "users_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "password_policy_id")
	private PasswordPolicy policy;
	
	@OneToMany(mappedBy = "userStatus", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, orphanRemoval = true)
	List<PasswordHistory> passwordHistory;
	
	@Column(name = "change_required")
	private boolean changeRequired;
	
	@Column(name = "locked")
	private boolean locked;
	
	@Column(name = "about_to_expire")
	private boolean aboutToExpire;
	
	@Column(name = "notification_sent")
	private boolean notificationSent;
	
	@Column(name = "expired")
	private boolean expired;
	
	@Column(name = "logon_attempt_timestamp")
	private Timestamp logonAttemptTimestamp;
	
	@Column(name = "last_change_time")
	private Timestamp lastChangeTime;
	
	@Column(name = "failed_logon_count")
	private int failedLogonCount;
	
	@Column(name = "expiration_date", insertable = false, updatable = false)
	private Timestamp expirationDate;
	
	@Column(name = "notification_date", insertable = false, updatable = false)
	private Timestamp notificationDate;

	public PasswordPolicyUserStatus() {
		super();
	}

	public int getPasswordPolicyUserStatusId() {
		return passwordPolicyUserStatusId;
	}

	public void setPasswordPolicyUserStatusId(int passwordPolicyUserStatusId) {
		this.passwordPolicyUserStatusId = passwordPolicyUserStatusId;
	}

	public boolean isChangeRequired() {
		return changeRequired;
	}

	public void setChangeRequired(boolean changeRequired) {
		this.changeRequired = changeRequired;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isAboutToExpire() {
		return aboutToExpire;
	}

	public void setAboutToExpire(boolean aboutToExpire) {
		this.aboutToExpire = aboutToExpire;
	}

	public boolean isNotificationSent() {
		return notificationSent;
	}

	public void setNotificationSent(boolean notificationSent) {
		this.notificationSent = notificationSent;
	}

	public Timestamp getLogonAttemptTimestamp() {
		return logonAttemptTimestamp;
	}

	public void setLogonAttemptTimestamp(Timestamp logonAttemptTimestamp) {
		this.logonAttemptTimestamp = logonAttemptTimestamp;
	}

	public int getFailedLogonCount() {
		return failedLogonCount;
	}

	public void setFailedLogonCount(int failedLogonCount) {
		this.failedLogonCount = failedLogonCount;
	}

	public PasswordPolicy getPolicy() {
		return policy;
	}

	public void setPolicy(PasswordPolicy policy) {
		this.policy = policy;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<PasswordHistory> getPasswordHistory() {
		return passwordHistory;
	}

	public void setPasswordHistory(List<PasswordHistory> passwordHistory) {
		this.passwordHistory = passwordHistory;
	}

	public Timestamp getLastChangeTime() {
		return lastChangeTime;
	}

	public void setLastChangeTime(Timestamp lastChangeTime) {
		this.lastChangeTime = lastChangeTime;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public Timestamp getExpirationDate() {
		return expirationDate;
	}

	public Timestamp getNotificationDate() {
		return notificationDate;
	}
   
}
