package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.AdditionalCriteria;

/**
 * The persistent class for the company database table.
 * 
 */
@Entity
@Table(name = "company_view")
@NamedQueries({

		@NamedQuery(name = "findAllCompaniesOrdered", query = "Select c from Company c order by c.coName"),
		@NamedQuery(name = "findAllActiveAssessment", query = "Select c from Company c where c.expired = false and c.assessmentProductCount > 0"),
		@NamedQuery(name = "findAllCompaniesFiltered", query = "Select c from Company c where c.companyId IN :companies order by c.coName"),
		@NamedQuery(name = "getCompanyBatchUsers", query = "Select c from Company c where c.companyId = :id", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "c.users") }),
		@NamedQuery(name = "getCompanyBatchProjects", query = "Select c from Company c where c.companyId = :id", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "c.projects") }),
		@NamedQuery(name = "getCompanyBatchUsersProjects", query = "Select c from Company c where c.companyId = :id", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "c.projects"),
				@QueryHint(name = "eclipselink.batch", value = "c.users") }) })
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// db scheme is missing IDENTITY for this table
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "company_id")
	private int companyId;

	private short admininstall;

	@Column(name = "BillingContact")
	private String billingContact;

	@Column(name = "BillingPhone")
	private String billingPhone;

	@Column(name = "Coaddress")
	private String coaddress;

	@Column(name = "Coaddress2")
	private String coaddress2;

	@Column(name = "CoCity")
	private String coCity;

	@Column(name = "CoID")
	private String coID;

	@Column(name = "CoName")
	private String coName;

	private short convlogoflag;

	private int convtype;

	@Column(name = "CoState")
	private String coState;

	@Column(name = "CoZip")
	private String coZip;

	@Column(name = "country_iso_code")
	private String countryIsoCode;
	
	@Column(name = "main_contact")
	private String mainContact;
	
	@Column(name = "main_phone")
	private String mainPhone;
	
	@Column(name = "custom_banner_count")
	private int customBannerCount;

	@Column(name = "enforce_secure_password")
	private short enforceSecurePassword;

	private String filePath;

	@Column(name = "flash_flag")
	private short flashFlag;

	private String HRAdmPhone;

	private String HRcontact;

	@Column(name = "iadvice_flag")
	private short iadviceFlag;

	private String ISAdmPhone;

	private String IScontact;

	@Column(name = "lang_pref")
	private String langPref;

	private Time lastupdate;

	private int lpoolflag;

	private int permitallprog;

	@Column(name = "randomize_passwords")
	private short randomizePasswords;

	private String rowguid;

	private short sendUpdate;

	@Column(name = "ServerDesc")
	private String serverDesc;

	private String serverid;

	@Column(name = "show_custom_banner")
	private short showCustomBanner;

	@Column(name = "show_lang_pref")
	private short showLangPref;

	@Column(name = "show_member_details")
	private short showMemberDetails;

	@Column(name = "show_multirater")
	private short showMultirater;

	@Column(name = "show_reporting")
	private short showReporting;

	private int startweek;

	private String STMPSrv;

	@Column(name = "SubType")
	private String subType;

	private String syspass;

	@Column(name = "use_ssl")
	private short useSsl;

	@Column(name = "WebPath")
	private String webPath;

	@Column(name = "project_count", insertable = false, updatable = false)
	private int projectCount;

	@Column(name = "users_count", insertable = false, updatable = false)
	private int usersCount;

	@Column(name = "ap_count", insertable = false, updatable = false)
	private int assessmentProductCount;

	@Column(name = "coach_flag")
	private boolean coachFlag;
	
	@Column(name = "expired")
	private boolean expired;

	// bi-directional many-to-many association to ScheduledEmailCompany
	@OneToMany(mappedBy = "company")
	private List<ScheduledEmailCompany> scheduledEmailCompanies;

	// bi-directional many-to-one association to Project
	@OneToMany(mappedBy = "company")
	private List<Project> projects;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "company")
	private List<User> users;

	@OneToMany(mappedBy = "company")
	private List<CourseRoster> courseRosters;

	// bi-directional many-to-one association to Position
	@OneToMany(mappedBy = "company")
	private List<CourseVisibility> courseVisiblities;

	@OneToMany(mappedBy = "company")
	private List<Roster> rosters;

	@OneToMany(mappedBy = "company")
	private List<CompanyProduct> companyProducts;

	// bi-directional many-to-one association to Project
	@OneToMany(mappedBy = "company")
	private List<Hchylvl> hchylvls;

	// bi-directional many-to-one association to ScheduledEmailLog
	@OneToMany(mappedBy = "company")
	private List<ScheduledEmailLog> scheduledEmailLogs;

	// bi-directional many-to-one association to ScheduledEmailStatus
	@OneToMany(mappedBy = "company")
	private List<ScheduledEmailStatus> scheduledEmailStatuses;

	@OneToMany(mappedBy = "company")
	private List<CompanyProjectType> companyProjectTypes;
	
	@OneToMany(mappedBy = "company")
	private List<CompanyPasswordPolicy> passwordPolicies;

	public Company() {
	}

	public int getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public short getAdmininstall() {
		return this.admininstall;
	}

	public void setAdmininstall(short admininstall) {
		this.admininstall = admininstall;
	}

	public String getBillingContact() {
		return this.billingContact;
	}

	public void setBillingContact(String billingContact) {
		this.billingContact = billingContact;
	}

	public String getBillingPhone() {
		return this.billingPhone;
	}

	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}

	public String getCoaddress() {
		return this.coaddress;
	}

	public void setCoaddress(String coaddress) {
		this.coaddress = coaddress;
	}

	public String getCoCity() {
		return this.coCity;
	}

	public void setCoCity(String coCity) {
		this.coCity = coCity;
	}

	public String getCoID() {
		return this.coID;
	}

	public void setCoID(String coID) {
		this.coID = coID;
	}

	public String getCoName() {
		return this.coName;
	}

	public void setCoName(String coName) {
		this.coName = coName;
	}

	public short getConvlogoflag() {
		return this.convlogoflag;
	}

	public void setConvlogoflag(short convlogoflag) {
		this.convlogoflag = convlogoflag;
	}

	public int getConvtype() {
		return this.convtype;
	}

	public void setConvtype(int convtype) {
		this.convtype = convtype;
	}

	public String getCoState() {
		return this.coState;
	}

	public void setCoState(String coState) {
		this.coState = coState;
	}

	public String getCoZip() {
		return this.coZip;
	}

	public void setCoZip(String coZip) {
		this.coZip = coZip;
	}

	public int getCustomBannerCount() {
		return this.customBannerCount;
	}

	public void setCustomBannerCount(int customBannerCount) {
		this.customBannerCount = customBannerCount;
	}

	public short getEnforceSecurePassword() {
		return this.enforceSecurePassword;
	}

	public void setEnforceSecurePassword(short enforceSecurePassword) {
		this.enforceSecurePassword = enforceSecurePassword;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public short getFlashFlag() {
		return this.flashFlag;
	}

	public void setFlashFlag(short flashFlag) {
		this.flashFlag = flashFlag;
	}

	public String getHRAdmPhone() {
		return this.HRAdmPhone;
	}

	public void setHRAdmPhone(String HRAdmPhone) {
		this.HRAdmPhone = HRAdmPhone;
	}

	public String getHRcontact() {
		return this.HRcontact;
	}

	public void setHRcontact(String HRcontact) {
		this.HRcontact = HRcontact;
	}

	public short getIadviceFlag() {
		return this.iadviceFlag;
	}

	public void setIadviceFlag(short iadviceFlag) {
		this.iadviceFlag = iadviceFlag;
	}

	public String getISAdmPhone() {
		return this.ISAdmPhone;
	}

	public void setISAdmPhone(String ISAdmPhone) {
		this.ISAdmPhone = ISAdmPhone;
	}

	public String getIScontact() {
		return this.IScontact;
	}

	public void setIScontact(String IScontact) {
		this.IScontact = IScontact;
	}

	public String getLangPref() {
		return this.langPref;
	}

	public void setLangPref(String langPref) {
		this.langPref = langPref;
	}

	public Time getLastupdate() {
		return this.lastupdate;
	}

	public void setLastupdate(Time lastupdate) {
		this.lastupdate = lastupdate;
	}

	public int getLpoolflag() {
		return this.lpoolflag;
	}

	public void setLpoolflag(int lpoolflag) {
		this.lpoolflag = lpoolflag;
	}

	public int getPermitallprog() {
		return this.permitallprog;
	}

	public void setPermitallprog(int permitallprog) {
		this.permitallprog = permitallprog;
	}

	public short getRandomizePasswords() {
		return this.randomizePasswords;
	}

	public void setRandomizePasswords(short randomizePasswords) {
		this.randomizePasswords = randomizePasswords;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public short getSendUpdate() {
		return this.sendUpdate;
	}

	public void setSendUpdate(short sendUpdate) {
		this.sendUpdate = sendUpdate;
	}

	public String getServerDesc() {
		return this.serverDesc;
	}

	public void setServerDesc(String serverDesc) {
		this.serverDesc = serverDesc;
	}

	public String getServerid() {
		return this.serverid;
	}

	public void setServerid(String serverid) {
		this.serverid = serverid;
	}

	public short getShowCustomBanner() {
		return this.showCustomBanner;
	}

	public void setShowCustomBanner(short showCustomBanner) {
		this.showCustomBanner = showCustomBanner;
	}

	public short getShowLangPref() {
		return this.showLangPref;
	}

	public void setShowLangPref(short showLangPref) {
		this.showLangPref = showLangPref;
	}

	public short getShowMemberDetails() {
		return this.showMemberDetails;
	}

	public void setShowMemberDetails(short showMemberDetails) {
		this.showMemberDetails = showMemberDetails;
	}

	public short getShowMultirater() {
		return this.showMultirater;
	}

	public void setShowMultirater(short showMultirater) {
		this.showMultirater = showMultirater;
	}

	public short getShowReporting() {
		return this.showReporting;
	}

	public void setShowReporting(short showReporting) {
		this.showReporting = showReporting;
	}

	public int getStartweek() {
		return this.startweek;
	}

	public void setStartweek(int startweek) {
		this.startweek = startweek;
	}

	public String getSTMPSrv() {
		return this.STMPSrv;
	}

	public void setSTMPSrv(String STMPSrv) {
		this.STMPSrv = STMPSrv;
	}

	public String getSubType() {
		return this.subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getSyspass() {
		return this.syspass;
	}

	public void setSyspass(String syspass) {
		this.syspass = syspass;
	}

	public short getUseSsl() {
		return this.useSsl;
	}

	public void setUseSsl(short useSsl) {
		this.useSsl = useSsl;
	}

	public String getWebPath() {
		return this.webPath;
	}

	public void setWebPath(String webPath) {
		this.webPath = webPath;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void setProjectCount(int projectCount) {
		this.projectCount = projectCount;
	}

	public int getProjectCount() {
		return projectCount;
	}

	public void setUsersCount(int usersCount) {
		this.usersCount = usersCount;
	}

	public int getUsersCount() {
		return usersCount;
	}

	public void setScheduledEmailCompanies(List<ScheduledEmailCompany> scheduledEmailCompanies) {
		this.scheduledEmailCompanies = scheduledEmailCompanies;
	}

	public List<ScheduledEmailCompany> getScheduledEmailCompanies() {
		return scheduledEmailCompanies;
	}

	public void setCourseRosters(List<CourseRoster> courseRosters) {
		this.courseRosters = courseRosters;
	}

	public List<CourseRoster> getCourseRosters() {
		return courseRosters;
	}

	public void setRosters(List<Roster> rosters) {
		this.rosters = rosters;
	}

	public List<Roster> getRosters() {
		return rosters;
	}

	public void setCourseVisiblities(List<CourseVisibility> courseVisiblities) {
		this.courseVisiblities = courseVisiblities;
	}

	public List<CourseVisibility> getCourseVisiblities() {
		return courseVisiblities;
	}

	public void setHchylvls(List<Hchylvl> hchylvls) {
		this.hchylvls = hchylvls;
	}

	public List<Hchylvl> getHchylvls() {
		return hchylvls;
	}

	public void setCompanyProducts(List<CompanyProduct> companyProducts) {
		this.companyProducts = companyProducts;
	}

	public List<CompanyProduct> getCompanyProducts() {
		return companyProducts;
	}

	public void setAssessmentProductCount(int assessmentProductCount) {
		this.assessmentProductCount = assessmentProductCount;
	}

	public int getAssessmentProductCount() {
		return assessmentProductCount;
	}

	public void setScheduledEmailLogs(List<ScheduledEmailLog> scheduledEmailLogs) {
		this.scheduledEmailLogs = scheduledEmailLogs;
	}

	public List<ScheduledEmailLog> getScheduledEmailLogs() {
		return scheduledEmailLogs;
	}

	public void setScheduledEmailStatuses(List<ScheduledEmailStatus> scheduledEmailStatuses) {
		this.scheduledEmailStatuses = scheduledEmailStatuses;
	}

	public List<ScheduledEmailStatus> getScheduledEmailStatuses() {
		return scheduledEmailStatuses;
	}

	/*****************************************************************/
	public void setCompanyProjectTypes(List<CompanyProjectType> value) {
		this.companyProjectTypes = value;
	}

	public List<CompanyProjectType> getCompanyProjectTypes() {
		return this.companyProjectTypes;
	}

	public boolean getCoachFlag() {
		return coachFlag;
	}

	public void setCoachFlag(boolean coachFlag) {
		this.coachFlag = coachFlag;
	}

	public List<CompanyPasswordPolicy> getPasswordPolicies() {
		return passwordPolicies;
	}

	public void setPasswordPolicies(List<CompanyPasswordPolicy> passwordPolicies) {
		this.passwordPolicies = passwordPolicies;
	}

	public String getCoaddress2() {
		return coaddress2;
	}

	public void setCoaddress2(String coaddress2) {
		this.coaddress2 = coaddress2;
	}

	public String getMainContact() {
		return mainContact;
	}

	public void setMainContact(String mainContact) {
		this.mainContact = mainContact;
	}

	public String getMainPhone() {
		return mainPhone;
	}

	public void setMainPhone(String mainPhone) {
		this.mainPhone = mainPhone;
	}

	public String getCountryIsoCode() {
		return countryIsoCode;
	}

	public void setCountryIsoCode(String countryIsoCode) {
		this.countryIsoCode = countryIsoCode;
	}

}