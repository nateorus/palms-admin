package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the scorm_course_directories database table.
 * 
 */
@Entity
@Table(name="scorm_course_directories")


@NamedQueries({
    @NamedQuery(name="findScormCourseIdByCourseId", query="select s.scormCourseId from ScormCourseDirectory s where s.courseId = :courseId")
})


public class ScormCourseDirectory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="course_id")
	private String courseId;

	@Id
	private int id;

	@Column(name="manifest_dir")
	private String manifestDir;

	@Column(name="scorm_course_id")
	private String scormCourseId;

	@Column(name="scorm_root_id")
	private String scormRootId;

    public ScormCourseDirectory() {
    }

	public String getCourseId() {
		return this.courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getManifestDir() {
		return this.manifestDir;
	}

	public void setManifestDir(String manifestDir) {
		this.manifestDir = manifestDir;
	}

	public String getScormCourseId() {
		return this.scormCourseId;
	}

	public void setScormCourseId(String scormCourseId) {
		this.scormCourseId = scormCourseId;
	}

	public String getScormRootId() {
		return this.scormRootId;
	}

	public void setScormRootId(String scormRootId) {
		this.scormRootId = scormRootId;
	}

}