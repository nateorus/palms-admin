package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the position database table.
 * 
 */
@Embeddable
public class PositionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="company_id",insertable=false, updatable=false)
	private int companyId;

	@Column(name="users_id",insertable=false, updatable=false)
	private int usersId;

	@Column(name="course",insertable=false, updatable=false)
	private int courseId;

    public PositionPK() {
    }
	public int getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getUsersId() {
		return this.usersId;
	}
	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}
	public int getCourse() {
		return this.courseId;
	}
	public void setCourse(int courseId) {
		this.courseId = courseId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PositionPK)) {
			return false;
		}
		PositionPK castOther = (PositionPK)other;
		return 
			(this.companyId == castOther.companyId)
			&& (this.usersId == castOther.usersId)
			&& (this.courseId == castOther.courseId);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.companyId;
		hash = hash * prime + this.usersId;
		hash = hash * prime + this.courseId;
		
		return hash;
    }
}