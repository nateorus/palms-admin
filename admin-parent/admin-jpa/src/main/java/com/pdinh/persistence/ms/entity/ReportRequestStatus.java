package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "report_request_status")
public class ReportRequestStatus implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static final String STATUS_REQUESTED = "REQUESTED";
	public static final String STATUS_QUEUED = "QUEUED";
	public static final String STATUS_PROCESSING = "PROCESSING";
	public static final String STATUS_GENERATED = "GENERATED";
	public static final String STATUS_STORED = "STORED";
	public static final String STATUS_ERROR = "ERROR";
	public static final String STATUS_QUEUEING_FAILED = "QUEUEING_FAILED";
	public static final String STATUS_CANCELLED = "CANCELLED";
	public static final String STATUS_DELETED = "DELETED";
	public static final String STATUS_ALL = "ALL";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "report_request_status_id", updatable=false)
	private int reportRequestStatusId;
	
	@Column(name = "code", updatable=false)
	private String code;
	
	@Column(name = "label", updatable=false)
	private String label;
	
	@Column(name = "description", updatable=false)
	private String description;

	public int getReportRequestStatusId() {
		return reportRequestStatusId;
	}

	public void setReportRequestStatusId(int reportRequestStatusId) {
		this.reportRequestStatusId = reportRequestStatusId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
