/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: TargetLevelType
 * 
 */
@Entity
@Table(name = "target_level_type")
public class TargetLevelType implements Serializable {
	//
	// Static variables
	//
	private static final long serialVersionUID = 1L;

	// These are the A by D target levels
	// Additional levels (for other groups) may be needed in the future
	public static final int FLL = 1;
	public static final int MLL = 2;
	public static final int SLL = 3;
	public static final int SEA = 4;
	public static final int IC = 5;
	public static final int BUL = 6;
	public static final int CEO = 7;
	public static final int BOARD = 8;

	//
	// Static methods.
	//

	//
	// Instance variables.
	//
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "target_level_type_id")
	private int targetLevelTypeId;

	private String name;

	private String code;

	@Column(name = "target_level_index")
	private int targetLevelIndex;

	//
	// Constructors.
	//

	public TargetLevelType() {
		super();
	}

	public TargetLevelType(int id) {
		this.targetLevelTypeId = id;
	}

	//
	// Instance methods.
	//

	// *******************************************************************************************//
	// //
	// Get/Set Functions //
	// //
	// *******************************************************************************************//

	/*****************************************************************************************/
	public int getTargetLevelTypeId() {
		return this.targetLevelTypeId;
	}

	public void setTargetLevelTypeId(int value) {
		this.targetLevelTypeId = value;
	}

	/*****************************************************************************************/
	public String getName() {
		return this.name;
	}

	public void setName(String value) {
		this.name = value;
	}

	/*****************************************************************************************/
	public String getCode() {
		return this.code;
	}

	public void setCode(String value) {
		this.name = code;
	}

	/*****************************************************************************************/
	public int getTargetLevelIndex() {
		return this.targetLevelIndex;
	}

	public void setTargetLevelIndex(int value) {
		this.targetLevelIndex = value;
	}
}
