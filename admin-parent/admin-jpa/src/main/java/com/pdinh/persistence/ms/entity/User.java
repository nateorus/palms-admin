package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.pdinh.jaxb.adapter.SqlTimestampAdapter;
import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@EntityListeners(value = EntityAuditListener.class)
@Table(name = "users")
@NamedQueries({
		@NamedQuery(name = "getUserByEmailAndCompany", query = "SELECT u FROM User u WHERE u.email = :email AND u.company.companyId = :companyId  AND u.active = 1"),
		@NamedQuery(name = "getUserByUsername", query = "SELECT u FROM User u WHERE u.username = :username AND u.active = 1"),
		@NamedQuery(name = "findSuperAdminUsers", query = "SELECT u FROM User u WHERE u.type = 4 AND u.active = 1 AND u.company.companyId = :companyId ORDER BY u.type, u.lastname ASC"),
		@NamedQuery(name = "findAdminUsers", query = "SELECT u FROM User u WHERE u.type IN (4,3) AND u.active = 1 AND u.company.companyId = :companyId ORDER BY u.type, u.lastname ASC"),
		// --NW: Called from projectDAO, gets ProjectUser entities... don't
		// blame me
		@NamedQuery(name = "findUsersInProjectByProjectId", query = "SELECT pu FROM ProjectUser pu WHERE pu.user.active = 1 AND pu.project.projectId = :projectId ORDER BY pu.user.lastname", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "pu.user"),
				@QueryHint(name = "eclipselink.batch", value = "pu.user.scheduledEmailStatuses"),
				@QueryHint(name = "eclipselink.batch", value = "pu.user.positions.course") }),

		@NamedQuery(name = "findActiveUsersByCompanyId", query = "SELECT u FROM User u WHERE u.active = 1 AND u.company.companyId = :companyId ORDER BY u.lastname ASC", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "u.projectUsers") }),
		@NamedQuery(name = "findUsersNotInProjectByProjectId", query = "SELECT u FROM User u WHERE u.active = 1 AND u.company.companyId = :companyId "
				+ "and u.usersId not in (SELECT pu.user.usersId FROM ProjectUser pu WHERE pu.project.projectId = :projectId ) ORDER BY u.lastname"),
		@NamedQuery(name = "isUniqueUsername", query = "SELECT count(u.username) FROM User u WHERE u.username = :username"),
		@NamedQuery(name = "isUniqueUsernameForUser", query = "SELECT count(u.username) FROM User u WHERE u.username = :username AND u.usersId = :usersId"),
		@NamedQuery(name = "findAllActive", query = "SELECT u FROM User u WHERE u.active = 1"),
		@NamedQuery(name = "findAllCoaches", query = "SELECT u FROM User u WHERE u.coachFlag = true"),
		@NamedQuery(name = "findActiveUnlinkedInCompany", query = "SELECT u FROM User u WHERE u.active = 1 and u.externallyManaged=0 and u.company.companyId = :companyId"),
		@NamedQuery(name = "findTinCanUsersByCompanyId", query = "SELECT distinct u FROM User u, Position p WHERE u.active = 1 AND u.company.companyId = :companyId AND u.usersId = p.user.usersId AND p.course.course IN (SELECT c.course from Course c where c.tinCanUri IS NOT NULL) ORDER BY u.lastname ASC", hints = { @QueryHint(name = "eclipselink.batch.type", value = "EXISTS") }),

		@NamedQuery(name = "findUserDataForBulkUpload", query = "SELECT new com.pdinh.persistence.dto.BulkUploadUser(u.usersId, u.firstname, u.lastname, u.email, u.supervisorName, u.supervisorEmail, u.langPref, u.optional1, u.optional2, u.optional3, u.username, u.password) "
				+ "FROM User u WHERE u.active = 1 AND u.company.companyId = :companyId AND u.email in :userEmails"),
		@NamedQuery(name = "findUsersInList", query = "SELECT u FROM User u WHERE u.active = 1 AND u.usersId in :userIds"),
		@NamedQuery(name = "findByCompanyIdAndProjectTypeId", query = "SELECT distinct u FROM User u, ProjectUser pu WHERE u.active = 1 AND u.company.companyId = :companyId AND u.usersId = pu.user.usersId AND pu.project.projectType.projectTypeId = :projectTypeId ORDER BY u.lastname ASC", hints = { @QueryHint(name = "eclipselink.batch.type", value = "EXISTS") })

})
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements EntityAuditLogger, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "users_id")
	@XmlAttribute
	private int usersId;

	@Column(name = "a_accept")
	private short aAccept;

	@Column(name = "a_accept_date")
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp aAcceptDate;

	@Column(name = "access_mode")
	private short accessMode;

	private short active;

	private String addr;

	private String addr2;

	@Column(name = "admin_home_page")
	private int adminHomePage;

	private int agegrp;

	private int ahlevel;

	@Column(name = "cc_tour_launched")
	private short ccTourLaunched;

	private String city;

	private int cojobid;

	private int convblck;

	private int convmod;

	private int cosubid;

	private short dispatch;

	private short edlevel;

	private String email;

	private String employeeid;

	private int empstatus;

	private String firstname;

	private int funcarea;

	private short gender;

	private String handle;

	private int industry;

	private int jobclass;

	private String jobtitle;

	private short laccept;

	private int ladmin;

	@Column(name = "lang_pref")
	private String langPref;

	private String lastname;

	@Column(name = "launch_demo")
	private int launchDemo;

	private short learningType;

	@Column(name = "login_status")
	private short loginStatus;

	private short mentorGender;

	private short mserverid;

	private int onlineaccess;

	@Column(name = "optional_1")
	private String optional1;

	@Column(name = "optional_2")
	private String optional2;

	@Column(name = "optional_3")
	private String optional3;

	private String password;

	@Column(name = "report_criteria_param")
	private String reportCriteriaParam;

	private String rowguid;

	private String rowguid51;

	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp startdate;

	private String state;

	@Column(name = "supervisor_email")
	private String supervisorEmail;

	@Column(name = "supervisor_name")
	private String supervisorName;

	private int timeind;

	private int timeonline;

	private int timeorg;

	private short tiPosition;

	private String title;

	private int type;

	@Column(name = "userid_old")
	private int useridOld;

	@Column(name = "username")
	private String username;

	private int workenv;

	private short workwatch;

	private String zip;

	private byte[] hashedpassword;

	@Column(name = "ext_admin_id")
	private String extAdminId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "office_location_id")
	private OfficeLocation officeLocation;

	// bi-directional many-to-one association to Position
	@OneToMany(mappedBy = "user", cascade = { CascadeType.PERSIST })
	@XmlTransient
	private List<Position> positions;

	// bi-directional many-to-one association to Roster
	@OneToMany(mappedBy = "user", cascade = { CascadeType.PERSIST })
	@XmlTransient
	private List<Roster> rosters;

	// bi-directional many-to-one association to ProjectUser
	@OneToMany(mappedBy = "user")
	@XmlTransient
	private List<ProjectUser> projectUsers;

	// bi-directional many-to-one association to UserCoursePrefs
	@OneToMany(mappedBy = "user")
	@XmlTransient
	private List<UserCoursePrefs> userCoursePrefs;

	// bi-directional many-to-one association to ParticipantNote
	@OneToMany(mappedBy = "user")
	@XmlTransient
	private List<UserNote> participantNotes;

	// bi-directional many-to-one association to UserCoursePrefs
	@OneToMany(mappedBy = "admin")
	@XmlTransient
	private List<Project> projects;

	// bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name = "company_id")
	@XmlTransient
	private Company company;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "user", cascade = { CascadeType.PERSIST }, orphanRemoval = true, targetEntity = UserDocument.class)
	private List<UserDocument> userDocuments = new ArrayList<UserDocument>();

	// bi-directional many-to-one association to ScheduledEmailLog
	@OneToMany(mappedBy = "user")
	@XmlTransient
	private List<ScheduledEmailLog> scheduledEmailLogs;

	// bi-directional many-to-one association to ScheduledEmailStatus
	@OneToMany(mappedBy = "user")
	@XmlTransient
	private List<ScheduledEmailStatus> scheduledEmailStatuses;

	@Column(name = "coach_flag")
	private boolean coachFlag;

	@Column(name = "c_accept")
	private boolean cAccept;

	@Column(name = "c_accept_date")
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp cAcceptDate;

	@Column(name = "externally_managed")
	private boolean externallyManaged;

	@OneToOne(mappedBy = "user", fetch = FetchType.LAZY, targetEntity = PasswordPolicyUserStatus.class)
	@XmlTransient
	private PasswordPolicyUserStatus passwordPolicyStatus;

	@ManyToOne
	@JoinColumn(name = "country_id")
	@XmlTransient
	private Country country;

	@Column(name = "global_accept")
	private boolean globalAccept;

	@OneToMany(mappedBy = "user", orphanRemoval = true, targetEntity = PhoneNumber.class)
	private List<PhoneNumber> phoneNumbers;

	public List<PhoneNumber> getBusinessPhones() {
		List<PhoneNumber> numbers = new ArrayList<PhoneNumber>();
		for (PhoneNumber number : this.getPhoneNumbers()) {
			if (number.getPhoneNumberType().getAbbv().equals(PhoneNumberType.BUISINESS_PHONE_ABBV)) {
				numbers.add(number);
			}
		}
		return numbers;
	}

	public List<PhoneNumber> getHomePhones() {
		List<PhoneNumber> numbers = new ArrayList<PhoneNumber>();
		for (PhoneNumber number : this.getPhoneNumbers()) {
			if (number.getPhoneNumberType().getAbbv().equals(PhoneNumberType.HOME_PHONE_ABBV)) {
				numbers.add(number);
			}
		}
		return numbers;
	}

	public List<PhoneNumber> getMobilePhones() {
		List<PhoneNumber> numbers = new ArrayList<PhoneNumber>();
		for (PhoneNumber number : this.getPhoneNumbers()) {
			if (number.getPhoneNumberType().getAbbv().equals(PhoneNumberType.MOBILE_PHONE_ABBV)) {
				numbers.add(number);
			}
		}
		return numbers;
	}

	public User() {
	}

	public int getUsersId() {
		return this.usersId;
	}

	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public short getAAccept() {
		return this.aAccept;
	}

	public void setAAccept(short aAccept) {
		this.aAccept = aAccept;
	}

	public Timestamp getAAcceptDate() {
		return this.aAcceptDate;
	}

	public void setAAcceptDate(Timestamp aAcceptDate) {
		this.aAcceptDate = aAcceptDate;
	}

	public short getAccessMode() {
		return this.accessMode;
	}

	public void setAccessMode(short accessMode) {
		this.accessMode = accessMode;
	}

	public short getActive() {
		return this.active;
	}

	public void setActive(short active) {
		this.active = active;
	}

	public String getAddr() {
		return this.addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public int getAdminHomePage() {
		return this.adminHomePage;
	}

	public void setAdminHomePage(int adminHomePage) {
		this.adminHomePage = adminHomePage;
	}

	public int getAgegrp() {
		return this.agegrp;
	}

	public void setAgegrp(int agegrp) {
		this.agegrp = agegrp;
	}

	public int getAhlevel() {
		return this.ahlevel;
	}

	public void setAhlevel(int ahlevel) {
		this.ahlevel = ahlevel;
	}

	public short getCcTourLaunched() {
		return this.ccTourLaunched;
	}

	public void setCcTourLaunched(short ccTourLaunched) {
		this.ccTourLaunched = ccTourLaunched;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getCojobid() {
		return this.cojobid;
	}

	public void setCojobid(int cojobid) {
		this.cojobid = cojobid;
	}

	public int getConvblck() {
		return this.convblck;
	}

	public void setConvblck(int convblck) {
		this.convblck = convblck;
	}

	public int getConvmod() {
		return this.convmod;
	}

	public void setConvmod(int convmod) {
		this.convmod = convmod;
	}

	public int getCosubid() {
		return this.cosubid;
	}

	public void setCosubid(int cosubid) {
		this.cosubid = cosubid;
	}

	public short getDispatch() {
		return this.dispatch;
	}

	public void setDispatch(short dispatch) {
		this.dispatch = dispatch;
	}

	public short getEdlevel() {
		return this.edlevel;
	}

	public void setEdlevel(short edlevel) {
		this.edlevel = edlevel;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployeeid() {
		return this.employeeid;
	}

	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}

	public int getEmpstatus() {
		return this.empstatus;
	}

	public void setEmpstatus(int empstatus) {
		this.empstatus = empstatus;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getFuncarea() {
		return this.funcarea;
	}

	public void setFuncarea(int funcarea) {
		this.funcarea = funcarea;
	}

	public short getGender() {
		return this.gender;
	}

	public void setGender(short gender) {
		this.gender = gender;
	}

	public String getHandle() {
		return this.handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public int getIndustry() {
		return this.industry;
	}

	public void setIndustry(int industry) {
		this.industry = industry;
	}

	public int getJobclass() {
		return this.jobclass;
	}

	public void setJobclass(int jobclass) {
		this.jobclass = jobclass;
	}

	public String getJobtitle() {
		return this.jobtitle;
	}

	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}

	public short getLaccept() {
		return this.laccept;
	}

	public void setLaccept(short laccept) {
		this.laccept = laccept;
	}

	public int getLadmin() {
		return this.ladmin;
	}

	public void setLadmin(int ladmin) {
		this.ladmin = ladmin;
	}

	public String getLangPref() {
		return this.langPref;
	}

	public void setLangPref(String langPref) {
		this.langPref = langPref;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getLaunchDemo() {
		return this.launchDemo;
	}

	public void setLaunchDemo(int launchDemo) {
		this.launchDemo = launchDemo;
	}

	public short getLearningType() {
		return this.learningType;
	}

	public void setLearningType(short learningType) {
		this.learningType = learningType;
	}

	public short getLoginStatus() {
		return this.loginStatus;
	}

	public void setLoginStatus(short loginStatus) {
		this.loginStatus = loginStatus;
	}

	public short getMentorGender() {
		return this.mentorGender;
	}

	public void setMentorGender(short mentorGender) {
		this.mentorGender = mentorGender;
	}

	public short getMserverid() {
		return this.mserverid;
	}

	public void setMserverid(short mserverid) {
		this.mserverid = mserverid;
	}

	public int getOnlineaccess() {
		return this.onlineaccess;
	}

	public void setOnlineaccess(int onlineaccess) {
		this.onlineaccess = onlineaccess;
	}

	public String getOptional1() {
		return this.optional1;
	}

	public void setOptional1(String optional1) {
		this.optional1 = optional1;
	}

	public String getOptional2() {
		return this.optional2;
	}

	public void setOptional2(String optional2) {
		this.optional2 = optional2;
	}

	public String getOptional3() {
		return this.optional3;
	}

	public void setOptional3(String optional3) {
		this.optional3 = optional3;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReportCriteriaParam() {
		return this.reportCriteriaParam;
	}

	public void setReportCriteriaParam(String reportCriteriaParam) {
		this.reportCriteriaParam = reportCriteriaParam;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public String getRowguid51() {
		return this.rowguid51;
	}

	public void setRowguid51(String rowguid51) {
		this.rowguid51 = rowguid51;
	}

	public Timestamp getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Timestamp startdate) {
		this.startdate = startdate;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSupervisorEmail() {
		return this.supervisorEmail;
	}

	public void setSupervisorEmail(String supervisorEmail) {
		this.supervisorEmail = supervisorEmail;
	}

	public String getSupervisorName() {
		return this.supervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	public int getTimeind() {
		return this.timeind;
	}

	public void setTimeind(int timeind) {
		this.timeind = timeind;
	}

	public int getTimeonline() {
		return this.timeonline;
	}

	public void setTimeonline(int timeonline) {
		this.timeonline = timeonline;
	}

	public int getTimeorg() {
		return this.timeorg;
	}

	public void setTimeorg(int timeorg) {
		this.timeorg = timeorg;
	}

	public short getTiPosition() {
		return this.tiPosition;
	}

	public void setTiPosition(short tiPosition) {
		this.tiPosition = tiPosition;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getUseridOld() {
		return this.useridOld;
	}

	public void setUseridOld(int useridOld) {
		this.useridOld = useridOld;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getWorkenv() {
		return this.workenv;
	}

	public void setWorkenv(int workenv) {
		this.workenv = workenv;
	}

	public short getWorkwatch() {
		return this.workwatch;
	}

	public void setWorkwatch(short workwatch) {
		this.workwatch = workwatch;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public List<Position> getPositions() {
		return this.positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	public List<ProjectUser> getProjectUsers() {
		return this.projectUsers;
	}

	public void setProjectUsers(List<ProjectUser> projectUsers) {
		this.projectUsers = projectUsers;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company companyBean) {
		this.company = companyBean;
	}

	public List<UserDocument> getUserDocuments() {
		return userDocuments;
	}

	public void setUserDocuments(List<UserDocument> userDocuments) {
		this.userDocuments = userDocuments;
	}

	public void setRosters(List<Roster> rosters) {
		this.rosters = rosters;
	}

	public List<Roster> getRosters() {
		return rosters;
	}

	public void setUserCoursePrefs(List<UserCoursePrefs> userCoursePrefs) {
		this.userCoursePrefs = userCoursePrefs;
	}

	public List<UserCoursePrefs> getUserCoursePrefs() {
		return userCoursePrefs;
	}

	public void setParticipantNotes(List<UserNote> participantNotes) {
		this.participantNotes = participantNotes;
	}

	public List<UserNote> getParticipantNotes() {
		return participantNotes;
	}

	public void setExtAdminId(String extAdminId) {
		this.extAdminId = extAdminId;
	}

	public String getExtAdminId() {
		return extAdminId;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setScheduledEmailLogs(List<ScheduledEmailLog> scheduledEmailLogs) {
		this.scheduledEmailLogs = scheduledEmailLogs;
	}

	public List<ScheduledEmailLog> getScheduledEmailLogs() {
		return scheduledEmailLogs;
	}

	public void setScheduledEmailStatuses(List<ScheduledEmailStatus> scheduledEmailStatuses) {
		this.scheduledEmailStatuses = scheduledEmailStatuses;
	}

	public List<ScheduledEmailStatus> getScheduledEmailStatuses() {
		return scheduledEmailStatuses;
	}

	public byte[] getHashedpassword() {
		return hashedpassword;
	}

	public void setHashedpassword(byte[] hashedpassword) {
		this.hashedpassword = hashedpassword;
	}

	public boolean isCoachFlag() {
		return coachFlag;
	}

	public boolean getCoachFlag() {
		return isCoachFlag();
	}

	public void setCoachFlag(boolean coachFlag) {
		this.coachFlag = coachFlag;
	}

	public boolean iscAccept() {
		return cAccept;
	}

	public void setcAccept(boolean cAccept) {
		this.cAccept = cAccept;
	}

	public Timestamp getcAcceptDate() {
		return cAcceptDate;
	}

	public void setcAcceptDate(Timestamp cAcceptDate) {
		this.cAcceptDate = cAcceptDate;
	}

	public OfficeLocation getOfficeLocation() {
		return officeLocation;
	}

	public void setOfficeLocation(OfficeLocation officeLocation) {
		this.officeLocation = officeLocation;
	}

	public boolean isExternallyManaged() {
		return externallyManaged;
	}

	public void setExternallyManaged(boolean externallyManaged) {
		this.externallyManaged = externallyManaged;
	}

	public PasswordPolicyUserStatus getPasswordPolicyStatus() {
		return passwordPolicyStatus;
	}

	public void setPasswordPolicyStatus(PasswordPolicyUserStatus passwordPolicyStatus) {
		this.passwordPolicyStatus = passwordPolicyStatus;
	}

	public boolean isGlobalAccept() {
		return globalAccept;
	}

	public void setGlobalAccept(boolean globalAccept) {
		this.globalAccept = globalAccept;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user,
						EntityAuditLogger.OP_CREATE_USER, getUsersId(), getUsername(), getFirstname(), getLastname(),
						getEmail() });
			} else {
				auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user, operation,
						getUsersId(), getUsername(), getFirstname(), getLastname(), getEmail() });
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user,
						EntityAuditLogger.OP_UPDATE_USER, getUsersId(), getUsername(), getFirstname(), getLastname(),
						getEmail() });
			} else {
				auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user, operation,
						getUsersId(), getUsername(), getFirstname(), getLastname(), getEmail() });
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:
			// This should never happen
			auditLog.info("{} performed DELETE_USER operation on user: {}({}), {}, {}, {}", new Object[] { user,
					getUsersId(), getUsername(), getFirstname(), getLastname(), getEmail() });
			break;
		}

	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

}