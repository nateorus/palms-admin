package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.CacheIsolationType;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the coaching_plan database table.
 * 
 */
@Entity
@EntityListeners(value=EntityAuditListener.class)
@Table(name = "coaching_plan")
@NamedQueries({
		@NamedQuery(name = "findByProjectAndParticipant", query = "SELECT cp FROM CoachingPlan cp WHERE cp.participant.usersId = :participantId AND cp.project.projectId = :projectId"),
		@NamedQuery(name = "findCoachingPlansByProjectId", query = "SELECT cp FROM CoachingPlan cp WHERE cp.project.projectId = :projectId"),
		@NamedQuery(name = "findByParticipant", query = "SELECT cp FROM CoachingPlan cp WHERE cp.participant.usersId = :participantId AND cp.coach.usersId != 0"),
		@NamedQuery(name = "findByCoach", query = "SELECT cp FROM CoachingPlan cp WHERE cp.coach.usersId = :coachId"),
		@NamedQuery(name = "findByCoachAndCompany", query = "SELECT cp FROM CoachingPlan cp WHERE cp.coach.usersId = :coachId and cp.participant.company.companyId = :companyId"),
		@NamedQuery(name = "findByCoachAndProject", query = "SELECT cp FROM CoachingPlan cp WHERE cp.coach.usersId = :coachId and cp.project.projectId IN :projectIds")})
@Cache(isolation = CacheIsolationType.ISOLATED)
public class CoachingPlan implements EntityAuditLogger, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "coaching_plan_id")
	private int coachingPlanId;

	@Column(name = "coaching_plan_document")
	@Basic
	private String coachingPlanDocument;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;

	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	// uni-directional many-to-one association to CoachingPlanStatusType
	@ManyToOne
	@JoinColumn(name = "coaching_plan_status_type_id")
	private CoachingPlanStatusType coachingPlanStatusType;

	@ManyToOne
	@JoinColumn(name = "target_level_type_id")
	@JoinFetch(JoinFetchType.OUTER)
	private TargetLevelType targetLevelType;

	@Column(name = "type_text")
	@Basic
	private String typeText;

	// bi-directional many-to-one association to User
	@ManyToOne(cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "coach_id")
	private User coach;

	// bi-directional many-to-one association to User
	@ManyToOne(cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "participant_id")
	private User participant;

	public CoachingPlan() {
	}

	public int getCoachingPlanId() {
		return this.coachingPlanId;
	}

	public void setCoachingPlanId(int coachingPlanId) {
		this.coachingPlanId = coachingPlanId;
	}

	public String getCoachingPlanDocument() {
		return this.coachingPlanDocument;
	}

	public void setCoachingPlanDocument(String coachingPlanDocument) {
		this.coachingPlanDocument = coachingPlanDocument;
	}

	public CoachingPlanStatusType getCoachingPlanStatusType() {
		return this.coachingPlanStatusType;
	}

	public void setCoachingPlanStatusType(CoachingPlanStatusType coachingPlanStatusType) {
		this.coachingPlanStatusType = coachingPlanStatusType;
	}

	public TargetLevelType getTargetLevelType() {
		return targetLevelType;
	}

	public void setTargetLevelType(TargetLevelType targetLevelType) {
		this.targetLevelType = targetLevelType;
	}

	public String getTypeText() {
		return typeText;
	}

	public void setTypeText(String typeText) {
		this.typeText = typeText;
	}

	public User getCoach() {
		return this.coach;
	}

	public void setCoach(User coach) {
		this.coach = coach;
	}

	public User getParticipant() {
		return this.participant;
	}

	public void setParticipant(User participant) {
		this.participant = participant;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "CoachingPlan [coachingPlanId=" + coachingPlanId + "\n\t, coachingPlanDocument=" + coachingPlanDocument
				+ "\n\t, endDate=" + endDate + "\n\t, lastModifiedDate=" + lastModifiedDate + "\n\t, project="
				+ project + "\n\t, startDate=" + startDate + "\n\t, coachingPlanStatusType=" + coachingPlanStatusType
				+ "\n\t, targetLevelType=" + targetLevelType + "\n\t, typeText=" + typeText + "\n\t, coach=" + coach
				+ "\n\t, participant=" + participant + "]";
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action){
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()){
				auditLog.info("{} performed {} operation on Coaching Plan for participant: ({}) in project: {}({})", new Object[]{user, EntityAuditLogger.OP_CREATE_COACHING_PLAN, getParticipant().getFirstname() + " " + getParticipant().getLastname(),getProject().getProjectId(),getProject().getName()});
			}else{
				auditLog.info("{} performed {} operation on Coaching Plan for participant: ({}) in project: {}({})", new Object[]{user, operation,getParticipant().getFirstname() + " " + getParticipant().getLastname(),getProject().getProjectId(),getProject().getName()});
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()){
				auditLog.info("{} performed {} operation on Coaching Plan for participant: ({}) in project: {}({})", new Object[]{user, EntityAuditLogger.OP_UPDATE_COACHING_PLAN, getParticipant().getFirstname() + " " + getParticipant().getLastname(),getProject().getProjectId(),getProject().getName()});
			}else{
				auditLog.info("{} performed {} operation on Coaching Plan for participant: ({}) in project: {}({})", new Object[]{user, operation, getParticipant().getFirstname() + " " + getParticipant().getLastname(),getProject().getProjectId(),getProject().getName()});
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:
			
			auditLog.info("{} performed {} operation on Coaching Plan id: {},  for participant: ({}) in project: {}({})", new Object[]{user, EntityAuditLogger.OP_DELETE_COACHING_PLAN, getCoachingPlanId(),getParticipant().getFirstname() + " " + getParticipant().getLastname(), getProject().getProjectId(),getProject().getName()});
			break;
		}
		
	}

}