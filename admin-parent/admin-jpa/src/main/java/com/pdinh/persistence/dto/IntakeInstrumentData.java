package com.pdinh.persistence.dto;

/**
 * POJO used to obtain minimal information necessary from platform DB in order
 * to populate assessment_intrument table in LRM database
 * 
 * @author dbailey
 * 
 */
public class IntakeInstrumentData {

	private final String name;
	private final int courseId;
	private final String courseCode;
	private final String courseFlowTypeName;
	private final boolean languageSupported;

	public IntakeInstrumentData(String name, int courseId, String courseCode, String courseFlowTypeName,
			boolean languageSupported) {
		this.name = name;
		this.courseId = courseId;
		this.courseCode = courseCode;
		this.courseFlowTypeName = courseFlowTypeName;
		this.languageSupported = languageSupported;
	}

	public String getName() {
		return name;
	}

	public int getCourseId() {
		return courseId;
	}

	public String getCourseFlowTypeName() {
		return courseFlowTypeName;
	}

	public boolean isLanguageSupported() {
		return languageSupported;
	}

	public String getCourseCode() {
		return courseCode;
	}
}
