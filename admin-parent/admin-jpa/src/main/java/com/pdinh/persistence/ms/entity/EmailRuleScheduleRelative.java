package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the email_rule_schedule_relative database table.
 * 
 */
@Entity
@Table(name="email_rule_schedule_relative")
public class EmailRuleScheduleRelative extends EmailRuleSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="days")
	private int days;

	@Column(name="days_after")
	private Boolean daysAfter;

	@Column(name="relative_to_date")
	private Timestamp relativeToDate;

    public EmailRuleScheduleRelative() {
    }

	public int getDays() {
		return this.days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public Boolean getDaysAfter() {
		return this.daysAfter;
	}

	public void setDaysAfter(Boolean daysAfter) {
		this.daysAfter = daysAfter;
	}

	public Timestamp getRelativeToDate() {
		return this.relativeToDate;
	}

	public void setRelativeToDate(Timestamp relativeToDate) {
		this.relativeToDate = relativeToDate;
	}

}