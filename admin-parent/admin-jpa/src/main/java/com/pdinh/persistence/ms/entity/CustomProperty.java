package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the custom_properties database table.
 * 
 */
@Entity
@Table(name = "custom_properties")
public class CustomProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int customPropertyId;

	@Column(name = "section_id")
	private String sectionId;

	@Column(name = "properties_name")
	private String propertiesName;

	private Boolean overwrite;

	public CustomProperty() {
	}

	public int getCustomPropertyId() {
		return customPropertyId;
	}

	public void setCustomPropertyId(int customPropertyId) {
		this.customPropertyId = customPropertyId;
	}

	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	public String getPropertiesName() {
		return propertiesName;
	}

	public void setPropertiesName(String propertiesName) {
		this.propertiesName = propertiesName;
	}

	public Boolean getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(Boolean overwrite) {
		this.overwrite = overwrite;
	}

}