package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the ScheduledEmailLog database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "findScheduledEmailLogsByProjectId", query = "select sel from ScheduledEmailLog sel where sel.project.projectId = :projectId")
})
public class ScheduledEmailLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="email_log_id")
	private int emailLogId;

	@Column(name="alt_email")
	private String altEmail;

	@Column(name="date_completed")
	private Timestamp dateCompleted;

	@Column(name="email_constant")
	private String emailConstant;

	@Column(name="language_code")
	private String languageCode;

	//bi-directional many-to-one association to User
    @ManyToOne
	@JoinColumn(name="users_id" )
	private User user;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;

	//bi-directional many-to-one association to Project
    @ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

	//bi-directional many-to-one association to ScheduledEmailDef
    @ManyToOne
	@JoinColumn(name="email_id")
	private ScheduledEmailDef scheduledEmailDef;
    
    @JoinColumn(name="scheduled_email_log_status_id")
    private ScheduledEmailLogStatus status;
    
    //@Column(name="rule_id")
    //private int ruleId;
    
    @ManyToOne
    @JoinColumn(name="email_rule_schedule_id")
    private EmailRuleSchedule schedule;
    
    @ManyToOne
	@JoinColumn(name="rule_id" )
	private EmailRule emailRule;
        
    @Column(name="error_message")
    private String errorMessage;

    @Column(name="sent_email_subject")
    private String sentEmailSubject;

    @Column(name="sent_email_body")
    private String sentEmailBody;

    
    public ScheduledEmailLog() {
    }

	public int getEmailLogId() {
		return this.emailLogId;
	}

	public void setEmailLogId(int emailLogId) {
		this.emailLogId = emailLogId;
	}

	public String getAltEmail() {
		return this.altEmail;
	}

	public void setAltEmail(String altEmail) {
		this.altEmail = altEmail;
	}

	public Timestamp getDateCompleted() {
		return this.dateCompleted;
	}

	public void setDateCompleted(Timestamp dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public String getEmailConstant() {
		return this.emailConstant;
	}

	public void setEmailConstant(String emailConstant) {
		this.emailConstant = emailConstant;
	}

	public String getLanguageCode() {
		return this.languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	public ScheduledEmailDef getScheduledEmailDef() {
		return this.scheduledEmailDef;
	}

	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}


	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public EmailRule getEmailRule() {
		return emailRule;
	}

	public void setEmailRule(EmailRule emailRule) {
		this.emailRule = emailRule;
	}

	public EmailRuleSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(EmailRuleSchedule schedule) {
		this.schedule = schedule;
	}

	public ScheduledEmailLogStatus getStatus() {
		return status;
	}

	public void setStatus(ScheduledEmailLogStatus status) {
		this.status = status;
	}

	public String getSentEmailSubject() {
		return sentEmailSubject;
	}

	public void setSentEmailSubject(String sentEmailSubject) {
		this.sentEmailSubject = sentEmailSubject;
	}

	public String getSentEmailBody() {
		return sentEmailBody;
	}

	public void setSentEmailBody(String sentEmailBody) {
		this.sentEmailBody = sentEmailBody;
	}

	
}