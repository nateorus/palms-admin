package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the consent_user database table.
 * 
 */
@Entity
@Table(name = "consent_user")
@NamedQueries({
		@NamedQuery(name = "findAllUnAcceptedForParticipant", query = "SELECT cu FROM ConsentUser cu WHERE cu.user.usersId = :usersId AND cu.accepted = 0"),
		@NamedQuery(name = "findByParticipantAndType", query = "SELECT cu FROM ConsentUser cu WHERE cu.user.usersId = :usersId AND cu.consentType = :consentType") })
public class ConsentUser implements Serializable {
	private static final long serialVersionUID = 1L;

	public ConsentUser() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "consent_user_id")
	private int consentUserId;

	private short accepted;

	@Column(name = "consent_type")
	private String consentType;

	@Column(name = "date_accepted")
	private Timestamp dateAccepted;

	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	public int getConsentUserId() {
		return this.consentUserId;
	}

	public void setConsentUserId(int consentUserId) {
		this.consentUserId = consentUserId;
	}

	public short getAccepted() {
		return this.accepted;
	}

	public void setAccepted(short accepted) {
		this.accepted = accepted;
	}

	public String getConsentType() {
		return this.consentType;
	}

	public void setConsentType(String consentType) {
		this.consentType = consentType;
	}

	public Timestamp getDateAccepted() {
		return this.dateAccepted;
	}

	public void setDateAccepted(Timestamp now) {
		this.dateAccepted = now;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}