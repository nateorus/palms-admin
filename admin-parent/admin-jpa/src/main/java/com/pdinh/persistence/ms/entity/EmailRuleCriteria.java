package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: EmailRuleCriteria
 *
 */
@Entity
@Table(name="email_rule_criteria")
public class EmailRuleCriteria implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "email_rule_criteria_type_id")	
	private EmailRuleCriteriaType emailRuleCriteriaType;
	
    @ManyToOne
	@JoinColumn(name="email_rule_id")
	private EmailRule emailRule;
    
	public EmailRuleCriteria() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmailRuleCriteriaType getEmailRuleCriteriaType() {
		return emailRuleCriteriaType;
	}

	public void setEmailRuleCriteriaType(EmailRuleCriteriaType emailRuleCriteriaType) {
		this.emailRuleCriteriaType = emailRuleCriteriaType;
	}

	public EmailRule getEmailRule() {
		return emailRule;
	}

	public void setEmailRule(EmailRule emailRule) {
		this.emailRule = emailRule;
	}
   
}
