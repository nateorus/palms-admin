package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: AssessmentCourse
 * NOTE: This JPA entity is not mapped to a real table. It is the returned result from a 
 * Stored Procedure, user_course_list_status_assessment, which is used to populate the 
 * course list on the dashboard. 
 * As of now the class and the db object are at least one field out of sync.
 * If validation errors appear, turn off table validation for this project. 
 */
@Entity
@Table(name = "assessment_course")
public class AssessmentCourse implements Serializable {

	private static final long serialVersionUID = 1L;

	public AssessmentCourse() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "assessment_course_id")
	private int assessmentCourseId;

	@Column(name = "course")
	private int courseId;

	@Column(name = "title")
	private String title;

	@Column(name = "sblurb")
	private String sblurb;

	@Column(name = "type")
	private int type;

	@Column(name = "abbv")
	private String abbv;

	@Column(name = "description")
	private String description;

	@Column(name = "course_duration")
	private String courseDuration;

	@Column(name = "completion_status")
	private int completionStatus;
	
	@Column(name = "allow_report_download")
	private Boolean allowReportDownload;

	public int getAssessmentCourseId() {
		return assessmentCourseId;
	}

	public void setAssessmentCourseId(int assessmentCourseId) {
		this.assessmentCourseId = assessmentCourseId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSblurb() {
		return sblurb;
	}

	public void setSblurb(String sblurb) {
		this.sblurb = sblurb;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getAbbv() {
		return abbv;
	}

	public void setAbbv(String abbv) {
		this.abbv = abbv;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCourseDuration() {
		return courseDuration;
	}

	public void setCourseDuration(String courseDuration) {
		this.courseDuration = courseDuration;
	}

	public int getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(int completionStatus) {
		this.completionStatus = completionStatus;
	}

	public Boolean getAllowReportDownload() {
		return allowReportDownload;
	}

	public void setAllowReportDownload(Boolean allowReportDownload) {
		this.allowReportDownload = allowReportDownload;
	}

}
