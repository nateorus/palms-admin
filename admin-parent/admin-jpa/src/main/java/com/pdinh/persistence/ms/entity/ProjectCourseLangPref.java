package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="project_course_lang_pref")
@NamedQueries({
	@NamedQuery(name="findByProjectAndCourse", query="SELECT pclp FROM ProjectCourseLangPref pclp WHERE pclp.course.course = :courseId AND pclp.project.projectId = :projectId")
})
public class ProjectCourseLangPref implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="project_course_lang_pref_id")
    private int projectCourseLangPrefId;

    //bi-directional many-to-one association to Project
    @ManyToOne
    @JoinColumn(name="project_id")
    private Project project;

    //bi-directional many-to-one association to Course
    @ManyToOne
    @JoinColumn(name="course_id")
    private Course course;

    //bi-directional many-to-one association to Language
    @ManyToOne
    @JoinColumn(name="language_id")
    private Language language;
    
    public ProjectCourseLangPref() {
    	
    }
    
    public ProjectCourseLangPref(ProjectCourseLangPref lp){
    	this.setCourse(lp.getCourse());
    	this.setLanguage(lp.getLanguage());
    	
    }

	public int getProjectCourseLangPrefId() {
		return projectCourseLangPrefId;
	}

	public void setProjectCourseLangPrefId(int projectCourseLangPrefId) {
		this.projectCourseLangPrefId = projectCourseLangPrefId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

    
}
