package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PasswordHistory
 *
 */
@Entity
@Table(name="password_history")

public class PasswordHistory implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "password_history_id")
	private int passwordHistoryId;
	
	@ManyToOne
	@JoinColumn(name = "password_policy_user_status_id")
	private PasswordPolicyUserStatus userStatus;
	
	@Column(name = "password_hash")
	private byte[] passwordHash;
	
	@Column(name = "date_created")
	private Timestamp dateCreated;
	
	@Column(name = "active")
	private boolean active;

	public PasswordHistory() {
		super();
	}

	public int getPasswordHistoryId() {
		return passwordHistoryId;
	}

	public void setPasswordHistoryId(int passwordHistoryId) {
		this.passwordHistoryId = passwordHistoryId;
	}

	public byte[] getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public PasswordPolicyUserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(PasswordPolicyUserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
   
}
