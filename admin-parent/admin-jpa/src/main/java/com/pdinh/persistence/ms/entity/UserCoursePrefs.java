package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.User;

/**
 * The persistent class for the project_user database table.
 * 
 */
@Entity
@Table(name="user_course_prefs")
@NamedQueries({
    @NamedQuery(name="getLanguagePrefByUserAndCourse", query="SELECT ucp.language FROM UserCoursePrefs ucp WHERE ucp.course.course = :courseId AND ucp.user.usersId = :userId"),
    @NamedQuery(name="getLanguagePrefByUserAndCourseAbbv", query="SELECT ucp FROM UserCoursePrefs ucp WHERE ucp.course.abbv = :courseAbbv AND ucp.user.usersId = :userId"),
    @NamedQuery(name="getLanguagePrefsByUserX", query="SELECT ucp.language FROM UserCoursePrefs ucp WHERE ucp.user.usersId = :userId"),
    @NamedQuery(name="getLanguagePrefsByUser", query="SELECT ucp from UserCoursePrefs ucp WHERE ucp.user.usersId = :userId")
    })
public class UserCoursePrefs implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="user_course_prefs_id")
    private int userCoursePrefsId;

    //bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    //bi-directional many-to-one association to Course
    @ManyToOne
    @JoinColumn(name="course_id")
    private Course course;

    //bi-directional many-to-one association to Language
    @ManyToOne
    @JoinColumn(name="language_id")
    private Language language;

    public UserCoursePrefs() {
    }

    public void setUserCoursePrefsId(int userCoursePrefsId) {
        this.userCoursePrefsId = userCoursePrefsId;
    }

    public int getUserCoursePrefsId() {
        return userCoursePrefsId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Language getLanguage() {
        return language;
    }

   
}