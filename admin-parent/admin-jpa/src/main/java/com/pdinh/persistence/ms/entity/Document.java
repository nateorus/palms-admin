package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The persistent class for the document database table, this is the 
 * base class for user and project documents
 * 
 */

@Entity
@Table(name="document")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="document_type", discriminatorType=DiscriminatorType.STRING)
@XmlRootElement(name="document")
@XmlAccessorType(XmlAccessType.FIELD)
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="document_id")
	@XmlAttribute
	private int documentId;

	private String description;
	
	@Column(name="include_in_integration_grid")
	private Boolean includeInIntegrationGrid;

	@ManyToOne(targetEntity=FileAttachment.class,cascade=CascadeType.ALL)
	@JoinColumn(name="file_attachment_id")
	private FileAttachment fileAttachment;
	
	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIncludeInIntegrationGrid() {
		return includeInIntegrationGrid;
	}

	public void setIncludeInIntegrationGrid(Boolean includeInIntegrationGrid) {
		this.includeInIntegrationGrid = includeInIntegrationGrid;
	}

	public FileAttachment getFileAttachment() {
		return fileAttachment;
	}

	public void setFileAttachment(FileAttachment fileAttachment) {
		this.fileAttachment = fileAttachment;
	}
	
	
}
