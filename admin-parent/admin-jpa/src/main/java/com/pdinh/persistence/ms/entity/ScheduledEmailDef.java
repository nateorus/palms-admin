package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the ScheduledEmailDef database table.
 * 
 */
@Entity
@EntityListeners(value = EntityAuditListener.class)
@NamedQueries({

		@NamedQuery(name = "findAllTemplates", query = "Select sed from ScheduledEmailDef sed where sed.isTemplate = 1", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts") }),
		@NamedQuery(name = "findAllTemplatesByType", query = "SELECT sed FROM ScheduledEmailDef sed WHERE sed.isTemplate = 1 AND sed.emailType=:emailType", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts") }),
		@NamedQuery(name = "findAllTemplatesAssessment", query = "SELECT sed FROM ScheduledEmailDef sed WHERE sed.isTemplate = 1 AND sed.emailType IN ('TLT','ABYD','MLLLVA')", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts") }),
		@NamedQuery(name = "findAllTemplatesByCompanyProductType", query = "SELECT sed FROM ScheduledEmailDef sed WHERE sed.isTemplate = 1 AND sed.emailType IN ( SELECT cp.productCode FROM CompanyProduct cp WHERE cp.company.companyId=:companyId)", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),

		@NamedQuery(name = "findClientLevelTemplates", query = "SELECT sed  FROM ScheduledEmailDef sed, ScheduledEmailCompany sec WHERE sed.id=sec.scheduledEmailDef.id AND sed.isTemplate = 0  AND sed.level = 'CLIENT'  AND sec.company.companyId=:companyId", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),

		@NamedQuery(name = "findSystemAndProductLevelTemplates", query = "SELECT sed FROM ScheduledEmailDef sed where sed.isTemplate=1 and ((sed.level = 'SYS'))", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),

		@NamedQuery(name = "findAllTemplatesByProject", query = "SELECT sed FROM ScheduledEmailDef sed, ScheduledEmailProject sep WHERE sed.id=sep.scheduledEmailDef.id  AND sed.isTemplate = 0 	AND sed.level = 'PROJ' 	AND sep.project.projectId=:projectId", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),

		@NamedQuery(name = "findAllSystemTemplates", query = "SELECT sed FROM ScheduledEmailDef sed WHERE  sed.isTemplate = 1  AND sed.level = 'SYS'", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),
		@NamedQuery(name = "findAllGlobalTemplates", query = "SELECT sed FROM ScheduledEmailDef sed WHERE  sed.isTemplate = 1  AND sed.level = 'GLOBAL'", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),
		@NamedQuery(name = "findTemplateById", query = "SELECT sed FROM ScheduledEmailDef sed WHERE sed.id=:id", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") }),
		@NamedQuery(name = "findSysAndClientTemplatesByConstant", query = "SELECT distinct sed FROM ScheduledEmailDef sed, ScheduledEmailCompany sec WHERE sed.constant = :constant and (sed.level='SYS' or (sec.company.companyId = :companyId and sed.id=sec.scheduledEmailDef.id))", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "sed.scheduledEmailTexts.language") })

})
public class ScheduledEmailDef implements EntityAuditLogger, Serializable {
	private static final long serialVersionUID = 1L;

	public static final String SYS = "SYS";
	public static final String GLOBAL = "GLOBAL";
	public static final String CLIENT = "CLIENT";
	public static final String PROJ = "PROJ";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "active")
	private boolean active;
	@Column(name = "constant")
	private String constant;
	@Column(name = "emailType")
	private String emailType;
	@Column(name = "isTemplate")
	private int isTemplate;
	@Column(name = "title")
	private String title;
	@Column(name = "level")
	private String level;
	@Column(name = "fromKeyCode")
	private String fromKeyCode;
	@Column(name = "customFrom")
	private String customFrom;
	@Column(name = "customReplyTo")
	private String customReplyTo;
	@Column(name = "constantStyleLabel")
	private String constantStyleLabel;
	@Column(name = "emailTypeProdLabel")
	private String emailTypeProdLabel;

	// bi-directional many-to-many association to ScheduledEmailCompany
	@OneToMany(targetEntity = com.pdinh.persistence.ms.entity.ScheduledEmailCompany.class, mappedBy = "scheduledEmailDef", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST }, orphanRemoval = true)
	private List<ScheduledEmailCompany> scheduledEmailCompanies;
/*
	// bi-directional many-to-many association to ScheduledEmailProject
	@OneToMany(targetEntity = com.pdinh.persistence.ms.entity.ScheduledEmailProject.class, mappedBy = "scheduledEmailDef")
	private List<ScheduledEmailProject> scheduledEmailProjects;
*/
	// bi-directional many-to-one association to ScheduledEmailText
	@OneToMany(mappedBy = "scheduledEmailDef", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST }, orphanRemoval = true)
	private List<ScheduledEmailText> scheduledEmailTexts = new ArrayList<ScheduledEmailText>();

	// bi-directional many-to-one association to ScheduledEmailLog
	//@OneToMany(mappedBy = "scheduledEmailDef", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
	//private List<ScheduledEmailLog> scheduledEmailLogs;

	// bi-directional many-to-one association to ScheduledEmailStatus
	//This does not belong here
	//@OneToMany(mappedBy = "scheduledEmailDef", cascade = { CascadeType.ALL })
	//private List<ScheduledEmailStatus> scheduledEmailStatuses;

	public ScheduledEmailDef() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConstant() {
		return this.constant;
	}

	public void setConstant(String constant) {
		this.constant = constant;
	}

	public String getEmailType() {
		return this.emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public int getIsTemplate() {
		return this.isTemplate;
	}

	public void setIsTemplate(int isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ScheduledEmailText> getScheduledEmailTexts() {
		return this.scheduledEmailTexts;
	}

	public void setScheduledEmailTexts(List<ScheduledEmailText> scheduledEmailTexts) {
		this.scheduledEmailTexts = scheduledEmailTexts;
	}
/*
	public void setScheduledEmailLogs(List<ScheduledEmailLog> scheduledEmailLogs) {
		this.scheduledEmailLogs = scheduledEmailLogs;
	}

	public List<ScheduledEmailLog> getScheduledEmailLogs() {
		return scheduledEmailLogs;
	}
*/
	/*
	public void setScheduledEmailStatuses(List<ScheduledEmailStatus> scheduledEmailStatuses) {
		this.scheduledEmailStatuses = scheduledEmailStatuses;
	}

	public List<ScheduledEmailStatus> getScheduledEmailStatuses() {
		return scheduledEmailStatuses;
	}
	*/

	public void setScheduledEmailCompanies(List<ScheduledEmailCompany> scheduledEmailCompanies) {
		this.scheduledEmailCompanies = scheduledEmailCompanies;
	}

	public List<ScheduledEmailCompany> getScheduledEmailCompanies() {
		return scheduledEmailCompanies;
	}
/*
	public void setScheduledEmailProjects(List<ScheduledEmailProject> scheduledEmailProjects) {
		this.scheduledEmailProjects = scheduledEmailProjects;
	}

	public List<ScheduledEmailProject> getScheduledEmailProjects() {
		return scheduledEmailProjects;
	}
*/
	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on Email Template ({})", new Object[] { user,
						EntityAuditLogger.OP_CREATE_EMAIL_TEMPLATE, getTitle() });
			} else {
				auditLog.info("{} performed {} operation on Email Template ({})", new Object[] { user, operation,
						getTitle() });
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on Email Template {}({})", new Object[] { user,
						EntityAuditLogger.OP_UPDATE_EMAIL_TEMPLATE, getId(), getTitle() });
			} else {
				auditLog.info("{} performed {} operation on Email Template {}({})", new Object[] { user, operation,
						getId(), getTitle() });
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:

			auditLog.info("{} performed {} operation on Email Template: {}({})", new Object[] { user,
					EntityAuditLogger.OP_DELETE_EMAIL_TEMPLATE, getId(), getTitle() });
			break;
		}

	}

	public String getFromKeyCode() {
		return fromKeyCode;
	}

	public void setFromKeyCode(String fromKeyCode) {
		this.fromKeyCode = fromKeyCode;
	}

	public String getCustomFrom() {
		return customFrom;
	}

	public void setCustomFrom(String customFrom) {
		this.customFrom = customFrom;
	}

	public String getCustomReplyTo() {
		return customReplyTo;
	}

	public void setCustomReplyTo(String customReplyTo) {
		this.customReplyTo = customReplyTo;
	}

	public String getConstantStyleLabel() {
		return constantStyleLabel;
	}

	public void setConstantStyleLabel(String constantStyleLabel) {
		this.constantStyleLabel = constantStyleLabel;
	}

	public String getEmailTypeProdLabel() {
		return emailTypeProdLabel;
	}

	public void setEmailTypeProdLabel(String emailTypeProdLabel) {
		this.emailTypeProdLabel = emailTypeProdLabel;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}