package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the email_rule_schedule_status database table.
 * 
 */
@Entity
@Table(name="email_rule_schedule_status")
@NamedQueries({@NamedQuery(name = "findStatusByCode", query = "Select s from EmailRuleScheduleStatus s "
		+ "where s.code = :code")})
public class EmailRuleScheduleStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String NOT_PROCESSED = "NOT_PROCESSED";
	public static final String CREATED_WAITING = "CREATED_WAITING";
	public static final String WAITING_EXECUTE = "WAITING_EXECUTE";
	public static final String DETERMINE_USERS = "DETERMINE_USERS";
	public static final String BUILDING_TEMPLATE = "BUILDING_TEMPLATE";
	public static final String SENDING_EMAILS = "SENDING_EMAILS";
	public static final String EMAIL_SENT = "EMAIL_SENT";
	public static final String NO_ERRORS = "NO_ERRORS";
	public static final String ERRORS_ON_COMPLETE = "ERRORS_ON_COMPLETE";
	public static final String NO_MATCH = "NO_MATCH";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String code;

	private String name;

    public EmailRuleScheduleStatus() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}