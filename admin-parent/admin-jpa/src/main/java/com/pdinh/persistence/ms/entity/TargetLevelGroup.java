/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TargetLevelGroup
 *
 */
@Entity
@Table(name="target_level_group")

public class TargetLevelGroup implements Serializable
{
	//
	// Static variables
	//
	private static final long serialVersionUID = 1L;
	
	//
	// Static methods.
	//

	//
	// Instance variables.
	//
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="target_level_group_id")
	private int targetLevelGroupId;
	
	private String name;
	
	@OneToMany(mappedBy="targetLevelGroup")
	@OrderBy("sequenceOrder ASC")
	private List<TargetLevelGroupType> targetLevelGroupTypes;
	
	//
	// Constructors.
	//

	public TargetLevelGroup() {
		super();
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public int getTargetLevelGroupId()
	{
		return this.targetLevelGroupId;
	}
		
	public void setTargetLevelGroupId(int value)
	{
		this.targetLevelGroupId = value;
	}

	/*****************************************************************************************/
	public String getName()
	{
		return this.name;
	}
		
	public void setName(String value)
	{
		this.name = value;
	}

	/*****************************************************************************************/
	public List<TargetLevelGroupType> getTargetLevelGroupTypes()
	{
		return this.targetLevelGroupTypes;
	}
		
	public void setTargetLevelGroupTypes(List<TargetLevelGroupType> value)
	{
		this.targetLevelGroupTypes = value;
	}
  
}
