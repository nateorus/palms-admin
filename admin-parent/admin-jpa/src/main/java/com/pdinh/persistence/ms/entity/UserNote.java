package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.pdinh.persistence.ms.entity.User;

/**
 * The persistent class for the project_user database table.
 * 
 */
@Entity
@Table(name="user_note")
@NamedQueries({
    @NamedQuery(name="getNotesByUser", query="SELECT un from UserNote un WHERE un.user.usersId = :userId ORDER BY un.dateAdded")
    })
public class UserNote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="user_note_id")
    private int userNoteId;

    //bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name="users_id")
    private User user;
    
    private String title;
    
    private String note;

    @Column(name="date_added")
    private Timestamp dateAdded;

    @Column(name="ext_admin_id")
    private String extAdminId;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }

	public void setUserNoteId(int userNoteId) {
		this.userNoteId = userNoteId;
	}

	public int getUserNoteId() {
		return userNoteId;
	}

	public void setExtAdminId(String extAdminId) {
		this.extAdminId = extAdminId;
	}

	public String getExtAdminId() {
		return extAdminId;
	}
}

