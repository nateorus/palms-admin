package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

/**
 * The persistent class for the course_group_template database table.
 * 
 */
@Entity
@Table(name = "course_group_template")
@NamedQueries({ @NamedQuery(name = "findAllCourseGroupTemplateByProjectTypeId", query = "SELECT cgt FROM CourseGroupTemplate cgt JOIN cgt.courseFlowType cft JOIN cgt.projectType pt WHERE pt.projectTypeId = :projectTypeId ORDER BY cft.sequence ASC, cgt.sequence ASC", hints = {
		@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
		@QueryHint(name = "eclipselink.batch", value = "cgt.courseFlowType"),
		@QueryHint(name = "eclipselink.batch", value = "cgt.course") }),
		@NamedQuery(name="findAllCourseGroupTemplatesOrdered", query="SELECT cgt FROM CourseGroupTemplate cgt ORDER BY cgt.courseFlowType.sequence ASC, cgt.sequence ASC", hints = {
		@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
		@QueryHint(name = "eclipselink.batch", value = "cgt.courseFlowType"),
		@QueryHint(name = "eclipselink.batch", value = "cgt.course") })})
public class CourseGroupTemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_group_template_id")
	private int id;

	@Column(name = "enabled_flag")
	private Boolean enabled;

	@Column(name = "selected_flag")
	private Boolean selected;

	@Column(name = "sequence_order")
	private int sequence;

	// bi-directional many-to-one association to ProjectType
	@ManyToOne
	@JoinColumn(name = "project_type_id")
	private ProjectType projectType;

	// bi-directional many-to-one association to Course
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course;

	// bi-directional many-to-one association to CourseFlow
	@ManyToOne
	@JoinColumn(name = "course_flow_type_id")
	private CourseFlowType courseFlowType;

	@Column(name = "selection_type")
	private String selectionType;

	@Column(name = "object_name")
	private String objectName;
	
	@Column(name = "allow_report_download")
	private Boolean allowReportDownload;
	
	@Column(name = "download_report_type_id")
	private int downloadReportTypeId;
	
	public static final String CHECKBOX = "CHECKBOX";
	public static final String RADIO = "RADIO";
	public static final String VIAEDGECHOICE = "viaEdgeChoice";

	public CourseGroupTemplate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getSelected() {
		return this.selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public ProjectType getProjectType() {
		return this.projectType;
	}

	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	public CourseFlowType getCourseFlowType() {
		return this.courseFlowType;
	}

	public void setCourseFlowType(CourseFlowType courseFlowType) {
		this.courseFlowType = courseFlowType;
	}

	public String getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(String selectionType) {
		this.selectionType = selectionType;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public Boolean getAllowReportDownload() {
		return allowReportDownload;
	}

	public void setAllowReportDownload(Boolean allowReportDownload) {
		this.allowReportDownload = allowReportDownload;
	}

	public int getDownloadReportTypeId() {
		return downloadReportTypeId;
	}

	public void setDownloadReportTypeId(int downloadReportTypeId) {
		this.downloadReportTypeId = downloadReportTypeId;
	}

}