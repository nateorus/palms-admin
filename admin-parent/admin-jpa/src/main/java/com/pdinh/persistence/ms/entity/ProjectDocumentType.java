package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for list of possible project document types
 * 
 */
@Entity
@Table(name="project_document_type")
public class ProjectDocumentType implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="project_document_type_id")
	private int projectDocumentTypeId;
	
	private String name;

	public int getProjectDocumentTypeId() {
		return projectDocumentTypeId;
	}

	public void setProjectDocumentTypeId(int projectDocumentTypeId) {
		this.projectDocumentTypeId = projectDocumentTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
