package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the project database table.
 *
 */
@Entity
@EntityListeners(value = EntityAuditListener.class)
@Table(name = "project_view")
@NamedQueries({

		@NamedQuery(name = "getProjectByName", query = "Select p from Project p where p.name = :name"),

		@NamedQuery(name = "getProjectBatchUsers", query = "Select p from Project p where p.projectId = :id", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectUsers.user") }),

		@NamedQuery(name = "getProjectBatchUsersPositions", query = "Select p from Project p where p.projectId = :id", hints = {
				@QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE),
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectCourses.course"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectUsers.user.positions.course") }),

		@NamedQuery(name = "findProjectsByCompanyId", query = "Select p from Project p where p.company.companyId = :id order by p.name", hints = {
				@QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE),
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.admin"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectDocuments"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectReportConfigs")
				}),

		@NamedQuery(name = "findAuthorizedProjectsByCompanyId", query = "Select p from Project p where p.company.companyId = :id and p.projectId IN :projectIds order by p.name", hints = {
				@QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE),
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.admin") }),

		@NamedQuery(name = "findDistinctProjectTypes", query = "Select distinct p.projectTypeOld from Project p where p.projectTypeOld is not null and p.projectTypeOld <> '' and p.company.companyId = :id", hints = {}),

		@NamedQuery(name = "findProjectsByUserId", query = "SELECT p FROM Project p WHERE p.projectId IN (SELECT pu.project.projectId FROM ProjectUser pu WHERE pu.user.usersId = :id)", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findAuthorizedProjects", query = "Select p from Project p where p.projectId IN :projectIds order by p.company.coName, p.name", hints = {
				@QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE),
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectDocuments")}),
		@NamedQuery(name = "findAuthorizedProjectsInCompanies", query = "Select p from Project p where ((p.projectId IN :projectIds) or (p.company.companyId IN :companyIds)) and p.active = 1 order by p.company.coName, p.name", hints = {
				@QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE),
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.admin"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectDocuments")}),
		@NamedQuery(name = "findAuthorizedProjectsInCompany", query = "Select p from Project p where ((p.projectId IN :projectIds) AND (p.company.companyId = :companyId)) and p.active = 1 order by p.company.coName, p.name", hints = {
				@QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE),
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "p.admin"),
				@QueryHint(name = "eclipselink.batch", value = "p.projectDocuments") }),

		@NamedQuery(name = "findProjectsClonedFromProject", query = "Select p from Project p where p.clonedFromProjectId = :parentProjectId and p.active = 1"),
		@NamedQuery(name = "findTinCanProjectsByCompanyId", query = "SELECT distinct p FROM Project p, CourseGroupTemplate cgt WHERE p.company.companyId = :companyId AND cgt.projectType.projectTypeId = p.projectType.projectTypeId AND cgt.course.course IN (SELECT c.course from Course c where c.tinCanUri IS NOT NULL) ORDER BY p.name ASC", hints = { @QueryHint(name = "eclipselink.batch.type", value = "EXISTS") })

})
public class Project implements EntityAuditLogger, Serializable {
	private static final long serialVersionUID = 1L;

	// // TODO This is redundant to The longer names in ProjectType?
	// Converted type references to reference ProjectType
	// public static final int TLT_WITH_COGS = 1;
	// public static final int TLT_WITHOUT_COGS = 2;
	// public static final int ABYD = 3;
	// public static final int ASSESSMENT_TESTING = 4;
	// public static final int GL_GPS = 12;
	// public static final int COACHING = 13;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_id")
	private int projectId;

	private int active;

	@ManyToOne
	@JoinColumn(name = "admin_id")
	private User admin;

	@Column(name = "date_created")
	private Timestamp dateCreated;

	private String name;

	// @Column(name = "from_email")
	// private String fromEmail;

	@Column(name = "project_code")
	private String projectCode;

	@Column(name = "project_type")
	private String projectTypeOld;

	@Column(name = "ext_admin_id")
	private String extAdminId;

	@Column(name = "report_content_manifest_ref")
	private String reportContentManifestRef;

	@ManyToOne
	@JoinColumn(name = "target_level_type_id")
	@JoinFetch(JoinFetchType.OUTER)
	private TargetLevelType targetLevelType;

	private String rowguid;

	@Column(name = "users_count", insertable = false, updatable = false)
	private int usersCount;

	// @Column(name = "participants_added")
	// private boolean participantsAdded;

	// bi-directional many-to-one association to ProjectType
	@ManyToOne
	@JoinColumn(name = "project_type_id")
	@JoinFetch(JoinFetchType.OUTER)
	private ProjectType projectType;

	// bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name = "company_id")
	@JoinFetch(JoinFetchType.OUTER)
	private Company company;

	// bi-directional many-to-many association to ScheduledEmailProject
	@OneToMany(mappedBy = "project")
	private List<ScheduledEmailProject> scheduledEmailProjects;

	// bi-directional many-to-one association to ProjectCourse
	@OneToMany(mappedBy = "project", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@OrderBy("sequence ASC")
	private List<ProjectCourse> projectCourses;

	// bi-directional many-to-one association to ProjectUser
	// @OneToMany(mappedBy = "project", cascade = { CascadeType.PERSIST,
	// CascadeType.MERGE })
	@OneToMany(mappedBy = "project")
	private List<ProjectUser> projectUsers;

	// bi-directional many-to-one association to ProjectDocument
	@OneToMany(mappedBy = "project", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<ProjectDocument> projectDocuments = new ArrayList<ProjectDocument>();

	@ManyToOne
	@JoinColumn(name = "office_location_id")
	@JoinFetch(JoinFetchType.OUTER)
	private OfficeLocation officeLocation;

	@Column(name = "project_due_date")
	private Timestamp dueDate;

	@Column(name = "allow_report_download")
	private Boolean allowReportDownload;

	// TODO: AV: Map it to the ConfigurationType Entity
	@Column(name = "configuration_type_id")
	private int configurationTypeId;

	@Column(name = "scoring_method_id")
	private int scoringMethodId;

	@Column(name = "assigned_pm_id")
	private int assignedPmId;

	@Column(name = "internal_scheduler_id")
	private int internalSchedulerId;

	@Column(name = "is_billable")
	private Boolean billable;

	@Column(name = "non_billable_type_id")
	private Integer nonBillableTypeId;

	// This is actually a recursive FK into Project, but we don't make the
	// association in JPA because we don't need it.
	@Column(name = "cloned_from_project_id")
	private Integer clonedFromProjectId;

	@Column(name = "include_client_logonid_password")
	private boolean includeClientLogonIdPassword;

	// bi-directional many-to-one association to ProjectCourseLangPref
	@OneToMany(mappedBy = "project", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	@XmlTransient
	private List<ProjectCourseLangPref> projectCourseLangPref;

	@OneToMany(mappedBy = "project", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<ProjectReport> projectReportConfigs;

	public Project() {
	}

	public Project(Project p) {
		this.setActive(p.getActive());
		this.setAdmin(p.getAdmin());
		this.setAllowReportDownload(p.getAllowReportDownload());
		this.setCompany(p.getCompany());
		this.setConfigurationTypeId(p.getConfigurationTypeId());
		this.setDueDate(p.getDueDate());
		this.setExtAdminId(p.getExtAdminId());
		this.setName(p.getName());
		this.setOfficeLocation(p.getOfficeLocation());
		this.setProjectCode(p.getProjectCode());

		List<ProjectCourse> projectCourses = new ArrayList<ProjectCourse>();
		for (ProjectCourse pc : p.getProjectCourses()) {
			ProjectCourse newPc = new ProjectCourse(pc);
			newPc.setProject(this);
			projectCourses.add(newPc);
		}
		this.setProjectCourses(projectCourses);

		List<ProjectCourseLangPref> langPrefs = new ArrayList<ProjectCourseLangPref>();
		for (ProjectCourseLangPref langpref : p.getProjectCourseLangPref()) {
			ProjectCourseLangPref newLp = new ProjectCourseLangPref(langpref);
			newLp.setProject(this);
			langPrefs.add(newLp);
		}
		this.setProjectCourseLangPref(langPrefs);

		List<ProjectReport> prjRpts = new ArrayList<ProjectReport>();
		for (ProjectReport pr : p.getProjectReportConfigs()){
			ProjectReport prjRpt = new ProjectReport(pr);
			prjRpt.setProject(this);
			prjRpts.add(prjRpt);
		}
		this.setProjectReportConfigs(prjRpts);

		this.setProjectType(p.getProjectType());
		this.setProjectTypeOld(p.getProjectTypeOld());
		this.setReportContentManifestRef(this.getReportContentManifestRef());
		this.setScoringMethodId(p.getScoringMethodId());
		this.setTargetLevelType(p.getTargetLevelType());
		this.setAssignedPmId(p.getAssignedPmId());
		this.setInternalSchedulerId(p.getInternalSchedulerId());
		this.setBillable(p.getBillable());
		this.setNonBillableTypeId(p.getNonBillableTypeId());
		this.setIncludeClientLogonIdPassword(p.isIncludeClientLogonIdPassword());
	}

	public int getProjectId() {
		return this.projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectCode() {
		return this.projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectTypeOld() {
		return this.projectTypeOld;
	}

	public void setProjectTypeOld(String projectTypeOld) {
		this.projectTypeOld = projectTypeOld;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<ProjectUser> getProjectUsers() {
		return this.projectUsers;
	}

	public void setProjectUsers(List<ProjectUser> projectUsers) {
		this.projectUsers = projectUsers;
	}

	public void setUsersCount(int usersCount) {
		this.usersCount = usersCount;
	}

	public int getUsersCount() {
		return usersCount;
	}

	/*
		public boolean getParticipantsAdded() {
			return this.participantsAdded;
		}

		public void setParticipantsAdded(boolean participantsAdded) {
			this.participantsAdded = participantsAdded;
		}
	*/
	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	public ProjectType getProjectType() {
		return projectType;
	}

	public List<ProjectCourse> getProjectCourses() {
		return projectCourses;
	}

	public void setProjectCourses(List<ProjectCourse> projectCourses) {
		this.projectCourses = projectCourses;
	}

	public void setScheduledEmailProjects(List<ScheduledEmailProject> scheduledEmailProjects) {
		this.scheduledEmailProjects = scheduledEmailProjects;
	}

	public List<ScheduledEmailProject> getScheduledEmailProjects() {
		return scheduledEmailProjects;
	}

	public List<ProjectDocument> getProjectDocuments() {
		return projectDocuments;
	}

	public void setProjectDocuments(List<ProjectDocument> projectDocuments) {
		this.projectDocuments = projectDocuments;
	}

	public void setExtAdminId(String extAdminId) {
		this.extAdminId = extAdminId;
	}

	public String getExtAdminId() {
		return extAdminId;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public User getAdmin() {
		return admin;
	}

	public TargetLevelType getTargetLevelType() {
		return targetLevelType;
	}

	public void setTargetLevelType(TargetLevelType targetLevelType) {
		this.targetLevelType = targetLevelType;
	}

	/*
		public String getFromEmail() {
			return fromEmail;
		}

		public void setFromEmail(String fromEmail) {
			this.fromEmail = fromEmail;
		}
	*/
	public String getReportContentManifestRef() {
		return reportContentManifestRef;
	}

	public void setReportContentManifestRef(String reportContentManifestRef) {
		this.reportContentManifestRef = reportContentManifestRef;
	}

	public OfficeLocation getOfficeLocation() {
		return officeLocation;
	}

	public void setOfficeLocation(OfficeLocation officeLocation) {
		this.officeLocation = officeLocation;
	}

	public Timestamp getDueDate() {
		return dueDate;
	}

	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		String officeLocation = "";
		if (!(getOfficeLocation() == null)) {
			officeLocation = getOfficeLocation().getLocation();
		}
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on project: ({}), {}, {}, {}", new Object[] { user,
						EntityAuditLogger.OP_CREATE_PROJECT, getName(), getCompany().getCoName(),
						getProjectType().getName(), officeLocation });
			} else {
				auditLog.info("{} performed {} operation on project: ({}), {}, {}, {}", new Object[] { user, operation,
						getName(), getCompany().getCoName(), getProjectType().getName(), officeLocation });
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on project: {}({}), {}, {}, {}", new Object[] { user,
						EntityAuditLogger.OP_UPDATE_PROJECT, getProjectId(), getName(), getCompany().getCoName(),
						getProjectType().getName(), officeLocation });
			} else {
				auditLog.info("{} performed {} operation on project: {}({}), {}, {}, {}", new Object[] { user,
						operation, getProjectId(), getName(), getCompany().getCoName(), getProjectType().getName(),
						officeLocation });
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:
			// This should never happen
			auditLog.info("{} performed DELETE_PROJECT operation on PROJECT: {}({}), {}, {}, {}", new Object[] { user,
					getProjectId(), getName(), getCompany().getCoName(), getProjectType().getName(), officeLocation });
			break;
		}

	}

	public Boolean getAllowReportDownload() {
		return allowReportDownload;
	}

	public void setAllowReportDownload(Boolean allowReportDownload) {
		this.allowReportDownload = allowReportDownload;
	}

	public int getConfigurationTypeId() {
		return configurationTypeId;
	}

	public void setConfigurationTypeId(int configurationTypeId) {
		this.configurationTypeId = configurationTypeId;
	}

	public int getScoringMethodId() {
		return scoringMethodId;
	}

	public void setScoringMethodId(int scoringMethodId) {
		this.scoringMethodId = scoringMethodId;
	}

	public int getAssignedPmId() {
		return assignedPmId;
	}

	public void setAssignedPmId(int assignedPmId) {
		this.assignedPmId = assignedPmId;
	}

	public int getInternalSchedulerId() {
		return internalSchedulerId;
	}

	public void setInternalSchedulerId(int internalSchedulerId) {
		this.internalSchedulerId = internalSchedulerId;
	}

	public List<ProjectCourseLangPref> getProjectCourseLangPref() {
		return projectCourseLangPref;
	}

	public void setProjectCourseLangPref(List<ProjectCourseLangPref> projectCourseLangPref) {
		this.projectCourseLangPref = projectCourseLangPref;
	}

	public Boolean getBillable() {
		return billable;
	}

	public void setBillable(Boolean billable) {
		this.billable = billable;
	}

	public Integer getNonBillableTypeId() {
		return nonBillableTypeId;
	}

	public void setNonBillableTypeId(Integer nonBillableTypeId) {
		this.nonBillableTypeId = nonBillableTypeId;
	}

	public Integer getClonedFromProjectId() {
		return clonedFromProjectId;
	}

	public void setClonedFromProjectId(Integer clonedFromProjectId) {
		this.clonedFromProjectId = clonedFromProjectId;
	}

	public List<ProjectReport> getProjectReportConfigs() {
		return projectReportConfigs;
	}

	public void setProjectReportConfigs(List<ProjectReport> projectReportConfigs) {
		this.projectReportConfigs = projectReportConfigs;
	}

	public boolean isIncludeClientLogonIdPassword() {
		return includeClientLogonIdPassword;
	}

	public void setIncludeClientLogonIdPassword(boolean includeClientLogonIdPassword) {
		this.includeClientLogonIdPassword = includeClientLogonIdPassword;
	}

}