package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Cacheable(value = false)
@Table(name = "project_report")
@NamedQueries({
    @NamedQuery(
        name="getTemplatesForProjectType",
        query="SELECT pr FROM ProjectReport pr WHERE pr.projectTypeId = :projectTypeId and pr.template = true"),
    
    @NamedQuery(name = "getAllProjectReportTemplates", query = "SELECT pr FROM ProjectReport pr WHERE "
    		+ "pr.template = 'true'")
 })
public class ProjectReport implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static final int CONTENT_RULE_ANY = 1;
	public static final int CONTENT_RULE_ALL = 2;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="project_report_id")
	private int id;
	
	
	@Column(name = "project_type_id")
	private int projectTypeId;
	
	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;
	
	@ManyToOne
	@JoinColumn(name = "report_type_id")
	private ReportType reportType;
	
	@Column(name = "allow_autogenerate")
	private boolean allowAutogenerate;
	
	@Column(name = "allow_user_download")
	private boolean allowUserDownload;
	
	@Column(name = "allow_user_download_enabled")
	private boolean allowUserDownloadEnabled;
	
	@Column(name = "allow_autogenerate_enabled")
	private boolean allowAutogenerateEnabled;
	
	@Column(name = "allow_autogenerate_visible")
	private boolean allowAutogenerateVisible;
	
	@Column(name = "allow_user_download_visible")
	private boolean allowUserDownloadVisible;
	
	@Column(name = "selected")
	private boolean selected;
	
	@Column(name = "sequence_order")
	private int sequenceOrder;
	
	@Column(name = "selection_type")
	private String selectionType;
	
	@Column(name = "object_name")
	private String objectName;
	
	@Column(name = "template")
	private boolean template;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "content_rule")
	private int contentRule;
	
	@OneToMany(mappedBy = "projectReport", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<ProjectReportContent> projectReportContent;
	
	public ProjectReport(){
		
	}
	
	public ProjectReport(ProjectReport projectReport){
		this.setAllowAutogenerate(projectReport.isAllowAutogenerate());
		this.setAllowAutogenerateEnabled(projectReport.isAllowAutogenerateEnabled());
		this.setAllowUserDownload(projectReport.isAllowUserDownload());
		this.setAllowUserDownloadEnabled(projectReport.isAllowUserDownloadEnabled());
		this.setAllowAutogenerateVisible(projectReport.isAllowAutogenerateVisible());
		this.setAllowUserDownloadVisible(projectReport.isAllowUserDownloadVisible());
		this.setCode(projectReport.getCode());
		this.setContentRule(projectReport.getContentRule());
		this.setObjectName(projectReport.getObjectName());
		List<ProjectReportContent> newContent = new ArrayList<ProjectReportContent>();
		for (ProjectReportContent c : projectReport.getProjectReportContent()){
			ProjectReportContent prc = new ProjectReportContent();
			prc.setProjectReport(this);
			prc.setCourse(c.getCourse());
			prc.setDisplayDownloadLink(c.isDisplayDownloadLink());
			newContent.add(prc);
		}
		this.setProjectReportContent(newContent);
		this.setProjectTypeId(projectReport.getProjectTypeId());
		this.setReportType(projectReport.getReportType());
		this.setSelected(projectReport.isSelected());
		this.setSequenceOrder(projectReport.getSequenceOrder());
		this.setTemplate(false);
		this.setSelectionType(projectReport.getSelectionType());
		
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

	public boolean isAllowAutogenerate() {
		return allowAutogenerate;
	}

	public void setAllowAutogenerate(boolean allowAutogenerate) {
		this.allowAutogenerate = allowAutogenerate;
	}

	public boolean isAllowUserDownload() {
		return allowUserDownload;
	}

	public void setAllowUserDownload(boolean allowUserDownload) {
		this.allowUserDownload = allowUserDownload;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(String selectionType) {
		this.selectionType = selectionType;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public boolean isTemplate() {
		return template;
	}

	public void setTemplate(boolean template) {
		this.template = template;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getContentRule() {
		return contentRule;
	}

	public void setContentRule(int contentRule) {
		this.contentRule = contentRule;
	}

	public List<ProjectReportContent> getProjectReportContent() {
		return projectReportContent;
	}

	public void setProjectReportContent(
			List<ProjectReportContent> projectReportContent) {
		this.projectReportContent = projectReportContent;
	}

	public int getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public boolean isAllowUserDownloadEnabled() {
		return allowUserDownloadEnabled;
	}

	public void setAllowUserDownloadEnabled(boolean allowUserDownloadEnabled) {
		this.allowUserDownloadEnabled = allowUserDownloadEnabled;
	}

	public boolean isAllowAutogenerateEnabled() {
		return allowAutogenerateEnabled;
	}

	public void setAllowAutogenerateEnabled(boolean allowAutogenerateEnabled) {
		this.allowAutogenerateEnabled = allowAutogenerateEnabled;
	}

	public int getSequenceOrder() {
		return sequenceOrder;
	}

	public void setSequenceOrder(int sequenceOrder) {
		this.sequenceOrder = sequenceOrder;
	}

	public boolean isAllowAutogenerateVisible() {
		return allowAutogenerateVisible;
	}

	public void setAllowAutogenerateVisible(boolean allowAutogenerateVisible) {
		this.allowAutogenerateVisible = allowAutogenerateVisible;
	}

	public boolean isAllowUserDownloadVisible() {
		return allowUserDownloadVisible;
	}

	public void setAllowUserDownloadVisible(boolean allowUserDownloadVisible) {
		this.allowUserDownloadVisible = allowUserDownloadVisible;
	}
	
}
