package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "configuration_group")
public class ConfigurationGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	// Public key names
	public static String UNASSIGNED = "UNASSIGNED";
	public static String ABYD_MLL = "ABYD_MLL";
	public static String ABYD_BUL = "ABYD_BUL";
	public static String GLGPS_MLL = "GLGPS_MLL";
	public static String ERICSSON_MLL = "ERICSSON_MLL";
	public static String ERICSSON_BUL = "ERICSSON_BUL";
	public static String KFALR = "KFALR";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "configuration_group_id")
	private int configurationGroupId;

	@Column(name = "configuration_group_code")
	private String configurationGroupCode;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "configurationGroup")
	private List<ConfigurationType> configurationTypes;

	public ConfigurationGroup() {
	}

	public int getConfigurationGroupId() {
		return configurationGroupId;
	}

	public void setConfigurationGroupId(int value) {
		this.configurationGroupId = value;
	}

	public String getConfigurationGroupCode() {
		return this.configurationGroupCode;
	}

	public void setConfigurationGroupCode(String value) {
		this.configurationGroupCode = value;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String value) {
		this.description = value;
	}

	public List<ConfigurationType> getConfigurationTypes() {
		return this.configurationTypes;
	}

	public void setConfigurationTypes(List<ConfigurationType> value) {
		this.configurationTypes = value;
	}

}
