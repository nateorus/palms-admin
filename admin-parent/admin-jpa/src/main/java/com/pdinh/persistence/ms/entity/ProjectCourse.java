package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

/**
 * The persistent class for the project_course database table.
 * 
 */

// commenting out this hint prevent a JPA bug manifesting itself
// - select distinct on text column fails in sql server
// @QueryHint(name="eclipselink.batch", value="pc.courseFlowType"),
@Entity
@Table(name = "project_content")
// @formatter:off
@NamedQueries({
    @NamedQuery(
        name="getProjectCourseByProjectId",
        query="SELECT pc FROM ProjectCourse pc JOIN pc.courseFlowType cft WHERE pc.project.projectId = :projectId ORDER BY cft.sequence ASC, pc.sequence ASC",
        hints={@QueryHint(name="eclipselink.batch.type", value="EXISTS"),
        	   @QueryHint(name="eclipselink.batch", value="pc.courseFlowType"),
               @QueryHint(name="eclipselink.batch", value="pc.course")})
})
@NamedNativeQueries({
    @NamedNativeQuery(
    	name="getInstrumentsForIntake",
    	query="SELECT cs.title, cs.course, cs.abbv, cft.name, "
    		+ "(SELECT count(*) FROM course_language cl WHERE cl.course_id = cs.course AND cl.lang_id = ?1) "
    		+ "FROM project_content pc, course cs, course_flow_type cft "
    		+ "WHERE pc.content_id = cs.course "
    		+ "AND cs.abbv in ('finex', 'chq') "
    		+ "AND pc.course_flow_type_id = cft.course_flow_type_id "
    		+ "AND pc.project_id = ?2"
    		)
})
// @formatter:on
public class ProjectCourse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_content_id")
	private int projectCourseId;

	@Column(name = "enabled_flag")
	private Boolean enabled;

	@Column(name = "selected_flag")
	private Boolean selected;

	@Column(name = "sequence_order")
	private int sequence;

	// bi-directional many-to-one association to Course
	@ManyToOne
	@JoinColumn(name = "content_id")
	private Course course;

	// bi-directional many-to-one association to CourseFlowType
	@ManyToOne
	@JoinColumn(name = "course_flow_type_id")
	private CourseFlowType courseFlowType;

	// bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	@Column(name = "download_report_type_id")
	private int downloadReportTypeId;

	public ProjectCourse() {
	}

	public ProjectCourse(ProjectCourse pc) {
		this.setCourse(pc.getCourse());
		this.setCourseFlowType(pc.getCourseFlowType());
		this.setDownloadReportTypeId(pc.getDownloadReportTypeId());
		this.setEnabled(pc.getEnabled());
		this.setSelected(pc.getSelected());
		this.setSequence(pc.getSequence());
	}

	public int getProjectCourseId() {
		return this.projectCourseId;
	}

	public void setProjectCourseId(int projectCourseId) {
		this.projectCourseId = projectCourseId;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getSelected() {
		return this.selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public CourseFlowType getCourseFlowType() {
		return this.courseFlowType;
	}

	public void setCourseFlowType(CourseFlowType courseFlowType) {
		this.courseFlowType = courseFlowType;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getSequence() {
		return sequence;
	}

	public int getDownloadReportTypeId() {
		return downloadReportTypeId;
	}

	public void setDownloadReportTypeId(int downloadReportTypeId) {
		this.downloadReportTypeId = downloadReportTypeId;
	}

}