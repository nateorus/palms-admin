package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.pdinh.jaxb.adapter.SqlTimestampAdapter;

/**
 * The persistent class representing a file attachment
 * 
 */

@Entity
@Table(name="file_attachment")
@XmlRootElement(name="fileAttachment")
@XmlAccessorType(XmlAccessType.FIELD)
public class FileAttachment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="file_attachment_id")
	@XmlAttribute
	private int id;

	private String name;
	private String path;
	
	@Column(name="date_uploaded")
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp dateUploaded;
	
	@Column(name="ext_admin_id")
	private String extAdminId;	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Timestamp dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public String getExtAdminId() {
		return extAdminId;
	}

	public void setExtAdminId(String extAdminId) {
		this.extAdminId = extAdminId;
	}


}
