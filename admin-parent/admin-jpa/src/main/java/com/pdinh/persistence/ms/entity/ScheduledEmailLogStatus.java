package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="scheduled_email_log_status")
@NamedQueries({
	@NamedQuery(name = "findStatusByCodeAndLangId", query = "select status from ScheduledEmailLogStatus status where status.code = :status_code and status.languageId = :language_id")
})
public class ScheduledEmailLogStatus implements Serializable{

	/**
	 * 
	 */
	public static final String STATUS_SUCCESSFUL = "SUCCESSFUL";
	public static final String STATUS_INVALID_ADDRESS = "INVALID_ADDRESS";
	public static final String STATUS_UNKNOWN_ERROR = "UNKNOWN_ERROR";
	public static final String STATUS_554_IP_LOOKUP_FAILED = "554_IP_LOOKUP_FAILED";
	public static final String STATUS_550_MAIL_NOT_PROCESSED = "550_MAIL_NOT_PROCESSED";
	public static final String STATUS_550_INVALID_RECIPIENT = "550_INVALID_RECIPIENT";
	public static final String STATUS_CONNECTION_BROKEN = "CONNECTION_BROKEN";
	public static final String STATUS_550_ADDRESS_REJECTED = "550_5.1.1_ADDRESS_REJECTED";
	public static final String STATUS_421_TOO_MANY_MESSAGES = "421_TOO_MANY_MESSAGES";
	public static final String STATUS_SENDING_EMAIL = "SENDING_EMAIL";
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="code")
	private String code;
	
	@Column(name="label")
	private String label;
	
	@Column(name="description")
	private String description;
	
	@Column(name="language_id")
	private int languageId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLanguageId() {
		return languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

}
