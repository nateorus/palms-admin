package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;

@Entity
@Table(name = "self_reg_config")
@NamedQueries({
	@NamedQuery(name = "findConfigByGuid", query = "SELECT c FROM SelfRegConfig c WHERE c.guid = :guid", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "c.project"),
			@QueryHint(name = "eclipselink.batch", value = "c.project.company"),
			@QueryHint(name = "eclipselink.batch", value = "c.allListItems")
	}),
	@NamedQuery(name = "findConfigByProjectId", query = "SELECT c FROM SelfRegConfig c WHERE c.project.projectId = :projectId", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "c.project"),
			@QueryHint(name = "eclipselink.batch", value = "c.project.company"),
			@QueryHint(name = "eclipselink.batch", value = "c.allListItems")
	})
})
public class SelfRegConfig implements Serializable{

	public List<SelfRegListItem> getAllListItems() {
		return allListItems;
	}

	public void setAllListItems(List<SelfRegListItem> allListItems) {
		this.allListItems = allListItems;
	}

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "self_reg_config_id")
	private int selfRegConfigId;
	
	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;
	
	@ManyToOne
	@JoinColumn(name = "scheduled_email_def_id")
	private ScheduledEmailDef scheduledEmailDef;
	
	private boolean enabled;
	
	@Column(name = "self_reg_guid")
	private String guid;
	
	@Column(name = "self_reg_limit")
	private int registrationLimit;
	
	@Column(name = "self_reg_count")
	private int registrationCount;
	
	@Column(name = "last_reg_time")
	private Timestamp lastRegistration;
	
	@OneToMany(mappedBy = "selfRegConfig", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE }, orphanRemoval = true)
	private List<SelfRegListItem> allListItems;
	
	public int getSelfRegConfigId() {
		return selfRegConfigId;
	}

	public void setSelfRegConfigId(int selfRegConfigId) {
		this.selfRegConfigId = selfRegConfigId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getRegistrationLimit() {
		return registrationLimit;
	}

	public void setRegistrationLimit(int registrationLimit) {
		this.registrationLimit = registrationLimit;
	}

	public int getRegistrationCount() {
		return registrationCount;
	}

	public void setRegistrationCount(int registrationCount) {
		this.registrationCount = registrationCount;
	}

	public Timestamp getLastRegistration() {
		return lastRegistration;
	}

	public void setLastRegistration(Timestamp lastRegistration) {
		this.lastRegistration = lastRegistration;
	}

	public ScheduledEmailDef getScheduledEmailDef() {
		return scheduledEmailDef;
	}

	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}


	
	
	

}
