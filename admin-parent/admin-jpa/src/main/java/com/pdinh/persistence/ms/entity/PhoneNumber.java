package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

@Entity
@Table(name = "phone_number")
@NamedQueries({
	@NamedQuery(name = "findByUsersIdAndType", query = "SELECT p FROM PhoneNumber p WHERE p.user.usersId = :usersId and p.phoneNumberType.abbv = :type", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "p.user"),
			@QueryHint(name = "eclipselink.batch", value = "p.phoneNumberType")}),

			@NamedQuery(name = "findByUsersIdListAndType", query = "SELECT p FROM PhoneNumber p WHERE p.user.usersId in :usersIdList and p.phoneNumberType.abbv = :type", hints = {
					@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
					@QueryHint(name = "eclipselink.batch", value = "p.user"),
					@QueryHint(name = "eclipselink.batch", value = "p.phoneNumberType")})
})
public class PhoneNumber implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int phoneNumberId;

	@Column(name = "number")
	private String number;

	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	@Column(name = "is_primary")
	private boolean primary;

	@Column(name = "note")
	private String note;

	@ManyToOne
	@JoinColumn(name = "phone_number_type_id")
	private PhoneNumberType phoneNumberType;

	public PhoneNumber(PhoneNumber number){
		this.setNote(number.getNote());
		this.setNumber(number.getNumber());
		this.setPhoneNumberType(number.getPhoneNumberType());
		this.setPrimary(number.isPrimary());
		this.setUser(number.getUser());
	}

	public PhoneNumber(){

	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}


	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	public PhoneNumberType getPhoneNumberType() {
		return phoneNumberType;
	}

	public void setPhoneNumberType(PhoneNumberType phoneNumberType) {
		this.phoneNumberType = phoneNumberType;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
