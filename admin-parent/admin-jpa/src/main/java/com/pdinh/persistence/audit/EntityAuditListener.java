package com.pdinh.persistence.audit;

import java.util.Map;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




@Stateless
public class EntityAuditListener {
	private static final Logger log = LoggerFactory.getLogger(EntityAuditListener.class);
	
	
	public EntityAuditListener(){
		
	}
	
	@PreUpdate
	public void auditUpdate(Object o){
		log.debug("Updating existing object {}", o);
		FacesContext context = FacesContext.getCurrentInstance();
		String remoteUser = "";
		String operation = "";
		if (context == null){
			log.debug("Context is null");
		}else{
			log.debug("Context is not NULL: User: {}", context.getExternalContext().getRemoteUser());
			
			Map<Object, Object> attrs = context.getAttributes();
			if (attrs.containsKey("updateOperationName")){
				log.debug("Update Operation: {}", attrs.get("updateOperationName"));
				operation = (String) attrs.get("updateOperationName");
			}
			
			remoteUser = context.getExternalContext().getRemoteUser();
		}
		if (o instanceof EntityAuditLogger){
			((EntityAuditLogger) o).doAudit(remoteUser, EntityAuditLogger.UPDATE_ACTION, operation);
		}

	}
	@PrePersist
	public void auditCreate(Object o){
		log.debug("Creating new Object {}", o);
		FacesContext context = FacesContext.getCurrentInstance();
		String remoteUser = "";
		String operation = "";
		if (context == null){
			log.debug("Context is null");
		}else{
			log.debug("Context is not NULL: User: {}", context.getExternalContext().getRemoteUser());
			
			Map<Object, Object> attrs = context.getAttributes();
			if (attrs.containsKey("createOperationName")){
				log.debug("Update Operation: {}", attrs.get("updateOperationName"));
				operation = (String) attrs.get("createOperationName");
			}
			
			remoteUser = context.getExternalContext().getRemoteUser();
		}
		if (o instanceof EntityAuditLogger){
			((EntityAuditLogger) o).doAudit(remoteUser, EntityAuditLogger.CREATE_ACTION, operation);
		}

	}
	
	@PreRemove
	public void auditDelete(Object o){
		log.debug("Creating new Object {}", o);
		FacesContext context = FacesContext.getCurrentInstance();
		String remoteUser = "";
		String operation = "Delete";
		if (context == null){
			log.debug("Context is null");
		}else{
			log.debug("Context is not NULL: User: {}", context.getExternalContext().getRemoteUser());
			
			Map<Object, Object> attrs = context.getAttributes();
			if (attrs.containsKey("deleteOperationName")){
				log.debug("Update Operation: {}", attrs.get("deleteOperationName"));
				operation = (String) attrs.get("deleteOperationName");
			}
			
			remoteUser = context.getExternalContext().getRemoteUser();
		}
		if (o instanceof EntityAuditLogger){
			((EntityAuditLogger) o).doAudit(remoteUser, EntityAuditLogger.DELETE_ACTION, operation);
		}
		
	}
}
