/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/*
 * Entity implementation class for Entity: TargetLevelType
 */
@Entity
@Table(name="company_project_type")

public class CompanyProjectType implements Serializable
{
	//
	// Static variables
	//
	private static final long serialVersionUID = 1L;
	
	//
	// Static methods.
	//

	//
	// Instance variables.
	//
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="company_project_type_id")
	private int companyProjectTypeId;

	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="project_type_id")
	private ProjectType projectType;

	//
	// Constructors.
	//

    public CompanyProjectType()
    {
		super();
    }

	//
	// Instance methods.
	//
    
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getCompanyProjectTypeId()
	{
		return this.companyProjectTypeId;
	}
		
	public void setCompanyProjectTypeId(int value)
	{
		this.companyProjectTypeId = value;
	}

	/*****************************************************************************************/
	public Company getCompany()
	{
		return this.company;
	}
		
	public void setCompany(Company value)
	{
		this.company = value;
	}

	/*****************************************************************************************/
	public ProjectType getProjectType()
	{
		return this.projectType;
	}
		
	public void setProjectType(ProjectType value)
	{
		this.projectType = value;
	}

}
