package com.pdinh.persistence.ms.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="project_document")
public class ProjectDocument extends Document {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="project_document_type_id")
	private ProjectDocumentType projectDocumentType;	
		
	@ManyToOne(targetEntity=Project.class)
	@JoinColumn(name="project_id")
	private Project project;

	public ProjectDocumentType getProjectDocumentType() {
		return projectDocumentType;
	}

	public void setProjectDocumentType(ProjectDocumentType projectDocumentType) {
		this.projectDocumentType = projectDocumentType;
	}	

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}
