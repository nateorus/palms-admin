package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@Table(name = "product")
@NamedQueries({
		@NamedQuery(name = "findAssessmentProductsByCompanyId", query = "SELECT p FROM Product p WHERE p.isAssessment = 1 AND p.code IN (SELECT DISTINCT cp.productCode FROM CompanyProduct cp WHERE cp.company.companyId = :companyId)"),
		@NamedQuery(name = "findProductByCode", query = "SELECT p FROM Product p WHERE p.code = :code") })
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String PROD_ABYD = "ABYD";
	public static final String PROD_COACH = "COACH";
	public static final String PROD_KFP = "KFP";
	public static final String PROD_GLGPS = "MLLLVA";
	public static final String PROD_TLT = "TLT";
	public static final String PROD_TLTDEMO = "TLTDEMO";
	public static final String PROD_VIAEDGE = "VIAEDGE";

	// The following is an oddity. This product code is found in the
	// project_type table (in the code - the product code - column) but is not
	// found in the product table
	public static final String PROD_ASMT = "ASMT";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int productId;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "isCustom")
	private Boolean isCustom;

	@Column(name = "isAssessment")
	private Boolean isAssessment;

	@Column(name = "isDevelopment")
	private Boolean isDevelopment;

	@OneToMany(mappedBy = "product")
	private List<ProductContent> productContent;

	public Product() {
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Boolean isCustom) {
		this.isCustom = isCustom;
	}

	public Boolean getIsAssessment() {
		return isAssessment;
	}

	public void setIsAssessment(Boolean isAssessment) {
		this.isAssessment = isAssessment;
	}

	public Boolean getIsDevelopment() {
		return isDevelopment;
	}

	public void setIsDevelopment(Boolean isDevelopment) {
		this.isDevelopment = isDevelopment;
	}

	public List<ProductContent> getProductContent() {
		return productContent;
	}

	public void setProductContent(List<ProductContent> productContent) {
		this.productContent = productContent;
	}

}
