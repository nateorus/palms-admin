package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the Languages database table.
 * "WHERE l.id NOT IN (SELECT l.id FROM Course c JOIN c.languages la WHERE c.abbv = :abbv)"
 * ),
 */
@Entity
@Table(name = "Languages")
@NamedQueries({

		@NamedQuery(name = "findAllAssessmentLanguages", query = "SELECT l FROM Language l where l.activeAssessment = 1 order by l.listOrder ASC, l.name ASC"),
		@NamedQuery(name = "getLanguageByCode", query = "SELECT l FROM Language l where l.code = :code"),
		@NamedQuery(name = "findLanguagesByCourseAbbv", query = "SELECT l FROM Language l JOIN l.courses c WHERE c.abbv = :abbv"),
		@NamedQuery(name = "findLanguagesUnusedByCourseAbbv", query = "SELECT l FROM Language l "
				+ "WHERE l.code NOT IN (SELECT l2.code FROM Course c JOIN c.languages l2 WHERE c.abbv = :abbv)"),
		@NamedQuery(name = "countLanguageCourseReferenceByCode", query = "SELECT count(c) FROM Language l JOIN l.courses c WHERE l.code = :code")

})
public class Language implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "active_assessment")
	private short activeAssessment;

	@Column(name = "active_development")
	private short activeDevelopment;

	@Column(name = "list_order")
	private int listOrder;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "trans_name")
	private String transName;

	@Column(name = "trans_name_unicode")
	private String transNameUnicode;

	// bi-directional many-to-one association to ScheduledEmailText
	@OneToMany(mappedBy = "language")
	private List<ScheduledEmailText> scheduledEmailTexts;

	// bi-directional many-to-many association to Course
	@ManyToMany(mappedBy = "languages")
	private List<Course> courses;

	// bi-directional many-to-one association to UserCoursePrefs
	@OneToMany(mappedBy = "language")
	private List<UserCoursePrefs> userCoursePrefs;

	@Column(name = "language_country")
	private String languageCountryCode;

	public Language() {
	}

	public Language(String code2, String transName2) {
		super();
		this.code = code2;
		this.transNameUnicode = transName2;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getActiveAssessment() {
		return this.activeAssessment;
	}

	public void setActiveAssessment(short activeAssessment) {
		this.activeAssessment = activeAssessment;
	}

	public short getActiveDevelopment() {
		return this.activeDevelopment;
	}

	public void setActiveDevelopment(short activeDevelopment) {
		this.activeDevelopment = activeDevelopment;
	}

	public int getListOrder() {
		return this.listOrder;
	}

	public void setListOrder(int listOrder) {
		this.listOrder = listOrder;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public String getTransName() {
		return this.transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}

	public String getTransNameUnicode() {
		return this.transNameUnicode;
	}

	public void setTransNameUnicode(String transNameUnicode) {
		this.transNameUnicode = transNameUnicode;
	}

	public List<ScheduledEmailText> getScheduledEmailTexts() {
		return this.scheduledEmailTexts;
	}

	public void setScheduledEmailTexts(List<ScheduledEmailText> scheduledEmailTexts) {
		this.scheduledEmailTexts = scheduledEmailTexts;
	}

	public List<Course> getCourses() {
		return this.courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public void setUserCoursePrefs(List<UserCoursePrefs> userCoursePrefs) {
		this.userCoursePrefs = userCoursePrefs;
	}

	public List<UserCoursePrefs> getUserCoursePrefs() {
		return userCoursePrefs;
	}

	public String getLanguageCountryCode() {
		return languageCountryCode;
	}

	public void setLanguageCountryCode(String languageCountryCode) {
		this.languageCountryCode = languageCountryCode;
	}

}