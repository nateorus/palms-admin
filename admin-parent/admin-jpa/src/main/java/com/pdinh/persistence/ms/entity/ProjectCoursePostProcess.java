/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/*
 * The persistent class for the scorecard_flow database table.
 */

@Entity
@Table(name="project_course_post_process")

@NamedQueries({

	@NamedQuery(
			name="findByCourseId", 
			query="SELECT p FROM ProjectCoursePostProcess p WHERE p.courseId = :courseId"),


	@NamedQuery(
			name="findByProjectId", 
			query="SELECT p FROM ProjectCoursePostProcess p WHERE p.projectId = :projectId")

})

public class ProjectCoursePostProcess implements Serializable
{
	//
	// Static data.
	//
	private static final long serialVersionUID = 1L;
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="project_course_post_process_id")
	private int projectCoursePostProcessid;

	@Column (name="project_id")
	private int projectId;

	@Column (name="course")
	private int courseId;

	//
	// Constructors.
	//
	public ProjectCoursePostProcess()
	{
		// Does nothing currently
	}

	//
	// Instance methods.
	//

	/*
	 * toString() method
	 */
	public String toString()
	{
		String str = "ProjectCoursePostProcess:  ";
		str += "id=" + this.projectCoursePostProcessid + ", projectId=" + this.projectId + ", courseId=" + this.courseId;

		return str;
	}

	// Getters & setters

	//----------------------------------
	public int getProjectCoursePostProcessid()
	{
		return this.projectCoursePostProcessid;
	}

	public void setProjectCoursePostProcessid(int value)
	{
		this.projectCoursePostProcessid = value;
	}

	//----------------------------------
	public int getProjectId()
	{
		return this.projectId;
	}

	public void setProjectId(int value)
	{
		this.projectId = value;
	}

	//----------------------------------
	public int getCourseId()
	{
		return this.courseId;
	}

	public void setCourseId(int value)
	{
		this.courseId = value;
	}

}
