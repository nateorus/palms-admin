package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "report_participant")
public class ReportParticipant implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "report_participant_id")
	private int reportParticipantId;
	
	@ManyToOne
	@JoinColumn(name = "report_generate_request_id")
	private ReportGenerateRequest reportGenerateRequest;
	
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;
	
	@ManyToOne
	@JoinColumn(name = "lang_id")
	private Language language;
	
	@Column(name = "course_version_map")
	private String courseVersionMap;
	
	public ReportParticipant(){
		
	}

	public int getReportParticipantId() {
		return reportParticipantId;
	}

	public void setReportParticipantId(int reportParticipantId) {
		this.reportParticipantId = reportParticipantId;
	}

	public ReportGenerateRequest getReportGenerateRequest() {
		return reportGenerateRequest;
	}

	public void setReportGenerateRequest(ReportGenerateRequest reportGenerateRequest) {
		this.reportGenerateRequest = reportGenerateRequest;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getCourseVersionMap() {
		return courseVersionMap;
	}

	public void setCourseVersionMap(String courseVersionMap) {
		this.courseVersionMap = courseVersionMap;
	}
}
