package com.pdinh.persistence.ms.entity;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the user document database table.
 * 
 */
@Entity
@EntityListeners(value = EntityAuditListener.class)
@Table(name = "user_document")
@XmlRootElement(name = "userDocument")
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries({ @NamedQuery(name = "findDocumentByFileAttachmentId", query = "SELECT doc FROM UserDocument doc WHERE doc.fileAttachment.id = :fileAttachmentId") })
public class UserDocument extends Document implements EntityAuditLogger {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "user_document_type_id")
	private UserDocumentType userDocumentType;

	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	public UserDocumentType getUserDocumentType() {
		return userDocumentType;
	}

	public void setUserDocumentType(UserDocumentType userDocumentType) {
		this.userDocumentType = userDocumentType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			auditLog.info("{} performed {} operation on user {}({}). document id: {}, Type: {}", new Object[] { user,
					EntityAuditLogger.OP_CREATE_USER_DOCUMENT, getUser().getUsersId(), getUser().getUsername(),
					getDocumentId(), getUserDocumentType().getName() });
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			auditLog.info("{} performed {} operation on user {}({}). document id: {}, Type: {}", new Object[] { user,
					EntityAuditLogger.OP_UPDATE_USER_DOCUMENT, getUser().getUsersId(), getUser().getUsername(),
					getDocumentId(), getUserDocumentType().getName() });
			break;
		case EntityAuditLogger.DELETE_ACTION:
			auditLog.info("{} performed {} operation on user {}({}). document id: {}, Type: {}", new Object[] { user,
					EntityAuditLogger.OP_REMOVE_USER_DOCUMENT, getUser().getUsersId(), getUser().getUsername(),
					getDocumentId(), getUserDocumentType().getName() });
			break;
		}

	}
}
