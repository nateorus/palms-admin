package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: ContentToken
 * 
 */
@Entity
@Table(name = "contentToken")
@NamedQueries({
		@NamedQuery(name = "findAllUserTokens", query = "Select c from ContentToken c where c.users_id = :users_id"),
		@NamedQuery(name = "findByGuid", query = "Select c from ContentToken c where c.token= :guid") })
public class ContentToken implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "users_id")
	private int users_id;

	@Column(name = "content_id")
	private String content_id;

	@Column(name = "guid")
	private String token;

	@Column(name = "timestamp", insertable = false)
	private Timestamp timestamp;

	@Column(name = "company_id")
	private int company_id;
	
	@Column(name = "verify_token")
	private int verifyToken;

	public int getVerifyToken() {
		return verifyToken;
	}

	public void setVerifyToken(int verifyToken) {
		this.verifyToken = verifyToken;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsers_id() {
		return users_id;
	}

	public void setUsers_id(int users_id) {
		this.users_id = users_id;
	}

	public String getContent_id() {
		return content_id;
	}

	public void setContent_id(String content_id) {
		this.content_id = content_id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private static final long serialVersionUID = 1L;

	public ContentToken() {
		super();
	}

	@Override
	public String toString() {
		return "ContentToken [id=" + id + ", users_id=" + users_id + ", content_id=" + content_id + ", token=" + token
				+ ", timestamp=" + timestamp + ", company_id=" + company_id + "]";
	}

}
