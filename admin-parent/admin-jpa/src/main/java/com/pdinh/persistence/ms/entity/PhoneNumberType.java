package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name = "phone_number_type")
@NamedQueries({
	@NamedQuery(name = "getByAbbv", query = "SELECT t FROM PhoneNumberType t WHERE t.abbv = :abbv")
})
	
public class PhoneNumberType implements Serializable{

	public static final String BUISINESS_PHONE_ABBV = "B";
	public static final String HOME_PHONE_ABBV = "H";
	public static final String MOBILE_PHONE_ABBV = "M";
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int phoneNumberTypeId;
	
	private String name;
	
	private String abbv;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbv() {
		return abbv;
	}

	public void setAbbv(String abbv) {
		this.abbv = abbv;
	}

}
