package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: EmailRuleSchedule
 *
 */
@Entity
@Table(name="email_rule_schedule")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="jpa_type", discriminatorType=DiscriminatorType.STRING)
@NamedQueries({
	@NamedQuery(name = "findEmailRuleScheduleByEmailTemplateId", query = "SELECT ers FROM EmailRuleSchedule ers WHERE ers.emailRule.id IN  (SELECT er.id FROM EmailRule er WHERE er.emailTemplate.id=:emailId)"),
	@NamedQuery(name = "findRelativeActiveSchedulesByProject", query = "SELECT ers FROM EmailRuleSchedule ers WHERE ers.emailRule.project.projectId = :projectId and ers.emailRuleScheduleType.code = 'RELATIVE_DATE' and ers.complete = false")
})
public class EmailRuleSchedule implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
    @ManyToOne
	@JoinColumn(name="email_rule_id")
	private EmailRule emailRule;
    
    @ManyToOne
	@JoinColumn(name="email_rule_schedule_type_id")
	private EmailRuleScheduleType emailRuleScheduleType;

    @ManyToOne
	@JoinColumn(name="email_rule_schedule_status_id")
	private EmailRuleScheduleStatus emailRuleScheduleStatus;

    //@OneToOne(mappedBy="emailRuleSchedule", cascade=CascadeType.ALL, orphanRemoval=true, optional=false)
	//private EmailRuleToProcess emailRuleToProcess;

	@Column(name="complete")
	private Boolean complete;
	
	@Column(name="error")
	private Boolean error;  
	
	@Column(name="timezone")
	private String timezone;
	
	
	public EmailRuleSchedule() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmailRule getEmailRule() {
		return emailRule;
	}

	public void setEmailRule(EmailRule emailRule) {
		this.emailRule = emailRule;
	}

	public EmailRuleScheduleType getEmailRuleScheduleType() {
		return emailRuleScheduleType;
	}

	public void setEmailRuleScheduleType(EmailRuleScheduleType emailRuleScheduleType) {
		this.emailRuleScheduleType = emailRuleScheduleType;
	}

	public EmailRuleScheduleStatus getEmailRuleScheduleStatus() {
		return emailRuleScheduleStatus;
	}

	public void setEmailRuleScheduleStatus(
			EmailRuleScheduleStatus emailRuleScheduleStatus) {
		this.emailRuleScheduleStatus = emailRuleScheduleStatus;
	}

	public Boolean isComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

	public Boolean isError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	//public EmailRuleToProcess getEmailRuleToProcess() {
	//	return emailRuleToProcess;
	//}

	//public void setEmailRuleToProcess(EmailRuleToProcess emailRuleToProcess) {
	//	this.emailRuleToProcess = emailRuleToProcess;
	//}
   
}
