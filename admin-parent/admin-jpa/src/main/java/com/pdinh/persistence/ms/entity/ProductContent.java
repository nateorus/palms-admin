package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@Table(name = "content_product")
@NamedQueries({
		@NamedQuery(name = "findProductContentByCode", query = "SELECT pc FROM ProductContent pc WHERE pc.product.code = :code") })
public class ProductContent implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int productContentId;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@ManyToOne
	@JoinColumn(name = "content_type_id")
	private ProductContentType productContentType;

	@Column(name = "content_id")
	private int contentId;

	public ProductContent() {
	}

	public int getProductContentId() {
		return productContentId;
	}

	public void setProductContentId(int productContentId) {
		this.productContentId = productContentId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductContentType getProductContentType() {
		return productContentType;
	}

	public void setProductContentType(ProductContentType productContentType) {
		this.productContentType = productContentType;
	}

	public int getContentId() {
		return contentId;
	}

	public void setContentId(int contentId) {
		this.contentId = contentId;
	}

}
