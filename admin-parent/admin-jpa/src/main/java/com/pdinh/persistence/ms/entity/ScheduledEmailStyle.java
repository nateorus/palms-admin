package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the ScheduledEmailStyle database table.
 * 
 */
@Entity
@Table(name = "scheduledEmailStyle")
public class ScheduledEmailStyle implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIRST_PASSWORD_LABEL_EN = "Initial Email";
	public static final String PARTICIPANT_REMINDER_LABEL_EN = "Reminder Email";
	public static final String CUSTOM_LABEL_EN = "Custom";
	
	public static final String FIRST_PASSWORD_CONSTANT = "FIRST_PASSWORD";
	public static final String PARTICIPANT_REMINDER_CONSTANT = "PARTICIPANT_REMINDER";
	public static final String CUSTOM_CONSTANT = "CUSTOM";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "style_constant")
	private String styleConstant;

	@Column(name = "style_label")
	private String styleLabel;

	// setters and getters
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStyleConstant() {
		return this.styleConstant;
	}

	public void setStyleConstant(String styleConstant) {
		this.styleConstant = styleConstant;
	}

	public String getStyleLabel() {
		return this.styleLabel;
	}

	public void setStyleLabel(String styleLabel) {
		this.styleLabel = styleLabel;
	}

}
