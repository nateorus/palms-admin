package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

@Entity
@EntityListeners(value = EntityAuditListener.class)
@Table(name = "report_generate_request")
@NamedQueries({
	@NamedQuery(name = "getReportGenerateRequestsByProjectId", query = "SELECT distinct(rgr) FROM ReportGenerateRequest rgr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rp.project.projectId = :projectId ORDER BY rgr.requestTime DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportGenerateRequestsByProjectId", query = "SELECT count(distinct(rgr)) FROM ReportGenerateRequest rgr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rp.project.projectId = :projectId"),
			
	@NamedQuery(name = "getGenerateRequestsByProjAndStat", query = "SELECT distinct(rgr) FROM ReportGenerateRequest rgr, ReportParticipant rp, ReportRequest rr WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rr MEMBER OF rgr.reportRequests AND "
			+ "rr.reportRequestStatus.code IN :statuses AND "
			+ "rp.project.projectId = :projectId ORDER BY rgr.requestTime DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountGenerateRequestsByProjAndStat", query = "SELECT count(distinct(rgr)) FROM ReportGenerateRequest rgr, ReportParticipant rp, ReportRequest rr WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rr MEMBER OF rgr.reportRequests AND "
			+ "rr.reportRequestStatus.code IN :statuses AND "
			+ "rp.project.projectId = :projectId"),
			
	@NamedQuery(name = "getReportGenerateRequestsByCompanyId", query = "SELECT distinct(rgr) FROM ReportGenerateRequest rgr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rp.user.company.companyId = :companyId ORDER BY rgr.requestTime DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportGenerateRequestsByCompanyId", query = "SELECT count(distinct(rgr)) FROM ReportGenerateRequest rgr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rp.user.company.companyId = :companyId"),
			
	@NamedQuery(name = "getGenerateRequestsByCompAndStat", query = "SELECT distinct(rgr) FROM ReportGenerateRequest rgr, ReportParticipant rp, ReportRequest rr WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rr MEMBER OF rgr.reportRequests AND "
			+ "rr.reportRequestStatus.code IN :statuses AND "
			+ "rp.user.company.companyId = :companyId ORDER BY rgr.requestTime DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountGenerateRequestsByCompAndStat", query = "SELECT count(distinct(rgr)) FROM ReportGenerateRequest rgr, ReportParticipant rp, ReportRequest rr WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rr MEMBER OF rgr.reportRequests AND "
			+ "rr.reportRequestStatus.code IN :statuses AND "
			+ "rp.user.company.companyId = :companyId"),
			
	@NamedQuery(name = "getReportGenerateRequestsByUserId", query = "SELECT distinct(rgr) FROM ReportGenerateRequest rgr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rp.user.usersId = :usersId ORDER BY rgr.requestTime DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountReportGenerateRequestsByUserId", query = "SELECT count(distinct(rgr)) FROM ReportGenerateRequest rgr, ReportParticipant rp WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rp.user.usersId = :usersId"),
			
	@NamedQuery(name = "getGenerateRequestsByUserAndStat", query = "SELECT distinct(rgr) FROM ReportGenerateRequest rgr, ReportParticipant rp, ReportRequest rr WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rr MEMBER OF rgr.reportRequests AND "
			+ "rr.reportRequestStatus.code IN :statuses AND "
			+ "rp.user.usersId = :usersId ORDER BY rgr.requestTime DESC", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")}),
			
	@NamedQuery(name = "getCountGenerateRequestsByUserAndStat", query = "SELECT count(distinct(rgr)) FROM ReportGenerateRequest rgr, ReportParticipant rp, ReportRequest rr WHERE "
			+ "rp MEMBER OF rgr.reportParticipants AND "
			+ "rr MEMBER OF rgr.reportRequests AND "
			+ "rr.reportRequestStatus.code IN :statuses AND "
			+ "rp.user.usersId = :usersId"),
			
	@NamedQuery(name = "getRGRByIdBatchChildren", query = "SELECT rgr FROM ReportGenerateRequest rgr WHERE "
			+ "rgr.reportGenerateRequestId = :id", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "JOIN"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.user"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.project"),
			@QueryHint(name = "eclipselink.batch", value = "rgr.reportParticipants.reportGenerateRequest"),
			//@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests"),
			//@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportParticipants"),
			//@QueryHint(name = "eclipselink.batch", value = "rgr.reportRequests.reportRequestStatus")
			})
})
public class ReportGenerateRequest implements EntityAuditLogger, Serializable{

	public static final int REQUEST_PRIORITY_HIGH = 1;
	public static final int REQUEST_PRIORITY_LOW = 2;

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "report_generate_request_id")
	private int reportGenerateRequestId;
	
	@Column(name = "subject_id")
	private int subjectId;
	
	@Column(name = "principal")
	private String principal;
	
	@Column(name = "request_name")
	private String requestName;
	
	@Column(name = "request_time")
	private Timestamp requestTime;
	
	@Column(name = "priority")
	private int priority;
	
	@OneToMany(mappedBy = "reportGenerateRequest", cascade = {CascadeType.PERSIST }, orphanRemoval = true)
	List<ReportParticipant> reportParticipants;
	
	@OneToMany(mappedBy = "reportGenerateRequest", cascade = {CascadeType.PERSIST }, orphanRemoval = true)
	List<ReportRequest> reportRequests;

	public int getReportGenerateRequestId() {
		return reportGenerateRequestId;
	}

	public void setReportGenerateRequestId(int reportGenerateRequestId) {
		this.reportGenerateRequestId = reportGenerateRequestId;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public List<ReportParticipant> getReportParticipants() {
		return reportParticipants;
	}

	public void setReportParticipants(List<ReportParticipant> reportParticipants) {
		this.reportParticipants = reportParticipants;
	}

	public List<ReportRequest> getReportRequests() {
		return reportRequests;
	}

	public void setReportRequests(List<ReportRequest> reportRequests) {
		this.reportRequests = reportRequests;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		String principal = user;
		String projectName = "";
		int projectId = 0;
		try {
			projectName = getReportParticipants().get(0).getProject().getName();
			projectId = getReportParticipants().get(0).getProject().getProjectId();
		}catch(IndexOutOfBoundsException e){
			//no participants
		}
		if (principal == null || principal.isEmpty()){
			principal = this.principal;
		}
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on request named: ({}), including {} reports in project ({}){}", new Object[] { principal,
						EntityAuditLogger.OP_REQUEST_REPORTS, this.getRequestName(), getReportRequests().size(),
						projectId, projectName });
			} else {
				auditLog.info("{} performed {} operation on request named: ({}), including {} reports in project ({}){}", new Object[] { principal,
						operation, this.getRequestName(), getReportRequests().size(),
						projectId, projectName });
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on request named: ({}){}, including {} reports in project ({}){}", new Object[] { principal,
						EntityAuditLogger.OP_UPDATE_REPORT_REQUEST, this.getReportGenerateRequestId(), this.getRequestName(), getReportRequests().size(),
						projectId, projectName });
			} else {
				auditLog.info("{} performed {} operation on request named: ({}){}, including {} reports in project ({}){}", new Object[] { principal,
						operation, this.getReportGenerateRequestId(), this.getRequestName(), getReportRequests().size(),
						projectId, projectName });
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:
			// This should never happen
			auditLog.info("{} performed {} operation on request named: ({}){}, including {} reports in project ({}){}", new Object[] { principal,
					EntityAuditLogger.OP_DELETE_REPORT_REQUEST, this.getReportGenerateRequestId(), this.getRequestName(),
					getReportRequests().size(), projectId, projectName });
			break;
		}
		
	}
}
