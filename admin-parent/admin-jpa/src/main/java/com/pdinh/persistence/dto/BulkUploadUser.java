package com.pdinh.persistence.dto;

public class BulkUploadUser {

	private final int userId;
	private String firstName = null;
	private String lastName = null;
	private String emailAddress = null;
	private String businessPhone = null;
	private String supervisorName = null;
	private String supervisorEmail = null;
	private String languageCode = null;
	private String optional1 = null;
	private String optional2 = null;
	private String optional3 = null;
	private String logonId = null;
	private String password = null;

	public BulkUploadUser(int userId, String firstName, String lastName, String emailAddress, String supervisorName,
			String supervisorEmail, String languageCode, String optional1, String optional2, String optional3,
			String logonId, String password) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.supervisorName = supervisorName;
		this.supervisorEmail = supervisorEmail;
		this.languageCode = languageCode;
		this.optional1 = optional1;
		this.optional2 = optional2;
		this.optional3 = optional3;
		this.logonId = logonId;
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getSupervisorName() {
		return supervisorName;
	}

	public String getSupervisorEmail() {
		return supervisorEmail;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public String getOptional1() {
		return optional1;
	}

	public String getOptional2() {
		return optional2;
	}

	public String getOptional3() {
		return optional3;
	}

	public int getUserId() {
		return userId;
	}

	public String getLogonId() {
		return logonId;
	}

	public String getPassword() {
		return password;
	}
}
