package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the project_type_report_type database table.
 * 
 */
@Entity
@Table(name="project_type_report_type")
public class ProjectTypeReportType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="project_type_report_type_id")
	private int projectTypeReportTypeId;

	//bi-directional many-to-one association to ReportType
    @ManyToOne
	@JoinColumn(name="report_type_id")
	private ReportType reportType;

	//bi-directional many-to-one association to ProjectType
    @ManyToOne
	@JoinColumn(name="project_type_id")
	private ProjectType projectType;

    @Column(name="sequence_order")
    private int sequenceOrder;
    
    public ProjectTypeReportType() {
    }

	public int getProjectTypeReportTypeId() {
		return this.projectTypeReportTypeId;
	}

	public void setProjectTypeReportTypeId(int projectTypeReportTypeId) {
		this.projectTypeReportTypeId = projectTypeReportTypeId;
	}

	public ReportType getReportType() {
		return this.reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}
	
	public ProjectType getProjectType() {
		return this.projectType;
	}

	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	public int getSequenceOrder() {
		return sequenceOrder;
	}

	public void setSequenceOrder(int sequenceOrder) {
		this.sequenceOrder = sequenceOrder;
	}
	
	
}