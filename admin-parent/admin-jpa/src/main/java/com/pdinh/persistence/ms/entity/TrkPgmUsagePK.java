package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the position database table.
 * 
 */
@Embeddable
public class TrkPgmUsagePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	
	@Column(name="iteration")
	private int interation;
	
	@Column(name="company_id",insertable=false, updatable=false)
	private int companyId;

	@Column(name="users_id",insertable=false, updatable=false)
	private int usersId;

	@Column(name="course",insertable=false, updatable=false)
	private int courseId;

    public TrkPgmUsagePK() {
    }
	public int getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getUsersId() {
		return this.usersId;
	}
	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}
	public int getCourse() {
		return this.courseId;
	}
	public void setCourse(int courseId) {
		this.courseId = courseId;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getInteration() {
		return interation;
	}
	public void setInteration(int interation) {
		this.interation = interation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + companyId;
		result = prime * result + courseId;
		result = prime * result + interation;
		result = prime * result + usersId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrkPgmUsagePK other = (TrkPgmUsagePK) obj;
		if (companyId != other.companyId)
			return false;
		if (courseId != other.courseId)
			return false;
		if (interation != other.interation)
			return false;
		if (usersId != other.usersId)
			return false;
		return true;
	}
}