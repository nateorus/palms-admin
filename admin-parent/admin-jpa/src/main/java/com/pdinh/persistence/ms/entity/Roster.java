package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the roster database table.
 * 
 */
@Entity
@Table(name = "roster")
@NamedQueries({
		@NamedQuery(name = "findRosterByUserIdAndCourseId", query = "SELECT r FROM Roster r WHERE r.user.usersId = :userId AND r.course.course = :courseId"),
		@NamedQuery(name = "existsRosterByUserIdAndCourseId", query = "SELECT COUNT(r.rowguid) FROM Roster r WHERE r.user.usersId = :userId AND r.course.course = :courseId"),
		@NamedQuery(name = "findRostersByUserId", query = "SELECT r FROM Roster r WHERE r.user.usersId = :userId") })
public class Roster implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RosterPK id;

	@Column(name = "followupcomp", insertable = false)
	private short followupcomp;

	private Timestamp followupdate;

	private Timestamp followupRdate;

	@Column(name = "followupRsent", insertable = false)
	private short followupRsent;

	@Column(name = "followupsent", insertable = false)
	private short followupsent;

	@Column(name = "last_node_id")
	private int lastNodeId;

	@Column(name = "ratercomp", insertable = false)
	private short ratercomp;

	@Column(name = "rateremail", insertable = false)
	private short rateremail;

	@Column(name = "rateremailsent", insertable = false)
	private short rateremailsent;

	@Column(name = "raterreceiveresults", insertable = false)
	private short raterreceiveresults;

	@Column(name = "raterrefs", insertable = false)
	private int raterrefs;

	@Column(name = "ratertype", insertable = false)
	private int ratertype;

	@Column(name = "regdate", insertable = false, updatable = false)
	private Timestamp regdate;

	private Timestamp reminderdate;

	@Column(name = "remindersent", insertable = false)
	private short remindersent;

	@Column(name = "rowguid", insertable = false, updatable = false)
	private String rowguid;

	@Column(name = "usecompdef", insertable = false)
	private int usecompdef;

	// bi-directional many-to-one association to Course
	@MapsId("courseId")
	@ManyToOne
	@JoinColumn(name = "courseID")
	private Course course;

	// bi-directional many-to-one association to User
	@MapsId("usersId")
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	// bi-directional many-to-one association to Company
	@MapsId("companyId")
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public Roster() {
	}

	public RosterPK getId() {
		return this.id;
	}

	public void setId(RosterPK id) {
		this.id = id;
	}

	public short getFollowupcomp() {
		return this.followupcomp;
	}

	public void setFollowupcomp(short followupcomp) {
		this.followupcomp = followupcomp;
	}

	public Timestamp getFollowupdate() {
		return this.followupdate;
	}

	public void setFollowupdate(Timestamp followupdate) {
		this.followupdate = followupdate;
	}

	public Timestamp getFollowupRdate() {
		return this.followupRdate;
	}

	public void setFollowupRdate(Timestamp followupRdate) {
		this.followupRdate = followupRdate;
	}

	public short getFollowupRsent() {
		return this.followupRsent;
	}

	public void setFollowupRsent(short followupRsent) {
		this.followupRsent = followupRsent;
	}

	public short getFollowupsent() {
		return this.followupsent;
	}

	public void setFollowupsent(short followupsent) {
		this.followupsent = followupsent;
	}

	public int getLastNodeId() {
		return this.lastNodeId;
	}

	public void setLastNodeId(int lastNodeId) {
		this.lastNodeId = lastNodeId;
	}

	public short getRatercomp() {
		return this.ratercomp;
	}

	public void setRatercomp(short ratercomp) {
		this.ratercomp = ratercomp;
	}

	public short getRateremail() {
		return this.rateremail;
	}

	public void setRateremail(short rateremail) {
		this.rateremail = rateremail;
	}

	public short getRateremailsent() {
		return this.rateremailsent;
	}

	public void setRateremailsent(short rateremailsent) {
		this.rateremailsent = rateremailsent;
	}

	public short getRaterreceiveresults() {
		return this.raterreceiveresults;
	}

	public void setRaterreceiveresults(short raterreceiveresults) {
		this.raterreceiveresults = raterreceiveresults;
	}

	public int getRaterrefs() {
		return this.raterrefs;
	}

	public void setRaterrefs(int raterrefs) {
		this.raterrefs = raterrefs;
	}

	public int getRatertype() {
		return this.ratertype;
	}

	public void setRatertype(int ratertype) {
		this.ratertype = ratertype;
	}

	public Timestamp getRegdate() {
		return this.regdate;
	}

	public void setRegdate(Timestamp regdate) {
		this.regdate = regdate;
	}

	public Timestamp getReminderdate() {
		return this.reminderdate;
	}

	public void setReminderdate(Timestamp reminderdate) {
		this.reminderdate = reminderdate;
	}

	public short getRemindersent() {
		return this.remindersent;
	}

	public void setRemindersent(short remindersent) {
		this.remindersent = remindersent;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public int getUsecompdef() {
		return this.usecompdef;
	}

	public void setUsecompdef(int usecompdef) {
		this.usecompdef = usecompdef;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Course getCourse() {
		return course;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}

}