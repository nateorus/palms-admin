package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.pdinh.persistence.ms.entity.Company;

/**
 * The persistent class for the company_custom_properties database table.
 * 
 */
@Entity
@Table(name = "company_custom_properties")
@NamedQueries({
	@NamedQuery(name="findAllByCompany", query="Select c from CompanyCustomProperty c where c.company.companyId = :companyId"),
	@NamedQuery(name="findByCompanyAndSectionId", query="Select c from CompanyCustomProperty c where c.company.companyId = :companyId and c.customProperty.sectionId = :sectionId")
})
public class CompanyCustomProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int companyCustomPropertyId;

	@OneToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@OneToOne
	@JoinColumn(name = "custom_properties_id")
	private CustomProperty customProperty;

	public CompanyCustomProperty() {
	}

	public int getCompanyCustomPropertyId() {
		return companyCustomPropertyId;
	}

	public void setCompanyCustomPropertyId(int companyCustomPropertyId) {
		this.companyCustomPropertyId = companyCustomPropertyId;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public CustomProperty getCustomProperty() {
		return customProperty;
	}

	public void setCustomProperty(CustomProperty customProperty) {
		this.customProperty = customProperty;
	}

}