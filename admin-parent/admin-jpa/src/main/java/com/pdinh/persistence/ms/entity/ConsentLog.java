package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.QueryHint;
import javax.persistence.Table;

/**
 * The persistent class for the consent_log database table.
 *
 */
@Entity
@Table(name = "consent_log")
@NamedQueries({
		@NamedQuery(name = "findMostRecentForParticipant", query = "select c from ConsentLog c where c.user.usersId = :usersId and c.consentLogId = (select max(cl.consentLogId) from ConsentLog cl where cl.user.usersId = :usersId)", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "c.consent"),
				@QueryHint(name = "eclipselink.batch", value = "c.consentText") }),
		@NamedQuery(name = "findAllForParticipant", query = "select c from ConsentLog c where c.user.usersId = :usersId") })
public class ConsentLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "consent_log_id")
	private int consentLogId;

	@OneToOne
	@JoinColumn(name = "consent_id")
	private Consent consent;

	@OneToOne
	@JoinColumn(name = "consent_text_id")
	private ConsentText consentText;

	@Column(name = "consent_version")
	private double consentVersion;

	private Timestamp date;

	private String disposition;

	private Boolean accepted;

	private String language;

	@Column(name = "orig_ip_addr")
	private String origIpAddr;

	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	public ConsentLog() {
	}

	public int getConsentLogId() {
		return this.consentLogId;
	}

	public void setConsentLogId(int consentLogId) {
		this.consentLogId = consentLogId;
	}

	public double getConsentVersion() {
		return this.consentVersion;
	}

	public void setConsentVersion(double consentVersion) {
		this.consentVersion = consentVersion;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp timestamp) {
		this.date = timestamp;
	}

	public String getDisposition() {
		return this.disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getOrigIpAddr() {
		return this.origIpAddr;
	}

	public void setOrigIpAddr(String origIpAddr) {
		this.origIpAddr = origIpAddr;
	}

	//
	// public int getUsersId() {
	// return this.usersId;
	// }
	//
	// public void setUsersId(int usersId) {
	// this.usersId = usersId;
	// }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Consent getConsent() {
		return consent;
	}

	public void setConsent(Consent consent) {
		this.consent = consent;
	}

	public ConsentText getConsentText() {
		return consentText;
	}

	public void setConsentText(ConsentText consentText) {
		this.consentText = consentText;
	}

}