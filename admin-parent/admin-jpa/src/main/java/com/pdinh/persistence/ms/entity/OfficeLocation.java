package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "office_location")
// Orders office names with physical offices first then the non-physical ones
@NamedQueries({ @NamedQuery(name = "findAllOfficeLocationsOrdered", query = "Select o from OfficeLocation o order by o.isPhysical desc, o.location") })
public class OfficeLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "office_location_id")
	private int id;

	@Column(name = "location")
	private String location;

	// If we get more values than 1 or 0, this will have to be re-factored
	@Column(name = "is_Physical")
	private boolean isPhysical;

	public OfficeLocation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isPysical() {
		return isPhysical;
	}

	@Override
	public String toString() {
		return "OfficeLocation [id=" + id + ", location=" + location + ", isPhysical=" + isPhysical + "]";
	}

}