package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the report_type database table.
 * 
 */
@Entity
@Table(name = "report_type")
@NamedQueries({
	@NamedQuery(name = "findReportTypesByProjectTypeIds", query = "Select distinct ptrt.reportType from ProjectTypeReportType ptrt "
			+ "where ptrt.projectType.projectTypeId in :projectTypeIds order by ptrt.reportType.name asc"),
	@NamedQuery(name = "findReportTypeByCode", query = "SELECT rp FROM ReportType rp WHERE rp.code = :code")
})
public class ReportType implements Serializable {
	private static final long serialVersionUID = 1L;

	// These values are used as flags. They cannot be changed for now but should
	// eventually be refactored to reflect the actual "code" value in the
	// report_type table
	public static final String COACHING_PLAN = "COACHING_PLAN";
	// public static final String COACHING_SUMMARY = "COACHING_SUMMARY";

	// These are the actual DB key names (column = "code") found in the
	// underlying table. These values are needed for some of the checks done in
	// ReportWizardControllerBean
	public static final String COACHING_PLAN_SUMMARY = "COACHING_PLAN_SUMMARY";
	public static final String GLGPS_DEVELOPMENT = "LVA_MLL_DEVELOPMENT";
	public static final String GLGPS_READINESS = "LVA_MLL_READINESS";
	public static final String GLGPS_FIT = "LVA_MLL_FIT";
	public static final String VIAEDGE_INDIVIDUAL_SUMMARY = "VIAEDGE_INDIVIDUAL_SUMMARY";
	public static final String VIAEDGE_INDIVIDUAL_COACHING = "VIAEDGE_INDIVIDUAL_COACHING";
	public static final String VIAEDGE_INDIVIDUAL_FEEDBACK = "VIAEDGE_INDIVIDUAL_FEEDBACK";
	public static final String VIAEDGE_GROUP_FEEDBACK = "VIAEDGE_GROUP_FEEDBACK";
	public static final String VIAEDGE_GROUP_EXTRACT = "VIAEDGE_GROUP_EXTRACT";
	public static final String CHQ_RPT = "CHQ_DETAIL_REPORT";
	public static final String GPI_RPT = "GPI_GRAPHICAL_REPORT";
	public static final String LEI_RPT = "LEI_GRAPHICAL_REPORT";
	public static final String RV_RPT = "RAV_B_GRAPHICAL_REPORT";
	public static final String WG_RPT = "WGE_GRAPHICAL_REPORT";
	public static final String FEX_RPT = "FINEX_DETAIL_REPORT";
	public static final String KFP_NARRATIVE = "KFP_NARRATIVE";
	public static final String KFP_INDIVIDUAL = "KFP_INDIVIDUAL";
	public static final String KFP_SLATE = "KFP_SLATE";
	public static final String KFP_GROUP = "KFP_GROUP";
	public static final String ALP_IND_V2 = "ALP_IND_V2";
	public static final String ALP_TALENT_GRID_V2 = "ALP_TALENT_GRID_V2";
	// public static final String VIAEDGE_TLT_COACHING = "VIAEDGE_TLT_COACHING";
	// public static final String VIAEDGE_TLT_FEEDBACK = "VIAEDGE_TLT_FEEDBACK";
	public static final String TLT_LAG_INDIVIDUAL_SUMMARY = "TLT_LAG_INDIVIDUAL_SUMMARY";
	public static final String OTHER_IND = "OTHER_IND";
	public static final String OTHER_GRP = "OTHER_GRP";

	// report subtype
	// This subtype is VIAEDGE_INDIVIDUAL_FEEDBACK with special handling
	public static final String HYBRID_RPT = "hybridReport";

	// A by D group report types
	public static final String TLT_GROUP_DETAIL_REPORT = "TLT_GROUP_DETAIL_REPORT";
	public static final String TLT_EXTRACT_REPORT = "TLT_EXTRACT_REPORT";
	public static final String TLT_EXTRACT_REPORT_RAW = "TLT_EXTRACT_REPORT_RAW";
	public static final String ABYD_EXTRACT = "ABYD_EXTRACT";
	public static final String ABYD_ANALYTICS_EXTRACT = "ABYD_ANALYTICS_EXTRACT";
	public static final String ABYD_RAW_EXTRACT = "ABYD_RAW_EXTRACT";

	// These reports are not in the repot_type table (old school data)
	public static final String SHELL_LAR_DEVELOPMENT = "SHELL_LAR_DEVELOPMENT";
	public static final String SHELL_LAR_READINESS = "SHELL_LAR_READINESS";

	// This is an Oxcart report that wouldn't normally belong here, but we are
	// using it in the viaEDGE/TLT hybrid report and we need a place to get the
	// report name from. When everything moves to the report server, then this
	// will probably be more widely used
	// public static final String TLT_KF_INDIVIDUAL_SUMMARY_REPORT =
	// "TLT_KF_INDIVIDUAL_SUMMARY_REPORT";
	public static final String TLT_INDIVIDUAL_SUMMARY_REPORT = "TLT_INDIVIDUAL_SUMMARY_REPORT";

	/*
	 * Report type lists
	 */
	public static final List<String> LVA_MLL_REPORTS_TYPES = new ArrayList<String>();
	public static final List<String> SHELL_LOT_REPORTS_TYPES = new ArrayList<String>();
	public static final Map<String, String> REPORT_TYPE_DISPLAY_NAMES = new HashMap<String, String>();
	static {
		LVA_MLL_REPORTS_TYPES.add(GLGPS_DEVELOPMENT);
		LVA_MLL_REPORTS_TYPES.add(GLGPS_READINESS);
		LVA_MLL_REPORTS_TYPES.add(GLGPS_FIT);

		SHELL_LOT_REPORTS_TYPES.add(SHELL_LAR_DEVELOPMENT);
		SHELL_LOT_REPORTS_TYPES.add(SHELL_LAR_READINESS);

		REPORT_TYPE_DISPLAY_NAMES.put(GLGPS_DEVELOPMENT, "GL GPS Development");
		REPORT_TYPE_DISPLAY_NAMES.put(GLGPS_READINESS, "GL GPS Readiness");
		REPORT_TYPE_DISPLAY_NAMES.put(GLGPS_FIT, "GL GPS Fit");
		REPORT_TYPE_DISPLAY_NAMES.put(SHELL_LAR_DEVELOPMENT, "SHELL LOT Development");
		REPORT_TYPE_DISPLAY_NAMES.put(SHELL_LAR_READINESS, "SHELL LOT Readiness");
	}

	/*
	 * Report sub types, these can be derived from the report type but for now
	 * the current report generator is expecting these values
	 */
	public static final String SUB_TYPE_DEVELOPMENT = "development";
	public static final String SUB_TYPE_READINESS = "readiness";
	public static final String SUB_TYPE_FIT = "fit";

	// Actual object definition

	@Id
	@Column(name = "report_type_id")
	private int reportTypeId;

	private String code;

	private String name;

	@Column(name = "participant_language_visible")
	private Boolean participantLanguageVisible;

	@Column(name = "report_language_visible")
	private Boolean reportLanguageVisible;

	@ManyToOne
	@JoinColumn(name = "target_level_group_id")
	private TargetLevelGroup targetLevelGroup;

	@ManyToOne
	@JoinColumn(name = "report_source_type_id")
	private ReportSourceType reportSourceType;
	
	@Column(name = "group_report")
	private boolean groupReport;
	
	@Column(name = "content_type")
	private String contentType;

	public ReportType() {
	}

	public int getReportTypeId() {
		return this.reportTypeId;
	}

	public void setReportTypeId(int reportTypeId) {
		this.reportTypeId = reportTypeId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getParticipantLanguageVisible() {
		return this.participantLanguageVisible;
	}

	public void setParticipantLanguageVisible(Boolean participantLanguageVisible) {
		this.participantLanguageVisible = participantLanguageVisible;
	}

	public Boolean getReportLanguageVisible() {
		return this.reportLanguageVisible;
	}

	public void setReportLanguageVisible(Boolean reportLanguageVisible) {
		this.reportLanguageVisible = reportLanguageVisible;
	}

	public TargetLevelGroup getTargetLevelGroup() {
		return targetLevelGroup;
	}

	public void setTargetLevelGroup(TargetLevelGroup targetLevelGroup) {
		this.targetLevelGroup = targetLevelGroup;
	}

	public ReportSourceType getReportSourceType() {
		return reportSourceType;
	}

	public void setReportSourceType(ReportSourceType reportSourceType) {
		this.reportSourceType = reportSourceType;
	}

	public int getId() {
		return getReportTypeId();
	}

	//
	// Convenience methods
	//
	//

	/**
	 * Convenience method to allow legacy calls (those that existed and that are
	 * not affected by the hybrid report) to be made.
	 * 
	 * @param type
	 * @return subtype
	 */
	public static String getSubTypeFromType(String type) {
		return getSubTypeFromType(type, false);
	}

	public static String getSubTypeFromType(String type, boolean hybridRpt) {
		String subType = "";
		if (SHELL_LAR_DEVELOPMENT.equals(type) || GLGPS_DEVELOPMENT.equals(type)) {
			subType = SUB_TYPE_DEVELOPMENT;
		} else if (SHELL_LAR_READINESS.equals(type) || GLGPS_READINESS.equals(type)) {
			subType = SUB_TYPE_READINESS;
		} else if (GLGPS_FIT.equals(type)) {
			subType = SUB_TYPE_FIT;
		} else if (ReportType.VIAEDGE_INDIVIDUAL_COACHING.equals(type)) {
			subType = VIAEDGE_INDIVIDUAL_COACHING;
		} else if (VIAEDGE_INDIVIDUAL_FEEDBACK.equals(type)) {
			subType = hybridRpt ? HYBRID_RPT : VIAEDGE_INDIVIDUAL_FEEDBACK;
		}
		return subType;
	}

	public boolean isGroupReport() {
		return groupReport;
	}

	public void setGroupReport(boolean groupReport) {
		this.groupReport = groupReport;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}