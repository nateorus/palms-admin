package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the course_roster database table.
 * 
 */
@Entity
@Table(name="course_roster")
@NamedQueries({
	@NamedQuery(name = "getCourseRostersByCompanyIdBatchCourse", query = "SELECT cr FROM CourseRoster cr WHERE cr.company.companyId = :companyId", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "cr.course")}),
	@NamedQuery(name = "deleteCourseRosterByCompanyIdAndCourseId", query = "DELETE FROM CourseRoster cr where cr.company.companyId = :companyId and cr.course.course = :courseId"),
	@NamedQuery(name = "deleteCourseRosterByCompanyIdAndCourseIds", query = "DELETE FROM CourseRoster cr where cr.company.companyId = :companyId and cr.course.course IN :courseIds")
	})
public class CourseRoster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cr_id")
	private int crId;

    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;

    @ManyToOne
	@JoinColumn(name="course")
	private Course course;

	private String rowguid;

    public CourseRoster() {
    }

	public int getCrId() {
		return this.crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Course getCourse() {
		return course;
	}
}