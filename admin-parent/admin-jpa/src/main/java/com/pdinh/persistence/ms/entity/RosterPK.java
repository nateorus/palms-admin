package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the roster database table.
 * 
 */
@Embeddable
public class RosterPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="courseID",insertable=false, updatable=false)
	private int courseId;

	@Column(name="company_id",insertable=false, updatable=false)
	private int companyId;

	@Column(name="users_id",insertable=false, updatable=false)
	private int usersId;

    public RosterPK() {
    }
	public int getCourseId() {
		return this.courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getUsersId() {
		return this.usersId;
	}
	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RosterPK)) {
			return false;
		}
		RosterPK castOther = (RosterPK)other;
		return 
			(this.courseId == castOther.courseId)
			&& (this.companyId == castOther.companyId)
			&& (this.usersId == castOther.usersId);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.courseId;
		hash = hash * prime + this.companyId;
		hash = hash * prime + this.usersId;
		
		return hash;
    }
}