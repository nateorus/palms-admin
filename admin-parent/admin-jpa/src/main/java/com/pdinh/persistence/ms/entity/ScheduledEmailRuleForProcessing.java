package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;

/**
 * The persistent class for the ScheduledEmailLog database table.
 * 
 */
@Entity
@NamedQueries({

	@NamedQuery(name = "findAllRulesForProcessing", query = "Select rfp from ScheduledEmailRuleForProcessing rfp where rfp.ruleId=:ruleId", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS") }) // i need to ask about the value part
})	

public class ScheduledEmailRuleForProcessing implements Serializable{
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="send_date")
	private Timestamp sendDate;
	
	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;

	//bi-directional many-to-one association to Project
    @ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

	//bi-directional many-to-one association to ScheduledEmailDef
    @ManyToOne
	@JoinColumn(name="email_id")
	private ScheduledEmailDef scheduledEmailDef;  // email template Id
    
 //bi-directional many-to-one association to  EmailRulesDisplayDao (for participant list) "rule_id"
 // @ManyToOne
//  @JoinColumn(name="rule_id")
//	private ScheduledEmailRule scheduledEmailRule;  // EmailRulesDisplayDao (for participant list) "rule_id"
@Column(name="rule_id")
private int ruleId;
    
   	private int isFixedDate;  // 0 = fixed, 1= relative, 2=duration
	private int daysBeforeOrAfterRelative;  // can be + or - 

	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
	public ScheduledEmailDef getScheduledEmailDef(){
		return this.scheduledEmailDef;
	}
	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef){
			this.scheduledEmailDef = scheduledEmailDef;
	}
	
	public Project getProject(){
		return this.project;
	}
	public void setProject(Project project){
		this.project = project;
	}
	
	public Company getCompany(){
		return this.company;
	}
	public void setCompany(Company company){
		this.company = company;
	}
	
	public Timestamp getSendDate(){
		return this.sendDate;
	}
	public void setSendDate(Timestamp sendDate){
		this.sendDate = sendDate;
	}
	
//	public ScheduledEmailRule getScheduledEmailRule(){
//		return this.scheduledEmailRule;
//	}
//	public void setScheduledEmailRule(ScheduledEmailRule scheduledEmailRule){
//		this.scheduledEmailRule = scheduledEmailRule;
//	}

	public int getRuleId(){
		return this.ruleId;
	}
	public void setRuleId(int ruleId){
		this.ruleId = ruleId;
	}
	
	public int getIsFixedDate(){
		return this.isFixedDate;
	}
	public void setIsFixedDate(int isFixedDate){
		this.isFixedDate = isFixedDate;
	}
	
	public int getDaysBeforeOrAfterRelative(){
		return this.daysBeforeOrAfterRelative;
	}
	public void setDaysBeforeOrAfterRelative(int daysBeforeOrAfterRelative){
		this.daysBeforeOrAfterRelative = daysBeforeOrAfterRelative;
	}
}
