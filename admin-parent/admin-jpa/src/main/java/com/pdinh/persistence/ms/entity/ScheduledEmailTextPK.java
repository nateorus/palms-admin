package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class ScheduledEmailTextPK implements Serializable {

    private static final long serialVersionUID = 1L;
    	
   	@Column(name="email_id",insertable=false, updatable=false)
	private int emailId;

   	@Column(name="lang",insertable=false, updatable=false)
	private String lang;

    public ScheduledEmailTextPK() {
    }

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ScheduledEmailTextPK)) {
			return false;
		}
		ScheduledEmailTextPK castOther = (ScheduledEmailTextPK)other;
		return 
			(this.emailId == castOther.emailId)
			&& (this.lang.equals(castOther.lang));

    }
    
	public int hashCode() {
		final int prime = 67;
		int hash = 17;
		hash = hash * prime + this.emailId + lang.hashCode();
		
		return hash;
    }

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

}
