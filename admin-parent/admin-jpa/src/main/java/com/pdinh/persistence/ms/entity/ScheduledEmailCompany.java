package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ScheduledEmailCompany database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "findScheduledEmailCompanyByEmailId", query = "SELECT sec FROM ScheduledEmailCompany sec WHERE sec.scheduledEmailDef.id=:emailId")
})
public class ScheduledEmailCompany implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;

    @ManyToOne
	@JoinColumn(name="email_id")
	private ScheduledEmailDef scheduledEmailDef;

    public ScheduledEmailCompany() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}

	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}

	public ScheduledEmailDef getScheduledEmailDef() {
		return scheduledEmailDef;
	}

}