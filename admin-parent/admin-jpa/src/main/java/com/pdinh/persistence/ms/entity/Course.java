package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the course database table.
 * 
 */
@Entity
@Table(name = "course")
@NamedQueries({
		@NamedQuery(name = "findCourseByAbbv", query = "SELECT c FROM Course c where c.abbv = :abbv"),
		@NamedQuery(name = "getCourseTypeByScormCourseId", query = "select c.type FROM Course c where c.course = (select s.courseId from ScormCourseDirectory s where s.scormCourseId = :scorm_course_id)"),
		@NamedQuery(name = "getMaxCourseId", query = "SELECT MAX(c.course) FROM Course c"),
		@NamedQuery(name = "findAllTinCanCourses", query = "SELECT c FROM Course c where c.tinCanUri is NOT NULL"),
		@NamedQuery(name = "findAllCourseAbbvIdArray", query = "SELECT c.abbv, c.course FROM Course c"),
		@NamedQuery(name = "findCoursesIn", query = "SELECT c FROM Course c WHERE c.course IN :courseIds") })
public class Course implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int course;

	private String abbv;

	private int active;

	private String audience;

	@Column(name = "conv_collection")
	private String convCollection;

	@Column(name = "course_completion_type")
	private int courseCompletionType;

	@Column(name = "course_duration")
	private float courseDuration;

	@Column(name = "course_label")
	private String courseLabel;

	@Column(name = "course_vol")
	private String courseVol;

	private String description;

	@Column(name = "ia_collection")
	private String iaCollection;

	private String keyskills;

	private String objectives;

	@Column(name = "parent_course_id")
	private int parentCourseId;

	private int raterflag;

	private int raterrefs;

	private int ratertype;

	private short restricted;

	private String rowguid;

	private String sblurb;

	private String title;

	@Column(name = "title_html")
	private String titleHtml;

	@Column(name = "title_text")
	private String titleText;

	private int type;

	@Column(name = "has_test_data")
	private Boolean hasTestData;

	@Column(name = "launch_params")
	private String launchParams;

	@Column(name = "tin_can_uri")
	private String tinCanUri;

	// bi-directional many-to-many association to Language
	@ManyToMany
	@JoinTable(name = "course_language", joinColumns = { @JoinColumn(name = "course_id") }, inverseJoinColumns = { @JoinColumn(name = "lang_id") })
	private List<Language> languages;



	// bi-directional many-to-one association to CourseAssociation
	@OneToMany(mappedBy = "course")
	private List<CourseAssociation> courseAssociations;

	// bi-directional many-to-one association to CourseAssociation
	@OneToMany(mappedBy = "associatedCourse")
	private List<CourseAssociation> associatedCourseAssociations;

	// Constants for abbreviations not a complete list doesn't include
	// some legacy abbreviations
	public static final String SL1 = "sl1";
	public static final String SL2 = "sl2";
	public static final String SL3 = "sl3";
	public static final String SL0 = "sl0";
	public static final String WG = "wg";
	public static final String RV = "rv";
	public static final String DPT1 = "dpt1";
	public static final String DPT2 = "dpt2";
	public static final String GAPS1 = "gaps1";
	public static final String GAPS2 = "gaps2";
	public static final String YIT = "yit";
	public static final String GPI = "gpi";
	public static final String CS = "cs";
	public static final String DEMO = "demo";
	public static final String LEI = "lei";
	public static final String GPIL = "gpil";
	public static final String FINEX = "finex";
	public static final String CHQ = "chq";
	public static final String CS2 = "cs2";
	public static final String STEIB = "steib";
	public static final String SCEIB = "sceib";
	public static final String LVAPM = "lvapm";
	public static final String IBLVA = "iblva";
	public static final String LVADR = "lvadr";
	public static final String LVABM = "lvabm";
	public static final String LVACI = "lvaci";
	public static final String JNJPC = "jnjpc";
	public static final String VEDGE = "vedge";
	public static final String VEDGE_NODEMO = "vedge_nodemo";
	public static final String KFP = "kfp";
	public static final String RV2 = "rv2";

	public static final int CHQ_ID = 52;

	public Course() {
	}

	public int getCourse() {
		return this.course;
	}

	public void setCourse(int course) {
		this.course = course;
	}

	public String getAbbv() {
		return this.abbv;
	}

	public void setAbbv(String abbv) {
		this.abbv = abbv;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getAudience() {
		return this.audience;
	}

	public void setAudience(String audience) {
		this.audience = audience;
	}

	public String getConvCollection() {
		return this.convCollection;
	}

	public void setConvCollection(String convCollection) {
		this.convCollection = convCollection;
	}

	public int getCourseCompletionType() {
		return this.courseCompletionType;
	}

	public void setCourseCompletionType(int courseCompletionType) {
		this.courseCompletionType = courseCompletionType;
	}

	public float getCourseDuration() {
		return this.courseDuration;
	}

	public void setCourseDuration(float courseDuration) {
		this.courseDuration = courseDuration;
	}

	public String getCourseLabel() {
		return this.courseLabel;
	}

	public void setCourseLabel(String courseLabel) {
		this.courseLabel = courseLabel;
	}

	public String getCourseVol() {
		return this.courseVol;
	}

	public void setCourseVol(String courseVol) {
		this.courseVol = courseVol;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIaCollection() {
		return this.iaCollection;
	}

	public void setIaCollection(String iaCollection) {
		this.iaCollection = iaCollection;
	}

	public String getKeyskills() {
		return this.keyskills;
	}

	public void setKeyskills(String keyskills) {
		this.keyskills = keyskills;
	}

	public String getObjectives() {
		return this.objectives;
	}

	public void setObjectives(String objectives) {
		this.objectives = objectives;
	}

	public int getParentCourseId() {
		return this.parentCourseId;
	}

	public void setParentCourseId(int parentCourseId) {
		this.parentCourseId = parentCourseId;
	}

	public int getRaterflag() {
		return this.raterflag;
	}

	public void setRaterflag(int raterflag) {
		this.raterflag = raterflag;
	}

	public int getRaterrefs() {
		return this.raterrefs;
	}

	public void setRaterrefs(int raterrefs) {
		this.raterrefs = raterrefs;
	}

	public int getRatertype() {
		return this.ratertype;
	}

	public void setRatertype(int ratertype) {
		this.ratertype = ratertype;
	}

	public short getRestricted() {
		return this.restricted;
	}

	public void setRestricted(short restricted) {
		this.restricted = restricted;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public String getSblurb() {
		return this.sblurb;
	}

	public void setSblurb(String sblurb) {
		this.sblurb = sblurb;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleHtml() {
		return this.titleHtml;
	}

	public void setTitleHtml(String titleHtml) {
		this.titleHtml = titleHtml;
	}

	public String getTitleText() {
		return this.titleText;
	}

	public void setTitleText(String titleText) {
		this.titleText = titleText;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<Language> getLanguages() {
		return this.languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}


	public void setCourseAssociations(List<CourseAssociation> courseAssociations) {
		this.courseAssociations = courseAssociations;
	}

	public List<CourseAssociation> getCourseAssociations() {
		return courseAssociations;
	}

	public void setAssociatedCourseAssociations(List<CourseAssociation> associatedCourseAssociations) {
		this.associatedCourseAssociations = associatedCourseAssociations;
	}

	public List<CourseAssociation> getAssociatedCourseAssociations() {
		return associatedCourseAssociations;
	}

	public Boolean getHasTestData() {
		return hasTestData;
	}

	public void setHasTestData(Boolean hasTestData) {
		this.hasTestData = hasTestData;
	}

	public String getLaunchParams() {
		return launchParams;
	}

	public void setLaunchParams(String launchParams) {
		this.launchParams = launchParams;
	}

	public String getTinCanUri() {
		return tinCanUri;
	}

	public void setTinCanUri(String tinCanUri) {
		this.tinCanUri = tinCanUri;
	}
}