package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "self_reg_list_item_type")
public class SelfRegListItemType implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static final String ALLOWED_DOMAIN_TYPE_CODE = "ALLOWED_DOMAIN";
	public static final String WELCOME_MESSAGE_TYPE_CODE = "WELCOME_MESSAGE"; 
	public static final String PAGE_HEADER_TYPE_CODE = "PAGE_HEADER"; 
	public static final String REGISTERED_USER_ID_TYPE_CODE = "REGISTERED_USER_ID"; 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_type_id")
	private int selfRegListItemTypeId;
	
	@Column(name = "item_type_code")
	private String itemTypeCode;
	
	@Column(name = "item_type_label")
	private String itemTypeLabel;

	public int getSelfRegListItemTypeId() {
		return selfRegListItemTypeId;
	}

	public void setSelfRegListItemTypeId(int selfRegListItemTypeId) {
		this.selfRegListItemTypeId = selfRegListItemTypeId;
	}

	public String getItemTypeCode() {
		return itemTypeCode;
	}

	public void setItemTypeCode(String itemTypeCode) {
		this.itemTypeCode = itemTypeCode;
	}

	public String getItemTypeLabel() {
		return itemTypeLabel;
	}

	public void setItemTypeLabel(String itemTypeLabel) {
		this.itemTypeLabel = itemTypeLabel;
	}


}
