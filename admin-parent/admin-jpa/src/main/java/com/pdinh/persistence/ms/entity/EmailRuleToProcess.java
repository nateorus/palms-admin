package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the email_rule_to_process database table.
 * 
 */
@Entity
@Table(name = "email_rule_to_process")
@NamedQueries({
		@NamedQuery(name = "findEmailRuleToProcessByScheduleId", query = "select erp from EmailRuleToProcess erp where erp.emailRuleSchedule.id = :emailRuleScheduleId"),
		@NamedQuery(name = "findEmailRuleToProcessByProjectId", query = "select erp from EmailRuleToProcess erp where erp.emailRuleSchedule.id in "
				+ "(select ers.id from EmailRuleSchedule ers where ers.emailRule.id in (select er.id from EmailRule er where er.project.projectId = :projectId))") })
public class EmailRuleToProcess implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "send_date")
	private Timestamp sendDate;

	@ManyToOne
	@JoinColumn(name = "email_rule_id")
	private EmailRule emailRule;

	@OneToOne
	@JoinColumn(name = "email_rule_schedule_id")
	private EmailRuleSchedule emailRuleSchedule;

	public EmailRuleToProcess() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Timestamp sendDate) {
		this.sendDate = sendDate;
	}

	public EmailRule getEmailRule() {
		return this.emailRule;
	}

	public void setEmailRule(EmailRule emailRule) {
		this.emailRule = emailRule;
	}

	public EmailRuleSchedule getEmailRuleSchedule() {
		return emailRuleSchedule;
	}

	public void setEmailRuleSchedule(EmailRuleSchedule emailRuleSchedule) {
		this.emailRuleSchedule = emailRuleSchedule;
	}

	// public EmailRuleSchedule getEmailRuleSchedule() {
	// return this.emailRuleSchedule;
	// }

	// public void setEmailRuleSchedule(EmailRuleSchedule emailRuleSchedule) {
	// this.emailRuleSchedule = emailRuleSchedule;
	// }

}