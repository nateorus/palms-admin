package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the course_visibility database table.
 * 
 */
@Entity
@Table(name="course_visibility")
@NamedQueries({
	@NamedQuery(name = "findByCompanyIdAndCourseId", query = "Select cv from CourseVisibility cv where cv.company.companyId = :companyId and cv.course.course = :courseId"),
	@NamedQuery(name = "deleteByCompanyIdAndCourseId", query = "DELETE FROM CourseVisibility cv where cv.company.companyId = :companyId and cv.course.course = :courseId"),
	@NamedQuery(name = "deleteByCompanyIdAndCourseIds", query = "DELETE FROM CourseVisibility cv WHERE cv.company.companyId = :companyId and cv.course.course IN :courseIds"),
	@NamedQuery(name = "findByCompanyBatchCourses", query = "SELECT cv FROM CourseVisibility cv WHERE cv.company.companyId = :companyId", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "cv.course")
	})})
public class CourseVisibility implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CourseVisibilityPK id;

	//bi-directional many-to-one association to Company
    @MapsId("companyId")
    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
    
    //bi-directional many-to-one association to Course
	@MapsId("courseId")
	@ManyToOne
	@JoinColumn(name="course_id")
	private Course course;
	
	private String rowguid;

    public CourseVisibility() {
    }

	public CourseVisibilityPK getId() {
		return this.id;
	}

	public void setId(CourseVisibilityPK id) {
		this.id = id;
	}
	
	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Course getCourse() {
		return course;
	}

}