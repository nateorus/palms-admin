package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

@Entity
@Table(name = "course_version")
@NamedQueries({
		@NamedQuery(name = "findCourseVersionsByCourseId", query = "SELECT cv FROM CourseVersion cv where cv.course.course = :courseId", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "cv.course") }),
		@NamedQuery(name = "findAllCourseVersionsBatchCourse", query = "SELECT cv FROM CourseVersion cv", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "cv.course") }) })
public class CourseVersion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_version_id")
	private int courseVersionId;

	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course;

	@Column(name = "course_version_label")
	private String courseVersionLabel;

	@Column(name = "version_number")
	private int versionNumber;

	@Column(name = "version_start_ts")
	private Timestamp versionStartTs;

	@Column(name = "version_end_ts")
	private Timestamp versionEndTs;

	@Column(name = "is_active")
	private boolean isActive;

	@Column(name = "is_default")
	private boolean isDefault;

	public int getCourseVersionId() {
		return courseVersionId;
	}

	public void setCourseVersionId(int courseVersionId) {
		this.courseVersionId = courseVersionId;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getCourseVersionLabel() {
		return courseVersionLabel;
	}

	public void setCourseVersionLabel(String courseVersionLabel) {
		this.courseVersionLabel = courseVersionLabel;
	}

	public int getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	public Timestamp getVersionStartTs() {
		return versionStartTs;
	}

	public void setVersionStartTs(Timestamp versionStartTs) {
		this.versionStartTs = versionStartTs;
	}

	public Timestamp getVersionEndTs() {
		return versionEndTs;
	}

	public void setVersionEndTs(Timestamp versionEndTs) {
		this.versionEndTs = versionEndTs;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

}
