package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * Entity implementation class for Entity: PasswordPolicy
 *
 */
@Entity
@EntityListeners(value=EntityAuditListener.class)
@Table(name="password_policy")
@NamedQueries({
	@NamedQuery(name = "findTemplatePolicyByConstant", query = "select p from PasswordPolicy p where p.constant = :constant and p.template = true")
})
public class PasswordPolicy implements EntityAuditLogger, Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "password_policy_id")
	private int passwordPolicyId;
	
	private String description;
	
	@Column(name = "date_created")
	private Timestamp dateCreated;
	
	@Column(name = "date_modified")
	private Timestamp dateModified;
	
	@Column(name = "term_in_days")
	private int termInDays;
	
	@Column(name = "allowed_changes")
	private int allowedChanges;
	
	@Column(name = "allowed_changes_period_minutes")
	private int allowedChangesPeriodMinutes;
	
	@Column(name = "unsuccessful_attempts")
	private int unsuccessfulAttempts;
	
	@Column(name = "unsuccessful_attempts_period_minutes")
	private int unsuccessfulAttemptsPeriodMinutes;
	
	@Column(name = "max_passwords_in_history")
	private int maxPasswordsInHistory;
	
	@Column(name = "notification_days_prior_to_expiration")
	private int notificationDaysPriorToExpiration;
	
	@Column(name = "reminder_notification_period_days")
	private int reminderNotificationPeriodDays;
	
	@Column(name = "lockout_duration_minutes")
	private int lockoutDurationMinutes;
	
	@Column(name = "validation_custom_regex")
	private String validationCustomRegex;
	
	@Column(name = "generate_random")
	private boolean generateRandom;
	
	@Column(name = "is_template")
	private boolean template;
	
	@Column(name = "minimum_length")
	private int minimumLength;
	
	@Column(name = "maximum_length")
	private int maximumLength;
	
	@Column(name = "require_lowercase")
	private boolean requireLowercase;
	
	@Column(name = "require_uppercase")
	private boolean requireUppercase;
	
	@Column(name = "require_numeric")
	private boolean requireNumeric;
	
	@Column(name = "require_special")
	private boolean requireSpecial;
	
	@Column(name = "constant")
	private String constant;
	
	@ManyToOne
	@JoinColumn(name = "scheduled_email_def_id", referencedColumnName = "id")
	private ScheduledEmailDef scheduledEmailDef;

	public PasswordPolicy() {
		super();
	}

	public int getPasswordPolicyId() {
		return passwordPolicyId;
	}

	public void setPasswordPolicyId(int passwordPolicyId) {
		this.passwordPolicyId = passwordPolicyId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public int getTermInDays() {
		return termInDays;
	}

	public void setTermInDays(int termInDays) {
		this.termInDays = termInDays;
	}

	public int getAllowedChanges() {
		return allowedChanges;
	}

	public void setAllowedChanges(int allowedChanges) {
		this.allowedChanges = allowedChanges;
	}

	public int getAllowedChangesPeriodMinutes() {
		return allowedChangesPeriodMinutes;
	}

	public void setAllowedChangesPeriodMinutes(int allowedChangesPeriodMinutes) {
		this.allowedChangesPeriodMinutes = allowedChangesPeriodMinutes;
	}

	public int getUnsuccessfulAttempts() {
		return unsuccessfulAttempts;
	}

	public void setUnsuccessfulAttempts(int unsuccessfulAttempts) {
		this.unsuccessfulAttempts = unsuccessfulAttempts;
	}

	public int getUnsuccessfulAttemptsPeriodMinutes() {
		return unsuccessfulAttemptsPeriodMinutes;
	}

	public void setUnsuccessfulAttemptsPeriodMinutes(
			int unsuccessfulAttemptsPeriodMinutes) {
		this.unsuccessfulAttemptsPeriodMinutes = unsuccessfulAttemptsPeriodMinutes;
	}

	public int getMaxPasswordsInHistory() {
		return maxPasswordsInHistory;
	}

	public void setMaxPasswordsInHistory(int maxPasswordsInHistory) {
		this.maxPasswordsInHistory = maxPasswordsInHistory;
	}

	public int getNotificationDaysPriorToExpiration() {
		return notificationDaysPriorToExpiration;
	}

	public void setNotificationDaysPriorToExpiration(
			int notificationDaysPriorToExpiration) {
		this.notificationDaysPriorToExpiration = notificationDaysPriorToExpiration;
	}

	public int getReminderNotificationPeriodDays() {
		return reminderNotificationPeriodDays;
	}

	public void setReminderNotificationPeriodDays(int reminderNotificationPeriodDays) {
		this.reminderNotificationPeriodDays = reminderNotificationPeriodDays;
	}

	public int getLockoutDurationMinutes() {
		return lockoutDurationMinutes;
	}

	public void setLockoutDurationMinutes(int lockoutDurationMinutes) {
		this.lockoutDurationMinutes = lockoutDurationMinutes;
	}

	public String getValidationCustomRegex() {
		return validationCustomRegex;
	}

	public void setValidationCustomRegex(String validationCustomRegex) {
		this.validationCustomRegex = validationCustomRegex;
	}

	public boolean isGenerateRandom() {
		return generateRandom;
	}

	public void setGenerateRandom(boolean generateRandom) {
		this.generateRandom = generateRandom;
	}

	public boolean isTemplate() {
		return template;
	}

	public void setTemplate(boolean template) {
		this.template = template;
	}

	public int getMinimumLength() {
		return minimumLength;
	}

	public void setMinimumLength(int minimumLength) {
		this.minimumLength = minimumLength;
	}

	public int getMaximumLength() {
		return maximumLength;
	}

	public void setMaximumLength(int maximumLength) {
		this.maximumLength = maximumLength;
	}

	public boolean isRequireLowercase() {
		return requireLowercase;
	}

	public void setRequireLowercase(boolean requireLowercase) {
		this.requireLowercase = requireLowercase;
	}

	public boolean isRequireUppercase() {
		return requireUppercase;
	}

	public void setRequireUppercase(boolean requireUppercase) {
		this.requireUppercase = requireUppercase;
	}

	public boolean isRequireNumeric() {
		return requireNumeric;
	}

	public void setRequireNumeric(boolean requireNumeric) {
		this.requireNumeric = requireNumeric;
	}

	public boolean isRequireSpecial() {
		return requireSpecial;
	}

	public void setRequireSpecial(boolean requireSpecial) {
		this.requireSpecial = requireSpecial;
	}

	public ScheduledEmailDef getScheduledEmailDef() {
		return scheduledEmailDef;
	}

	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(String constant) {
		this.constant = constant;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action){
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()){
				auditLog.info("{} performed {} operation on Password Policy ({})", new Object[]{user, EntityAuditLogger.OP_CREATE_PASSWORD_POLICY, getDescription()});
			}else{
				auditLog.info("{} performed {} operation on Password Policy ({})", new Object[]{user, operation, getDescription()});
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()){
				auditLog.info("{} performed {} operation on Password Policy {}({})", new Object[]{user, EntityAuditLogger.OP_UPDATE_PASSWORD_POLICY, getPasswordPolicyId(),getDescription()});
			}else{
				auditLog.info("{} performed {} operation on Password Policy {}({})", new Object[]{user, operation, getPasswordPolicyId(),getDescription()});
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:
			
			auditLog.info("{} performed {} operation on Password Policy: {}({})", new Object[]{user, EntityAuditLogger.OP_DELETE_PASSWORD_POLICY,  getPasswordPolicyId(),getDescription()});
			break;
		}
		
	}
   
}
