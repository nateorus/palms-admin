package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ScheduledEmailText database table.
 * 
 */
@Entity
@NamedQueries({
	
	@NamedQuery(name = "findScheduledEmailById", query = "SELECT setxt FROM ScheduledEmailText setxt WHERE setxt.id=:id", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name= "eclipselink.batch", value="setxt.language")}),
	@NamedQuery(name = "deleteScheduledEmailTextsByTemplateId", query = "DELETE FROM ScheduledEmailText txt WHERE txt.scheduledEmailDef.id=:id"),
	@NamedQuery(name = "findByDefBatchLang", query = "SELECT txt FROM ScheduledEmailText txt WHERE txt.scheduledEmailDef.id = :defId", hints = {
		@QueryHint(name= "eclipselink.join-fetch", value ="txt.language"),
		@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
		@QueryHint(name= "eclipselink.batch", value="txt.scheduledEmailDef")
	})
})
public class ScheduledEmailText implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="subject")
	private String subject;

	@Column(name="text")
	private String text;

	//bi-directional many-to-one association to Language
    @ManyToOne
	@JoinColumn(name="lang", referencedColumnName="code")
	private Language language;

	//bi-directional many-to-one association to ScheduledEmailDef
    @ManyToOne
	@JoinColumn(name="email_id")
	private ScheduledEmailDef scheduledEmailDef;

    public ScheduledEmailText() {
    }

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}
	
	public ScheduledEmailDef getScheduledEmailDef() {
		return this.scheduledEmailDef;
	}

	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
}