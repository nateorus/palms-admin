package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the coaching_plan_status_type database table.
 * 
 */
@Entity
@Table(name = "coaching_plan_status_type")
public class CoachingPlanStatusType implements Serializable {
	private static final long serialVersionUID = 1L;

	public static int NOT_STARTED = 0;
	public static int IN_PROGRESS = 1;
	public static int COMPLETE = 2;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "coaching_plan_status_type_id")
	private int coachingPlanStatusTypeId;

	private String description;

	private String name;

	public CoachingPlanStatusType() {
	}

	public int getCoachingPlanStatusTypeId() {
		return this.coachingPlanStatusTypeId;
	}

	public void setCoachingPlanStatusTypeId(int coachingPlanStatusTypeId) {
		this.coachingPlanStatusTypeId = coachingPlanStatusTypeId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}