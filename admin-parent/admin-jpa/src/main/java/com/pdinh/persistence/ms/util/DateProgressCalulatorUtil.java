package com.pdinh.persistence.ms.util;

import java.util.Date;

public class DateProgressCalulatorUtil {

	public static int calculateElapsedPercentage(Date startDate, Date endDate) {
		int elapsedPercentage = 0;

		if (startDate != null && endDate != null && startDate.getTime() <= endDate.getTime()
				&& startDate.getTime() <= new Date().getTime()) {
			Date todayDate = new Date();
			long dayMillis = 1000 * 60 * 60 * 24;

			// add a day to the end date since the 100% can only be reached the
			// day after the end date, dates
			// are stored as the day of at 12:00am
			long diff_a = (endDate.getTime() + dayMillis) - startDate.getTime(); // total
																					// time
			long totalDays = (diff_a / dayMillis); // positive number of days
			long diff_b = todayDate.getTime() - startDate.getTime(); // elapsed
																		// time
			long elapsedDays = (diff_b / dayMillis); // number of
														// days

			if (totalDays > 0) {
				elapsedPercentage = (Math.round(Float.valueOf(elapsedDays * 100) / totalDays));
			} else {
				elapsedPercentage = 100;
			}

			if (elapsedPercentage > 100) {
				elapsedPercentage = 100; // set max of 100 (%)
			}
		}
		if (elapsedPercentage > 0) {
			return elapsedPercentage;
		} else {
			return 0;
		}
	}
}
