package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the consent database table.
 *
 */
@Entity
@Table(name = "consent")
@NamedQueries({
		@NamedQuery(name = "findAllDefaultConsentVersions", query = "select c from Consent c where c.isCustom = 0 order by c.consentVersion asc"),
		@NamedQuery(name = "findAllConsentVersionsByCompany", query = "select c from Consent c where c.company.companyId = :companyId order by c.consentVersion asc"),
		@NamedQuery(name = "findByCompany", query = "select c from Consent c where c.company.companyId = :companyId and c.consentVersion = (select max(cc.consentVersion) from Consent cc where cc.company.companyId = :companyId)"),
		@NamedQuery(name = "findDefaultConsent", query = "select c from Consent c where c.isCustom = 0 and c.consentVersion = (select max(cc.consentVersion) from Consent cc where cc.isCustom = 0)") })
public class Consent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "consent_id")
	private int consentId;

	@JoinColumn(name = "company_id")
	private Company company;

	@Column(name = "consent_version")
	private double consentVersion;

	@Column(name = "date_active")
	private Timestamp dateActive;

	private String description;

	@Column(name = "fork_locale")
	private Boolean forkLocale;

	@Column(name = "is_custom")
	private Boolean isCustom;

	@Column(name = "type_constant")
	private String typeConstant;

	@OneToMany
	@JoinColumn(name = "consent_id")
	private List<ConsentText> consentTexts;

	@Column(name = "is_major_version")
	private boolean majorVersion;

	public Consent() {
	}

	public int getConsentId() {
		return this.consentId;
	}

	public void setConsentId(int consentId) {
		this.consentId = consentId;
	}

	// public int getCompanyId() {
	// return this.companyId;
	// }
	//
	// public void setCompanyId(int companyId) {
	// this.companyId = companyId;
	// }

	public double getConsentVersion() {
		return this.consentVersion;
	}

	public void setConsentVersion(double consentVersion) {
		this.consentVersion = consentVersion;
	}

	public Timestamp getDateActive() {
		return this.dateActive;
	}

	public void setDateActive(Timestamp dateActive) {
		this.dateActive = dateActive;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getForkLocale() {
		return this.forkLocale;
	}

	public void setForkLocale(Boolean forkLocale) {
		this.forkLocale = forkLocale;
	}

	public Boolean getIsCustom() {
		return this.isCustom;
	}

	public void setIsCustom(Boolean isCustom) {
		this.isCustom = isCustom;
	}

	public String getTypeConstant() {
		return this.typeConstant;
	}

	public void setTypeConstant(String typeConstant) {
		this.typeConstant = typeConstant;
	}

	public List<ConsentText> getConsentTexts() {
		return consentTexts;
	}

	public void setConsentTexts(List<ConsentText> consentTexts) {
		this.consentTexts = consentTexts;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(boolean majorVersion) {
		this.majorVersion = majorVersion;
	}

}