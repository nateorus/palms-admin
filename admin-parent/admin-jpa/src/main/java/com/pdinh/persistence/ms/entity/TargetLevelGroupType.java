/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TargetLevelGroupType
 *
 */
@Entity
@Table(name="target_level_group_type")
@NamedQueries(value = { 
		@NamedQuery(name = "findAllGroupTypesBatch", query = "select tlgt from TargetLevelGroupType tlgt", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value="tlgt.targetLevelGroup"),
				@QueryHint(name = "eclipselink.batch", value="tlgt.targetLevelType")
		}) 
})
public class TargetLevelGroupType implements Serializable
{
	//
	// Static variables
	//
	private static final long serialVersionUID = 1L;
	
	//
	// Static methods.
	//

	//
	// Instance variables.
	//
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="target_level_group_type_id")
	private int targetLevelGroupTypeId;

    @ManyToOne
	@JoinColumn(name="target_level_group_id")
	private TargetLevelGroup targetLevelGroup;

    @ManyToOne
	@JoinColumn(name="target_level_type_id")
	private TargetLevelType targetLevelType;

	@Column(name="sequence_order")
	private int sequenceOrder;

	//
	// Constructors.
	//

	public TargetLevelGroupType() {
		super();
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getTargetLevelGroupTypeId()
	{
		return this.targetLevelGroupTypeId;
	}
		
	public void setTargetLevelGroupTypeId(int value)
	{
		this.targetLevelGroupTypeId = value;
	}

	/*****************************************************************************************/
	public TargetLevelGroup getTargetLevelGroup()
	{
		return this.targetLevelGroup;
	}
		
	public void setTargetLevelGroup(TargetLevelGroup value)
	{
		this.targetLevelGroup = value;
	}

	/*****************************************************************************************/
	public TargetLevelType getTargetLevelType()
	{
		return this.targetLevelType;
	}
		
	public void setTargetLevelType(TargetLevelType value)
	{
		this.targetLevelType = value;
	}

	/*****************************************************************************************/
	public int getSequenceOrder()
	{
		return this.sequenceOrder;
	}
		
	public void setSequenceOrder(int value)
	{
		this.sequenceOrder = value;
	}

}
