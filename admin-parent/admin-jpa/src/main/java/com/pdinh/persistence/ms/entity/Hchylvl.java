package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the hchylvl database table.
 * 
 */
@Entity
@Table(name="hchylvl")
@NamedQueries({
    @NamedQuery(
        name="findByLevelByName",
        query="SELECT h FROM Hchylvl h WHERE h.company.companyId = :companyId and h.levelname= :levelName")
 })
public class Hchylvl implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="hchyid")
	private int id;
	
	@Column(name="levelid")
	private String levelid;

	private String levelname;

	private String rowguid;
	
	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;

    public Hchylvl() {
    }


	
	public int getId() {
		return this.id;
	}

	public void setId(int levelid) {
		this.id = levelid;
	}

	public String getLevelname() {
		return this.levelname;
	}

	public void setLevelname(String levelname) {
		this.levelname = levelname;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}



	public void setCompany(Company company) {
		this.company = company;
	}



	public Company getCompany() {
		return company;
	}



	public void setLevelid(String levelid) {
		this.levelid = levelid;
	}



	public String getLevelid() {
		return levelid;
	}

}