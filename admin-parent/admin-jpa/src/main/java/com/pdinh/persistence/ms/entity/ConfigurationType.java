package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "configuration_type")
public class ConfigurationType implements Serializable {

	private static final long serialVersionUID = 7944351967522674803L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "configuration_type_id")
	private int configurationTypeId;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "sequence_order")
	private int sequenceOrder;

	@ManyToOne
	@JoinColumn(name = "configuration_group_id")
	private ConfigurationGroup configurationGroup;

	public ConfigurationType() {
		super();
	}

	public ConfigurationType(int id) {
		this.configurationTypeId = id;
	}

	public int getConfigurationTypeId() {
		return configurationTypeId;
	}

	public void setConfigurationTypeId(int configurationTypeId) {
		this.configurationTypeId = configurationTypeId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getSequenceOrder() {
		return sequenceOrder;
	}

	public void setSequenceOrder(int sequenceOrder) {
		this.sequenceOrder = sequenceOrder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ConfigurationGroup getConfigurationGroup() {
		return this.configurationGroup;
	}

	public void setConfigurationGroup(ConfigurationGroup value) {
		this.configurationGroup = value;
	}
}
