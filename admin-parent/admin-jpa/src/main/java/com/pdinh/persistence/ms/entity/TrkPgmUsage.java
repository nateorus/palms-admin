package com.pdinh.persistence.ms.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="trkPgmUsage")
@NamedQueries({
    @NamedQuery(name="findTrkPgmUsageByRowguid", query="Select t from TrkPgmUsage t where t.rowguid = :rowguid"),
    @NamedQuery(name="getTrkPgmUsageIterations", query="Select count(t.rowguid) from TrkPgmUsage t where t.course.course = :courseId and t.user.usersId = :usersId")
})
public class TrkPgmUsage {
	
	@EmbeddedId
	private TrkPgmUsagePK id;
	
	@Column(name="rowguid",insertable=false, updatable=false)
	private String rowguid;
	
	@Column(name="start_time")
	private Timestamp startTime;
	
	@Column(name="end_time")
	private Timestamp endTime;
	
	//bi-directional many-to-one association to Course
	@MapsId("courseId")
	@ManyToOne
	@JoinColumn(name="course")
	private Course course;

	//bi-directional many-to-one association to User
	@MapsId("usersId")
    @ManyToOne
	@JoinColumn(name="users_id")
	private User user;
    
	//bi-directional many-to-one association to Company
    @MapsId("companyId")
    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
		
	public TrkPgmUsagePK getId() {
		return id;
	}

	public void setId(TrkPgmUsagePK id) {
		this.id = id;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
