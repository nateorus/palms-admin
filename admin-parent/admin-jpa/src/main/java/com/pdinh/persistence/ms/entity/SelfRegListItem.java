package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "self_reg_list_item")
public class SelfRegListItem implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "self_reg_list_item_id")
	private int selfRegListItemId;
	
	@Column(name = "item_text")
	private String itemText;
	
	@Column(name = "item_lang_code")
	private String langCode;
	
	@Column(name = "active")
	private boolean active;
	
	@ManyToOne
	@JoinColumn(name = "self_reg_config_id")
	private SelfRegConfig selfRegConfig;

	@ManyToOne
	@JoinColumn(name = "item_type_id")
	private SelfRegListItemType itemType;
	
	
	public SelfRegConfig getSelfRegConfig() {
		return selfRegConfig;
	}

	public void setSelfRegConfig(SelfRegConfig selfRegConfig) {
		this.selfRegConfig = selfRegConfig;
	}	

	public int getSelfRegListItemId() {
		return selfRegListItemId;
	}

	public void setSelfRegListItemId(int selfRegListItemId) {
		this.selfRegListItemId = selfRegListItemId;
	}

	public String getItemText() {
		return itemText;
	}

	public void setItemText(String itemText) {
		this.itemText = itemText;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public SelfRegListItemType getItemType() {
		return itemType;
	}

	public void setItemType(SelfRegListItemType itemType) {
		this.itemType = itemType;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	

}
