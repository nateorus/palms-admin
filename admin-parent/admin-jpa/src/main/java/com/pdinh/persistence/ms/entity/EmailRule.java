package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.QueryHint;
import javax.persistence.Table;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the email_rule database table.
 * 
 */
@Entity
@EntityListeners(value = EntityAuditListener.class)
@Table(name = "email_rule")
@NamedQueries({
		@NamedQuery(name = "findEmailRulesByProjectId", query = "select er from EmailRule er where er.project.projectId = :projectId", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "er.emailTemplate")
		}),
		@NamedQuery(name = "findEmailRulesByEmailTemplateId", query = "select er from EmailRule er where er.emailTemplate.id = :emailId")

})
public class EmailRule implements EntityAuditLogger, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private Boolean active;

	@Column(name = "all_participants")
	private Boolean allParticipants;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@ManyToOne
	@JoinColumn(name = "email_id")
	private ScheduledEmailDef emailTemplate;

	@Column(name = "excluded_participants")
	private Boolean excludedParticipants;

	private String language;

	private String name;

	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	@OneToMany(mappedBy = "emailRule", cascade = {CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = true)
	private List<EmailRuleParticipant> emailRuleParticipants;

	@OneToMany(mappedBy = "emailRule", cascade = { CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = true)
	private List<EmailRuleToProcess> emailRuleToProcess;

	@OneToMany(mappedBy = "emailRule", cascade = { CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = true)
	private List<EmailRuleCriteria> emailRuleCiteria;

	@OneToMany(mappedBy = "emailRule", cascade = { CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = true)
	private List<EmailRuleSchedule> emailRuleSchedules;

	@Column(name = "date_created")
	private Timestamp dateCreated;

	public EmailRule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getAllParticipants() {
		return this.allParticipants;
	}

	public void setAllParticipants(Boolean allParticipants) {
		this.allParticipants = allParticipants;
	}

	public Boolean getExcludedParticipants() {
		return this.excludedParticipants;
	}

	public void setExcludedParticipants(Boolean excludedParticipants) {
		this.excludedParticipants = excludedParticipants;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EmailRuleParticipant> getEmailRuleParticipants() {
		return this.emailRuleParticipants;
	}

	public void setEmailRuleParticipants(List<EmailRuleParticipant> emailRuleParticipants) {
		this.emailRuleParticipants = emailRuleParticipants;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ScheduledEmailDef getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(ScheduledEmailDef emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<EmailRuleCriteria> getEmailRuleCiteria() {
		return emailRuleCiteria;
	}

	public void setEmailRuleCiteria(List<EmailRuleCriteria> emailRuleCiteria) {
		this.emailRuleCiteria = emailRuleCiteria;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<EmailRuleToProcess> getEmailRuleToProcess() {
		return emailRuleToProcess;
	}

	public void setEmailRuleToProcess(List<EmailRuleToProcess> emailRuleToProcess) {
		this.emailRuleToProcess = emailRuleToProcess;
	}

	public List<EmailRuleSchedule> getEmailRuleSchedules() {
		return emailRuleSchedules;
	}

	public void setEmailRuleSchedules(List<EmailRuleSchedule> emailRuleSchedules) {
		this.emailRuleSchedules = emailRuleSchedules;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on Email rule ({}) in project: {}({})", new Object[] { user,
						EntityAuditLogger.OP_CREATE_EMAIL_RULE, getName(), getProject().getProjectId(),
						getProject().getName() });
			} else {
				auditLog.info("{} performed {} operation on Email rule ({}) in project: {}({})", new Object[] { user,
						operation, getName(), getProject().getProjectId(), getProject().getName() });
			}
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			if (operation.isEmpty()) {
				auditLog.info("{} performed {} operation on Email rule ({}) in project: {}({})", new Object[] { user,
						EntityAuditLogger.OP_UPDATE_EMAIL_RULE, getName(), getProject().getProjectId(),
						getProject().getName() });
			} else {
				auditLog.info("{} performed {} operation on Email rule ({}) in project: {}({})", new Object[] { user,
						operation, getName(), getProject().getProjectId(), getProject().getName() });
			}
			break;
		case EntityAuditLogger.DELETE_ACTION:

			auditLog.info("{} performed {} operation on Email rule: {}({}) in project: {}({})", new Object[] { user,
					EntityAuditLogger.OP_DELETE_EMAIL_RULE, getId(), getName(), getProject().getProjectId(),
					getProject().getName() });
			break;
		}

	}

}