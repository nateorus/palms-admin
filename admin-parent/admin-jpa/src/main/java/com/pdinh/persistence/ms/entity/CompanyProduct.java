package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;

/**
 * The persistent class for the company_product database table.
 * 
 */
@Entity
@Table(name = "company_product")

@NamedQueries({
	@NamedQuery(name = "findCompanyProductsByProductCode", query = "Select cp from CompanyProduct cp where cp.productCode = :productCode order by cp.company.coName"),
	@NamedQuery(name = "findByCompanyIdAndProductCode", query = "Select cp from CompanyProduct cp where cp.productCode = :productCode and cp.company.companyId = :companyId")})
public class CompanyProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@Column(name = "product_code")
	private String productCode;
	
	@OneToMany(cascade = {CascadeType.PERSIST}, mappedBy = "companyProduct", orphanRemoval = true)
	private List<CompanyProductMetadata> companyProductMetadata;

	public CompanyProduct() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}

}