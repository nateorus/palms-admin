package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

import com.pdinh.persistence.audit.EntityAuditListener;
import com.pdinh.persistence.audit.EntityAuditLogger;

/**
 * The persistent class for the project_user database table.
 * 
 */
@Entity
@EntityListeners(value = EntityAuditListener.class)
@Table(name = "project_user")
@NamedQueries({
		@NamedQuery(name = "findByProjectAndUser", query = "Select pu from ProjectUser pu where pu.project.projectId = :projectId and pu.user.usersId = :userId"),
		@NamedQuery(name = "findProjectUsersFiltered", query = "SELECT pu FROM ProjectUser pu WHERE pu.project.projectId = :projectId and pu.user.usersId IN :usersIds", hints = {}),
		@NamedQuery(name = "findProjectUsersByIds", query = "SELECT pu FROM ProjectUser pu WHERE pu.project.projectId = :projectId and pu.rowId IN :projectUserIds", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "pu.user.positions.course") }),
		@NamedQuery(name = "findProjectUsersByProjectId", query = "SELECT pu FROM ProjectUser pu WHERE pu.project.projectId = :projectId", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "pu.user.positions.course") }),
		@NamedQuery(name = "findProjectUsersByUserId", query = "SELECT pu FROM ProjectUser pu WHERE pu.user.usersId = :userId ORDER BY pu.dateAdded DESC", hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "pu.user.positions.course") }) })
public class ProjectUser implements EntityAuditLogger, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "row_id")
	private int rowId;

	@Column(name = "date_added", insertable = false)
	private Timestamp dateAdded;

	// bi-directional many-to-one association to Project
	// @ManyToOne(cascade = { CascadeType.MERGE })
	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	@Column(name = "project_content_completion_status")
	private int projectContentCompletionStatus;

	@Column(name = "added_by")
	private String addedBy;

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public ProjectUser() {
	}

	public int getRowId() {
		return this.rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public Timestamp getDateAdded() {
		return this.dateAdded;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void doAudit(String user, int action, String operation) {
		switch (action) {
		case EntityAuditLogger.CREATE_ACTION:
			auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user,
					EntityAuditLogger.OP_ADD_USER_TO_PROJECT, getUser().getUsersId(), getUser().getUsername(),
					getUser().getFirstname(), getUser().getLastname(), getUser().getEmail() });
			break;
		case EntityAuditLogger.UPDATE_ACTION:
			auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user,
					EntityAuditLogger.OP_UPDATE_PROJECT_USER, getUser().getUsersId(), getUser().getUsername(),
					getUser().getFirstname(), getUser().getLastname(), getUser().getEmail() });
			break;
		case EntityAuditLogger.DELETE_ACTION:
			auditLog.info("{} performed {} operation on user: {}({}), {}, {}, {}", new Object[] { user,
					EntityAuditLogger.OP_REMOVE_USER_FROM_PROJECT, getUser().getUsersId(), getUser().getUsername(),
					getUser().getFirstname(), getUser().getLastname(), getUser().getEmail() });

			break;
		}

	}

	public int getProjectContentCompletionStatus() {
		return projectContentCompletionStatus;
	}

	public void setProjectContentCompletionStatus(int projectContentCompletionStatus) {
		this.projectContentCompletionStatus = projectContentCompletionStatus;
	}

}