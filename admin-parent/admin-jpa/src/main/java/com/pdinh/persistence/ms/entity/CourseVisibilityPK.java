package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the course_visibility database table.
 * 
 */
@Embeddable
public class CourseVisibilityPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="company_id",insertable=false, updatable=false)
	private int companyId;

	@Column(name="course_id",insertable=false, updatable=false)
	private int courseId;

    public CourseVisibilityPK() {
    }
	public int getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getCourseId() {
		return this.courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CourseVisibilityPK)) {
			return false;
		}
		CourseVisibilityPK castOther = (CourseVisibilityPK)other;
		return 
			(this.companyId == castOther.companyId)
			&& (this.courseId == castOther.courseId);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.companyId;
		hash = hash * prime + this.courseId;
		
		return hash;
    }
}