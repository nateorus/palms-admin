package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the email_rule_participants database table.
 * 
 */
@Entity
@Table(name="email_rule_participants")
public class EmailRuleParticipant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private Boolean included;

	@OneToOne	
	@JoinColumn(name = "user_id")	
	private User user;

	//bi-directional many-to-one association to EmailRule
    @ManyToOne
	@JoinColumn(name="email_rule_id")
	private EmailRule emailRule;

    public EmailRuleParticipant() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIncluded() {
		return this.included;
	}

	public void setIncluded(Boolean included) {
		this.included = included;
	}

	public EmailRule getEmailRule() {
		return this.emailRule;
	}

	public void setEmailRule(EmailRule emailRule) {
		this.emailRule = emailRule;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}