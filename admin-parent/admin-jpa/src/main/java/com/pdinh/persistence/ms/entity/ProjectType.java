package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * The persistent class for the project_type database table.
 * 
 */
@Entity
@Table(name = "project_type")
@NamedQueries({
		@NamedQuery(name = "findAllByProductCode", query = "Select pt from ProjectType pt where pt.productTypeCode = :productCode"),
		@NamedQuery(name = "getProjectTypeFromProjectId", query = "SELECT pt FROM Project p, ProjectType pt WHERE p.projectType.projectTypeId = pt.projectTypeId AND p.projectId =  :projectId"),
		@NamedQuery(name = "findProjectTypeByCode", query = "SELECT pt FROM ProjectType pt WHERE pt.projectTypeCode = :projectTypeCode"),
		@NamedQuery(name = "findAllByCompanyId", query = "SELECT pt FROM ProjectType pt WHERE pt.productTypeCode IN (SELECT cp.productCode FROM CompanyProduct cp where cp.company.companyId = :companyId) OR pt.projectTypeId IN (SELECT cpt.projectType.projectTypeId FROM CompanyProjectType cpt where cpt.company.companyId = :companyId) order by pt.name"),
		@NamedQuery(name = "findTinCanProjectTypesByCompanyId", query = "SELECT DISTINCT pt FROM ProjectType pt, CourseGroupTemplate cgt WHERE ( pt.productTypeCode IN (SELECT cp.productCode FROM CompanyProduct cp where cp.company.companyId = :companyId) OR pt.projectTypeId IN (SELECT cpt.projectType.projectTypeId FROM CompanyProjectType cpt where cpt.company.companyId = :companyId)) AND pt.projectTypeId = cgt.projectType.projectTypeId AND cgt.course.course IN (SELECT c.course from Course c where c.tinCanUri IS NOT NULL) order by pt.name")

})
public class ProjectType implements Serializable {
	private static final long serialVersionUID = 1L;

	// TODO Re-do these constants based upon project type code rather than
	// project type id (involves moderate numbers of code changes as well)
	/*
	public static final int TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES = 1;
	public static final int TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES = 2;
	public static final int ASSESSMENT_BY_DESIGN_ONLINE = 3;
	public static final int ASSESSMENT_TESTING = 4;
	public static final int TARGET_FULL_LP = 5;
	public static final int TARGET_EXTENDED_LP = 6;
	public static final int TARGET_ASIA_INDIA_LP = 7;
	public static final int TARGET_CANADA_LP = 8;
	public static final int TARGET_FORENSIC = 9;
	public static final int SHELL_ELECTRONIC_INBOX_LOT = 10;
	public static final int SHELL_ELECTRONIC_INBOX_LOC = 11;
	public static final int MLL_LVA = 12; // Another name for the GL-GPS
	public static final int COACH = 13;
	public static final int TLTDEMO = 14;
	public static final int VIAEDGE = 15;
	public static final int VIAEDGE_TLT = 16;
	public static final int TLT_LAG = 17;
	public static final int TLT_LAG_COG = 18;
	public static final int READINESS_ASSMT_BUL = 19;
	*/
	public static final String TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES = "TLT_COG";
	public static final String TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES = "TLT";
	public static final String ASSESSMENT_BY_DESIGN_ONLINE = "ABYD";
	public static final String ASSESSMENT_TESTING = "ASMT";
	public static final String TARGET_FULL_LP = "TARGET_FULL_LP";
	public static final String TARGET_EXTENDED_LP = "TARGET_EXT_LP";
	public static final String TARGET_ASIA_INDIA_LP = "TARGET_ASIA_INDIA_LP";
	public static final String TARGET_CANADA_LP = "TARGET_CANADA_LP";
	public static final String TARGET_FORENSIC = "TARGET_FORENSIC";
	public static final String SHELL_ELECTRONIC_INBOX_LOT = "SLOT";
	public static final String SHELL_ELECTRONIC_INBOX_LOC = "SLOC";
	public static final String GLGPS = "GL_GPS"; // Another name for the GL-GPS
	public static final String COACH = "COACH";
	public static final String TLTDEMO = "TLTDEMO";
	public static final String VIAEDGE = "VIAEDGE";
	public static final String VIAEDGE_TLT = "VIAEDGE_TLT";
	public static final String READINESS_ASSMT_BUL = "RED_ASMT";
	public static final String KF_ASSMT_POTENTIAL = "KFAP";
	public static final String ERICSSON = "ERICSSON";

	// ------------------------------------------------------------------------
	// Per PA-421, TLT w/ Learning Agility has been deprecated. It no longer
	// appears in the selection list, but the code and constants that reference
	// those project types remain so that existing projects of these types wil
	// continue to work correctly.
	public static final String TLT_LAG = "TLT_LAG";
	public static final String TLT_LAG_COG = "TLT_LAG_COG";
	// ------------------ End of deprecated items -----------------------------

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_type_id")
	private int projectTypeId;

	@Lob()
	private String description;

	private String name;

	// Column(name = "product_type_code") // Not yet... maybe someday
	@Column(name = "code")
	private String productTypeCode;

	@Column(name = "project_type_code")
	private String projectTypeCode;

	@Column(name = "target_level_visible")
	private Boolean targetLevelVisible;

	@Column(name = "url_generator_visible")
	private boolean urlGeneratorVisible;

	@Column(name = "report_content_manifest_ref")
	private String reportContentManifestRef;

	@ManyToOne
	@JoinColumn(name = "target_level_group_id")
	private TargetLevelGroup targetLevelGroup;

	// bi-directional many-to-one association to Project
	@OneToMany(mappedBy = "projectType")
	private List<Project> projects;

	// bi-directional many-to-one association to CourseGroupTemplate
	@OneToMany(mappedBy = "projectType")
	private List<CourseGroupTemplate> courseGroupTemplates;

	// bi-directional many-to-one association to ProjectTypeReportType
	@OneToMany(mappedBy = "projectType")
	@OrderBy("sequenceOrder ASC")
	private List<ProjectTypeReportType> projectTypeReportTypes;

	@Column(name = "allow_report_download_visible")
	private Boolean allowReportDownloadVisible;

	@Column(name = "configuration_type_visible")
	private Boolean configurationTypeVisible;

	@Column(name = "scoring_method_visible")
	private Boolean scoringMethodVisible;

	@Column(name = "billable_visible")
	private Boolean billableVisible;

	public ProjectType() {
	}

	public int getProjectTypeId() {
		return this.projectTypeId;
	}

	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public void setCourseGroupTemplates(List<CourseGroupTemplate> courseGroupTemplates) {
		this.courseGroupTemplates = courseGroupTemplates;
	}

	public List<CourseGroupTemplate> getCourseGroupTemplates() {
		return courseGroupTemplates;
	}

	public List<ProjectTypeReportType> getProjectTypeReportTypes() {
		return projectTypeReportTypes;
	}

	public void setProjectTypeReportTypes(List<ProjectTypeReportType> projectTypeReportTypes) {
		this.projectTypeReportTypes = projectTypeReportTypes;
	}

	public void setProjectTypeCode(String projectTypeCode) {
		this.projectTypeCode = projectTypeCode;
	}

	public String getProjectTypeCode() {
		return projectTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setTargetLevelVisible(Boolean value) {
		this.targetLevelVisible = value;
	}

	public Boolean getTargetLevelVisible() {
		return this.targetLevelVisible;
	}

	public void setTargetLevelGroup(TargetLevelGroup value) {
		this.targetLevelGroup = value;
	}

	public TargetLevelGroup getTargetLevelGroup() {
		return this.targetLevelGroup;
	}

	public void setUrlGeneratorVisible(boolean value) {
		this.urlGeneratorVisible = value;
	}

	public boolean getUrlGeneratorVisible() {
		return this.urlGeneratorVisible;
	}

	public String getReportContentManifestRef() {
		return reportContentManifestRef;
	}

	public void setReportContentManifestRef(String reportContentManifestRef) {
		this.reportContentManifestRef = reportContentManifestRef;
	}

	public Boolean getAllowReportDownloadVisible() {
		return allowReportDownloadVisible;
	}

	public void setAllowReportDownloadVisible(Boolean allowReportDownloadVisible) {
		this.allowReportDownloadVisible = allowReportDownloadVisible;
	}

	public Boolean getConfigurationTypeVisible() {
		return configurationTypeVisible;
	}

	public void setConfigurationTypeVisible(Boolean configurationTypeVisible) {
		this.configurationTypeVisible = configurationTypeVisible;
	}

	public Boolean getScoringMethodVisible() {
		return scoringMethodVisible;
	}

	public void setScoringMethodVisible(Boolean scoringMethodVisible) {
		this.scoringMethodVisible = scoringMethodVisible;
	}

	public Boolean getBillableVisible() {
		return billableVisible;
	}

	public void setBillableVisible(Boolean billableVisible) {
		this.billableVisible = billableVisible;
	}
}