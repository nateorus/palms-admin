package com.pdinh.persistence.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface EntityAuditLogger {
	public static final Logger auditLog = LoggerFactory.getLogger("AUDIT-ADMIN");

	public static final int CREATE_ACTION = 1;
	public static final int UPDATE_ACTION = 2;
	public static final int DELETE_ACTION = 3;

	public static final String OP_CREATE_USER = "CREATE_USER";
	public static final String OP_UPDATE_USER = "UPDATE_USER";
	public static final String OP_ADD_USER_TO_PROJECT = "ADD_USER_TO_PROJECT";
	public static final String OP_REMOVE_USER_FROM_PROJECT = "REMOVE_USER_FROM_PROJECT";
	public static final String OP_UPDATE_PROJECT_USER = "UPDATE_PROJECT_USER";
	public static final String OP_CREATE_USER_DOCUMENT = "CREATE_USER_DOCUMENT";
	public static final String OP_UPDATE_USER_DOCUMENT = "UPDATE_USER_DOCUMENT";
	public static final String OP_REMOVE_USER_DOCUMENT = "REMOVE_USER_DOCUMENT";
	public static final String OP_DOWNLOAD_USER_DOCUMENT = "DOWNLOAD_USER_DOCUMENT";
	public static final String OP_RESET_PASSWORD = "RESET_PASSWORD";
	public static final String OP_DOWNLOAD_REPORT = "DOWNLOAD_REPORT";
	public static final String OP_EXPORT_STATUS = "EXPORT_STATUS";
	public static final String OP_ACCESS_TEST_DATA = "ACCESS_TEST_DATA";
	public static final String OP_ACCESS_URL_GENERATOR = "ACCESS_URL_GENERATOR";
	public static final String OP_ACCESS_ABYD_SETUP = "ACCESS_ABYD_SETUP";
	public static final String OP_ACCESS_LRM_SETUP = "ACCESS_LRM_SETUP";
	public static final String OP_ACCESS_LRM_PROJECT_MANAGMENT = "ACCESS_LRM_PROJECT_MANAGMENT";
	public static final String OP_ACCESS_LRM_CLIENT_LIST = "ACCESS_LRM_CLIENT_LIST";
	public static final String OP_ACCESS_PALMS_ADMIN_CLIENT_LIST = "ACCESS_PALMS_ADMIN_CLIENT_LIST";
	public static final String OP_ACCESS_AUTHORIZATION_SYSTEM = "ACCESS_AUTHORIZATION_SYSTEM";
	public static final String OP_ACCESS_PALMS_ADMIN_PRJ_MGMT_FROM_LRM = "ACCESS_PALMS_ADMIN_PRJ_MGMT_FROM_LRM";
	public static final String OP_ACCESS_PALMS_ADMIN_IMP_MGMT_FROM_LRM = "ACCESS_PALMS_ADMIN_IMP_MGMT_FROM_LRM";
	public static final String OP_CREATE_PROJECT = "CREATE_PROJECT";
	public static final String OP_UPDATE_PROJECT = "UPDATE_PROJECT";
	public static final String OP_CREATE_PROJECT_AND_ACCESS_LRM_SETUP = "CREATE_PROJECT_AND_ACCESS_LRM_SETUP";
	public static final String OP_UPDATE_PROJECT_AND_ACCESS_LRM_SETUP = "UPDATE_PROJECT_AND_ACCESS_LRM_SETUP";
	public static final String OP_ACTIVATE_INSTRUMENT = "ACTIVATE_INSTRUMENT";
	public static final String OP_DEACTIVATE_INSTRUMENT = "DEACTIVATE_INSTRUMENT";
	public static final String OP_RESET_INSTRUMENT = "RESET_INSTRUMENT";
	public static final String OP_SAVE_PROJECT_NORMS = "SAVE_PROJECT_NORMS";
	public static final String OP_SAVE_PARTICIPANT_NORMS = "SAVE_PARTICIPANT_NORMS";
	public static final String OP_CREATE_EMAIL_RULE = "CREATE_EMAIL_RULE";
	public static final String OP_UPDATE_EMAIL_RULE = "UPDATE_EMAIL_RULE";
	public static final String OP_DELETE_EMAIL_RULE = "DELETE_EMAIL_RULE";
	public static final String OP_CREATE_EMAIL_TEMPLATE = "CREATE_EMAIL_TEMPLATE";
	public static final String OP_UPDATE_EMAIL_TEMPLATE = "UPDATE_EMAIL_TEMPLATE";
	public static final String OP_DELETE_EMAIL_TEMPLATE = "DELETE_EMAIL_TEMPLATE";
	public static final String OP_LOGON_AS_CLIENT_ADMIN = "LOGON_AS_CLIENT_ADMIN";
	public static final String OP_CREATE_PASSWORD_POLICY = "CREATE_PASSWORD_POLICY";
	public static final String OP_UPDATE_PASSWORD_POLICY = "UPDATE_PASSWORD_POLICY";
	public static final String OP_DELETE_PASSWORD_POLICY = "DELETE_PASSWORD_POLICY";
	public static final String OP_LOGIN_PALMS_ADMIN = "LOGIN_PALMS_ADMIN";
	public static final String OP_BULK_USER_UPLOAD = "BULK_USER_UPLOAD";
	public static final String OP_CREATE_COACHING_PLAN = "CREATE_COACHING_PLAN";
	public static final String OP_UPDATE_COACHING_PLAN = "UPDATE_COACHING_PLAN";
	public static final String OP_DELETE_COACHING_PLAN = "DELETE_COACHING_PLAN";
	public static final String OP_CREATE_LRM_PROJECT = "CREATE_LRM_PROJECT";
	public static final String OP_UPDATE_LRM_PROJECT = "UPDATE_LRM_PROJECT";
	public static final String OP_CREATE_LRM_ASSESSMENT = "CREATE_LRM_ASSESSMENT";
	public static final String OP_UPDATE_LRM_ASSESSMENT = "UPDATE_LRM_ASSESSMENT";
	public static final String OP_LOGIN_LRM = "LOGIN_LRM";
	public static final String OP_LRM_ACCEPT_REQUEST = "LRM_ACCEPT_REQUEST";
	public static final String OP_LRM_CANCEL_REQUEST = "LRM_CANCEL_REQUEST";
	public static final String OP_LRM_UPDATE_TRACKER = "LRM_UPDATE_TRACKER";
	public static final String OP_LRM_SCHEDULE_REQUEST = "LRM_SCHEDULE_REQUEST";
	public static final String OP_LRM_UPDATE_PPT_DELIVERY = "LRM_UPDATE_PPT_DELIVERY";
	public static final String OP_CREATE_FIELD = "CREATE_FIELD";
	public static final String OP_UPDATE_FIELD = "UPDATE_FIELD";
	public static final String OP_DELETE_FIELD = "DELETE_FIELD";

	public static final String OP_ENABLE_PRODUCT = "ENABLE_PRODUCT";
	public static final String OP_DISABLE_PRODUCT = "DISABLE_PRODUCT";

	public static final String OP_IMPORT_CREATE_IMPORT_JOB = "CREATE_IMPORT_JOB";
	public static final String OP_IMPORT_UPLOAD_IMPORT_FILE = "UPLOAD_IMPORT_FILE";
	public static final String OP_IMPORT_COMMIT_IMPORT_JOB = "COMMIT_IMPORT_JOB";
	public static final String OP_IMPORT_CANCEL_IMPORT_JOB = "CANCEL_IMPORT_JOB";
	public static final String OP_IMPORT_CLOSE_IMPORT_JOB = "CLOSE_IMPORT_JOB";
	public static final String OP_IMPORT_OPEN_IMPORT_JOB = "OPEN_IMPORT_JOB";
	public static final String OP_IMPORT_DOWNLOAD_IMPORT_FILE = "DOWNLOAD_IMPORT_FILE";
	public static final String OP_IMPORT_GENERATE_IMPORT_FILE = "GENERATE_IMPORT_FILE";
	public static final String OP_CREATE_DELIVERY_REQUEST = "CREATE_DELIVERY_REQUEST";

	public static final String OP_REQUEST_REPORTS = "REQUEST_REPORTS";
	public static final String OP_UPDATE_REPORT_REQUEST = "UPDATE_REPORT_REQUEST";
	public static final String OP_DELETE_REPORT_REQUEST = "DELETE_REPORT_REQUEST";
	public static final String OP_CANCEL_REPORT_REQUEST = "CANCEL_REPORT_REQUEST";
	
	public static final String OP_UPDATE_SELF_REG_CONFIG = "UPDATE_SELF_REG_CONFIG";
	public static final String OP_CREATE_SELF_REG_CONFIG = "CREATE_SELF_REG_CONFIG";
	public static final String OP_SELF_REGISTER_PROJECT = "SELF_REGISTER_PROJECT";

	// public static final String DOWNLOAD_USER_DOCUMENT =
	// "DOWNLOAD_USER_DOCUMENT";

	public void doAudit(String user, int action, String operation);

}
