package com.pdinh.persistence.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class CompanyProductStatus implements Serializable {

	private static final long serialVersionUID = 5457664250045179831L;

	int productId;
	String productCode;
	boolean custom;
	String productStatus;
	boolean enabled;
	Timestamp expirationDate;
	String productName;

	public CompanyProductStatus(Integer productId, String productCode, Boolean custom,
			String productStatus, Boolean enabled, Timestamp expirationDate, String
			productName){
		this.productId = productId;
		this.productCode = productCode;
		this.custom = custom;
		this.productStatus = productStatus;
		this.enabled = enabled;
		this.expirationDate = expirationDate;
		this.productName = productName;
	}
	public CompanyProductStatus(){

	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public boolean isCustom() {
		return custom;
	}
	public void setCustom(boolean custom) {
		this.custom = custom;
	}
	public String getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public Timestamp getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Timestamp expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

}
