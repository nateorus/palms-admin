package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CompanyPasswordPolicy
 *
 */
@Entity
@Table(name="company_password_policy")
@NamedQueries({
	@NamedQuery(name = "findByCompanyId", query = "select cpp from CompanyPasswordPolicy cpp where cpp.company.companyId = :companyId")
})

public class CompanyPasswordPolicy implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "company_password_policy_id")
	private int companyPasswordPolicyId;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@Column(name = "user_type")
	private int userType;
	
	@ManyToOne
	@JoinColumn(name = "default_policy_id")
	private PasswordPolicy passwordPolicy;

	public CompanyPasswordPolicy() {
		super();
	}

	public int getCompanyPasswordPolicyId() {
		return companyPasswordPolicyId;
	}

	public void setCompanyPasswordPolicyId(int companyPasswordPolicyId) {
		this.companyPasswordPolicyId = companyPasswordPolicyId;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public PasswordPolicy getPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(PasswordPolicy passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
   
}
