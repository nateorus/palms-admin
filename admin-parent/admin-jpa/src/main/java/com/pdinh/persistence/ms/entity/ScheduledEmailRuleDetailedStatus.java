package com.pdinh.persistence.ms.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the report_type database table.
 * 
 */
@Entity
@Table(name="email_rule_detail_status")
//@NamedQuery(name = "findReportTypesByProjectTypeIds", query = "Select distinct ptrt.reportType from ProjectTypeReportType ptrt "
//		+ "where ptrt.projectType.projectTypeId in :projectTypeIds order by ptrt.reportType.name asc")


public class ScheduledEmailRuleDetailedStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private int emailRuleStatusDescriptionId;

	@Column(name = "email_rule_id")
	private String emailRuleStatusDescription;

	@Column(name = "status_desc_id")
	private String emailRuleStatusCode;
	
	public int getEmailRuleStatusDescriptionId(){
		return this.emailRuleStatusDescriptionId;
	}
	public void setEmailRuleStatusDescriptionId(int emailRuleStatusDescriptionId){
		this.emailRuleStatusDescriptionId = emailRuleStatusDescriptionId;
	}
	
	public String getEmailRuleStatusDescription(){
		return this.emailRuleStatusDescription;
	}
	public void setEmailRuleStatusDescription(String emailRuleStatusDescription){
		this.emailRuleStatusDescription = emailRuleStatusDescription;
	}
	
	public String getEmailRuleStatusCode(){
		return this.emailRuleStatusCode;
	}
	public void setEmailRuleStatusCode(String emailRuleStatusCode){
		this.emailRuleStatusCode = emailRuleStatusCode;
	}
}
