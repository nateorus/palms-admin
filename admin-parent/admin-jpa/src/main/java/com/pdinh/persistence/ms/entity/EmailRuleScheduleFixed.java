package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the email_rule_schedule_relative database table.
 * 
 */
@Entity
@Table(name="email_rule_schedule_fixed")
public class EmailRuleScheduleFixed extends EmailRuleSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="date")
	private Timestamp date;

    public EmailRuleScheduleFixed() {
    }

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}
	
}