package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the ScheduledEmailStatus database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "findScheduledEmailStatusByCompanyProjectUserEmailConstant", query = "SELECT ses FROM ScheduledEmailStatus ses WHERE ses.company.companyId = :companyId AND ses.project.projectId = :projectId AND ses.user.usersId = :userId AND ses.emailConstant = :emailConstant") })
public class ScheduledEmailStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIRST_PASSWORD = "FIRST_PASSWORD";
	public static final String PARTICIPANT_REMINDER = "PARTICIPANT_REMINDER";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "email_status_id")
	private int emailStatusId;

	@Column(name = "alt_email")
	private String altEmail;

	@Column(name = "date_completed")
	private Timestamp dateCompleted;

	@Column(name = "email_constant")
	private String emailConstant;

	@Column(name = "language_code")
	private String languageCode;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	// bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	// bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	// bi-directional many-to-one association to ScheduledEmailDef
	@ManyToOne
	@JoinColumn(name = "email_id")
	private ScheduledEmailDef scheduledEmailDef;

	public ScheduledEmailStatus() {
	}

	public int getEmailStatusId() {
		return this.emailStatusId;
	}

	public void setEmailStatusId(int emailStatusId) {
		this.emailStatusId = emailStatusId;
	}

	public String getAltEmail() {
		return this.altEmail;
	}

	public void setAltEmail(String altEmail) {
		this.altEmail = altEmail;
	}

	public Timestamp getDateCompleted() {
		return this.dateCompleted;
	}

	public void setDateCompleted(Timestamp dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public String getEmailConstant() {
		return this.emailConstant;
	}

	public void setEmailConstant(String emailConstant) {
		this.emailConstant = emailConstant;
	}

	public String getLanguageCode() {
		return this.languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public ScheduledEmailDef getScheduledEmailDef() {
		return this.scheduledEmailDef;
	}

	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}

}