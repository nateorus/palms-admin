package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;


/**
 * The persistent class for the ScheduledEmailRule database table.
 * 
 */


// NEED NAMED QUERIES:
//I�ll do named query and Dao method to �get all rules for this project.�
//And  a create() . 
@NamedQueries({

	@NamedQuery(name = "findAllEmailRulesForProject", query = "Select ser from ScheduledEmailRule ser where ser.project.projectId=:projectId", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
			@QueryHint(name = "eclipselink.batch", value = "ser.scheduledEmailRulesForProcessing") })
})
@Entity
public class ScheduledEmailRule implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int ruleId; // rule Id
	
	@Column(name = "name")
	private String name; // rule name
	@Column(name = "active")
	private int active;  //rule active
	
	//bi-directional many-to-one association to ScheduledEmailDef
    @ManyToOne
	@JoinColumn(name="email_id")
	private ScheduledEmailDef scheduledEmailDef;  // email template 
	
	@Column(name = "language")
	private String language;
	
	// list of the scheduledEmailRules - one for each fixed or relative date scheduled.
	// bi-directional many-to-one association to ScheduledEmailStatus
	@ManyToOne(targetEntity=com.pdinh.persistence.ms.entity.ScheduledEmailRuleForProcessing.class, cascade = { CascadeType.ALL })
	private List<ScheduledEmailRuleForProcessing> scheduledEmailRulesForProcessing;  
	
	//bi-directional many-to-one association to ScheduledEmailRule
    @ManyToOne
	@JoinColumn(name="project_id")
	private Project project;  // use this to get project end date
	

	public int getRuleId(){
		return this.ruleId;
	}
	public void setRuleId(int ruleId){
		this.ruleId = ruleId;
	}
	
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public int getActive(){
		return this.active;
	}
	public void setActive(int active){
		this.active = active;
	}
	
	public ScheduledEmailDef getScheduledEmailDef(){
		return this.scheduledEmailDef;
	}
	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef){
		this.scheduledEmailDef = scheduledEmailDef;
	}
	
	public String getLanguage(){
		return this.language;
	}
	public void setLanguage(String language){
		this.language = language;
	}
	
	public List<ScheduledEmailRuleForProcessing> getScheduledEmailRulesForProcessing(){
		return this.scheduledEmailRulesForProcessing;
	}
	public void setScheduledEmailRulesForProcessing(List<ScheduledEmailRuleForProcessing>  scheduledEmailRulesForProcessing){
		this.scheduledEmailRulesForProcessing = scheduledEmailRulesForProcessing;
	}
	
	public Project getProject(){
		return this.project;
	}
	public void setProject(Project project){
		this.project = project;
	}
}
