package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "project_report_content")
public class ProjectReportContent implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="project_report_content_id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "project_report_id")
	private ProjectReport projectReport;
	
	@Column(name = "course_id")
	private int course;
	
	@Column(name = "display_download_link")
	private boolean displayDownloadLink;
	
	@Column(name = "required")
	private boolean required;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ProjectReport getProjectReport() {
		return projectReport;
	}

	public void setProjectReport(ProjectReport projectReport) {
		this.projectReport = projectReport;
	}

	public int getCourse() {
		return course;
	}

	public void setCourse(int course) {
		this.course = course;
	}

	public boolean isDisplayDownloadLink() {
		return displayDownloadLink;
	}

	public void setDisplayDownloadLink(boolean displayDownloadLink) {
		this.displayDownloadLink = displayDownloadLink;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}


}
