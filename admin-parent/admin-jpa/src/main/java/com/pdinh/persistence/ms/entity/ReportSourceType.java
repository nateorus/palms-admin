package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "report_source_type")
public class ReportSourceType implements Serializable {
	private static final long serialVersionUID = 1L;

	// Reporting types - Values used in the table underlying this object
	public static final int PALMS = 1;
	public static final int OXCART = 2;
	public static final int COACHING_EXTRACT = 3;
	public static final int HYBRID = 4;

	@Id
	@Column(name = "report_source_type_id")
	private int reportSourceTypeId;

	private String name;

	@Column(name = "url_ref")
	private String urlRef;

	private String constant;

	@Column(name = "xml_param_name")
	private String xmlParamName;

	public int getReportSourceTypeId() {
		return reportSourceTypeId;
	}

	public void setReportSourceTypeId(int reportSourceTypeId) {
		this.reportSourceTypeId = reportSourceTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrlRef() {
		return urlRef;
	}

	public void setUrlRef(String urlRef) {
		this.urlRef = urlRef;
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(String constant) {
		this.constant = constant;
	}

	public String getXmlParamName() {
		return xmlParamName;
	}

	public void setXmlParamName(String xmlParamName) {
		this.xmlParamName = xmlParamName;
	}

}
