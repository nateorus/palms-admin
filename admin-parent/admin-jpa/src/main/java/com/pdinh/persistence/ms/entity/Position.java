package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.config.CacheIsolationType;

/**
 * The persistent class for the position database table.
 * 
 */
@Entity
@Table(name = "position")
@NamedQueries({
		@NamedQuery(name = "findPositionByUsersIdAndCourseAbbv", query = "SELECT p FROM Position p WHERE p.user.usersId = :usersId and p.user.active = 1 and p.course.abbv = :courseAbbv"),
		@NamedQuery(name = "findPositionByUsersIdAndCourseId", query = "SELECT p FROM Position p WHERE p.user.usersId = :usersId and p.user.active = 1 and p.course.course = :courseId"),
		/*
		@NamedQuery(name = "findPositionByCompletionStatusDateIfHasScorecard", query = "SELECT p FROM Position p WHERE p.user.active = 1 and NOT p.course.courseScorecard is NULL and p.completionStatusDate >= :completionStatusDate"),
		@NamedQuery(name = "findPositionIfHasScorecard", query = "SELECT p FROM Position p WHERE p.user.active = 1 and NOT p.course.courseScorecard is NULL", hints = {
				@QueryHint(name = QueryHints.BATCH_TYPE, value = "EXISTS"),
				@QueryHint(name = QueryHints.BATCH, value = "p.user"),
				@QueryHint(name = QueryHints.BATCH, value = "p.company"),
				@QueryHint(name = QueryHints.BATCH, value = "p.user.company"),
				@QueryHint(name = QueryHints.BATCH, value = "p.course"),
				@QueryHint(name = QueryHints.BATCH, value = "p.course.courseScorecard") }),
		*/
		@NamedQuery(name = "findMaximumPositionId", query = "SELECT DISTINCT p FROM Position p WHERE p.positionId = (select MAX(p1.positionId) FROM Position p1)")

		,
		/*
		@NamedQuery(name = "findPositionsByLastPositionId", query = "SELECT p FROM Position p WHERE p.user.active = 1 and NOT p.course.courseScorecard is NULL AND p.positionId > :positionId")

		,
		*/

		// Reworked the above query so that CourseScorecard was not required in
		// Course
		@NamedQuery(name = "findPositionsByLastPositionId", query = "SELECT p FROM Position p WHERE p.course.course IN (SELECT c.course.course FROM CourseScorecard c) AND p.user.active = 1 AND p.positionId > :positionId")

		,

		@NamedQuery(name = "findPositionsByUsersIdAndProjectId",
		// query="SELECT p FROM Position p WHERE p.user.usersId = :usersId AND p.course.course IN (SELECT pc FROM ProjectCourse pc WHERE pc.project.projectId = :projectId)")
		query = "SELECT p FROM Position p WHERE p.user.usersId = :usersId AND EXISTS (SELECT pc FROM ProjectCourse pc WHERE pc.project.projectId = :projectId AND pc.course.course = p.course.course AND pc.selected = 1)"),
		@NamedQuery(name = "findPositionsByCourseAbbv", query = "SELECT p FROM Position p WHERE p.user.active = 1 and p.course.abbv = :courseAbbv"),
		@NamedQuery(name = "findPositionsByUsersIdsAndCourseAbbvs", query = "SELECT p FROM Position p WHERE p.user.active = 1 and p.course.abbv IN :courseAbbvs and p.user.usersId IN :usersIds"),
		@NamedQuery(name = "findPositionsByUsersIds", query = "SELECT p FROM Position p WHERE p.user.active = 1 and p.user.usersId IN :usersIds")

})
@Cache(isolation = CacheIsolationType.ISOLATED)
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int NOT_STARTED = 0;
	public static final int INCOMPLETE = 1;
	public static final int COMPLETE = 2;

	@EmbeddedId
	private PositionPK id;

	@Column(name = "completion_status", insertable = false)
	private int completionStatus;

	@Column(name = "completion_status_date")
	private Timestamp completionStatusDate;

	@Column(name = "finished", insertable = false)
	private int finished;

	private short node;

	@Column(name = "percent_taken", insertable = false)
	private float percentTaken;

	@Column(name = "percent_taken_date")
	private Timestamp percentTakenDate;

	@Column(name = "rowguid", insertable = false, updatable = false)
	private String rowguid;

	private int score;

	private int section;

	@Column(name = "styleList", insertable = false, updatable = false)
	private String styleList;

	@Column(name = "updatetime", insertable = false, updatable = false)
	private Timestamp updatetime;

	@Column(name = "activated_flag")
	private Boolean activated;

	@Column(name = "position_id", insertable = false, updatable = false)
	private long positionId;

	// bi-directional many-to-one association to Course
	@MapsId("courseId")
	@ManyToOne
	@JoinColumn(name = "course")
	private Course course;

	// bi-directional many-to-one association to User
	@MapsId("usersId")
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	// bi-directional many-to-one association to Company
	@MapsId("companyId")
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	private boolean reset;

	public Position() {
	}

	@Override
	public String toString() {
		String ret = "Position: ";
		ret += "id=" + this.positionId + ", ";
		ret += "course=" + (this.course == null ? "Course is null" : this.course.getAbbv()) + ", ";
		ret += "copletionStatus=" + this.completionStatus;

		return ret;
	}

	public PositionPK getId() {
		return this.id;
	}

	public void setId(PositionPK id) {
		this.id = id;
	}

	public int getCompletionStatus() {
		return this.completionStatus;
	}

	public void setCompletionStatus(int completionStatus) {
		this.completionStatus = completionStatus;
	}

	public Timestamp getCompletionStatusDate() {
		return this.completionStatusDate;
	}

	public void setCompletionStatusDate(Timestamp completionStatusDate) {
		this.completionStatusDate = completionStatusDate;
	}

	public int getFinished() {
		return this.finished;
	}

	public void setFinished(int finished) {
		this.finished = finished;
	}

	public short getNode() {
		return this.node;
	}

	public void setNode(short node) {
		this.node = node;
	}

	public float getPercentTaken() {
		return this.percentTaken;
	}

	public void setPercentTaken(float percentTaken) {
		this.percentTaken = percentTaken;
	}

	public Timestamp getPercentTakenDate() {
		return this.percentTakenDate;
	}

	public void setPercentTakenDate(Timestamp percentTakenDate) {
		this.percentTakenDate = percentTakenDate;
	}

	public String getRowguid() {
		return this.rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public int getScore() {
		return this.score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getSection() {
		return this.section;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public String getStyleList() {
		return this.styleList;
	}

	public void setStyleList(String styleList) {
		this.styleList = styleList;
	}

	public Timestamp getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course courseBean) {
		this.course = courseBean;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Boolean getActivated() {
		return activated;
	}

	// ---------------------------------------
	public void setPositionId(long value) {
		this.positionId = value;
	}

	public long getPositionId() {
		return this.positionId;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

}