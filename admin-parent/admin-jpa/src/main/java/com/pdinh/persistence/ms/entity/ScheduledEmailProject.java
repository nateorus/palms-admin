package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ScheduledEmailProject database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "findScheduledEmailProjectByEmailId", query = "SELECT sep FROM ScheduledEmailProject sep WHERE sep.scheduledEmailDef.id=:emailId")
})
public class ScheduledEmailProject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

    @ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

    @ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
    
    @ManyToOne
	@JoinColumn(name="email_id")
	private ScheduledEmailDef scheduledEmailDef;

    public ScheduledEmailProject() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Project getProject() {
		return project;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}
	
	public void setScheduledEmailDef(ScheduledEmailDef scheduledEmailDef) {
		this.scheduledEmailDef = scheduledEmailDef;
	}

	public ScheduledEmailDef getScheduledEmailDef() {
		return scheduledEmailDef;
	}

}