package com.pdinh.persistence.ms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the course_association database table.
 * 
 */
@Entity
@Table(name="course_association")
public class CourseAssociation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_association_id")
	private int courseAssociationId;

	//bi-directional many-to-one association to Course
    @ManyToOne
	@JoinColumn(name="course_id")
	private Course course;

	//bi-directional many-to-one association to Course
    @ManyToOne
	@JoinColumn(name="associated_course_id")
	private Course associatedCourse;

	@Column(name="master_flag")
	private Boolean master;
	
    public CourseAssociation() {
    }

	public int getCourseAssociationId() {
		return this.courseAssociationId;
	}

	public void setCourseAssociationId(int courseAssociationId) {
		this.courseAssociationId = courseAssociationId;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Course getCourse() {
		return course;
	}

	public void setAssociatedCourse(Course associatedCourse) {
		this.associatedCourse = associatedCourse;
	}

	public Course getAssociatedCourse() {
		return associatedCourse;
	}

	public void setMaster(Boolean master) {
		this.master = master;
	}

	public Boolean getMaster() {
		return master;
	}
	
}