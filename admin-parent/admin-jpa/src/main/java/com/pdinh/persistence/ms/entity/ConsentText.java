package com.pdinh.persistence.ms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

/**
 * The persistent class for the consent_text database table.
 *
 */
@Entity
@Table(name = "consent_text")
@NamedQueries({
		@NamedQuery(name = "findForUserByTypeLangLocation", query = "SELECT ct FROM ConsentText ct WHERE ct.consentType = :consentType AND ct.lang = :lang AND ct.constant = :constant AND ct.consent IN (SELECT c FROM Consent c WHERE c.company.companyId = :companyId)", hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"),
				@QueryHint(name = "eclipselink.batch", value = "ct.consent") }),
		@NamedQuery(name = "findDefaultForTypeLangLocation", query = "SELECT ct FROM ConsentText ct WHERE ct.consentType = :consentType AND ct.lang = :lang AND ct.constant = :constant AND ct.consent.consentId = :consentId"),
		@NamedQuery(name = "findAllForType", query = "SELECT ct FROM ConsentText ct WHERE ct.consentType = :consentType ORDER BY ct.lang, ct.constant") })
public class ConsentText implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String ASSESSMENT = "ASSESSMENT";
	public static final String COACHING = "COACHING";
	public static final String ADMINISTRATION = "ADMINISTRATION";
	public static final String DEVELOPMENT = "DEVELOPMENT";
	public static final String GLOBAL = "GLOBAL";

	public ConsentText() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "consent_text_id")
	private int consentTextId;

	@ManyToOne
	@JoinColumn(name = "consent_id")
	private Consent consent;

	@Column(name = "lang")
	private String lang;

	@Column(name = "consent_text")
	private String consentText;

	@Column(name = "is_template")
	private int isTemplate;

	@Column(name = "constant")
	private String constant;

	@Column(name = "consent_type")
	private String consentType;

	public int getConsentTextId() {
		return consentTextId;
	}

	public void setConsentTextId(int consentTextId) {
		this.consentTextId = consentTextId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getConsentText() {
		return consentText;
	}

	public void setConsentText(String consentText) {
		this.consentText = consentText;
	}

	public int isTemplate() {
		return isTemplate;
	}

	public void setIsTemplate(int isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(String constant) {
		this.constant = constant;
	}

	public String getConsentType() {
		return consentType;
	}

	public void setConsentType(String consentType) {
		this.consentType = consentType;
	}

	public Consent getConsent() {
		return consent;
	}

	public void setConsent(Consent consent) {
		this.consent = consent;
	}

	// public int getConsentId() {
	// return consentId;
	// }
	//
	// public void setConsentId(int consentId) {
	// this.consentId = consentId;
	// }
}