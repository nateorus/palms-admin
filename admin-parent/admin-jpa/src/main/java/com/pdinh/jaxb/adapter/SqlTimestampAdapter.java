package com.pdinh.jaxb.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Timestamp;

public class SqlTimestampAdapter extends XmlAdapter<String,Timestamp>{

	@Override
	public Timestamp unmarshal(String v) throws Exception {		
		return Timestamp.valueOf(v);
	}

	@Override
	public String marshal(Timestamp timestamp) throws Exception {
		return timestamp.toString();
	}



}
