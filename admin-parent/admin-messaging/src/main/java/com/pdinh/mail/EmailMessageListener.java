package com.pdinh.mail;

import java.util.Calendar;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleScheduleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailLogDao;
import com.pdinh.data.ms.dao.ScheduledEmailLogStatusDao;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailLog;
import com.pdinh.persistence.ms.entity.ScheduledEmailLogStatus;

/**
 * Message-Driven Bean implementation class for: EmailMessageListener
 * 
 */
@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") }, mappedName = "jms/EmlMessageQueue")
public class EmailMessageListener implements MessageListener {

	@Resource(name = "mail/pdinh")
	private Session mailSession;

	@EJB
	private ScheduledEmailLogDao scheduledEmailLogDao;

	@EJB
	private ScheduledEmailLogStatusDao scheduledEmailLogStatusDao;

	@EJB
	private EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	private EmailRuleScheduleDao emailRuleScheduleDao;

	private static final Logger log = LoggerFactory.getLogger(EmailMessageListener.class);

	@Override
	public void onMessage(Message message) {
		log.debug("Recieved Message From Queue");
		if (message instanceof MapMessage) {
			MapMessage emailMessage = (MapMessage) message;

			try {
				if (emailMessage.itemExists("subject") && emailMessage.itemExists("toAddress")
						&& emailMessage.itemExists("plainTextMessage") && emailMessage.itemExists("htmlMessage")) {
					log.debug("found all required properties in message");
					String fromAddress = "";
					String replyToAddress = "";
					if (emailMessage.itemExists("fromAddress")) {
						fromAddress = emailMessage.getString("fromAddress");
					}
					if (emailMessage.itemExists("replyToAddress")) {
						replyToAddress = emailMessage.getString("replyToAddress");
					}
					if (sendMailMessage(emailMessage.getString("subject"), fromAddress, replyToAddress,
							emailMessage.getString("toAddress"), emailMessage.getString("htmlMessage"),
							emailMessage.getString("plainTextMessage"), emailMessage.getInt("emailLogId"))) {
						log.debug("SendMailMessage () returned true");
					} else {
						log.debug("sendMailMessage() returned false");
					}
				} else {
					log.debug("Required message property was found missing");
				}
			} catch (JMSException e) {
				log.error("JMSException occurred attempting to process email message: {}", e.getMessage());
				e.printStackTrace();
			}
		}

	}

	private Boolean sendMailMessage(String subject, String fromAddress, String replyToAddress, String toAddress,
			String htmlMessage, String plainTextMessage, Integer emailLogId) {
		MimeMessage msg = new MimeMessage(mailSession);
		Multipart multiPart = new MimeMultipart("alternative");
		ScheduledEmailLog emailLog = scheduledEmailLogDao.findById(emailLogId);
		try {
			log.debug("Attempting to send message to " + toAddress);

			MimeBodyPart text = new MimeBodyPart();
			MimeBodyPart html = new MimeBodyPart();

			msg.setSubject(subject, "utf-8");
			if (!fromAddress.equals("")) {
				msg.setFrom(new InternetAddress(fromAddress));
			}

			if (!replyToAddress.equals("")) {
				msg.setReplyTo(new InternetAddress[] { new InternetAddress(replyToAddress) });
			}
			msg.setRecipient(RecipientType.TO, new InternetAddress(toAddress));

			if (plainTextMessage != null) {
				text.setText(plainTextMessage, "utf-8");
				multiPart.addBodyPart(text);
			}

			if (htmlMessage != null) {
				html.setContent(htmlMessage, "text/html; charset=utf-8");
				multiPart.addBodyPart(html);
			}

			msg.setContent(multiPart);

			Transport.send(msg);

			log.debug("Sent message to {} successfully.", toAddress);

			ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
					ScheduledEmailLogStatus.STATUS_SUCCESSFUL, 1);
			emailLog.setStatus(status);
			emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
			scheduledEmailLogDao.update(emailLog);

			return true;
		} catch (MessagingException me) {
			log.error("There was MessagingException sending the email. {}", me.getMessage());
			me.printStackTrace();
			ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
					ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);
			emailLog.setStatus(status);
			emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
			emailLog.setErrorMessage(me.getMessage());
			scheduledEmailLogDao.update(emailLog);

			// Updating email rule schedule status and flagging an error.
			EmailRuleSchedule schedule = emailLog.getSchedule();
			if (schedule != null) {
				schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
						.findStatusByCode(EmailRuleScheduleStatus.ERRORS_ON_COMPLETE));
				schedule.setError(true);
				emailRuleScheduleDao.update(schedule);
			}

			// manage exception
			return false;
		} catch (Exception e) {
			log.error("There was an Exception sending the email. {}", e.getMessage());
			e.printStackTrace();
			ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
					ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);
			emailLog.setStatus(status);
			emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
			emailLog.setErrorMessage(e.getMessage());
			scheduledEmailLogDao.update(emailLog);

			// Updating email rule schedule status and flagging an error.
			EmailRuleSchedule schedule = emailLog.getSchedule();
			if (schedule != null) {
				schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
						.findStatusByCode(EmailRuleScheduleStatus.ERRORS_ON_COMPLETE));
				schedule.setError(true);
				emailRuleScheduleDao.update(schedule);
			}

			return false;
		}
	}

}
