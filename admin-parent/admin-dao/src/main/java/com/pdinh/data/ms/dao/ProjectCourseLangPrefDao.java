package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ProjectCourseLangPref;

/**
 * Session Bean implementation class ClientDao
 */
@Stateless
@LocalBean
public class ProjectCourseLangPrefDao extends GenericDao<ProjectCourseLangPref, Integer> {
	public ProjectCourseLangPref findByProjectAndCourse(int projectId, int courseId) {
		TypedQuery<ProjectCourseLangPref> pclp = getEntityManager().createNamedQuery("findByProjectAndCourse", ProjectCourseLangPref.class);
		pclp.setParameter("projectId", projectId);
		pclp.setParameter("courseId", courseId);
		try {
			return pclp.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
