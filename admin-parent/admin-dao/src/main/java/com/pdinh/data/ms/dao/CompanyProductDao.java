package com.pdinh.data.ms.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.dto.CompanyProductStatus;
import com.pdinh.persistence.ms.entity.AssessmentCourse;
import com.pdinh.persistence.ms.entity.CompanyProduct;

/**
 * Session Bean implementation class CompanyProductDao
 */
@Stateless
@LocalBean
public class CompanyProductDao extends GenericDao<CompanyProduct, Integer>  {

	private static Logger log = LoggerFactory.getLogger(CompanyProductDao.class);
	
    /**
     * Default constructor. 
     */
    public CompanyProductDao() {
        // TODO Auto-generated constructor stub
    }
    
    public List<CompanyProduct> findCompanyProductsByProductCode(String productCode) {
		log.debug("IN CompanyProductDao.findCompanyProductsByProductCode productCode: {}", productCode);
		TypedQuery<CompanyProduct> query = getEntityManager().createNamedQuery("findCompanyProductsByProductCode",
				CompanyProduct.class);

		query.setParameter("productCode", productCode);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}
    
    public List<CompanyProduct> findByCompanyIdAndProductCode(String productCode, int companyId){
    	TypedQuery<CompanyProduct> query = getEntityManager().createNamedQuery("findByCompanyIdAndProductCode", CompanyProduct.class);
    	query.setParameter("productCode", productCode);
    	query.setParameter("companyId", companyId);
    	try{
    		return query.getResultList();
    	} catch (NoResultException nre){
    		return null;
    	}
    }
    
    public List<CompanyProductStatus> getAllProductStatusForCompany(int companyId){
    	Query query = entityManager.createNativeQuery("get_product_status ?");
    	query.setParameter(1, companyId);
    	try {
			List<Object> objList = query.getResultList();
			List<CompanyProductStatus> cpsList = new ArrayList<CompanyProductStatus>();
			int n = 0;
			for (Object obj : objList) {
				Object[] objArray = (Object[]) obj;
				Boolean enabled = false;
				if (objArray[3].toString().equalsIgnoreCase("enabled")){
					enabled = true;
				}
				Boolean custom = false;
				if ((Integer)objArray[2] == 1){
					custom = true;
				}
				CompanyProductStatus cps = new CompanyProductStatus((Integer)objArray[0], (String)objArray[1], custom, (String)objArray[3], enabled, (Timestamp)objArray[4],(String)objArray[5]);
				
				cpsList.add(cps);
			}
			return cpsList;
		} catch (NoResultException e) {
			return null;
		}
    }

}

