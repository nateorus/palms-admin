package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pdinh.persistence.ms.entity.ScormCourseDirectory;

/**
 * Session Bean implementation class ScormCourseDirectoryDao
 */
@Stateless
@LocalBean
public class ScormCourseDirectoryDao extends GenericDao<ScormCourseDirectory, Integer> {
	
	public String getScormCourseIdByCourseId (String courseId) {

		//String scormCourseID = null;
		
	    Query scormId = getEntityManager().createNamedQuery("findScormCourseIdByCourseId");
	    scormId.setParameter("courseId", courseId);
	    return (String)scormId.getSingleResult();
//		List<String> typelist = (List<String>) listq.getResultList();
//		
//		
//		
//
//		Connection conn = DBUtil.getConnection();
//
//		String selectStatement = "select scorm_course_id from platform.dbo.scorm_course_directories where course_id = ?";
//		
//		PreparedStatement prepStmt = null;
//
//	    try {
//			prepStmt = conn.prepareStatement(selectStatement);
//			prepStmt.setString(1, courseID);
//			ResultSet rs;
//
//			rs = prepStmt.executeQuery();
//			
//			rs.next();
//			scormCourseID = rs.getString("scorm_course_id");
//	    	
//	    } catch (SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//	    } finally {
//			try {
//				prepStmt.close();
//			} catch (Exception e) {
//			
//			}
//	      
//			try {
//				conn.close();
//			} catch (Exception e) {
//		  
//			}
//	    }
//
//		return scormCourseID;
	}

}