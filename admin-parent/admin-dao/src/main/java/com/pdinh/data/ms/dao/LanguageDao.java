package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.Language;

/**
 * Session Bean implementation class LanguageDao
 */
@Stateless
@LocalBean
public class LanguageDao extends GenericDao<Language, Integer> {
	private static final Logger log = LoggerFactory.getLogger(LanguageDao.class);
	/**
	 * Default constructor.
	 */
	public LanguageDao() {
		// TODO Auto-generated constructor stub
	}

	/* JJB - If only want to show languages for assessments comment this out.
	@Override
	public List<Language> findAll() {
	    TypedQuery<Language> query = 
	        getEntityManager().createNamedQuery("findAllAssessmentLanguages",Language.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	*/
	public Language findByCode(String code) {
		String sql = "SELECT l FROM Language l where l.code = ?1";
		TypedQuery<Language> query = getEntityManager().createQuery(sql, Language.class);
		query.setParameter(1, code);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<Language> findByCourseAbbv(String abbv) {
		log.debug("IN LanguageDao.findByCourseAbbv code: {}", abbv);
		TypedQuery<Language> query = getEntityManager().createNamedQuery("findLanguagesByCourseAbbv", Language.class);
		query.setParameter("abbv", abbv);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<Language> findAllAssessmentLanguages() {
		TypedQuery<Language> query = getEntityManager().createNamedQuery("findAllAssessmentLanguages", Language.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
