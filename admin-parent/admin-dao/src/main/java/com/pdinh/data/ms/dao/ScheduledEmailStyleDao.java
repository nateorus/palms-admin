package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pdinh.persistence.ms.entity.ScheduledEmailDef;

@Stateless
@LocalBean
public class ScheduledEmailStyleDao extends GenericDao<ScheduledEmailDef, Integer> {

	public HashMap<String, String> getTemplateConstants() {
		String sql = "SELECT  ses.style_constant, ses.style_label FROM ScheduledEmailStyle ses";
		Query query = getEntityManager().createNativeQuery(sql);
		HashMap<String, String> map = new HashMap<String, String>();
		List<Object> list = new ArrayList<Object>();
		try {
			list = query.getResultList();

			int x = 0;
			for (int i = 0; i < list.size(); i++) {
				Object[] row = (Object[]) list.get(i);
				// System.out.println(" index: " + i + "  " +
				// Arrays.toString(row));
				if (row[0] == null || row[1] == null) {
					continue;
				}
				map.put((String) row[0], (String) row[1]);
				// System.out.println(" item 1,   item 2 " + row[0] + " ,   " +
				// row[1]);
			}

			return map;
		} catch (NoResultException nre) {
			return null;
		}
	}

	public String getConstantStyleLabelFromConstant(String constant) {
		String label = "";
		Query query = entityManager
				.createNativeQuery("SELECT style_label FROM scheduledEmailStyle WHERE style_constant = ? ");
		query.setParameter(1, constant);
		try {

			label = (String) query.getSingleResult();
			if (label.equals("") || label.equals(null)) {
				label = "Custom";
			}
			return label;

		} catch (NoResultException nre) {
			return "Custom";
		}
	}

}
