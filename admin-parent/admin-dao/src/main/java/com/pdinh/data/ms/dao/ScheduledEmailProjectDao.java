package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailProject;

@Stateless
@LocalBean
public class ScheduledEmailProjectDao extends GenericDao<ScheduledEmailProject, Integer> {
	
	public List<ScheduledEmailProject> findScheduledEmailProjectByEmailId(int emailId) {
		TypedQuery<ScheduledEmailProject> query = getEntityManager().createNamedQuery("findScheduledEmailProjectByEmailId",
				ScheduledEmailProject.class);
		query.setParameter("emailId", emailId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
}

