package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ScheduledEmailRule;
import com.pdinh.persistence.ms.entity.ScheduledEmailRuleForProcessing;

@Stateless
@LocalBean
public class ScheduledEmailRuleForProcessingDao extends GenericDao<ScheduledEmailRuleForProcessing, Integer> {

	public List<ScheduledEmailRuleForProcessing> findAllRulesForProcessing() {
		TypedQuery<ScheduledEmailRuleForProcessing> query = getEntityManager().createNamedQuery("findAllRulesForProcessing",
				ScheduledEmailRuleForProcessing.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	@EJB
	CompanyDao companyDao;
	@EJB
	ProjectDao projectDao;
	@EJB
	ScheduledEmailDefDao scheduledEmailDefDao;
	@EJB
	ScheduledEmailDefDao scheduledEmailTextDao;
	@EJB
	ScheduledEmailRuleDao scheduledEmailRuleDao;
	
	
	// create method
	public ScheduledEmailRuleForProcessing create(Integer companyId, Integer projectId, Integer scheduledEmailDefId, Integer scheduledEmailTextId, int emailRuleId) {
		ScheduledEmailRuleForProcessing scheduledEmailRule = new ScheduledEmailRuleForProcessing();
		scheduledEmailRule.setCompany(companyDao.findById(companyId));
		scheduledEmailRule.setProject(projectDao.findById(projectId));
		scheduledEmailRule.setScheduledEmailDef(scheduledEmailDefDao.findById(scheduledEmailDefId));
		scheduledEmailRule.setRuleId(emailRuleId);
		// also do the scheduled email rules display!!
		
		create(scheduledEmailRule);
		
		return scheduledEmailRule;
	}
}
