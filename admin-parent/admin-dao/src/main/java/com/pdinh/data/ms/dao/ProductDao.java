package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Product;

@Stateless
@LocalBean
public class ProductDao extends GenericDao<Product, Integer> {

	public ProductDao() {

	}

	public List<Product> findAssessmentProductsByCompanyId(int companyId) {
		TypedQuery<Product> product = getEntityManager().createNamedQuery("findAssessmentProductsByCompanyId",
				Product.class);
		product.setParameter("companyId", companyId);

		try {
			return product.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public Product findProductByCode(String code) {
		TypedQuery<Product> product = getEntityManager().createNamedQuery("findProductByCode", Product.class);
		product.setParameter("code", code);
		try {
			return product.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
