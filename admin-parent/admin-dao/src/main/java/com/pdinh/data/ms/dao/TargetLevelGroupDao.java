/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.TargetLevelGroup;


/*
 * TargetLevelGroupDao
 */
@Stateless
@LocalBean
public class TargetLevelGroupDao extends GenericDao<TargetLevelGroup, Integer>
{
	// No explicit implementation code at this point
}
