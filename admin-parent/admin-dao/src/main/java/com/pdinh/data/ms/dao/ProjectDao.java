package com.pdinh.data.ms.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseGroupTemplate;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourseLangPref;
import com.pdinh.persistence.ms.entity.ProjectUser;

/**
 * Session Bean implementation class ProjectDao
 */
@Stateless
@LocalBean
public class ProjectDao extends GenericDao<Project, Integer> {

	@EJB
	private LanguageDao languageDao;

	@EJB
	private ProjectCourseLangPrefDao projectCourseLanguagePrefDao;

	private static final Logger log = LoggerFactory.getLogger(ProjectDao.class);

	public Project findBatchById(Integer projectId) {
		log.debug("IN ProjectDao.findBatchById projectId: {}", projectId);
		TypedQuery<Project> projectquery = getEntityManager().createNamedQuery("getProjectBatchUsersPositions",
				Project.class);
		projectquery.setParameter("id", projectId);
		try {
			return projectquery.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<ProjectUser> findProjectParticipantsByProjectId(Integer projectId) {
		log.debug("IN ProjectDao.findProjectParticipantsByProjectId projectId: {}", projectId);
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findUsersInProjectByProjectId",
				ProjectUser.class);

		query.setParameter("projectId", projectId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<ProjectUser> findAuthorizedParticipantsByProjectId(Integer projectId, List<Integer> usersIds) {
		log.debug("IN ProjectDao.findAuthorizedParticipantsByProjectId projectId: {}", projectId);
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findProjectUsersFiltered",
				ProjectUser.class);
		// Avoids sql syntax exception with empty list
		if (usersIds.isEmpty()) {
			usersIds.add(0);
		}
		query.setParameter("projectId", projectId);
		query.setParameter("usersIds", usersIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/*
	 * public List<User> findProjectParticipantsByProjectId(Integer projectId) {
	 * System
	 * .out.println("IN ProjectDao.findProjectParticipantsByProjectId projectId: "
	 * + projectId); TypedQuery<User> query =
	 * getEntityManager().createNamedQuery
	 * ("findUsersInProjectByProjectId",User.class);
	 * query.setParameter("projectId", projectId); try { return
	 * query.getResultList(); } catch (NoResultException nre) { return null; }
	 *
	 * }
	 */
	public List<Project> findAllByCompanyId(Integer companyId) {
		log.debug("IN ProjectDao.findAllByCompanyId companyId: {}", companyId);

		TypedQuery<Project> query = getEntityManager().createNamedQuery("findProjectsByCompanyId", Project.class);
		query.setParameter("id", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<Project> findAuthorizedByCompanyId(Integer companyId, List<Integer> projectIds) {
		log.debug("IN ProjectDao.findAuthorizedByCompanyId companyId: {}, Authorized ids: {}", companyId, projectIds);
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findAuthorizedProjectsByCompanyId",
				Project.class);
		if (projectIds.isEmpty()) {
			// This prevents SQL exception on empty id list
			projectIds.add(0);
		}
		query.setParameter("id", companyId);
		query.setParameter("projectIds", projectIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<Project> findAllByUserId(Integer userId) {
		log.debug("IN ProjectDao.findAllByUserId userId: {}", userId);
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findProjectsByUserId", Project.class);
		query.setParameter("id", userId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<CourseGroupTemplate> findCourseGroupTemplatesByProjectTypeId(Integer projectTypeId) {
		log.debug("IN ProjectDao.findCourseGroupTemplatesByProjectTypeId projectTypeId: {}", projectTypeId);
		TypedQuery<CourseGroupTemplate> query = getEntityManager().createNamedQuery(
				"findAllCourseGroupTemplateByProjectTypeId", CourseGroupTemplate.class);
		query.setParameter("projectTypeId", projectTypeId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public String resolveTargetLevel(Project project) {
		String retval = "n/a";
		if (project.getTargetLevelType() != null) {
			retval = project.getTargetLevelType().getCode();
		}
		return retval;
	}

	public List<String> findDistinctProjectTypes(Integer companyId) {
		log.debug("IN ProjectDao.findDistinctProjectTypes companyId: {}", companyId);
		Query listq = getEntityManager().createNamedQuery("findDistinctProjectTypes");
		listq.setParameter("id", companyId);
		List<String> typelist = listq.getResultList();
		return typelist;
	}

	public List<Project> findAuthorizedProjects(List<Integer> projectIds) {
		log.debug("IN ProjectDao.findAuthorizedProjects Authorized ids: {}", projectIds);
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findAuthorizedProjects", Project.class);
		if (projectIds.isEmpty()) {
			// This prevents SQL exception on empty id list
			projectIds.add(0);
		}
		query.setParameter("projectIds", projectIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<Project> findAuthorizedProjectsInCompanies(List<Integer> projectIds, List<Integer> companyIds) {
		log.debug("IN ProjectDao.findAuthorizedProjectsInCompanies Authorized project ids: {}, companies {}", projectIds, companyIds);
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findAuthorizedProjectsInCompanies", Project.class);
		if (projectIds.isEmpty()) {
			// This prevents SQL exception on empty id list
			projectIds.add(0);
		}
		if (companyIds.isEmpty()){
			companyIds.add(0);
		}
		query.setParameter("projectIds", projectIds);
		query.setParameter("companyIds", companyIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public List<Project> findAuthorizedProjectsInCompany(List<Integer> projectIds, int companyId) {
		log.debug("IN ProjectDao.findAuthorizedProjectsInCompany Authorized project ids: {}, company {}", projectIds, companyId);
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findAuthorizedProjectsInCompany", Project.class);
		if (projectIds.isEmpty()) {
			// This prevents SQL exception on empty id list
			projectIds.add(0);
		}

		query.setParameter("projectIds", projectIds);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	@Override
	public Project create(Project project) {
		UUID guid = UUID.randomUUID();
		String rowguid = guid.toString();
		// Date sqlDate = java.sql.Date.valueOf("2011-07-20");
		Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());

		project.setRowguid(rowguid);
		project.setDateCreated(sqlDate);
		project.setActive(1);

		getEntityManager().persist(project);

		return project;
	}

	public ProjectCourseLangPref setProjectCourseLanguage(Project project, Course course, String langCode) {

		TypedQuery<ProjectCourseLangPref> query = getEntityManager().createNamedQuery("findByProjectAndCourse",
				ProjectCourseLangPref.class);
		query.setParameter("projectId", project.getProjectId());
		query.setParameter("courseId", course.getCourse());
		ProjectCourseLangPref pclp = null;
		try {
			pclp = query.getSingleResult();
		} catch (NoResultException nre) {
			pclp = null;
		}
		if (pclp == null) {
			log.debug("Project course lang pref not found, create");
			pclp = new ProjectCourseLangPref();
			pclp.setProject(project);;
			pclp.setCourse(course);
		} else {
			log.debug("found Project Course lang pref: {}", pclp.getLanguage().getCode());
		}

		Language language = languageDao.findByCode(langCode);

		if (language != null) {
			pclp.setLanguage(language);
			getEntityManager().persist(pclp);
			log.debug("updated language to {}", language.getCode());
		} else {
			log.debug("language code {} isn't supported, project course pref not updated for project: ", langCode);
			pclp = null;
		}

		return pclp;

	}

	public String findChqLanguageCodeForProject(int projectId) {
		ProjectCourseLangPref pclp = projectCourseLanguagePrefDao.findByProjectAndCourse(projectId, Course.CHQ_ID);
		if (pclp != null) {
			return pclp.getLanguage().getCode();
		}else{
			return null;
		}
	}

	public List<Project> findProjectsClonedFromProject(int parentProjectId) {
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findProjectsClonedFromProject", Project.class);
		query.setParameter("parentProjectId", parentProjectId);
		return query.getResultList();
	}

	public List<Project> findTinCanProjectsByCompanyId(int companyId) {
		
		TypedQuery<Project> query = getEntityManager().createNamedQuery("findTinCanProjectsByCompanyId", Project.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
		
	}
}
