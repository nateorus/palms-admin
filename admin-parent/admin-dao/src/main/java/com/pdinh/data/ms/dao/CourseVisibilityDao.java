package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CourseScorecard;
import com.pdinh.persistence.ms.entity.CourseVisibility;

@Stateless
@LocalBean
public class CourseVisibilityDao extends GenericDao<CourseVisibility, Integer> {

	public CourseVisibility findByCompanyIdAndCourseId(int companyId, int courseId){
		
	    	
    	TypedQuery<CourseVisibility> query = entityManager.createNamedQuery("findByCompanyIdAndCourseId",CourseVisibility.class);
		query.setParameter("courseId", courseId);
		query.setParameter("companyId", companyId);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	    
	}
	
	public void deleteByCompanyIdAndCourseId(int companyId, int courseId){
		TypedQuery<CourseVisibility> query = entityManager.createNamedQuery("deleteByCompanyIdAndCourseId",CourseVisibility.class);
		query.setParameter("courseId", courseId);
		query.setParameter("companyId", companyId);
		
		try {
			query.executeUpdate();
			return;
		} catch (NoResultException nre){
			return;
		}
	}
	
	public void deleteByCompanyIdAndCourseIds(int companyId, List<Integer> courseIds){
		TypedQuery<CourseVisibility> query = entityManager.createNamedQuery("deleteByCompanyIdAndCourseIds",CourseVisibility.class);
		if (courseIds.isEmpty()){
			//courseIds.add(0);
			return;
		}
		query.setParameter("courseIds", courseIds);
		query.setParameter("companyId", companyId);
		
		try {
			query.executeUpdate();
			return;
		} catch (NoResultException nre){
			return;
		}
	}
	
	public List<CourseVisibility> findByCompanyBatchCourses(int companyId){
		TypedQuery<CourseVisibility> query = entityManager.createNamedQuery("findByCompanyBatchCourses",CourseVisibility.class);
		query.setParameter("companyId", companyId);
		try{
			return query.getResultList();
		}catch (NoResultException nre){
			return null;
		}
	}
	
	public void createBatch(List<CourseVisibility> visibilities){
		for (CourseVisibility cv : visibilities){
			this.create(cv);
		}
	}
}
