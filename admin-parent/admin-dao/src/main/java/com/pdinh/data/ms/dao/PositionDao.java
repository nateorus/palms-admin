package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Position;

/**
 * Session Bean implementation class ProjectUserDao
 */
@Stateless
@LocalBean
public class PositionDao extends GenericDao<Position, Integer> {

	/**
	 * Default constructor.
	 */
	public PositionDao() {
		// TODO Auto-generated constructor stub
	}

	public Position findByUsersIdAndCourseAbbv(int usersId, String courseAbbv) {

		TypedQuery<Position> t = entityManager.createNamedQuery("findPositionByUsersIdAndCourseAbbv", Position.class);
		t.setParameter("usersId", usersId);
		t.setParameter("courseAbbv", courseAbbv);

		try {
			return t.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * findByUsersIdAndCourseId -  Use a named query to get a Position object for a specific user and course
	 */
	public Position findByUsersIdAndCourseId(int usersId, int courseId) {
		TypedQuery<Position> t = entityManager.createNamedQuery("findPositionByUsersIdAndCourseId", Position.class);
		t.setParameter("usersId", usersId);
		t.setParameter("courseId", courseId);

		try {
			return t.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
		public List<Position> findByCompletionStatusAndCompletionStatusDate(Timestamp completionStatusDate) {
			// Ignoring ts for now
			TypedQuery<Position> t = getEntityManager().createNamedQuery("findPositionIfHasScorecard", Position.class);
			t.setParameter("completionStatusDate", completionStatusDate);

			try {
				return t.getResultList();
			} catch (NoResultException nre) {
				return null;
			}
		}

		public List<Position> findIfHasScorecard() {
			TypedQuery<Position> t = getEntityManager().createNamedQuery("findPositionIfHasScorecard", Position.class);

			try {
				return t.getResultList();
			} catch (NoResultException nre) {
				return null;
			}
		}
	*/
	public Position findMaximumPositionId() {

		TypedQuery<Position> t = entityManager.createNamedQuery("findMaximumPositionId", Position.class);

		try {
			return t.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	// Required in the SynchronizerBean
	public List<Position> findPositionsByLastPositionId(int positionId) {
		TypedQuery<Position> t = getEntityManager().createNamedQuery("findPositionsByLastPositionId", Position.class);
		t.setParameter("positionId", positionId);

		try {
			return t.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/*
	 * findByUsersIdAndProjectId -  Use a named query to get a list of Position object for a specific user and project
	 * 
	 * @param usersId - The participant ID to look for
	 * @param projectId - The project ID to look for
	 * @return A List of Position objects
	 */
	public List<Position> findPositionsByUsersIdAndProjectId(int usersId, int projectId) {
		TypedQuery<Position> t = entityManager.createNamedQuery("findPositionsByUsersIdAndProjectId", Position.class);
		t.setParameter("usersId", usersId);
		t.setParameter("projectId", projectId);

		try {
			return t.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * findPositionsByCourseAbbv -  Use a named query to get a list of Position object for a specific course
	 * 
	 * @param courseAbbv - The course abbreviation to look for
	 * @return A List of Position objects
	 */
	public List<Position> findPositionsByCourseAbbv(String courseAbbv) {
		TypedQuery<Position> t = entityManager.createNamedQuery("findPositionsByCourseAbbv", Position.class);
		t.setParameter("courseAbbv", courseAbbv);

		try {
			return t.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * findPositionsUsersIdsAndCourseAbbvs-  Use a named query to get a list of Position objects for specific users and courses
	 * 
	 * @param usersIds - The user IDs to look for
	 * @param courseAbbvs - The course abbreviations to look for
	 * @return A List of Position objects
	 */
	public List<Position> findPositionsByUsersIdsAndCourseAbbvs(List<Integer> usersIds, List<String> courseAbbvs) {
		TypedQuery<Position> t = entityManager
				.createNamedQuery("findPositionsByUsersIdsAndCourseAbbvs", Position.class);
		t.setParameter("usersIds", usersIds);
		t.setParameter("courseAbbvs", courseAbbvs);

		try {
			return t.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * findPositionsByUsersIds-  Use a named query to get a list of Position objects for specific users
	 * 
	 * @param usersIds - The user IDs to look for
	 * @return A List of Position objects
	 */
	public List<Position> findPositionsByUsersIds(List<Integer> usersIds) {
		TypedQuery<Position> t = entityManager.createNamedQuery("findPositionsByUsersIds", Position.class);
		t.setParameter("usersIds", usersIds);
		try {
			return t.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

}
