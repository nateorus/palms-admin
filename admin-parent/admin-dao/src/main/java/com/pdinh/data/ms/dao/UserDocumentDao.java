package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.UserDocument;

@Stateless
@LocalBean
public class UserDocumentDao extends GenericDao<UserDocument, Integer> {

	public UserDocument findDocumentByFileAttachmentId(int fileAttachmentId) {
		TypedQuery<UserDocument> query = getEntityManager().createNamedQuery("findDocumentByFileAttachmentId",
				UserDocument.class);
		query.setParameter("fileAttachmentId", fileAttachmentId);
		try {
			List<UserDocument> results = query.getResultList();
			if (results == null || results.isEmpty()) {
				return null;
			}
			return results.get(0);
		} catch (Exception e) {
			return null;
		}
	}
}
