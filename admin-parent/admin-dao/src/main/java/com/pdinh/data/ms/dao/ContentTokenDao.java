package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ContentToken;

@Stateless
@LocalBean
public class ContentTokenDao extends GenericDao<ContentToken, Integer> {

	public ContentTokenDao() {

	}

	public List<ContentToken> findAllUserTokens(Integer users_id) {
		System.out.println("IN ContentTokenDao.findAllUserTokens users_id: " + users_id);
		TypedQuery<ContentToken> query = getEntityManager().createNamedQuery("findAllUserTokens", ContentToken.class);
		query.setParameter("id", users_id);
		List<ContentToken> tmp = query.getResultList();
		System.out.println("tmp size = " + tmp.size());

		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public ContentToken findByGuid(String guid) {
		System.out.println("IN ContentTokenDao.finByGuid guid: " + guid);
		TypedQuery<ContentToken> query = getEntityManager().createNamedQuery("findByGuid", ContentToken.class);
		query.setParameter("guid", guid);
		// List<ContentToken> tmp = query.getSingleResult();
		// System.out.println("tmp size = " + tmp.size());

		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public int deleteAllUserContentTokens(Integer users_id, String content_id) {
		Query query = getEntityManager().createQuery(
				"delete from ContentToken c where c.users_id =?1 and c.content_id = ?2");
		query.setParameter(1, users_id);
		query.setParameter(2, content_id);
		return query.executeUpdate();
	}
}
