package com.pdinh.data.ms.dao;

import java.util.Calendar;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.persistence.ms.entity.User;

@Stateless
@LocalBean
public class ScheduledEmailStatusDao extends GenericDao<ScheduledEmailStatus, Integer> {
    public ScheduledEmailStatus findByCompanyProjectUserEmailConstant(Integer companyId, Integer projectId, Integer userId, String emailConstant) {
		TypedQuery<ScheduledEmailStatus> query 
		    = getEntityManager().createNamedQuery("findScheduledEmailStatusByCompanyProjectUserEmailConstant",ScheduledEmailStatus.class);
		query.setParameter("companyId", companyId);
		query.setParameter("projectId", projectId);
		query.setParameter("userId", userId);
		query.setParameter("emailConstant", emailConstant);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
    
	public ScheduledEmailStatus createOrUpdate(Company company, Project project, User user, ScheduledEmailText scheduledEmailText) {
		Boolean create = false;
		ScheduledEmailStatus scheduledEmailStatus = findByCompanyProjectUserEmailConstant(company.getCompanyId(),project.getProjectId(),user.getUsersId(),scheduledEmailText.getScheduledEmailDef().getConstant());
		if (scheduledEmailStatus == null) {
			create = true;
			scheduledEmailStatus = new ScheduledEmailStatus();
		}
		scheduledEmailStatus.setCompany(company);
		scheduledEmailStatus.setProject(project);
		scheduledEmailStatus.setUser(user);
		scheduledEmailStatus.setLanguageCode(scheduledEmailText.getLanguage().getCode());
		scheduledEmailStatus.setEmailConstant(scheduledEmailText.getScheduledEmailDef().getConstant());
		scheduledEmailStatus.setScheduledEmailDef(scheduledEmailText.getScheduledEmailDef());
		scheduledEmailStatus.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
		
		if (create) {
			create(scheduledEmailStatus);
		} else {
			update(scheduledEmailStatus);
		}
		
		return scheduledEmailStatus;
	}
}
