package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.AssessmentCourse;
import com.pdinh.persistence.ms.entity.Product;
import com.pdinh.persistence.ms.entity.ProductContent;

@Stateless
@LocalBean
public class ProductContentDao extends GenericDao<ProductContent, Integer> {

	public int enableIASetContent(int setId, int companyId){
		Query query = entityManager.createNativeQuery("util_add_ia_set ?,?");
		query.setParameter(1, setId);
		query.setParameter(2, companyId);
		try {
			return query.executeUpdate();
		
		} catch (NoResultException e) {
			return 0;
		}
	}
	/**
	 * 
	 * @param companyId
	 * @param contentId
	 * @param contentTypeId
	 * @return 0 if the company has no products which enable this content. -1 if the company
	 * does have a product enabled with this content
	 */
	public int getContentProductStatus(int companyId, int contentId, int contentTypeId){
		Query query = entityManager.createNativeQuery("get_content_product_status ?,?,?");
		query.setParameter(1,  companyId);
		query.setParameter(2, contentId);
		query.setParameter(3, contentTypeId);
		try{
			Object[] objArray = (Object[]) query.getSingleResult();
			return (Integer)objArray[0];
			
		}catch (NoResultException nre){
			return 0;
		}
	}
}
