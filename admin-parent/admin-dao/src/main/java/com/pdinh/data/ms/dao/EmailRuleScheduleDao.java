package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.EmailRuleSchedule;

@Stateless
@LocalBean
public class EmailRuleScheduleDao extends GenericDao<EmailRuleSchedule, Integer> {

	public List<EmailRuleSchedule> findEmailRuleScheduleByEmailTemplateId(int emailId) {
		TypedQuery<EmailRuleSchedule> emailRuleSchedule = getEntityManager().createNamedQuery("findEmailRuleScheduleByEmailTemplateId",
				EmailRuleSchedule.class);
		emailRuleSchedule.setParameter("emailId", emailId);
		
		try {
			return emailRuleSchedule.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<EmailRuleSchedule> findRelativeActiveSchedulesByProject(int projectId) {
		TypedQuery<EmailRuleSchedule> emailRuleSchedule = getEntityManager().createNamedQuery("findRelativeActiveSchedulesByProject",
				EmailRuleSchedule.class);
		emailRuleSchedule.setParameter("projectId", projectId);
		
		try {
			return emailRuleSchedule.getResultList();
		} catch (NoResultException nre) {
			return new ArrayList<EmailRuleSchedule>();
		}
	}
}