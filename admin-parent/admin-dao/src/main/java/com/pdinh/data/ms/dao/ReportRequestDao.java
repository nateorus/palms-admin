package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.pdinh.persistence.ms.entity.ReportRequest;
@Stateless
@LocalBean
public class ReportRequestDao extends GenericDao<ReportRequest, Integer>{

	public ReportRequest getReportRequestBatchChildren(Integer reportRequestId){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportRequestBatchChildren", ReportRequest.class);
		query.setParameter("reportRequestId", reportRequestId);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public ReportRequest getReportRequestByKey(String reportRequestKey){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportRequestByKey", ReportRequest.class);
		query.setParameter("reportRequestKey", reportRequestKey);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public List<ReportRequest> getReportRequestsByProjectId(Integer projectId, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportRequestsByProjectId", ReportRequest.class);
		query.setParameter("projectId", projectId);
		
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportRequestsByProjectId(Integer projectId){
		Query query = getEntityManager().createNamedQuery("getCountReportRequestsByProjectId", ReportRequest.class);
		query.setParameter("projectId", projectId);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportRequest> getReportRequestsByCompanyId(Integer companyId, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportRequestsByCompanyId", ReportRequest.class);
		
		query.setParameter("companyId", companyId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportRequestsByCompanyId(Integer companyId){
		Query query = getEntityManager().createNamedQuery("getCountReportRequestsByCompanyId", ReportRequest.class);
		query.setParameter("companyId", companyId);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportRequest> getReportRequestsByCompanyIdAndStatus(Integer companyId, List<String> statuses, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportRequestsByCompanyIdAndStatus", ReportRequest.class);
		query.setParameter("companyId", companyId);
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportRequestsByCompanyIdAndStatus(Integer companyId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountReportRequestsByCompanyIdAndStatus", ReportRequest.class);
		query.setParameter("companyId", companyId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	
	public List<ReportRequest> getReportRequestsByProjectIdAndStatus(Integer projectId, List<String> statuses, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportRequestsByProjectIdAndStatus", ReportRequest.class);
		query.setParameter("projectId", projectId);
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportRequestsByProjectIdAndStatus(Integer projectId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountReportRequestsByProjectIdAndStatus", ReportRequest.class);
		query.setParameter("projectId", projectId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportRequest> getIndividualRequestsByUserId(Integer userId, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getIndividualRequestsByUserId", ReportRequest.class);
		query.setParameter("userId", userId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountIndividualRequestsByUserId(Integer userId){
		Query query = getEntityManager().createNamedQuery("getCountIndividualRequestsByUserId", ReportRequest.class);
		query.setParameter("userId", userId);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportRequest> getIndividualRequestsByUserIdAndStatus(Integer userId, List<String> statuses, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getIndividualRequestsByUserIdAndStatus", ReportRequest.class);
		query.setParameter("userId", userId);
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountIndividualRequestsByUserIdAndStatus(Integer userId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountIndividualRequestsByUserIdAndStatus", ReportRequest.class);
		query.setParameter("userId", userId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportRequest> getAllRequestsByUserIdAndStatus(Integer userId, List<String> statuses, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getAllRequestsByUserIdAndStatus", ReportRequest.class);
		query.setParameter("userId", userId);
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountAllRequestsByUserIdAndStatus(Integer userId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountAllRequestsByUserIdAndStatus", ReportRequest.class);
		query.setParameter("userId", userId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportRequest> getRequestsByStatus(List<String> statuses, int maxResults, int page){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getRequestsByStatus", ReportRequest.class);
		
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public List<ReportRequest> getRequestsSubmittedBy(Integer subjectId, List<String> statuses, int maxResults, int page){
		
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getRequestsSubmittedBy", ReportRequest.class);
		query.setParameter("subjectId", subjectId);
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		
		try{
			
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public List<ReportRequest> getByTypeSubmittedBy(Integer subjectId, String reportTypeCode){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getByTypeSubmittedBy", ReportRequest.class);
		query.setParameter("subjectId", subjectId);
		query.setParameter("reportTypeCode", reportTypeCode);
		
		try{
			
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public List<ReportRequest> getReportsByParent(Integer reportGenerateRequestId){
		TypedQuery <ReportRequest> query = getEntityManager().createNamedQuery("getReportsByParent", ReportRequest.class);
		query.setParameter("reportGenerateRequestId", reportGenerateRequestId);
		try{
			
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
}
