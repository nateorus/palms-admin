package com.pdinh.data.ms.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CoachingPlan;

@Stateless
@LocalBean
public class CoachingPlanDao extends GenericDao<CoachingPlan, Integer> {

	@Override
	public void update(CoachingPlan coachingPlan) {
		coachingPlan.setLastModifiedDate(new Timestamp(System.currentTimeMillis()));
		super.update(coachingPlan);
	}
	
	public List<CoachingPlan> findCoachingPlansByProjectId(int projectId){
		TypedQuery<CoachingPlan> query = getEntityManager().createNamedQuery("findCoachingPlansByProjectId", CoachingPlan.class);
		query.setParameter("projectId", projectId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public CoachingPlan findByProjectAndParticipant(int projectId, int participantId) {
		TypedQuery<CoachingPlan> query = getEntityManager().createNamedQuery("findByProjectAndParticipant",
				CoachingPlan.class);
		query.setParameter("participantId", participantId);
		query.setParameter("projectId", projectId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<CoachingPlan> findByParticipant(int participantId) {
		TypedQuery<CoachingPlan> query = getEntityManager().createNamedQuery("findByParticipant", CoachingPlan.class);
		query.setParameter("participantId", participantId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<CoachingPlan> findByCoach(int coachId) {
		TypedQuery<CoachingPlan> query = getEntityManager().createNamedQuery("findByCoach", CoachingPlan.class);
		query.setParameter("coachId", coachId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<CoachingPlan> findByCoachAndCompany(int coachId, int companyId) {
		TypedQuery<CoachingPlan> query = getEntityManager().createNamedQuery("findByCoachAndCompany", CoachingPlan.class);
		query.setParameter("coachId", coachId);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<CoachingPlan> findByCoachAndProject(int coachId, List<Integer> projectIds) {
		TypedQuery<CoachingPlan> query = getEntityManager().createNamedQuery("findByCoachAndProject", CoachingPlan.class);
		if (projectIds == null || projectIds.isEmpty()){
			projectIds.add(0);
		}
		query.setParameter("coachId", coachId);
		query.setParameter("projectIds", projectIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
