package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.ConfigurationGroup;

@Stateless
@LocalBean
public class ConfigurationGroupDao extends GenericDao<ConfigurationGroup, Integer> {

}
