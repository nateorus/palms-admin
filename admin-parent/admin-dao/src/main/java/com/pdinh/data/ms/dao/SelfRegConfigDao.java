package com.pdinh.data.ms.dao;

import java.util.List;
import java.util.UUID;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.SelfRegConfig;

@Stateless
@LocalBean
public class SelfRegConfigDao extends GenericDao<SelfRegConfig, Integer>{
	
	public SelfRegConfig findConfigByGuid(String guid){
		TypedQuery<SelfRegConfig> query = getEntityManager().createNamedQuery("findConfigByGuid", SelfRegConfig.class);
		
		query.setParameter("guid", guid);
		try{
			return query.getSingleResult();
		}catch (Exception e){
			return null;
		}
	}
	
	public List<SelfRegConfig> findConfigByProjectId(Integer projectId){
		TypedQuery<SelfRegConfig> query = getEntityManager().createNamedQuery("findConfigByProjectId", SelfRegConfig.class);
		query.setParameter("projectId", projectId);
		try{
			return query.getResultList();
		}catch (Exception e){
			return null;
		}
	}

}
