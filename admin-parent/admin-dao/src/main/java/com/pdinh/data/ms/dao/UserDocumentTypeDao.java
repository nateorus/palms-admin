package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.UserDocumentType;

@Stateless
@LocalBean
public class UserDocumentTypeDao extends GenericDao<UserDocumentType,Integer>{
}
