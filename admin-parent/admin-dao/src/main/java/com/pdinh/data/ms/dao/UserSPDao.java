package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.eclipse.persistence.queries.ResultSetMappingQuery;
import org.eclipse.persistence.queries.StoredProcedureCall;
import org.eclipse.persistence.sessions.Session;

import com.pdinh.persistence.dto.LightUser;

/**
 * Dao to access (read-only) platform USERS table via stored procedures.
 */
@Stateless
@LocalBean
public class UserSPDao {

	// Reference exists only so we can grab the EntityManager.
	@EJB
	UserDao userDao;

	@SuppressWarnings("unchecked")
	public LightUser findByCompanyAndEmail(int companyId, String email) {
		email.trim();

		StoredProcedureCall call = new StoredProcedureCall();
		call.setProcedureName("users_find_by_company_and_email");
		call.addNamedArgument("company_id", "companyId");
		call.addNamedArgument("email", "email");

		ResultSetMappingQuery query = new ResultSetMappingQuery(call);
		query.addArgument("companyId");
		query.addArgument("email");

		List<Object> arguments = new ArrayList<Object>();
		arguments.add(companyId);
		arguments.add(email);
		Session session = userDao.getEntityManager().unwrap(Session.class);
		List<Object[]> rows = (List<Object[]>) session.executeQuery(query, arguments);

		if (rows.isEmpty()) {
			return null;
		}

		return getLightUserFor(rows.get(0));
	}

	@SuppressWarnings("unchecked")
	public List<LightUser> findAllInList(List<Integer> idList) {
		List<LightUser> results = new ArrayList<LightUser>();

		StringBuilder builder = new StringBuilder();
		for (Integer id : idList) {
			builder.append(id.intValue());
			builder.append(" ");
		}

		StoredProcedureCall call = new StoredProcedureCall();
		call.setProcedureName("users_get_for_id_in_list");
		call.addNamedArgument("userids", "userids");

		ResultSetMappingQuery query = new ResultSetMappingQuery(call);
		query.addArgument("userids");

		List<String> arguments = new ArrayList<String>();
		arguments.add(builder.toString().trim());

		Session session = userDao.getEntityManager().unwrap(Session.class);
		for (Object[] row : (List<Object[]>) session.executeQuery(query, arguments)) {
			results.add(getLightUserFor(row));
		}

		return results;
	}

	private LightUser getLightUserFor(Object[] row) {
		int id = ((Integer) row[0]).intValue();
		String firstName = (String) row[1];
		String lastName = (String) row[2];
		String email = (String) row[3];

		return new LightUser(id, firstName, lastName, email);
	}
}
