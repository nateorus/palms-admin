package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.PasswordHistory;

@Stateless
@LocalBean
public class PasswordHistoryDao extends GenericDao<PasswordHistory, Integer> {

}
