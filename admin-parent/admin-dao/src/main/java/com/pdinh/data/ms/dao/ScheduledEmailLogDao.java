package com.pdinh.data.ms.dao;

import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;

import com.pdinh.persistence.ms.entity.EmailRule;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailLog;
import com.pdinh.persistence.ms.entity.ScheduledEmailLogStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.persistence.ms.entity.User;

@Stateless
@LocalBean
public class ScheduledEmailLogDao extends GenericDao<ScheduledEmailLog, Integer> {
	@EJB
	CompanyDao companyDao;
	@EJB
	ProjectDao projectDao;
	@EJB
	UserDao userDao;
	@EJB
	ScheduledEmailDefDao scheduledEmailDefDao;
	
	// JJB: Still contemplating what is better...having no entity dependencies passed in or having them passed in
	// For now have a create for both scenarios
	public ScheduledEmailLog create(Integer companyId, Integer projectId, Integer userId, Integer scheduledEmailDefId, String languageCode) {
		ScheduledEmailLog scheduledEmailLog = new ScheduledEmailLog();
		scheduledEmailLog.setCompany(companyDao.findById(companyId));
		scheduledEmailLog.setProject(projectDao.findById(projectId));
		scheduledEmailLog.setUser(userDao.findById(userId));
		scheduledEmailLog.setLanguageCode(languageCode);
		ScheduledEmailDef scheduledEmailDef = scheduledEmailDefDao.findById(scheduledEmailDefId);
		scheduledEmailLog.setEmailConstant(scheduledEmailDef.getConstant());
		scheduledEmailLog.setScheduledEmailDef(scheduledEmailDef);
		scheduledEmailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
		
		create(scheduledEmailLog);
		
		return scheduledEmailLog;
	}

	public ScheduledEmailLog create(Company company, Project project, User user, ScheduledEmailText scheduledEmailText, ScheduledEmailLogStatus status, EmailRuleSchedule schedule, String sentEmailSubject, String sentEmailBody) {
		ScheduledEmailLog scheduledEmailLog = new ScheduledEmailLog();
		scheduledEmailLog.setCompany(company);
		if (!(project == null)){
			scheduledEmailLog.setProject(project);
		}
		scheduledEmailLog.setUser(user);
		if (!(scheduledEmailText == null)){
			scheduledEmailLog.setLanguageCode(scheduledEmailText.getLanguage().getCode());
			scheduledEmailLog.setEmailConstant(scheduledEmailText.getScheduledEmailDef().getConstant());
			scheduledEmailLog.setScheduledEmailDef(scheduledEmailText.getScheduledEmailDef());
		}else{
			scheduledEmailLog.setEmailConstant(schedule.getEmailRule().getEmailTemplate().getConstant());
			scheduledEmailLog.setScheduledEmailDef(schedule.getEmailRule().getEmailTemplate());
			scheduledEmailLog.setLanguageCode(user.getLangPref());
		}
		//update date completed later
		scheduledEmailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
		scheduledEmailLog.setStatus(status);
		scheduledEmailLog.setSchedule(schedule);
		scheduledEmailLog.setSentEmailSubject(sentEmailSubject);
		scheduledEmailLog.setSentEmailBody(sentEmailBody);
		
		create(scheduledEmailLog);
		
		return scheduledEmailLog;
	}

	public List<ScheduledEmailLog> findScheduledEmailLogsByProjectId(int projectId) {
		TypedQuery<ScheduledEmailLog> scheduledEmailLog = getEntityManager().createNamedQuery("findScheduledEmailLogsByProjectId",
				ScheduledEmailLog.class);
		scheduledEmailLog.setParameter("projectId", projectId);
		
		try {
			return scheduledEmailLog.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
