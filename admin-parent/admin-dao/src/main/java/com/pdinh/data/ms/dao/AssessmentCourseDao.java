package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pdinh.persistence.ms.entity.AssessmentCourse;

@Stateless
@LocalBean
public class AssessmentCourseDao extends GenericDao<AssessmentCourse, Integer> {

	public AssessmentCourseDao() {
		super();
	}

	public List<AssessmentCourse> findAllForParticipant(int companyId, int usersId) {
		Query query = entityManager.createNativeQuery("user_course_list_status_assessment ?,?");
		query.setParameter(1, companyId);
		query.setParameter(2, usersId);
		try {
			List<Object> objList = query.getResultList();
			List<AssessmentCourse> acs = new ArrayList<AssessmentCourse>();
			int n = 0;
			for (Object obj : objList) {
				AssessmentCourse ac = new AssessmentCourse();
				Object[] objArray = (Object[]) obj;
				ac.setCourseId((Integer) objArray[0]);
				ac.setTitle((String) objArray[1]);
				ac.setSblurb((String) objArray[2]);
				ac.setType((Integer) objArray[3]);
				ac.setAbbv((String) objArray[4]);
				ac.setDescription((String) objArray[5]);
				ac.setCourseDuration((String) objArray[6]);
				ac.setCompletionStatus((Integer) objArray[7]);
				acs.add(ac);
			}
			return acs;
		} catch (NoResultException e) {
			return null;
		}

	}
}
