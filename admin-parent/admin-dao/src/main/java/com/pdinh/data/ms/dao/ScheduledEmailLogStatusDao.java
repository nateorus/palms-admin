package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ScheduledEmailLogStatus;

@Stateless
@LocalBean
public class ScheduledEmailLogStatusDao extends GenericDao<ScheduledEmailLogStatus, Integer> {

	public ScheduledEmailLogStatus findStatusByCodeAndLangId(String status_code, Integer language_id){
		TypedQuery<ScheduledEmailLogStatus> query = getEntityManager().createNamedQuery("findStatusByCodeAndLangId", ScheduledEmailLogStatus.class);
		query.setParameter("status_code", status_code);
		query.setParameter("language_id", language_id);
		try{
			return query.getSingleResult();
		}catch (NoResultException e){
			return null;
		}
		
		
	}
}
