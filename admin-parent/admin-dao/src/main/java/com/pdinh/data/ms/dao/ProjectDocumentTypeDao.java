package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.ProjectDocumentType;

@Stateless
@LocalBean
public class ProjectDocumentTypeDao extends GenericDao<ProjectDocumentType,Integer>{
}
