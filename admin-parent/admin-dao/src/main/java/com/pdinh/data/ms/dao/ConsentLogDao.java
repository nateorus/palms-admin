package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ConsentLog;

@Stateless
@LocalBean
public class ConsentLogDao extends GenericDao<ConsentLog, Integer> {

	public ConsentLog findMostRecentForParticipant(int usersId) {
		TypedQuery<ConsentLog> query = getEntityManager().createNamedQuery("findMostRecentForParticipant",
				ConsentLog.class);
		query.setParameter("usersId", usersId);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
		}
		return null;
	}

	public List<ConsentLog> findAllForParticipant(int usersId) {
		TypedQuery<ConsentLog> query = getEntityManager().createNamedQuery("findAllForParticipant", ConsentLog.class);
		query.setParameter("usersId", usersId);
		try {
			return query.getResultList();
		} catch (Exception e) {
		}
		return null;
	}
}
