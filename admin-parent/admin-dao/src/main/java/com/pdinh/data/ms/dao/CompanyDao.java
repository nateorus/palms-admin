package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Product;
import com.pdinh.persistence.ms.entity.ProjectUser;

/**
 * Session Bean implementation class CompanyDao
 */
@Stateless
@LocalBean
public class CompanyDao extends GenericDao<Company, Integer> {

	// TODO No longer used?
	public static List<String> assessmentProductCodes = new ArrayList<String>(Arrays.asList(Product.PROD_ABYD,
			Product.PROD_TLT, Product.PROD_GLGPS, Product.PROD_COACH, Product.PROD_TLTDEMO));

	/**
	 * Default constructor.
	 */
	public CompanyDao() {
		// TODO Auto-generated constructor stub
	}

	public List<Company> findAllCompaniesOrdered() {
		TypedQuery<Company> query = getEntityManager().createNamedQuery("findAllCompaniesOrdered", Company.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<Company> findAllCompaniesFiltered(List<Integer> companies) {
		if (companies.isEmpty()) {
			companies.add(0);
		}
		TypedQuery<Company> query = getEntityManager().createNamedQuery("findAllCompaniesFiltered", Company.class);
		query.setParameter("companies", companies);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<Company> findAllActiveAssessment() {

		TypedQuery<Company> query = getEntityManager().createNamedQuery("findAllActiveAssessment", Company.class);
		// query.setParameter("assessmentProductCodes", assessmentProductCodes);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public Company findBatchUsersProjectsById(Integer companyId) {
		System.out.println("IN CompanyDao.findBatchById companyId: " + companyId);

		TypedQuery<ProjectUser> query = getEntityManager()
				.createNamedQuery("getCompanyUserProjects", ProjectUser.class);
		query.setParameter("id", companyId);
		List<ProjectUser> tmp = query.getResultList();
		System.out.println("tmp size = " + tmp.size());

		TypedQuery<Company> projectquery = getEntityManager().createNamedQuery("getCompanyBatchUsersProjects",
				Company.class);
		projectquery.setParameter("id", companyId);
		try {
			return projectquery.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}

	}
}
