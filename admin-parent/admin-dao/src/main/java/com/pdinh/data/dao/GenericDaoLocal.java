package com.pdinh.data.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

@Local
public interface GenericDaoLocal<T, ID extends Serializable> {
	T create(T object);

	void update(T object);

	void delete(T object);

	T findById(ID id);

	T findById(ID id, boolean doRefresh);

	List<T> findAll();

	List<T> findAll(boolean doRefresh);

}
