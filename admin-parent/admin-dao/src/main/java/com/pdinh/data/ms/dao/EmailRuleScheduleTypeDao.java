package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.EmailRuleScheduleType;

@Stateless
@LocalBean
public class EmailRuleScheduleTypeDao extends GenericDao<EmailRuleScheduleType, Integer> {

}