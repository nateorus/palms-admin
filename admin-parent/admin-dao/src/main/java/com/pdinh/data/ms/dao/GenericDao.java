package com.pdinh.data.ms.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.pdinh.data.dao.GenericDaoLocal;

public abstract class GenericDao<T, ID extends Serializable> implements GenericDaoLocal<T, ID> {
	protected Class<T> persistentClass;

	@PersistenceContext(unitName = "PDINHServer-ms-dao")
	protected EntityManager entityManager;

	@SuppressWarnings("unchecked")
	protected GenericDao() {
		if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
			ParameterizedType type = (ParameterizedType) (getClass().getGenericSuperclass());
			System.out.println(type.getActualTypeArguments()[0]);
			this.persistentClass = (Class<T>) type.getActualTypeArguments()[0];
		}
	}

	protected GenericDao(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public T findById(ID id) {
		return getEntityManager().find(persistentClass, id);
	}

	@Override
	public T findById(ID id, boolean doRefresh) {
		T object = getEntityManager().find(persistentClass, id);
		if (doRefresh) {
			refresh(object);
		}
		return object;
	}

	@Override
	public List<T> findAll() {
		List<T> list = getEntityManager().createQuery("SELECT obj from " + persistentClass.getSimpleName() + " obj",
				persistentClass).getResultList();
		return list;
	}

	@Override
	public List<T> findAll(boolean doRefresh) {
		String sql = String.format("SELECT obj from %1$s obj", persistentClass.getSimpleName());
		TypedQuery<T> query = getEntityManager().createQuery(sql, persistentClass);
		if (doRefresh)
			query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<T> list = query.getResultList();
		return list;
	}

	@Override
	public void update(T object) {
		getEntityManager().merge(object);
		// Not yet sure if need this
		// getEntityManager().flush();
	}

	@Override
	public T create(T object) {
		getEntityManager().persist(object);

		// need this to get generated IDs after persist
		getEntityManager().flush();
		return object;
	}

	@Override
	public void delete(T object) {
		if (!getEntityManager().contains(object)) {
			object = getEntityManager().merge(object);
		}
		getEntityManager().remove(object);
	}

	public void evictAll() {
		getEntityManager().getEntityManagerFactory().getCache().evictAll();
	}

	public void refresh(T object) {
		getEntityManager().refresh(object);
	}
}
