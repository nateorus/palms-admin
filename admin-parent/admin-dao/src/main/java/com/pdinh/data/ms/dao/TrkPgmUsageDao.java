package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.TrkPgmUsage;
import com.pdinh.persistence.ms.entity.TrkPgmUsagePK;

/**
 * Session Bean implementation class CompanyDao
 */
@Stateless
@LocalBean
public class TrkPgmUsageDao extends GenericDao<TrkPgmUsage, TrkPgmUsagePK>  {

   public TrkPgmUsage findByRowguid(String rowguid) {
	   TypedQuery<TrkPgmUsage> t = entityManager.createNamedQuery("findTrkPgmUsageByRowguid",TrkPgmUsage.class);
	   t.setParameter("rowguid", rowguid);
		
	   try {
		   return t.getSingleResult();
	   } catch (NoResultException e) {
		   return null;
	   }
   }
   
   public int getTrkPrgUsageIterations(int usersId, int courseId) {
	   TypedQuery<Long> countQuery = entityManager.createNamedQuery("getTrkPgmUsageIterations",Long.class);
	   countQuery.setParameter("usersId", usersId);
	   countQuery.setParameter("courseId", courseId);
		
	   try {
		   return countQuery.getSingleResult().intValue();
	   } catch (NoResultException e) {
		   return 0;
	   }
   }
}    

