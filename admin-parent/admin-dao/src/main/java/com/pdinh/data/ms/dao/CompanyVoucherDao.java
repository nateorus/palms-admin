package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.CompanyVoucher;

/*
 * CompanyVoucherDao
 */
@Stateless
@LocalBean
public class CompanyVoucherDao extends GenericDao<CompanyVoucher, Integer> {
	/**
	 * Returns a CompanyVoucher entity. To get the voucherId, call
	 * companyVoucher.getVoucherId()
	 * 
	 * @param company
	 * @return CompanyVoucher object
	 */
	public CompanyVoucher findVoucherForCompany(Company company) {
		TypedQuery<CompanyVoucher> query = getEntityManager().createNamedQuery("findVoucherForCompany",
				CompanyVoucher.class);
		try {
			query.setParameter("company", company);
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
