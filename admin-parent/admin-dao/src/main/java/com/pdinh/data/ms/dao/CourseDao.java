package com.pdinh.data.ms.dao;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Course;

/**
 * Session Bean implementation class CourseDao
 */
@Stateless
@LocalBean
public class CourseDao extends GenericDao<Course, Integer> {

	public Course findByAbbv(String abbv) {
		TypedQuery<Course> query = getEntityManager().createNamedQuery("findCourseByAbbv", Course.class);
		query.setParameter("abbv", abbv);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<Course> findAllByUsersId(int userId) {
		TypedQuery<Course> query = getEntityManager().createNamedQuery("findAllByUsersId", Course.class);
		query.setParameter("userId", userId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<Course> findCoursesIn(List<Integer> courseIds){
		TypedQuery<Course> query = getEntityManager().createNamedQuery("findCoursesIn", Course.class);
		if (courseIds.isEmpty()){
			return null;
		}
		query.setParameter("courseIds", courseIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public Integer getMaxCourseId() {
		TypedQuery<Integer> query = getEntityManager().createNamedQuery("getMaxCourseId", Integer.class);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public Course createOrUpdate(String abbv, String name, String description) {
		Course course;
		if (findByAbbv(abbv) != null) {
			course = update(abbv, name, description);
		} else {
			course = create(abbv, name, description);
		}

		return course;
	}

	public Course create(String abbv, String title, String description) {
		// Only add if it doesn't exist
		if (findByAbbv(abbv) == null) {
			Course course = new Course();
			course.setAbbv(abbv);
			course.setTitle(title);
			course.setDescription(description);
			getEntityManager().persist(course);
			return course;
		} else {
			return null;
		}
	}

	public Course update(String abbv, String title, String description) {
		Course course = findByAbbv(abbv);
		course.setAbbv(abbv);
		course.setTitle(title);
		course.setDescription(description);
		getEntityManager().persist(course);
		return course;
	}

	@RolesAllowed("ADMIN")
	public void removeByAbbv(String abbv) {
		Course obj = findByAbbv(abbv);
		if (obj != null) {
			getEntityManager().remove(obj);
		}
	}

	public int getCourseType(String scormCourseId) throws NoResultException {
		System.out.println("in getCourseType, scormCourseId: " + scormCourseId);
		Query scormIdQ = getEntityManager().createNamedQuery("getCourseTypeByScormCourseId");
		scormIdQ.setParameter("scorm_course_id", scormCourseId);
		return (Integer) scormIdQ.getSingleResult();
	}
	
	public List<Course> findAllTinCanCourses() {
		TypedQuery<Course> query = getEntityManager().createNamedQuery("findAllTinCanCourses", Course.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<Object[]> findAllCourseAbbvIdArray(){
		Query query = getEntityManager().createQuery("SELECT c.abbv, c.course FROM Course c");
		try{
			return query.getResultList();
		}catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<Course> findAllBatchLanguage(){
		TypedQuery<Course> query = getEntityManager().createQuery("Select c from Course c", Course.class);
		query.setHint("eclipselink.join-fetch", "c.languages");
		try{
			return query.getResultList();
		}catch (NoResultException nre) {
			return null;
		}
	}
}
