package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ConsentUser;

@Stateless
@LocalBean
public class ConsentUserDao extends GenericDao<ConsentUser, Integer> {

	public ConsentUserDao() {
		super();
	}

	public List<ConsentUser> findAllUnAcceptedForParticipant(int usersId) {
		TypedQuery<ConsentUser> query = getEntityManager().createNamedQuery("findAllUnAcceptedForParticipant",
				ConsentUser.class);
		query.setParameter("usersId", usersId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ConsentUser> findByParticipantAndType(int usersId, String consentType) {
		TypedQuery<ConsentUser> query = getEntityManager().createNamedQuery("findByParticipantAndType",
				ConsentUser.class);
		query.setParameter("usersId", usersId);
		query.setParameter("consentType", consentType);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
