package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.ReportType;

@Stateless
@LocalBean
public class ReportRequestStatusDao extends GenericDao<ReportRequestStatus, Integer>{
	
	public ReportRequestStatus findReportRequestStatusByCode(String code){
		TypedQuery<ReportRequestStatus> query = entityManager.createQuery("SELECT rrs FROM ReportRequestStatus rrs WHERE rrs.code = :code", ReportRequestStatus.class);
		query.setParameter("code", code);
		try{
			return query.getSingleResult();
		}catch (NoResultException nre){
			return null;
		}
	}

}
