package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Roster;

/**
 * Session Bean implementation class RosterDao
 */
@Stateless
@LocalBean
public class RosterDao extends GenericDao<Roster, Integer> {

	/**
	 * Default constructor.
	 */
	public RosterDao() {
		// TODO Auto-generated constructor stub
	}

	public Roster findByUserIdAndCourseId(Integer userId, Integer courseId) {
		TypedQuery<Roster> query = getEntityManager().createNamedQuery("findRosterByUserIdAndCourseId", Roster.class);
		query.setParameter("userId", userId);
		query.setParameter("courseId", courseId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public Boolean existsByUserIdAndCourseId(Integer userId, Integer courseId) {
		Query query = getEntityManager().createNamedQuery("existsRosterByUserIdAndCourseId");
		query.setParameter("userId", userId);
		query.setParameter("courseId", courseId);
		long matchCounter = (Long) query.getSingleResult();
		return (matchCounter > 0);
	}

	public List<Roster> findRostersByUserId(Integer userId) {
		TypedQuery<Roster> query = getEntityManager().createNamedQuery("findRostersByUserId", Roster.class);
		query.setParameter("userId", userId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
