package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CompanyCustomProperty;

/*
 * CompanyCustomPropertyDao
 */
@Stateless
@LocalBean
public class CompanyCustomPropertyDao extends GenericDao<CompanyCustomProperty, Integer> {

	public List<CompanyCustomProperty> findAllByCompany(int companyId) {
		TypedQuery<CompanyCustomProperty> query = getEntityManager().createNamedQuery("findAllByCompany", CompanyCustomProperty.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public CompanyCustomProperty findByCompanyAndSectionId(int companyId, String sectionId) {		
		TypedQuery<CompanyCustomProperty> query = getEntityManager().createNamedQuery("findByCompanyAndSectionId", CompanyCustomProperty.class);
		query.setParameter("companyId", companyId);
		query.setParameter("sectionId", sectionId);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
