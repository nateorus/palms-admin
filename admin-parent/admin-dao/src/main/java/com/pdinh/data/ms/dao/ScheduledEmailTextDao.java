package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;


@Stateless
@LocalBean
public class ScheduledEmailTextDao extends GenericDao<ScheduledEmailText, Integer>{

	private static final Logger log = LoggerFactory.getLogger(ScheduledEmailTextDao.class);
	
	public int deleteScheduledEmailTextsByTemplateId(int id) {
		Query query = getEntityManager().createNamedQuery("deleteScheduledEmailTextsByTemplateId");
		query.setParameter("id", id);
		try {
			return query.executeUpdate();
		} catch (NoResultException nre) {
			return -1;
		}
	}
	
	public List<ScheduledEmailText> findByDefBatchLang(Integer defId){
		log.debug("In findByDefBatchLang...");
		TypedQuery<ScheduledEmailText> query = getEntityManager().createNamedQuery("findByDefBatchLang", ScheduledEmailText.class);
		query.setParameter("defId", defId);
		try{
			return query.getResultList();
		}catch (NoResultException nre){
			return null;
		}
	}
	
}
