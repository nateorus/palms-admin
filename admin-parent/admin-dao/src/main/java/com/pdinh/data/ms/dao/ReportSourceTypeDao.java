package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.ReportSourceType;

/**
 * Session Bean implementation class ClientDao
 */
@Stateless
@LocalBean
public class ReportSourceTypeDao extends GenericDao<ReportSourceType, Integer> {

}
