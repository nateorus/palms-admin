package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ProjectType;

/**
 * Session Bean implementation class ClientDao
 */
@Stateless
@LocalBean
public class ProjectTypeDao extends GenericDao<ProjectType, Integer> {

	public List<ProjectType> findAllByProductCode(String productCode) {
		TypedQuery<ProjectType> query = getEntityManager().createNamedQuery("findAllByProductCode", ProjectType.class);
		query.setParameter("productCode", productCode);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	public String getProjectTypeFromProjectId(int projectId) {
		TypedQuery<ProjectType> query = getEntityManager().createNamedQuery("getProjectTypeFromProjectId",
				ProjectType.class);
		query.setParameter("projectId", projectId);
		try {
			ProjectType pt = query.getSingleResult();
			String projectTypeCode = pt.getProjectTypeCode();
			return projectTypeCode;

		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public ProjectType findProjectTypeByCode(String projectTypeCode) {
		TypedQuery<ProjectType> query = getEntityManager().createNamedQuery("findProjectTypeByCode",
				ProjectType.class);
		query.setParameter("projectTypeCode", projectTypeCode);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ProjectType> findAllByCompanyId(Integer companyId) {
		TypedQuery<ProjectType> query = getEntityManager().createNamedQuery("findAllByCompanyId", ProjectType.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<ProjectType> findTinCanProjectTypesByCompanyId(Integer companyId) {
		TypedQuery<ProjectType> query = getEntityManager().createNamedQuery("findTinCanProjectTypesByCompanyId", ProjectType.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
