package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportRequest;

@Stateless
@LocalBean
public class ReportGenerateRequestDao extends GenericDao<ReportGenerateRequest, Integer>{

	public List<ReportGenerateRequest> getReportGenerateRequestsByProjectId(Integer projectId, int maxResults, int page){
		TypedQuery <ReportGenerateRequest> query = getEntityManager().createNamedQuery("getReportGenerateRequestsByProjectId", ReportGenerateRequest.class);
		
		query.setParameter("projectId", projectId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportGenerateRequestsByProjectId(Integer projectId){
		Query query = getEntityManager().createNamedQuery("getCountReportGenerateRequestsByProjectId", ReportGenerateRequest.class);
		query.setParameter("projectId", projectId);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportGenerateRequest> getGenerateRequestsByProjAndStat(Integer projectId, int maxResults, int page, List<String> statuses){
		TypedQuery<ReportGenerateRequest> query = getEntityManager().createNamedQuery("getGenerateRequestsByProjAndStat", ReportGenerateRequest.class);
		query.setParameter("projectId", projectId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		query.setParameter("statuses", statuses);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountGenerateRequestsByProjAndStat(Integer projectId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountGenerateRequestsByProjAndStat", ReportGenerateRequest.class);
		query.setParameter("projectId", projectId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportGenerateRequest> getGenerateRequestsByCompAndStat(Integer companyId, int maxResults, int page, List<String> statuses){
		TypedQuery<ReportGenerateRequest> query = getEntityManager().createNamedQuery("getGenerateRequestsByCompAndStat", ReportGenerateRequest.class);
		query.setParameter("companyId", companyId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		query.setParameter("statuses", statuses);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	
	public Long getCountGenerateRequestsByCompAndStat(Integer companyId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountGenerateRequestsByCompAndStat", ReportGenerateRequest.class);
		query.setParameter("companyId", companyId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportGenerateRequest> getReportGenerateRequestsByCompanyId(Integer companyId, int maxResults, int page){
		TypedQuery <ReportGenerateRequest> query = getEntityManager().createNamedQuery("getReportGenerateRequestsByCompanyId", ReportGenerateRequest.class);
		
		query.setParameter("companyId", companyId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportGenerateRequestsByCompanyId(Integer companyId){
		Query query = getEntityManager().createNamedQuery("getCountReportGenerateRequestsByCompanyId", ReportGenerateRequest.class);
		query.setParameter("companyId", companyId);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public List<ReportGenerateRequest> getReportGenerateRequestsByUserId(Integer usersId, int maxResults, int page){
		TypedQuery <ReportGenerateRequest> query = getEntityManager().createNamedQuery("getReportGenerateRequestsByUserId", ReportGenerateRequest.class);
		
		query.setParameter("usersId", usersId);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountReportGenerateRequestsByUserId(Integer usersId){
		Query query = getEntityManager().createNamedQuery("getCountReportGenerateRequestsByUserId", ReportGenerateRequest.class);
		query.setParameter("usersId", usersId);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	
	public List<ReportGenerateRequest> getGenerateRequestsByUserAndStat(Integer usersId, int maxResults, int page, List<String> statuses){
		TypedQuery <ReportGenerateRequest> query = getEntityManager().createNamedQuery("getGenerateRequestsByUserAndStat", ReportGenerateRequest.class);
		
		query.setParameter("usersId", usersId);
		query.setParameter("statuses", statuses);
		query.setFirstResult(page * maxResults);
		query.setMaxResults(maxResults);
		try{
			return query.getResultList();
		} catch (NoResultException nre){
			return null;
		}
	}
	
	public Long getCountGenerateRequestsByUserAndStat(Integer usersId, List<String> statuses){
		Query query = getEntityManager().createNamedQuery("getCountGenerateRequestsByUserAndStat", ReportGenerateRequest.class);
		query.setParameter("usersId", usersId);
		query.setParameter("statuses", statuses);
		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	public ReportGenerateRequest getRGRByIdBatchChildren(Integer id){
		TypedQuery <ReportGenerateRequest> query = getEntityManager().createNamedQuery("getRGRByIdBatchChildren", ReportGenerateRequest.class);
		query.setParameter("id", id);
		try{
			return query.getSingleResult();
		}catch (NoResultException nre){
			return null;
		}
	}
	
}
