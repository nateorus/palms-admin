package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ProjectReport;

@Stateless
@LocalBean
public class ProjectReportDao extends GenericDao<ProjectReport, Integer>{
	
	public List<ProjectReport> getTemplatesForProjectType(Integer projectTypeId){
		
		TypedQuery<ProjectReport> query = getEntityManager().createNamedQuery("getTemplatesForProjectType", ProjectReport.class);
		query.setParameter("projectTypeId", projectTypeId);
		
		try{
			return query.getResultList();
		}catch(NoResultException nre){
			return null;
		}
	}
	
	public List<ProjectReport> getAllProjectReportTemplates(){
		TypedQuery<ProjectReport> query = getEntityManager().createNamedQuery("getAllProjectReportTemplates", ProjectReport.class);
		
		try{
			return query.getResultList();
		}catch(NoResultException nre){
			return null;
		}
	}

}
