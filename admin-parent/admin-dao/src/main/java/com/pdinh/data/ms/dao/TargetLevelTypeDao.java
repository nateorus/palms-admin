/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.TargetLevelType;

/*
 * TargetLevelTypeDao
 */
@Stateless
@LocalBean
public class TargetLevelTypeDao extends GenericDao<TargetLevelType, Integer> {
	// No explicit implementation code at this point

	public TargetLevelTypeDao() {
		// TODO Auto-generated constructor stub
	}

	public TargetLevelType findByCode(String code) {
		String sql = "SELECT t FROM TargetLevelType t where t.code = ?1";
		TypedQuery<TargetLevelType> query = getEntityManager().createQuery(sql, TargetLevelType.class);
		query.setParameter(1, code);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
