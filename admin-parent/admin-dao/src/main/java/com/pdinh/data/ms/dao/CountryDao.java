package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Country;

@Stateless
@LocalBean
public class CountryDao extends GenericDao<Country, Integer> {
	
	public Country findCountryByCode(String code){
		TypedQuery<Country> query = getEntityManager().createNamedQuery("findCountryByCode", Country.class);
		query.setParameter("code", code);
		try{
			return query.getSingleResult();
		}catch (NoResultException nre){
			return null;
		}
	}

}
