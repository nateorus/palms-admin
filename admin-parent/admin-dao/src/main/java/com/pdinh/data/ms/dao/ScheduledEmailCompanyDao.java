package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ScheduledEmailCompany;
import com.pdinh.persistence.ms.entity.ScheduledEmailProject;

@Stateless
@LocalBean
public class ScheduledEmailCompanyDao extends GenericDao<ScheduledEmailCompany, Integer> {
	public List<ScheduledEmailCompany> findScheduledEmailCompanyByEmailId(int emailId) {
		TypedQuery<ScheduledEmailCompany> query = getEntityManager().createNamedQuery("findScheduledEmailCompanyByEmailId",
				ScheduledEmailCompany.class);
		query.setParameter("emailId", emailId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
