package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ScheduledEmailRule;
import com.pdinh.persistence.ms.entity.ScheduledEmailRuleForProcessing;

public class ScheduledEmailRuleDao extends GenericDao<ScheduledEmailRule, Integer>  {

	@EJB
	ScheduledEmailDefDao scheduledEmailDefDao; 
	
	@EJB
	ProjectDao projectDao;
	
	public List<ScheduledEmailRule> findAllEmailRulesForProject() {
		TypedQuery<ScheduledEmailRule> query = getEntityManager().createNamedQuery("findAllEmailRulesForProject",
				ScheduledEmailRule.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	// create method
	public ScheduledEmailRule create(String name, int active, Integer scheduledEmailDefId, String language, List<ScheduledEmailRuleForProcessing> scheduledEmailRulesForProcessing, Integer projectId ) {
		ScheduledEmailRule scheduledEmailRule = new ScheduledEmailRule();
		scheduledEmailRule.setName(name);
		scheduledEmailRule.setActive(active);
		scheduledEmailRule.setScheduledEmailDef(scheduledEmailDefDao.findById(scheduledEmailDefId));   // email template 
		scheduledEmailRule.setLanguage(language);
		scheduledEmailRule.setScheduledEmailRulesForProcessing(scheduledEmailRulesForProcessing);
		scheduledEmailRule.setProject(projectDao.findById(projectId));
			
		create(scheduledEmailRule);
		
		return scheduledEmailRule;
	}
}
