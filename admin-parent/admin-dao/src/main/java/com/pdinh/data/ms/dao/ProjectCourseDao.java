package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.dto.IntakeInstrumentData;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseFlowType;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;

/**
 * Session Bean implementation class ClientDao
 */
@Stateless
@LocalBean
public class ProjectCourseDao extends GenericDao<ProjectCourse, Integer> {
	@EJB
	private ProjectDao projectDao;
	@EJB
	private CourseDao courseDao;
	@EJB
	private CourseFlowTypeDao courseFlowTypeDao;

	private static final Logger log = LoggerFactory.getLogger(ProjectCourseDao.class);

	public List<ProjectCourse> findByProjectAndCourseFlowType(Integer projectId, Integer courseFlowTypeId) {
		String sql = "SELECT pc FROM ProjectCourse pc WHERE pc.project.projectId=?1 AND pc.courseFlowType.courseFlowTypeId=?2 ORDER BY pc.sequence";
		TypedQuery<ProjectCourse> query = getEntityManager().createQuery(sql, ProjectCourse.class);
		query.setParameter(1, projectId);
		query.setParameter(2, courseFlowTypeId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public int deleteAllForProject(Integer projectId) {
		Query query = getEntityManager().createQuery("delete from ProjectCourse pc WHERE pc.project.projectId=?1");
		query.setParameter(1, projectId);
		return query.executeUpdate();
	}

	public ProjectCourse create(Integer projectId, Integer courseId, Integer courseFlowTypeId, Boolean enabled,
			Boolean selected) {
		ProjectCourse projectCourse = new ProjectCourse();

		Project project = projectDao.findById(projectId);
		projectCourse.setProject(project);

		Course course = courseDao.findById(courseId);
		projectCourse.setCourse(course);

		CourseFlowType courseFlowType = courseFlowTypeDao.findById(courseFlowTypeId);
		projectCourse.setCourseFlowType(courseFlowType);

		projectCourse.setEnabled(enabled);
		projectCourse.setSelected(selected);

		getEntityManager().persist(projectCourse);

		return projectCourse;
	}

	public List<ProjectCourse> findProjectCourseByProjectId(Integer projectId) {
		log.debug("IN ProjectCourseDao.findProjectCourseByProjectId projectId: {}", projectId);
		TypedQuery<ProjectCourse> query = getEntityManager().createNamedQuery("getProjectCourseByProjectId",
				ProjectCourse.class);
		query.setParameter("projectId", projectId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public List<IntakeInstrumentData> getInstrumentsForIntake(int projectId, int langId) {
		Query query = getEntityManager().createNamedQuery("getInstrumentsForIntake");
		query.setParameter(1, langId);
		query.setParameter(2, projectId);
		List<Object[]> rows = query.getResultList();

		List<IntakeInstrumentData> list = new ArrayList<IntakeInstrumentData>();
		for (Object[] row : rows) {
			IntakeInstrumentData data = new IntakeInstrumentData((String) row[0], (Integer) row[1], (String) row[2],
					(String) row[3], ((Integer) row[4]).intValue() == 1);
			list.add(data);
		}
		return list;
	}
}
