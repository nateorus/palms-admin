package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Product;
import com.pdinh.persistence.ms.entity.ProductContent;
import com.pdinh.persistence.ms.entity.ProductContentType;

@Stateless
@LocalBean
public class ProductContentTypeDao extends GenericDao<ProductContentType, Integer> {

}
