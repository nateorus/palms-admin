package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.PhoneNumber;

@Stateless
@LocalBean
public class PhoneNumberDao extends GenericDao<PhoneNumber, Integer> {

	public void create(List<PhoneNumber> numbers){
		for (PhoneNumber number : numbers){
			create(number);
		}
	}

	public List<PhoneNumber> findByUsersIdAndType(Integer usersId, String typeAbbv){
		TypedQuery<PhoneNumber> typedQuery = getEntityManager().createNamedQuery("findByUsersIdAndType", PhoneNumber.class);
		typedQuery.setParameter("usersId", usersId);
		typedQuery.setParameter("type", typeAbbv);
		try{
			return typedQuery.getResultList();
		}catch(NoResultException e){
			return null;
		}
	}

	public List<PhoneNumber> findByUsersIdListAndType(List<Integer> usersIdList, String typeAbbv) {
		TypedQuery<PhoneNumber> typedQuery = getEntityManager().createNamedQuery("findByUsersIdListAndType", PhoneNumber.class);
		typedQuery.setParameter("usersIdList", usersIdList);
		typedQuery.setParameter("type", typeAbbv);
		try{
			return typedQuery.getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
}
