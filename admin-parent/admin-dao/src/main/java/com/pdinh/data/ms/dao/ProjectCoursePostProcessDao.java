/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ProjectCoursePostProcess;

/**
 * ProjectCoursePostProcessDao bean implementation
 */
// TODO Should this be implemented with overrides here and methods declared in daoClient (consistent with BizSimServer)?
//		Or should it be implemented as seen here (consistent with PDINHServer)?
@Stateless
@LocalBean
public class ProjectCoursePostProcessDao extends GenericDao<ProjectCoursePostProcess, Integer>
{
	/*
	 * get a list of entries that contain this course
	 */
	public List<ProjectCoursePostProcess> findByCourseId(int courseId)
	{
		TypedQuery<ProjectCoursePostProcess> t = entityManager.createNamedQuery("findByCourseId",ProjectCoursePostProcess.class);
		t.setParameter("courseId", courseId);

		try
		{
			return t.getResultList();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

	public List<ProjectCoursePostProcess> findByProjectId(int projectId)
	{
		/*
		 * get a list of entries that contain this project
		 */
		TypedQuery<ProjectCoursePostProcess> t = entityManager.createNamedQuery("findByProjectId",ProjectCoursePostProcess.class);
		t.setParameter("projectId", projectId);

		try
		{
			return t.getResultList();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}
	
	public ProjectCoursePostProcess findByProjectIdAndCourseId(int projectId, int courseId)
	{
		/*
		 * get a row from the table 
		 */
		TypedQuery<ProjectCoursePostProcess> t = entityManager.createNamedQuery("findByProjectIdAndCourseId",ProjectCoursePostProcess.class);
		t.setParameter("projectId", projectId);
		t.setParameter("courseId", courseId);

		try
		{
			return t.getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}

	}
	
    public int deleteAllForProject(Integer projectId) {
    	Query query = getEntityManager().createQuery("delete from ProjectCoursePostProcess pcpp WHERE pcpp.projectId=?1");
    	query.setParameter(1, projectId);
    	return query.executeUpdate();
    }

}
