package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ReportType;

/**
 * Session Bean implementation class ReportTypeDao
 */
@Stateless
@LocalBean
public class ReportTypeDao extends GenericDao<ReportType,Integer> {
	
	public List<ReportType> findReportTypesByProjectTypeIds(List<Integer> projectTypeIds) {		
		TypedQuery<ReportType> query = entityManager.createNamedQuery("findReportTypesByProjectTypeIds",ReportType.class);
		query.setParameter("projectTypeIds", projectTypeIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public ReportType findReportTypeByCode(String code){
		TypedQuery<ReportType> query = entityManager.createNamedQuery("findReportTypeByCode", ReportType.class);
		query.setParameter("code", code);
		try{
			return query.getSingleResult();
		}catch (NoResultException nre){
			return null;
		}
	}
}
