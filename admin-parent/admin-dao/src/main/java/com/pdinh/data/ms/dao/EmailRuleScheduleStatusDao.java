package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;


@Stateless
@LocalBean
public class EmailRuleScheduleStatusDao extends GenericDao<EmailRuleScheduleStatus, Integer> {
	
	public EmailRuleScheduleStatus findStatusByCode(String code) {
	    TypedQuery<EmailRuleScheduleStatus> query = getEntityManager().createNamedQuery("findStatusByCode",EmailRuleScheduleStatus.class);
		query.setParameter("code", code);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}

	}
}