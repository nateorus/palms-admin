package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CourseGroupTemplate;


/**
 * Session Bean implementation class AbdCourseDao
 */
@Stateless
@LocalBean
public class CourseGroupTemplateDao extends GenericDao<CourseGroupTemplate, Integer> {
	public List<CourseGroupTemplate> findCourseGroupTemplatesByProjectTypeId(Integer projectTypeId) {
	    TypedQuery<CourseGroupTemplate> query = getEntityManager().createNamedQuery("findAllCourseGroupTemplateByProjectTypeId",CourseGroupTemplate.class);
		query.setParameter("projectTypeId", projectTypeId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}

	}
	
	public List<CourseGroupTemplate> findByProjectTypeAndCourseFlowType(Integer projectTypeId, Integer courseFlowTypeId) {
    	String sql = "SELECT cgt FROM CourseGroupTemplate cgt WHERE cgt.projectType.projectTypeId=?1 AND cgt.courseFlowType.courseFlowTypeId=?2 ORDER BY cgt.sequence";
    	TypedQuery<CourseGroupTemplate> query = getEntityManager().createQuery(sql,CourseGroupTemplate.class);
    	query.setParameter(1, projectTypeId);
    	query.setParameter(2, courseFlowTypeId);
		try {
	    	return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
    }
	
	public List<CourseGroupTemplate> findAllCourseGroupTemplatesOrdered() {
		TypedQuery<CourseGroupTemplate> query = getEntityManager().createNamedQuery("findAllCourseGroupTemplatesOrdered",CourseGroupTemplate.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
