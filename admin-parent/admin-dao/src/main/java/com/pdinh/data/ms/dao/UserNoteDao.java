package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.UserNote;

@Stateless
@LocalBean
public class UserNoteDao extends GenericDao<UserNote,Integer>{
    
	public List<UserNote> findNotesByUserId(int userId) {
	    TypedQuery<UserNote> pnquery = getEntityManager().createNamedQuery("getNotesByUser",UserNote.class);
		pnquery.setParameter("userId", userId);
    	return pnquery.getResultList();
	}

}
