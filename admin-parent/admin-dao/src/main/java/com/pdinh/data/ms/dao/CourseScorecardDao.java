package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CourseScorecard;


/**
 * Session Bean implementation class ProjectUserDao
 */
@Stateless
@LocalBean
public class CourseScorecardDao extends GenericDao<CourseScorecard, Integer>  {

    /**
     * Default constructor. 
     */
    public CourseScorecardDao() {
        // TODO Auto-generated constructor stub
    }

    public CourseScorecard findByCourse(int course) {
    	
    	TypedQuery<CourseScorecard> t = entityManager.createNamedQuery("findCourseScorecardByCourse",CourseScorecard.class);
		t.setParameter("course", course);
		
		try {
			return t.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
    }
}
