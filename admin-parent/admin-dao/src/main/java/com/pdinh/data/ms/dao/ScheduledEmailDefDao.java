package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.AssessmentCourse;
import com.pdinh.persistence.ms.entity.ConsentText;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;

@Stateless
@LocalBean
public class ScheduledEmailDefDao extends GenericDao<ScheduledEmailDef, Integer> {

	private static final Logger log = LoggerFactory.getLogger(ScheduledEmailDefDao.class);
	public List<ScheduledEmailDef> findAllTemplates() {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findAllTemplates",
				ScheduledEmailDef.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ScheduledEmailDef> findAllTemplatesByType(String emailType) {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findAllTemplatesByType",
				ScheduledEmailDef.class);
		query.setParameter("emailType", emailType);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ScheduledEmailDef> findAllTemplatesByCompanyProductType(Integer companyId) {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery(
				"findAllTemplatesByCompanyProductType", ScheduledEmailDef.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<ScheduledEmailDef> findSysAndClientTemplatesByConstant(String constant, Integer companyId) {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery(
				"findSysAndClientTemplatesByConstant", ScheduledEmailDef.class);
		query.setParameter("companyId", companyId);
		query.setParameter("constant", constant);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ScheduledEmailDef> findClientLevelTemplates(Integer companyId) {
		log.debug("In findClientLevelTemplates()");
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findClientLevelTemplates",
				ScheduledEmailDef.class);
		//query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		query.setParameter("companyId", companyId);
		try {
			List<ScheduledEmailDef> results = query.getResultList();
			log.debug("Returning {} results", results.size());
			return results;
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ScheduledEmailDef> findSystemAndProductLevelTemplates() {
		log.debug("In findSystemAndProductLevelTemplates()");
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findSystemAndProductLevelTemplates",
				ScheduledEmailDef.class);
		try {
			List<ScheduledEmailDef> results =  query.getResultList();
			log.debug("Returning {} results", results.size());
			return results;
		} catch (NoResultException nre) {
			return null;
		}
	}
	

	public List<ScheduledEmailDef> findAllTemplatesByProject(Integer projectId) {
		log.debug("In findAllTemplatesByProject");
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findAllTemplatesByProject",
				ScheduledEmailDef.class);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		//query.setParameter("companyId", companyId);
		query.setParameter("projectId", projectId);
		try {
			List<ScheduledEmailDef> results = query.getResultList();
			log.debug("Returning {} results", results.size());
			return results;
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ScheduledEmailDef> findAllTemplatesAssessment() {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findAllTemplatesAssessment",
				ScheduledEmailDef.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<ScheduledEmailDef> findAllSystemTemplates() {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findAllSystemTemplates",
				ScheduledEmailDef.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}			
	}
	
	public List<ScheduledEmailDef> findAllGlobalTemplates() {
		log.debug("In findAllGlobalTemplates ()");
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findAllGlobalTemplates",
				ScheduledEmailDef.class);
		try {
			List<ScheduledEmailDef> results = query.getResultList();
			log.debug("Returning {} results", results.size());
			return results;
		} catch (NoResultException nre) {
			return null;
		}			
	}
	
	public ScheduledEmailDef  findTemplateById() {
		TypedQuery<ScheduledEmailDef> query = getEntityManager().createNamedQuery("findTemplateById",
				ScheduledEmailDef.class);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}	
	
	public HashMap<String, String> getTemplateTypes() {
		String sql = "select distinct sed.emailType, sed.emailTypeProdLabel from ScheduledEmailDef sed";
		Query query = getEntityManager().createNativeQuery(sql);
		HashMap<String, String>  map = new HashMap<String, String>();
		List<Object> list = new ArrayList<Object>();
		try {
			list  =   query.getResultList();

			int x = 0;
			for(int i = 0; i < list.size(); i++){				
				Object[] row  = (Object[]) list.get(i);
				//System.out.println( " index: " +  i + "  " + Arrays.toString(row));
				//System.out.println(" item 1,   item 2 " + row[0] + " ,   " + row[1]);
				if(row[0] == null || row[1] == null){
					continue;
				}
				map.put((String) row[1], (String) row[0]); 
				
			}
				
			return map;
		} catch (NoResultException nre) {
			return null;
		}
	}

	public HashMap<String, String>  getTemplateConstants() {
		String sql = "SELECT distinct sed.constant, sed.constantStyleLabel FROM ScheduledEmailDef sed";
		Query query = getEntityManager().createNativeQuery(sql);
		HashMap<String, String>  map = new HashMap<String, String>();
		List<Object> list = new ArrayList<Object>();
		try {
			list  =   query.getResultList();

			int x = 0;
			for(int i = 0; i < list.size(); i++){				
				Object[] row  = (Object[]) list.get(i);
				//System.out.println( " index: " +  i + "  " + Arrays.toString(row));				
				if(row[0] == null || row[1] == null){
					continue;
				}
				map.put((String) row[1], (String) row[0]); 
				//System.out.println(" item 1,   item 2 " + row[0] + " ,   " + row[1]);
			}
				
			return map;
		} catch (NoResultException nre) {
			return null;
		}
	}

	
	public List<ScheduledEmailDef> findAllClientTemplates(Integer companyId) {
		log.debug("In findAllClientTemplates() {}", companyId);
		List<ScheduledEmailDef> listOfClientTemplates = new ArrayList<ScheduledEmailDef>();
		listOfClientTemplates.addAll(findAllGlobalTemplates());
		listOfClientTemplates.addAll(findClientLevelTemplates(companyId));
		log.debug("Returning {} Results", listOfClientTemplates.size());
		return listOfClientTemplates;
	}	

	public List<ScheduledEmailDef> findAllProjectTemplates(Integer companyId, Integer projectId) {
		log.debug("In findAllProjectTemplates()");
		List<ScheduledEmailDef> listOfProjectTemplates = new ArrayList<ScheduledEmailDef>();
		listOfProjectTemplates.addAll(findAllClientTemplates(companyId));
		listOfProjectTemplates.addAll(findAllTemplatesByProject(projectId));
		log.debug("returning {} results", listOfProjectTemplates.size());
		return listOfProjectTemplates;
	}
	
}
	
	
	
	

