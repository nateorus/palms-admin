package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.PhoneNumberType;
@Stateless
@LocalBean
public class PhoneNumberTypeDao extends GenericDao<PhoneNumberType, Integer> {

	public PhoneNumberType getByAbbv(String abbv){
		TypedQuery<PhoneNumberType> query = getEntityManager().createNamedQuery("getByAbbv", PhoneNumberType.class);
		query.setParameter("abbv", abbv);
		try{
			return query.getSingleResult();
		}catch (NoResultException e){
			return null;
		}
	}
}
