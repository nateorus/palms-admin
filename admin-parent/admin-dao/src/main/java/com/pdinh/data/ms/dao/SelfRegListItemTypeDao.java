package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.SelfRegListItemType;

@Stateless
@LocalBean
public class SelfRegListItemTypeDao extends GenericDao<SelfRegListItemType, Integer>{

}
