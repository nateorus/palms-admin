package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ConsentText;

@Stateless
@LocalBean
public class ConsentTextDao extends GenericDao<ConsentText, Integer> {

	public ConsentTextDao() {
		super();
	}

	public ConsentText findForUserByTypeLangLocation(int companyId, String consentType, String lang, String location,
			int consentId) {
		TypedQuery<ConsentText> query = getEntityManager().createNamedQuery("findForUserByTypeLangLocation",
				ConsentText.class);
		query.setParameter("companyId", companyId);
		query.setParameter("consentType", consentType);
		query.setParameter("lang", lang);
		query.setParameter("constant", location);
		query.setParameter("consentId", consentId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			System.out.println("No custom consent for type " + consentType + ", lang " + lang + ", location "
					+ location + ", companyId " + companyId);
			try {
				return findDefaultForTypeLangLocation(consentType, lang, location, consentId);
			} catch (Exception e) {
				System.err.print("EXCEPTION caught... " + e.getMessage());
				return null;
			}
		}
	}

	public ConsentText findDefaultForTypeLangLocation(String consentType, String lang, String location, int consentId) {
		TypedQuery<ConsentText> query = getEntityManager().createNamedQuery("findDefaultForTypeLangLocation",
				ConsentText.class);
		query.setParameter("consentType", consentType);
		query.setParameter("lang", lang);
		query.setParameter("constant", location);
		query.setParameter("consentId", consentId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			// If none found, try English
			System.out.print("Nothing found for language given, defaulting to en. " + nre.getMessage());
			query = getEntityManager().createNamedQuery("findDefaultForTypeLangLocation", ConsentText.class);
			query.setParameter("consentType", consentType);
			query.setParameter("lang", "en");
			query.setParameter("constant", location);
			query.setParameter("consentId", consentId);
			try {
				return query.getSingleResult();
			} catch (NoResultException nre2) {
				System.err.print("EXCEPTION caught!!! " + nre2.getMessage());
				return null;
			}
		}
	}
}
