package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.CoachingPlanStatusType;

@Stateless
@LocalBean
public class CoachingPlanStatusTypeDao extends GenericDao<CoachingPlanStatusType, Integer> {
}
