package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;

@Stateless
@LocalBean
public class PasswordPolicyDao extends GenericDao<PasswordPolicy, Integer> {

	public PasswordPolicy findTemplatePolicyByConstant(String constant){
		TypedQuery<PasswordPolicy> query = getEntityManager().createNamedQuery("findTemplatePolicyByConstant", PasswordPolicy.class);
		query.setParameter("constant", constant);
		try{
			return query.getSingleResult();
		}catch (NoResultException e){
			return null;
		}
	}
}
