package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.ProjectUser;

/**
 * Session Bean implementation class ProjectUserDao
 */
@Stateless
@LocalBean
public class ProjectUserDao extends GenericDao<ProjectUser, Integer> {

	private static final Logger log = LoggerFactory.getLogger(ProjectUserDao.class);

	/**
	 * Default constructor.
	 */
	public ProjectUserDao() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * Need to set the participantsAdded attribute on 
	 * the Project, to indicate that participants have 
	 * been added, and so the project can be locked in
	 * AbyD setup.  Requirement based on NHN-1817
	 * 
	 */
	/*  Doing this in Create User service now
	@Override
	public ProjectUser create(ProjectUser pu) {
		ProjectUser ret = super.create(pu);

		if (ret.getProject().getProjectType().getProjectTypeId() == ProjectType.ASSESSMENT_BY_DESIGN_ONLINE
				|| ret.getProject().getProjectType().getProjectTypeId() == ProjectType.READINESS_ASSMT_BUL
				|| ret.getProject().getProjectType().getProjectTypeId() == ProjectType.MLL_LVA) {
			if (!pu.getProject().getParticipantsAdded()) {
				pu.getProject().setParticipantsAdded(true);
				super.update(pu);
			}
		}

		return ret;
	}
	*/

	/**
	 * findProjectUsersFiltered
	 * 
	 * @param projectId
	 *            The PALMS ID of the desired project
	 * @param usersIds
	 *            A list of user IDs. Only returns records where the user id is
	 *            in the provided list.
	 * @return A list of ProjectUser objects in the specified project, whose IDs
	 *         are found in the list.
	 */

	public List<ProjectUser> findProjectUsersFiltered(int projectId, List<Integer> usersIds) {
		log.debug("IN ProjectUserDao.findProjectUsersFiltered projectId: {}, Authorized user ids: {}", projectId,
				usersIds);
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findProjectUsersFiltered",
				ProjectUser.class);
		query.setParameter("projectId", projectId);
		query.setParameter("usersIds", usersIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<ProjectUser> findProjectUsersByProjectId(int projectId) {
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findProjectUsersByProjectId",
				ProjectUser.class);
		query.setParameter("projectId", projectId);

		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Return a subset of ProjectUser entities from the given project. Used in
	 * conjunction with the EntityService getEntityIds() method to return only
	 * authorized users in the project
	 * 
	 * @param projectId
	 *            - the id of the Project to search
	 * @param projectUserIds
	 *            - a collection of rowIds from ProjectUser entities
	 * @return
	 */

	public List<ProjectUser> findProjectUsersByIds(int projectId, List<Integer> projectUserIds) {
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findProjectUsersByIds", ProjectUser.class);
		if (projectUserIds.isEmpty()) {
			projectUserIds.add(0);
		}

		query.setParameter("projectId", projectId);
		query.setParameter("projectUserIds", projectUserIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * findByProjectIdAndUserId
	 * 
	 * @param projectId
	 *            The PALMS ID of the desired project
	 * @param userId
	 *            The PALMS ID of the desired user
	 * @return The desired ProjectUser object
	 */
	public ProjectUser findByProjectIdAndUserId(Integer projectId, Integer userId) {
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findByProjectAndUser", ProjectUser.class);
		query.setParameter("projectId", projectId);
		query.setParameter("userId", userId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * findProjectUsersByUserId
	 * 
	 * @param userId
	 *            The PALMS ID of the desired user
	 * @return A list of ProjectUser objects in the specified userId.
	 */
	public List<ProjectUser> findProjectUsersByUserId(Integer userId) {
		TypedQuery<ProjectUser> query = getEntityManager().createNamedQuery("findProjectUsersByUserId",
				ProjectUser.class);
		query.setParameter("userId", userId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
