package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Consent;

@Stateless
@LocalBean
public class ConsentDao extends GenericDao<Consent, Integer> {

	public Consent findByCompany(int companyId) {
		TypedQuery<Consent> query = getEntityManager().createNamedQuery("findByCompany", Consent.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Consent findDefaultConsent(int companyId) {
		Consent consent;
		consent = findByCompany(companyId);
		if (consent == null) {
			TypedQuery<Consent> query = getEntityManager().createNamedQuery("findDefaultConsent", Consent.class);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		} else {
			return consent;
		}
	}

	public List<Consent> findAllConsentVersionsByCompany(int companyId) {
		TypedQuery<Consent> query = getEntityManager().createNamedQuery("findAllConsentVersionsByCompany",
				Consent.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Consent>();
		}
	}

	public List<Consent> findAllDefaultConsentVersions(int companyId) {
		List<Consent> consents = findAllConsentVersionsByCompany(companyId);
		if (consents.isEmpty()) {
			TypedQuery<Consent> query = getEntityManager().createNamedQuery("findAllDefaultConsentVersions",
					Consent.class);
			try {
				return query.getResultList();
			} catch (NoResultException e) {
				return null;
			}
		} else {
			return consents;
		}
	}

}
