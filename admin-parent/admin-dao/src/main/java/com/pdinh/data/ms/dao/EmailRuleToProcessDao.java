package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.EmailRuleToProcess;

@Stateless
@LocalBean
public class EmailRuleToProcessDao extends GenericDao<EmailRuleToProcess, Integer> {

	public EmailRuleToProcess findEmailRuleToProcessByScheduleId(int emailRuleScheduleId) {
		TypedQuery<EmailRuleToProcess> emailRuleToProcess = getEntityManager().createNamedQuery(
				"findEmailRuleToProcessByScheduleId", EmailRuleToProcess.class);
		emailRuleToProcess.setParameter("emailRuleScheduleId", emailRuleScheduleId);

		try {
			return emailRuleToProcess.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<EmailRuleToProcess> findEmailRuleToProcessByProjectId(int projectId) {
		TypedQuery<EmailRuleToProcess> emailRuleToProcess = getEntityManager().createNamedQuery(
				"findEmailRuleToProcessByProjectId", EmailRuleToProcess.class);
		emailRuleToProcess.setParameter("projectId", projectId);

		try {
			return emailRuleToProcess.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

}