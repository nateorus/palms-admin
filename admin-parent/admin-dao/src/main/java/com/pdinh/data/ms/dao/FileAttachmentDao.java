package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.FileAttachment;

@Stateless
@LocalBean
public class FileAttachmentDao extends GenericDao <FileAttachment,Integer> {
}
