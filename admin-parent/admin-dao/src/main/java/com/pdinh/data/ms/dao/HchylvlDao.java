package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.Hchylvl;

@Stateless
@LocalBean
public class HchylvlDao extends GenericDao<Hchylvl, Integer>{

	public HchylvlDao(){
		
	}
	
    public Hchylvl findByLeveName(String levelName, int companyId) {
		TypedQuery<Hchylvl> query  = getEntityManager().createNamedQuery("findByLevelByName",Hchylvl.class);
		query.setParameter("levelName", levelName);
		query.setParameter("companyId", companyId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
}
