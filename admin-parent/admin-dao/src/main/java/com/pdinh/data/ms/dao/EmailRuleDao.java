package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.EmailRule;

@Stateless
@LocalBean
public class EmailRuleDao extends GenericDao<EmailRule, Integer> {

	public List<EmailRule> findEmailRulesByProjectId(int projectId) {
		TypedQuery<EmailRule> emailRule = getEntityManager().createNamedQuery("findEmailRulesByProjectId",
				EmailRule.class);
		emailRule.setParameter("projectId", projectId);

		try {
			return emailRule.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<EmailRule> findEmailRulesByEmailTemplateId(int emailId) {
		TypedQuery<EmailRule> emailRule = getEntityManager().createNamedQuery("findEmailRulesByEmailTemplateId",
				EmailRule.class);
		emailRule.setParameter("emailId", emailId);

		try {
			return emailRule.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}