package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.OfficeLocation;

@Stateless
@LocalBean
public class OfficeLocationDao extends GenericDao<OfficeLocation, Integer> {

	/**
	 * Default constructor.
	 */
	public OfficeLocationDao() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * findAllOfficeLocationsOrdered - Gets offices in alpha order, physical
	 * offices first, then virtual ones. If we need more flexibility at some
	 * point, then we should refactor this method (and, possibly, the database)
	 * to provide that flexibility at that time. Likewise, if we need either the
	 * physical or the non-physical offices as separate lists, then we can
	 * re-factor to add the requisite methods at that point in time
	 * 
	 * NOTE: This should only be used by the ProjectSetupSingleton. Please get
	 * your office location lists from that source only.
	 * 
	 * @return
	 */
	public List<OfficeLocation> findAllOfficeLocationsOrdered() {
		TypedQuery<OfficeLocation> query = getEntityManager().createNamedQuery("findAllOfficeLocationsOrdered",
				OfficeLocation.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
