package com.pdinh.data.ms.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.dto.BulkUploadUser;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailStatus;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.persistence.ms.entity.UserCoursePrefs;

/**
 * Session Bean implementation class UserDao
 */
@Stateless
@LocalBean
public class UserDao extends GenericDao<User, Integer> {

	private static final Logger log = LoggerFactory.getLogger(UserDao.class);
	@EJB
	private LanguageDao languageDao;

	@EJB
	private ProjectDao projectDao;

	//
	// Static variables
	//
	/*
	static private String CONNECTION_FACTORY = "jms/PalmsConnectionFactory";
	private static final String CONNECTION_QUEUE = "jms/PptUpdateQueue";
	*/
	@Override
	public User create(User user) {
		UUID guid = UUID.randomUUID();
		String rowguid = guid.toString();
		guid = UUID.randomUUID();
		String rowguid51 = guid.toString();
		Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());

		user.setRowguid(rowguid);
		user.setRowguid51(rowguid51);
		user.setStartdate(sqlDate);

		getEntityManager().persist(user);
		// getEntityManager().flush();

		return user;
	}

	private void projectNorms(Project project) {
		// TODO: check to see if user is in a project that has norms
	}

	public User update(Integer userId, String firstName, String lastName, String email, String username) {
		User user = findById(userId);
		user.setFirstname(firstName);
		user.setLastname(lastName);
		user.setEmail(email);
		user.setUsername(username);
		getEntityManager().persist(user);
		/*
				sendUserUpdateMsg(user.getUsersId());
		*/
		return user;
	}

	/**
	 * Update method for User objects - Overrides the generic method so we can
	 * put an update message on the queue.
	 * 
	 * @param user
	 *            - The User object that contains the updated information
	 */
	/* This doesn't work anymore.  finding different solution.  see LRMI-294
	@Override
	public void update(User user) {
		// ----- Copied from the GenericDao class
		getEntityManager().merge(user);
		// Not yet sure if need this
		// getEntityManager().flush();
		// -----

		sendUserUpdateMsg(user.getUsersId());
	}
	*/

	/**
	 * Method that sends the update message out. Note that there is no
	 * validation on this end.
	 * 
	 * @param userId
	 */
	/*
	private void sendUserUpdateMsg(int userId) {
		Connection connection = null;

		// Get the JNDI Context
		try {
			Context jndiContext = new InitialContext();

			// Create the Connection Factory
			ConnectionFactory connectionFactory = null;
			connectionFactory = (ConnectionFactory) jndiContext.lookup(CONNECTION_FACTORY);
			connection = connectionFactory.createConnection();

			// Create the session
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Initialize Message Producer
			MessageProducer producer = null;

			// Create new queue
			Queue queue = (Queue) jndiContext.lookup(CONNECTION_QUEUE);

			// Create Message Producer
			producer = session.createProducer(queue);

			// Send the message
			TextMessage tm = session.createTextMessage("" + userId);
			producer.send(tm);
			log.debug("Ppt Update Message for ppt {} sent...", userId);
		} catch (NamingException e) {
			log.error("ERROR:  NamingException detected in sendUserUpdateMsg(): {}", e.getMessage());
			e.printStackTrace();
		} catch (JMSException e) {
			log.error("ERROR:  JMSException detected in sendUserUpdateMsg(): {}", e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (JMSException e) {
				log.error("Error closing JMS connection: {}", e.getMessage());
				e.printStackTrace();
			}
		}
	}
	*/

	/**
	 * 
	 * @param companyId
	 * @param projectId
	 * @return
	 */
	public List<User> findUsersNotInProjectByProjectId(int companyId, int projectId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findUsersNotInProjectByProjectId", User.class);
		query.setParameter("companyId", companyId);
		query.setParameter("projectId", projectId);
		return query.getResultList();
	}

	public List<User> findActiveUsersByCompanyId(int companyId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findActiveUsersByCompanyId", User.class);
		query.setParameter("companyId", companyId);
		return query.getResultList();
	}

	public List<User> findActiveUsersByCompanyIdPaged(int companyId, int startingAt, int maxPerPage) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findActiveUsersByCompanyId", User.class);
		query.setParameter("companyId", companyId);
		query.setMaxResults(maxPerPage);
		query.setFirstResult(startingAt);
		return query.getResultList();
	}

	public List<UserCoursePrefs> findUserCoursePrefs(int userId) {
		TypedQuery<UserCoursePrefs> ucpquery = getEntityManager().createNamedQuery("getLanguagePrefsByUser",
				UserCoursePrefs.class);
		ucpquery.setParameter("userId", userId);
		return ucpquery.getResultList();
	}

	public List<User> findSuperAdminUsers(int companyId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findSuperAdminUsers", User.class);
		query.setParameter("companyId", companyId);
		return query.getResultList();
	}

	public List<User> findAdminUsers(int companyId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findAdminUsers", User.class);
		query.setParameter("companyId", companyId);
		return query.getResultList();
	}

	public List<User> findByEmailAndCompany(String email, int companyId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("getUserByEmailAndCompany", User.class);
		query.setParameter("email", email);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<User> findByUsername(String username, boolean refresh) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("getUserByUsername", User.class);
		query.setParameter("username", username);
		if (refresh) {
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		}
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<User> findAllActive() {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findAllActive", User.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<User> findAllCoaches() {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findAllCoaches", User.class);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public int isUniqueUsername(String username) {
		TypedQuery<Long> query = getEntityManager().createNamedQuery("isUniqueUsername", Long.class);
		query.setParameter("username", username);
		try {
			return query.getSingleResult().intValue();
		} catch (NoResultException nre) {
			return 0;
		}
	}

	public int isUniqueUsernameForUser(String username, int usersId) {
		TypedQuery<Long> query = getEntityManager().createNamedQuery("isUniqueUsernameForUser", Long.class);
		query.setParameter("username", username);
		query.setParameter("usersId", usersId);
		try {
			return query.getSingleResult().intValue();
		} catch (NoResultException nre) {
			return 0;
		}
	}

	public UserCoursePrefs updateCourseLanguagePref(User user, Course course, String langCode) {
		// System.out.println("in UserDao.updateCourseLanguagePref");
		TypedQuery<UserCoursePrefs> query = getEntityManager().createNamedQuery("getLanguagePrefByUserAndCourseAbbv",
				UserCoursePrefs.class);
		query.setParameter("userId", user.getUsersId());
		query.setParameter("courseAbbv", course.getAbbv());
		UserCoursePrefs ucp = null;
		try {
			ucp = query.getSingleResult();
		} catch (NoResultException nre) {
			ucp = null;
		}
		if (ucp == null) {
			// System.out.println("not found, create");
			ucp = new UserCoursePrefs();
			ucp.setUser(user);
			ucp.setCourse(course);
		} else {
			; // System.out.println("found ucp " + ucp.getLanguage().getCode());
		}

		Language language = languageDao.findByCode(langCode);

		if (language != null) {
			ucp.setLanguage(language);
			getEntityManager().persist(ucp);
			// System.out.println("updated language to " + language.getCode());
		} else {
			// System.out.println("language code " + langCode +
			// " isn't supported, user course pref not updated for user: " +
			// user.getUsername() + " id: " +
			// user.getUsersId() + " course: " + course.getAbbv());
			ucp = null;
		}

		return ucp;
	}

	public Timestamp resolveInitialEmail(User user, Project project) {
		// System.out.println("In resolveInitialEmail: " +
		// user.getScheduledEmailStatuses().size());
		for (ScheduledEmailStatus ses : user.getScheduledEmailStatuses()) {
			if (ses.getEmailConstant().equals(ScheduledEmailStatus.FIRST_PASSWORD)
					&& ses.getProject().getProjectId() == project.getProjectId()) {
				return ses.getDateCompleted();
			}
		}
		return null;
	}

	public Timestamp resolveReminderEmail(User user, Project project) {
		// System.out.println("In resolveReminderEmail: " +
		// user.getScheduledEmailStatuses().size());
		for (ScheduledEmailStatus ses : user.getScheduledEmailStatuses()) {
			if (ses.getEmailConstant().equals(ScheduledEmailStatus.PARTICIPANT_REMINDER)
					&& ses.getProject().getProjectId() == project.getProjectId()) {
				return ses.getDateCompleted();
			}
		}
		return null;
	}

	public List<User> findActiveUnlinkedInCompany(Integer companyId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findActiveUnlinkedInCompany", User.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<User> findAuthorizedUsersInCompany(Integer companyId, List<Integer> projectIds,
			List<Integer> projectUserIds, List<Integer> userIds) {
		TypedQuery<User> typedQuery = entityManager
				.createQuery(
						"SELECT u FROM User u LEFT JOIN u.projectUsers pu WHERE u.company.companyId = :companyId and ((pu.project.projectId IN :projectIds) or (pu.rowId IN :projectUserIds) or (u.usersId IN :userIds))",
						User.class);
		typedQuery.setHint(QueryHints.BATCH_TYPE, "EXISTS");

		typedQuery.setParameter("companyId", companyId);
		typedQuery.setParameter("userIds", userIds);
		typedQuery.setParameter("projectIds", projectIds);
		typedQuery.setParameter("projectUserIds", projectUserIds);

		try {
			List<User> users = typedQuery.getResultList();
			return users;
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<User> findTinCanUsersInCompany(Integer companyId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findTinCanUsersByCompanyId", User.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public List<User> findByCompanyIdAndProjectTypeId(Integer companyId, Integer projectTypeId) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findByCompanyIdAndProjectTypeId", User.class);
		query.setParameter("companyId", companyId);
		query.setParameter("projectTypeId", projectTypeId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<BulkUploadUser> findUserDataForBulkUpload(int companyId, Set<String> userEmails) {
		TypedQuery<BulkUploadUser> query = getEntityManager().createNamedQuery("findUserDataForBulkUpload",
				BulkUploadUser.class);
		query.setParameter("companyId", companyId);
		query.setParameter("userEmails", userEmails);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<User> findUsersInList(List<Integer> userIds) {
		TypedQuery<User> query = getEntityManager().createNamedQuery("findUsersInList", User.class);
		query.setParameter("userIds", userIds);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return new ArrayList<User>();
		}
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public User resetPassword(byte[] bs, String password, User user) {
		user.setHashedpassword(bs);
		user.setPassword(password);
		user.setLoginStatus((short) 0);
		update(user);
		return user;
	}
}
