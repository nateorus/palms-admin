package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CourseRoster;
import com.pdinh.persistence.ms.entity.CourseVisibility;

@Stateless
@LocalBean
public class CourseRosterDao extends GenericDao<CourseRoster, Integer>{
	
	public List<CourseRoster> getCourseRostersByCompanyIdBatchCourse(Integer companyId){
		TypedQuery <CourseRoster> query = getEntityManager().createNamedQuery("getCourseRostersByCompanyIdBatchCourse", CourseRoster.class);
		query.setParameter("companyId", companyId);
		try {
			return query.getResultList();
		}catch (NoResultException nre){
			return null;
		}
	}
	
	public void deleteCourseRosterByCompanyIdAndCourseId(int companyId, int courseId){
		TypedQuery<CourseRoster> query = getEntityManager().createNamedQuery("deleteCourseRosterByCompanyIdAndCourseId", CourseRoster.class);
		query.setParameter("companyId", companyId);
		query.setParameter("courseId", courseId);
		query.executeUpdate();
		return;
	}
	
	public void deleteCourseRosterByCompanyIdAndCourseIds(int companyId, List<Integer> courseIds){
		TypedQuery<CourseRoster> query = getEntityManager().createNamedQuery("deleteCourseRosterByCompanyIdAndCourseIds", CourseRoster.class);
		query.setParameter("companyId", companyId);
		if (courseIds.isEmpty()){
			return;
			//courseIds.add(0);
		}
		query.setParameter("courseIds", courseIds);
		query.executeUpdate();
		return;
	}
	
	public void createBatch(List<CourseRoster> rosters){
		for (CourseRoster cr: rosters){
			this.create(cr);
		}
	}

}
