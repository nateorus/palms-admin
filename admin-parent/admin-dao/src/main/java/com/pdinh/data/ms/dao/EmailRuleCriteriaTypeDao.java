package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.EmailRuleCriteriaType;

@Stateless
@LocalBean
public class EmailRuleCriteriaTypeDao extends GenericDao<EmailRuleCriteriaType, Integer> {

}