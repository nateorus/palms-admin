package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;

@Stateless
@LocalBean
public class PasswordPolicyUserStatusDao extends GenericDao<PasswordPolicyUserStatus, Integer>{

	public List<PasswordPolicyUserStatus> findAllExpiringPasswords(){
		TypedQuery<PasswordPolicyUserStatus> query = getEntityManager().createNamedQuery("findAllExpiringPasswords", PasswordPolicyUserStatus.class);
		try{
			return query.getResultList();
		}catch (NoResultException e){
			return null;
		}
	}
	
	public List<PasswordPolicyUserStatus> findAllExpiredPasswords(){
		TypedQuery<PasswordPolicyUserStatus> query = getEntityManager().createNamedQuery("findAllExpiredPasswords", PasswordPolicyUserStatus.class);
		try{
			return query.getResultList();
		}catch (NoResultException e){
			return null;
		}
	}
	//not using currently... may need in future 
	public List<PasswordPolicyUserStatus> findAllInCompanyWithPolicy(Integer companyId, Integer userType){
		TypedQuery<PasswordPolicyUserStatus> query = getEntityManager().createNamedQuery("findAllInCompanyWithPolicy", PasswordPolicyUserStatus.class);
		query.setParameter("companyId", companyId);
		query.setParameter("userType", userType);
		try{
			return query.getResultList();
		}catch (NoResultException e){
			return null;
		}
	}
	
	public int updateUsersToNewPolicy(PasswordPolicy policy, Integer userType, Integer companyId){
		
		Query query = getEntityManager().createQuery("UPDATE PasswordPolicyUserStatus us SET us.policy = :policy WHERE us.user.type = :userType and us.user.company.companyId = :companyId");
		query.setParameter("policy", policy);
		query.setParameter("userType", userType);
		query.setParameter("companyId", companyId);
		try{
			return query.executeUpdate();
		}catch (Exception e){
			return 0;
		}
	}
}
