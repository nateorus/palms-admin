package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.EmailFromFieldKeyCombination;


/**
 * Session Bean implementation class EmailFromFieldKeyCombinationDao
 */
@Stateless
@LocalBean

public class EmailFromFieldKeyCombinationDao  extends  GenericDao<EmailFromFieldKeyCombination, Integer> {
// add new key combination 
// get all key combinations
	public EmailFromFieldKeyCombination  findKeyCombinationByKeyCode(String keyCode) {
		TypedQuery<EmailFromFieldKeyCombination> keyCombination = getEntityManager().createNamedQuery("findKeyCombinationByKeyCode",
				EmailFromFieldKeyCombination.class);
		keyCombination.setParameter("keyCode", keyCode);
		
		try {
			return keyCombination.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
}
