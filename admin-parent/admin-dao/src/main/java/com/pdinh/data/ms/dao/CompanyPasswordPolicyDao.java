package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CompanyPasswordPolicy;

@Stateless
@LocalBean
public class CompanyPasswordPolicyDao extends GenericDao<CompanyPasswordPolicy, Integer>{

	public List<CompanyPasswordPolicy> findByCompanyId(Integer companyId){
		TypedQuery query = entityManager.createNamedQuery("findByCompanyId", CompanyPasswordPolicy.class);
		query.setParameter("companyId", companyId);
		try{
			return query.getResultList();
		}catch (NoResultException e){
			return null;
		}
	}
}
