package com.pdinh.data.ms.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.CourseVersion;

@Stateless
@LocalBean
public class CourseVersionDao extends GenericDao<CourseVersion, Integer> {

	public List<CourseVersion> findCourseVersionsByCourseId(int courseId) {
		TypedQuery<CourseVersion> query = entityManager.createNamedQuery("findCourseVersionsByCourseId",
				CourseVersion.class);
		query.setParameter("courseId", courseId);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<CourseVersion>();
		}
	}

	public List<CourseVersion> findAllBatchCourse() {
		TypedQuery<CourseVersion> query = entityManager.createNamedQuery("findAllCourseVersionsBatchCourse",
				CourseVersion.class);

		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<CourseVersion>();
		}
	}

}
