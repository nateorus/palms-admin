package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.CustomProperty;

@Stateless
@LocalBean
public class CustomPropertyDao extends GenericDao<CustomProperty, Integer> {

}
