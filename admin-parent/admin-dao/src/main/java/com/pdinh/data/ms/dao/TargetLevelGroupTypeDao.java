/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.data.ms.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.TargetLevelGroup;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;

/*
 * TargetLevelGroupTypeDao
 */
@Stateless
@LocalBean
public class TargetLevelGroupTypeDao extends GenericDao<TargetLevelGroup, Integer>
{
	public List<TargetLevelGroupType> findAllGroupTypesBatch(){
		TypedQuery<TargetLevelGroupType> query = getEntityManager().createNamedQuery("findAllGroupTypesBatch", TargetLevelGroupType.class);
		
		try {
			return query.getResultList();
		}catch (NoResultException e){
			return null;
		}
	}
}
