package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.SelfRegListItem;

@Stateless
@LocalBean
public class SelfRegListItemDao extends GenericDao<SelfRegListItem, Integer> {

}
