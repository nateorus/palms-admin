package com.pdinh.data.ms.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.ScheduledEmailRuleDetailedStatus;

/**
 * Session Bean implementation class ScheduledEmailRuleDetailedStatusDao
 */
@Stateless
@LocalBean
public class ScheduledEmailRuleDetailedStatusDao  extends  GenericDao<ScheduledEmailRuleDetailedStatus, Integer>{

}
