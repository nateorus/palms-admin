package com.kf.enterprise.presentation.util;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;

@ManagedBean(name = "participantConverter")
@ViewScoped
@FacesConverter(value = "ParticipantConverter")
public class ParticipantConverter implements Converter {

	private List<ReportParticipantRepresentation> allParticipantObjs;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2.trim().equals(""))
			return null;
		if ((arg2 != null) && (!(arg2.equals(String.valueOf(arg0)))) && (!(arg2.equals("")))) {
			for (ReportParticipantRepresentation rpr : this.allParticipantObjs) {
				if (rpr.getUsersId() == Integer.parseInt(arg2))
					return rpr;
			}
			return null;
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if ((arg2 != null) && arg2 instanceof ReportParticipantRepresentation) {
			String pid = "" + ((ReportParticipantRepresentation) arg2).getUsersId();
			return pid;
		} else {
			return null;
		}
	}

	public List<ReportParticipantRepresentation> getAllParticipantObjs() {
		return allParticipantObjs;
	}

	public void setAllParticipantObjs(List<ReportParticipantRepresentation> allParticipantObjs) {
		this.allParticipantObjs = allParticipantObjs;
	}

}
