package com.kf.enterprise.presentation.util;

import java.util.Comparator;

import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;

public class ReportParticipantRepComparer implements Comparator<ReportParticipantRepresentation> {

	@Override
	public int compare(ReportParticipantRepresentation o1, ReportParticipantRepresentation o2) {
		if (!(o1 == null || o2 == null)) {
			String fl = o1.getFirstName() + " " + o1.getLastName();
			return fl.compareTo(o2.getFirstName() + " " + o2.getLastName());
		} else {
			System.out.println("comparedString object is NULL!");
			return 0;
		}
	}

}
