package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.ClientObj;
import com.pdinh.presentation.domain.ConsentObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean(name="clientListViewBean")
@ViewScoped
public class ClientListViewBean implements Serializable {
    private static final long serialVersionUID = 1L;

	private boolean manageProjectsDisabled = true;
	private boolean manageParticipantsDisabled = true;
	private boolean manageEmailTemplatesDisabled = true;
	private boolean editClientInfoDisabled = true;
	private boolean editPasswordPoliciesDisabled = true;
	private boolean productEnableDisabled = true;
    private boolean downloadReportsButtonDisabled = true;
	private boolean consentDisabled = true;

    private List<ClientObj> clients;
    private List<ParticipantObj> selections;
	private ClientObj selectedClient;
	private ConsentObj consent;
	private List<LanguageObj> assessmentLanguages;

    public void setClientObjs(List<ClientObj> clients) {
        this.clients = clients;
    }

    public List<ClientObj> getClientObjs() {
        return clients;
    }

    public void setSelections(List<ParticipantObj> selections) {
        this.selections = selections;
    }

    public List<ParticipantObj> getSelections() {
        return selections;
    }

	public void setManageProjectsDisabled(boolean manageProjectsDisabled) {
        this.manageProjectsDisabled = manageProjectsDisabled;
    }

	public boolean getManageProjectsDisabled() {
        return manageProjectsDisabled;
    }

	public void setManageParticipantsDisabled(boolean manageParticipantsDisabled) {
        this.manageParticipantsDisabled = manageParticipantsDisabled;
    }

	public boolean getManageParticipantsDisabled() {
        return manageParticipantsDisabled;
    }

	public void setManageEmailTemplatesDisabled(boolean manageEmailTemplatesDisabled) {
    	this.manageEmailTemplatesDisabled = manageEmailTemplatesDisabled;
    }

	public boolean getManageEmailTemplatesDisabled() {
        return manageEmailTemplatesDisabled;
    }

	public boolean getEditClientInfoDisabled() {
		return editClientInfoDisabled;
	}

	public void setEditClientInfoDisabled(boolean editClientInfoDisabled) {
		this.editClientInfoDisabled = editClientInfoDisabled;
	}

	public ClientObj getSelectedClient() {
		return selectedClient;
	}

	public void setSelectedClient(ClientObj selectedClient) {
		this.selectedClient = selectedClient;
	}

	public boolean getEditPasswordPoliciesDisabled() {
		return editPasswordPoliciesDisabled;
	}

	public void setEditPasswordPoliciesDisabled(
boolean editPasswordPoliciesDisabled) {
		this.editPasswordPoliciesDisabled = editPasswordPoliciesDisabled;
	}

	public boolean getProductEnableDisabled() {
		return productEnableDisabled;
	}

	public void setProductEnableDisabled(boolean productEnableDisabled) {
		this.productEnableDisabled = productEnableDisabled;
	}

	public boolean isDownloadReportsButtonDisabled() {
		return downloadReportsButtonDisabled;
	}

	public void setDownloadReportsButtonDisabled(
			boolean downloadReportsButtonDisabled) {
		this.downloadReportsButtonDisabled = downloadReportsButtonDisabled;
	}

	public ConsentObj getConsent() {
		return consent;
	}

	public void setConsent(ConsentObj consent) {
		this.consent = consent;
	}

	public boolean isConsentDisabled() {
		return consentDisabled;
	}

	public void setConsentDisabled(boolean consentDisabled) {
		this.consentDisabled = consentDisabled;
	}

	public List<LanguageObj> getAssessmentLanguages() {
		return assessmentLanguages;
	}

	public void setAssessmentLanguages(List<LanguageObj> assessmentLanguages) {
		this.assessmentLanguages = assessmentLanguages;
	}

}

