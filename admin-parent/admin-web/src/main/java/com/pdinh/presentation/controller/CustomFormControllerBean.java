package com.pdinh.presentation.controller;

public abstract class CustomFormControllerBean extends ControllerBeanBase {

	public static final String CONTROLLER_NAME = "CustomFormControllerBean";

	public CustomFormControllerBean() {
		super();
	}

	public CustomFormControllerBean(String formName) {
		super(formName);
	}

	public void handleCustomRadioChange() {
	}

	public void handleCustomSelectChange() {
	}

	public String handleCustomFormSubmit() {
		return null;
	}
}
