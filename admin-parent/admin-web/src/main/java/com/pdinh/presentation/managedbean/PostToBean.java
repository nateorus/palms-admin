package com.pdinh.presentation.managedbean;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.pdinh.auth.persistence.entity.PalmsSession;

@ManagedBean
@ViewScoped
public class PostToBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@Resource(mappedName = "application/config_properties")
	private Properties properties;

	private String path;
	private String sysName;
	private String returnTo;
	private Integer palmsUserId;
	private String page;
	private String mode;
	private String returnURL;

	public void init() {
		System.out.println("adminweb - PostToBean.init()!");
	}

	public void preparePost() {
		String hostName = "";
		ExternalContext econtext = FacesContext.getCurrentInstance().getExternalContext();

		String protocol = "http://";
		if (econtext.isSecure()) {
			protocol = "https://";
		}

		RequestContext context = RequestContext.getCurrentInstance();
		PalmsSession ps = sessionBean.getPalmsSession();
		context.addCallbackParam("sessionToken", ps.getSessionToken());
		context.addCallbackParam("subjectId", sessionBean.getRoleContext().getSubject().getSubject_id());

		if (sessionBean.getProject() != null) {
			context.addCallbackParam("projectId", sessionBean.getProject().getProjectId());
		}

		if (getReturnTo() != null) {
			context.addCallbackParam("returnTo", getReturnTo());
		}

		if (getPalmsUserId() != null) {
			context.addCallbackParam("palmsUserId", getPalmsUserId().intValue());
		}

		if (sessionBean.getLrmProjectId() > 0) {
			context.addCallbackParam("lrmprojectid", sessionBean.getLrmProjectId());
		}

		if (sessionBean.getImportJobId() > 0) {
			context.addCallbackParam("importjobid", sessionBean.getImportJobId());
		}

		if (getSysName().equalsIgnoreCase("lrm")) {
			hostName = properties.getProperty("lrmHost");

		} else if (getSysName().equalsIgnoreCase("auth")) {
			hostName = properties.getProperty("authorizationHost");

		} else if (getSysName().equalsIgnoreCase("clientadmin")) {
			hostName = properties.getProperty("clientAdminHost");
			context.addCallbackParam("mode", getMode());
			context.addCallbackParam("returnURL", getReturnURL());
			context.addCallbackParam("page", getPage());

		} else {
			hostName = econtext.getRequestServerName() + ":" + econtext.getRequestServerPort();
		}

		if (hostName.startsWith("http")) {
			context.addCallbackParam("url", hostName + getPath());
		} else {
			context.addCallbackParam("url", protocol + hostName + getPath());
		}
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public String getReturnTo() {
		return returnTo;
	}

	public void setReturnTo(String returnTo) {
		this.returnTo = returnTo;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public Integer getPalmsUserId() {
		return palmsUserId;
	}

	public void setPalmsUserId(Integer palmsUserId) {
		this.palmsUserId = palmsUserId;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}
}
