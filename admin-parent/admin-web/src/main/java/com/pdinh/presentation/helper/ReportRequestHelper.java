package com.pdinh.presentation.helper;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.JaxbUtil;
import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequestList;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentationList;
import com.pdinh.data.jaxb.reporting.ReportRequestResponseList;
import com.pdinh.data.jaxb.reporting.SystemHealthCheckResponse;
import com.pdinh.data.ms.dao.ReportGenerateRequestDao;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.ResultObj;
import com.pdinh.presentation.domain.UpdateReportRequestResultObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.rest.security.APIAuthorizationServiceLocal;
import com.pdinh.rest.security.AuthorizationClientFilter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HttpURLConnectionFactory;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

@ManagedBean(name = "reportRequestHelper")
@ViewScoped
public class ReportRequestHelper implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ReportRequestHelper.class);
	private static final Logger auditLog = LoggerFactory.getLogger("AUDIT_ADMIN");

	//@EJB
	//private APIAuthorizationServiceLocal apiAuthorizationService;

	@EJB
	private ReportGenerateRequestDao reportGenerateRequestDao;

	@EJB
	private ReportRequestDao reportRequestDao;

	/*
	@EJB
	private AsynchronousAuditLoggerLocal auditLog;
	*/

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@Resource(name = "application/config_properties")
	private Properties configProperties;

	private Client client;

	@PostConstruct
	private void init() {
		log.debug("Initializing Jersey Client");
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		//client = Client.create(config);
		client = new Client(new URLConnectionClientHandler(
		        new HttpURLConnectionFactory() {
		    Proxy p = null;
		    @Override
		    public HttpURLConnection getHttpURLConnection(URL url)
		            throws IOException {
		        if (p == null) {
		            if (configProperties.containsKey("http.proxyHost")){
		            	log.debug("Initializing Proxy for Jersey Client.  Host: {}, Port: {}",
		            			configProperties.getProperty("http.proxyHost"),
		            			configProperties.getProperty("http.proxyPort"));
		                p = new Proxy(Proxy.Type.HTTP,
	                        new InetSocketAddress(
	                        		configProperties.getProperty("http.proxyHost"),
	                        Integer.valueOf(configProperties.getProperty("http.proxyPort"))));
		            } else {
		            	p = Proxy.NO_PROXY;
		            }
		            
		        }
		        return (HttpURLConnection) url.openConnection(p);
		    }
		}), config);
		client.addFilter(new AuthorizationClientFilter(configProperties));
	}

	/**
	 * Lists Report Requests by project using the getProjectReportRequests REST
	 * api endpoint
	 * 
	 * @see com.pdinh.enterprise.rest.ReportRequestResource
	 * @param projectId
	 *            The id of the project from which to list
	 * @param maxRecords
	 *            The maximum number of records to return
	 * @param page
	 *            increment this value to page through batches of results. The
	 *            first page is zero
	 * @param statuses
	 *            A list of status codes to match. static code values found in
	 *            com.pdinh.persistence.ms.entity.ReportRequestStatus
	 * @return A list of ReportRepresentation objects meeting the given
	 *         criteria, sorted in descending order by status update timestamp.
	 */
	public ReportRepresentationList getReportListByProject(int projectId, int maxRecords, int page,
			List<String> statuses) {
		String urlString = "/listreports/getProjectReportRequests/" + projectId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
	
		return this.getList(urlString, maxRecords, page, statuses);
	}

	/**
	 * Lists Report Requests by user using the getUserReportRequests REST api
	 * endpoint
	 * 
	 * @see com.pdinh.enterprise.rest.ReportRequestResource
	 * @param userId
	 *            The id of the user from which to list
	 * @param maxRecords
	 *            The maximum number of records to return
	 * @param page
	 *            increment this value to page through batches of results. The
	 *            first page is zero
	 * @param statuses
	 *            A list of status codes to match. static code values found in
	 *            com.pdinh.persistence.ms.entity.ReportRequestStatus
	 * @return A list of ReportRepresentation objects meeting the given
	 *         criteria, sorted in descending order by status update timestamp.
	 */
	public ReportRepresentationList getReportListByUser(int userId, int maxRecords, int page, List<String> statuses) {
		String urlString = "/listreports/getUserReportRequests/" + userId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		//String urlString = "/getUserReportRequests/" + userId + "/";
		// System.out.println("DEBUG REPORT CENTER url string = " + urlString);
		return this.getList(urlString, maxRecords, page, statuses);
	}

	/**
	 * Lists Report Requests by subject using the getSubjectReportRequests REST
	 * api endpoint
	 * 
	 * @see com.pdinh.enterprise.rest.ReportRequestResource
	 * @param subjectId
	 *            The id of the subject from which to list
	 * @param maxRecords
	 *            The maximum number of records to return
	 * @param page
	 *            increment this value to page through batches of results. The
	 *            first page is zero
	 * @param statuses
	 *            A list of status codes to match. static code values found in
	 *            com.pdinh.persistence.ms.entity.ReportRequestStatus
	 * @return A list of ReportRepresentation objects meeting the given
	 *         criteria, sorted in descending order by status update timestamp.
	 */

	public ReportRepresentationList getReportListBySubject(int subjectId, int maxRecords, int page,
			List<String> statuses) {
		String urlString = "/listreports/getSubjectReportRequests/" + subjectId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		//String urlString = "/getSubjectReportRequests/" + subjectId + "/";

		return this.getList(urlString, maxRecords, page, statuses);
	}

	/**
	 * Lists Report Requests by company using the getCompanyReportRequests REST
	 * api endpoint
	 * 
	 * @see com.pdinh.enterprise.rest.ReportRequestResource
	 * @param companyId
	 *            The id of the company from which to list
	 * @param maxRecords
	 *            The maximum number of records to return
	 * @param page
	 *            increment this value to page through batches of results. The
	 *            first page is zero
	 * @param statuses
	 *            A list of status codes to match. static code values found in
	 *            com.pdinh.persistence.ms.entity.ReportRequestStatus
	 * @return A list of ReportRepresentation objects meeting the given
	 *         criteria, sorted in descending order by status update timestamp.
	 */

	public ReportRepresentationList getReportListByCompany(int companyId, int maxRecords, int page,
			List<String> statuses) {

		String urlString = "/listreports/getCompanyReportRequests/" + companyId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}

		return this.getList(urlString, maxRecords, page, statuses);
	}

	public GenerateReportsRequestList getRequestListByCompany(int companyId, int maxRecords, int page,
			List<String> statuses) {
		String urlString = "/listreports/getCompanyGenerateRequests/" + companyId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		//String urlString = "/getCompanyGenerateRequests/" + companyId + "/";

		return this.getRequestList(urlString, maxRecords, page, statuses);

	}

	public GenerateReportsRequestList getRequestListByProject(int projectId, int maxRecords, int page,
			List<String> statuses) {
		String urlString = "/listreports/getProjectGenerateRequests/" + projectId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		//String urlString = "/getProjectGenerateRequests/" + projectId + "/";

		return this.getRequestList(urlString, maxRecords, page, statuses);

	}

	public GenerateReportsRequestList getRequestListByUser(int usersId, int maxRecords, int page, List<String> statuses) {
		
		String urlString = "/listreports/getUserGenerateRequests/" + usersId + "/";
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		//String urlString = "/getUserGenerateRequests/" + usersId + "/";
		log.trace("Got URL String: {}", urlString);
		return this.getRequestList(urlString, maxRecords, page, statuses);

	}

	/**
	 * Use this method to update the persistence state of a report request. It
	 * calls the updateRequest REST api endpoint of the PALMS rest api. Updates
	 * to some data may not be allowed.
	 * 
	 * @see com.pdinh.enterprise.rest.ReportRequestResource
	 * @param report
	 *            A Report Request representation with some changes to persisted
	 *            data, such as status.
	 * @return A ResultObj of subtype UpdateReportRequestResultObj, or ErrorObj.
	 *         The former is a successful result, though the status string will
	 *         indicate "ok" or "not modified", if no changes resulted to the
	 *         persistence state.
	 */

	public ResultObj updateReportRequest(ReportRepresentation report) {
		// String urlString = "/updateRequest";
		/*
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		Client client = Client.create(config);
		*/
		String url = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT
				+ "/reports/updateRequest";
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		//client.addFilter(new AuthorizationClientFilter(configProperties));
		WebResource resource = client.resource(url);
		ClientResponse response = null;
		try {
			response = resource.type(javax.ws.rs.core.MediaType.APPLICATION_XML).post(ClientResponse.class, report);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());

		}
		if (response.getStatus() == 200) {
			return new UpdateReportRequestResultObj("ok");
		} else if (response.getStatus() == 304) {
			return new UpdateReportRequestResultObj("not modified");
		} else {
			ErrorRepresentation error = response.getEntity(ErrorRepresentation.class);
			return new ErrorObj(error.getErrorCode(), error.getErrorMessage() + " Details: (" + error.getErrorDetail()
					+ ")");
		}

	}

	/**
	 * Use this method to submit Report Requests to the queue. For the
	 * participants list, only ids are required. No need to include names or
	 * email. Project name may be required depending on the report type.
	 * 
	 * @param reportsRequest
	 * @return a ReportRequestResponseList object, which is a wrapper around a
	 *         list of ReportRequestResponse objects. There may be successful
	 *         and error responses contained in the same list. If the
	 *         reportRequestKey is null this typically indicates an error, and
	 *         you should then look at the error message and code fields.
	 */

	public ReportRequestResponseList submitReportsRequest(GenerateReportsRequest reportsRequest) {
		ReportRequestResponseList response = new ReportRequestResponseList();
		/*
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		Client client = Client.create(config);
		*/
		String url = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT
				+ "/reports/requestReports";
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		//client.addFilter(new AuthorizationClientFilter(configProperties));
		WebResource resource = client.resource(url);
		try {

			response = resource.accept(javax.ws.rs.core.MediaType.APPLICATION_XML).post(
					ReportRequestResponseList.class, reportsRequest);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());

		}
		/*
		Subject subject = SecurityUtils.getSubject();
		
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). {} report requests submitted", 
				new Object[]{subject.getPrincipal(), EntityAuditLogger.OP_REQUEST_REPORTS, 
					sessionBean.getCompany().getCoName(), sessionBean.getCompany().getCompanyId(),
					sessionBean.getSelectedProject(), sessionBean.getProject().getName(),
					response.getReportRequestResponseList().size()});
					*/
		return response;
	}

	public StreamedContent downloadAllRequested(GenerateReportsRequest request) throws PalmsException {
		ReportGenerateRequest requestEntity = reportGenerateRequestDao.findById(request.getRequestId());
		List<ReportRequest> reportRequests = requestEntity.getReportRequests();
		List<ReportRepresentation> representationList = new ArrayList<ReportRepresentation>();
		for (ReportRequest rep : reportRequests) {
			ReportRepresentation rptRep = new ReportRepresentation(rep);
			representationList.add(rptRep);
		}
		ReportRepresentationList list = new ReportRepresentationList();
		list.setReports(representationList);
		try {
			return this.downloadReportFiles(list);
		} catch (PalmsException e) {
			throw e;
		}
	}

	public List<ReportRepresentation> getReportRepresentations(GenerateReportsRequest request) {
		ReportGenerateRequest requestEntity = reportGenerateRequestDao.findById(request.getRequestId());
		List<ReportRequest> reportRequests = requestEntity.getReportRequests();
		List<ReportRepresentation> representationList = new ArrayList<ReportRepresentation>();
		for (ReportRequest rep : reportRequests) {
			ReportRepresentation rptRep = new ReportRepresentation(rep);
			representationList.add(rptRep);
		}
		return representationList;
	}

	public ReportRepresentation uploadReportFile(ReportRepresentation rep, File file) {
		log.debug("In upload report file with file named: {}", file.getName());
		rep.setCreateType(ReportRequest.CREATE_TYPE_UPLOAD);
		GenerateReportsRequest genReq = new GenerateReportsRequest();
		List<ReportRepresentation> reportList = new ArrayList<ReportRepresentation>();
		reportList.add(rep);
		genReq.setRequestName(rep.getReportName());
		genReq.setReportTypeList(reportList);
		genReq.setParticipants(rep.getParticipants());
		genReq.setPrincipal(sessionBean.getRoleContext().getSubject().getPrincipal());
		genReq.setSubjectId(sessionBean.getRoleContext().getSubject().getSubject_id());
		/*
				DefaultClientConfig config = new DefaultClientConfig();
				config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
				Client client = Client.create(config);
				*/
		String url = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT
				+ "/reports/uploadReport";
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		//client.addFilter(new AuthorizationClientFilter(configProperties));
		WebResource resource = client.resource(url);
		String xml = JaxbUtil.marshalToXmlString(genReq, GenerateReportsRequest.class);
		FormDataMultiPart data = new FormDataMultiPart();
		FileDataBodyPart fileBody = new FileDataBodyPart("file", file, MediaType.APPLICATION_OCTET_STREAM_TYPE);

		ContentDisposition cd = null;
		try {
			cd = new ContentDisposition("attachment; filename=\"" + file.getName() + "\"");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
		data.setContentDisposition(cd);
		data.bodyPart(fileBody);
		data.bodyPart(new FormDataBodyPart("xml", xml, MediaType.APPLICATION_XML_TYPE));
		data.field("fileName", file.getName());
		log.debug("about to post file and xml to rest api");
		ClientResponse response = resource.type(MediaType.MULTIPART_FORM_DATA).post(ClientResponse.class, data);
		log.debug("Got response from REST api");

		try{
			rep = response.getEntity(ReportRepresentation.class);
		}catch (WebApplicationException w){
			log.error("Got Error while Uploading report: {}", w.getMessage());
			rep.setErrorCode(PalmsErrorCode.UPLOAD_FAILED.getCode());
			rep.setErrorMessage(PalmsErrorCode.UPLOAD_FAILED.getMessage());
		}

		return rep;

	}

	public StreamedContent downloadReportFiles(ReportRepresentationList storedReports) throws PalmsException {
		// ReportRequestResponseList response = new ReportRequestResponseList();
		Subject subject = SecurityUtils.getSubject();
		storedReports.setRequestedBy((String) subject.getPrincipal());
		/*
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		Client client = Client.create(config);
		*/
		String url = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT
				+ "/download/downloadReports";
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		//client.addFilter(new AuthorizationClientFilter(configProperties));
		WebResource resource = client.resource(url);
		ClientResponse response = null;
		StreamedContent reportFiles = null;

		try {

			response = resource.type(javax.ws.rs.core.MediaType.APPLICATION_XML).post(ClientResponse.class,
					storedReports);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());

		}
		if (response.getStatus() == 200) {
			String disposition = response.getHeaders().getFirst("Content-Disposition");
			disposition = disposition.substring(disposition.indexOf("filename="));
			String[] diparts = disposition.split("=");
			String filename = diparts[1];
			if (filename.endsWith(".zip")) {
				String EXTRACT_DATE_FORMAT = "yyyyMMdd";
				SimpleDateFormat sdf = new SimpleDateFormat(EXTRACT_DATE_FORMAT);
				Date now = new Date();
				filename = "reports_" + sdf.format(now) + ".zip";
			}
			reportFiles = new DefaultStreamedContent(response.getEntityInputStream(), response.getHeaders().getFirst(
					"Content-Type"), filename);
			log.debug("Got something here:  {}", reportFiles.getName());
			log.debug("Content type: {}", reportFiles.getContentType());

			return reportFiles;
		} else {
			ErrorRepresentation e = response.getEntity(ErrorRepresentation.class);
			throw new PalmsException(e.getErrorMessage(), e.getErrorCode(), e.getErrorDetail());
		}
	}

	public GenerateReportsRequest getGenerateRequestDetails(int requestId, int subjectId) {
		GenerateReportsRequest reqRep = new GenerateReportsRequest();
		/*
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		Client client = Client.create(config);
		*/
		String url = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT
				+ "/listreports/getGenerateRequestDetails";
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		url = url + "/" + String.valueOf(requestId);
		if (subjectId > 0){
			url = url + "?subjectId=" + String.valueOf(subjectId);
		}
		//client.addFilter(new AuthorizationClientFilter(configProperties));
		WebResource resource = client.resource(url);
		try {
			reqRep = resource.get(GenerateReportsRequest.class);
		} catch (UniformInterfaceException ure) {
			log.debug("Caught Uniform Interface Exception: {}", ure.getMessage());
		}
		return reqRep;
	}

	public List<ReportRequest> getCurrentlyProcessingRequests() {
		return reportRequestDao.getRequestsByStatus(Arrays.asList(ReportRequestStatus.STATUS_PROCESSING), 10, 0);
	}

	private GenerateReportsRequestList getRequestList(String urlPath, int maxRecords, int page, List<String> statuses) {

		GenerateReportsRequestList list = new GenerateReportsRequestList();
		for (String status : statuses) {
			urlPath += status + "/";
		}
		if (maxRecords > 0) {
			urlPath += "?max=" + maxRecords;
			if (page > 0) {
				urlPath += "&page=" + page;
			}
		}
		//HttpURLConnection conn = getSecureConnection(urlPath, "GET");
		WebResource resource = client.resource(urlPath);
		try {
			/*
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

			String inputLine;
			String xml = "";
			while ((inputLine = in.readLine()) != null) {
				// log.debug("line: {}", inputLine);
				xml += inputLine;
			}
			in.close();
			
			list = (GenerateReportsRequestList) JaxbUtil.unmarshal(xml, GenerateReportsRequestList.class);
			*/
			list = resource.get(GenerateReportsRequestList.class);
			log.debug("finished.  returning reports list");
		} catch (Exception e) {
			log.error("Got Error reading or transforming api return value: {}", e.getMessage());
		}

		return list;
	}

	private ReportRepresentationList getList(String urlPath, int maxRecords, int page, List<String> statuses) {
		ReportRepresentationList list = new ReportRepresentationList();

		for (String status : statuses) {
			urlPath += status + "/";
		}
		if (maxRecords > 0) {
			urlPath += "?max=" + maxRecords;
			if (page > 0) {
				urlPath += "&page=" + page;
			}
		}
		if (urlPath.contains("getUserReportRequests")){
			log.debug("User Context.  Adding subjectId for authorization");
			String queryDelim = "?";
			if (maxRecords > 0 || page > 0){
				queryDelim = "&";
			}
			if (!sessionBean.isClientContextPermitted(sessionBean.getCompany().getCompanyId())){
				urlPath = urlPath += queryDelim + "subjectId=" + String.valueOf(sessionBean.getSubject_id());
			}
		}
		log.debug("Making secure connection to: {}", urlPath);
		//HttpURLConnection conn = getSecureConnection(urlPath, "GET");
		WebResource resource = client.resource(urlPath); 
		
		try {
			/*
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

			String inputLine;
			String xml = "";
			while ((inputLine = in.readLine()) != null) {
				// log.debug("line: {}", inputLine);
				xml += inputLine;
			}
			in.close();

			list = (ReportRepresentationList) JaxbUtil.unmarshal(xml, ReportRepresentationList.class);
			*/
			list = resource.get(ReportRepresentationList.class);
			log.debug("finished.  returning {} reports of {}", list.getReports().size(), list.getTotalRecordCount());
		} catch (Exception e) {
			log.error("Got Error reading or transforming api return value: {}", e.getMessage());
		}

		return list;
	}
/*
	private HttpURLConnection getSecureConnection(String apiPath, String httpMethod) {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String protocol = "http";
		if (request.isSecure()) {
			protocol = "https";
		}
		String urlTarget = protocol + "://" + request.getServerName() + ":" + request.getServerPort()
				+ "/adminrest/jaxrs/listreports" + apiPath;
		log.debug("Got urlTarget: {}", urlTarget);
		URL currUrl;
		HttpURLConnection conn = null;
		try {
			currUrl = new URL(urlTarget);

			conn = (HttpURLConnection) currUrl.openConnection();

			conn.setRequestMethod(httpMethod);
			conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			conn.setRequestProperty("ACCEPT", "application/xml");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		apiAuthorizationService.authorizePALMSAPIRequest(conn);

		return conn;

	}
*/
	public SystemHealthCheckResponse getSystemHealth() {
		SystemHealthCheckResponse health = new SystemHealthCheckResponse();
		/*
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		Client client = Client.create(config);
		*/
		String url = configProperties.getProperty("importPollerHost") + URLContextConstants.IMPORTPOLLER_REST_CONTEXT
				+ "/report/healthcheck";
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}

		//client.addFilter(new AuthorizationClientFilter(configProperties));
		WebResource resource = client.resource(url);
		try {
			health = resource.get(SystemHealthCheckResponse.class);
		} catch (UniformInterfaceException ure) {
			log.error("Got UniformInterfaceException.  Could not communicate with poller app: {}", ure.getMessage());
			health = this.getSystemStatusFromAdminRestApi();
		} catch (ClientHandlerException e) {
			log.error("Got ClientHandlerException.  Could not communicate with poller app: {}", e.getMessage());
			health = this.getSystemStatusFromAdminRestApi();
		}
		return health;
	}
	
	private SystemHealthCheckResponse getSystemStatusFromAdminRestApi() {
		log.debug("Getting system status from palms rest api");
		/*
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);

		Client client = Client.create(config);
		*/
		String resourceName = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + "/health/ping";
		log.debug("Requesting system status from URI: {}", resourceName);
		SystemHealthCheckResponse document = null;
		ClientResponse response = null;
		
		try {
			//client.addFilter(new AuthorizationClientFilter(configProperties));
			WebResource resource = client.resource(resourceName);
			response = resource.type(MediaType.APPLICATION_XML)
					.get(ClientResponse.class);

			document = response.getEntity(SystemHealthCheckResponse.class);
		} catch (ClientHandlerException e){
			log.error("Could not communicate with Palms Rest Api.  {}", e.getMessage());
			
		} catch (UniformInterfaceException e){
			log.error("Could not communicate with Palms Rest Api.  {}", e.getMessage());
			
		} finally {
			client.destroy();
		}
		document.setPollingInterval(0);
		document.setCountHighPriorityQueue(0);
		document.setCountLowPriorityQueue(0);
		document.setPollingStatus("unknown");
		document.setSqsStatus("unknown");
		document.setMessageBatchSize(0);
		return document;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

}
