package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.AddUserResultObj;

@ManagedBean
@ViewScoped
public class ImportParticipantViewBean implements Serializable{


	private static final long serialVersionUID = 1L;

	private Boolean renderResultsTable = false;
	private Boolean showCompletePanel = false;
	private Boolean showUploadPanel = true;
	private Boolean disableProcessButton;
	private String helpFileUrl;
	private String importTemplateFileUrl;
	private int percentComplete = 0;
	
	
	private List<AddUserResultObj> results = new ArrayList<AddUserResultObj>();

	public void setResults(List<AddUserResultObj> results) {
		this.results = results;
	}

	public List<AddUserResultObj> getResults() {
		return results;
	}

	public void setRenderResultsTable(Boolean renderResultsTable) {
		this.renderResultsTable = renderResultsTable;
	}

	public Boolean getRenderResultsTable() {
		return renderResultsTable;
	}

	public void setShowCompletePanel(Boolean showCompletePanel) {
		this.showCompletePanel = showCompletePanel;
	}

	public Boolean getShowCompletePanel() {
		return showCompletePanel;
	}

	public void setShowUploadPanel(Boolean showUploadPanel) {
		this.showUploadPanel = showUploadPanel;
	}

	public Boolean getShowUploadPanel() {
		return showUploadPanel;
	}
	public void setDisableProcessButton(Boolean enabelProcessButton) {
		this.disableProcessButton = enabelProcessButton;
	}

	public Boolean getDisableProcessButton() {
		return disableProcessButton;
	}

	public String getHelpFileUrl() {
		return helpFileUrl;
	}

	public void setHelpFileUrl(String helpFileUrl) {
		this.helpFileUrl = helpFileUrl;
	}

	public String getImportTemplateFileUrl() {
		return importTemplateFileUrl;
	}

	public void setImportTemplateFileUrl(String importTemplateFileUrl) {
		this.importTemplateFileUrl = importTemplateFileUrl;
	}

	public int getPercentComplete() {
		return percentComplete;
	}

	public void setPercentComplete(int percentComplete) {
		this.percentComplete = percentComplete;
	}
}
