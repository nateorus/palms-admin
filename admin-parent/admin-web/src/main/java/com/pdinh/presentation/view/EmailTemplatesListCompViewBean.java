package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;

@ManagedBean(name = "emailTemplatesListCompViewBean")
@ViewScoped
public class EmailTemplatesListCompViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Boolean disableTemplateEdit = true;
	private Boolean disableTemplateClone = true;
	private boolean disableTemplateDelete = true;

	private Boolean disableCreateNewTemplate = true;
	private Boolean showEmailPanel = true;
	private Boolean showEmailPreview = false;

	private List<EmailTemplateObj> templates;
	private EmailTemplateObj selectedTemplate;
	private List<EmailTemplateObj> filteredTemplates;
	private EmailTextObj selectedEmailText;

	private String cloneTemplateTitle;

	private String templateTitle;
	private String templateType;
	private String templateConst;
	private Integer templateIsTemplate;
	private String templateLevel;

	private String emailBody;
	private String emailSubject;

	private String sendToAddress;
	private String sentFromAddress;

	private List<ListItemObj> currentEmailLangs;
	private String currentLanguage = "en";

	public Boolean getDisableTemplateEdit() {
		return disableTemplateEdit;
	}

	public void setDisableTemplateEdit(Boolean disableTemplateEdit) {
		this.disableTemplateEdit = disableTemplateEdit;
	}

	public Boolean getDisableTemplateClone() {
		return disableTemplateClone;
	}

	public void setDisableTemplateClone(Boolean disableTemplateClone) {
		this.disableTemplateClone = disableTemplateClone;
	}

	public Boolean getShowEmailPanel() {
		return showEmailPanel;
	}

	public void setShowEmailPanel(Boolean showEmailPanel) {
		this.showEmailPanel = showEmailPanel;
	}

	public List<EmailTemplateObj> getTemplates() {
		return templates;
	}

	public void setTemplates(List<EmailTemplateObj> templates) {
		this.templates = templates;
	}

	public String getTemplateTitle() {
		return templateTitle;
	}

	public void setTemplateTitle(String templateTitle) {
		this.templateTitle = templateTitle;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getTemplateConst() {
		return templateConst;
	}

	public void setTemplateConst(String templateConst) {
		this.templateConst = templateConst;
	}

	public Integer getTemplateIsTemplate() {
		return templateIsTemplate;
	}

	public void setTemplateIsTemplate(Integer templateIsTemplate) {
		this.templateIsTemplate = templateIsTemplate;
	}

	public String getTemplateLevel() {
		return templateLevel;
	}

	public void setTemplateLevel(String templateLevel) {
		this.templateLevel = templateLevel;
	}

	public List<ListItemObj> getCurrentEmailLangs() {
		return currentEmailLangs;
	}

	public void setCurrentEmailLangs(List<ListItemObj> currentEmailLangs) {
		this.currentEmailLangs = currentEmailLangs;
	}

	public EmailTemplateObj getSelectedTemplate() {
		return selectedTemplate;
	}

	public void setSelectedTemplate(EmailTemplateObj selectedTemplate) {
		this.selectedTemplate = selectedTemplate;
	}

	public String getCurrentLanguage() {
		return currentLanguage;
	}

	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
	}

	public Boolean getShowEmailPreview() {
		return showEmailPreview;
	}

	public void setShowEmailPreview(Boolean showEmailPreview) {
		this.showEmailPreview = showEmailPreview;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getSendToAddress() {
		return sendToAddress;
	}

	public void setSendToAddress(String sendToAddress) {
		this.sendToAddress = sendToAddress;
	}

	public String getSentFromAddress() {
		return sentFromAddress;
	}

	public void setSentFromAddress(String sentFromAddress) {
		this.sentFromAddress = sentFromAddress;
	}

	public String getCloneTemplateTitle() {
		return cloneTemplateTitle;
	}

	public void setCloneTemplateTitle(String cloneTemplateTitle) {
		this.cloneTemplateTitle = cloneTemplateTitle;
	}

	public Boolean getDisableCreateNewTemplate() {
		return disableCreateNewTemplate;
	}

	public void setDisableCreateNewTemplate(Boolean disableCreateNewTemplate) {
		this.disableCreateNewTemplate = disableCreateNewTemplate;
	}

	public EmailTextObj getSelectedEmailText() {
		return selectedEmailText;
	}

	public void setSelectedEmailText(EmailTextObj selectedEmailText) {
		this.selectedEmailText = selectedEmailText;
	}

	public List<EmailTemplateObj> getFilteredTemplates() {
		return filteredTemplates;
	}

	public void setFilteredTemplates(List<EmailTemplateObj> filteredTemplates) {
		this.filteredTemplates = filteredTemplates;
	}

	public boolean isDisableTemplateDelete() {
		return disableTemplateDelete;
	}

	public void setDisableTemplateDelete(boolean disableTemplateDelete) {
		this.disableTemplateDelete = disableTemplateDelete;
	}
}
