package com.pdinh.presentation.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.UserDocumentTypeDao;
import com.pdinh.enterprise.managedbean.form.LoginFormBean;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.ms.entity.FileAttachment;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.persistence.ms.entity.UserDocument;
import com.pdinh.persistence.ms.entity.UserDocumentType;
import com.pdinh.presentation.domain.FileAttachmentObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.ParticipantDocumentObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.UserDocumentsViewBean;

@ManagedBean(name = "userDocumentsControllerBean")
@ViewScoped
public class UserDocumentsControllerBean extends DocumentsControllerBean {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(UserDocumentsControllerBean.class);

	@EJB
	private UserDocumentTypeDao userDocumentTypeDao;

	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "userDocumentsViewBean", value = "#{userDocumentsViewBean}")
	protected UserDocumentsViewBean userDocumentsViewBean;

	@ManagedProperty(name = "loginFormBean", value = "#{loginFormBean}")
	private LoginFormBean loginFormBean;

	@Resource(name = "application/config_properties")
	private Properties appConfigProperties;

	@PostConstruct
	public void postConstruct() {
		super.setDocumentsViewBean(userDocumentsViewBean);
		super.setAppConfigProperties(appConfigProperties);
		super.setFileAttachmentService(fileAttachmentService);
		super.setLoginFormBean(loginFormBean);
	}

	public void initForEdit() {
		log.debug("Setting username for documents view bean auditing purposes {}", sessionBean.getUser().getUsername());
		userDocumentsViewBean.setUsername(sessionBean.getUser().getUsername());
		userDocumentsViewBean.setUsers_id(sessionBean.getUser().getUsersId());
		populateDocuments();
	}

	// Populate documents
	private void populateDocuments() {
		log.debug("In UserDocumentsControllerBean populateDocuments()");
		userDocumentsViewBean.clear();

		List<ListItemObj> documentTypes = new ArrayList<ListItemObj>();
		/**
		 * List assembled from embedded screen shot in User Stories v0.8
		 * http://mspwebdev6
		 * /aande/abydmigration/Shared%20Documents/Design%20Docs
		 * /AbyD%20migration_user%20stories_v0.8.doc Unsure whether this will
		 * live in here or in a table. Need similar item for projects too.
		 */

		List<UserDocumentType> userDocumentTypes = userDocumentTypeDao.findAll();
		for (int i = 0; i < userDocumentTypes.size(); i++) {
			UserDocumentType udt = userDocumentTypes.get(i);
			documentTypes.add(new ListItemObj(udt.getUserDocumentTypeId(), udt.getName()));
		}
		userDocumentsViewBean.setDocumentTypes(documentTypes);

		List documents = findAllDocumentsByUser();
		userDocumentsViewBean.setDocuments(documents);

		// TODO - needs to be action-aware?
		userDocumentsViewBean.setRemoveDocumentsDisabled(true);
	}

	public void addDocument() {
		log.debug("IN UserDocumentsControllerBean.addDocument()");
		userDocumentsViewBean.setDoSave(true);

		ParticipantDocumentObj document = new ParticipantDocumentObj();
		document.setDescription(userDocumentsViewBean.getNewDocumentDescription());
		document.setShortDescription(userDocumentsViewBean.getNewDocumentDescription());
		document.setType(userDocumentsViewBean.getNewDocumentType());
		document.setTypeDisplay(userDocumentsViewBean.getDocumentNameByType(document.getType()));
		document.setFileAttachment(userDocumentsViewBean.getLastDocumentFileUploaded());
		document.setIsIntegrationGrid(userDocumentsViewBean.getNewDocumentIsIntegrationGrid());

		// write this file to disk, will need to clean up any files that don't
		// actually get saved
		FileAttachmentObj fileAttachmentObj = userDocumentsViewBean.getLastDocumentFileUploaded();

		try {
			fileAttachmentService.write(fileAttachmentObj.getPath() + "/" + fileAttachmentObj.getName(),
					fileAttachmentObj.getNewFileInputStream());
			userDocumentsViewBean.getDocuments().add(document);
		} catch (IOException e) {
			e.printStackTrace();
		}

		userDocumentsViewBean.clearNewDocumentData();
	}

	public List<ParticipantDocumentObj> findAllDocumentsByUser() {
		log.debug("IN UserDocumentsControllerBean.findAllDocumentsByUser()");
		User user = sessionBean.getUser();
		List<ParticipantDocumentObj> documents = new ArrayList<ParticipantDocumentObj>();
		if (user != null) {
			List<UserDocument> userDocuments = user.getUserDocuments();
			for (UserDocument userDocument : userDocuments) {

				FileAttachment fa = userDocument.getFileAttachment();
				FileAttachmentObj fileAttachmentObj = new FileAttachmentObj(fa.getId(), fa.getName(), fa.getPath(),
						fa.getDateUploaded(), fa.getExtAdminId());

				ParticipantDocumentObj participantDocumentObj = new ParticipantDocumentObj(
						userDocument.getDocumentId(), userDocument.getDescription(), userDocument
								.getIncludeInIntegrationGrid().booleanValue(), userDocument.getUserDocumentType()
								.getUserDocumentTypeId(), userDocument.getUserDocumentType().getName(),
						fileAttachmentObj, false);
				participantDocumentObj.setShortDescription(userDocument.getDescription());
				// use path for now, ideally would just use id, but IDs are not
				// getting set to objects on save
				fileAttachmentObj.setUrl(getEncodedDocumentUrl(fileAttachmentObj.getPath() + "/"
						+ fileAttachmentObj.getName()));
				documents.add(participantDocumentObj);
			}
		}
		return documents;
	}

	public UserDocumentTypeDao getUserDocumentTypeDao() {
		return userDocumentTypeDao;
	}

	public void setUserDocumentTypeDao(UserDocumentTypeDao userDocumentTypeDao) {
		this.userDocumentTypeDao = userDocumentTypeDao;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public UserDocumentsViewBean getUserDocumentsViewBean() {
		return userDocumentsViewBean;
	}

	public void setUserDocumentsViewBean(UserDocumentsViewBean userDocumentsViewBean) {
		this.userDocumentsViewBean = userDocumentsViewBean;
	}

	@Override
	public LoginFormBean getLoginFormBean() {
		return loginFormBean;
	}

	@Override
	public void setLoginFormBean(LoginFormBean loginFormBean) {
		this.loginFormBean = loginFormBean;
	}

}
