package com.pdinh.presentation.controller;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.ms.dao.ReportTypeDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.helper.ReportRequestHelper;
import com.pdinh.presentation.helper.ReportWizardHelper;
import com.pdinh.presentation.helper.Utils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ReportUploadViewBean;

@ManagedBean
@ViewScoped
public class ReportUploadControllerBean implements Serializable {

	private static final long serialVersionUID = -4321083229033609640L;

	final static Logger logger = LoggerFactory.getLogger(ReportUploadControllerBean.class);

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "reportUploadViewBean", value = "#{reportUploadViewBean}")
	private ReportUploadViewBean viewBean;

	@ManagedProperty(name = "reportRequestHelper", value = "#{reportRequestHelper}")
	private ReportRequestHelper reportRequestHelper;

	@ManagedProperty(name = "reportWizardHelper", value = "#{reportWizardHelper}")
	private ReportWizardHelper reportWizardHelper;

	@Resource(name = "application/config_properties")
	private Properties configProperties;

	@EJB
	private ReportTypeDao reportTypeDao;

	@EJB
	private ReportingSingleton reportingSingleton;

	@EJB
	private LanguageSingleton languageSingleton;

	private boolean firstTime = true;
	private Subject subject;

	private void initView() {

		viewBean.init();

		if (firstTime) {

			logger.debug(">>>>>>>>>>>> Report Upload Controller - Init View <<<<<<<<<<<<<<<<");

			setSubject(SecurityUtils.getSubject());
			viewBean.setReportLanguages(languageSingleton.getLanguages());

			firstTime = false;
		}

	}

	public void initUpload(ParticipantObj[] participants) {

		initView();

		if (participants != null) {

			List<Integer> projectTypeIds = new ArrayList<Integer>();
			viewBean.setParticipants(Arrays.asList(participants));

			for (ParticipantObj ppt : viewBean.getParticipants()) {
				Integer projectTypeId = ppt.getProject().getTypeId();
				if (!projectTypeIds.contains(projectTypeId)) {
					projectTypeIds.add(projectTypeId);
				}
			}

			List<ReportType> reportTypes = reportTypeDao.findReportTypesByProjectTypeIds(projectTypeIds);
			ReportTypeObj reportTypeObj;

			for (ReportType type : reportTypes) {
				if (getSubject().isPermitted("generateAnyReport") || getSubject().isPermitted(type.getCode())) {
					reportTypeObj = new ReportTypeObj();
					reportTypeObj.setName(type.getName());
					reportTypeObj.setCode(type.getCode());
					if (type.getTargetLevelGroup() != null) {
						for (TargetLevelGroupType targetLevel : type.getTargetLevelGroup().getTargetLevelGroupTypes()) {
							reportTypeObj.getTargetLevelTypes().add(
									new TargetLevelTypeObj(targetLevel.getTargetLevelType().getTargetLevelTypeId(),
											targetLevel.getTargetLevelType().getName(), targetLevel
													.getTargetLevelType().getCode()));
						}
					}
					viewBean.getReportTypes().add(reportTypeObj);
				}
			}

			Collections.sort(viewBean.getReportTypes(), ReportTypeObj.Comparators.NAME);

			reportTypeObj = (viewBean.getParticipants().size() > 1) ? ReportTypeObj
					.transformReportType(reportingSingleton.getReportType("OTHER_GRP")) : ReportTypeObj
					.transformReportType(reportingSingleton.getReportType("OTHER_IND"));

			viewBean.getReportTypes().add(reportTypeObj);
			this.setDefaultReportLanguage(participants);

			reportWizardHelper.filterALPReports(viewBean.getReportTypes(), Arrays.asList(participants));

			this.prepopulateRequestName();
		}
	}

	private void prepopulateRequestName() {
		String requestName = "Manual Upload";
		List<ParticipantObj> ppts = viewBean.getParticipants();

		if (ppts != null) {
			if (ppts.size() > 1) {
				if (sessionBean.getProject() != null && sessionBean.getSelectedProject() != null) {
					requestName = sessionBean.getProject().getName();
				}
			} else if (viewBean.getParticipants().size() == 1) {
				requestName = viewBean.getParticipants().get(0).getLastName();
			}
		}
		viewBean.setRequestName(requestName + " - ");
	}

	private void setDefaultReportLanguage(ParticipantObj[] participants) {
		String defaultLanguageCode = "en";
		if (participants != null && participants.length == 1) {
			defaultLanguageCode = participants[0].getLanguageId();
		}
		viewBean.setReportLanguage(defaultLanguageCode);
	}

	public void populateTargetLevels() {
		for (ReportTypeObj reportType : viewBean.getReportTypes()) {
			if (reportType.getCode().equals(viewBean.getReportCode())) {
				viewBean.setTargetLevels(reportType.getTargetLevelTypes());
				break;
			}
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		viewBean.setUploadedFile(event.getFile());
		LabelUtils.addMessage("uploadReports", "Upload Report File:", "File (" + viewBean.getUploadedFilename()
				+ ") was attached successfully.");
	}

	public void handleCancelUploadReport() {
		viewBean.setUploadedFile(null);
	}

	public void handleUploadReport() throws IOException {

		if (!validateReportUpload()) {
			return;
		}

		UploadedFile uploadedFile = viewBean.getUploadedFile();
		String fileRoot = configProperties.getProperty("filesRoot");

		logger.trace("Uploaded File: File Name = {}; Content Type = {}; Size = {}", uploadedFile.getFileName(),
				uploadedFile.getContentType(), uploadedFile.getSize());

		resetTargetLevelValue();

		if (uploadedFile != null) {
			File file = Utils.convertInputStreamToFile(uploadedFile.getInputstream(),
					FilenameUtils.getName(uploadedFile.getFileName()), fileRoot);
			ReportRepresentation report = new ReportRepresentation();
			report.setTitle(viewBean.getRequestName());
			report.setReportName(viewBean.getRequestName());
			report.setCode(viewBean.getReportCode());
			report.setContentType(uploadedFile.getContentType());
			report.setReportLang(viewBean.getReportLanguage());
			report.setCreateType(ReportRequest.CREATE_TYPE_UPLOAD);
			report.setManuallyUploaded(true);
			report.setFilesize(uploadedFile.getSize());
			report.setTargetLevel(viewBean.getTargetLevel());
			report.setParticipants(transformParticipants(viewBean.getParticipants()));
			report.setFilesize(file.length());
			report = reportRequestHelper.uploadReportFile(report, file);
			logger.trace(
					"Got response from helper. Request Key: {}, Request Id: {}, Content Type = {}; File Size = {}",
					report.getReportRequestKey(), report.getRequestId(), report.getContentType(), report.getFilesize());

			boolean fileDeleted = FileUtils.deleteQuietly(file.getParentFile());

			if (!fileDeleted) {
				file.getParentFile().deleteOnExit();
				file.deleteOnExit();
				logger.warn("Unable to delete temp file and directory, it will delete them once application server restarted.");
			} else {
				logger.info("Temp file and directory were successfully deleted.");
			}

			if (report.getErrorCode() != null || report.getErrorMessage() != null) {
				RequestContext.getCurrentInstance().addCallbackParam("error", true);
				logger.error("Error occur during report upload: {} - {}", report.getErrorCode(),
						report.getErrorMessage());
			} else {
				RequestContext.getCurrentInstance().addCallbackParam("error", false);
			}

		}

		viewBean.init();
		logger.trace(">>>>>>>>>>>>> Uploading Report <<<<<<<<<<<<<<<<<<<<<<");
	}

	private void resetTargetLevelValue() {
		viewBean.setTargetLevel(viewBean.getTargetLevels().isEmpty() ? null : viewBean.getTargetLevel());
	}

	private ReportParticipantRepresentation transformParticipantObj(ParticipantObj obj) {
		ReportParticipantRepresentation reportParticipant = new ReportParticipantRepresentation();
		reportParticipant.setUsersId(obj.getUserId());
		reportParticipant.setFirstName(obj.getFirstName());
		reportParticipant.setLastName(obj.getLastName());
		reportParticipant.setEmail(obj.getEmail());
		reportParticipant.setProjectId(obj.getProject().getProjectId());
		reportParticipant.setProjectName(obj.getProject().getName());
		reportParticipant.setCourseVersionMap(obj.courseVersionsToString());
		return reportParticipant;
	}

	private List<ReportParticipantRepresentation> transformParticipants(List<ParticipantObj> objs) {
		List<ReportParticipantRepresentation> reportParticipants = new ArrayList<ReportParticipantRepresentation>();
		for (ParticipantObj participant : viewBean.getParticipants()) {
			reportParticipants.add(transformParticipantObj(participant));
		}
		return reportParticipants;
	}

	private boolean validateReportUpload() {
		boolean valid = true;

		if (StringUtils.isBlank(viewBean.getRequestName())) {
			LabelUtils.addErrorMessage("uploadReports", "Request Name:", "Please enter request name.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		if (StringUtils.isBlank(viewBean.getReportCode())) {
			LabelUtils.addErrorMessage("uploadReports", "Report Type:", "Please select report type.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		if (viewBean.getUploadedFile() == null) {
			LabelUtils.addErrorMessage("uploadReports", "Upload Report File:", "Please upload report file.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		return valid;
	}

	public String goToDownloadReports() {
		if (viewBean.getParticipants() == null || viewBean.getParticipants().size() > 1) {
			return sessionBean.goToDownloadReports(sessionBean.getSelectedProject());
		} else {
			return sessionBean.goToDownloadReports(0);
		}
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public ReportUploadViewBean getReportUploadViewBean() {
		return viewBean;
	}

	public void setReportUploadViewBean(ReportUploadViewBean reportUploadViewBean) {
		this.viewBean = reportUploadViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public ReportRequestHelper getReportRequestHelper() {
		return reportRequestHelper;
	}

	public void setReportRequestHelper(ReportRequestHelper reportRequestHelper) {
		this.reportRequestHelper = reportRequestHelper;
	}

	public ReportWizardHelper getReportWizardHelper() {
		return reportWizardHelper;
	}

	public void setReportWizardHelper(ReportWizardHelper reportWizardHelper) {
		this.reportWizardHelper = reportWizardHelper;
	}

}
