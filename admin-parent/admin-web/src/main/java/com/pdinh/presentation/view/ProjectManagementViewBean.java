package com.pdinh.presentation.view;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.helper.CourseStatusColumnModel;

@ManagedBean(name = "projectManagementViewBean")
@ViewScoped
public class ProjectManagementViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer projectId;
	private String projectName;
	private String projectType;
	private String projectCode;
	private String targetLevel;
	private String createdBy;
	private Timestamp createdDate;
	private Timestamp dueDate;
	private String fromEmail;
	private String adobeConnectHelpFileUrl;
	private String normHeaderTitle;
	private String participantLoginUrl;
	private String loginToken;
	private String loginUsername;

	private Boolean editParticipantDisabled = true;
	private Boolean trackParticipantDisabled = true;
	private Boolean delParticipantDisabled = true;
	private Boolean addParticipantsToClipboardDisabled = true;
	private Boolean printCoachingPlanDisabled = false;
	private Boolean setCoachAndTimeframeDisabled = true;
	private Boolean projectNormsDisabled = false;
	private Boolean addExistingParticipantsDisabled = true;
	private boolean assignConsultantDisabled = true;
	private boolean manageProjectDeliveryDisabled = true;
	private boolean logInDisabled = true;
	private boolean addSingleParticipantDisabled = false;
	private boolean addExistingParticipantDisabled = false;
	private boolean importParticipantsDisabled = false;
	private boolean disableTestData = false;
	private boolean createReportsDisabled = false;
	private boolean activateInstrumentsDisabled = true;
	private boolean deliverySetupLinkDisabled = false;
	private boolean editProjectButtonDisabled = false;
	private boolean manageDocumentsLinkDisabled = false;
	private boolean projectEmailItemDisabled = false;
	private boolean abydSetupDisabled = false;
	private boolean sendEmailsButtonDisabled = false;
	private boolean manageProjectDisabled = false;
	private boolean manageSelfRegDisabled = false;

	private Boolean renderActivateInstruments = false;
	private Boolean renderSendEmails = false;
	private Boolean renderUrlGenerator = false;
	private Boolean renderTestData = false;
	private Boolean renderExportStatus = false;
	private Boolean renderSetCoachAndTimeframe = false;
	private Boolean renderPrintCoachingPlan = false;
	private Boolean renderCreateReports = false;
	private Boolean renderProjectNorms = false;
	private Boolean renderAutoScore = false;
	private boolean renderAssignConsultant = false;
	private Boolean renderViaEdgeStatus = false;
	private Boolean renderBreCrc = false;

	private ParticipantObj[] selectedParticipants;
	private List<ParticipantObj> participants;
	private List<ParticipantObj> exportParticipants;
	private List<Integer> projectUserIds;
	private boolean consultantsLoaded = false;
	private boolean retakeBtnDisabled = true;
	private String retakeParticipantName;
	private int retakeParticipantId;
	private boolean selectAll = true;
	private boolean selectAllDisabled;
	private boolean reportUploadDisabled;
	private boolean renderReportUpload = true;
	private boolean downloadReportDisabled;
	private boolean renderDownloadReport = true;

	private boolean isAbydProduct = false; // Includes multiple project types
	private boolean isGlGpsProduct = false;

	private List<CourseStatusColumnModel> preworkColumns = new ArrayList<CourseStatusColumnModel>();

	public void setSelectedParticipants(ParticipantObj[] selectedParticipants) {
		this.selectedParticipants = selectedParticipants;
	}

	public ParticipantObj[] getSelectedParticipants() {
		return selectedParticipants;
	}

	public void setEditParticipantDisabled(Boolean editParticipantDisabled) {
		this.editParticipantDisabled = editParticipantDisabled;
	}

	public Boolean getEditParticipantDisabled() {
		return editParticipantDisabled;
	}

	public Boolean getTrackParticipantDisabled() {
		return trackParticipantDisabled;
	}

	public void setTrackParticipantDisabled(Boolean trackParticipantDisabled) {
		this.trackParticipantDisabled = trackParticipantDisabled;
	}

	public void setDelParticipantDisabled(Boolean delParticipantDisabled) {
		this.delParticipantDisabled = delParticipantDisabled;
	}

	public Boolean getDelParticipantDisabled() {
		return delParticipantDisabled;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setAddParticipantsToClipboardDisabled(Boolean addParticipantsToClipboardDisabled) {
		this.addParticipantsToClipboardDisabled = addParticipantsToClipboardDisabled;
	}

	public Boolean getAddParticipantsToClipboardDisabled() {
		return addParticipantsToClipboardDisabled;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void clearSelectedParticipants() {
		this.selectedParticipants = null;
	}

	public void setExportParticipants(List<ParticipantObj> exportParticipants) {
		this.exportParticipants = exportParticipants;
	}

	public List<ParticipantObj> getExportParticipants() {
		return exportParticipants;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public Boolean getRenderUrlGenerator() {
		return renderUrlGenerator;
	}

	public void setRenderUrlGenerator(Boolean renderUrlGenerator) {
		this.renderUrlGenerator = renderUrlGenerator;
	}

	public Boolean getRenderActivateInstruments() {
		return renderActivateInstruments;
	}

	public void setRenderActivateInstruments(Boolean renderActivateInstruments) {
		this.renderActivateInstruments = renderActivateInstruments;
	}

	public Boolean getRenderSendEmails() {
		return renderSendEmails;
	}

	public void setRenderSendEmails(Boolean renderSendEmails) {
		this.renderSendEmails = renderSendEmails;
	}

	public Boolean getRenderTestData() {
		return renderTestData;
	}

	public void setRenderTestData(Boolean renderTestData) {
		this.renderTestData = renderTestData;
	}

	public Boolean getRenderExportStatus() {
		return renderExportStatus;
	}

	public void setRenderExportStatus(Boolean renderExportStatus) {
		this.renderExportStatus = renderExportStatus;
	}

	public Boolean getRenderSetCoachAndTimeframe() {
		return renderSetCoachAndTimeframe;
	}

	public void setRenderSetCoachAndTimeframe(Boolean renderSetCoachAndTimeframe) {
		this.renderSetCoachAndTimeframe = renderSetCoachAndTimeframe;
	}

	public Boolean getRenderPrintCoachingPlan() {
		return renderPrintCoachingPlan;
	}

	public void setRenderPrintCoachingPlan(Boolean renderPrintCoachingPlan) {
		this.renderPrintCoachingPlan = renderPrintCoachingPlan;
	}

	public Boolean getRenderCreateReports() {
		return renderCreateReports;
	}

	public void setRenderCreateReports(Boolean renderCreateReports) {
		this.renderCreateReports = renderCreateReports;
	}

	public List<CourseStatusColumnModel> getPreworkColumns() {
		return preworkColumns;
	}

	public void setPreworkColumns(List<CourseStatusColumnModel> preworkColumns) {
		this.preworkColumns = preworkColumns;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getAdobeConnectHelpFileUrl() {
		return adobeConnectHelpFileUrl;
	}

	public void setAdobeConnectHelpFileUrl(String adobeConnectHelpFileUrl) {
		this.adobeConnectHelpFileUrl = adobeConnectHelpFileUrl;
	}

	public Boolean getPrintCoachingPlanDisabled() {
		return printCoachingPlanDisabled;
	}

	public void setPrintCoachingPlanDisabled(Boolean printCoachingPlanDisabled) {
		this.printCoachingPlanDisabled = printCoachingPlanDisabled;
	}

	public Boolean getSetCoachAndTimeframeDisabled() {
		return setCoachAndTimeframeDisabled;
	}

	public void setSetCoachAndTimeframeDisabled(Boolean setCoachAndTimeframeDisabled) {
		this.setCoachAndTimeframeDisabled = setCoachAndTimeframeDisabled;
	}

	public Boolean getRenderProjectNorms() {
		return renderProjectNorms;
	}

	public void setRenderProjectNorms(Boolean renderProjectNorms) {
		this.renderProjectNorms = renderProjectNorms;
	}

	public Boolean getProjectNormsDisabled() {
		return projectNormsDisabled;
	}

	public void setProjectNormsDisabled(Boolean projectNormsDisabled) {
		this.projectNormsDisabled = projectNormsDisabled;
	}

	public Boolean getRenderAutoScore() {
		return renderAutoScore;
	}

	public void setRenderAutoScore(Boolean renderAutoScore) {
		this.renderAutoScore = renderAutoScore;
	}

	public String getNormHeaderTitle() {
		return normHeaderTitle;
	}

	public void setNormHeaderTitle(String normHeaderTitle) {
		this.normHeaderTitle = normHeaderTitle;
	}

	public Boolean getAddExistingParticipantsDisabled() {
		return addExistingParticipantsDisabled;
	}

	public void setAddExistingParticipantsDisabled(Boolean addExistingParticipantsDisabled) {
		this.addExistingParticipantsDisabled = addExistingParticipantsDisabled;
	}

	public boolean isRenderAssignConsultant() {
		return renderAssignConsultant;
	}

	public void setRenderAssignConsultant(boolean renderAssignConsultant) {
		this.renderAssignConsultant = renderAssignConsultant;
	}

	public boolean isAssignConsultantDisabled() {
		return assignConsultantDisabled;
	}

	public void setAssignConsultantDisabled(boolean assignConsultantDisabled) {
		this.assignConsultantDisabled = assignConsultantDisabled;
	}

	public Timestamp getDueDate() {
		return dueDate;
	}

	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	public String getParticipantLoginUrl() {
		return participantLoginUrl;
	}

	public void setParticipantLoginUrl(String participantLoginUrl) {
		this.participantLoginUrl = participantLoginUrl;
	}

	public String getLoginToken() {
		return loginToken;
	}

	public void setLoginToken(String loginToken) {
		this.loginToken = loginToken;
	}

	public String getLoginUsername() {
		return loginUsername;
	}

	public void setLoginUsername(String loginUsername) {
		this.loginUsername = loginUsername;
	}

	public List<Integer> getProjectUserIds() {
		return projectUserIds;
	}

	public void setProjectUserIds(List<Integer> projectUserIds) {
		this.projectUserIds = projectUserIds;
	}

	public boolean isConsultantsLoaded() {
		return consultantsLoaded;
	}

	public void setConsultantsLoaded(boolean consultantsLoaded) {
		this.consultantsLoaded = consultantsLoaded;
	}

	public boolean isManageProjectDeliveryDisabled() {
		return manageProjectDeliveryDisabled;
	}

	public void setManageProjectDeliveryDisabled(boolean manageProjectDeliveryDisabled) {
		this.manageProjectDeliveryDisabled = manageProjectDeliveryDisabled;
	}

	public Boolean isRenderViaEdgeStatus() {
		return renderViaEdgeStatus;
	}

	public void setRenderViaEdgeStatus(Boolean renderViaEdgeStatus) {
		this.renderViaEdgeStatus = renderViaEdgeStatus;
	}

	public Boolean isRenderBreCrc() {
		return renderBreCrc;
	}

	public void setRenderBreCrc(Boolean renderBreCrc) {
		this.renderBreCrc = renderBreCrc;
	}

	public boolean isLogInDisabled() {
		return logInDisabled;
	}

	public void setLogInDisabled(boolean logInDisabled) {
		this.logInDisabled = logInDisabled;
	}

	public boolean isAddSingleParticipantDisabled() {
		return addSingleParticipantDisabled;
	}

	public void setAddSingleParticipantDisabled(boolean addSingleParticipantDisabled) {
		this.addSingleParticipantDisabled = addSingleParticipantDisabled;
	}

	public boolean isAddExistingParticipantDisabled() {
		return addExistingParticipantDisabled;
	}

	public void setAddExistingParticipantDisabled(boolean addExistingParticipantDisabled) {
		this.addExistingParticipantDisabled = addExistingParticipantDisabled;
	}

	public boolean isImportParticipantsDisabled() {
		return importParticipantsDisabled;
	}

	public void setImportParticipantsDisabled(boolean importParticipantsDisabled) {
		this.importParticipantsDisabled = importParticipantsDisabled;
	}

	public boolean isDisableTestData() {
		return disableTestData;
	}

	public void setDisableTestData(boolean disableTestData) {
		this.disableTestData = disableTestData;
	}

	public boolean isCreateReportsDisabled() {
		return createReportsDisabled;
	}

	public void setCreateReportsDisabled(boolean createReportsDisabled) {
		this.createReportsDisabled = createReportsDisabled;
	}

	public boolean isActivateInstrumentsDisabled() {
		return activateInstrumentsDisabled;
	}

	public void setActivateInstrumentsDisabled(boolean activateInstrumentsDisabled) {
		this.activateInstrumentsDisabled = activateInstrumentsDisabled;
	}

	public boolean isDeliverySetupLinkDisabled() {
		return deliverySetupLinkDisabled;
	}

	public void setDeliverySetupLinkDisabled(boolean deliverySetupLinkDisabled) {
		this.deliverySetupLinkDisabled = deliverySetupLinkDisabled;
	}

	public boolean isEditProjectButtonDisabled() {
		return editProjectButtonDisabled;
	}

	public void setEditProjectButtonDisabled(boolean editProjectButtonDisabled) {
		this.editProjectButtonDisabled = editProjectButtonDisabled;
	}

	public boolean isManageDocumentsLinkDisabled() {
		return manageDocumentsLinkDisabled;
	}

	public void setManageDocumentsLinkDisabled(boolean manageDocumentsLinkDisabled) {
		this.manageDocumentsLinkDisabled = manageDocumentsLinkDisabled;
	}

	public boolean isProjectEmailItemDisabled() {
		return projectEmailItemDisabled;
	}

	public void setProjectEmailItemDisabled(boolean projectEmailItemDisabled) {
		this.projectEmailItemDisabled = projectEmailItemDisabled;
	}

	public boolean isAbydSetupDisabled() {
		return abydSetupDisabled;
	}

	public void setAbydSetupDisabled(boolean abydSetupDisabled) {
		this.abydSetupDisabled = abydSetupDisabled;
	}

	public boolean isSendEmailsButtonDisabled() {
		return sendEmailsButtonDisabled;
	}

	public void setSendEmailsButtonDisabled(boolean sendEmailsButtonDisabled) {
		this.sendEmailsButtonDisabled = sendEmailsButtonDisabled;
	}

	public String getRetakeParticipantName() {
		return retakeParticipantName;
	}

	public void setRetakeParticipantName(String fName, String lName) {
		retakeParticipantName = fName + " " + lName;
	}

	public int getRetakeParticipantId() {
		return retakeParticipantId;
	}

	public void setRetakeParticipantId(int retakeParticipantId) {
		this.retakeParticipantId = retakeParticipantId;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}

	public boolean isSelectAllDisabled() {
		return selectAllDisabled;
	}

	public void setSelectAllDisabled(boolean selectAllDisabled) {
		this.selectAllDisabled = selectAllDisabled;
	}

	public boolean isManageProjectDisabled() {
		return manageProjectDisabled;
	}

	public void setManageProjectDisabled(boolean manageProjectDisabled) {
		this.manageProjectDisabled = manageProjectDisabled;
	}

	public boolean isReportUploadDisabled() {
		return reportUploadDisabled;
	}

	public void setReportUploadDisabled(boolean reportUploadDisabled) {
		this.reportUploadDisabled = reportUploadDisabled;
	}

	public boolean isRenderReportUpload() {
		return renderReportUpload;
	}

	public void setRenderReportUpload(boolean renderReportUpload) {
		this.renderReportUpload = renderReportUpload;
	}

	public boolean isDownloadReportDisabled() {
		return downloadReportDisabled;
	}

	public void setDownloadReportDisabled(boolean downloadReportDisabled) {
		this.downloadReportDisabled = downloadReportDisabled;
	}

	public boolean isRenderDownloadReport() {
		return renderDownloadReport;
	}

	public void setRenderDownloadReport(boolean renderDownloadReport) {
		this.renderDownloadReport = renderDownloadReport;
	}

	public boolean isRetakeBtnDisabled() {
		return retakeBtnDisabled;
	}

	public void setRetakeBtnDisabled(boolean retakeBtnDisabled) {
		this.retakeBtnDisabled = retakeBtnDisabled;
	}

	public boolean getIsAbydProduct() {
		return this.isAbydProduct;
	}

	public void setIsAbydProduct(boolean value) {
		this.isAbydProduct = value;
	}

	public boolean getIsGlGpsProduct() {
		return this.isGlGpsProduct;
	}

	public void setIsGlGpsProduct(boolean value) {
		this.isGlGpsProduct = value;
	}

	public boolean isManageSelfRegDisabled() {
		return manageSelfRegDisabled;
	}

	public void setManageSelfRegDisabled(boolean manageSelfRegDisabled) {
		this.manageSelfRegDisabled = manageSelfRegDisabled;
	}
}
