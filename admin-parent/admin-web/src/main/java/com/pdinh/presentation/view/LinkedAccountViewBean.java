package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.persistence.ms.entity.User;

@ManagedBean(name="linkedAccountViewBean")
@ViewScoped
public class LinkedAccountViewBean implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LinkedAccountViewBean.class);
	private ExternalSubjectDataObj selectedUser;
	private User existingLinkedUser;
	private boolean linkRadioDisabled = false;
	private String selectedLinkOption = "";
	private String usersIdInput = "";
	private boolean linkRadioSubmitDisabled = true;
	private boolean lookupByEmailButtonDisabled = true;
	private boolean refreshAccountButtonDisabled = true;
	private boolean unlinkAccountButtonDisabled =true;
	private boolean usersIdInputDisabled = true;
	
	private boolean saveLinkedAccountButtonDisabled = true;
	
	private LazyDataModel<ExternalSubjectDataObj> lazyModel;
	private ExternalSubjectDataObj selectedSubject;
	private List<ExternalSubjectDataObj> subjects;
	
	
	public ExternalSubjectDataObj getSelectedUser() {
		return selectedUser;
	}
	public void setSelectedUser(ExternalSubjectDataObj selectedUser) {
		this.selectedUser = selectedUser;
	}
	public boolean isLinkRadioDisabled() {
		return linkRadioDisabled;
	}
	public void setLinkRadioDisabled(boolean linkRadioDisabled) {
		this.linkRadioDisabled = linkRadioDisabled;
	}
	public String getSelectedLinkOption() {
		return selectedLinkOption;
	}
	public void setSelectedLinkOption(String selectedLinkOption) {
		this.selectedLinkOption = selectedLinkOption;
	}
	public String getUsersIdInput() {
		return usersIdInput;
	}
	public void setUsersIdInput(String usersIdInput) {
		this.usersIdInput = usersIdInput;
	}
	public boolean isLinkRadioSubmitDisabled() {
		return linkRadioSubmitDisabled;
	}
	public void setLinkRadioSubmitDisabled(boolean linkRadioSubmitDisabled) {
		this.linkRadioSubmitDisabled = linkRadioSubmitDisabled;
	}
	public boolean isLookupByEmailButtonDisabled() {
		return lookupByEmailButtonDisabled;
	}
	public void setLookupByEmailButtonDisabled(boolean lookupByEmailButtonDisabled) {
		this.lookupByEmailButtonDisabled = lookupByEmailButtonDisabled;
	}
	public User getExistingLinkedUser() {
		return existingLinkedUser;
	}
	public void setExistingLinkedUser(User existingLinkedUser) {
		this.existingLinkedUser = existingLinkedUser;
	}
	public boolean isRefreshAccountButtonDisabled() {
		return refreshAccountButtonDisabled;
	}
	public void setRefreshAccountButtonDisabled(boolean refreshAccountButtonDisabled) {
		this.refreshAccountButtonDisabled = refreshAccountButtonDisabled;
	}
	public boolean isUnlinkAccountButtonDisabled() {
		return unlinkAccountButtonDisabled;
	}
	public void setUnlinkAccountButtonDisabled(boolean unlinkAccountButtonDisabled) {
		this.unlinkAccountButtonDisabled = unlinkAccountButtonDisabled;
	}

	public boolean isSaveLinkedAccountButtonDisabled() {
		return saveLinkedAccountButtonDisabled;
	}
	public void setSaveLinkedAccountButtonDisabled(
			boolean saveLinkedAccountButtonDisabled) {
		this.saveLinkedAccountButtonDisabled = saveLinkedAccountButtonDisabled;
	}
	public LazyDataModel<ExternalSubjectDataObj> getLazyModel() {
		return lazyModel;
	}
	public void setLazyModel(LazyDataModel<ExternalSubjectDataObj> lazyModel) {
		this.lazyModel = lazyModel;
	}
	public ExternalSubjectDataObj getSelectedSubject() {
		return selectedSubject;
	}
	public void setSelectedSubject(ExternalSubjectDataObj selectedSubject) {
		this.selectedSubject = selectedSubject;
	}
	public List<ExternalSubjectDataObj> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<ExternalSubjectDataObj> subjects) {
		this.subjects = subjects;
	}
	public boolean isUsersIdInputDisabled() {
		return usersIdInputDisabled;
	}
	public void setUsersIdInputDisabled(boolean usersIdInputDisabled) {
		this.usersIdInputDisabled = usersIdInputDisabled;
	}

}
