package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.ms.dao.ScheduledEmailCompanyDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.data.ms.dao.ScheduledEmailTextDao;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.ScheduledEmailCompany;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.EmailTranslationObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EditEmailTemplateViewBean;
import com.pdinh.presentation.view.EmailTemplateViewBean;

@ManagedBean(name = "editEmailTemplateControllerBean")
@ViewScoped
public class EditEmailTemplateControllerBean  implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EditEmailTemplateControllerBean.class);
	
//	@EJB
//	LanguageDao languageDao;
//
//	@EJB
//	CompanyDao companyDao;
//
//	@EJB
//	ScheduledEmailDefDao scheduledEmailDefDao;
//
//	@EJB
//	ScheduledEmailTextDao scheduledEmailTextDao;
//
//	@EJB
//	ScheduledEmailCompanyDao scheduledEmailCompanyDao;

//	@ManagedProperty(name = "editEmailTemplateViewBean", value = "#{editEmailTemplateViewBean}")
//	private EditEmailTemplateViewBean editEmailTemplateViewBean;
	
//	@ManagedProperty(name = "emailTemplateViewBean", value = "#{emailTemplateViewBean}")
//	private EmailTemplateViewBean emailTemplateViewBean;
	
//	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
//	private SessionBean sessionBean;

//	private boolean firstView = true;
//	private Boolean showEmailPreview = false;
//	private Boolean disableNewLanguageSelect = true;
//	private Boolean disableTemplateEdit = true;
//	private Boolean disableTemplateClone = true;
//	private Boolean disableTranslationAdd = false;
//	private Boolean disableTranslationEdit = true;
//	private Boolean showEmailPanel = true;
//	private EmailTemplateObj currentTemplate;
//	private EmailTranslationObj currentLanguage;
//	private EmailTextObj currentEto;
	
	private Boolean isSystemLevel = false;
	
	private String templateId;
	//private String emailId;

//	public void clear() {
//		firstView = true;
////		showEmailPreview = false;
////		disableNewLanguageSelect = true;
////		disableTemplateEdit = true;
////		disableTemplateClone = true;
////		disableTranslationAdd = false;
////		disableTranslationEdit = true;
////		showEmailPanel = true;
//		currentTemplate = null;
//		currentLanguage = null;
//		currentEto = null;
//	}

//	public void initForView() {
//		log.debug("EDITEmailTemplateListControllerBean: In initForView");
//		
//		if (firstView) {
//			log.debug("EDITEmailTemplateListControllerBean: firstView");
//			firstView = false;
//			// clear current template but note id of selected
//			// so it can be re-selected
//			Integer currentTemplateId = 0;
//			ScheduledEmailDef sed;
//			ScheduledEmailText set;
//			EmailTemplateObj eto;
//			EmailTextObj etxo;
//			Iterator<ScheduledEmailDef> sedi;
//			Iterator<ScheduledEmailText> seti;
//			List<EmailTextObj> emailTexts;
//			List<ScheduledEmailDef> companyTemplates;
//			List<ScheduledEmailDef> systemTemplates;
//
//			if (currentTemplate != null)
//				currentTemplateId = currentTemplate.getId();
//			currentTemplate = null;
//
//			
//			Integer companyId =  0; 
//			Integer projectId =  0; 
//		
//			templateId =  new Integer(getEmailTemplateViewBean().getCurrentTemplate().getId()).toString(); 
//			//emailId =  getEmailTemplateViewBean().//sessionBean.getCurrentEmailId();
//			
//			if( templateId.equals(""))	{
//				log.debug("the templateId has no data");
//			}else{
//				log.debug("templateId {}", templateId);
//				
//			}
//
//			if( emailId.equals("")){
//				log.debug("the email has no data");
//			}else{
//				log.debug("emailId {}", emailId);
//			}
//			
//			
//			try{
//				companyId =  sessionBean.getCompany().getCompanyId();
//				log.debug("companyId  {} ",  companyId);	
//			}catch (Exception e){
//				companyId = null;
//			}
//	
//			try{
//				projectId =  sessionBean.getProject().getProjectId();
//				log.debug("projectId  {} ", projectId);	
//			}catch (Exception e){
//				projectId = null;
//			}
//			
//			// if there is no companyId, 
//			// we are at the SYSTEM/GLOBAL level for templates
//			// this may be temporary as UI changes.... 
//			// will need to do this if NOT first view, too....
//			if(companyId == null){				
//				log.debug("companyId = null, so at SYS level!" );
//				 isSystemLevel = true;  // set to say we're looking for System Level Templates
//			} else if (companyId != null  && projectId == null ){
//				// we're at a client level
//				log.debug("companyId != null  && projectId == null, so at CLIENT level!" );
//			}else if (companyId != null && projectId != null ){
//				// we're at a project level
//				log.debug("companyId != null && projectId != null, so at PROJECT level!" );
//			}
//			
//			sed  = scheduledEmailDefDao.findById(new Integer(templateId));
//			if(sed == null){
//				log.debug("scheduledEmailDefDao == null  ");
//			}else{
//				log.debug("sed.getemailtype {} ", sed.getEmailType());
//			}
//			
//			
//			set =  this.scheduledEmailTextDao.findById(new Integer(emailId));
//			
//			this.getEditEmailTemplateViewBean().setTemplateType(sed.getEmailType());
//			this.getEditEmailTemplateViewBean().setTemplateTitle(sed.getTitle());
//			this.getEditEmailTemplateViewBean().setEditEmailSubject(set.getSubject());
//			this.getEditEmailTemplateViewBean().setEditEmailBody(set.getText());
//			
//			currentEto = new EmailTextObj(set.getId(), set.getSubject(), set.getText(), set.getLanguage().getCode()); 
//			 
//			getEditEmailTemplateViewBean().setCurrentLanguage("en");			
//			getEditEmailTemplateViewBean().setCopyPasteKeywords(new ArrayList<String>());
//
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partFirstName]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partLastName]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partEmailAddress]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partId]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partPassword]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partOption1]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partOption2]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partOption3]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partManagerName]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partManagerEmail]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[projectName]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[clientName]");
//			getEditEmailTemplateViewBean().getCopyPasteKeywords().add("[partUrl]");
//
//			if (currentTemplate != null) {
//				//updateLanguageList();
//			}
//		}
//	}

/*	
	public void initEditTranslation(ActionEvent event) {
		log.debug("In initEditTranslation(ActionEvent event)");

		String selectedLang = getEditEmailTemplateViewBean().getCurrentLanguage();
		log.debug("String selectedLang = .... ", selectedLang);// empty
		
		
		EmailTextObj eto = getEmailTextObjByLang(selectedLang);
		currentEto = eto;
		ScheduledEmailText set = scheduledEmailTextDao.findById(currentEto.getId());
		getEditEmailTemplateViewBean().setTemplateType(currentTemplate.getType());
		getEditEmailTemplateViewBean().setTemplateTitle(currentTemplate.getTitle());
		getEditEmailTemplateViewBean().setEditEmailBody(set.getText());
		getEditEmailTemplateViewBean().setEditEmailSubject(set.getSubject());
		
		getEditEmailTemplateViewBean().setCurrentEmailTextObj(currentEto);
		
		log.debug("currentEto.... ", eto.getLangCode());// empty
		log.debug("eto.getSubject() .... ", eto.getSubject());// empty
		log.debug("eto.getEmailText().... ", eto.getEmailText());   // empty
		
	}

	public EmailTextObj getEmailTextObjByLang(String langCode) {
		System.out.println("In getEmailTextObjByLang");
		List<EmailTextObj> list = currentTemplate.getEmailText();
		Iterator<EmailTextObj> iterator = list.iterator();
		while (iterator.hasNext()) {
			EmailTextObj emailText = iterator.next();
			if (emailText.getLangCode().equals(langCode)) {
				return emailText;
			}
		}
		return null;
	}

	public EditEmailTemplateViewBean getEditEmailTemplateViewBean() {
		return editEmailTemplateViewBean;
	}

	public void setEditEmailTemplateViewBean(EditEmailTemplateViewBean editEmailTemplateViewBean) {
		this.editEmailTemplateViewBean = editEmailTemplateViewBean;
	}
	

	public void setDisableTemplateEdit(Boolean enableTemplateEdit) {
		this.disableTemplateEdit = enableTemplateEdit;
	}

	public Boolean getDisableTemplateEdit() {
		return disableTemplateEdit;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}
	
	public void setTemplateId(String templateId) {
	this.templateId = templateId;
}

public String getTemplateId() {
	return templateId;
}

//public void setEmailId(String emailId) {
//	this.emailId = emailId;
//}
//
//public String getEmailId() {
//	return emailId;
//}	
/*
	public void setCurrentEto(EmailTextObj currentEto) {
		this.currentEto = currentEto;
	}

	public EmailTextObj getCurrentEto() {
		return currentEto;
	}

	public void setDisableTranslationEdit(Boolean disableTranslationEdit) {
		this.disableTranslationEdit = disableTranslationEdit;
	}

	public Boolean getDisableTranslationEdit() {
		return disableTranslationEdit;
	}

	public void setShowEmailPanel(Boolean showEmailPanel) {
		this.showEmailPanel = showEmailPanel;
	}

	public Boolean getShowEmailPanel() {
		return showEmailPanel;
	}

	public void setDisableTranslationAdd(Boolean disableTranslationAdd) {
		this.disableTranslationAdd = disableTranslationAdd;
	}

	public Boolean getDisableTranslationAdd() {
		return disableTranslationAdd;
	}

	public void setDisableTemplateClone(Boolean disableTemplateClone) {
		this.disableTemplateClone = disableTemplateClone;
	}

	public Boolean getDisableTemplateClone() {
		return disableTemplateClone;
	}

	*/
	
}
