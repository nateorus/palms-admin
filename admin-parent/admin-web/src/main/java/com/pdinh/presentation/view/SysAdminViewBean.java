package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.presentation.helper.RoleColumnModel;

@ManagedBean(name="sysAdminViewBean")
@ViewScoped
public class SysAdminViewBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List <ExternalSubjectDataObj> subjects;
	private boolean auditLogButtonDisabled = true;
	private boolean assignRolesButtonDisabled = true;
	private boolean viewRolesButtonDisabled = true;
	private boolean compareRolesButtonDisabled = true;
	private boolean reinitRolesButtonDisabled = true;
	private boolean linkedAccountButtonDisabled = true;
	private boolean linkedIdColumnRendered = false;
	private ExternalSubjectDataObj selectedSubject;
	private List <ExternalSubjectDataObj> selectedSubjects;
	private RoleContext context;
	private PalmsSubject selectedPalmsSubject;
	private List<RoleColumnModel> roleColumns = new ArrayList<RoleColumnModel>();
	private List<Role> roles;
	private Role selectedRole;
	private int selectedRoleId = 0;
	private PalmsRealm realm;
	private List <ExternalSubjectDataObj> subjectsWithRoles = new ArrayList<ExternalSubjectDataObj>();
	private boolean renderedCompareRoles = false;
	private boolean palmsUsersRendered = true;
	private boolean systemStatusRendered;
	private int activeIndex;

	public List <ExternalSubjectDataObj> getSubjects() {
		return subjects;
	}

	public void setSubjects(List <ExternalSubjectDataObj> subjects) {
		this.subjects = subjects;
	}

	public boolean isAuditLogButtonDisabled() {
		return auditLogButtonDisabled;
	}

	public void setAuditLogButtonDisabled(boolean auditLogButtonDisabled) {
		this.auditLogButtonDisabled = auditLogButtonDisabled;
	}

	public boolean isAssignRolesButtonDisabled() {
		return assignRolesButtonDisabled;
	}

	public void setAssignRolesButtonDisabled(boolean assignRolesButtonDisabled) {
		this.assignRolesButtonDisabled = assignRolesButtonDisabled;
	}

	public ExternalSubjectDataObj getSelectedSubject() {
		return selectedSubject;
	}

	public void setSelectedSubject(ExternalSubjectDataObj selectedSubject) {
		this.selectedSubject = selectedSubject;
	}

	public RoleContext getContext() {
		return context;
	}

	public void setContext(RoleContext context) {
		this.context = context;
	}

	public PalmsSubject getSelectedPalmsSubject() {
		return selectedPalmsSubject;
	}

	public void setSelectedPalmsSubject(PalmsSubject selectedPalmsSubject) {
		this.selectedPalmsSubject = selectedPalmsSubject;
	}

	public List <ExternalSubjectDataObj> getSelectedSubjects() {
		return selectedSubjects;
	}

	public void setSelectedSubjects(List <ExternalSubjectDataObj> selectedSubjects) {
		this.selectedSubjects = selectedSubjects;
	}

	public boolean isViewRolesButtonDisabled() {
		return viewRolesButtonDisabled;
	}

	public void setViewRolesButtonDisabled(boolean viewRolesButtonDisabled) {
		this.viewRolesButtonDisabled = viewRolesButtonDisabled;
	}

	public List<RoleColumnModel> getRoleColumns() {
		return roleColumns;
	}

	public void setRoleColumns(List<RoleColumnModel> roleColumns) {
		this.roleColumns = roleColumns;
	}

	public PalmsRealm getRealm() {
		return realm;
	}

	public void setRealm(PalmsRealm realm) {
		this.realm = realm;
	}

	public List <ExternalSubjectDataObj> getSubjectsWithRoles() {
		return subjectsWithRoles;
	}

	public void setSubjectsWithRoles(List <ExternalSubjectDataObj> subjectsWithRoles) {
		this.subjectsWithRoles = subjectsWithRoles;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(Role selectedRole) {
		this.selectedRole = selectedRole;
	}

	public int getSelectedRoleId() {
		return selectedRoleId;
	}

	public void setSelectedRoleId(int selectedRoleId) {
		this.selectedRoleId = selectedRoleId;
	}

	public boolean isCompareRolesButtonDisabled() {
		return compareRolesButtonDisabled;
	}

	public void setCompareRolesButtonDisabled(boolean compareRolesButtonDisabled) {
		this.compareRolesButtonDisabled = compareRolesButtonDisabled;
	}

	public boolean isRenderedCompareRoles() {
		return renderedCompareRoles;
	}

	public void setRenderedCompareRoles(boolean renderedCompareRoles) {
		this.renderedCompareRoles = renderedCompareRoles;
	}

	public boolean isReinitRolesButtonDisabled() {
		return reinitRolesButtonDisabled;
	}

	public void setReinitRolesButtonDisabled(boolean reinitRolesButtonDisabled) {
		this.reinitRolesButtonDisabled = reinitRolesButtonDisabled;
	}

	public boolean isLinkedAccountButtonDisabled() {
		return linkedAccountButtonDisabled;
	}

	public void setLinkedAccountButtonDisabled(boolean linkedAccountButtonDisabled) {
		this.linkedAccountButtonDisabled = linkedAccountButtonDisabled;
	}

	public boolean isLinkedIdColumnRendered() {
		return linkedIdColumnRendered;
	}

	public void setLinkedIdColumnRendered(boolean linkedIdColumnRendered) {
		this.linkedIdColumnRendered = linkedIdColumnRendered;
	}

	public boolean isPalmsUsersRendered() {
		return palmsUsersRendered;
	}

	public void setPalmsUsersRendered(boolean palmsUsersRendered) {
		this.palmsUsersRendered = palmsUsersRendered;
	}

	public boolean isSystemStatusRendered() {
		return systemStatusRendered;
	}

	public void setSystemStatusRendered(boolean systemStatusRendered) {
		this.systemStatusRendered = systemStatusRendered;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}



}
