package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.persistence.entity.AuditEvent;

@ManagedBean(name = "auditLogViewBean")
@ViewScoped
public class AuditLogViewBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private AuditEvent selectedEvent;
	private List<AuditEvent> auditEvents;
	private List<AuditEvent> filteredEvents;
	private List<String> searchStrings;

	public AuditEvent getSelectedEvent() {
		return selectedEvent;
	}

	public void setSelectedEvent(AuditEvent selectedEvent) {
		this.selectedEvent = selectedEvent;
	}

	public List<AuditEvent> getAuditEvents() {
		return auditEvents;
	}

	public void setAuditEvents(List<AuditEvent> auditEvents) {
		this.auditEvents = auditEvents;
	}

	public List<AuditEvent> getFilteredEvents() {
		return filteredEvents;
	}

	public void setFilteredEvents(List<AuditEvent> filteredEvents) {
		this.filteredEvents = filteredEvents;
	}

	public List<String> getSearchStrings() {
		return searchStrings;
	}

	public void setSearchStrings(List<String> searchStrings) {
		this.searchStrings = searchStrings;
	}

}
