package com.pdinh.presentation.managedbean;

import java.io.Serializable;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@ManagedBean(name = "startupConfigBean", eager = true)
@ApplicationScoped
public class StartupConfigBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	@PostConstruct
	public void postConstruct() {
		initStartupConfig();
	}

	public void initStartupConfig() {
		initAllowInvalidCerts();

	}

	public void initAllowInvalidCerts() {
		/*
		 * Allow acceptance of invalid certs, this should only be done in dev /
		 * QA environments where we are using self-signed certs. In production
		 * allowInvalidCerts shouldn't be added at all.
		 */
		if ("true".equals(configProperties.getProperty("allowInvalidCerts"))) {
			System.out.println("StartupConfigBean allowing invalid certs");
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			try {
				SSLContext sslContext = SSLContext.getInstance("SSL");
				sslContext.init(null, trustAllCerts, new SecureRandom());

				// use what ever host the certificate is using
				HostnameVerifier hv = new HostnameVerifier() {
					@Override
					public boolean verify(String urlHostName, SSLSession session) {
						return true;
					}
				};

				HttpsURLConnection.setDefaultHostnameVerifier(hv);
				HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

}
