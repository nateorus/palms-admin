package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.manage.emailtemplates.EmailTemplatesService;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.helper.TextUtils;
import com.pdinh.presentation.helper.Utils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EmailTemplatesListCompViewBean;

@ManagedBean(name = "emailTemplatesListCompControllerBean")
@ViewScoped
public class EmailTemplatesListCompControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	final static Logger log = LoggerFactory.getLogger(EmailTemplatesListCompControllerBean.class);

	@EJB
	private MailTemplatesServiceLocalImpl mailTemplatesService;

	@EJB
	private EmailTemplatesService emailTemplatesService;

	@ManagedProperty(name = "editEmailTemplateDetailsCompControllerBean", value = "#{editEmailTemplateDetailsCompControllerBean}")
	private EditEmailTemplateDetailsCompControllerBean editEmailTemplateDetailsCompControllerBean;

	@ManagedProperty(name = "emailTemplatesListCompViewBean", value = "#{emailTemplatesListCompViewBean}")
	private EmailTemplatesListCompViewBean emailTemplatesListCompViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	boolean fistTime = true;

	/**
	 * initial set up of the Templates List tab..
	 */
	public void initEmailTemplateList() {
		if (isFistTime()) {
			Subject subject = SecurityUtils.getSubject();
			String currentLevel = sessionBean.getTemplateLevel();

			if (currentLevel.equals(ScheduledEmailDef.PROJ)) {
				if (subject.isPermitted("createProjectEmailTemplate")) {
					emailTemplatesListCompViewBean.setDisableCreateNewTemplate(false);
				}
			} else if (currentLevel.equals(ScheduledEmailDef.CLIENT)) {
				if (subject.isPermitted("createClientEmailTemplate")) {
					emailTemplatesListCompViewBean.setDisableCreateNewTemplate(false);
				}
			} else if (currentLevel.equals(ScheduledEmailDef.GLOBAL)) {
				if (subject.isPermitted("createGlobalEmailTemplate")) {
					emailTemplatesListCompViewBean.setDisableCreateNewTemplate(false);
				}
			} else if (currentLevel.equals(ScheduledEmailDef.SYS)) {
				if (subject.isPermitted("createSystemEmailTemplate")) {
					emailTemplatesListCompViewBean.setDisableCreateNewTemplate(false);
				}
			} else {
				emailTemplatesListCompViewBean.setDisableCreateNewTemplate(true);
			}

			emailTemplatesListCompViewBean.setTemplates(new ArrayList<EmailTemplateObj>());
			emailTemplatesListCompViewBean.setTemplates(getTemplatesList(currentLevel, sessionBean.getCompany(),
					sessionBean.getProject()));

			setFistTime(false);

			log.debug(">>>>>>>>>>>>>>>> initEmailTemplateList <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		}
	}

	/**
	 * the actual getting of the templates list
	 */
	public List<EmailTemplateObj> getTemplatesList(String currentLevel, Company company, Project project) {

		List<ScheduledEmailDef> templates = emailTemplatesService.getEmailTemplatesByLevel(currentLevel, company,
				project);
		log.debug("Finished Getting ScheduledEmailDefs");
		List<EmailTemplateObj> emailTemplates = new ArrayList<EmailTemplateObj>();

		for (ScheduledEmailDef template : templates) {
			log.trace("Got Template: {}", template.getTitle());
			emailTemplates.add(emailTemplatesService.scheduledEmailDef2EmailTemplateObj(template));
		}

		return emailTemplates;

	}

	public void initCloneTemplate(ActionEvent event) {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		emailTemplatesListCompViewBean.setCloneTemplateTitle(template.getTitle());
		emailTemplatesListCompViewBean.setTemplateConst(template.getStyle());
		emailTemplatesListCompViewBean.setTemplateType(template.getType());
		emailTemplatesListCompViewBean.setTemplateIsTemplate(template.getIsTemplate());
		emailTemplatesListCompViewBean.setTemplateLevel(getSessionBean().getTemplateLevel());
	}

	public void initEditTemplate(ActionEvent event) {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		editEmailTemplateDetailsCompControllerBean.initForEdit(template);
	}

	public void initCreateNewTemplate(ActionEvent event) {
		editEmailTemplateDetailsCompControllerBean.iniForNew();
	}

	public void handleSelectTemplate(SelectEvent evt) {
		Subject subject = SecurityUtils.getSubject();
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		EmailTextObj defaultText = emailTemplatesService.findEmailTextByLangCode(template, "en");

		if (template.getLevel().equals(ScheduledEmailDef.SYS)) {
			if (subject.isPermitted("editSystemEmailTemplate")) {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(false);
			} else {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(true);
			}
		} else if (template.getLevel().equals(ScheduledEmailDef.GLOBAL)) {
			if (subject.isPermitted("editGlobalEmailTemplate")) {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(false);
			} else {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(true);
			}
		} else if (template.getLevel().equals(ScheduledEmailDef.CLIENT)) {
			if (subject.isPermitted("editClientEmailTemplate")) {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(false);
			} else {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(true);
			}
		} else if (template.getLevel().equals(ScheduledEmailDef.PROJ)) {
			if (subject.isPermitted("editProjectEmailTemplate")) {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(false);
			} else {
				emailTemplatesListCompViewBean.setDisableTemplateEdit(true);
			}
		} else {
			emailTemplatesListCompViewBean.setDisableTemplateEdit(true);
		}

		if (template.getLevel().equals(ScheduledEmailDef.SYS) || template.getLevel().equals(ScheduledEmailDef.GLOBAL)
				|| (!emailTemplatesService.getEmailRuleNamesByTemplate(template).isEmpty())) {
			// disallow deleting sys and global email templates regardless of
			// permissions
			emailTemplatesListCompViewBean.setDisableTemplateDelete(true);
		} else if (template.getLevel().equals(ScheduledEmailDef.CLIENT)) {
			if (subject.isPermitted("deleteClientEmailTemplate")) {
				emailTemplatesListCompViewBean.setDisableTemplateDelete(false);
			} else {
				emailTemplatesListCompViewBean.setDisableTemplateDelete(true);
			}
		} else if (template.getLevel().equals(ScheduledEmailDef.PROJ)) {
			if (subject.isPermitted("deleteProjectEmailTemplate")) {
				emailTemplatesListCompViewBean.setDisableTemplateDelete(false);
			} else {
				emailTemplatesListCompViewBean.setDisableTemplateDelete(true);
			}
		}

		emailTemplatesListCompViewBean.setCurrentEmailLangs(emailTemplatesService.getTemplateLanguges(template));
		emailTemplatesListCompViewBean.setCurrentLanguage("en");
		emailTemplatesListCompViewBean.setEmailSubject(defaultText.getSubject());

		emailTemplatesListCompViewBean.setEmailBody(TextUtils.getHtmlText(defaultText.getEmailText()));

		if (subject.isPermitted("cloneEmailTemplate")) {
			emailTemplatesListCompViewBean.setDisableTemplateClone(false);
		} else {
			emailTemplatesListCompViewBean.setDisableTemplateClone(true);
		}
		emailTemplatesListCompViewBean.setShowEmailPreview(true);

	}

	public void handleUnselectTemplate(UnselectEvent evt) {
		resetDefaults(false);
	}

	public void handleSendTemplatePreview() {
		validateAndSend();
	}

	private void validateAndSend() {
		Boolean errors = validateEmailFields();
		if (!errors) {
			sendTemplatePreviewEmail();
		}
	}

	private Boolean validateEmailFields() {
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rc = RequestContext.getCurrentInstance();
		String sendTo = emailTemplatesListCompViewBean.getSendToAddress();
		String sendFrom = emailTemplatesListCompViewBean.getSentFromAddress();
		Boolean error = false;

		// check for valid email send to:
		if (!Utils.validateEmailAddress(sendTo)) {
			fc.addMessage("sendTemplatePreviewPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Please use valid 'Send To' address.", ""));
			rc.addCallbackParam("notValid", true);
			log.error("Please use valid 'Send To' address ({}).", sendTo);
			error = true;
		}

		// check for valid email sent from:
		if (!Utils.validateEmailAddress(sendFrom)) {
			fc.addMessage("sendTemplatePreviewPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Please use valid 'Sent From' address.", ""));
			rc.addCallbackParam("notValid", true);
			log.error("Please use valid 'Sent From' address ({}).", sendFrom);
			error = true;
		}

		return error;
	}

	public void initSendTemplatePreview() {
	}

	private void sendTemplatePreviewEmail() {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		String currentLanguage = emailTemplatesListCompViewBean.getCurrentLanguage();
		EmailTextObj currentText = emailTemplatesService.findEmailTextByLangCode(template, currentLanguage);
		String messageText = currentText.getEmailText();
		if (currentText != null) {
			if (currentText.getId() != null) {
				mailTemplatesService.sendPreviewTemplate(currentText.getSubject(),
						emailTemplatesListCompViewBean.getSentFromAddress(),
						emailTemplatesListCompViewBean.getSentFromAddress(),
						emailTemplatesListCompViewBean.getSendToAddress(), messageText);
			} else {
				log.error("Unable to send email because cannot find email text (log entry id {})", 0);
			}
		}
	}

	public void handleLanguageChange(AjaxBehaviorEvent evt) {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		String currentLanguage = emailTemplatesListCompViewBean.getCurrentLanguage();
		EmailTextObj currentText = emailTemplatesService.findEmailTextByLangCode(template, currentLanguage);
		currentText.setEmailText(TextUtils.getHtmlText(currentText.getEmailText()));
		emailTemplatesListCompViewBean.setEmailSubject(currentText.getSubject());
		emailTemplatesListCompViewBean.setEmailBody(currentText.getEmailText());
		emailTemplatesListCompViewBean.setSelectedEmailText(currentText);
	}

	public void deleteTemplate(ActionEvent event) {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		log.debug(">>>>>>>>>>>> template = {}", template);
		emailTemplatesService.deleteTemplate(template);
		resetDefaults(true);
	}

	public void saveNewTemplate(ActionEvent event) {
		getEditEmailTemplateDetailsCompControllerBean().handleCreateTemplate();
		resetDefaults(true);
	}

	public void cancelEditTemplate(ActionEvent event) {
		resetDefaults(true);
	}

	public void cancelCreateTemplate(ActionEvent event) {
		// add cancel create template here 
	}

	public void saveEditTemplate(ActionEvent event) {
		getEditEmailTemplateDetailsCompControllerBean().handleUpdateTemplate();
		resetDefaults(true);
	}

	public void handleCloneTemplate(ActionEvent event) {
		String level = ScheduledEmailDef.PROJ;
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		String title = emailTemplatesListCompViewBean.getCloneTemplateTitle();

		if (sessionBean.getTemplateLevel().equals(ScheduledEmailDef.SYS)
				|| sessionBean.getTemplateLevel().equals(ScheduledEmailDef.GLOBAL)
				|| sessionBean.getTemplateLevel().equals(ScheduledEmailDef.CLIENT)) {
			level = ScheduledEmailDef.CLIENT;
		}
		if (!validateCloneTemplate(title)) {
			emailTemplatesService.cloneTemplate(template, title, level, sessionBean.getCompany(),
					sessionBean.getProject());
			resetDefaults(true);
		}
	}

	private boolean validateCloneTemplate(String templateName) {
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rc = RequestContext.getCurrentInstance();
		boolean error = false;

		if (templateName.trim().isEmpty()) {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Name required.", ""));
			log.error("Template Name required.");
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		return error;
	}

	public void resetDefaults(boolean refresh) {
		emailTemplatesListCompViewBean.setShowEmailPreview(false);
		emailTemplatesListCompViewBean.setDisableTemplateEdit(true);
		emailTemplatesListCompViewBean.setDisableTemplateClone(true);
		emailTemplatesListCompViewBean.setShowEmailPanel(true);
		emailTemplatesListCompViewBean.setSelectedTemplate(null);
		emailTemplatesListCompViewBean.setFilteredTemplates(null);
		if (refresh) {
			refresh();
		}
	}

	private void refresh() {
		setFistTime(true);
	}

	// GETTERS AND SETTERS
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public EditEmailTemplateDetailsCompControllerBean getEditEmailTemplateDetailsCompControllerBean() {
		return editEmailTemplateDetailsCompControllerBean;
	}

	public void setEditEmailTemplateDetailsCompControllerBean(
			EditEmailTemplateDetailsCompControllerBean editEmailTemplateDetailsCompControllerBean) {
		this.editEmailTemplateDetailsCompControllerBean = editEmailTemplateDetailsCompControllerBean;
	}

	public EmailTemplatesListCompViewBean getEmailTemplatesListCompViewBean() {
		return emailTemplatesListCompViewBean;
	}

	public void setEmailTemplatesListCompViewBean(EmailTemplatesListCompViewBean emailTemplatesListCompViewBean) {
		this.emailTemplatesListCompViewBean = emailTemplatesListCompViewBean;
	}

	public boolean isFistTime() {
		return fistTime;
	}

	public void setFistTime(boolean fistTime) {
		this.fistTime = fistTime;
	}

}
