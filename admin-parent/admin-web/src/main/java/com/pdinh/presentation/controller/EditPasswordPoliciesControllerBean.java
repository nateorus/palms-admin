package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.CompanyPasswordPolicyDao;
import com.pdinh.data.ms.dao.PasswordPolicyDao;
import com.pdinh.data.ms.dao.PasswordPolicyUserStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.CompanyPasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.presentation.converter.ScheduledEmailDefConverter;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EditPasswordPoliciesViewBean;

@ManagedBean(name = "editPasswordPoliciesControllerBean")
@ViewScoped
public class EditPasswordPoliciesControllerBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(EditPasswordPoliciesControllerBean.class);
	
	@EJB
	private PasswordPolicyDao passwordPolicyDao;
	
	@EJB
	private CompanyPasswordPolicyDao companyPasswordPolicyDao;
	
	@EJB
	private ScheduledEmailDefDao emailDefDao;
	
	@EJB
	private PasswordPolicyUserStatusDao pwdPolicyUserStatusDao;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(name = "editPasswordPoliciesViewBean", value = "#{editPasswordPoliciesViewBean}")
	private EditPasswordPoliciesViewBean editPasswordPoliciesViewBean;
	
	@ManagedProperty(name = "scheduledEmailDefConverter", value="#{scheduledEmailDefConverter}")
	private ScheduledEmailDefConverter scheduledEmailDefConverter;

	private boolean firstTime = true;
	
	public void initView() {
		log.debug("In editPasswordPoliciesControllerBean.initView()");
		log.debug("First Time? {}", firstTime);
		if (firstTime){
			Company company = sessionBean.getCompany();
			if (company == null){
				log.debug("Company is NULL!!");
			}
			editPasswordPoliciesViewBean.setPolicies(company.getPasswordPolicies());
			List<ScheduledEmailDef> emails = emailDefDao.findSysAndClientTemplatesByConstant("PASSWORD_EXPIRATION", company.getCompanyId());
			editPasswordPoliciesViewBean.setAvailableEmails(emails);
			setFirstTime(false);
		}
		resetPolicyFormInputValues();
		
		
	}

	
	public void resetPolicyFormInputValues(){
		editPasswordPoliciesViewBean.setPolicyDescription("");
		editPasswordPoliciesViewBean.setAllowedChanges(0);
		editPasswordPoliciesViewBean.setAllowedChangesPeriodMinutes(0);
		
		editPasswordPoliciesViewBean.setGenerateRandom(false);
		editPasswordPoliciesViewBean.setLockoutDurationMinutes(0);
		editPasswordPoliciesViewBean.setLockoutDurationMinutes(0);
		editPasswordPoliciesViewBean.setMaximumLength(0);
		editPasswordPoliciesViewBean.setMinimumLength(0);
		editPasswordPoliciesViewBean.setMaxPasswordsInHistory(0);
		editPasswordPoliciesViewBean.setNotificationDaysPriorToExpiration(0);
		editPasswordPoliciesViewBean.setReminderNotificationPeriodDays(0);
		editPasswordPoliciesViewBean.setRequireLowercase(false);
		editPasswordPoliciesViewBean.setRequireUppercase(false);
		editPasswordPoliciesViewBean.setRequireNumeric(false);
		editPasswordPoliciesViewBean.setRequireSpecial(false);
		editPasswordPoliciesViewBean.setValidationCustomRegex("");
		editPasswordPoliciesViewBean.setUnsuccessfulAttemptsPeriodMinutes(0);
		editPasswordPoliciesViewBean.setUnsuccessfulAttempts(0);
		editPasswordPoliciesViewBean.setTermInDays(0);
		editPasswordPoliciesViewBean.setTemplate(false);
		editPasswordPoliciesViewBean.setSelectedTypeId("");
		editPasswordPoliciesViewBean.setSelectedEmailTemplate(editPasswordPoliciesViewBean.getEmptyEmailTemplate());
	}
	
	public void handleTypeSelect(){
		Subject subject = SecurityUtils.getSubject();
		log.debug("Handle Type Select");
		List<CompanyPasswordPolicy> policies = editPasswordPoliciesViewBean.getPolicies();
		String selectedTypeId = editPasswordPoliciesViewBean.getSelectedTypeId();
		log.debug("Selected type id: {}", selectedTypeId);
		//PasswordPolicy policy = null;
		if (!(selectedTypeId.equalsIgnoreCase(""))){
			for (CompanyPasswordPolicy coPolicy: policies){
				if (coPolicy.getUserType() == (Integer.valueOf(selectedTypeId))){
					log.debug("found selected user type: {}", selectedTypeId);
					//policy = coPolicy.getPasswordPolicy();
					editPasswordPoliciesViewBean.setSelectedPolicy(coPolicy);
					setInputValues(coPolicy.getPasswordPolicy());
					
					if (coPolicy.getPasswordPolicy().isTemplate()){
						enableInputEdit(false);
						if (subject.isPermitted("createPasswordPolicies")){
							editPasswordPoliciesViewBean.setCustomizeButtonDisabled(false);
						}
						editPasswordPoliciesViewBean.setSaveButtonDisabled(true);
						editPasswordPoliciesViewBean.setResetButtonDisabled(true);
					}else{
						enableInputEdit(true);
						editPasswordPoliciesViewBean.setNewPolicy(coPolicy.getPasswordPolicy());
						editPasswordPoliciesViewBean.setCustomizeButtonDisabled(true);
						if (subject.isPermitted("editPasswordPolicies")){
							editPasswordPoliciesViewBean.setSaveButtonDisabled(false);
							editPasswordPoliciesViewBean.setResetButtonDisabled(false);
						}
						
					}
				}
			}
		}else{
			log.debug("No Type Selected");
			resetPolicyFormInputValues();
		}
	}
	
	public void handleCustomizePasswordPolicy(ActionEvent event){
		log.debug("Customizing Password Policy");
		String selectedUserTypeId = editPasswordPoliciesViewBean.getSelectedTypeId();
		List<CompanyPasswordPolicy> coPolicies = editPasswordPoliciesViewBean.getPolicies();
		PasswordPolicy selectedPolicy;
		for (CompanyPasswordPolicy coPolicy:coPolicies){
			if (coPolicy.getUserType() == Integer.valueOf(selectedUserTypeId)){
				log.debug("Found Policy for user type {}", selectedUserTypeId);
				if (coPolicy.getPasswordPolicy().isTemplate()){
					log.debug("Cloning Template policy to custom version ");
					selectedPolicy = new PasswordPolicy();
					PasswordPolicy template = coPolicy.getPasswordPolicy();
					selectedPolicy.setDescription(coPolicy.getCompany().getCoName() + " - " + template.getDescription());
					selectedPolicy.setTemplate(false);
					selectedPolicy.setConstant(String.valueOf(coPolicy.getCompany().getCompanyId()) + "_TYPE" + selectedUserTypeId);
					selectedPolicy.setTermInDays(template.getTermInDays());
					selectedPolicy.setUnsuccessfulAttempts(template.getUnsuccessfulAttempts());
					selectedPolicy.setUnsuccessfulAttemptsPeriodMinutes(template.getUnsuccessfulAttemptsPeriodMinutes());
					selectedPolicy.setAllowedChanges(template.getAllowedChanges());
					selectedPolicy.setAllowedChangesPeriodMinutes(template.getAllowedChangesPeriodMinutes());
					selectedPolicy.setDateCreated(new Timestamp(new Date().getTime()));
					selectedPolicy.setDateModified(new Timestamp(new Date().getTime()));
					selectedPolicy.setGenerateRandom(template.isGenerateRandom());
					selectedPolicy.setLockoutDurationMinutes(template.getLockoutDurationMinutes());
					selectedPolicy.setMaximumLength(template.getMaximumLength());
					selectedPolicy.setMaxPasswordsInHistory(template.getMaxPasswordsInHistory());
					selectedPolicy.setMinimumLength(template.getMinimumLength());
					selectedPolicy.setNotificationDaysPriorToExpiration(template.getNotificationDaysPriorToExpiration());
					selectedPolicy.setReminderNotificationPeriodDays(template.getReminderNotificationPeriodDays());
					selectedPolicy.setRequireLowercase(template.isRequireLowercase());
					selectedPolicy.setRequireNumeric(template.isRequireNumeric());
					selectedPolicy.setRequireUppercase(template.isRequireUppercase());
					selectedPolicy.setRequireSpecial(template.isRequireSpecial());
					selectedPolicy.setScheduledEmailDef(template.getScheduledEmailDef());
				}else{
					selectedPolicy = coPolicy.getPasswordPolicy();
				}
				editPasswordPoliciesViewBean.setNewPolicy(selectedPolicy);
				editPasswordPoliciesViewBean.setSaveButtonDisabled(false);
				editPasswordPoliciesViewBean.setCustomizeButtonDisabled(true);
				setInputValues(selectedPolicy);
				enableInputEdit(true);
				break;
			}
		}
	}
	
	public void handleSavePasswordPolicy(){
		log.debug("In Save Password Policy");
		PasswordPolicy policy = editPasswordPoliciesViewBean.getNewPolicy();
		policy = getInputValues(policy);
		if (policy.getPasswordPolicyId() > 0){
			log.debug("Update to existing policy");
			passwordPolicyDao.update(policy);
		}else{
			log.debug("create new custom policy");
			policy = passwordPolicyDao.create(policy);
		}
		String selectedUserTypeId = editPasswordPoliciesViewBean.getSelectedTypeId();
		List<CompanyPasswordPolicy> coPolicies = editPasswordPoliciesViewBean.getPolicies();
		
		for (CompanyPasswordPolicy coPolicy:coPolicies){
			if (coPolicy.getUserType() == Integer.valueOf(selectedUserTypeId)){
				coPolicy.setPasswordPolicy(policy);
				companyPasswordPolicyDao.update(coPolicy);
				int updatecount = pwdPolicyUserStatusDao.updateUsersToNewPolicy(policy, coPolicy.getUserType(), coPolicy.getCompany().getCompanyId());
				log.debug("updated {} users to new password policy");
				break;
			}
		}
		Company company = sessionBean.getCompany();
		editPasswordPoliciesViewBean.setPolicies(company.getPasswordPolicies());
	}
	
	public void resetCompanyDefaultPolicies(){
		String selectedTypeId = editPasswordPoliciesViewBean.getSelectedTypeId();
		PasswordPolicy adminPolicy = passwordPolicyDao.findTemplatePolicyByConstant("ADMIN_DEFAULT");
		PasswordPolicy pptPolicy = passwordPolicyDao.findTemplatePolicyByConstant("PARTICIPANT_DEFAULT");
		Company company = sessionBean.getCompany();
		List<CompanyPasswordPolicy> coPolicies = company.getPasswordPolicies();
		for (CompanyPasswordPolicy coPolicy: coPolicies){
			if (coPolicy.getUserType() == Integer.valueOf(selectedTypeId)){
				PasswordPolicy customPolicy = coPolicy.getPasswordPolicy();
				if (coPolicy.getUserType() == 1 || coPolicy.getUserType() == 2){
					coPolicy.setPasswordPolicy(pptPolicy);
					pwdPolicyUserStatusDao.updateUsersToNewPolicy(pptPolicy, coPolicy.getUserType(), company.getCompanyId());
				}else{
					coPolicy.setPasswordPolicy(adminPolicy);
					pwdPolicyUserStatusDao.updateUsersToNewPolicy(adminPolicy, coPolicy.getUserType(), company.getCompanyId());
				}
				companyPasswordPolicyDao.update(coPolicy);
				passwordPolicyDao.delete(customPolicy);
				setInputValues(coPolicy.getPasswordPolicy());
				enableInputEdit(false);
				editPasswordPoliciesViewBean.setSaveButtonDisabled(true);
				editPasswordPoliciesViewBean.setCustomizeButtonDisabled(false);
				editPasswordPoliciesViewBean.setResetButtonDisabled(true);
				break;
			}
		}
		editPasswordPoliciesViewBean.setPolicies(company.getPasswordPolicies());
	}
	
	public void enableInputEdit(boolean editable){
		editPasswordPoliciesViewBean.setAllowedChangesInputDisabled(!editable);
		editPasswordPoliciesViewBean.setAllowedChangesPeriodMinutesInputDisabled(!editable);
		editPasswordPoliciesViewBean.setDescriptionInputDisabled(!editable);
		editPasswordPoliciesViewBean.setTermInDaysInputDisabled(!editable);
		editPasswordPoliciesViewBean.setUnsuccessfulAttemptsInputDisabled(!editable);
		editPasswordPoliciesViewBean.setUnsuccessfulAttemptsPeriodMinutesInputDisabled(!editable);
		editPasswordPoliciesViewBean.setMaxPasswordsInHistoryInputDisabled(!editable);
		editPasswordPoliciesViewBean.setNotificationDaysPriorToExpirationInputDisabled(!editable);
		editPasswordPoliciesViewBean.setReminderNotificationPeriodDaysInputDisabled(!editable);
		editPasswordPoliciesViewBean.setLockoutDurationMinutesInputDisabled(!editable);
		editPasswordPoliciesViewBean.setValidationCustomRegexInputDisabled(!editable);
		editPasswordPoliciesViewBean.setGenerateRandomInputDisabled(!editable);
		editPasswordPoliciesViewBean.setMinimumLengthInputDisabled(!editable);
		editPasswordPoliciesViewBean.setMaximumLengthInputDisabled(!editable);
		editPasswordPoliciesViewBean.setRequireLowercaseInputDisabled(!editable);
		editPasswordPoliciesViewBean.setRequireUppercaseInputDisabled(!editable);
		editPasswordPoliciesViewBean.setRequireNumericInputDisabled(!editable);
		
		editPasswordPoliciesViewBean.setRequireSpecialInputDisabled(!editable);
		editPasswordPoliciesViewBean.setEmailTemplateInputDisabled(!editable);
	}
	
	public void setInputValues(PasswordPolicy policy){
		log.debug("Setting input values for Policy {}", policy.getDescription());
		editPasswordPoliciesViewBean.setSelectedEmailTemplate(policy.getScheduledEmailDef());
		editPasswordPoliciesViewBean.setPolicyDescription(policy.getDescription());
		editPasswordPoliciesViewBean.setTermInDays(policy.getTermInDays());
		editPasswordPoliciesViewBean.setAllowedChanges(policy.getAllowedChanges());
		editPasswordPoliciesViewBean.setAllowedChangesPeriodMinutes(policy.getAllowedChangesPeriodMinutes());
		editPasswordPoliciesViewBean.setUnsuccessfulAttempts(policy.getUnsuccessfulAttempts());
		editPasswordPoliciesViewBean.setUnsuccessfulAttemptsPeriodMinutes(policy.getUnsuccessfulAttemptsPeriodMinutes());
		editPasswordPoliciesViewBean.setMaxPasswordsInHistory(policy.getMaxPasswordsInHistory());
		editPasswordPoliciesViewBean.setNotificationDaysPriorToExpiration(policy.getNotificationDaysPriorToExpiration());
		editPasswordPoliciesViewBean.setReminderNotificationPeriodDays(policy.getReminderNotificationPeriodDays());
		editPasswordPoliciesViewBean.setLockoutDurationMinutes(policy.getLockoutDurationMinutes());
		editPasswordPoliciesViewBean.setValidationCustomRegex(policy.getValidationCustomRegex());
		editPasswordPoliciesViewBean.setGenerateRandom(policy.isGenerateRandom());
		editPasswordPoliciesViewBean.setTemplate(policy.isTemplate());
		editPasswordPoliciesViewBean.setMinimumLength(policy.getMinimumLength());
		editPasswordPoliciesViewBean.setMaximumLength(policy.getMaximumLength());
		editPasswordPoliciesViewBean.setRequireLowercase(policy.isRequireLowercase());
		editPasswordPoliciesViewBean.setRequireUppercase(policy.isRequireUppercase());
		editPasswordPoliciesViewBean.setRequireNumeric(policy.isRequireNumeric());
		editPasswordPoliciesViewBean.setRequireSpecial(policy.isRequireSpecial());
		List<ScheduledEmailDef> defs = editPasswordPoliciesViewBean.getAvailableEmails();
		if (!(policy.getScheduledEmailDef() == null)){
			for (ScheduledEmailDef def:defs){
				if (def.getTitle().equalsIgnoreCase(policy.getScheduledEmailDef().getTitle())){
					log.debug("Setting Email Template {}", def.getTitle());
					editPasswordPoliciesViewBean.setSelectedEmailTemplate(def);
					break;
				}
			}
		}else{
			log.debug("Setting Empty email template");
			editPasswordPoliciesViewBean.setSelectedEmailTemplate(editPasswordPoliciesViewBean.getEmptyEmailTemplate());
		}
		
	}
	
	public PasswordPolicy getInputValues(PasswordPolicy policy){
		
		policy.setDescription(editPasswordPoliciesViewBean.getPolicyDescription());
		policy.setTermInDays(editPasswordPoliciesViewBean.getTermInDays());
		policy.setAllowedChanges(editPasswordPoliciesViewBean.getAllowedChanges());
		policy.setAllowedChangesPeriodMinutes(editPasswordPoliciesViewBean.getAllowedChangesPeriodMinutes());
		policy.setUnsuccessfulAttempts(editPasswordPoliciesViewBean.getUnsuccessfulAttempts());
		policy.setUnsuccessfulAttemptsPeriodMinutes(editPasswordPoliciesViewBean.getUnsuccessfulAttemptsPeriodMinutes());
		policy.setMaxPasswordsInHistory(editPasswordPoliciesViewBean.getMaxPasswordsInHistory());
		policy.setNotificationDaysPriorToExpiration(editPasswordPoliciesViewBean.getNotificationDaysPriorToExpiration());
		policy.setReminderNotificationPeriodDays(editPasswordPoliciesViewBean.getReminderNotificationPeriodDays());
		policy.setLockoutDurationMinutes(editPasswordPoliciesViewBean.getLockoutDurationMinutes());
		policy.setValidationCustomRegex(editPasswordPoliciesViewBean.getValidationCustomRegex());
		policy.setGenerateRandom(editPasswordPoliciesViewBean.isGenerateRandom());
		policy.setTemplate(editPasswordPoliciesViewBean.isTemplate());
		policy.setMinimumLength(editPasswordPoliciesViewBean.getMinimumLength());
		policy.setMaximumLength(editPasswordPoliciesViewBean.getMaximumLength());
		policy.setRequireLowercase(editPasswordPoliciesViewBean.isRequireLowercase());
		policy.setRequireUppercase(editPasswordPoliciesViewBean.isRequireUppercase());
		policy.setRequireNumeric(editPasswordPoliciesViewBean.isRequireNumeric());
		policy.setRequireSpecial(editPasswordPoliciesViewBean.isRequireSpecial());
		ScheduledEmailDef def = editPasswordPoliciesViewBean.getSelectedEmailTemplate();
		if (def.getId() > 0){
			policy.setScheduledEmailDef(def);
		}
		return policy;
	}


	public SessionBean getSessionBean() {
		return sessionBean;
	}


	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}


	public EditPasswordPoliciesViewBean getEditPasswordPoliciesViewBean() {
		return editPasswordPoliciesViewBean;
	}


	public void setEditPasswordPoliciesViewBean(
			EditPasswordPoliciesViewBean editPasswordPoliciesViewBean) {
		this.editPasswordPoliciesViewBean = editPasswordPoliciesViewBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public ScheduledEmailDefConverter getScheduledEmailDefConverter() {
		return scheduledEmailDefConverter;
	}

	public void setScheduledEmailDefConverter(ScheduledEmailDefConverter scheduledEmailDefConverter) {
		this.scheduledEmailDefConverter = scheduledEmailDefConverter;
	}
}
