package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailRuleCriteriaObj;
import com.pdinh.presentation.domain.EmailRuleCriteriaTypeObj;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean
@ViewScoped
public class EmailRuleSetupViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private Boolean active;
	private Integer templateId;
	private List<EmailTemplateObj> templates;
	private String languageCode;
	private List<ListItemObj> languages;
	private String defaultLanguage;
	private Boolean disableRulesTable = true;
	private Boolean disableAddCriteria;
	private Boolean disableParticipantsTab;

	private List<Object> scheduledItems;
	private List<EmailRuleCriteriaObj> criteriaItems;
	private List<EmailRuleCriteriaTypeObj> criteriaMenuItems;

	private Boolean allOrSelectedSelection;
	private Boolean excludedSelection;
	private Boolean disableExcludedSelection;

	private List<ParticipantObj> participants;
	private List<ParticipantObj> selectedParticipants;

	private Boolean disableRemoveParticipants = true;
	private boolean newEmailRule;
	private Integer currectRuleId;

	private boolean noCriticalError = true;
	private boolean disableEditTemplate = true;
	private boolean disableCloneTemplate = true;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<EmailTemplateObj> getTemplates() {
		return templates;
	}

	public void setTemplates(List<EmailTemplateObj> templates) {
		this.templates = templates;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public Boolean getDisableRulesTable() {
		return disableRulesTable;
	}

	public void setDisableRulesTable(Boolean disableRulesTable) {
		this.disableRulesTable = disableRulesTable;
	}

	public List<Object> getScheduledItems() {
		return scheduledItems;
	}

	public void setScheduledItems(List<Object> scheduledItems) {
		this.scheduledItems = scheduledItems;
	}

	public List<EmailRuleCriteriaObj> getCriteriaItems() {
		return criteriaItems;
	}

	public void setCriteriaItems(List<EmailRuleCriteriaObj> criteriaItems) {
		this.criteriaItems = criteriaItems;
	}

	public Boolean getDisableAddCriteria() {
		return disableAddCriteria;
	}

	public void setDisableAddCriteria(Boolean disableAddCriteria) {
		this.disableAddCriteria = disableAddCriteria;
	}

	public Boolean getDisableParticipantsTab() {
		return disableParticipantsTab;
	}

	public void setDisableParticipantsTab(Boolean disableParticipantsTab) {
		this.disableParticipantsTab = disableParticipantsTab;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getSelectedParticipants() {
		return selectedParticipants;
	}

	public void setSelectedParticipants(List<ParticipantObj> selectedParticipants) {
		this.selectedParticipants = selectedParticipants;
	}

	public Boolean getDisableRemoveParticipants() {
		return disableRemoveParticipants;
	}

	public void setDisableRemoveParticipants(Boolean disableRemoveParticipants) {
		this.disableRemoveParticipants = disableRemoveParticipants;
	}

	public Boolean getAllOrSelectedSelection() {
		return allOrSelectedSelection;
	}

	public void setAllOrSelectedSelection(Boolean allOrSelectedSelection) {
		this.allOrSelectedSelection = allOrSelectedSelection;
	}

	public Boolean getExcludedSelection() {
		return excludedSelection;
	}

	public void setExcludedSelection(Boolean excludedSelection) {
		this.excludedSelection = excludedSelection;
	}

	public Boolean getDisableExcludedSelection() {
		return disableExcludedSelection;
	}

	public void setDisableExcludedSelection(Boolean disableExcludedSelection) {
		this.disableExcludedSelection = disableExcludedSelection;
	}

	public boolean isNewEmailRule() {
		return newEmailRule;
	}

	public void setNewEmailRule(boolean newEmailRule) {
		this.newEmailRule = newEmailRule;
	}

	public Integer getCurrectRuleId() {
		return currectRuleId;
	}

	public void setCurrectRuleId(Integer currectRuleId) {
		this.currectRuleId = currectRuleId;
	}

	public boolean isNoCriticalError() {
		return noCriticalError;
	}

	public void setNoCriticalError(boolean noCriticalError) {
		this.noCriticalError = noCriticalError;
	}

	public List<EmailRuleCriteriaTypeObj> getCriteriaMenuItems() {
		return criteriaMenuItems;
	}

	public void setCriteriaMenuItems(List<EmailRuleCriteriaTypeObj> criteriaMenuItems) {
		this.criteriaMenuItems = criteriaMenuItems;
	}

	public boolean isDisableEditTemplate() {
		return disableEditTemplate;
	}

	public void setDisableEditTemplate(boolean disableEditTemplate) {
		this.disableEditTemplate = disableEditTemplate;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public boolean isDisableCloneTemplate() {
		return disableCloneTemplate;
	}

	public void setDisableCloneTemplate(boolean disableCloneTemplate) {
		this.disableCloneTemplate = disableCloneTemplate;
	}

	public List<ListItemObj> getLanguages() {
		return languages;
	}

	public void setLanguages(List<ListItemObj> languages) {
		this.languages = languages;
	}
}
