package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.pdinh.presentation.domain.SuperUserObj;

@ManagedBean(name="superUserListViewBean")
@ViewScoped
public class SuperUserListViewBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
	private List<SuperUserObj> superUsers;
	private SuperUserObj selectedSuperUser;
	private String token;

	public void setSuperUsers(List<SuperUserObj> superUsers) {
		this.superUsers = superUsers;
	}
	
	public List<SuperUserObj> getSuperUsers() {
		return superUsers;
	}
	
	public void setSelectedSuperUser(SuperUserObj selectedSuperUser) {
		this.selectedSuperUser = selectedSuperUser;
	}
	
	public SuperUserObj getSelectedSuperUser() {
		return selectedSuperUser;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}

