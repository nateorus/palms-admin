package com.pdinh.presentation.converter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.view.ParticipantInfoViewBean;

@ManagedBean(name = "officeLocationDtoConverter")
@ViewScoped
public class OfficeLocationDtoConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;

	//@ManagedProperty(name = "participantInfoViewBean", value = "#{participantInfoViewBean}")
	//private ParticipantInfoViewBean participantInfoViewBean;
	
	//set me
	private List<OfficeLocationObj> officeList = new ArrayList<OfficeLocationObj>();
	
	@EJB
	private ProjectSetupSingleton projectSetupSingleton;

	@Override
	public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String value) {
		OfficeLocationObj foundOfficeLocationDto = null;
		for (OfficeLocationObj officeLocationObj : officeList) {
			if (value.equals(officeLocationObj.getOfficeLocationId().toString())) {
				foundOfficeLocationDto = officeLocationObj;
				break;
			}
		}
		return foundOfficeLocationDto;
	}

	@Override
	public String getAsString(FacesContext ctx, UIComponent uiComponent, Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			OfficeLocationObj officeLocationDto = (OfficeLocationObj) value;
			return officeLocationDto.getOfficeLocationId().toString();
		}
	}

	public List<OfficeLocationObj> getOfficeList() {
		return officeList;
	}

	public void setOfficeList(List<OfficeLocationObj> officeList) {
		this.officeList = officeList;
	}

}
