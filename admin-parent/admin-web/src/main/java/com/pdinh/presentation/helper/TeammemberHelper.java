package com.pdinh.presentation.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.AuthorizedEntityDao;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.presentation.managedbean.SessionBean;

@ManagedBean
@ViewScoped
public class TeammemberHelper implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(TeammemberHelper.class);

	@Resource(mappedName = "application/config_properties")
	private Properties properties;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	@EJB
	private PalmsRealmDao palmsRealmDao;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private ExternalSubjectServiceLocal subjectService;

	@EJB
	private PermissionServiceLocal permissionService;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@EJB
	private PalmsSubjectDao palmsSubjectDao;

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private AuthorizedEntityDao authorizedEntityDao;

	@EJB
	private GlobalRolesBean rolesBean;

	private PalmsRealm realm;
	private List<PalmsRealm> realms;
	private String currentUserName;
	private String currentEntityTypeCode;
	private Integer currentEntityId;
	private EntityType currentEntityType;
	private EntityRule currentEntityRule;

	public void init(String entityTypeCode, Integer entityId, String realmPropertyName, String entityRuleName) {
		log.debug(">>>>>>>>>TeammemberHelper>>>>>>> realmPropertyValue = {}", properties.getProperty(realmPropertyName));
		log.debug(">>>>>>>>>TeammemberHelper>>>>>>> entityTypeCode = {}", entityTypeCode);
		log.debug(">>>>>>>>>TeammemberHelper>>>>>>> entityId = {}", entityId);

		setRealms(findRealmsByName(realmPropertyName));
		setRealm(this.getRealms().get(0));
		setCurrentUserName(SecurityUtils.getSubject().getPrincipal().toString());
		setCurrentEntityType(rolesBean.getEntityType(entityTypeCode));
		setCurrentEntityTypeCode(entityTypeCode);
		setCurrentEntityId(entityId);
		if (!(entityRuleName == null || entityRuleName.isEmpty())) {
			setCurrentEntityRule(rolesBean.getEntityRule(entityRuleName));
		}
	}

	public PalmsRealm findRealmByName(String realmName) {
		return findRealmsByName(realmName).get(0);
		// return
		// palmsRealmDao.findRealmByName(properties.getProperty(realmName));
	}

	public List<PalmsRealm> findRealmsByName(String realmNames) {
		List<String> names = Arrays.asList(properties.getProperty(realmNames).split(","));
		return palmsRealmDao.findRealmsInListOfNames(names);
	}
	
	public List<AuthorizedEntity> getEntities(List<Integer> entityIds){
		
		List<AuthorizedEntity> entities = new ArrayList<AuthorizedEntity>();
		
		entities.addAll(authorizationService.getEntities(entityIds, this.getCurrentEntityTypeCode()));
		
		return entities;
	}

	public List<Role> getRoles(String entityTypeCode) {
		return permissionService.getAvailableRoles(realm.getRoleTemplate(), entityTypeCode);
	}

	public List<ExternalSubjectDataObj> getAllSubjects(String filter) {
		List<ExternalSubjectDataObj> subjectList = new ArrayList<ExternalSubjectDataObj>();
		for (PalmsRealm realm : getRealms()) {
			subjectList.addAll(subjectService.getExternalSubjects(realm, filter, false));
		}
		return subjectList;
		/*
		List<ExternalSubjectDataObj> subjectList = subjectService.getExternalSubjects(getRealm(), filter, false);
		List<ExternalSubjectDataObj> returnList = new ArrayList<ExternalSubjectDataObj>();
		for (ExternalSubjectDataObj obj:subjectList){
			PalmsSubject palmsSubject = palmsSubjectDao.findSubjectByPrincipalAndRealm(obj.getUsername(), getRealm().getRealm_id());
			RoleContext context = entityService.getDefaultContext(palmsSubject);
			AuthorizedEntity entity = entityService.getAuthorizedEntity(context, currentEntityType.getEntity_type_code(), currentEntityId);
			ExternalSubjectDataObj newObj = obj;
			newObj.setCurrentRuleName(entity.getEntityRule().getEntity_rule_name());
			returnList.add(newObj);
		}
		return returnList;
		*/
	}

	public List<ExternalSubjectDataObj> getSubjectsWithRole(Role role) {
		List<String> realmNames = this.getPalmsRealmNames();
		return subjectService.getSubjectsWithRole(realmNames, role);

	}

	public List<ExternalSubjectDataObj> getSubjectsWithEntity(Integer entityId, String entityTypeCode) {
		List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();
		for (PalmsRealm realm : getRealms()) {
			subjects.addAll(subjectService.getSubjectsWithEntity(realm, entityId, entityTypeCode));
		}
		return subjects;
	}
	
	public List<ExternalSubjectDataObj> getSubjectsWithEntities(List<Integer> entityIds, String entityTypeCode){
		List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();
		for (PalmsRealm realm : getRealms()){
			subjects.addAll(subjectService.getSubjectsWithEntities(realm, entityIds, entityTypeCode));
		}
		return subjects;
	}

	public Role findRoleById(int roleId) {
		return rolesBean.getRole(roleId);
	}

	public Role findRoleByName(String name) {
		return rolesBean.getRole(name);
	}

	public void saveTeammembers(List<ExternalSubjectDataObj> teammembers, Integer companyId, Integer projectId) {
		Subject admin = SecurityUtils.getSubject();
		List<ExternalSubjectDataObj> distinctSubjects = getDistinctSubjects(teammembers);

		for (ExternalSubjectDataObj subject : distinctSubjects) {
			List<Role> roles = new ArrayList<Role>();
			RoleContext dContext = subjectService.getDefaultRoleContext(subject.getUsername(),
					getRealm().getRealm_id(), subject.getEntryDN(), subject.getExternal_id());
			for (ExternalSubjectDataObj teammember : teammembers) {
				if (subject.getUsername().equalsIgnoreCase(teammember.getUsername())) {
					if (teammember.getCurrentRole() != null && teammember.getCurrentRuleName() == null) {
						// roles.add(teammember.getCurrentRole());
						log.debug(">>>>>>>>> Add Entity Role: {} for {}", teammember.getCurrentRole().getLabel(),
								teammember.getFirstLastEmail());
						if (this.getCurrentEntityTypeCode().equalsIgnoreCase(EntityType.COMPANY_TYPE)) {

							log.debug("Calling Authorization Service for Role: {}, Subject: {}, Entity {}",
									new Object[] { teammember.getCurrentRole().getName(), subject.getUsername(),
											sessionBean.getCompany().getCoName() });
							authorizationService.grantCompanyAccess(subject.getUsername(), getRealm().getRealm_name(),
									sessionBean.getCompany(), admin.getPrincipal().toString(), teammember
											.getCurrentRole().getName(), getCurrentEntityRule().getEntity_rule_name());

						} else if (this.getCurrentEntityTypeCode().equalsIgnoreCase(EntityType.PROJECT_TYPE)) {

							authorizationService.grantProjectAccess(dContext.getSubject(), sessionBean.getProject(),
									admin.getPrincipal().toString(), teammember.getCurrentRole().getName(),
									getCurrentEntityRule().getEntity_rule_name());

						}

					}
				}
			}

			/*
			if (companyId != null) {
				dContext = grandEntityAccess(EntityType.COMPANY_TYPE, companyId, null, getCurrentEntityRule(),
						dContext, roles);
			}

			if (projectId != null) {
				dContext = grandEntityAccess(EntityType.PROJECT_TYPE, projectId, companyId, getCurrentEntityRule(),
						dContext, roles);
			}
			*/
		}
		log.debug(">>>>>>>>> saveTeammembers: entityId = {}; entityType = {}", getCurrentEntityId(),
				getCurrentEntityType());
	}

	public void removeTeammembers(List<ExternalSubjectDataObj> teammembers) {
		Subject subject = SecurityUtils.getSubject();
		Integer entityId = getCurrentEntityId();
		EntityType entityType = getCurrentEntityType();

		for (ExternalSubjectDataObj teammember : teammembers) {
			PalmsSubject palmsSubject = palmsSubjectDao.findSubjectByPrincipalAndRealm(teammember.getUsername(),
					realm.getRealm_id());

			if (palmsSubject == null) {
				break;
			}

			RoleContext context = entityService.getDefaultContext(palmsSubject);
			AuthorizedEntity entity = entityService.getAuthorizedEntity(context, entityType.getEntity_type_code(),
					entityId);
			if (teammember.getCurrentRole() != null) {
				entity = entityService.revokeEntityRole(entity, subject.getPrincipal().toString(),
						teammember.getCurrentRole());
			} else {
				log.error("removeTeammembers > teammember current role is null");
			}

			if (entity != null) {
				if (entity.getRoles() == null || entity.getRoles().size() == 0) {
					log.debug(">>>>>>>>> removing entity {}", entity.getSubject_id());
					entityService.revokeEntityAccess(entity.getRoleContext(), entity.getEntity_id(), entity
							.getEntityType().getEntity_type_code(), subject.getPrincipal().toString());
				}
			} else {
				log.debug("removeTeammembers > entity is null");
			}

		}
	}

	public void assignConsultants(List<ExternalSubjectDataObj> consultants, Integer companyId, Integer projectId,
			Integer projectUserId) {
		Subject admin = SecurityUtils.getSubject();

		for (ExternalSubjectDataObj consultant : consultants) {
			RoleContext dContext = subjectService.getDefaultRoleContext(consultant.getUsername(), getRealm()
					.getRealm_id(), consultant.getEntryDN(), consultant.getExternal_id());

			ProjectUser pu = projectUserDao.findById(projectUserId);
			authorizationService.grantProjectUserAccess(dContext.getSubject(), pu.getProject(), pu.getUser(), admin
					.getPrincipal().toString(), "Consultant", EntityRule.ALLOW_CHILDREN);
		}
	}

	public void unassignConsultants(List<ExternalSubjectDataObj> consultants, Integer companyId, Integer projectId,
			Integer projectUserId) {

		Role role = findRoleByName("Consultant");
		AuthorizedEntity entity = new AuthorizedEntity();

		for (ExternalSubjectDataObj consultant : consultants) {

			log.debug(">>>>>>>>>>>>>>>> unassign consultant username = {}", consultant.getUsername());

			PalmsSubject palmsSubject = palmsSubjectDao.findSubjectByPrincipalAndRealm(consultant.getUsername(),
					getRealm().getRealm_id());

			if (palmsSubject == null) {
				break;
			}

			RoleContext context = entityService.getDefaultContext(palmsSubject);

			// Participant (User)
			entity = entityService.revokeEntityRole(
					entityService.getAuthorizedEntity(context, EntityType.PROJECT_USER_TYPE, projectUserId),
					getCurrentUserName(), role);

			if (entity != null) {
				if (entity.getRoles() == null || entity.getRoles().size() == 0) {
					context = entityService.revokeEntityAccess(context, projectUserId, EntityType.PROJECT_USER_TYPE,
							getCurrentUserName());
					log.debug(">>>>>>>>>>>>>>>> unassign consultant no roles found revoke entity access for {}, {}",
							projectUserId, EntityType.PROJECT_USER_TYPE);
				}
			}

		}
	}

	public List<ExternalSubjectDataObj> fetchTeammembers(String entityTypeCode, Integer entityId, String realmName) {
		EntityType entityType = rolesBean.getEntityType(entityTypeCode);
		List<PalmsRealm> realms = findRealmsByName(realmName);
		List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();
		log.debug("In fetchTeammembers with {} Realms. {}", realms.size(), realmName);
		for (PalmsRealm realm : realms) {
			log.debug("New Realm:  {}", realm.getRealm_name());
			if (entityId != null) {
				List<ExternalSubjectDataObj> subjectsWithEntity = subjectService.getSubjectsWithEntity(realm, entityId,
						entityType.getEntity_type_code());
				log.debug("Finished getting subjectsWithEntity.  Got {} subjects", subjectsWithEntity.size());
				List<AuthorizedEntity> authorizedEntities = authorizedEntityDao.findAllByEntityIdTypeAndRealmBatchRoles(entityId,
						entityType.getEntity_type_id(), realm.getRealm_id());

				log.debug(">>>>>>>>> authorizedEntities: {}", authorizedEntities.size());
				for (AuthorizedEntity entity : authorizedEntities) {

					for (ExternalSubjectDataObj subjectWithEntity : subjectsWithEntity) {
						PalmsSubject palmsSubject = palmsSubjectDao.findById(entity.getSubject_id());

						if (palmsSubject != null) {
							if (palmsSubject.getPrincipal().equalsIgnoreCase(subjectWithEntity.getUsername())) {
								if (entity.getRoles() != null && entity.getRoles().size() > 0) {
									for (Role entityRole : entity.getRoles()) {
										ExternalSubjectDataObj subjectWithEntityCopy = new ExternalSubjectDataObj(
												subjectWithEntity);
										subjectWithEntityCopy.setCurrentRole(entityRole);
										subjectWithEntityCopy.setCurrentRuleName(entity.getEntityRule()
												.getEntity_rule_name());
										subjects.add(subjectWithEntityCopy);
										log.debug(">>>>>>>>> role label to set: {}", entityRole.getLabel());
										log.debug("Entity Rule:  {}", subjectWithEntity.getCurrentRuleName());
									}
								}else{
									//Roles are inherited
									ExternalSubjectDataObj subjectWithEntityCopy = new ExternalSubjectDataObj(
											subjectWithEntity);
									Role entityRole = new Role();
									entityRole.setLabel("[Inherited Roles]");
									entityRole.setRole_id(0);
									subjectWithEntityCopy.setCurrentRole(entityRole);
									subjectWithEntityCopy.setCurrentRuleName(entity.getEntityRule()
											.getEntity_rule_name());
									subjects.add(subjectWithEntityCopy);
									log.debug(">>>>>>>>> role label to set: {}", entityRole.getLabel());
									log.debug("Entity Rule:  {}", subjectWithEntity.getCurrentRuleName());
								}
							}
						}
					}
				}

			}
		}

		return subjects;
	}

	public void finalizeTeammemberList(List<ExternalSubjectDataObj> targetList, List<ExternalSubjectDataObj> sourceList) {
		for (ExternalSubjectDataObj sourceMember : sourceList) {
			if (sourceMember.getCurrentRole() != null) {
				if (!isTeammemberExists(targetList, sourceMember.getUsername(), sourceMember.getCurrentRole()
						.getLabel())) {
					targetList.add(new ExternalSubjectDataObj(sourceMember));
				}
			}
		}
	}

	private boolean isTeammemberExists(List<ExternalSubjectDataObj> listToSearch, String username, String roleLabel) {
		for (ExternalSubjectDataObj item : listToSearch) {
			if (item.getUsername().equalsIgnoreCase(username)) {
				if (item.getCurrentRole() != null) {
					if (item.getCurrentRole().getLabel().equalsIgnoreCase(roleLabel)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private ExternalSubjectDataObj findTeammember(List<ExternalSubjectDataObj> listToSearch, String username) {
		for (ExternalSubjectDataObj item : listToSearch) {
			if (item.getUsername().equalsIgnoreCase(username)) {
				return item;
			}
		}
		return null;
	}

	private List<ExternalSubjectDataObj> getDistinctSubjects(List<ExternalSubjectDataObj> subjects) {
		if (subjects == null || subjects.isEmpty()) {
			return new ArrayList<ExternalSubjectDataObj>();
		} else {
			return new ArrayList<ExternalSubjectDataObj>(new HashSet<ExternalSubjectDataObj>(subjects));
		}
	}

	public void validateRemoveList(List<ExternalSubjectDataObj> saveList, List<ExternalSubjectDataObj> removeList) {
		if (!(saveList == null)) {
			for (ExternalSubjectDataObj saveSubject : saveList) {
				ExternalSubjectDataObj candidate = findTeammember(removeList, saveSubject.getUsername());
				if (candidate != null) {
					removeList.remove(candidate);
				}
			}
		}
	}

	public List<String> getPalmsRealmNames() {
		return Arrays.asList(properties.getProperty("palmsRealmNames").split(","));
	}
	
	public List<ExternalSubjectDataObj> getAllUsersWithPermission(String permissionName) {
		
		List<String> realmNames = Arrays.asList(properties.getProperty("palmsRealmNames").split(","));
		String realmName = properties.getProperty("primaryRealmName");
		PalmsRealm realm = palmsRealmDao.findRealmByName(realmName);
		List<Role> schedulingRoles = permissionService.getRolesWithPermission(permissionName, realm);
		List<ExternalSubjectDataObj> assignedPmPcs = new ArrayList<ExternalSubjectDataObj>();
		for(Role role : schedulingRoles){
			log.debug("Getting schedulers from role: {}", role.getLabel());
			assignedPmPcs.addAll(subjectService.getSubjectsWithRole(realmNames, role));
			log.debug("Got {} schedulers", assignedPmPcs.size());
		}
		
		return subjectService.dedupeSubjects(assignedPmPcs);
	}
	
	public List<ExternalSubjectDataObj> getAssignedPmPcs(){
		return this.getAllUsersWithPermission("projectManagerCoordinator");
	}
	
	public List<ExternalSubjectDataObj> getSchedulers(){
		return this.getAllUsersWithPermission("scheduleAssessments");
	}
/*
	public List<ExternalSubjectDataObj> getAssignedPmPcs(int companyId, int projectId) {
		List<String> realmNames = this.getPalmsRealmNames();
		List<String> clientLevelRoleNames = Arrays.asList(new String[] { "OperationsAdmin" });
		List<String> projectLevelRoleNames = Arrays.asList(new String[] { "Operations" });
		List<ExternalSubjectDataObj> assignedPmPcs = new ArrayList<ExternalSubjectDataObj>();
		assignedPmPcs.addAll(subjectService.findSubjectsByRoles(companyId, EntityType.COMPANY_TYPE, realmNames,
				clientLevelRoleNames, false));
		if (projectId > 0) {
			assignedPmPcs.addAll(subjectService.findSubjectsByRoles(projectId, EntityType.PROJECT_TYPE, realmNames,
					projectLevelRoleNames, false));
		}
		return subjectService.dedupeSubjects(assignedPmPcs);
	}
	
	

	public List<ExternalSubjectDataObj> getSchedulers(int companyId, int projectId) {
		List<String> realmNames = this.getPalmsRealmNames();
		List<String> clientLevelRoleNames = Arrays.asList(new String[] { "OperationsAdmin" });
		List<String> projectLevelRoleNames = Arrays.asList(new String[] { "Operations", "ResourceCoordinator" });
		List<ExternalSubjectDataObj> schedulers = new ArrayList<ExternalSubjectDataObj>();
		schedulers.addAll(subjectService.findSubjectsByRoles(companyId, EntityType.COMPANY_TYPE, realmNames,
				clientLevelRoleNames, false));
		if (projectId > 0) {
			schedulers.addAll(subjectService.findSubjectsByRoles(projectId, EntityType.PROJECT_TYPE, realmNames,
					projectLevelRoleNames, false));
		}
		return subjectService.dedupeSubjects(schedulers);
	}
*/
	public PalmsSubject findSubjectByPrincipalAndRealmName(String principal) {
		for (String palmsRealmName : this.getPalmsRealmNames()) {
			PalmsSubject ps = palmsSubjectDao.findSubjectByPrincipalAndRealmName(principal, palmsRealmName);
			if (ps != null) {
				return ps;
			}
		}
		return null;
	}

	public List<PalmsSubject> findSubjectsById(List<Integer> subjectIds) {
		return palmsSubjectDao.findSubjectsById(subjectIds);
	}

	public PalmsRealm getRealm() {
		return realm;
	}

	public void setRealm(PalmsRealm realm) {
		this.realm = realm;
	}

	public String getCurrentUserName() {
		return currentUserName;
	}

	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}

	public String getCurrentEntityTypeCode() {
		return currentEntityTypeCode;
	}

	public void setCurrentEntityTypeCode(String currentEntityTypeCode) {
		this.currentEntityTypeCode = currentEntityTypeCode;
	}

	public Integer getCurrentEntityId() {
		return currentEntityId;
	}

	public void setCurrentEntityId(Integer currentEntityId) {
		this.currentEntityId = currentEntityId;
	}

	public EntityType getCurrentEntityType() {
		return currentEntityType;
	}

	public void setCurrentEntityType(EntityType currentEntityType) {
		this.currentEntityType = currentEntityType;
	}

	public EntityRule getCurrentEntityRule() {
		return currentEntityRule;
	}

	public void setCurrentEntityRule(EntityRule currentEntityRule) {
		this.currentEntityRule = currentEntityRule;
	}

	public List<PalmsRealm> getRealms() {
		return realms;
	}

	public void setRealms(List<PalmsRealm> realms) {
		this.realms = realms;
	}

}
