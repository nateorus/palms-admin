package com.pdinh.presentation.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.util.ComponentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.EnrollmentServiceLocalImpl;
import com.pdinh.data.jaxb.lrm.AddUserResponse;
import com.pdinh.data.jaxb.lrm.AddUserServiceLocal;
import com.pdinh.data.ms.dao.ConsentUserDao;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.RosterDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.ConsentText;
import com.pdinh.persistence.ms.entity.ConsentUser;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.Roster;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ProjectParticipantsViewBean;

@ManagedBean(name = "projectParticipantsControllerBean")
@ViewScoped
public class ProjectParticipantsControllerBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ProjectParticipantsControllerBean.class);

	@EJB
	private EnrollmentServiceLocalImpl enrollmentService;

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private UserDao userDao;

	@EJB
	private RosterDao rosterDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private ConsentUserDao consentUserDao;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@EJB
	private AddUserServiceLocal addUserService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "projectParticipantsViewBean", value = "#{projectParticipantsViewBean}")
	private ProjectParticipantsViewBean projectParticipantsViewBean;

	private boolean isPDAProject = false;

	// public void handleInitForSelection(ActionEvent event) {
	// initForSelection();
	// }

	public void handleRowSelect(SelectEvent event) {
		updateAddSensitivity();
	}

	public void handleRowUnselect(UnselectEvent event) {
		updateAddSensitivity();
	}

	public void updateAddSensitivity() {

		int selLen = 0;
		List<ParticipantObj> ppl = getProjectParticipantsViewBean().getSelectedUsers();
		if (ppl != null)
			selLen = ppl.size();
		log.debug("  selection length = {} ", selLen);

		if (selLen > 0) {
			getProjectParticipantsViewBean().setAddToSelectionDisabled(false);
		} else {
			getProjectParticipantsViewBean().setAddToSelectionDisabled(true);
		}
	}

	public void initForSelection(Boolean inProject) {
		log.debug("In ProjectParticipantsControllerBean initForSelection()");

		// Populate client users into view list
		List<ParticipantObj> pp = new ArrayList<ParticipantObj>();
		List<User> availableUsers = new ArrayList<User>();

		// List<User> unusedClientUsers =
		// userDao.findUsersNotInProjectByProjectId(sessionBean.getCompany().getCompanyId(),sessionBean.getProject().getProjectId());
		// User u;
		// Iterator<User> iterator = unusedClientUsers.iterator();
		// while (iterator.hasNext()) {
		// u = iterator.next();
		// //System.out.println("added " + u.getUsersId() + " " +
		// u.getFirstname() + " " + u.getLastname() + " " + u.getEmail());
		// pp.add(new
		// ParticipantObj(u.getUsersId(),u.getFirstname(),u.getLastname(),u.getEmail(),u.getUsername()));
		// }

		if (inProject) {
			availableUsers = getUsersInProject();
		} else {
			availableUsers = getUsersNotInProject();
		}

		for (User u : availableUsers) {
			pp.add(new ParticipantObj(u.getUsersId(), u.getFirstname(), u.getLastname(), u.getEmail(), u.getUsername()));
		}

		projectParticipantsViewBean.setParticipants(pp);

		int availsize = projectParticipantsViewBean.getParticipants().size();
		log.debug(" avail count = {}", availsize);

		// initialize view bean selections list
		List<ParticipantObj> selections = new ArrayList<ParticipantObj>();
		projectParticipantsViewBean.setSelections(selections);

		projectParticipantsViewBean.setAddToSelectionDisabled(true);
		projectParticipantsViewBean.setAddExistingParticipantsDisabled(true);

		// reset pagination when dialog is displayed
		resetAvailableParticipantsTable();
	}

	private List<User> getUsersNotInProject() {
		List<User> users = new ArrayList<User>();
		Subject subject = SecurityUtils.getSubject();
		if (subject.isPermitted("viewAllUsers")
				|| subject.isPermitted(EntityType.COMPANY_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
						+ sessionBean.getCompany().getCompanyId())
				|| subject.isPermitted(EntityType.COMPANY_TYPE + ":viewAllUsers:"
						+ sessionBean.getCompany().getCompanyId())) {
			users = userDao.findUsersNotInProjectByProjectId(sessionBean.getCompany().getCompanyId(), sessionBean
					.getProject().getProjectId());
			return users;
		} else {
			users = authorizationService.getAllowedUsersNotInProject(sessionBean.getRoleContext(), sessionBean
					.getCompany().getCompanyId(), sessionBean.getProject().getProjectId());
			log.debug("Found {} users allowed not in project", users.size());
			return users;
		}

	}

	private List<User> getUsersInProject() {
		List<ProjectUser> projectUsers = sessionBean.getProject().getProjectUsers();
		List<User> users = new ArrayList<User>();

		for (ProjectUser projectUser : projectUsers) {
			users.add(projectUser.getUser());
		}

		return users;
	}

	private void resetAvailableParticipantsTable() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		DataTable dt = (DataTable) ComponentUtils.findComponent(facesContext.getViewRoot(), "tblAvailableParticipants");
		dt.reset();
		log.debug("reset datatable {} ", dt.getClientId());
	}

	public String remove(ParticipantObj pp) {
		log.debug("projectParticipantsControllerBean.remove id: {}", pp.getUserId());
		// remove from selection list
		projectParticipantsViewBean.getSelections().remove(pp);
		// add back to available list
		projectParticipantsViewBean.getParticipants().add(0, pp);

		int availsize = projectParticipantsViewBean.getParticipants().size();
		int selsize = projectParticipantsViewBean.getSelections().size();
		log.debug("\t in remove, avail count = {}, selected count = {}", availsize, selsize);
		projectParticipantsViewBean.setAddExistingParticipantsDisabled(selsize == 0);
		return null;
	}

	// add to bottom selection list and remove from available list
	public void handleAddToSelection(ActionEvent actionEvent) {
		log.debug("projectParticipantsControllerBean.handleAddToSelection");

		List<ParticipantObj> selectedUsers = projectParticipantsViewBean.getSelectedUsers();
		List<ParticipantObj> participants = projectParticipantsViewBean.getParticipants();
		List<ParticipantObj> selections = projectParticipantsViewBean.getSelections();

		participants.removeAll(selectedUsers);
		selections.addAll(selectedUsers);

		// Clearing selected users and disabling Add To Selection button
		projectParticipantsViewBean.setSelectedUsers(null);
		projectParticipantsViewBean.setAddToSelectionDisabled(true);

		// Not sure why we doing that Add Participants button is part of
		// projectManagment
		Boolean disableAddParticipants = (projectParticipantsViewBean.getSelections().size() == 0);
		projectParticipantsViewBean.setAddExistingParticipantsDisabled(disableAddParticipants);

		resetFilters();
	}

	public void resetFilters() {
		projectParticipantsViewBean.setFilteredUsers(null);
	}

	public void save() {
		// Create User
		log.debug("projectParticipantsControllerBean.save");

		// Populate client users into view list
		List<ParticipantObj> selList = projectParticipantsViewBean.getSelections();
		Subject subject = SecurityUtils.getSubject();
		ParticipantObj p;
		Iterator<ParticipantObj> iterator = selList.iterator();
		Project currentProject = sessionBean.getProject();
		List<ProjectCourse> pcList = getSessionBean().getProject().getProjectCourses();
		List<User> users = new ArrayList<User>();

		while (iterator.hasNext()) {
			p = iterator.next();

			User user = userDao.findById(p.getUserId());
			users.add(user);

			if (user != null) {
				ProjectUser projectUser = new ProjectUser();
				projectUser.setProject(currentProject);
				projectUser.setUser(user);
				projectUser.setAddedBy(subject.getPrincipal().toString());
				currentProject.getProjectUsers().add(projectUser);
				log.debug("added {}", p.getUserId());

				// JJB - Seems like weirdness but now seems to be working
				// my first experiment - do it with a DAO method
				projectUserDao.create(projectUser);

				// add to roster and position
				if (user.getPositions() == null) {
					user.setPositions(new ArrayList<Position>());
				}
				if (user.getRosters() == null) {
					user.setRosters(new ArrayList<Roster>());
				}

				// Loop through courses in project and add the position entries
				Iterator<ProjectCourse> pcIterator = pcList.iterator();
				while (pcIterator.hasNext()) {
					ProjectCourse projectCourse = pcIterator.next();
					// JJB: Should only enroll if not already enrolled
					if (projectCourse.getSelected()) {
						user = getEnrollmentService().enrollUserInCourse(sessionBean.getCompany(), projectCourse, user);
						getEnrollmentService().activateDeactivateInstrument(sessionBean.getCompany(), projectCourse,
								user);
					} else {
						activateUnactivateInstrument(projectCourse, user.getPositions());
					}
				}

				String consentType = ConsentText.ASSESSMENT;
				if (currentProject.getProjectType().getProjectTypeCode().equals(ProjectType.COACH)) {
					consentType = ConsentText.COACHING;
				}
				List<ConsentUser> consentUsers = new ArrayList<ConsentUser>();
				consentUsers = consentUserDao.findByParticipantAndType(user.getUsersId(), consentType);
				if (consentUsers == null || consentUsers.size() == 0) {
					ConsentUser consentUser = new ConsentUser();
					consentUser.setAccepted((short) 0);
					consentUser.setUser(user);
					consentUser.setConsentType(consentType);
					consentUserDao.create(consentUser);
				}
			}
		}

		projectParticipantsViewBean.setUpdatePDAError(false);
		if (isPDAProject) {
			AddUserResponse response = addUserService.addUsersToLrm(users,
					getSessionBean().getProject().getProjectId(), getSessionBean().getSubjectId());
			if (response.getPostResult().equalsIgnoreCase("FAILED")) {
				projectParticipantsViewBean.setUpdatePDAError(true);
			}
		}

		// projectDao.update(currentProject);
		log.debug("End of save");
	}

	public void resetPDAError(ActionEvent event) {
		projectParticipantsViewBean.setUpdatePDAError(false);
	}

	public void activateUnactivateInstrument(ProjectCourse projectCourse, List<Position> positions) {
		log.debug("In activateUnactivateInstrument");
		// JJB: Making assumption then when an existing user is added to a
		// project with assessment day instruments
		// that those assessment day instrument become inactive
		for (Position position : positions) {
			if (position.getCourse().getAbbv().equals(projectCourse.getCourse().getAbbv())
					&& projectCourse.getSelected()) {
				if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 1 && !position.getActivated()) {
					log.debug("Activate deactivated instrument {}", position.getCourse().getAbbv());
					position.setActivated(true);
					positionDao.update(position);
				} else if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 2 && position.getActivated()) {
					log.debug("Deactivate activated instrument {}", position.getCourse().getAbbv());
					position.setActivated(false);
					positionDao.update(position);
				}
			}
		}
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	// Client users not in current project
	public List<User> getUnusedClientUsers() {
		return userDao.findUsersNotInProjectByProjectId(sessionBean.getCompany().getCompanyId(), sessionBean
				.getProject().getProjectId());
	}

	public void setEnrollmentService(EnrollmentServiceLocalImpl enrollmentService) {
		this.enrollmentService = enrollmentService;
	}

	public EnrollmentServiceLocalImpl getEnrollmentService() {
		return enrollmentService;
	}

	public ProjectParticipantsViewBean getProjectParticipantsViewBean() {
		return projectParticipantsViewBean;
	}

	public void setProjectParticipantsViewBean(ProjectParticipantsViewBean projectParticipantsViewBean) {
		this.projectParticipantsViewBean = projectParticipantsViewBean;
	}

	public boolean isPDAProject() {
		return isPDAProject;
	}

	public void setPDAProject(boolean isPDAProject) {
		this.isPDAProject = isPDAProject;
	}

}
