package com.pdinh.presentation.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.RealmFilter;

public class LazySubjectDataModel extends LazyDataModel<ExternalSubjectDataObj> {

	
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(LazySubjectDataModel.class);
	
	public ExternalSubjectServiceLocal getExternalSubjectService() {
		return externalSubjectService;
	}

	public void setExternalSubjectService(
			ExternalSubjectServiceLocal externalSubjectService) {
		this.externalSubjectService = externalSubjectService;
	}

	private List<ExternalSubjectDataObj> data;
	

	
	private String queryFilter;
	
	private RealmFilter filterDef;
	
	private Object[] parameters;
	
	public String getQueryFilter() {
		return queryFilter;
	}

	public void setQueryFilter(String queryFilter) {
		this.queryFilter = queryFilter;
	}


	@EJB
	private ExternalSubjectServiceLocal externalSubjectService;
	
	public LazySubjectDataModel(List<ExternalSubjectDataObj> subjectList){
		this.data = subjectList;
	}
	
	@Override  
    public ExternalSubjectDataObj getRowData(String rowKey) {  
        for(ExternalSubjectDataObj obj : data) {  
        	String uniqueId = obj.getUsername() + "_" + String.valueOf(obj.getRealm_id());
            if(uniqueId.equals(rowKey)){  
                return obj;  
            }
        }  
  
        return null;  
    }
	
	@Override  
    public Object getRowKey(ExternalSubjectDataObj obj) {  
        return obj.getUsername() + "_" + String.valueOf(obj.getRealm_id());  
    }  
	
	@Override  
    public List<ExternalSubjectDataObj> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,String> filters) {  
        List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();  
        //int pageNumber = (first + 1) / pageSize;
        log.debug("First: {}", first);
        log.debug("Page Size: {}", pageSize);
        //log.debug("Page Number: {}", pageNumber);

        if (externalSubjectService == null){
        	log.debug("Subject Service is NULLL!!!!!!!!!!!");
        	
        }
        List<ExternalSubjectDataObj> dataObjs = externalSubjectService.getExternalSubjectsPage(getFilterDef(), getParameters(), false, first, pageSize);
        setData(dataObjs);
        //filter  
        log.debug("Set Data: {}", dataObjs.size());
        for(ExternalSubjectDataObj subject : this.data) {  
            boolean match = true;  
  
            for(Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {  
                try {  
                    String filterProperty = it.next();  
                    String filterValue = filters.get(filterProperty);  
                    String fieldValue = String.valueOf(subject.getClass().getField(filterProperty).get(subject));  
  
                    if(filterValue == null || fieldValue.startsWith(filterValue)) {  
                        match = true;  
                    }  
                    else {  
                        match = false;  
                        break;  
                    }  
                } catch(Exception e) {  
                    match = false;  
                }   
            }  
  
            if(match) {  
                subjects.add(subject);  
            }  
        }  
  
        //sort  
        if(sortField != null) {  
            Collections.sort(subjects, new LazySorter(sortField, sortOrder));  
        }  
  
        //rowCount  
       // int dataSize = subjects.size();  
        int dataSize = externalSubjectService.getRowCount(getQueryFilter(), getFilterDef().getRealm().getLdap_base_domain());
       
        this.setRowCount(dataSize);  
        int currentDataSize = subjects.size();
        //paginate  
        if(currentDataSize > pageSize) {  
            try {  
                return subjects.subList(first, first + pageSize);  
            }  
            catch(IndexOutOfBoundsException e) {  
                return subjects.subList(first, first + (dataSize % pageSize));  
            }  
        }  
        else {  
            return subjects;  
        }  
    }  

	public List<ExternalSubjectDataObj> getData() {
		return data;
	}

	public void setData(List<ExternalSubjectDataObj> data) {
		this.data = data;
	}

	public RealmFilter getFilterDef() {
		return filterDef;
	}

	public void setFilterDef(RealmFilter filterDef) {
		this.filterDef = filterDef;
	}

	public Object[] getParameters() {
		return parameters;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

}
