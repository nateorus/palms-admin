package com.pdinh.presentation.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;

import com.pdinh.data.CreateUserServiceLocal;
import com.pdinh.data.EnrollmentServiceLocalImpl;
import com.pdinh.data.UsernameServiceLocal;
import com.pdinh.data.jaxb.lrm.AddUserResponse;
import com.pdinh.data.jaxb.lrm.AddUserServiceLocal;
import com.pdinh.data.ms.dao.ConsentUserDao;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.HchylvlDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.ms.dao.OfficeLocationDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Hchylvl;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.OfficeLocation;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.Roster;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.AddUserResultObj;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.helper.ParticipantValidator;
import com.pdinh.presentation.helper.Utils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ImportParticipantViewBean;

@ManagedBean
@ViewScoped
public class ImportParticipantControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ImportParticipantControllerBean.class);

	private static String FIRSTNAME_HEADER = "firstname";
	private static String LASTNAME_HEADER = "lastname";
	private static String EMAIL_HEADER = "email";
	private static String EMPLOYEEID_HEADER = "employeeid";
	private static String LANGUAGE_HEADER = "language";
	private static String PASSWORD_HEADER = "password";
	private static String OPTIONAL_1_HEADER = "optional_1";
	private static String OPTIONAL_2_HEADER = "optional_2";
	private static String OPTIONAL_3_HEADER = "optional_3";
	private static String SUPERVISOR_NAME_HEADER = "supervisor_name";
	private static String SUPERVISOR_EMAIL_HEADER = "supervisor_email";
	private static String OFFICE_LOCATION_HEADER = "office_location";
	private static String USERNAME_HEADER = "logon_id";
	private static String CHQ_LANG_HEADER = "chq_lang";
	private static String FINEX_LANG_HEADER = "finex_lang";
	private static String ORG_AREA_HEADER = "org_area";
	// private static String chqAbbv = "chq";
	// private static String fexAbbv = "finex";
	private Boolean stopPoll = false;
	private String csvString;
	private String uploader;
	private int totalLines = 0;

	public int getTotalLines() {
		return totalLines;
	}

	public void setTotalLines(int totalLines) {
		this.totalLines = totalLines;
	}

	public String getUploader() {
		return uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	private List<AddUserResultObj> importLog = new ArrayList<AddUserResultObj>();

	public List<AddUserResultObj> getImportLog() {
		return importLog;
	}

	public void setImportLog(List<AddUserResultObj> importLog) {
		this.importLog = importLog;
	}

	@EJB
	private CreateUserServiceLocal createUserService;

	@EJB
	private EnrollmentServiceLocalImpl enrollmentService;

	@EJB
	private UsernameServiceLocal usernameService;

	@EJB
	private LanguageDao languageDao;

	@EJB
	private CourseDao courseDao;

	@EJB
	UserDao userDao;

	@EJB
	HchylvlDao hchylvlDao;

	@EJB
	ProjectUserDao projectUserDao;

	@EJB
	ConsentUserDao consentUserDao;

	@EJB
	private LanguageSingleton languageSingleton;

	@EJB
	private OfficeLocationDao officeLocationDao;

	@EJB
	private AddUserServiceLocal addUserService;

	@ManagedProperty(name = "importParticipantViewBean", value = "#{importParticipantViewBean}")
	private ImportParticipantViewBean importParticipantViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "projectManagementControllerBean", value = "#{projectManagementControllerBean}")
	private ProjectManagementControllerBean projectManagementControllerBean;

	public void pollingListener() {
		if (stopPoll) {
			RequestContext.getCurrentInstance().addCallbackParam("stopPolling", true);
		}
	}

	public void initForImport() {
		stopPoll = false;
		this.csvString = null;
		importParticipantViewBean.setDisableProcessButton(true);
		importParticipantViewBean.setShowCompletePanel(false);
		importParticipantViewBean.setShowUploadPanel(true);
		importParticipantViewBean.setRenderResultsTable(false);
		importParticipantViewBean.setResults(new ArrayList<AddUserResultObj>());
		importParticipantViewBean.setPercentComplete(0);
		initFileLinkUrls();
	}

	public void handleComplete() {
		log.debug("Handle completion of progress bar here");
	}

	// determine file links based on project type
	public void initFileLinkUrls() {

		String base = Utils.getServerAndContextPath() + "/resources/documents";

		Project currentProject = sessionBean.getProject();

		int projectTypeId = currentProject.getProjectType().getProjectTypeId();

		if (projectTypeId == 1 || projectTypeId == 2) {
			importParticipantViewBean.setHelpFileUrl(base + "/bulk_upload_instructions_tlt.doc");
			importParticipantViewBean.setImportTemplateFileUrl(base + "/import_template_tlt.csv");
		} else {
			// use generic instructions
			importParticipantViewBean.setHelpFileUrl(base + "/bulk_upload_instructions.doc");
			importParticipantViewBean.setImportTemplateFileUrl(base + "/import_template.csv");
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		log.debug(" IN ImportParticipantControllerBean.handleFileUpload()");
		initForImport();
		UploadedFile file = event.getFile();
		log.debug("file size: {}",file.getSize());
		//file size limit 500 KB
		if ((file.getSize() / 1000) > 500){
			log.error("file size too big");
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "The uploaded file exceeds the size limit of 500 KB.  ", "Check the file for excessive blank lines or break the data into multiple files."));
			RequestContext.getCurrentInstance().addCallbackParam("stopPolling", true);
			this.stopPoll = true;
			importParticipantViewBean.setDisableProcessButton(true);
			importParticipantViewBean.setRenderResultsTable(true);
			return;
		}
		InputStream inputStream;
		try {
			inputStream = file.getInputstream();
			String csvString = IOUtils.toString(inputStream);
			this.csvString = csvString;
			importParticipantViewBean.setDisableProcessButton(false);
			importParticipantViewBean.setRenderResultsTable(true);
			this.stopPoll = false;
		} catch (IOException e1) {
			// TODO: handle exception
			RequestContext.getCurrentInstance().addCallbackParam("stopPolling", true);
			e1.printStackTrace();
		}
	}

	public void processDataFile() {
		Subject subject = SecurityUtils.getSubject();
		this.uploader = subject.getPrincipal().toString();
		if (this.csvString != null) {
			this.stopPoll = false;
			importParticipantViewBean.setDisableProcessButton(true);
			importParticipantViewBean.setRenderResultsTable(true);
			List<String[]> lines = null;
			String firstname;
			String lastname;
			String emailaddress;
			CSVReader reader = null;
			ParticipantValidator pv = new ParticipantValidator(userDao);
			Company company = sessionBean.getCompany();

			List<String> chqSupportedLanguageCodes = null;
			List<String> finexSupportedLanguageCodes = null;
			List<String> allSupportedLanguageCodes = null;
			List<User> importedUsers = new ArrayList<User>();

			int successCounter = 0;
			int failureCounter = 0;

			try {
				reader = new CSVReader(new StringReader(this.csvString));
				try {
					lines = reader.readAll();
				} catch (IOException e1) {
					// TODO: handle exception
					RequestContext.getCurrentInstance().addCallbackParam("stopPolling", true);
					e1.printStackTrace();
				}
				this.totalLines = lines.size();
			} finally {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// setup to validate the headers
			String[] headers = lines.get(0);
			HashMap<String, Boolean> headersmap = new HashMap<String, Boolean>();
			for (int h = 0; h < headers.length; h++) {
				headersmap.put(headers[h].trim().toLowerCase(), true);
			}

			// load supported language lists
			allSupportedLanguageCodes = new ArrayList<String>();
			List<Language> allLanguages = languageDao.findAll();
			for (Language language : allLanguages) {
				allSupportedLanguageCodes.add(language.getCode());
			}

			if (headersmap.containsKey(CHQ_LANG_HEADER)) {
				chqSupportedLanguageCodes = new ArrayList<String>();
				List<LanguageObj> languages = languageSingleton.getCourseLangs("chq");
				for (LanguageObj language : languages) {
					chqSupportedLanguageCodes.add(language.getCode());
				}
			}

			if (headersmap.containsKey(FINEX_LANG_HEADER)) {
				finexSupportedLanguageCodes = new ArrayList<String>();

				List<LanguageObj> languages = languageSingleton.getCourseLangs("fex");
				for (LanguageObj language : languages) {
					finexSupportedLanguageCodes.add(language.getCode());
				}
			}

			for (int i = 1; i < lines.size(); i++) {
				log.debug("Percent Complete: {}", (int) Math.round(i * 100.0 / this.totalLines));
				importParticipantViewBean.setPercentComplete((int) Math.round(i * 100.0 / this.totalLines));
				String[] line = lines.get(i);
				int length = line.length;
				HashMap<String, String> record = new HashMap<String, String>();
				for (int h = 0; h < headers.length; h++) {
					if (length >= h + 1 && line[h] != null && !line[h].equals("")) {
						record.put(headers[h].trim().toLowerCase(), line[h].trim());
					}
				}

				try {
					firstname = record.get(FIRSTNAME_HEADER);
					lastname = record.get(LASTNAME_HEADER);
					emailaddress = record.get(EMAIL_HEADER);
					String userLang = record.get(LANGUAGE_HEADER);
					String employeeId = record.get(EMPLOYEEID_HEADER);
					String password = record.get(PASSWORD_HEADER);
					String opt_1 = record.get(OPTIONAL_1_HEADER);
					String opt_2 = record.get(OPTIONAL_2_HEADER);
					String opt_3 = record.get(OPTIONAL_3_HEADER);
					String superName = record.get(SUPERVISOR_NAME_HEADER);
					String superEmail = record.get(SUPERVISOR_EMAIL_HEADER);
					String chqLang = record.get(CHQ_LANG_HEADER);
					String finexLang = record.get(FINEX_LANG_HEADER);
					String officeLocId = record.get(OFFICE_LOCATION_HEADER);
					String username = record.get(USERNAME_HEADER);
					FacesContext facesContext = FacesContext.getCurrentInstance();
					if (firstname == null && lastname == null && emailaddress == null){
						log.error("Encountered NULL data.  Terminating upload file processing");

						facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "File processing ended prematurely due to a blank line on row " + (i+1) + ". ", "Remove blank lines to process any remaining records in the file."));
						break;
					}else if ((firstname + lastname + emailaddress).trim().length() == 0){
						log.error("Encountered blank line.  Terminating upload file processing");
						facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "File processing ended prematurely due to a blank line on row " + (i+1) + ". ", "Remove blank lines to process any remaining records in the file."));
						break;
					}

					if (!headersmap.containsKey(USERNAME_HEADER) || username == null) {
						username = getUsernameService().generateUsername(emailaddress, firstname, lastname);
					}
					User user = new User();
					user.setFirstname(firstname);
					user.setLastname(lastname);
					user.setEmail(emailaddress);
					user.setUsername(username);
					user.setLangPref(userLang);
					user.setOptional1(opt_1);
					user.setOptional2(opt_2);
					user.setOptional3(opt_3);
					user.setSupervisorEmail(superEmail);
					user.setSupervisorName(superName);
					user.setEmployeeid(employeeId);
					user.setCompany(company);

					if (headersmap.containsKey(OFFICE_LOCATION_HEADER)) {
						if (officeLocId == null) {
							// skip it; We'll catch it later
							// (we don't have a result to stuff it in yet)
						} else {
							int olId;
							try {
								olId = Integer.parseInt(officeLocId);
							} catch (NumberFormatException e) {
								// Invalid id (text) make it 0 so it will fail
								// downstream
								olId = 0;
							}
							OfficeLocation oLoc = officeLocationDao.findById(olId);
							if (oLoc == null) {
								oLoc = new OfficeLocation();
								oLoc.setId(ParticipantValidator.VALIDATION_ERROR);
							}
							user.setOfficeLocation(oLoc);
						}
					}

					log.debug(" processing user {}", emailaddress);

					if (user.getCompany().getRandomizePasswords() == 1) {
						log.debug("Randomize Passwords is ON");
						if (user.getPassword() == null || user.getPassword() == "") {
							log.debug("password is null or empty string.  generating new");
							user.setPassword(Utils.getNewPassword());
						} else {
							log.debug("Password has been set from import file.  this is an error");
							ParticipantObj participantObj = new ParticipantObj(user.getUsersId(), user.getFirstname(),
									user.getLastname(), user.getEmail(), user.getUsername());
							AddUserResultObj result = new AddUserResultObj(participantObj);
							result.setStatus(ParticipantValidator.FAIL_STATUS);
							result.setSuccess(false);
							ErrorObj error = new ErrorObj("IMEC001",
									"Password Provided when company is set to generate random passwords");
							result.getErrors().add(error);
							continue;
						}
					} else {
						log.debug("Generate Random is OFF.  Setting password to {}", password);
						user.setPassword(password);
					}
					AddUserResultObj result = pv.validateUserObj(user);
					if (result.getSuccess()) {
						log.debug("user validated");

						// check language
						if (user.getLangPref() == null || user.getLangPref() == "") {
							log.debug(" missing language");
							result.setStatus(ParticipantValidator.FAIL_STATUS);
							result.setSuccess(false);
							ErrorObj error = new ErrorObj("IMEC010", "No user language preference name provided");
							result.getErrors().add(error);
						} else if (!allSupportedLanguageCodes.contains(userLang)) {
							log.debug(" unsupported language preference");
							result.setStatus(ParticipantValidator.FAIL_STATUS);
							result.setSuccess(false);
							ErrorObj error = new ErrorObj("IMEC013", "The user language preference " + userLang
									+ " is not supported");
							result.getErrors().add(error);
						}

						// if an instrument language pref column is present,
						// then value must be present
						if (headersmap.containsKey(CHQ_LANG_HEADER)) {
							if (chqLang == null || chqLang == "") {
								log.debug(" missing chq language");
								result.setStatus(ParticipantValidator.FAIL_STATUS);
								result.setSuccess(false);
								ErrorObj error = new ErrorObj("IMEC011",
										"CHQ language column present but value missing");
								result.getErrors().add(error);
							} else if (!chqSupportedLanguageCodes.contains(chqLang)) {
								log.debug(" unsupported chq language {}", chqLang);
								result.setStatus(ParticipantValidator.FAIL_STATUS);
								result.setSuccess(false);
								ErrorObj error = new ErrorObj("IMEC014", "CHQ language " + chqLang
										+ " is not supported");
								result.getErrors().add(error);
							}
						}
						if (headersmap.containsKey(FINEX_LANG_HEADER)) {
							if (finexLang == null || finexLang == "") {
								log.debug(" missing finex language");
								result.setStatus(ParticipantValidator.FAIL_STATUS);
								result.setSuccess(false);
								ErrorObj error = new ErrorObj("IMEC012",
										"FINEX language column present but value missing");
								result.getErrors().add(error);
							} else if (!finexSupportedLanguageCodes.contains(finexLang)) {
								log.debug(" unsupported finex language {}", finexLang);
								result.setStatus(ParticipantValidator.FAIL_STATUS);
								result.setSuccess(false);
								ErrorObj error = new ErrorObj("IMEC015", "FINEX language " + finexLang
										+ " is not supported");
								result.getErrors().add(error);
							}
						}

						String levelName = record.get(ORG_AREA_HEADER);
						if (levelName != null && levelName != "") {
							Hchylvl hLevel = hchylvlDao.findByLeveName(levelName, company.getCompanyId());
							if (hLevel != null) {
								user.setCosubid(hLevel.getId());
							} else {
								result.setStatus(ParticipantValidator.FAIL_STATUS);
								result.setSuccess(false);
								ErrorObj error = new ErrorObj("IME009", "Invalid Org Area value");
								result.getErrors().add(error);
								continue;
							}
						}

						// Add check of office loc ID.
						// "if the column is there, it needs to be populated"
						// Here because now we have a result to put errors into
						if (headersmap.containsKey(OFFICE_LOCATION_HEADER)) {
							if (user.getOfficeLocation() == null) {
								result.setStatus(ParticipantValidator.FAIL_STATUS);
								result.setSuccess(false);
								ErrorObj error = new ErrorObj("IME016",
										"Valid office location is required if header is present");
								result.getErrors().add(error);
							}
						}

						importParticipantViewBean.getResults().add(result);

						// if results have errors, just go back to top of loop
						if (!result.getErrors().isEmpty()) {
							continue;
						}

						// addParticipant(user);
						createUserService.createNewUser(user, company, sessionBean.getProject(), chqLang, finexLang);
						importedUsers.add(user);

						/*
						Course chq = courseDao.findByAbbv(chqAbbv);
						Course fex = courseDao.findByAbbv(fexAbbv);
						userDao.updateCourseLanguagePref(user, chq, chqLang);
						userDao.updateCourseLanguagePref(user, fex, finexLang);
						*/

					} else {
						// initial validation failed
						importParticipantViewBean.getResults().add(result);
					}
					importLog.add(result);
					if (result.getSuccess()) {
						successCounter++;
					} else {
						failureCounter++;
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					RequestContext.getCurrentInstance().addCallbackParam("stopPolling", true);

				}
			} // for each line
			EntityAuditLogger.auditLog.info(
					"{} performed {} action in Project {}({}).  {} participants attempted.  {} succeeded.  {} failed.",
					new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_BULK_USER_UPLOAD,
							sessionBean.getProject().getProjectId(), sessionBean.getProject().getName(),
							successCounter + failureCounter, successCounter, failureCounter });

			importParticipantViewBean.setShowCompletePanel(true);
			importParticipantViewBean.setShowUploadPanel(false);
			stopPoll = true;
			importParticipantViewBean.setPercentComplete(100);

			if (projectManagementControllerBean.isPDAProject()) {
				AddUserResponse response = addUserService.addUsersToLrm(importedUsers, getSessionBean().getProject().getProjectId(), getSessionBean().getSubjectId());
				if (response.getPostResult().equalsIgnoreCase("FAILED")) {
					FacesContext facesContext = FacesContext.getCurrentInstance();
					facesContext.addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_WARN,
									"Users were successfuly imported into PALMS, but corresponding PDA records were not updated.",
									"Check request status for PDA records."));
				}
			}

			// refresh the project view
			projectManagementControllerBean.refreshView();

		} // if (this.csvString != null)

	}

	// Someone please refactor to use service
	public void addParticipant(User user) {

		log.debug("IN ImportParticipantControllerBean.addParticipant()");
		// TODO: Move this to businesss logic tier
		// May also be able to do using cascading persist. For now this is
		// easier.
		log.debug("IN save");
		// Still trying to figure out pattern here, but working
		log.debug("create action");

		// Create User
		log.debug("userId={}", user.getEmail());
		log.debug("firstName={}", user.getFirstname());
		log.debug("lastName={}", user.getLastname());
		log.debug("email={}", user.getEmail());
		log.debug("langId={}", user.getLangPref());

		// Hardwired per Nate
		user.setActive((short) 1);
		user.setType(1);
		user.setMserverid((short) 1);
		user.setLearningType((short) 1);

		user.setPositions(new ArrayList<Position>());
		user.setRosters(new ArrayList<Roster>());

		// Loop through courses in project and add the position entries
		Iterator<ProjectCourse> iterator = getSessionBean().getProject().getProjectCourses().iterator();
		while (iterator.hasNext()) {
			ProjectCourse projectCourse = iterator.next();
			if (projectCourse.getSelected()) {
				user = getEnrollmentService().enrollUserInCourse(sessionBean.getCompany(), projectCourse, user);
				getEnrollmentService().activateDeactivateInstrument(sessionBean.getCompany(), projectCourse, user);
			}
		}
		userDao.create(user);

		// Set the Hashed Password.. uses first 10 characters of rowguid as Salt
		user.setHashedpassword(Utils.generateHashedPassword(user.getPassword(), user.getRowguid().substring(0, 10)
				.toUpperCase()));

		userDao.update(user);

		// add the user to the project
		ProjectUser projectUser = new ProjectUser();
		projectUser.setProject(sessionBean.getProject());
		projectUser.setUser(user);
		projectUser.setAddedBy(uploader);
		sessionBean.getProject().getProjectUsers().add(projectUser);
		projectUserDao.create(projectUser);
		/*
				// add consent for user
				ConsentUser consentUser = new ConsentUser();
				consentUser.setAccepted((short) 0);
				consentUser.setUser(user);
				String consentType = ConsentText.ASSESSMENT;
				if (sessionBean.getProject().getProjectType().equals(ProjectType.COACH)) {
					consentType = ConsentText.COACHING;
				}
				consentUser.setConsentType(consentType);
				consentUserDao.create(consentUser);
		*/
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setLanguageDao(LanguageDao languageDao) {
		this.languageDao = languageDao;
	}

	public LanguageDao getLanguageDao() {
		return languageDao;
	}

	public void setImportParticipantViewBean(ImportParticipantViewBean importParticipantViewBean) {
		this.importParticipantViewBean = importParticipantViewBean;
	}

	public ImportParticipantViewBean getImportParticipantViewBean() {
		return importParticipantViewBean;
	}

	public void setStopPoll(Boolean stopPoll) {
		this.stopPoll = stopPoll;
	}

	public Boolean getStopPoll() {
		return stopPoll;
	}

	public void setCsvString(String csvString) {
		this.csvString = csvString;
	}

	public String getCsvString() {
		return csvString;
	}

	public void setEnrollmentService(EnrollmentServiceLocalImpl enrollmentService) {
		this.enrollmentService = enrollmentService;
	}

	public EnrollmentServiceLocalImpl getEnrollmentService() {
		return enrollmentService;
	}

	public void setProjectManagementControllerBean(ProjectManagementControllerBean projectManagementControllerBean) {
		this.projectManagementControllerBean = projectManagementControllerBean;
	}

	public UsernameServiceLocal getUsernameService() {
		return usernameService;
	}

	public void setUsernameService(UsernameServiceLocal usernameService) {
		this.usernameService = usernameService;
	}

	public ProjectManagementControllerBean getProjectManagementControllerBean() {
		return projectManagementControllerBean;
	}

}
