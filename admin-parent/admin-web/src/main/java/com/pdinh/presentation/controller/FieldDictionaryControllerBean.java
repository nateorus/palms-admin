package com.pdinh.presentation.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.dto.StandardFieldDto;
import com.kf.uffda.service.UffdaService;
import com.kf.uffda.vo.CoreFieldGroupVo;
import com.pdinh.presentation.managedbean.LanguageBean;
import com.pdinh.presentation.view.CustomFieldViewBean;
import com.pdinh.presentation.view.FieldDictionaryViewBean;
import com.pdinh.presentation.view.ProjectFieldsViewBean;
import com.pdinh.presentation.view.StandardFieldViewBean;

@ManagedBean(name = "fieldDictionaryControllerBean")
@ViewScoped
public class FieldDictionaryControllerBean extends ControllerBeanBase implements Serializable {

	private static final long serialVersionUID = 7758073376820447628L;

	private static final Logger log = LoggerFactory.getLogger(FieldDictionaryControllerBean.class);

	@EJB
	UffdaService uffdaService;

	@ManagedProperty(name = "languageBean", value = "#{languageBean}")
	private LanguageBean languageBean;

	@ManagedProperty(name = "customFieldViewBean", value = "#{customFieldViewBean}")
	private CustomFieldViewBean customFieldViewBean;

	@ManagedProperty(name = "standardFieldViewBean", value = "#{standardFieldViewBean}")
	private StandardFieldViewBean standardFieldViewBean;

	@ManagedProperty(name = "projectFieldsViewBean", value = "#{projectFieldsViewBean}")
	private ProjectFieldsViewBean projectFieldsViewBean;

	@ManagedProperty(name = "fieldDictionaryViewBean", value = "#{fieldDictionaryViewBean}")
	private FieldDictionaryViewBean fieldDictionaryViewBean;

	public FieldDictionaryControllerBean() {
		super("frmFieldDictionary");
	}

	@Override
	public void initView(ComponentSystemEvent event) {
		if (isFirstTime()) {
			setFirstTime(false);

			fieldDictionaryViewBean.init();
			fieldDictionaryViewBean.setShowStandardField(false);
			fieldDictionaryViewBean.setShowCustomField(false);
			fieldDictionaryViewBean.setEditOnly(false);

			fieldDictionaryViewBean.addLanguages(languageBean.getLanguageListForInternal());

			String fieldId = getRequestParameter("fieldId");
			FieldDto field = null;
			if (fieldId != null) {
				field = projectFieldsViewBean.findFieldById(Integer.parseInt(fieldId));

				if (field != null) {
					if (field.isCustomField()) {
						fieldDictionaryViewBean.setShowCustomField(true);
						fieldDictionaryViewBean.setEditOnly(true);

						customFieldViewBean.init();
						customFieldViewBean.addLanguages(languageBean.getLanguageListForInternal());
						customFieldViewBean.setField(new CustomFieldDto((CustomFieldDto) field));
						customFieldViewBean.setEditOnly(true);
						customFieldViewBean.setValueMaps(uffdaService.getFieldValueMapSets(field.getCompanyId()));

					} else if (field.isStandardField()) {
						fieldDictionaryViewBean.setShowStandardField(true);
						fieldDictionaryViewBean.setEditOnly(true);

						standardFieldViewBean.init();
						standardFieldViewBean.setField(new StandardFieldDto(field));
						standardFieldViewBean.setEditOnly(true);
						standardFieldViewBean.setValueMaps(uffdaService.getFieldValueMapSets(field.getCompanyId()));
					}
				}

			} else {
				fieldDictionaryViewBean.clearFields();
				fieldDictionaryViewBean.addFields(uffdaService.getAllCoreFields(
						sessionBean.getCompany().getCompanyId(), CoreFieldGroupVo.PALMS, 0, null, true, true, false,
						true));
				try {
					fieldDictionaryViewBean.addFields(uffdaService.getCustomFieldDtos(sessionBean.getCompany()
							.getCompanyId(), false, true, true));
				} catch (Exception e) {
					log.error("Error occurred while reading the Custom Fields.  ", e);
				}
			}

			getSessionBean().clearProject();

		}
	}

	public void handleCreateCustomField(ActionEvent event) {
		fieldDictionaryViewBean.setShowStandardField(false);
		fieldDictionaryViewBean.setShowCustomField(true);
		customFieldViewBean.init();
		customFieldViewBean.getField().setInternal(false);
		customFieldViewBean.addLanguages(languageBean.getLanguageListForInternal());
		customFieldViewBean.setValueMaps(uffdaService.getFieldValueMapSets(sessionBean.getCompany().getCompanyId()));
	}

	public void handleAddSelectedField(ActionEvent event) {
		FieldDto field = fieldDictionaryViewBean.getField();
		if (field != null && field.isNotEmpty()) {

			// Check if the field as already been added
			if (field.isCustomField()) {
				if (projectFieldsViewBean.findFieldByDatabaseId(field.getDatabaseId(), field.getFieldCategory()) == null) {
					CustomFieldDto newField = new CustomFieldDto((CustomFieldDto) field);
					if (!newField.isAddedToAllProjects()) {
						newField.setId(projectFieldsViewBean.getNextFieldId());
						newField.setSequenceOrder(projectFieldsViewBean.getNextSequence(CustomFieldDto.CUSTOM_SEQ_BASE,
								CustomFieldDto.CUSTOM_SEQ_MAX));
						newField.setChanged(true);
					}
					projectFieldsViewBean.addField(newField);
					projectFieldsViewBean.setClientSideValueChanged(true);
				}

			} else if (field.isStandardField()) {
				if (projectFieldsViewBean.findFieldById(field.getId()) == null) {
					projectFieldsViewBean.addField(new StandardFieldDto(field));
					projectFieldsViewBean.sortFields();
					projectFieldsViewBean.setClientSideValueChanged(true);
				}

			} else {
				projectFieldsViewBean.addField(field);
				projectFieldsViewBean.sortFields();
				projectFieldsViewBean.setClientSideValueChanged(true);
			}
		}
	}

	public void handleSelectField(SelectEvent event) {
		fieldDictionaryViewBean.setAddIntakeFieldDisabled(true);
		FieldDto field = fieldDictionaryViewBean.getField();
		if (field != null && field.isNotEmpty()) {

			if (field.isCustomField()) {
				CustomFieldDto customFieldDto = (CustomFieldDto) field;
				if (!customFieldDto.isAddedToAllProjects()
						&& projectFieldsViewBean.findFieldByDatabaseId(field.getDatabaseId(), field.getFieldCategory()) == null) {
					fieldDictionaryViewBean.setAddIntakeFieldDisabled(false);
				}

			} else if (field.isStandardField()) {
				if (projectFieldsViewBean.findFieldById(field.getId()) == null) {
					fieldDictionaryViewBean.setAddIntakeFieldDisabled(false);
				}

			} else {
				fieldDictionaryViewBean.setAddIntakeFieldDisabled(false);
			}
		}
	}

	public void handleEditField(ActionEvent event) {
		FieldDto field = getField(event);
		if (field != null) {
			if (field.isStandardField()) {
				fieldDictionaryViewBean.setShowStandardField(true);
				fieldDictionaryViewBean.setShowCustomField(false);
				standardFieldViewBean.init();
				standardFieldViewBean.setField(new StandardFieldDto(field));
				standardFieldViewBean.setValueMaps(uffdaService.getFieldValueMapSets(field.getCompanyId()));

			} else if (field.isCustomField()) {
				fieldDictionaryViewBean.setShowStandardField(false);
				fieldDictionaryViewBean.setShowCustomField(true);
				customFieldViewBean.init();
				customFieldViewBean.addLanguages(languageBean.getLanguageListForInternal());
				customFieldViewBean.setField(new CustomFieldDto((CustomFieldDto) field));
				customFieldViewBean.getField().setSelectedLanguageCode(customFieldViewBean.getLanguageCode());
				customFieldViewBean.setValueMaps(uffdaService.getFieldValueMapSets(field.getCompanyId()));
			}
		}
	}

	public FieldDto getField(ActionEvent event) {
		FieldDto field = null;
		if (event != null) {
			Integer fieldId = (Integer) event.getComponent().getAttributes().get("fieldId");
			if (fieldId != null) {
				field = fieldDictionaryViewBean.findFieldById(fieldId);
			}
		}
		return field;
	}

	public boolean isEditable() {
		return sessionBean.isPermitted("editFieldDictionary");
	}

	public boolean isViewable() {
		return sessionBean.isPermitted("viewFieldDictionary");
	}

	public FieldDictionaryViewBean getFieldDictionaryViewBean() {
		return fieldDictionaryViewBean;
	}

	public void setFieldDictionaryViewBean(FieldDictionaryViewBean fieldDictionaryViewBean) {
		this.fieldDictionaryViewBean = fieldDictionaryViewBean;
	}

	public LanguageBean getLanguageBean() {
		return languageBean;
	}

	public void setLanguageBean(LanguageBean languageBean) {
		this.languageBean = languageBean;
	}

	public CustomFieldViewBean getCustomFieldViewBean() {
		return customFieldViewBean;
	}

	public void setCustomFieldViewBean(CustomFieldViewBean customFieldViewBean) {
		this.customFieldViewBean = customFieldViewBean;
	}

	public StandardFieldViewBean getStandardFieldViewBean() {
		return standardFieldViewBean;
	}

	public void setStandardFieldViewBean(StandardFieldViewBean standardFieldViewBean) {
		this.standardFieldViewBean = standardFieldViewBean;
	}

	public ProjectFieldsViewBean getProjectFieldsViewBean() {
		return projectFieldsViewBean;
	}

	public void setProjectFieldsViewBean(ProjectFieldsViewBean projectFieldsViewBean) {
		this.projectFieldsViewBean = projectFieldsViewBean;
	}

}