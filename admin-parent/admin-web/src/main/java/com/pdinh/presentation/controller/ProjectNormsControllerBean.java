package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.pdinh.data.InstrumentNormsServiceLocal;
import com.pdinh.data.instrumentnorms.Course;
import com.pdinh.data.instrumentnorms.CourseNormList;
import com.pdinh.data.instrumentnorms.FetchSelectedNormsList;
import com.pdinh.data.instrumentnorms.NormGroup;
import com.pdinh.data.instrumentnorms.Participant;
import com.pdinh.data.instrumentnorms.ProjectNormsDefaults;
import com.pdinh.data.instrumentnorms.SelectedNorms;
import com.pdinh.data.instrumentnorms.SetParticipantNormsList;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectTypeDao;
import com.pdinh.persistence.ms.entity.CompanyProduct;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.presentation.domain.CourseNormsObj;
import com.pdinh.presentation.domain.CourseNormsTripletObj;
import com.pdinh.presentation.domain.NormGroupObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectNormsDefaultsObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ProjectNormsViewBean;

@ManagedBean(name = "projectNormsControllerBean")
@ViewScoped
public class ProjectNormsControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final int IGNORE_GEN_ID = -1;
	private static final int IGNORE_SPEC_ID = -2;
	private static final int EMPTY_GEN_ID = -3;
	private static final int EMPTY_SPEC_ID = -4;

	private static final NormGroupObj emptySelectGenNormGroup = new NormGroupObj();
	static {
		emptySelectGenNormGroup.setId(EMPTY_GEN_ID);
		emptySelectGenNormGroup.setType(NormGroupObj.GEN_POP);
		emptySelectGenNormGroup.setValue("Select");
		emptySelectGenNormGroup.setSelected(true);
	}
	private static final NormGroupObj emptySelectSpecNormGroup = new NormGroupObj();
	static {
		emptySelectSpecNormGroup.setId(EMPTY_SPEC_ID);
		emptySelectSpecNormGroup.setType(NormGroupObj.SPEC_POP);
		emptySelectSpecNormGroup.setValue("Select");
		emptySelectSpecNormGroup.setSelected(true);
	}
	private static final NormGroupObj ignoreGenNormGroup = new NormGroupObj();
	static {
		ignoreGenNormGroup.setId(IGNORE_GEN_ID);
		ignoreGenNormGroup.setType(NormGroupObj.GEN_POP);
		ignoreGenNormGroup.setValue("Select");
		ignoreGenNormGroup.setSelected(true);
	}
	private static final NormGroupObj ignoreSpecNormGroup = new NormGroupObj();
	static {
		ignoreSpecNormGroup.setId(IGNORE_SPEC_ID);
		ignoreSpecNormGroup.setType(NormGroupObj.SPEC_POP);
		ignoreSpecNormGroup.setValue("Select");
		ignoreSpecNormGroup.setSelected(true);
	}

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "projectNormsViewBean", value = "#{projectNormsViewBean}")
	private ProjectNormsViewBean projectNormsViewBean;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private ProjectTypeDao projectTypeDao;

	@EJB
	private CourseDao courseDao;

	@EJB
	private InstrumentNormsServiceLocal instrumentNormsServiceLocal;

	Project currentProject;

	@Resource(name = "custom/abyd_properties")
	private Properties abydProperties;

	public void initWithParticipants(ParticipantObj[] selectedParticipants) {

		// convert selected participants to ParticipantObj
		List<Participant> participants = new ArrayList<Participant>();
		for (ParticipantObj p : selectedParticipants) {
			Participant participant = new Participant();
			// only set the id because we're fetching courses and
			// their norms
			participant.setId(p.getUserId());
			participants.add(participant);
		}

		// Initialize Project and course norms list if needed
		if (currentProject == null) {
			currentProject = getSessionBean().getProject();
		}

		// set up project info
		projectNormsViewBean.setProjectName(currentProject.getName());
		String projectTypeName = projectTypeDao.findById(currentProject.getProjectType().getProjectTypeId()).getName();
		projectNormsViewBean.setProjectTypeName(projectTypeName);

		if (projectNormsViewBean.getCourseNormsList() == null) {
			// Need to set up norm stuff here like in the "real" init() so we
			// can use it in the next for loop
			projectNormsViewBean.setProjectNormsDefaultsObj(new ProjectNormsDefaultsObj());
			List<CourseNormsTripletObj> viewTriplets = projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets();
			CourseNormList cnl = instrumentNormsServiceLocal.getCourseNormList(currentProject.getProjectId());
			projectNormsViewBean.setCourseNormsList(new ArrayList<CourseNormsObj>());
			for (Course c : cnl.getCourses()) {
				CourseNormsObj cObj = toCourseNormsObj(c);
				// set full course name
				cObj.setCourseName(c.getShortName());
				CourseNormsTripletObj triplet = new CourseNormsTripletObj();
				triplet.setCourse(cObj);
				viewTriplets.add(triplet);
				projectNormsViewBean.getCourseNormsList().add(cObj);
			}
		}

		// fetch selected norms list for participants
		FetchSelectedNormsList fetchSelectedNormsList = new FetchSelectedNormsList();
		fetchSelectedNormsList.setProjectId(currentProject.getProjectId());
		fetchSelectedNormsList.setParticipants(participants);
		SelectedNorms selectedNorms = instrumentNormsServiceLocal.fetchSelectedNormsList(fetchSelectedNormsList);

		ProjectNormsDefaults pnd = selectedNorms.getProjectNormsDefaults();
		// if there are participants returned
		// use only first participants info as the set value
		Participant p = selectedNorms.getParticipants().get(0);

		// MAKE MAPS
		// - course abbv to project default norms Map
		Map<String, CourseNormsObj> defaultCourseNormMap = new HashMap<String, CourseNormsObj>();
		for (Course cc : pnd.getCourses()) {
			defaultCourseNormMap.put(cc.getAbbv(), toCourseNormsObj(cc));
		}
		// - course abbv to participant override norms Map
		Map<String, CourseNormsObj> overrideCourseNormMap = new HashMap<String, CourseNormsObj>();
		for (Course cc : p.getCourses()) {
			overrideCourseNormMap.put(cc.getAbbv(), toCourseNormsObj(cc));
		}

		// - course abbv to courseTriplet Map
		Map<String, CourseNormsTripletObj> courseTripletMap = new HashMap<String, CourseNormsTripletObj>();
		for (CourseNormsTripletObj t : projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets()) {
			courseTripletMap.put(t.getCourse().getAbbv(), t);
		}

		// END MAKE MAPS

		// find empty/ignore and remove them
		removeEmptyIgnore();

		/*note:  mbp:  re: per NHN-1359 Issue: When norms are selected for the project and then 
		 * you select more than one Participant (but less than all participants), the norm values default to "Select" 
		 * instead of the norm selected at the project level. They should show the value selected at the project level. 
		 * Note: This works correctly if no Participants are pre-selected (i.e., this means all Participants in 
		 * the project are selected), or if only one participant is selected and you use either the 
		 * Set Project Norms button or the clipboard icon. 
		 * 
		 * I have only commented out the if/else, in case someone decides they want this to work differently.
		 */

		// if (participants.size() == 1) {
		// display info relevant to a single participant
		for (CourseNormsObj cObj : projectNormsViewBean.getCourseNormsList()) {
			String courseAbbv = cObj.getAbbv();
			CourseNormsTripletObj t = courseTripletMap.get(courseAbbv);
			NormGroupObj selectedGenNormGroup = findSelectedNormGroup(courseAbbv, NormGroupObj.GEN_POP, cObj,
					overrideCourseNormMap, defaultCourseNormMap);
			NormGroupObj selectedSpecNormGroup = findSelectedNormGroup(courseAbbv, NormGroupObj.SPEC_POP, cObj,
					overrideCourseNormMap, defaultCourseNormMap);

			t.setSelectedGenNormGroup(selectedGenNormGroup);
			t.setSelectedSpecNormGroup(selectedSpecNormGroup);
			// System.out.println("participant triplet:" + t.toString());
		}
		/*		} else {

					// insert blanks for all drop-down values when multi participant
					for (CourseNormsObj cObj : projectNormsViewBean.getCourseNormsList()) {
						String courseAbbv = cObj.getAbbv();
						CourseNormsTripletObj t = courseTripletMap.get(courseAbbv);
						// auto-fill value if there is only one option
						NormGroupObj onlyOption = cObj.getOnlyOption(NormGroupObj.GEN_POP);
						if (onlyOption != null) {
							t.setSelectedGenNormGroup(onlyOption);
						} else {
							cObj.getNormGroups().add(ignoreGenNormGroup);
							t.setSelectedGenNormGroup(ignoreGenNormGroup);
						}

						// auto-fill value if there is only one option
						onlyOption = cObj.getOnlyOption(NormGroupObj.SPEC_POP);
						if (onlyOption != null) {
							t.setSelectedSpecNormGroup(onlyOption);
						} else if (!cObj.isSpecPopNormListEmpty()) {
							cObj.getNormGroups().add(ignoreSpecNormGroup);
							t.setSelectedSpecNormGroup(ignoreSpecNormGroup);
							System.out.println("participant triplet:" + t.toString());
						}

					}
				} */

	}

	public void init() {
		currentProject = getSessionBean().getProject();

		// set up project info
		projectNormsViewBean.setProjectName(currentProject.getName());
		String projectTypeName = projectTypeDao.findById(currentProject.getProjectType().getProjectTypeId()).getName();
		projectNormsViewBean.setProjectTypeName(projectTypeName);

		// set up project norms defaults
		projectNormsViewBean.setProjectNormsDefaultsObj(new ProjectNormsDefaultsObj());
		List<CourseNormsTripletObj> viewTriplets = projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets();

		CourseNormList cnl = instrumentNormsServiceLocal.getCourseNormList(currentProject.getProjectId());
		projectNormsViewBean.setCourseNormsList(new ArrayList<CourseNormsObj>());

		for (Course c : cnl.getCourses()) {

			CourseNormsObj cObj = toCourseNormsObj(c);
			// set full course name
			cObj.setCourseName(c.getShortName());

			CourseNormsTripletObj triplet = new CourseNormsTripletObj();
			triplet.setCourse(cObj);
			viewTriplets.add(triplet);
			projectNormsViewBean.getCourseNormsList().add(cObj);
		}

		// find empty emptySelect*NormGroup and remove them
		removeEmptyIgnore();

		// set selected norms for courses
		FetchSelectedNormsList fetchSelectedNormsList = new FetchSelectedNormsList();
		fetchSelectedNormsList.setProjectId(currentProject.getProjectId());
		SelectedNorms selectedNorms = instrumentNormsServiceLocal.fetchSelectedNormsList(fetchSelectedNormsList);
		if (selectedNorms == null) {
			RequestContext requestContext = RequestContext.getCurrentInstance();
			FacesContext facesContext = FacesContext.getCurrentInstance();
			requestContext.addCallbackParam("notValid", true);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Project Norms Error", ""));
			// Return added... If we don't we get a NullPointerException in the
			// first reference to selectedNorms
			return;
		}
		// make course abbv to course Norms Object map
		Map<String, CourseNormsObj> defaultCourseNormMap = new HashMap<String, CourseNormsObj>();
		for (Course cc : selectedNorms.getProjectNormsDefaults().getCourses()) {
			defaultCourseNormMap.put(cc.getAbbv(), toCourseNormsObj(cc));
		}
		// set selected norms for courses
		for (CourseNormsObj cObj : projectNormsViewBean.getCourseNormsList()) {
			// set selected norm
			// - for General Population
			NormGroupObj selectedGenNorm = foobar(cObj, defaultCourseNormMap, NormGroupObj.GEN_POP);

			// - for Specific Population
			NormGroupObj selectedSpecNorm = foobar(cObj, defaultCourseNormMap, NormGroupObj.SPEC_POP);

			// update triplet
			Integer updateIndex = null;
			viewTriplets = projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets();
			for (CourseNormsTripletObj cno : viewTriplets) {
				if (cno.getCourse().getAbbv().equals(cObj.getAbbv())) {
					updateIndex = viewTriplets.indexOf(cno);
				}
			}

			// copy over course from list to projectNorms default
			// along with selected spec and gen norms
			CourseNormsTripletObj t = viewTriplets.get(updateIndex);
			t.setSelectedGenNormGroup(selectedGenNorm);

			t.setSelectedSpecNormGroup(selectedSpecNorm);
			if (selectedSpecNorm != null && !selectedSpecNorm.areValuesNull()) {
				System.out.println("project triplet:" + t.toString());
			} else {
				System.out.println("project triplet: empty");
			}
		}

	}

	private void removeEmptyIgnore() {
		for (CourseNormsObj cObj : projectNormsViewBean.getCourseNormsList()) {
			List<NormGroupObj> ng = cObj.getNormGroups();
			List<NormGroupObj> ngRemoval = new ArrayList<NormGroupObj>();
			for (NormGroupObj ngo : ng) {
				if (ngo.getId() < 0) {
					ngRemoval.add(ngo);
				}
			}
			for (NormGroupObj ngo : ngRemoval) {
				ng.remove(ngo);
			}
		}
	}

	private NormGroupObj foobar(CourseNormsObj cObj, Map<String, CourseNormsObj> defaultCourseNormMap, String type) {

		NormGroupObj selectedNorm = null;
		try {
			CourseNormsObj course = defaultCourseNormMap.get(cObj.getAbbv());
			selectedNorm = course.getSelectedNormByType(type);

		} catch (NullPointerException e) {
			// if there is only one norm option then automatically select it
			if (cObj.getNormGroupByType(type).size() == 1) {
				selectedNorm = cObj.getNormGroupByType(type).get(0);
			}
		}
		// if the selected norm is not found then make an return an empty one
		if (selectedNorm == null) {
			if (type == NormGroupObj.GEN_POP) {
				cObj.getNormGroups().add(emptySelectGenNormGroup);
				selectedNorm = emptySelectGenNormGroup;
			} else {
				if (!cObj.isSpecPopNormListEmpty()) {
					cObj.getNormGroups().add(emptySelectSpecNormGroup);
					selectedNorm = emptySelectSpecNormGroup;
				}
			}
		}
		return selectedNorm;
	}

	public void saveProjectWideNorms() {
		boolean validCourses = checkCourses();
		if (!validCourses) {
			return;
		}
		if (currentProject == null) {
			currentProject = getSessionBean().getProject();
		}
		ProjectNormsDefaults projectNormsDefaults = new ProjectNormsDefaults();
		projectNormsDefaults.setProjectId(currentProject.getProjectId());
		List<Course> courses = viewBeanTripletsToCourses();
		projectNormsDefaults.setCourses(courses);
		instrumentNormsServiceLocal.setProjectNormDefaults(projectNormsDefaults);
	}

	public void saveParticipantNorms(ParticipantObj[] ppl) {
		boolean validCourses = checkCourses();
		if (!validCourses) {
			return;
		}
		// set the course-norms for each of the participants
		List<Participant> participants = new ArrayList<Participant>();
		List<Course> courses = viewBeanTripletsToCourses();
		// remove courses that have only blank*NormGroups
		ArrayList<Course> emptyCourseList = new ArrayList<Course>();
		// - find empty courses
		for (Course c : courses) {
			if (c.getNormGroups().size() == 0)
				emptyCourseList.add(c);
		}
		// - remove empty courses
		for (Course c : emptyCourseList) {
			courses.remove(c);
		}

		for (ParticipantObj p : ppl) {
			Participant participant = new Participant();
			participant.setId(p.getUserId());
			participant.setCourses(courses);
			participants.add(participant);
		}

		if (currentProject == null) {
			currentProject = getSessionBean().getProject();
		}
		SetParticipantNormsList setParticipantNormsList = new SetParticipantNormsList();
		setParticipantNormsList.setProject(currentProject.getProjectId());
		setParticipantNormsList.setParticipants(participants);

		instrumentNormsServiceLocal.setParticipantNorms(setParticipantNormsList);
	}

	/*
	 * Helper functions
	 */

	// make sure that all courses have a general/special population norms pair
	private boolean checkCourses() {
		for (CourseNormsTripletObj triplet : projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets()) {
			NormGroupObj ngGen = triplet.getSelectedGenNormGroup();
			NormGroupObj ngSpec = triplet.getSelectedSpecNormGroup();
			RequestContext requestContext = RequestContext.getCurrentInstance();
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String courseName = triplet.getCourse().getCourseName();
			String errorMsg = "Norms must be set for both General and Special Populations: ";

			// gen is empty and spec is not
			if (ngGen.getId() == EMPTY_GEN_ID && ngSpec.getId() != EMPTY_SPEC_ID) {
				requestContext.addCallbackParam("notValid", true);
				facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg
						+ "Need to add Special Norm for course:" + courseName, ""));
				return false;
			}

			// spec is empty and gen is not and spec list size >0
			// if special population norms list size is > 0
			if (!triplet.getCourse().isSpecPopNormListEmpty() && ngSpec.getId() != null) {
				if (ngGen.getId() != EMPTY_GEN_ID && ngSpec.getId() == EMPTY_SPEC_ID) {
					requestContext.addCallbackParam("notValid", true);
					facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg
							+ "Need to add General Norm for course:" + courseName, ""));
					return false;
				}
			}

		}
		return true;
	}

	// returns which norm to use given the course and type
	// if there are participants norm group overrides then use 'em else
	// if there are default norm group overrides then use 'em else
	// return an empty norm group
	private NormGroupObj findSelectedNormGroup(String courseAbbv, String type, CourseNormsObj courseNormsObj,
			Map<String, CourseNormsObj> overrideCourseNormMap, Map<String, CourseNormsObj> defaultCourseNormMap) {

		// if course has only one option then auto select it
		NormGroupObj onlyOption = courseNormsObj.getOnlyOption(type);
		if (onlyOption != null) {
			return onlyOption;
		}
		// return empty if type = special pop and spec pop norm list is empty
		if (type.equals(NormGroupObj.SPEC_POP) && courseNormsObj.isSpecPopNormListEmpty()) {
			return null;
		}
		CourseNormsObj overrideCourse = null;
		CourseNormsObj defaultCourse = null;
		NormGroupObj overrideNorm = null;
		NormGroupObj defaultNorm = null;

		try {
			overrideCourse = overrideCourseNormMap.get(courseAbbv);
			overrideNorm = overrideCourse.getNormGroupByType(type).get(0);
		} catch (NullPointerException e) {
			overrideCourse = new CourseNormsObj();
		} catch (IndexOutOfBoundsException e) {
			overrideNorm = null;
		}

		try {
			defaultCourse = defaultCourseNormMap.get(courseAbbv);
			defaultNorm = defaultCourse.getNormGroupByType(type).get(0);
		} catch (NullPointerException e) {
			defaultCourse = new CourseNormsObj();
		} catch (IndexOutOfBoundsException e) {
			defaultNorm = null;
		}

		if (overrideNorm != null) {
			return overrideNorm;
		} else if (defaultNorm != null) {
			return defaultNorm;
		} else {
			if (type.equals(NormGroupObj.GEN_POP)) {
				courseNormsObj.getNormGroups().add(emptySelectGenNormGroup);
				return emptySelectGenNormGroup;
			} else if (type.equals(NormGroupObj.SPEC_POP) && !courseNormsObj.isSpecPopNormListEmpty()) {
				courseNormsObj.getNormGroups().add(emptySelectSpecNormGroup);
				return emptySelectSpecNormGroup;
			} else
				return null;
		}
	}

	private Participant toParticipant(ParticipantObj po) {
		List<Course> courses = new ArrayList<Course>();
		for (CourseNormsObj co : po.getCourseNorms()) {
			courses.add(toCourse(co));
		}

		Participant p = new Participant();
		p.setId(po.getUserId());
		p.setCourses(courses);
		return p;
	}

	private Course toCourse(CourseNormsObj co) {
		Course c = new Course();
		c.setAbbv(co.getAbbv());
		List<NormGroup> normGroups = new ArrayList<NormGroup>();
		for (NormGroupObj ngo : co.getNormGroups()) {
			normGroups.add(toNormGroup(ngo));
		}
		c.setNormGroups(normGroups);
		return c;
	}

	// gets viewBean triplets' courses and maps courses to the selected gen/spec
	// norm groups
	private List<Course> viewBeanTripletsToCourses() {
		List<Course> courses = new ArrayList<Course>();
		for (CourseNormsTripletObj triplet : projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets()) {
			// get selected norm group
			// general/special population
			List<NormGroup> normGroups = new ArrayList<NormGroup>();

			// gen pop
			NormGroup ng = toNormGroup(triplet.getSelectedGenNormGroup());
			// skip ERROR_ID/ IGNORE_ID
			if (ng.getId() != EMPTY_GEN_ID && ng.getId() != IGNORE_GEN_ID) {
				normGroups.add(ng);
			}

			// if the spec norm list size > 0 and there are values for selected
			// spec norm group
			if (!triplet.getCourse().isSpecPopNormListEmpty() && !triplet.getSelectedSpecNormGroup().areValuesNull()) {
				// spec pop
				ng = toNormGroup(triplet.getSelectedSpecNormGroup());
				// skip ERROR_ID/ IGNORE_ID
				if (ng.getId() != EMPTY_SPEC_ID && ng.getId() != IGNORE_SPEC_ID)
					normGroups.add(ng);
			}
			Course course = new Course();
			course.setAbbv(triplet.getCourse().getAbbv());
			course.setNormGroups(normGroups);

			courses.add(course);
		}
		return courses;
	}

	private NormGroup toNormGroup(NormGroupObj normGroupObj) {
		NormGroup normGroup = new NormGroup();
		normGroup.setId(normGroupObj.getId());
		normGroup.setSelected(normGroupObj.getIsSelected());
		normGroup.setValue(normGroupObj.getValue());
		normGroup.setType(normGroupObj.getType());
		return normGroup;
	}

	// private Course toCourse(CourseNormsObj courseNormsObj) {
	// Course course = new Course();
	// course.setAbbv(courseNormsObj.getAbbv());
	// List<NormGroup> normGroups = new ArrayList<NormGroup>();
	// for (NormGroupObj ngo : courseNormsObj.getNormGroups()) {
	// normGroups.add(toNormGroup(ngo));
	// }
	// course.setNormGroups(normGroups);
	// return course;
	// }

	private CourseNormsObj toCourseNormsObj(Course course) {
		CourseNormsObj courseNormsObj = new CourseNormsObj();
		courseNormsObj.setAbbv(course.getAbbv());

		// traverse course -> normGroup and convert to their
		// Obj forms
		List<NormGroupObj> normGroupObjList = new ArrayList<NormGroupObj>();
		for (NormGroup normGroup : course.getNormGroups()) {
			// convert norm groups
			NormGroupObj normGroupObj = new NormGroupObj();
			normGroupObj.setId(normGroup.getId());
			normGroupObj.setSelected(normGroup.getIsSelected());
			if (!normGroup.getValue().equals(""))
				normGroupObj.setValue(normGroup.getValue());
			else {
				String value = getNormName(course.getAbbv(), normGroup.getId(), normGroup.getType());
				normGroupObj.setValue(value);
			}

			normGroupObj.setType(normGroup.getType());
			normGroupObjList.add(normGroupObj);
		}
		courseNormsObj.setNormGroups(normGroupObjList);

		return courseNormsObj;
	}

	private String getNormName(String courseAbbv, int id, String type) {
		List<CourseNormsObj> courses = projectNormsViewBean.getCourseNormsList();
		for (CourseNormsObj course : courses) {
			if (course.getAbbv().equals(courseAbbv)) {
				for (NormGroupObj ng : course.getNormGroupByType(type)) {
					if (ng.getId() == id) {
						return ng.getValue();
					}
				}
			}
		}
		return "";
	}

	public Boolean hasProduct(String productCode) {
		for (CompanyProduct companyProduct : sessionBean.getCompany().getCompanyProducts()) {
			if (companyProduct.getProductCode().equalsIgnoreCase(productCode.toLowerCase())) {
				return true;
			}
		}

		return false;
	}

	/*
	 * Getter / Setter
	 */
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ProjectNormsViewBean getProjectNormsViewBean() {
		return projectNormsViewBean;
	}

	public void setProjectNormsViewBean(ProjectNormsViewBean projectNormsViewBean) {
		this.projectNormsViewBean = projectNormsViewBean;
	}

	public ProjectCourseDao getProjectCourseDao() {
		return projectCourseDao;
	}

	public void setProjectCourseDao(ProjectCourseDao projectCourseDao) {
		this.projectCourseDao = projectCourseDao;
	}

	public ProjectTypeDao getProjectTypeDao() {
		return projectTypeDao;
	}

	public void setProjectTypeDao(ProjectTypeDao projectTypeDao) {
		this.projectTypeDao = projectTypeDao;
	}

	public CourseDao getCourseDao() {
		return courseDao;
	}

	public void setCourseDao(CourseDao courseDao) {
		this.courseDao = courseDao;
	}

	public InstrumentNormsServiceLocal getInstrumentNormsServiceLocal() {
		return instrumentNormsServiceLocal;
	}

	public void setInstrumentNormsServiceLocal(InstrumentNormsServiceLocal instrumentNormsServiceLocal) {
		this.instrumentNormsServiceLocal = instrumentNormsServiceLocal;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

	public Properties getAbydProperties() {
		return abydProperties;
	}

	public void setAbydProperties(Properties abydProperties) {
		this.abydProperties = abydProperties;
	}

}
