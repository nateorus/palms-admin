package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RealmFilter;
import com.pdinh.data.CreateUserServiceLocal;
import com.pdinh.data.UsernameServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.helper.LazySubjectDataModel;
import com.pdinh.presentation.view.LinkedAccountViewBean;

@ManagedBean(name = "linkedAccountControllerBean")
@ViewScoped
public class LinkedAccountControllerBean implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LinkedAccountControllerBean.class);
	private boolean firstTime = true;
	@ManagedProperty(name = "linkedAccountViewBean", value = "#{linkedAccountViewBean}")
	private LinkedAccountViewBean linkedAccountViewBean;

	@Resource(mappedName="application/config_properties")
    private Properties properties;
	
	@EJB
	private PalmsRealmDao realmDao;
	
	@EJB
	private UserDao userDao;
	
	@EJB
	private CompanyDao companyDao;
	
	@EJB
	private UsernameServiceLocal usernameService;
	
	@EJB
	private CreateUserServiceLocal createUserService;
	
	@EJB
	private PalmsSubjectDao subjectDao;
	
	@EJB
	private ExternalSubjectServiceLocal subjectService;
	
	public void initView(){
		log.debug("Handling linked account dialog init");
		log.debug("Selected User: {}", linkedAccountViewBean.getSelectedUser().getUsername());
    	
    	linkedAccountViewBean.setSelectedLinkOption(null);
    	linkedAccountViewBean.setLinkRadioSubmitDisabled(true);
    	linkedAccountViewBean.setSaveLinkedAccountButtonDisabled(true);
    	linkedAccountViewBean.setUsersIdInput("");
    	linkedAccountViewBean.setUsersIdInputDisabled(true);
    	
    	PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(linkedAccountViewBean.getSelectedUser().getUsername(), linkedAccountViewBean.getSelectedUser().getRealm_id());
    	if (!(subj == null)){
    		log.debug("found subject entity: {}, {}", subj.getSubject_id(), subj.getPrincipal());
	    	if (!(subj.getLinked_id() == 0)){
	    		User user = userDao.findById(subj.getLinked_id());
	    		
	    		linkedAccountViewBean.setExistingLinkedUser(user);
	    		linkedAccountViewBean.setLinkRadioDisabled(true);
	    		linkedAccountViewBean.setRefreshAccountButtonDisabled(false);
	    		linkedAccountViewBean.setUnlinkAccountButtonDisabled(false);
	    		FacesContext facesContext = FacesContext.getCurrentInstance();
		    	facesContext.addMessage(null, new FacesMessage("Subject " + subj.getPrincipal() + " is linked to  " + user.getEmail() + "(" + user.getUsersId() + ")", "You may refresh or Unlink"));
	    	}else{
	    		linkedAccountViewBean.setLinkRadioDisabled(false);
	    		linkedAccountViewBean.setRefreshAccountButtonDisabled(true);
	    		linkedAccountViewBean.setUnlinkAccountButtonDisabled(true);
	    		linkedAccountViewBean.setLookupByEmailButtonDisabled(true);
	    	}
    	}else{
    		linkedAccountViewBean.setLinkRadioDisabled(false);
    		linkedAccountViewBean.setRefreshAccountButtonDisabled(true);
    		linkedAccountViewBean.setUnlinkAccountButtonDisabled(true);
    		linkedAccountViewBean.setLookupByEmailButtonDisabled(true);
    	}
	}
	public void handleUnlinkAccount(boolean deactivate){
		log.debug("Handling unlinking account");
		PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(linkedAccountViewBean.getSelectedUser().getUsername(), linkedAccountViewBean.getSelectedUser().getRealm_id());
		User user = userDao.findById(subj.getLinked_id());
		user.setExternallyManaged(false);
		if (deactivate){
			user.setActive((short)0);
			user.setEmail(user.getEmail() + "_disabled");
			user.setUsername(user.getUsername() + "_disabled");
		}
		userDao.update(user);
		subj.setLinked_id(0);
		subjectDao.update(subj);
		linkedAccountViewBean.setExistingLinkedUser(null);
		linkedAccountViewBean.setLookupByEmailButtonDisabled(false);
		linkedAccountViewBean.setRefreshAccountButtonDisabled(true);
		linkedAccountViewBean.setUnlinkAccountButtonDisabled(true);
		linkedAccountViewBean.setLinkRadioSubmitDisabled(true);
		linkedAccountViewBean.setSaveLinkedAccountButtonDisabled(true);
		linkedAccountViewBean.setLinkRadioDisabled(false);
		log.debug("finished unlinking user {} from {}", user.getUsersId(), subj.getPrincipal());
		FacesContext facesContext = FacesContext.getCurrentInstance();
    	facesContext.addMessage(null, new FacesMessage("Subject " + subj.getPrincipal() + " has been unlinked from user " + user.getUsersId(), "You may now link to a different user."));
	}
	public void handleRefreshLinkedAccount(){
		log.debug("Handle Refresh Linked account here");
	}
	public void handleSelectLinkOption(){
		if (linkedAccountViewBean.getSelectedLinkOption().equalsIgnoreCase("1")){
			linkedAccountViewBean.setSaveLinkedAccountButtonDisabled(false);
    	}else if (linkedAccountViewBean.getSelectedLinkOption().equalsIgnoreCase("2")){
    		linkedAccountViewBean.setLookupByEmailButtonDisabled(false);
    		linkedAccountViewBean.setUsersIdInputDisabled(false);
    		
    	}
	}
	
	public void handleIdUpdate(ValueChangeEvent e){
		log.debug("New Value: {}", e.getNewValue());
		try{
			int userid = Integer.valueOf(e.getNewValue().toString());
			if (e.getNewValue().toString().length() > 5){
				linkedAccountViewBean.setSaveLinkedAccountButtonDisabled(false);
			}else{
				FacesContext facesContext = FacesContext.getCurrentInstance();
		    	facesContext.addMessage(null, new FacesMessage("Not a valid users_id value", e.getNewValue() + " is an invalid value"));
			}
		}catch (NumberFormatException ne){
			FacesContext facesContext = FacesContext.getCurrentInstance();
	    	facesContext.addMessage(null, new FacesMessage("Please enter a numeric users_id value", e.getNewValue() + " is an invalid value"));
		}
		
	}
	public void lookupByEmail(){
		ExternalSubjectDataObj selectedUser = linkedAccountViewBean.getSelectedUser();
    	PalmsRealm realm = realmDao.findById(selectedUser.getRealm_id());
    	List<User> user = userDao.findByEmailAndCompany(selectedUser.getEmail(), realm.getLinked_company_id());
    	
    	if (user.isEmpty()){
    		FacesContext facesContext = FacesContext.getCurrentInstance();
        	facesContext.addMessage(null, new FacesMessage("No user found with email " + selectedUser.getEmail() + " in company " + realm.getLinked_company_id()));
        	
    	}else if (user.size() > 1){
    		String userids = "";
    		for (User u:user){
    			userids = userids + " " + u.getUsersId();
    		}
    		FacesContext facesContext = FacesContext.getCurrentInstance();
    		
        	facesContext.addMessage(null, new FacesMessage("Multiple users found: " + userids));
        	
    	}else{
    		linkedAccountViewBean.setUsersIdInput(String.valueOf(user.get(0).getUsersId()));
    		linkedAccountViewBean.setLinkRadioSubmitDisabled(false);
    		linkedAccountViewBean.setSaveLinkedAccountButtonDisabled(false);
    		FacesContext facesContext = FacesContext.getCurrentInstance();
    		
        	facesContext.addMessage(null, new FacesMessage("Found User: " + user.get(0).getUsersId() + " with email: " + user.get(0).getEmail()));
    	}
	}
	public void handleCreateLinkedAccount(){
		log.debug("In handleSubmitLinkSubject");
    	String selectedLinkOption = linkedAccountViewBean.getSelectedLinkOption();
    	PalmsRealm realm = realmDao.findById(linkedAccountViewBean.getSelectedUser().getRealm_id());
    	log.debug("Selected Link Option {}", selectedLinkOption);
    	if (selectedLinkOption.equalsIgnoreCase("1")){
    		
    		int company_id = realm.getLinked_company_id();
    		Company company = companyDao.findById(company_id);
    		ExternalSubjectDataObj user = linkedAccountViewBean.getSelectedUser();
    		User palms_user = new User();
    		palms_user.setFirstname(user.getFirstname());
    		palms_user.setLastname(user.getLastname());
    		palms_user.setEmail(user.getEmail());
    		palms_user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(), user.getLastname()));
    		palms_user.setExternallyManaged(true);
    		palms_user.setMserverid((short) 1);
    		palms_user.setType(1);
    		
    		if (userDao.findByEmailAndCompany(user.getEmail(), company_id).isEmpty()){
	    		palms_user = createUserService.createNewUser(palms_user, company, null, null, null);
	    		log.debug("Created New User with id: {}", palms_user.getUsersId());
	    		PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(linkedAccountViewBean.getSelectedUser().getUsername(), realm.getRealm_id());
	    		subj.setLinked_id(palms_user.getUsersId());
	    		subjectDao.update(subj);
	    		log.debug("Updated Link to subject id {}", subj.getSubject_id());
	    		FacesContext facesContext = FacesContext.getCurrentInstance();
		    	facesContext.addMessage(null, new FacesMessage("Created externally managed User " + palms_user.getUsersId() + " in company " + company_id, "Updates to this record are limited"));

    		}else{
    			FacesContext facesContext = FacesContext.getCurrentInstance();
		    	facesContext.addMessage(null, new FacesMessage("A user with email " + user.getEmail() + " already exists in company " + company_id, "Link to this user or change the user email"));
    		}
    		
    	}else if (selectedLinkOption.equalsIgnoreCase("2")){
    		log.debug("Lookup existing user here");
    		int UserId = Integer.valueOf(linkedAccountViewBean.getUsersIdInput());
    		User user = userDao.findById(UserId);
    		ExternalSubjectDataObj realmUser = linkedAccountViewBean.getSelectedUser();
    		user.setFirstname(realmUser.getFirstname());
    		user.setLastname(realmUser.getLastname());
    		user.setEmail(realmUser.getEmail());
    		user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(), user.getLastname()));
    		user.setPassword(null);
    		user.setHashedpassword(createUserService.generateHashedPassword(createUserService.getNewPassword(), user
    				.getRowguid().substring(0, 10).toUpperCase()));
    		user.setExternallyManaged(true);
    		userDao.update(user);
    		PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(linkedAccountViewBean.getSelectedUser().getUsername(), realm.getRealm_id());
    		subj.setLinked_id(user.getUsersId());
    		subj = subjectDao.update(subj);
    		FacesContext facesContext = FacesContext.getCurrentInstance();
	    	facesContext.addMessage(null, new FacesMessage("Created Link to existing user " + user.getEmail() + "(" + user.getUsersId() + ")", "User is now externally managed"));
    	}
    	
	}
	public void handleRowSelect(SelectEvent e){
		log.debug("Handling Select Event {}", e.getSource());
	}
	public void handleSelectPlatformUser(){
		log.debug("In Handle select platform user");
		linkedAccountViewBean.setUsersIdInput(String.valueOf(linkedAccountViewBean.getSelectedSubject().getExternal_id()));
		linkedAccountViewBean.setSaveLinkedAccountButtonDisabled(false);
	}
	public void initSubjectListDlg(){
		PalmsRealm palmsRealm = realmDao.findRealmByName(this.properties.getProperty("primaryRealmName"));
		//String filter = "select @ from users u, company c where u.company_id = c.company_id and u.company_id = '" + String.valueOf(palmsRealm.getLinked_company_id()) + "' and u.externally_managed=0 and u.active=1";
		PalmsRealm clientRealm = realmDao.findRealmByName(this.properties.getProperty("clientRealmName"));
		List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();
		
		RealmFilter realmFilter = new RealmFilter();
		for (RealmFilter filter : clientRealm.getFilters()){
			if (filter.getCode().equalsIgnoreCase("FINDBYCO")){
				realmFilter = filter;
				break;

			}
		}
		subjects = subjectService.getExternalSubjects(realmFilter, new Object[]{String.valueOf(palmsRealm.getLinked_company_id())}, false);
		//List<ExternalSubjectDataObj> subjects = subjectService.getExternalSubjectsPage(clientRealm, filter, false, 1, 10);
		LazySubjectDataModel model = new LazySubjectDataModel(subjects);
		model.setFilterDef(realmFilter);
		model.setParameters(new Object[]{String.valueOf(palmsRealm.getLinked_company_id())});
		model.setQueryFilter(realmFilter.getFilter().replaceFirst("\\{\\}", String.valueOf(palmsRealm.getLinked_company_id())));
		model.setExternalSubjectService(subjectService);
		linkedAccountViewBean.setLazyModel(model);
	}
	public boolean isFirstTime() {
		return firstTime;
	}
	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}
	public LinkedAccountViewBean getLinkedAccountViewBean() {
		return linkedAccountViewBean;
	}
	public void setLinkedAccountViewBean(LinkedAccountViewBean linkedAccountViewBean) {
		this.linkedAccountViewBean = linkedAccountViewBean;
	}

}
