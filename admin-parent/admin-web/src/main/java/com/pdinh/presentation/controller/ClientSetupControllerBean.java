package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.component.tabview.Tab;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.helper.TeammemberHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.AddTeammemberViewBean;
import com.pdinh.presentation.view.ClientSetupViewBean;

@ManagedBean
@ViewScoped
public class ClientSetupControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ClientSetupControllerBean.class);

	private Company currentClient;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private LanguageSingleton languageSingleton;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "clientSetupViewBean", value = "#{clientSetupViewBean}")
	private ClientSetupViewBean clientSetupViewBean;

	@ManagedProperty(name = "addTeammemberControllerBean", value = "#{addTeammemberControllerBean}")
	private AddTeammemberControllerBean addTeammemberControllerBean;

	@ManagedProperty(name = "addTeammemberViewBean", value = "#{addTeammemberViewBean}")
	private AddTeammemberViewBean addTeammemberViewBean;

	@ManagedProperty(name = "teammemberHelper", value = "#{teammemberHelper}")
	private TeammemberHelper teammemberHelper;

	private Boolean firstTime = true;

	private boolean clientRolesListExist = false;
	private boolean internalRolesListExist = false;
	private List<ExternalSubjectDataObj> internalRolesList;
	private List<ExternalSubjectDataObj> clientRolesList;

	public void initView() {
		if (firstTime) {
			firstTime = false;
			initForNew();

		}
	}

	public void initForNew() {
		// something here
	}

	public void initForEdit() {

		// if (firstTime) {
		// firstTime = false;
		// }

		Subject subject = SecurityUtils.getSubject();

		setCurrentClient(getSessionBean().getCompany());

		if (subject.isPermitted("AssignClientRoles")
				|| subject.isPermitted(EntityType.COMPANY_TYPE + ":AssignClientRoles:" + currentClient.getCompanyId())) {
			clientSetupViewBean.setInternalTeamTabDisabled(false);
			clientSetupViewBean.setClientTeamTabDisabled(false);
		}

		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Company: {} {}", getCurrentClient().getCoName(), getCurrentClient()
				.getCompanyId());

		clientSetupViewBean.setClientId(getCurrentClient().getCompanyId());
		clientSetupViewBean.setClientName(getCurrentClient().getCoName());
		clientSetupViewBean.setLanguageCode(getCurrentClient().getLangPref());

		clientSetupViewBean.setMainContact(getCurrentClient().getMainContact());
		clientSetupViewBean.setMainPhone(getCurrentClient().getMainPhone());
		clientSetupViewBean.setAddress1(getCurrentClient().getCoaddress());
		clientSetupViewBean.setAddress1(getCurrentClient().getCoaddress());
		clientSetupViewBean.setAddress2(getCurrentClient().getCoaddress2());
		clientSetupViewBean.setCity(getCurrentClient().getCoCity());
		clientSetupViewBean.setState(getCurrentClient().getCoState());
		clientSetupViewBean.setZipCode(getCurrentClient().getCoZip());
		clientSetupViewBean.setCountryCode(getCurrentClient().getCountryIsoCode());

		// Dealing with language drop down
		
		List<LanguageObj> languages = languageSingleton.getLanguages();

		setClientRolesListExist(false);
		setInternalRolesListExist(false);

		clientSetupViewBean.setLanguages(languages);
		clientSetupViewBean.setInternalTeammembers(new ArrayList<ExternalSubjectDataObj>());
		clientSetupViewBean.setClientTeammembers(new ArrayList<ExternalSubjectDataObj>());
		clientSetupViewBean.setRemoveInternalTeammembers(new ArrayList<ExternalSubjectDataObj>());
		clientSetupViewBean.setRemoveClientTeammembers(new ArrayList<ExternalSubjectDataObj>());
	}

	public void handleTabChange(TabChangeEvent event) {
		Tab currentTab = event.getTab();
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> currentTab.getId = {}", currentTab.getId());

		if (currentTab.getId().equalsIgnoreCase("clientTeam")) {
			if (!isClientRolesListExist()) {
				clientSetupViewBean.setClientTeammembers(fetchClientTeammembers());
				setClientRolesList(clientSetupViewBean.getClientTeammembers());
			} else {
				clientSetupViewBean.setClientTeammembers(getClientRolesList());
			}
		} else if (currentTab.getId().equalsIgnoreCase("internalTeam")) {
			if (!isInternalRolesListExist()) {
				clientSetupViewBean.setInternalTeammembers(fetchInternalTeammembers());
				setInternalRolesList(clientSetupViewBean.getInternalTeammembers());
			} else {
				clientSetupViewBean.setInternalTeammembers(getInternalRolesList());
			}
		}

		// TODO AV: We need a better way to do this.
		// saveInternalTeammembers();
		// saveClientTeammembers();

	}

	// Actions
	public void handleInitForNew(ActionEvent event) {
		initForNew();
	}

	public void handleInitForEdit(ActionEvent event) {
		initForEdit();
	}

	public void validateAndSave() {
		save();
		saveInternalTeammembers();
		saveClientTeammembers();
		sessionBean.refreshClient();
	}

	private void save() {
		Company updateClient = getCompanyDao().findById(getCurrentClient().getCompanyId());
		updateClient = setFields(updateClient);
		companyDao.update(updateClient);
	}

	private Company setFields(Company client) {
		client.setLangPref(clientSetupViewBean.getLanguageCode());
		client.setMainContact(clientSetupViewBean.getMainContact());
		client.setMainPhone(clientSetupViewBean.getMainPhone());
		client.setCoaddress(clientSetupViewBean.getAddress1());
		client.setCoaddress2(clientSetupViewBean.getAddress2());
		client.setCoCity(clientSetupViewBean.getCity());
		client.setCoState(clientSetupViewBean.getState());
		client.setCoZip(clientSetupViewBean.getZipCode());
		client.setCountryIsoCode(clientSetupViewBean.getCountryCode());
		return client;
	}

	/* INTERNAL ROLES */
	private void saveInternalTeammembers() {
		teammemberHelper.init(EntityType.COMPANY_TYPE, getCurrentClient().getCompanyId(), "palmsRealmNames",
				EntityRule.ALLOW_CHILDREN);
		teammemberHelper.validateRemoveList(getInternalRolesList(), clientSetupViewBean.getRemoveInternalTeammembers());
		teammemberHelper.removeTeammembers(clientSetupViewBean.getRemoveInternalTeammembers());
		teammemberHelper.saveTeammembers(getInternalRolesList(), getCurrentClient().getCompanyId(), null);

	}

	private List<ExternalSubjectDataObj> fetchInternalTeammembers() {
		setInternalRolesListExist(true);
		return teammemberHelper.fetchTeammembers(EntityType.COMPANY_TYPE, getCurrentClient().getCompanyId(),
				"palmsRealmNames");
	}

	public void handleInitInternalTeammembers(ActionEvent event) {
		addTeammemberControllerBean.initAvailableTeammembers(EntityType.COMPANY_TYPE,
				getCurrentClient().getCompanyId(), "palmsRealmNames", "", false);
	}

	public void handleAddInternalTeammemebers(ActionEvent event) {
		if (addTeammemberControllerBean.validateSelections()) {
			for (ExternalSubjectDataObj subject : addTeammemberViewBean.getSelectedTeammembers()) {
				subject.setCurrentRole(addTeammemberViewBean.getSelectedRole());
			}
			teammemberHelper.finalizeTeammemberList(clientSetupViewBean.getInternalTeammembers(),
					addTeammemberViewBean.getSelectedTeammembers());
			setInternalRolesList(clientSetupViewBean.getInternalTeammembers());
			addTeammemberViewBean.setSelectedTeammembers(null);
		}
	}

	/* CLIENT ROLES */
	private void saveClientTeammembers() {
		teammemberHelper.init(EntityType.COMPANY_TYPE, getCurrentClient().getCompanyId(), "clientRealmName",
				EntityRule.ALLOW_CHILDREN);
		teammemberHelper.validateRemoveList(getClientRolesList(), clientSetupViewBean.getRemoveClientTeammembers());
		teammemberHelper.removeTeammembers(clientSetupViewBean.getRemoveClientTeammembers());
		teammemberHelper.saveTeammembers(getClientRolesList(), getCurrentClient().getCompanyId(), null);
	}

	private List<ExternalSubjectDataObj> fetchClientTeammembers() {
		setClientRolesListExist(true);
		return teammemberHelper.fetchTeammembers(EntityType.COMPANY_TYPE, getCurrentClient().getCompanyId(),
				"clientRealmName");
	}

	public void handleInitClientTeammembers(ActionEvent event) {
		int companyId = getCurrentClient().getCompanyId();
		String filter = "select @ from users u, company c where u.company_id = c.company_id and u.company_id = "
				+ companyId;
		addTeammemberControllerBean.initAvailableTeammembers(EntityType.COMPANY_TYPE, companyId, "clientRealmName",
				filter, true);
	}

	public void handleAddClientTeammemebers(ActionEvent event) {
		if (addTeammemberControllerBean.validateSelections()) {
			for (ExternalSubjectDataObj subject : addTeammemberViewBean.getSelectedTeammembers()) {
				subject.setCurrentRole(addTeammemberViewBean.getSelectedRole());
			}
			teammemberHelper.finalizeTeammemberList(clientSetupViewBean.getClientTeammembers(),
					addTeammemberViewBean.getSelectedTeammembers());
			setClientRolesList(clientSetupViewBean.getClientTeammembers());
			addTeammemberViewBean.setSelectedTeammembers(null);
		}
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ClientSetupViewBean getClientSetupViewBean() {
		return clientSetupViewBean;
	}

	public void setClientSetupViewBean(ClientSetupViewBean clientSetupViewBean) {
		this.clientSetupViewBean = clientSetupViewBean;
	}

	public AddTeammemberControllerBean getAddTeammemberControllerBean() {
		return addTeammemberControllerBean;
	}

	public void setAddTeammemberControllerBean(AddTeammemberControllerBean addTeammemberControllerBean) {
		this.addTeammemberControllerBean = addTeammemberControllerBean;
	}

	public AddTeammemberViewBean getAddTeammemberViewBean() {
		return addTeammemberViewBean;
	}

	public void setAddTeammemberViewBean(AddTeammemberViewBean addTeammemberViewBean) {
		this.addTeammemberViewBean = addTeammemberViewBean;
	}

	public TeammemberHelper getTeammemberHelper() {
		return teammemberHelper;
	}

	public void setTeammemberHelper(TeammemberHelper teammemberHelper) {
		this.teammemberHelper = teammemberHelper;
	}

	public Company getCurrentClient() {
		return currentClient;
	}

	public void setCurrentClient(Company currentClient) {
		this.currentClient = currentClient;
	}

	public boolean isClientRolesListExist() {
		return clientRolesListExist;
	}

	public void setClientRolesListExist(boolean clientRolesListExist) {
		this.clientRolesListExist = clientRolesListExist;
	}

	public boolean isInternalRolesListExist() {
		return internalRolesListExist;
	}

	public void setInternalRolesListExist(boolean internalRolesListExist) {
		this.internalRolesListExist = internalRolesListExist;
	}

	public List<ExternalSubjectDataObj> getInternalRolesList() {
		return internalRolesList;
	}

	public void setInternalRolesList(List<ExternalSubjectDataObj> internalRolesList) {
		this.internalRolesList = internalRolesList;
	}

	public List<ExternalSubjectDataObj> getClientRolesList() {
		return clientRolesList;
	}

	public void setClientRolesList(List<ExternalSubjectDataObj> clientRolesList) {
		this.clientRolesList = clientRolesList;
	}

}
