package com.pdinh.presentation.managedbean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.data.dao.UploadImportJobDao;
import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.dao.PalmsSessionDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsSession;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.helper.Utils;

@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(SessionBean.class);
	private static final long serialVersionUID = 1L;
	private Company company;
	private Project project;
	private List<ProjectObj> projects;
	private User user;
	private ArrayList<ParticipantObj> selectedUsers = new ArrayList<ParticipantObj>();
	// distinct languages for current project
	private HashMap<String, String> projectLanguages;
	private RoleContext roleContext;
	private List<ProjectObj> filteredProjects;
	// private boolean useCompanyPermissions = false;
	// private boolean useProjectPermissions = false;
	private String username;
	private boolean loggedIn;
	private boolean includeClosedProjects = false;
	private boolean reportCenterDisabled = true;
	private int lrmProjectId;
	private int importJobId;

	@Resource(mappedName = "custom/abyd_properties")
	private Properties abydProperties;

	// private String authTargetUrl;

	public Properties getAbydProperties() {
		return abydProperties;
	}

	public void setAbydProperties(Properties abydProperties) {
		this.abydProperties = abydProperties;
	}

	private String templateLevel;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private UserDao userDao;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@EJB
	private PalmsSubjectDao subjectDao;

	@EJB
	private PalmsSessionDao palmsSessionDao;

	@EJB
	private UploadImportJobDao uploadImportJobDao;

	private Integer selectedProject;

	private String mode = "";

	@PostConstruct
	public void init() {
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()) {
			Set<?> realms = subject.getPrincipals().getRealmNames();
			String realm = "";
			if (realms.size() != 1) {
				if (realms.isEmpty()) {
					log.error("No Realms found...Subject Must belong to a Realm: {}", subject.getPrincipal());

				} else {
					log.error("Multiple realms detected...Subject Cannot belong to multiple realms: {}",
							subject.getPrincipal());

				}
			} else {
				Iterator<?> riter = realms.iterator();

				while (riter.hasNext()) {
					realm = (String) riter.next();
					log.debug("Realm: " + realm);
				}
			}
			log.debug("Principal: {}", subject.getPrincipal());
			log.debug("Primary Principal: {}", subject.getPrincipals().getPrimaryPrincipal());
			setSessionPermissions(realm, subject);
		}
	}

	public int getSubjectId() {
		return this.getRoleContext().getSubject().getSubject_id();
	}

	public void setSessionPermissions(String realmName, Subject subject) {
		log.debug("in SetSessionPermissions, SessionBean, with principal {} and realm {}", subject.getPrincipal(),
				realmName);

		PalmsSubject palmsSubject = subjectDao.findSubjectByPrincipalAndRealmName(subject.getPrincipal().toString(),
				realmName);
		RoleContext context = entityService.getDefaultContext(palmsSubject);
		this.setRoleContext(context);
		// Setup any app level permissions here
		this.setLoggedIn(true);
		this.setUsername(subject.getPrincipal().toString());

	}

	public PalmsSession getPalmsSession() {
		Subject subject = SecurityUtils.getSubject();
		return palmsSessionDao.findSessionById(subject.getSession().getId().toString());
	}

	public String handleClickAuthLink() {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation.", new Object[] { subject.getPrincipal(),
				EntityAuditLogger.OP_ACCESS_AUTHORIZATION_SYSTEM });
		setLrmProjectId(0);
		setImportJobId(0);
		return "postTo.xhtml?faces-redirect=true&path=/authorization/faces/dashboard.xhtml&sys=auth";
	}

	public String handleClickLrmLink() {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation.", new Object[] { subject.getPrincipal(),
				EntityAuditLogger.OP_ACCESS_LRM_CLIENT_LIST });
		setLrmProjectId(0);
		setImportJobId(0);
		return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/clientList.xhtml&sys=lrm";
	}

	public String goToDownloadReports(int projectId) {
		if (projectId < 1) {
			return "reportCenter.xhtml?initRepository=true&faces-redirect=true";
		}
		return "reportCenter.xhtml?initRepository=true&projectId=" + String.valueOf(projectId) + "&faces-redirect=true";
	}

	public boolean isProjectContextPermitted(int projectId) {
		Subject subject = SecurityUtils.getSubject();
		boolean[] projectPerms = subject.isPermitted(EntityType.PROJECT_TYPE + ":reportRequestView:" + projectId,
				EntityType.PROJECT_TYPE + ":allowchildren:" + projectId, EntityType.COMPANY_TYPE + ":allowchildren:"
						+ company.getCompanyId(), "reportRequestView", "ViewAllProjects");
		if ((projectPerms[0] && projectPerms[1]) || (projectPerms[1] && projectPerms[3])
				|| (projectPerms[3] && projectPerms[4]) || (projectPerms[2] && projectPerms[3])) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isClientContextPermitted(int companyId) {
		Subject subject = SecurityUtils.getSubject();

		boolean[] companyPerms = subject.isPermitted(EntityType.COMPANY_TYPE + ":reportRequestView:" + companyId,
				EntityType.COMPANY_TYPE + ":allowchildren:" + companyId, "reportRequestView", "ViewAllCompanies");
		if ((companyPerms[0] && companyPerms[1]) || (companyPerms[1] && companyPerms[2])
				|| (companyPerms[2] && companyPerms[3])) {
			return true;
		}
		return false;
	}

	public void updateReportCenterAvailability() {
		Subject subject = SecurityUtils.getSubject();

		if (this.company == null) {
			this.setReportCenterDisabled(true);
			return;
		}

		if (selectedProject != null && user == null) {
			setReportCenterDisabled(!this.isProjectContextPermitted(selectedProject));
		} else {
			if (user != null) {
				// Assume they have permissions on this user if they can see
				// them
				this.setReportCenterDisabled(!subject.isPermitted("reportRequestView"));

			} else {
				// Only company is selected -- check company perms only
				this.setReportCenterDisabled(!isClientContextPermitted(company.getCompanyId()));

			}
		}
	}

	/*
		public String goAuth() {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect(this.getAuthTargetUrl());
				return "success";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "failure";
		}
	*/

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void clearClient() {
		log.debug("Clear Client");
		company = null;
		project = null;
		user = null;
		clearSelectedUsers();
		this.setReportCenterDisabled(true);
	}

	public void clearProject() {
		log.debug("Clear Project");
		project = null;
		selectedProject = null;
		user = null;
	}

	public void clearUser() {
		log.debug("Clear User");
		user = null;
	}

	/**
	 * Currently using userId
	 * 
	 * @param selectedUsers
	 */
	public void setSelectedUsers(ArrayList<ParticipantObj> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public ArrayList<ParticipantObj> getSelectedUsers() {
		return selectedUsers;
	}

	public void addSelectedUser(ParticipantObj user) {
		log.debug("addSelectedUser {}", user);
		for (ParticipantObj u : getSelectedUsers()) {
			if (u.getUserId().equals(user.getUserId())) {
				return;
			}
		}
		this.getSelectedUsers().add(user);
	}

	public void removeSelectedUser(ParticipantObj user) {
		log.debug("removeSelectedUser {}", user);
		for (ParticipantObj u : getSelectedUsers()) {
			if (u.getUserId().equals(user.getUserId())) {
				getSelectedUsers().remove(u);
				return;
			}
		}
	}

	public void clearSelectedUsers() {
		this.selectedUsers = null;
		this.selectedUsers = new ArrayList<ParticipantObj>();
	}

	/*
	 * public List<Project> getProjects() {
	 * System.out.println("sessionBean.getProjects"); return
	 * projectDao.findAllByCompanyId(company.getCompanyId()); }
	 */

	public void setProjects(List<ProjectObj> projects) {
		this.projects = projects;
	}

	public List<ProjectObj> getProjects() {
		return projects;
	}

	public void changeProject() {
		log.debug("sessionBean.changeProject: {}", String.valueOf(getSelectedProject()));
		setProject(projectDao.findById(getSelectedProject()));
	}

	public void setSelectedProject(Integer selectedProject) {
		this.selectedProject = selectedProject;
	}

	public Integer getSelectedProject() {
		return selectedProject;
	}

	public void updateProject(Project p) {
		List<ProjectObj> projects = getProjects();
		int index = 0;
		for (ProjectObj obj : projects) {
			if (obj.getProjectId() == p.getProjectId()) {
				index = projects.indexOf(obj);
				log.debug("Found project {}, removing at index {}", p.getName(), index);
				projects.remove(obj);
				break;
			}
		}
		projects.add(index, setupProjectObject(p));
		setProjects(projects);

	}

	public ProjectObj setupProjectObject(Project p) {
		log.trace("projectname={}", p.getName());
		String adminId;
		if (p.getExtAdminId() == null) {
			adminId = p.getAdmin().getEmail() + " (NHN Admin)";
		} else {
			adminId = p.getExtAdminId();
		}

		String projectType = "";
		String projectTypeCode = "";
		int projectTypeId = 0;
		if (p.getProjectType() != null) {
			projectType = p.getProjectType().getName();
			projectTypeId = p.getProjectType().getProjectTypeId();
			projectTypeCode = p.getProjectType().getProjectTypeCode();
		}

		ProjectObj po = new ProjectObj(p.getProjectId(), p.getName(), p.getProjectCode(), projectType, projectTypeId,
				projectDao.resolveTargetLevel(p), adminId, p.getUsersCount(), p.getDateCreated(), p.getDueDate());

		po.setStatusName(p.getActive() == 1 ? "Open" : "Closed");
		po.setTypeCode(projectTypeCode);
		po.setAssignedPmPcId(p.getAssignedPmId());
		po.setInternalSchedulerId(p.getInternalSchedulerId());
		po.setIncludeClientLogonIdPassword(p.isIncludeClientLogonIdPassword());

		return po;

	}

	public void refreshProjects(boolean includeClosedProjects) {
		Subject subject = SecurityUtils.getSubject();
		log.debug("find all authorized projects for companyID {}", getCompany().getCompanyId());

		List<Project> projectList = authorizationService.getAllowedProjectList(getRoleContext(), getCompany()
				.getCompanyId());

		List<ProjectObj> pol = new ArrayList<ProjectObj>();
		log.debug("found {} projects", projectList.size());
		// populate into view list
		Project p;
		Iterator<Project> iterator = projectList.iterator();
		while (iterator.hasNext()) {
			p = iterator.next();
			if (includeClosedProjects || p.getActive() == 1) {
				pol.add(setupProjectObject(p));
			}
		}
		setProjects(pol);
	}

	public void refreshClient() {
		String clientInfo = (company != null) ? "Name: " + company.getCoName() + " Id: " + company.getCompanyId() : "";
		log.debug("sessionBean.refreshClient {}", clientInfo);
		if (company != null) {
			company = companyDao.findById(company.getCompanyId());
		}
	}

	public void refreshCurrentUser() {
		String userInfo = (user != null) ? "Name: " + user.getUsername() + " Id: " + user.getUsername() : "";
		log.debug("sessionBean.refreshCurrentUser {}", userInfo);
		if (user != null) {
			user = userDao.findById(user.getUsersId());
		}
	}

	public String getRowsPerPageTemplate(List<?> list) {
		return Utils.rowsPerPageTemplate(list);
	}

	public String handleNavClientList() {
		return "clientList.xhtml?faces-redirect=true";
	}

	public String handleNavProjectList() {
		return "projectList.xhtml?faces-redirect=true";
	}

	public String handleNavReportCenter() {
		return "reportCenter.xhtml?faces-redirect=true";
	}

	public String handleNavParticipantList() {
		return "participantList.xhtml?faces-redirect=true";
	}

	public String handleNavEmailTemplatesList() {
		// log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>handleNavEmailTemplatesList()........ emailTemplatesList.xhtml?faces-redirect=true");
		this.setTemplateLevel("CLIENT");
		return "emailTemplatesList.xhtml?faces-redirect=true";
	}

	public String handleNavGlobalEmailTemplatesList() {
		// log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>handleNavGlobalEmailTemplatesList()........ globalEmailTemplatesList.xhtml?faces-redirect=true");
		this.setTemplateLevel("GLOBAL");
		return "globalEmailTemplatesList.xhtml?faces-redirect=true";
	}

	public String handleNavEditEmailTemplate() {
		// log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>handleNavEditEmailTemplate()........ editEmailTemplate.xhtml?faces-redirect=true");
		return "editEmailTemplate.xhtml?faces-redirect=true";
	}

	public String handleNavSuperUserList() {
		return "superUserList.xhtml?faces-redirect=true";
	}

	public String handleNavProjectManagement() {
		return "projectManagement.xhtml?faces-redirect=true";
	}

	public void handleRefresh(ActionEvent event) {
		projectDao.evictAll();
		uploadImportJobDao.evictAll();

		ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
		String redirect = ctx.getRequestContextPath() + ctx.getRequestServletPath() + ctx.getRequestPathInfo() + "?";
		String queryString = getRequestParameter("queryString");

		log.debug("queryString: {}", queryString);

		if (queryString != null && !queryString.equals("null")) {
			redirect += queryString;
			if (!queryString.contains("faces-redirect=")) {
				redirect += "&faces-redirect=true";
			}

		} else {
			redirect += "faces-redirect=true";
		}

		log.debug("sessionBean.handleRefresh() redirect to: {}", redirect);

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(redirect);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// TODO: Needs to move this method into some utility class
	private static String getRequestParameter(String param) {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> map = context.getExternalContext().getRequestParameterMap();
		return map.get(param);
	}

	public String navLogout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			Subject subject = SecurityUtils.getSubject();
			subject.logout();
			request.logout();
			loggedIn = false;
			return "login_timeout?faces-redirect=true";
		} catch (ServletException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void resetSessionTimeout() {
		// FacesContext.getCurrentInstance().getExternalContext().setSessionMaxInactiveInterval(FacesContext.getCurrentInstance().getExternalContext().getSessionMaxInactiveInterval());
		log.debug("Resetting Session Timeout");
	}

	/*
	 *  IdleEvent no longer available in primefaces 3.5 and this method is not used anywhere (Andrey)
	 */

	/*	public void idleListener(IdleEvent event) {
			// invalidate session
			try {
				FacesContext
						.getCurrentInstance()
						.getExternalContext()
						.redirect(
								FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
										+ FacesContext.getCurrentInstance().getExternalContext().getRequestServletPath()
										+ "/login_timeout.xhtml?faces-redirect=true");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	*/
	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public String getServerAndContextPath() {
		return Utils.getServerAndContextPath();
	}

	public void setProjectLanguages(HashMap<String, String> projectLanguages) {
		this.projectLanguages = projectLanguages;
	}

	public HashMap<String, String> getProjectLanguages() {
		return projectLanguages;
	}

	public RoleContext getRoleContext() {
		return roleContext;
	}

	public void setRoleContext(RoleContext roleContext) {
		this.roleContext = roleContext;
	}

	public List<ProjectObj> getFilteredProjects() {
		return filteredProjects;
	}

	public void setFilteredProjects(List<ProjectObj> filteredProjects) {
		this.filteredProjects = filteredProjects;
	}

	public String getTemplateLevel() {
		return templateLevel;
	}

	public void setTemplateLevel(String templateLevel) {
		this.templateLevel = templateLevel;
	}

	/*
		public boolean isUseCompanyPermissions() {
			return useCompanyPermissions;
		}

		public void setUseCompanyPermissions(boolean useCompanyPermissions) {
			this.useCompanyPermissions = useCompanyPermissions;
		}

		public boolean isUseProjectPermissions() {
			return useProjectPermissions;
		}

		public void setUseProjectPermissions(boolean useProjectPermissions) {
			this.useProjectPermissions = useProjectPermissions;
		}*/

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	/*
		public String getAuthTargetUrl() {
			return authTargetUrl;
		}
	
		public void setAuthTargetUrl(String authTargetUrl) {
			this.authTargetUrl = authTargetUrl;
		}
	*/
	public boolean isIncludeClosedProjects() {
		return includeClosedProjects;
	}

	public void setIncludeClosedProjects(boolean includeClosedProjects) {
		this.includeClosedProjects = includeClosedProjects;
	}

	public int getSubject_id() {
		if (getRoleContext() != null && getRoleContext().getSubject() != null) {
			return getRoleContext().getSubject().getSubject_id();
		}
		return 0;
	}

	public String handleNavFieldDictionary() {
		return "formFieldManagement.xhtml?faces-redirect=true";
	}

	public boolean isPermitted(String permission) {
		Subject subject = SecurityUtils.getSubject();
		return subject.isPermitted(permission);
	}

	public boolean isReportCenterDisabled() {
		return reportCenterDisabled;
	}

	public void setReportCenterDisabled(boolean reportCenterDisabled) {
		this.reportCenterDisabled = reportCenterDisabled;
	}

	public int getLrmProjectId() {
		return lrmProjectId;
	}

	public void setLrmProjectId(int lrmProjectId) {
		this.lrmProjectId = lrmProjectId;
	}

	public int getImportJobId() {
		return importJobId;
	}

	public void setImportJobId(int importJobId) {
		this.importJobId = importJobId;
	}

}
