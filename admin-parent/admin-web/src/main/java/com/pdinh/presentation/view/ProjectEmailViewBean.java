package com.pdinh.presentation.view;

import java.io.Serializable;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailRuleObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.EmailTemplateObj;

@ManagedBean(name = "projectEmailViewBean")
@ViewScoped
public class ProjectEmailViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<EmailRuleObj> rules;
	private EmailRuleObj selectedRule;
	
	
	private Boolean editRuleDisabled = true;
	private Boolean deleteRuleDisabled = true;
	private Boolean createNewRuleDisabled = true;
	
	private List<ListItemObj> currentEmailLangs;

	
	public List<EmailRuleObj> getRules() {
		return rules;
	}

	public void setRules(List<EmailRuleObj> rules) {
		this.rules = rules;
	}

	public EmailRuleObj getSelectedRule() {
		return selectedRule;
	}

	public void setSelectedRule(EmailRuleObj selectedRule) {
		this.selectedRule = selectedRule;
	}

	public Boolean getEditRuleDisabled() {
		return editRuleDisabled;
	}

	public void setEditRuleDisabled(Boolean editRuleDisabled) {
		this.editRuleDisabled = editRuleDisabled;
	}


	
	public List<ListItemObj> getCurrentEmailLangs() {
		return currentEmailLangs;
	}

	public void setCurrentEmailLangs(List<ListItemObj> currentEmailLangs) {
		this.currentEmailLangs = currentEmailLangs;
	}
	


	public Boolean getDeleteRuleDisabled() {
		return deleteRuleDisabled;
	}

	public void setDeleteRuleDisabled(Boolean deleteRuleDisabled) {
		this.deleteRuleDisabled = deleteRuleDisabled;
	}

	public Boolean getCreateNewRuleDisabled() {
		return createNewRuleDisabled;
	}

	public void setCreateNewRuleDisabled(Boolean createNewRuleDisabled) {
		this.createNewRuleDisabled = createNewRuleDisabled;
	}


}
