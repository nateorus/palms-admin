package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ScheduledEmailLogDao;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.manage.emailtemplates.EmailTemplatesService;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailLog;
import com.pdinh.presentation.domain.EmailLogObj;
import com.pdinh.presentation.domain.EmailRuleObj;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EmailLogViewBean;

@ManagedBean
@ViewScoped
public class EmailLogControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EmailLogControllerBean.class);

	@Resource(mappedName = "custom/tlt_properties")
	private Properties tltProperties;

	@EJB
	private ScheduledEmailLogDao scheduledEmailLogDao;

	@EJB
	private MailTemplatesServiceLocalImpl mailTemplatesService;

	@EJB
	private EmailTemplatesService emailTemplateService;

	@ManagedProperty(name = "emailLogViewBean", value = "#{emailLogViewBean}")
	private EmailLogViewBean emailLogViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	private boolean firstTime = true;

	public void initView() {

		log.debug(">>>>>>>>>>>>>>>>> EmailLogControllerBean initView start");
		if (isFirstTime()) {

			setFirstTime(false);

			int projectId = sessionBean.getProject().getProjectId();

			List<ScheduledEmailLog> scheduledEmailLog = scheduledEmailLogDao
					.findScheduledEmailLogsByProjectId(projectId);
			List<EmailLogObj> emailLogObjs = new ArrayList<EmailLogObj>();

			if (scheduledEmailLog != null) {
				for (ScheduledEmailLog record : scheduledEmailLog) {
					log.debug("New Record Begin");
					EmailLogObj emailLogRecordObj = new EmailLogObj();
					EmailRuleObj emailRuleObj = new EmailRuleObj();
					ParticipantObj participant = new ParticipantObj();
					String participantLangCode = "en";

					// Id
					emailLogRecordObj.setId(record.getEmailLogId());

					// Email Rule
					log.debug("Begin Email Rule");
					
					if (record.getSchedule() != null) {
						if (record.getSchedule().getEmailRule().getEmailTemplate() != null) {
							emailRuleObj.setId(record.getSchedule().getEmailRule().getId());
							emailRuleObj.setName(record.getSchedule().getEmailRule().getName());
							emailRuleObj.setActive((record.getSchedule().getEmailRule().getActive()) ? "Yes" : "No");
							emailRuleObj.setLanguage(record.getSchedule().getEmailRule().getLanguage());
							emailRuleObj.setTemplate(convertToTemplateObj(record.getSchedule().getEmailRule()
									.getEmailTemplate()));
						}
					}

					emailLogRecordObj.setEmailRule(emailRuleObj);
					log.debug("end Email Rule, begin Participant");

					// Participant
					participant.setUserId(record.getUser().getUsersId());
					participant.setLastName(record.getUser().getLastname());
					participant.setFirstName(record.getUser().getFirstname());
					participant.setEmail(record.getUser().getEmail());
					participant.setUserName(record.getUser().getUsername());
					participant.setPassword(record.getUser().getPassword());
					participant.setOptional1(record.getUser().getOptional1());
					participant.setOptional2(record.getUser().getOptional2());
					participant.setOptional3(record.getUser().getOptional3());
					participant.setSupervisorEmail(record.getUser().getSupervisorEmail());
					participant.setSupervisorName(record.getUser().getSupervisorName());

					if (record.getUser().getLangPref() != null) {
						participantLangCode = record.getUser().getLangPref();
					} else {
						if (record.getCompany().getLangPref() != null) {
							participantLangCode = record.getCompany().getLangPref();
						}
					}

					participant.setLanguageId(participantLangCode);
					participant.setParticipantExperienceUrl(tltProperties.getProperty("tltParticipantExperienceUrl",
							"http://default.url") + "?lang=" + participant.getLanguageId());
					emailLogRecordObj.setParticipant(participant);
					
					log.debug("Finished participant, Begin Email Template");

					// Email Template
					EmailTemplateObj emailTeamplateObj = convertToTemplateObj(record.getScheduledEmailDef());
					emailLogRecordObj.setEmailTemplate(emailTeamplateObj);

					// Language Code
					emailLogRecordObj.setLangCode(record.getLanguageCode());

					// Status
					log.debug("Finished Email Template, Begin Status" );
					if (record.getStatus() != null) {
						emailLogRecordObj.setStatus(record.getStatus().getLabel());
					}

					// Date
					emailLogRecordObj.setDate(record.getDateCompleted());

					// Sent Subject and Body
					emailLogRecordObj.setSentEmailSubject(record.getSentEmailSubject());
					emailLogRecordObj.setSentEmailBody(record.getSentEmailBody());

					log.info("Log {}", emailLogRecordObj.getId());
					log.debug(">>>>>> Subject {}", emailLogRecordObj.getSentEmailSubject());
					log.trace(">>>>>> Email Body {}", emailLogRecordObj.getSentEmailBody());

					emailLogObjs.add(emailLogRecordObj);
				}
			}

			Collections.reverse(emailLogObjs);
			emailLogViewBean.setLog(emailLogObjs);
			emailLogViewBean.setFilteredRecords(null);
		}

		log.debug(">>>>>>>>>>>>>>>>> EmailLogControllerBean initView end");

	}

	public void refresh() {
		setFirstTime(true);
		initView();
	}

	public void refreshLog() {
		refresh();
	}

	public void resendEmail(ActionEvent event) {
		EmailLogObj logEntry = emailLogViewBean.getSelectedRecord();
		EmailTextObj emailTextObj = findEmailTextByLanguageCode(logEntry.getLangCode(), logEntry.getEmailTemplate());

		if (emailTextObj != null) {
			if (emailTextObj.getId() != null) {
				mailTemplatesService.sendTemplate(emailTextObj.getId(), logEntry.getParticipant(), sessionBean
						.getProject(), sessionBean.getCompany(), logEntry.getParticipant()
						.getParticipantExperienceUrl());
				refreshView();
			} else {
				log.error("Unable to send email because cannot find email text (log entry id {})", logEntry.getId());
			}
		}
	}

	private void refreshView() {
		initView();
	}

	private EmailTextObj findEmailTextByLanguageCode(String langCode, EmailTemplateObj templateObj) {
		return emailTemplateService.findEmailTextByLangCode(templateObj, langCode);
	}

	public void handleRowSelect(SelectEvent event) {
		EmailLogObj selectedRecord = emailLogViewBean.getSelectedRecord();
		if (selectedRecord != null) {
			Integer templateId = selectedRecord.getEmailTemplate().getId();
			if (emailTemplateService.isTemplateExits(templateId)) {
				emailLogViewBean.setDisabledResendButton(false);
			} else {
				emailLogViewBean.setDisabledResendButton(true);
			}
		} else {
			emailLogViewBean.setDisabledResendButton(true);
		}
	}

	public void handleRowUnselect(UnselectEvent event) {
		emailLogViewBean.setSelectedEmailText(null);
		emailLogViewBean.setDisabledResendButton(true);
	}

	/* 
	 *  Building emailTemplateObj from jpa scheduledEmailDef ...
	 */
	private EmailTemplateObj convertToTemplateObj(ScheduledEmailDef sed) {
		EmailTemplateObj templateObj = new EmailTemplateObj();

		if (sed != null) {
			templateObj = emailTemplateService.scheduledEmailDef2EmailTemplateObj(sed);
		}

		return templateObj;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public EmailLogViewBean getEmailLogViewBean() {
		return emailLogViewBean;
	}

	public void setEmailLogViewBean(EmailLogViewBean emailLogViewBean) {
		this.emailLogViewBean = emailLogViewBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}
}
