package com.pdinh.presentation.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseVersion;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportTypeObj;

@ManagedBean
@ViewScoped
public class ReportWizardHelper {

	final static Logger logger = LoggerFactory.getLogger(ReportWizardHelper.class);

	@EJB
	PositionDao positionDao;

	@EJB
	ReportingSingleton reportingSingleton;

	public void updateParticipantsWithCoursesVersion(List<ParticipantObj> participants) {
		List<Integer> usersIds = new ArrayList<Integer>();
		for (ParticipantObj participant : participants) {
			usersIds.add(participant.getUserId());
		}

		List<Position> positions = positionDao.findPositionsByUsersIds(usersIds);

		if (!positions.isEmpty()) {
			for (Position position : positions) {
				ParticipantObj participantObj = this.findParticipantById(
						Integer.valueOf(position.getUser().getUsersId()), participants);
				CourseVersion courseVersion = reportingSingleton.getCourseVersionByCompletionDate(
						position.getCompletionStatusDate(), position.getCourse().getCourse());
				int courseVersionNumber = (courseVersion != null) ? courseVersion.getVersionNumber() : 1;
				participantObj.getCourseVersions().put(position.getCourse().getAbbv(),
						String.valueOf(courseVersionNumber));
			}
		}

		// AV: Copying courses version for all participants records, some
		// participant can be in multiple projects and represented as a single
		// record for each project, so second record of the same participant
		// will not have course versions. This only relevant to Users and
		// Clipboard sections.

		for (ParticipantObj participant : participants) {
			if (participant.getCourseVersions().size() == 0) {
				participant.setCourseVersions(this.getCoursesVersionByUserId(participants, participant.getUserId()));
			}
			logger.trace("Id: {}; Name: {}; Course Versions: {}; Project Id: {}; Project Name: {}", participant
					.getUserId(), participant, participant.courseVersionsToString(), participant.getProject()
					.getProjectId(), participant.getProject().getName());
		}
	}

	public ParticipantObj findParticipantById(Integer participantId, List<ParticipantObj> participants) {
		if (!participants.isEmpty()) {
			for (ParticipantObj participant : participants) {
				if (participant.getUserId().equals(participantId)) {
					return participant;
				}
			}
		}
		return null;
	}

	public void filterALPReports(List<ReportTypeObj> reportTypes, List<ParticipantObj> participants) {
		List<Integer> versions = new ArrayList<Integer>();

		this.updateParticipantsWithCoursesVersion(participants);

		for (ParticipantObj participant : participants) {
			String courseVersionNumber = participant.getCourseVersions().get(Course.KFP);
			if (courseVersionNumber != null) {
				versions.add(Integer.valueOf(courseVersionNumber));
			}
		}

		if (!versions.isEmpty()) {
			if (versions.contains(1) && versions.contains(2)) {
				logger.trace("FOUND MIXED (V1 and V2) ALP PARTICIPANTS GROUP ...");
				this.findAndRemoveReportType(reportTypes, ReportType.KFP_INDIVIDUAL);
			} else if (versions.contains(1)) {
				logger.trace("FOUND V1 ALP PARTICIPANTS GROUP ...");
			} else if (versions.contains(2)) {
				logger.trace("FOUND V2 ALP PARTICIPANTS GROUP ...");
				this.findAndRemoveReportType(reportTypes, ReportType.KFP_INDIVIDUAL);
			} else if (versions.contains(0)) {
				logger.error("Unable to determined course version. This should never really happen.");
			}
		}
	}

	/**
	 * 
	 * Private methods
	 * 
	 **/

	// Finds report type by report type code and removes it from the list.
	private boolean findAndRemoveReportType(List<ReportTypeObj> reportTypes, String reportTypeCodeToRemove) {
		boolean foundAndRemoved = false;
		List<ReportTypeObj> removeList = new ArrayList<ReportTypeObj>();

		for (ReportTypeObj reportTypeObj : reportTypes) {
			if (reportTypeObj.getCode().equals(reportTypeCodeToRemove)) {
				removeList.add(reportTypeObj);
			}
			// logger.trace("reportTypeObj = {}", reportTypeObj.getName());
		}

		if (!removeList.isEmpty()) {
			reportTypes.removeAll(removeList);
			foundAndRemoved = true;
		}
		return foundAndRemoved;
	}

	private Map<String, String> getCoursesVersionByUserId(List<ParticipantObj> participants, Integer participantId) {
		for (ParticipantObj participant : participants) {
			if (participant.getUserId().equals(participantId) && participant.getCourseVersions().size() > 0) {
				return participant.getCourseVersions();
			}
		}
		return null;
	}

	/**
	 * createReportsObject - Creates the Reports object (essentially a List of
	 * ReportRequest Objects)for an Oxcart report request
	 * 
	 * @param participants
	 * @param reportTypes
	 * @return
	 */

	/*
	public static Reports createReportsObject(List<ParticipantObj> participants, ReportTypeObj[] reportTypes) {

		Reports reports = new Reports();

		// create list of participant report objects
		List<ReportParticipant> allReportParticipantsNoLangCodes = new ArrayList<ReportParticipant>();

		for (ParticipantObj participantObj : participants) {
			ReportParticipant rp = new ReportParticipant(participantObj.getUserId().toString(), participantObj
					.getProject().getProjectId() + "", participantObj.getProject().getName());
			allReportParticipantsNoLangCodes.add(rp);
		}

		for (int i = 0; i < reportTypes.length; i++) {
			ReportTypeObj reportType = reportTypes[i];

			ReportRequest rr = new ReportRequest();
			rr.setCode(reportType.getCode());

			if (reportType.isParticipantLanguageVisible() == false) {
				rr.setLangCode(reportType.getLanguageCode());
			}

			// use participants with lang code if report allows it
			if (reportType.isParticipantLanguageVisible()) {

				List<ReportParticipant> allReportParticipantsWithLangCodes = new ArrayList<ReportParticipant>();

				for (ParticipantObj participantObj : participants) {

					ReportParticipant rpWithLangCode = new ReportParticipant(participantObj.getUserId().toString(),
							participantObj.getProject().getProjectId() + "", participantObj.getProject().getName());

					// if participant language is not specified default to the
					// report language
					if (participantObj.getReportLanguageId() != null
							&& !participantObj.getReportLanguageId().equals("")) {
						rpWithLangCode.setLangCode(participantObj.getReportLanguageId());
					} else {
						rpWithLangCode.setLangCode(reportType.getLanguageCode());
					}
					allReportParticipantsWithLangCodes.add(rpWithLangCode);
				}
				rr.getReportParticipants().addAll(allReportParticipantsWithLangCodes);
			} else {
				rr.getReportParticipants().addAll(allReportParticipantsNoLangCodes);
			}

			if (reportType.getTargetLevelOverride() != null && !reportType.getTargetLevelOverride().equals("")) {
				rr.getOverrides().add(new Override("TARGET_LEVEL", reportType.getTargetLevelOverride()));
			}

			// Do analytics stuff
			if (reportType.isAnalyticsNameOptVisible()) {
				rr.setShowName(reportType.getAnalyticsNameOpt());
			}
			if (reportType.isAnalyticsShowLciOptVisible()) {
				rr.setShowLci(reportType.getAnalyticsShowLciOpt());
			}
			if (reportType.isAnalyticsLabelsOptVisible()) {
				rr.setAxisLabels(reportType.getAnalyticsLabelsOpt());
			}
			if (reportType.isAnalyticsSortOptVisible()) {
				rr.setSortId(reportType.getAnalyticsSortIdOpt());
			}

			reports.getReportRequests().add(rr);
		}
		return reports;
	}

	public static Participants createParticipantsObject(List<ParticipantObj> participantObjs) {
		Participants participants = new Participants();

		for (ParticipantObj participantObj : participantObjs) {
			ReportParticipant rp = new ReportParticipant(participantObj.getUserId().toString(), participantObj
					.getProject().getProjectId() + "", participantObj.getProject().getName());
			// // Set up the language preference. Use report selected if
			// available,
			// // else use participant default
			// String langCode = participantObj.getReportLanguageId() == null ?
			// participantObj.getLanguageId()
			// : participantObj.getReportLanguageId();
			// rp.setLangCode(langCode);

			// Always use the desired ppt report lang code. Additional logic for
			// reports will be handled elsewhere
			rp.setLangCode(participantObj.getReportLanguageId());
			participants.getParticipants().add(rp);
		}

		return participants;
	}

	public static CoachingPlanDetailedExtract createCoachingPlanDetailedExtractObject(
			List<ParticipantObj> participantObjs) {
		CoachingPlanDetailedExtract cpde = new CoachingPlanDetailedExtract();
		Participants participants = new Participants();

		for (ParticipantObj participantObj : participantObjs) {
			ReportParticipant rp = new ReportParticipant(participantObj.getUserId().toString(), participantObj
					.getProject().getProjectId() + "", participantObj.getProject().getName());
			participants.getParticipants().add(rp);
		}
		cpde.setParticipants(participants);
		return cpde;
	}
	*/

	/**
	 * getRcm - Call the appropriate servlet to generate and return the
	 * appropriate RCM as an XML string.
	 * 
	 * @param strUrl
	 * @param params
	 * @return
	 * @throws Exception
	 */

	/*
	public static String getRcm(String strUrl, Map<String, String> params) throws Exception {
		String rcm = "";
		StringBuffer sb = new StringBuffer();
		HttpURLConnection conn = null;
		URL url = null;
		BufferedReader reader = null;

		try {
			url = new URL(strUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setDoOutput(true);
			StringBuffer urlParameters = new StringBuffer();
			Iterator<Entry<String, String>> it = params.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> pair = it.next();
				urlParameters.append(pair.getKey() + "=" + URLEncoder.encode(pair.getValue(), "UTF-8"));
				if (it.hasNext()) {
					urlParameters.append("&");
				}
			}
			System.out.println("urlParameters.toString() = " + urlParameters.toString());

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(urlParameters.toString());
			writer.flush();
			String line;
			// Call servlet to get rcm filled
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			rcm = sb.toString();
			writer.close();
			reader.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			reader.close();
		}

		return rcm;
	}
	*/

}
