package com.pdinh.presentation.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.presentation.managedbean.SessionBean;


@ManagedBean(name = "globalEmailTemplatesListControllerBean")
@ViewScoped
public class GlobalEmailTemplatesListControllerBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EmailTemplateListControllerBean.class);

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	
	
	
	

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
}
