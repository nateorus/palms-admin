package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.component.tabview.Tab;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.EmailRuleToProcessDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.mail.MailRuleProcessorService;
import com.pdinh.persistence.ms.entity.EmailRule;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleFixed;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleRelative;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;
import com.pdinh.persistence.ms.entity.EmailRuleToProcess;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.presentation.domain.EmailRuleObj;
import com.pdinh.presentation.domain.EmailRuleScheduleObj;
import com.pdinh.presentation.domain.EmailRuleScheduleStatusObj;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.helper.RelativeDateCalculator;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EmailLogViewBean;
import com.pdinh.presentation.view.EmailRuleScheduleStatusViewBean;
import com.pdinh.presentation.view.ProjectEmailViewBean;

@ManagedBean(name = "projectEmailControllerBean")
@ViewScoped
public class ProjectEmailControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	final static Logger log = LoggerFactory.getLogger(ProjectEmailControllerBean.class);

	private boolean firstTime = true;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private LanguageDao languageDao;

	@EJB
	private EmailRuleDao emailRuleDao;

	@EJB
	private EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	private EmailRuleToProcessDao emailRuleToProcessDao;

	@EJB
	private MailRuleProcessorService mailRuleProcessorService;

	@EJB
	private LanguageSingleton languageSingleton;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "projectEmailViewBean", value = "#{projectEmailViewBean}")
	private ProjectEmailViewBean projectEmailViewBean;

	@ManagedProperty(name = "emailRuleScheduleStatusViewBean", value = "#{emailRuleScheduleStatusViewBean}")
	private EmailRuleScheduleStatusViewBean emailRuleScheduleStatusViewBean;

	@ManagedProperty(name = "emailLogControllerBean", value = "#{emailLogControllerBean}")
	private EmailLogControllerBean emailLogControllerBean;

	@ManagedProperty(name = "emailLogViewBean", value = "#{emailLogViewBean}")
	private EmailLogViewBean emailLogViewBean;

	private List<LanguageObj> languages;

	public void initView() {
		log.debug("In ProjectEmailControllerBean initView start");
		if (isFirstTime()) {
			Subject subject = SecurityUtils.getSubject();
			if (sessionBean.getProject() != null) {

				setFirstTime(false);

				setLanguages(languageSingleton.getLanguages());

				initRulesList();

				if (getProjectEmailViewBean().getSelectedRule() == null) {
					disableToolbarButtons(true);
				} else {
					disableToolbarButtons(false);
				}
				if (subject.isPermitted("createEmailRules")) {
					projectEmailViewBean.setCreateNewRuleDisabled(false);
				}
				emailRuleScheduleStatusViewBean.setStatusList(fetchRuleScheduleStatusList());
			}
		}

		// I need a way to check the template level, across the different
		// controllers... they can't all talk to each other... and you don't
		// always go
		// to the List bean when you jump to client emails from project emails
		getSessionBean().setTemplateLevel("PROJ");
		log.debug("In ProjectEmailControllerBean initView end");
	}

	public void handleTabChange(TabChangeEvent event) {
		Tab activeTab = event.getTab();

		if (activeTab.getId().equals("tabRules")) {
			refreshView();
		} else if (activeTab.getId().equals("tabTemplates")) {
			// we should init templates here
		} else if (activeTab.getId().equals("tabLog")) {
			emailLogControllerBean.initView();
			emailLogViewBean.setFilteredRecords(null);
		}
	}

	private void initRulesList() {

		List<EmailRuleObj> rules = new ArrayList<EmailRuleObj>();
		int projectId = sessionBean.getProject().getProjectId();
		List<EmailRule> emailRules = emailRuleDao.findEmailRulesByProjectId(projectId);

		for (EmailRule emailRule : emailRules) {

			if (emailRule.getEmailTemplate() == null) {
				log.error(">>>>>>> Found email rule ({}) without valid template. Email Template doesn't exist.",
						emailRule.getId());
			} else {
				EmailRuleObj ruleObj = new EmailRuleObj();
				EmailTemplateObj templateObj = new EmailTemplateObj();
				List<EmailRuleScheduleObj> scheduleList = new ArrayList<EmailRuleScheduleObj>();
				Boolean ruleComplete = isEmailRuleComplete(emailRule);
				Boolean ruleWithErrors = isEmailRuleHasErrors(emailRule);

				ruleObj.setId(emailRule.getId());
				ruleObj.setName(emailRule.getName());
				ruleObj.setActive((emailRule.getActive()) ? "Yes" : "No");
				ruleObj.setLanguage(getLanguageString(emailRule.getLanguage()));
				templateObj.setId(emailRule.getEmailTemplate().getId());
				templateObj.setTitle(emailRule.getEmailTemplate().getTitle());
				templateObj.setLevel(emailRule.getEmailTemplate().getLevel());

				ruleObj.setTemplate(templateObj);

				for (EmailRuleSchedule emailRuleSchedule : emailRule.getEmailRuleSchedules()) {
					EmailRuleScheduleObj emailRuleScheduleObj = new EmailRuleScheduleObj();
					EmailRuleScheduleStatusObj emailRuleScheduleStatusObj = new EmailRuleScheduleStatusObj();
					Date scheduledDate = new Date();

					emailRuleScheduleStatusObj.setId(emailRuleSchedule.getEmailRuleScheduleStatus().getId());
					emailRuleScheduleStatusObj.setName(emailRuleSchedule.getEmailRuleScheduleStatus().getName());
					emailRuleScheduleStatusObj.setCode(emailRuleSchedule.getEmailRuleScheduleStatus().getCode());
					emailRuleScheduleObj.setCurrentStatus(emailRuleScheduleStatusObj);

					emailRuleScheduleObj.setComplete(emailRuleSchedule.isComplete());
					emailRuleScheduleObj.setError(emailRuleSchedule.isError());
					emailRuleScheduleObj.setRuleId(emailRuleSchedule.getEmailRule().getId());

					if (emailRuleSchedule.getEmailRuleScheduleType().getCode().equals("FIXED_DATE")) {
						EmailRuleScheduleFixed fixedDate = (EmailRuleScheduleFixed) emailRuleSchedule;
						scheduledDate = fixedDate.getDate();
					} else if (emailRuleSchedule.getEmailRuleScheduleType().getCode().equals("RELATIVE_DATE")) {
						EmailRuleScheduleRelative relativeDate = (EmailRuleScheduleRelative) emailRuleSchedule;
						scheduledDate = RelativeDateCalculator.calculateRelativeDate(relativeDate.getDaysAfter(),
								relativeDate.getDays(), relativeDate.getRelativeToDate());
					}

					emailRuleScheduleObj.setScheduledDate(scheduledDate);
					scheduleList.add(emailRuleScheduleObj);
				}

				// Sorting schedule sub table to show all records in order.
				Collections.sort(scheduleList, new Comparator<EmailRuleScheduleObj>() {
					@Override
					public int compare(EmailRuleScheduleObj obj1, EmailRuleScheduleObj obj2) {
						return obj1.getScheduledDate().compareTo(obj2.getScheduledDate());
					}
				});

				ruleObj.setSchedules(scheduleList);
				ruleObj.setCompleted(ruleComplete);
				ruleObj.setError(ruleWithErrors);

				rules.add(ruleObj);
			}

		}

		getProjectEmailViewBean().setRules(rules);
	}

	private Boolean isEmailRuleComplete(EmailRule emailRule) {
		Boolean complete = false;
		int totalSchedule = emailRule.getEmailRuleSchedules().size();
		int totalComplete = 0;

		for (EmailRuleSchedule emailRuleSchedule : emailRule.getEmailRuleSchedules()) {
			if (emailRuleSchedule.isComplete()) {
				totalComplete++;
			}
		}

		complete = (totalSchedule == totalComplete) ? true : false;

		return complete;
	}

	private Boolean isEmailRuleHasErrors(EmailRule emailRule) {
		Boolean errors = false;
		int totalSchedule = emailRule.getEmailRuleSchedules().size();
		int totalErrors = 0;

		for (EmailRuleSchedule emailRuleSchedule : emailRule.getEmailRuleSchedules()) {
			if (emailRuleSchedule.isError()) {
				totalErrors++;
			}
		}

		errors = (totalSchedule == totalErrors) ? true : false;

		return errors;
	}

	private List<EmailRuleScheduleStatusObj> fetchRuleScheduleStatusList() {
		List<EmailRuleScheduleStatusObj> scheduleStatusObjs = new ArrayList<EmailRuleScheduleStatusObj>();
		List<EmailRuleScheduleStatus> scheduleStatusList = emailRuleScheduleStatusDao.findAll();

		for (EmailRuleScheduleStatus status : scheduleStatusList) {
			EmailRuleScheduleStatusObj statusObj = new EmailRuleScheduleStatusObj();

			statusObj.setId(status.getId());
			statusObj.setName(status.getName());
			statusObj.setCode(status.getCode());

			scheduleStatusObjs.add(statusObj);

		}

		return scheduleStatusObjs;
	}

	public void refreshView() {
		setFirstTime(true);
		initView();
		log.debug("EXIT refreshView");
	}

	public void handleChangeProject(AjaxBehaviorEvent event) {
		Integer pid = getSessionBean().getSelectedProject();
		Project project = projectDao.findBatchById(pid);
		sessionBean.setProject(project);
		log.debug("IN ProjectEmailControllerBean.handleChangeProject, selected project: {}", pid);
		refreshView();
	}

	private void disableToolbarButtons(Boolean disable) {
		Subject subject = SecurityUtils.getSubject();

		if (!(disable)) {// enabling

			if (subject.isPermitted("editEmailRules")) {
				getProjectEmailViewBean().setEditRuleDisabled(disable);
			}
			if (subject.isPermitted("deleteEmailRules")) {
				getProjectEmailViewBean().setDeleteRuleDisabled(disable);
			}
		} else {// disabling
			getProjectEmailViewBean().setEditRuleDisabled(disable);
			getProjectEmailViewBean().setDeleteRuleDisabled(disable);
		}
	}

	// TODO: This method needs to be revisited (rewritten)
	public String colorRuleRow(Boolean complete, Boolean error) {
		if (complete && !error) {
			return "row-complete";
		} else if (error) {
			return "row-errors";
		} else {
			return null;
		}
	}

	public void handleRowSelect(SelectEvent event) {
		Subject subject = SecurityUtils.getSubject();
		EmailRuleObj ruleObj = (EmailRuleObj) event.getObject();
		getProjectEmailViewBean().setSelectedRule(ruleObj);
		if (ruleObj != null) {
			disableToolbarButtons(false);
			if (subject.isPermitted("deleteEmailRules")) {
				getProjectEmailViewBean().setDeleteRuleDisabled(false);
			}
		} else {
			disableToolbarButtons(true);
			getProjectEmailViewBean().setDeleteRuleDisabled(true);
		}

	}

	public void handleRowUnselect(UnselectEvent event) {
		getProjectEmailViewBean().setSelectedRule(null);
		getProjectEmailViewBean().setDeleteRuleDisabled(true);
		disableToolbarButtons(true);
	}

	public String handleCreateNewRule() {
		return "emailRuleSetup.xhtml?faces-redirect=true&create=true";
	}

	public String handleEditRule() {
		int ruleId = projectEmailViewBean.getSelectedRule().getId();
		return "emailRuleSetup.xhtml?faces-redirect=true&create=false&ruleId=" + ruleId;
	}

	public void handleDeleteRule(ActionEvent event) {
		int ruleId = getProjectEmailViewBean().getSelectedRule().getId();
		getProjectEmailViewBean().setSelectedRule(null);
		EmailRule emailRule = emailRuleDao.findById(ruleId);
		emailRuleDao.delete(emailRule);
		refreshView();
	}
/*
	public void handleProcessRules(ActionEvent event) {
		List<EmailRuleToProcess> rulesToProcess = emailRuleToProcessDao.findEmailRuleToProcessByProjectId(sessionBean
				.getProject().getProjectId());
		for (EmailRuleToProcess rule : rulesToProcess) {
			mailRuleProcessorService.processRule(rule, false);
		}
		refreshView();
	}
*/
	public void handleScheduleStatus(EmailRuleScheduleObj schedule) {
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> handleScheduleStatus:  Setting currentSchedule to {}", schedule);
		emailRuleScheduleStatusViewBean.setCurrentSchedule(schedule);
	}

	public void handleScheduleStatusClose(ActionEvent event) {
		emailRuleScheduleStatusViewBean.setCurrentSchedule(null);
	}

	public String getLanguageString(String languageCode) {
		for (LanguageObj language : getLanguages()) {
			if (languageCode != null && languageCode.equalsIgnoreCase(language.getCode().toLowerCase())) {
				return language.getName() + " (" + languageCode + ")";
			}
		}
		return "Participant Default";
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ProjectEmailViewBean getProjectEmailViewBean() {
		return projectEmailViewBean;
	}

	public void setProjectEmailViewBean(ProjectEmailViewBean projectEmailViewBean) {
		this.projectEmailViewBean = projectEmailViewBean;
	}

	public List<LanguageObj> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageObj> languages) {
		this.languages = languages;
	}

	public EmailRuleScheduleStatusViewBean getEmailRuleScheduleStatusViewBean() {
		return emailRuleScheduleStatusViewBean;
	}

	public void setEmailRuleScheduleStatusViewBean(EmailRuleScheduleStatusViewBean emailRuleScheduleStatusViewBean) {
		this.emailRuleScheduleStatusViewBean = emailRuleScheduleStatusViewBean;
	}

	public EmailLogControllerBean getEmailLogControllerBean() {
		return emailLogControllerBean;
	}

	public void setEmailLogControllerBean(EmailLogControllerBean emailLogControllerBean) {
		this.emailLogControllerBean = emailLogControllerBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public EmailLogViewBean getEmailLogViewBean() {
		return emailLogViewBean;
	}

	public void setEmailLogViewBean(EmailLogViewBean emailLogViewBean) {
		this.emailLogViewBean = emailLogViewBean;
	}

}