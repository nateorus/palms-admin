package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.component.tabview.Tab;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequestList;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentationList;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ResultObj;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.helper.ReportRequestHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ReportCenterViewBean;

@ManagedBean
@ViewScoped
public class ReportCenterControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(ReportCenterControllerBean.class);
	final static Logger auditLog = LoggerFactory.getLogger("AUDIT-ADMIN");

	private boolean firstTime = true;
	private boolean dataLoaded = false;
	private Project currentProject;
	private Project project = null;
	// private User user = null;
	private Subject subject;
	private int counter = 1;
	@EJB
	private LanguageDao languageDao;
	@ManagedProperty(name = "reportRequestHelper", value = "#{reportRequestHelper}")
	private ReportRequestHelper reportRequestHelper;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "reportCenterViewBean", value = "#{reportCenterViewBean}")
	private ReportCenterViewBean viewBean;

	@EJB
	ProjectDao projectDao;

	@EJB
	UserDao userDao;

	@EJB
	ReportingSingleton reportingSingleton;

	@EJB
	LanguageSingleton languageSingleton;

	@EJB
	AuthorizationServiceLocal authorizationService;

	@EJB
	ProjectUserDao projectUserDao;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	public void initView() {

		logger.trace("In ReportCenterControllerBean initView... Page: {}", viewBean.getPage());

		if (isFirstTime()) {
			logger.trace("Reloading data for Report Center page ...");

			viewBean.initializeContext();

			if (viewBean.isInitRepository()) {
				viewBean.setCurrentTabId("repositoryTab");
			} else {
				viewBean.setCurrentTabId("queueTab");
			}
			if (viewBean.getRequestId() > 0) {
				logger.debug("Got Request Id param: {}", viewBean.getRequestId());
			}

			if (viewBean.getProjectId() != null && !viewBean.isCompanyContext()) {
				project = projectDao.findById(viewBean.getProjectId());
				logger.trace("Got project id {} as parameter.", project.getProjectId());
			} else {
				if (getSessionBean().getUser() != null) {
					viewBean.setUser(getSessionBean().getUser());
					this.setUserPanelData();
					logger.trace("Got user  {} from session user.", viewBean.getUser().getUsersId());
				} else if (getSessionBean().getProject() != null && !viewBean.isCompanyContext()) {
					project = getSessionBean().getProject();
					logger.trace("Got project  {} from session project.", project.getProjectId());
				} else if (getSessionBean().getSelectedProject() != null && !viewBean.isCompanyContext()) {
					project = projectDao.findById(getSessionBean().getSelectedProject());
					logger.trace("Got project {} as selected project.", project.getProjectId());
				}
			}

			if (viewBean.getUser() != null) {
				logger.debug("The user is NOT null... after context check");
			} else {
				logger.debug("User is null... after context check");
			}

			if (viewBean.getUser() != null) {
				logger.debug("User is Not Null");
				getSessionBean().setCompany(viewBean.getUser().getCompany());
				viewBean.setUserContext(true);
				// set user view stuff
			} else if (project != null) {
				logger.debug("Setting up project context");
				getSessionBean().setProject(project);
				getSessionBean().setCompany(project.getCompany());
				setCurrentProject(getSessionBean().getProject());
				this.setProjectPanelData(currentProject);
				viewBean.setProjectContext(true);
			} else {
				viewBean.setUserContext(false);
				viewBean.setProjectContext(false);
			}

			if (!viewBean.isProjectContext() && !viewBean.isUserContext()) {
				viewBean.setCompanyContext(true);
			}

			getSessionBean().refreshProjects(getSessionBean().isIncludeClosedProjects());

			setSubject(SecurityUtils.getSubject());

			viewBean.setReportRequestStatuses(reportingSingleton.getReportRequestStatuses());
			viewBean.setStatusOptions();
			viewBean.setPageTitle(createPageTitle());

			this.initChangeContextMenu();

			this.securityCheck();

			setFirstTime(false);

		}

	}

	private void securityCheck() {
		String context = "";
		boolean permitted = true;

		if (viewBean.isCompanyContext()) {
			context = "Client Context";
			permitted = this.isClientContextPermitted();
		} else if (viewBean.isProjectContext()) {
			context = "Project Context";
			permitted = this.isProjectContextPermitted();
		} else if (viewBean.isUserContext()) {
			context = "User Context";
			// TODO: AV: Do we need to check permission for user ???
		}

		if (!permitted) {
			LabelUtils.addErrorMessage(null, "Unauthorized:",
					"Unfortunately you are not authorized to view this content.");
			logger.error("Report Center: Unfortunately you are not authorized to view '{}' content as {}.", context,
					this.getSubject().getPrincipal());
		}

		viewBean.setMainContentRendered(permitted);

	}

	private void setUserPanelData() {
		if (viewBean.getUser() != null) {
			viewBean.setUserName(viewBean.getUser().getFirstname() + " " + viewBean.getUser().getLastname());
			viewBean.setUserEmail(viewBean.getUser().getEmail());
			if (viewBean.getUser().getPhoneNumbers() != null) {
				viewBean.setUserPhones(viewBean.getUser().getPhoneNumbers());
			}
			if (viewBean.getUser().getOfficeLocation() != null) {
				viewBean.setUserDeliveryOffice(viewBean.getUser().getOfficeLocation().getLocation());
			}
			for (LanguageObj lang : languageSingleton.getLanguages()) {
				if (viewBean.getUser().getLangPref() != null && viewBean.getUser().getLangPref().equals(lang.getCode()))
					viewBean.setUserLang(lang.getName());
			}
		}
	}

	private void setProjectPanelData(Project project) {
		viewBean.setProjectId(project.getProjectId());
		viewBean.setProjectName(project.getName());
		viewBean.setProjectType(project.getProjectType().getName());
		viewBean.setProjectCode(project.getProjectCode());
		viewBean.setTargetLevel(projectDao.resolveTargetLevel(project));
		viewBean.setCreatedBy(project.getExtAdminId());
		viewBean.setCreatedDate(project.getDateCreated());
		viewBean.setDueDate(project.getDueDate());
		viewBean.setProjectTypeCode(project.getProjectType().getProjectTypeCode());

		// Set the label flag
		viewBean.setHasProjectCodeLabel(false);
		if (ProjectType.ASSESSMENT_BY_DESIGN_ONLINE.equals(viewBean.getProjectTypeCode())
				|| ProjectType.GLGPS.equals(viewBean.getProjectTypeCode())
				|| ProjectType.READINESS_ASSMT_BUL.equals(viewBean.getProjectTypeCode())
				|| ProjectType.ERICSSON.equals(viewBean.getProjectTypeCode())) {
			viewBean.setHasProjectCodeLabel(true);
		}
	}

	private String createPageTitle() {
		String pageTitle = "Report Center";
		if (viewBean.isCompanyContext()) {
			pageTitle += " for Client";
		} else if (viewBean.isProjectContext()) {
			pageTitle += " for Project";
		} else if (viewBean.isUserContext()) {
			pageTitle += " for User";
		}
		return pageTitle;
	}

	private void initChangeContextMenu() {
		boolean clientContextPermitted = this.isClientContextPermitted();
		boolean projectContextPermitted = this.isProjectContextPermitted();

		if (viewBean.isCompanyContext()) {
			viewBean.setChangeContextRendered(false);
			viewBean.setChangeClientContextRendered(false);
			viewBean.setChangeProjectContextRendered(false);
		} else if (viewBean.isProjectContext()) {
			viewBean.setChangeContextRendered(clientContextPermitted);
			viewBean.setChangeClientContextRendered(clientContextPermitted);
			viewBean.setChangeProjectContextRendered(false);
		} else if (viewBean.isUserContext()) {
			viewBean.setChangeContextRendered(clientContextPermitted || projectContextPermitted);
			viewBean.setChangeClientContextRendered(clientContextPermitted);
			viewBean.setChangeProjectContextRendered(projectContextPermitted);
		}

	}

	private boolean isClientContextPermitted() {
		if (sessionBean.getCompany() != null) {
			if (sessionBean.isClientContextPermitted(sessionBean.getCompany().getCompanyId())) {
				return true;
			}
		}
		return false;
	}

	private boolean isProjectContextPermitted() {
		if (sessionBean.getProject() != null) {
			if (sessionBean.isProjectContextPermitted(sessionBean.getProject().getProjectId())) {
				return true;
			}
		}
		return false;
	}

	public void changeClientContext() {
		sessionBean.clearProject();

		this.setProject(null);
		this.setDataLoaded(false);

		viewBean.setProjectId(null);
		viewBean.setUser(null);
		viewBean.setProjectContext(false);
		viewBean.setUserContext(false);
		viewBean.setTabSwitcherRendered(true);

		refreshView();

	}

	public void changeProjectContext() {

		if (sessionBean.getProject() != null) {

			sessionBean.clearUser();
			this.setProject(sessionBean.getProject());
			sessionBean.setSelectedProject(this.getProject().getProjectId());

			this.setDataLoaded(false);

			viewBean.setProjectId(null);
			viewBean.setUser(null);
			viewBean.setProjectContext(false);
			viewBean.setUserContext(false);
			viewBean.setTabSwitcherRendered(true);

			refreshView();

		}
	}

	public void handleChangeProject() {

		if (sessionBean.getSelectedProject() == null) {
			this.changeClientContext();
			logger.trace("handleChangeProject, selected project: All Projects");
		} else if (sessionBean.getSelectedProject() != null
				&& sessionBean.isProjectContextPermitted(sessionBean.getSelectedProject())) {
			viewBean.setProjectId(getSessionBean().getSelectedProject());

			sessionBean.clearUser();
			sessionBean.setUser(null);
			viewBean.setUser(null);

			viewBean.setProjectContext(true);
			viewBean.setCompanyContext(false);
			viewBean.setUserContext(false);
			viewBean.setUserName(null);
			viewBean.setUserEmail(null);
			viewBean.setUserLang(null);
			this.dataLoaded = false;
			viewBean.setTabSwitcherRendered(true);
			viewBean.setReportManagerRendered(true);
			viewBean.setRequestDetailsRendered(false);
			viewBean.setProjectInfoCollapsed(false);

			logger.trace("handleChangeProject, selected project: {}", viewBean.getProjectId());
			this.refreshView();
		} else {
			logger.error("Project Context is Not Permitted");
		}
	}

	public void handleShowQueue() {
		logger.debug("Handling Show Queue");
		if (viewBean.getRequestId() > 0) {
			logger.debug("Request ID > 0: {}", viewBean.getRequestId());
			this.handleShowRequestDetails();
		} else {
			logger.debug("Request Id is not > 0: {}", viewBean.getRequestId());
			if (!dataLoaded) {
				logger.debug("Data is not loaded... checking is init repo");
				if (viewBean.isInitRepository()) {
					logger.debug("Init Repo True, select repo tab with javascript");
					RequestContext.getCurrentInstance().execute("handleReportSelectionChange(1);");
				} else {
					logger.debug("Init Repo Fale, select Queue tab with javascript");
					RequestContext.getCurrentInstance().execute("handleReportSelectionChange(0);");
				}
				logger.debug("Set data loaded true");
				setDataLoaded(true);
			}
		}
		logger.debug("Set tab switcher not rendered");
		viewBean.setTabSwitcherRendered(false);
	}

	public void filter(ActionEvent event) {
		// handle filtering of both tables at once, requests and reports
		List<ReportRepresentation> filteredReports = new ArrayList<ReportRepresentation>(
				viewBean.getRepositoryRecords());
		List<ReportRepresentation> removeReports = new ArrayList<ReportRepresentation>();

		List<GenerateReportsRequest> filteredRequests = new ArrayList<GenerateReportsRequest>(
				viewBean.getRepositoryRequestRecords());
		List<GenerateReportsRequest> removeRequests = new ArrayList<GenerateReportsRequest>();

		Date startDate = viewBean.getFromDate();
		Date endDate = viewBean.getToDate();

		if (!(endDate == null) || !(startDate == null)) {
			for (ReportRepresentation report : filteredReports) {
				Date requestDate = this.cleanDate(report.getRequestTime());
				// remove record if later than filter toDate
				if (!(endDate == null) && ((requestDate.after(endDate)))
						&& !requestDate.toString().equals(endDate.toString())) {
					removeReports.add(report);
				}
				// remove record if earlier than filter fromDate
				if (!(startDate == null)
						&& (requestDate.before(startDate) && !requestDate.toString().equals(startDate.toString()))) {
					removeReports.add(report);
				}
			}
			for (GenerateReportsRequest request : filteredRequests) {
				Date requestDate = this.cleanDate(request.getRequestTime());
				// remove record if later than filter toDate
				if (!(endDate == null) && ((requestDate.after(endDate)))
						&& !requestDate.toString().equals(endDate.toString())) {
					removeRequests.add(request);
				}
				// remove record if earlier than filter fromDate
				if (!(startDate == null)
						&& (requestDate.before(startDate) && !requestDate.toString().equals(startDate.toString()))) {
					removeRequests.add(request);
				}
			}
		}

		// replace records with those that include participant(s) listed in
		// AutoCompParts

		if (!(viewBean.getAutoCompPart() == null)) {
			logger.debug("filtering participants");
			boolean remove = true;
			List<Integer> requestIDs = new ArrayList<Integer>();
			for (ReportRepresentation report : filteredReports) {
				remove = true;
				for (ReportParticipantRepresentation participant : report.getParticipants()) {
					if (participant.getUsersId() == viewBean.getAutoCompPart().getUsersId()) {
						logger.debug("Found participant = {}; request id = {}; parent request id = {}",
								participant.getEmail(), report.getRequestId(), report.getParentRequestId());
						remove = false;
						requestIDs.add(Integer.valueOf(report.getParentRequestId()));
						break;
					}
				}
				if (remove) {
					removeReports.add(report);
				}
			}

			for (GenerateReportsRequest request : filteredRequests) {
				remove = true;
				for (Integer requestId : requestIDs) {
					if (requestId.intValue() == request.getRequestId()) {
						logger.debug("Keeping request = {}", request.getRequestId());
						remove = false;
						break;
					}
				}
				if (remove) {
					removeRequests.add(request);
				}
			}
		}

		filteredReports.removeAll(removeReports);
		filteredRequests.removeAll(removeRequests);

		viewBean.setFilteredRepositoryRequestRecords(filteredRequests);
		viewBean.setFilteredRepositoryRecords(filteredReports);

		// enableResetFilters();
		viewBean.setResetFiltersDisabled(false);

	}

	private Date cleanDate(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

	/*
		public void enableResetFilters() {
			if (viewBean.getFilteredRepositoryRecords().size() > 0
					|| viewBean.getFilteredRepositoryRequestRecords().size() > 0) {
				viewBean.setResetFiltersDisabled(false);
			} else {
				viewBean.setResetFiltersDisabled(true);
			}
		}
	*/

	// this is for datatables
	public void resetFilters() {
		resetFiltered();
		refreshData();
	}

	// this is only for filter dialog fields
	public void resetFiltered() {
		viewBean.setResetFiltersDisabled(true);
		viewBean.setFilteredRepositoryRecords(new ArrayList<ReportRepresentation>());
		viewBean.setFilteredRepositoryRequestRecords(new ArrayList<GenerateReportsRequest>());
		viewBean.clearFilterDialog();
		logger.debug("Resetting dialog filters to null");
	}

	private void loadReportsRepository(Integer clientId, Integer projectId, Integer userId) {

		if (clientId == null && projectId == null && userId == null) {
			return;
		}

		List<String> statuses = new ArrayList<String>();
		List<ReportRepresentation> records = new ArrayList<ReportRepresentation>();

		statuses.add(ReportRequestStatus.STATUS_STORED);
		if (userId != null) {
			logger.debug("Calling load Reports by Status with usersid: {}", userId);
			records = loadReportsByStatus(null, null, userId, statuses);
		} else if (clientId != null && projectId == null) {
			records = loadReportsByStatus(clientId, null, null, statuses);
		} else {
			records = loadReportsByStatus(null, projectId, null, statuses);
		}

		viewBean.setRepositoryRecords(records);
		viewBean.setFilteredRepositoryRecords(records);
		viewBean.initialize();
	}

	private void loadRequests(Integer clientId, Integer projectId, Integer userId) {
		if (clientId == null && projectId == null && userId == null) {
			return;
		}
		int maxRecords = Integer.valueOf(viewBean.getMaxRecords());
		int page = Integer.valueOf(viewBean.getPage());
		List<GenerateReportsRequest> requests = new ArrayList<GenerateReportsRequest>();
		GenerateReportsRequestList list = new GenerateReportsRequestList();
		List<String> statuses = new ArrayList<String>();
		if (viewBean.getCurrentTabId().equals("queueTab")) {
			statuses.add(ReportRequestStatus.STATUS_CANCELLED);
			statuses.add(ReportRequestStatus.STATUS_ERROR);
			statuses.add(ReportRequestStatus.STATUS_GENERATED);
			statuses.add(ReportRequestStatus.STATUS_PROCESSING);
			statuses.add(ReportRequestStatus.STATUS_QUEUED);
			statuses.add(ReportRequestStatus.STATUS_QUEUEING_FAILED);
			statuses.add(ReportRequestStatus.STATUS_REQUESTED);
			// TODO: add recordset nav to queue Tab
			maxRecords = 1000;

		} else {
			statuses.add(ReportRequestStatus.STATUS_STORED);
		}
		if (userId != null) {
			list = reportRequestHelper.getRequestListByUser(userId, maxRecords, page, statuses);

		} else if (clientId != null && projectId == null) {
			list = reportRequestHelper.getRequestListByCompany(clientId, maxRecords, page, statuses);

		} else {
			list = reportRequestHelper.getRequestListByProject(projectId, maxRecords, page, statuses);
		}
		requests = list.getReportsRequests();
		if (requests == null) {
			viewBean.setRequestRecordCount("0");
		} else {
			viewBean.setRequestRecordCount(String.valueOf(requests.size()));
		}
		viewBean.setTotalRequestCount(String.valueOf(list.getTotalRecordCount()));
		GenerateReportsRequestList queueRequests = new GenerateReportsRequestList();
		queueRequests.setReportsRequests(new ArrayList<GenerateReportsRequest>());
		GenerateReportsRequestList repositoryRequests = new GenerateReportsRequestList();
		repositoryRequests.setReportsRequests(new ArrayList<GenerateReportsRequest>());

		if (!(requests == null)) {
			/*
			for (GenerateReportsRequest grr : requests) {
				if (!grr.getStatus().equals("STORED")) {
					queueRequests.getReportsRequests().add(grr);
				} else {
					repositoryRequests.getReportsRequests().add(grr);
				}
			}
			*/
			if (requests.size() > 0) {

				viewBean.setRequestRecordCount(String.valueOf(requests.size()));
				viewBean.setTotalRequestCount(String.valueOf(list.getTotalRecordCount()));
				// viewBean.setTotalRequestCount(String.valueOf(list.g));
				logger.debug("Total Record Count: {}", viewBean.getTotalRecordCount());
				Date oldestDate = requests.get(requests.size() - 1).getRequestTime();
				Date newestDate = requests.get(0).getRequestTime();

				viewBean.setOldestRecordDateString(DateFormat.getInstance().format(oldestDate));
				viewBean.setNewestRecordDateString(DateFormat.getInstance().format(newestDate));
				logger.debug("Oldest: {}, Newest: {}", oldestDate, newestDate);
			}
		}
		if (viewBean.getCurrentTabId().equals("queueTab")) {
			viewBean.setFilteredRequestRecords(requests);
			viewBean.setRequestRecords(requests);
		} else {
			viewBean.setRepositoryRequestRecords(requests);
			viewBean.setFilteredRepositoryRequestRecords(requests);
		}
	}

	public void refreshData() {
		logger.debug("Refreshing data");
		String currentTab = viewBean.getCurrentTabId();
		logger.debug("Current Tab: {}", currentTab);
		if (currentTab.equals("queueTab")) {
			viewBean.setInitRepository(false);
		} else {
			// set is repository tab
			viewBean.setInitRepository(true);
		}
		viewBean.setSelectedRepositoryRecords(new ArrayList<ReportRepresentation>());
		viewBean.setSelectedDetailRecord(null);
		// note: we need to load REQUESTS AND REPORTS at the same time so that
		// the Filter dialog can work.
		// NW: But not if we are on the QUEUE TAB
		if (viewBean.isUserContext()) {
			if (viewBean.isInitRepository()) {
				loadReportsRepository(null, null, viewBean.getUser().getUsersId());
			}
			loadRequests(null, null, viewBean.getUser().getUsersId());
		} else if (viewBean.isProjectContext()) {
			if (viewBean.isInitRepository()) {
				loadReportsRepository(null, viewBean.getProjectId(), null);
			}
			loadRequests(null, viewBean.getProjectId(), null);
		} else {
			viewBean.setCompanyContext(true);
			if (viewBean.isInitRepository()) {
				loadReportsRepository(sessionBean.getCompany().getCompanyId(), null, null);
			}
			loadRequests(sessionBean.getCompany().getCompanyId(), null, null);
		}
		viewBean.initialize();
	}

	public void changeMaxRecords(AjaxBehaviorEvent event) {
		logger.debug("Max Records: {}", viewBean.getMaxRecords());
		viewBean.setPage(String.valueOf(0));
		viewBean.setReportsPage(String.valueOf(0));
		viewBean.setSelectedRepositoryRequestRecord(null);
		refreshData();
	}

	public void getNextPage() {
		int currentPage = Integer.valueOf(viewBean.getPage());
		currentPage = currentPage + 1;
		viewBean.setPage(String.valueOf(currentPage));
		viewBean.setSelectedRepositoryRequestRecord(null);
		refreshData();
	}

	public void getPrevPage() {
		int currentPage = Integer.valueOf(viewBean.getPage());
		currentPage = currentPage - 1;
		viewBean.setPage(String.valueOf(currentPage));
		viewBean.setSelectedRepositoryRequestRecord(null);
		refreshData();
	}

	public void getNextReportsPage() {
		int currentPage = Integer.valueOf(viewBean.getReportsPage());
		int currentlyViewing = (currentPage + 1) * Integer.valueOf(viewBean.getMaxRecords());
		if (((currentlyViewing) <= Integer.valueOf(viewBean.getTotalRecordCount()))) {
			currentPage = currentPage + 1;
			viewBean.setReportsPage(String.valueOf(currentPage));
			refreshData();
		}
	}

	public void getPrevReportsPage() {
		int currentPage = Integer.valueOf(viewBean.getReportsPage());
		if (currentPage != 0) {
			currentPage = currentPage - 1;
			viewBean.setReportsPage(String.valueOf(currentPage));
			refreshData();
		}
	}

	public String resolvePriority(int priority) {
		switch (priority) {
		case 1:
			return "HIGH";
		case 2:
			return "LOW";
		default:
			return "LOW";
		}
	}

	private List<ReportRepresentation> loadReportsByStatus(Integer clientId, Integer projectId, Integer userId,
			List<String> statuses) {
		logger.debug("doing secure rest call");
		List<ReportRepresentation> reportList = new ArrayList<ReportRepresentation>();
		int maxRecords = Integer.valueOf(viewBean.getMaxRecords());
		int page = Integer.valueOf(viewBean.getReportsPage());
		logger.debug("Page: {}", page);
		ReportRepresentationList list = new ReportRepresentationList();
		if (userId != null) {
			logger.debug("Calling Helper with userId: {}", userId);
			list = reportRequestHelper.getReportListByUser(userId, maxRecords, page, statuses);
		} else if (clientId != null) {
			list = reportRequestHelper.getReportListByCompany(clientId, maxRecords, page, statuses);
		} else if (projectId != null) {
			list = reportRequestHelper.getReportListByProject(projectId, maxRecords, page, statuses);
		}
		reportList = list.getReports();

		if (reportList != null) {
			viewBean.setReportRecordCount(String.valueOf(reportList.size()));
			viewBean.setTotalRecordCount(String.valueOf(list.getTotalRecordCount()));
			logger.debug("Total Record Count: {}", viewBean.getTotalRecordCount());
			Date oldestDate = reportList.get(reportList.size() - 1).getStatusTime();
			Date newestDate = reportList.get(0).getStatusTime();

			viewBean.setOldestRecordDateString(DateFormat.getInstance().format(oldestDate));
			viewBean.setNewestRecordDateString(DateFormat.getInstance().format(newestDate));
			logger.debug("Oldest: {}, Newest: {}", oldestDate, newestDate);

		} else {
			logger.error("Report request helper returned null");
		}

		return reportList;
	}

	public String getPptGroupLabel(ReportRepresentation record) {

		String label = "";
		ReportType reportType = reportingSingleton.getReportType(record.getCode());

		if (reportType.isGroupReport()) {
			label = (record.getTitle() != null) ? record.getTitle() : "[Group]";
		} else {
			if (record.getParticipants() != null && !record.getParticipants().isEmpty()) {
				label = formatParticipantDetails(record.getParticipants().get(0));
			}
		}

		return label;
	}

	public String getReportProjectLabel(ReportRepresentation report) {
		return this.getProjectListString(report.getParticipants());
	}

	public String getReportCreateTypeLabel(ReportRepresentation report) {
		String typeName = "";
		if (report.getCreateType() == 1) {
			typeName = "Manually Uploaded";
		} else if (report.getCreateType() == 2) {
			typeName = "Manually Requested";
		} else if (report.getCreateType() == 3) {
			typeName = "Auto Created";
		} else {
			typeName = "Unknown";
		}
		return typeName;
	}

	public String formatParticipantDetails(ReportParticipantRepresentation ppt) {
		return ppt.getLastName() + ", " + ppt.getFirstName() + " (" + ppt.getEmail() + ")";
	}

	public String getLanguageLabel(String code) {
		Language lang = languageSingleton.getLanguageByCode(code);
		return (lang != null) ? lang.getName() : "";
	}

	private String getSubjectFullName(Integer subjectId) {
		String fullName = "";
		if (subjectId != null) {
			ExternalSubjectDataObj subject = authorizationService.getSubjectMetadata(subjectId);
			if (subject != null) {
				fullName = subject.getFirstLastEmail();
			}
		}
		return fullName;
	}

	public void handleTabChange(TabChangeEvent event) {
		Tab activeTab = event.getTab();
		logger.debug("Active tab client id: {}, id: {}", activeTab.getClientId(), activeTab.getId());
		viewBean.setCurrentTabId(activeTab.getId());
		viewBean.setReturnToTabId(activeTab.getId());
		if (activeTab.getId().equals("queueTab")) {
			viewBean.setViewType("0");
		}
		viewBean.setPage("0");
		refreshData();
	}

	public void handleCancelFromDetails() {

		if (viewBean.getSelectedDetailRecords() == null || viewBean.getSelectedDetailRecords().isEmpty()) {
			logger.debug("Selected Records collection is empty");
			return;
		}
		for (ReportRepresentation rep : viewBean.getSelectedDetailRecords()) {

			rep.setStatus(ReportRequestStatus.STATUS_CANCELLED);

			ResultObj result = reportRequestHelper.updateReportRequest(rep);

			if (result instanceof ErrorObj) {
				// TODO: handle error -- alert dialog or something
				logger.debug("Got Error: {}", ((ErrorObj) result).getErrorMsg());
				logger.debug("Got Error Code: {}", ((ErrorObj) result).getErrorCode());

			} else {
				logger.debug("Success: {}", result.getStatus());
			}
		}
		handleShowRequestDetails();
	}

	public void handleShowRequestDetails() {
		logger.debug("In handleShowRequestDetails");
		viewBean.setPageTitle(viewBean.getPageTitle() + " > Report Request Details");
		viewBean.setReportManagerRendered(false);
		viewBean.setRequestDetailsRendered(true);
		viewBean.setProjectInfoCollapsed(true);
		viewBean.setSelectedDetailRecord(null);
		GenerateReportsRequest repReq = null;
		logger.debug("Current Tab: {}", viewBean.getCurrentTabId());
		int subjectId = 0;
		if (viewBean.isUserContext()) {
			// need to authorize
			if (sessionBean.isClientContextPermitted(sessionBean.getCompany().getCompanyId())) {
				subjectId = 0;// no further authorization needed
			} else {
				subjectId = sessionBean.getSubjectId();
			}
		}

		if (viewBean.getCurrentTabId().equals("repositoryTab")
				|| (viewBean.getCurrentTabId().equals("reportTab") && viewBean.getReturnToTabId().equals(
						"repositoryTab"))) {
			logger.debug("On Repository Tab");
			if (viewBean.getRequestId() > 0) {
				repReq = reportRequestHelper.getGenerateRequestDetails(viewBean.getRequestId(), subjectId);
			} else {
				repReq = reportRequestHelper.getGenerateRequestDetails(viewBean.getSelectedRepositoryRequestRecord()
						.getRequestId(), subjectId);
			}
			viewBean.setReturnToTabId("repositoryTab");

		} else {
			logger.debug("On Queue Tab");
			if (viewBean.getRequestId() > 0) {
				repReq = reportRequestHelper.getGenerateRequestDetails(viewBean.getRequestId(), subjectId);
			} else {
				repReq = reportRequestHelper.getGenerateRequestDetails(viewBean.getSelectedRequestRecord()
						.getRequestId(), subjectId);
			}
			viewBean.setReturnToTabId("queueTab");
		}
		logger.trace("Got {} participants", repReq.getParticipants().size());
		logger.trace("Got {} reports", repReq.getReportTypeList().size());
		viewBean.setRequestParticipants(repReq.getParticipants());
		viewBean.setRequestDetailsReports(repReq.getReportTypeList());
		viewBean.setFilteredDetailRecords(repReq.getReportTypeList());
		viewBean.setSelectedRequestRecord(repReq);
		viewBean.setCurrentTabId("reportsTab");
		viewBean.setRequestId(0);
		viewBean.setRequestProjectList(this.getProjectListString(repReq.getParticipants()));

		// We need update selected report after refreshing because report status
		// changes
		this.updateSelectedReports(viewBean.getRequestDetailsReports(), viewBean.getSelectedDetailRecords());
		this.disableButtonsBaseOnStatus(null, viewBean.getSelectedDetailRecords());
	}

	private void updateSelectedReports(List<ReportRepresentation> allReports, List<ReportRepresentation> selectedReports) {
		List<ReportRepresentation> newSelectedReports = new ArrayList<ReportRepresentation>();
		for (ReportRepresentation selectedReport : selectedReports) {
			for (ReportRepresentation report : allReports) {
				if (selectedReport.getRequestId() == report.getRequestId()) {
					newSelectedReports.add(report);
					break;
				}
			}
		}
		selectedReports.addAll(newSelectedReports);
		selectedReports.retainAll(newSelectedReports);
	}

	private String getProjectListString(List<ReportParticipantRepresentation> ppts) {
		String projectList = "";
		Map<Integer, String> pids = new HashMap<Integer, String>();
		for (ReportParticipantRepresentation rpr : ppts) {
			pids.put(rpr.getProjectId(), rpr.getProjectName());
		}
		for (Object key : pids.keySet()) {
			projectList += pids.get(key) + " [" + key.toString() + "], ";
		}
		projectList = projectList.substring(0, projectList.lastIndexOf(","));
		return projectList;
	}

	public void handleHideRequestDetails() {
		viewBean.setPageTitle(createPageTitle());
		viewBean.setReportManagerRendered(true);
		viewBean.setRequestDetailsRendered(false);
		viewBean.setProjectInfoCollapsed(false);
		viewBean.setCurrentTabId(viewBean.getReturnToTabId());

		if (!dataLoaded) {
			if (viewBean.getReturnToTabId().equalsIgnoreCase("repositoryTab")) {
				viewBean.setInitRepository(true);
			} else {
				viewBean.setInitRepository(false);
			}
			this.handleShowQueue();
		}

	}

	public void doNothing() {
		logger.debug("Do nothing here");
	}

	public void handleRepositoryRowSelect() {
		logger.trace("Selecting Repository > Reports");
		this.disableButtonsBaseOnStatus(null, viewBean.getSelectedRepositoryRecords());
	}

	public void handleRequestRowSelect() {
		logger.trace("Selecting Queue Request");
		this.disableButtonsBaseOnStatus(viewBean.getSelectedRequestRecord(), null);
	}

	public void handleRepoRequestRowSelect() {
		logger.trace("Selecting Repository > Requests");
		this.disableButtonsBaseOnStatus(viewBean.getSelectedRepositoryRequestRecord(), null);
	}

	public void handleDetailsRowSelect() {
		logger.trace("Selecting Request Details > Reports");
		// this.updateSelectedRecordsFromDetails();
		this.disableButtonsBaseOnStatus(null, viewBean.getSelectedDetailRecords());

	}

	public void handleDetailsRowUnselect() {
		logger.trace("Unselecting Request Details > Reports");
		// this.updateSelectedRecordsFromDetails();
		this.disableButtonsBaseOnStatus(null, viewBean.getSelectedDetailRecords());
	}

	public void handleRepoRequestRowUnselect() {
		logger.trace("Unselecting Repository > Requests");
		this.disableButtonsBaseOnStatus(viewBean.getSelectedRepositoryRequestRecord(), null);
	}

	public void handleRequestRowUnselect() {
		logger.trace("Unselecting Queue Request");
		this.disableButtonsBaseOnStatus(viewBean.getSelectedRequestRecord(), null);

	}

	public void handleRepositoryRowUnselect() {
		logger.trace("Unselecting Repository > Reports");
		this.disableButtonsBaseOnStatus(null, viewBean.getSelectedRepositoryRecords());
	}

	public void handleViewTypeSelect() {
		logger.trace("set view type: {}", viewBean.getViewType());
		viewBean.setPage("0");
	}

	/*
	private void updateSelectedRecordsFromDetails() {
		viewBean.setSelectedRepositoryRecords(new ArrayList<ReportRepresentation>());
		List<ReportRepresentation> selectedReportReps = viewBean.getSelectedDetailRecords();
		if (!(selectedReportReps == null)) {
			for (ReportRepresentation rep : viewBean.getSelectedDetailRecords()) {
				if (rep.getStatus().equalsIgnoreCase(ReportRequestStatus.STATUS_STORED)) {
					viewBean.getSelectedRepositoryRecords().add(rep);
				}
			}

		}
	}
	*/

	public void createReports() {
		logger.trace("Create reports");
	}

	public void resubmitSelectedReports() {
		boolean unsupportedType = false;

		if (viewBean.getCurrentTabId().equals("reportsTab")) {
			if (isReportTypeExists(viewBean.getSelectedDetailRecords(), ReportType.KFP_SLATE)) {
				unsupportedType = true;
			}
		} else if (viewBean.getCurrentTabId().equals("repositoryTab")) {
			if (isReportTypeExists(viewBean.getSelectedRepositoryRecords(), ReportType.KFP_SLATE)) {
				unsupportedType = true;
			}
		}

		if (unsupportedType) {
			LabelUtils.addErrorMessage("recreateReports", "Unsupported Report Type:",
					"ALP Talent Grid (v1.0) is no longer supported.");
		}

		viewBean.setRecreateInDialogDisabled(unsupportedType);
		viewBean.setRecreateRequestNameDisabled(unsupportedType);

		viewBean.setRecreateRequestName("Recreate - ");

		logger.trace("Resubmit reports");
	}

	public void resubmitReports() {
		logger.debug("Resubmitting reports");

		if (!this.validateResubmit()) {
			return;
		}

		GenerateReportsRequest reportsRequest = new GenerateReportsRequest();
		reportsRequest.setPrincipal((String) SecurityUtils.getSubject().getPrincipal());
		reportsRequest.setSubjectId(sessionBean.getSubjectId());
		reportsRequest.setRequestName(viewBean.getRecreateRequestName());
		reportsRequest.setPriority(ReportGenerateRequest.REQUEST_PRIORITY_LOW);
		reportsRequest.setReportTypeList(new ArrayList<ReportRepresentation>());
		List<ReportRepresentation> repsList = new ArrayList<ReportRepresentation>();
		if (viewBean.getCurrentTabId().equals("reportsTab")) {
			repsList = viewBean.getSelectedDetailRecords();
		} else if (viewBean.getCurrentTabId().equals("repositoryTab")) {
			repsList = viewBean.getSelectedRepositoryRecords();
		}
		logger.debug("CURRENT TAB IS " + viewBean.getCurrentTabId() + " view = " + viewBean.getViewType());
		List<ReportParticipantRepresentation> pptList = new ArrayList<ReportParticipantRepresentation>();
		for (ReportRepresentation rr : repsList) {
			if (rr.getCreateType() != ReportRequest.CREATE_TYPE_UPLOAD) {
				for (ReportParticipantRepresentation ppt : rr.getParticipants()) {
					logger.debug("Got ppt: {}", ppt.getLastName());
					boolean inList = false;

					for (ReportParticipantRepresentation ppt2 : pptList) {

						if (ppt2.getUsersId() == ppt.getUsersId()) {
							inList = true;
						}
					}
					if (!inList) {
						logger.debug("Ppt report lang: {}", ppt.getReportLang());
						pptList.add(ppt);
					}
				}
				rr.setParticipants(null);
				reportsRequest.getReportTypeList().add(rr);
			}

		}
		logger.debug("Adding {} participants to request, and {} reports", pptList.size(), reportsRequest
				.getReportTypeList().size());
		reportsRequest.setParticipants(pptList);
		if (pptList.size() > 0) {
			reportsRequest.setPriority(ReportGenerateRequest.REQUEST_PRIORITY_LOW);
		} else {
			reportsRequest.setPriority(ReportGenerateRequest.REQUEST_PRIORITY_HIGH);
		}
		reportRequestHelper.submitReportsRequest(reportsRequest);

		// when coming from repo tab.
		if (viewBean.isCompanyContext()) {
			this.loadRequests(sessionBean.getCompany().getCompanyId(), null, null);
		} else if (viewBean.isProjectContext()) {
			this.loadRequests(null, sessionBean.getSelectedProject(), null);
		} else if (viewBean.isUserContext()) {
			this.loadRequests(null, null, viewBean.getUser().getUsersId());
		}

	}

	private boolean validateResubmit() {
		boolean valid = true;
		if (viewBean.getRecreateRequestName().isEmpty()) {
			LabelUtils.addErrorMessage("recreateReports", "Request Name:", "Please enter request name.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			logger.error("Recreate report validation failed, Request Name is not set.");
			valid = false;
		}
		return valid;
	}

	private boolean isReportTypeExists(List<ReportRepresentation> reports, String type) {
		for (ReportRepresentation report : reports) {
			if (report.getCode().equals(type)) {
				return true;
			}
		}
		return false;
	}

	public void viewReportDetails() {
		if (viewBean.getCurrentTabId().equals("repositoryTab")) {
			viewBean.setSelectedDetailRecord(viewBean.getSelectedRepositoryRecords().get(0));
		} else if (viewBean.getCurrentTabId().equals("reportsTab")) {
			viewBean.setSelectedDetailRecord(viewBean.getSelectedDetailRecords().get(0));
		}

		logger.trace("View report details");
	}

	public void selectUnselectAll() {
		logger.trace("Select\\Unselect All");

		if (viewBean.getCurrentTabId().equals("repositoryTab")) {
			if (viewBean.isSelectAll()) {
				viewBean.setSelectedRepositoryRecords(viewBean.getRepositoryRecords());
				viewBean.setSelectAll(false);
			} else {
				viewBean.setSelectedRepositoryRecords(null);
				viewBean.setSelectAll(true);
			}
			this.disableButtonsBaseOnStatus(null, viewBean.getSelectedRepositoryRecords());
		} else if (viewBean.getCurrentTabId().equals("reportsTab")) {
			if (getReportCenterViewBean().isSelectAll()) {
				viewBean.setSelectedDetailRecords(viewBean.getRequestDetailsReports());
				viewBean.setSelectAll(false);
			} else {
				viewBean.setSelectedDetailRecords(null);
				viewBean.setSelectAll(true);
			}
			this.disableButtonsBaseOnStatus(null, viewBean.getSelectedDetailRecords());
		}
	}

	/*
		private void unselectAll() {
			// viewBean.setSelectedRequestRecord(null);
			// viewBean.setSelectedRepositoryRequestRecord(null);
			viewBean.setSelectedRepositoryRecords(new ArrayList<ReportRepresentation>());
			viewBean.setSelectedDetailRecords(new ArrayList<ReportRepresentation>());
			// viewBean.setSelectedDetailPpt(null);
			viewBean.setSelectAll(true);
		}
	*/
	public void downloadSelectedReports() {
		logger.trace("Download selected reports");

		ReportRepresentationList list = new ReportRepresentationList();

		if (viewBean.getCurrentTabId().equals("repositoryTab")) {
			list.setReports(filterReportsByStatus(viewBean.getSelectedRepositoryRecords(),
					ReportRequestStatus.STATUS_STORED));
		} else if (viewBean.getCurrentTabId().equals("reportsTab")) {
			list.setReports(filterReportsByStatus(viewBean.getSelectedDetailRecords(),
					ReportRequestStatus.STATUS_STORED));
		}

		logger.debug("Requesting download of {} reports", list.getReports().size());
		StreamedContent content = null;
		try {
			content = reportRequestHelper.downloadReportFiles(list);
		} catch (PalmsException e) {
			// TODO: expose error message to UI.
			logger.error("Error Downloading Selected Reports: {}, {}", e.getMessage(), e.getErrorDetail());
		}
		viewBean.setReportDownload(content);

	}

	private List<ReportRepresentation> filterReportsByStatus(List<ReportRepresentation> reports, String status) {
		List<ReportRepresentation> filteredList = new ArrayList<ReportRepresentation>();
		if (reports != null) {
			for (ReportRepresentation report : reports) {
				if (report.getStatus().equals(status)) {
					filteredList.add(report);
				}
			}
		}
		return filteredList;
	}

	public void initDeleteSelectedReports() {
		logger.trace("Initialize Delete Selected Reports ..............");
	}

	public void initDeleteSelectedRequest() {
		logger.trace("Initialize Delete Selected Request ..............");
	}

	public void deleteSelectedReports() {
		int deleteCount = 0;
		if (this.viewBean.getCurrentTabId().equals("repositoryTab")) {
			logger.warn("About to delete selected reports from REPOSITORY REPORTS view ....");
			this.deleteReportRequests(viewBean.getSelectedRepositoryRecords());
			viewBean.getRepositoryRecords().removeAll(viewBean.getSelectedRepositoryRecords());
			viewBean.setFilteredRepositoryRecords(viewBean.getRepositoryRecords());
			viewBean.setSelectedRepositoryRecords(new ArrayList<ReportRepresentation>());
		} else {
			logger.warn("About to delete Selected Reports from REQUEST DETAILS view ....");
			this.deleteReportRequests(viewBean.getSelectedDetailRecords());
			deleteCount = viewBean.getSelectedDetailRecords().size();
			viewBean.getRequestDetailsReports().removeAll(viewBean.getSelectedDetailRecords());
			viewBean.setFilteredDetailRecords(viewBean.getRequestDetailsReports());
			viewBean.setSelectedDetailRecords(new ArrayList<ReportRepresentation>());
		}
		GenerateReportsRequest request = viewBean.getSelectedRequestRecord();
		if (request != null) {
			request.setReportRequestCount(request.getReportRequestCount() - deleteCount);
			viewBean.setSelectedRequestRecord(request);
		}

	}

	public void deleteSelectedRequest() {
		logger.trace("Delete selected Request");
		GenerateReportsRequest request;
		if (viewBean.getCurrentTabId().equals("queueTab")) {
			request = viewBean.getSelectedRequestRecord();
			this.deleteReportRequests(reportRequestHelper.getReportRepresentations(request));
			viewBean.getRequestRecords().remove(request);
			viewBean.setFilteredRequestRecords(viewBean.getRequestRecords());
			viewBean.setSelectedRequestRecord(null);
		} else {
			request = viewBean.getSelectedRepositoryRequestRecord();
			this.deleteReportRequests(reportRequestHelper.getReportRepresentations(request));
			viewBean.getRepositoryRequestRecords().remove(request);
			viewBean.setFilteredRepositoryRequestRecords(viewBean.getRepositoryRequestRecords());
			viewBean.setSelectedRepositoryRequestRecord(null);
		}
		logger.debug("Deleted request record [{}] {}", request.getRequestId(), request.getRequestName());
	}

	private void deleteReportRequests(List<ReportRepresentation> rptReps) {
		for (ReportRepresentation rpt : rptReps) {
			rpt.setStatus(ReportRequestStatus.STATUS_DELETED);
			reportRequestHelper.updateReportRequest(rpt);
		}

		doContextAudit(SecurityUtils.getSubject(), EntityAuditLogger.OP_DELETE_REPORT_REQUEST, "Report Request",
				rptReps.size());

		// AV: No need to refresh data its handled by deleting items from the
		// list its faster than reloading all the data.
		// refreshData();

	}

	private void doContextAudit(Subject subject, String operation, String entityType, int numRecords) {
		String context = "";
		String id = "";
		String name = "";
		if (viewBean.isProjectContext()) {
			context = "project";
			id = String.valueOf(sessionBean.getSelectedProject());
			name = viewBean.getProjectName();
		}
		if (viewBean.isCompanyContext()) {
			context = "company";
			id = String.valueOf(sessionBean.getCompany().getCompanyId());
			name = sessionBean.getCompany().getCoName();
		} else if (viewBean.isUserContext()) {
			context = "user";
			id = String.valueOf(viewBean.getUser().getUsersId());
			name = viewBean.getUser().getFirstname() + " " + viewBean.getUser().getLastname();
		}
		auditLog.info("{} performed {} operation on {} {}s, in context {}: [{}] {}",
				new Object[] { subject.getPrincipal(), operation, numRecords, entityType, context, id, name });

	}

	public boolean isReportAuthorized(ReportRepresentation report) {
		if (!viewBean.isUserContext()) {
			// if user has rights to entire project or company, we
			// can assume all reports are authorized.... right?
			return true;
		}
		List<Integer> distinctProjectIds = new ArrayList<Integer>();

		for (ReportParticipantRepresentation ppt : report.getParticipants()) {
			if (!distinctProjectIds.contains(ppt.getProjectId())) {
				distinctProjectIds.add(ppt.getProjectId());
			}

		}
		boolean allProjectsAuthorized = true;
		for (Integer i : distinctProjectIds) {
			if (!sessionBean.isProjectContextPermitted(i)) {
				allProjectsAuthorized = false;
				break;
			}
		}
		if (allProjectsAuthorized) {
			return true;
		} else {
			// check perms for each project user
			Subject subject = SecurityUtils.getSubject();

			for (ReportParticipantRepresentation ppt : report.getParticipants()) {
				ProjectUser puser = projectUserDao.findByProjectIdAndUserId(ppt.getProjectId(), ppt.getUsersId());
				if (!subject.isPermitted(EntityType.PROJECT_USER_TYPE + ":allowchildren:" + puser.getRowId())) {

					return false;
				}
			}
		}

		return true;
	}

	public void downloadFromRequest() {
		StreamedContent content = null;
		try {
			if (viewBean.getCurrentTabId().equals("queueTab")) {
				content = reportRequestHelper.downloadAllRequested(viewBean.getSelectedRequestRecord());
			} else {
				// repositoryRequest view
				content = reportRequestHelper.downloadAllRequested(viewBean.getSelectedRepositoryRequestRecord());
			}
		} catch (PalmsException p) {
			p.printStackTrace();
		}
		viewBean.setReportDownload(content);
	}

	public void refreshView() {
		setFirstTime(true);
		initView();
	}

	private void disableButtonsBaseOnStatus(GenerateReportsRequest selection, List<ReportRepresentation> selections) {
		List<String> cancelStatuses = new ArrayList<String>(Arrays.asList(ReportRequestStatus.STATUS_QUEUED,
				ReportRequestStatus.STATUS_QUEUEING_FAILED, ReportRequestStatus.STATUS_REQUESTED));
		List<String> downloadStatuses = new ArrayList<String>(Arrays.asList(ReportRequestStatus.STATUS_STORED));

		boolean cancelDisabled = true;
		boolean downloadDisabled = true;
		boolean moreInfoDisabled = true;

		if (selections != null && selection == null) {

			moreInfoDisabled = (selections.size() == 1) ? false : true;
			cancelDisabled = isReportExistWithStatuses(cancelStatuses, selections) ? false : true;
			downloadDisabled = isReportExistWithStatuses(downloadStatuses, selections) ? false : true;

			viewBean.setCancelButtonDisabled(cancelDisabled);
			viewBean.setMoreInfoDisabled(moreInfoDisabled);
			viewBean.setDownloadReportDisabled(downloadDisabled);

			// Disable Recreat button is one of the selected reports was
			// manually uploaded
			viewBean.setRecreateDisabled(isManuallyUploaded(selections));

			// logger.debug(">>>>>>>>>>>>>>> selections.size = {}",
			// selections.size());
			// Disable Recreate button if there are multiple reports selected
			if (selections.size() > 1) {
				viewBean.setRecreateDisabled(true);
			}

		} else if (selection != null && selections == null) {
			for (String status : downloadStatuses) {
				if (selection.getStatus().equalsIgnoreCase(status)) {
					downloadDisabled = false;
					break;
				}
			}
			viewBean.setDownloadAllDisabled(downloadDisabled);
		}

		/*
				logger.debug(">>>>>>>>>>>>>>> downloadDisabled = {}", downloadDisabled);
				logger.debug(">>>>>>>>>>>>>>> cancelDisabled = {}", cancelDisabled);
				logger.debug(">>>>>>>>>>>>>>> moreInfoDisabled = {}", moreInfoDisabled);
		*/
	}

	private boolean isManuallyUploaded(List<ReportRepresentation> list) {
		for (ReportRepresentation record : list) {
			if (record.getCreateType() == ReportRequest.CREATE_TYPE_UPLOAD) {
				return true;
			}
		}
		return false;
	}

	private boolean isReportExistWithStatuses(List<String> statuses, List<ReportRepresentation> list) {
		boolean exists = false;
		for (String status : statuses) {
			for (ReportRepresentation record : list) {
				if (record.getStatus().equalsIgnoreCase(status)) {
					exists = true;
					break;
				}
			}
		}
		return exists;
	}

	// Getters/Setters
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ReportCenterViewBean getReportCenterViewBean() {
		return viewBean;
	}

	public void setReportCenterViewBean(ReportCenterViewBean reportCenterViewBean) {
		this.viewBean = reportCenterViewBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public int getCounter() {
		return counter++;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public ReportRequestHelper getReportRequestHelper() {
		return reportRequestHelper;
	}

	public void setReportRequestHelper(ReportRequestHelper reportRequestHelper) {
		this.reportRequestHelper = reportRequestHelper;
	}

	public boolean isDataLoaded() {
		return dataLoaded;
	}

	public void setDataLoaded(boolean dataLoaded) {
		this.dataLoaded = dataLoaded;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}