package com.pdinh.presentation.helper;

import java.io.Serializable;

public class CourseStatusColumnModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private String courseCode;
	private String header;
	
	public CourseStatusColumnModel(String courseCode, String header) {
		this.courseCode = courseCode;
		this.header = header;
	}
	
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
}
