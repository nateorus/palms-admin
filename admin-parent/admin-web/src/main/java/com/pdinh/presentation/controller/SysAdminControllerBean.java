package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.RoleContextDao;
import com.pdinh.auth.data.dao.RoleDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.data.singleton.ExternalSubjectCache;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.presentation.converter.RoleConverter;
import com.pdinh.presentation.helper.ReportRequestHelper;
import com.pdinh.presentation.helper.RoleColumnModel;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.LinkedAccountViewBean;
import com.pdinh.presentation.view.SysAdminViewBean;
import com.pdinh.rest.security.APIAuthorizationServiceLocal;

@ManagedBean(name = "sysAdminControllerBean")
@ViewScoped
public class SysAdminControllerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SysAdminControllerBean.class);
	@Resource(mappedName = "application/config_properties")
	private Properties properties;

	@ManagedProperty(name = "sysAdminViewBean", value = "#{sysAdminViewBean}")
	private SysAdminViewBean sysAdminViewBean;

	@ManagedProperty(name = "auditLogControllerBean", value = "#{auditLogControllerBean}")
	private AuditLogControllerBean auditLogControllerBean;

	@ManagedProperty(name = "editRolesControllerBean", value = "#{editRolesControllerBean}")
	private EditRolesControllerBean editRolesControllerBean;

	@ManagedProperty(name = "roleConverter", value = "#{roleConverter}")
	private RoleConverter roleConverter;

	@ManagedProperty(name = "linkedAccountViewBean", value = "#{linkedAccountViewBean}")
	private LinkedAccountViewBean linkedAccountViewBean;

	@ManagedProperty(name = "linkedAccountControllerBean", value = "#{linkedAccountControllerBean}")
	private LinkedAccountControllerBean linkedAccountControllerBean;

	@ManagedProperty(name = "systemHeartbeatControllerBean", value = "#{systemHeartbeatControllerBean}")
	private SystemHeartbeatControllerBean systemHeartbeatControllerBean;
	
	@ManagedProperty(name = "reportRequestHelper", value = "#{reportRequestHelper}")
	private ReportRequestHelper reportRequestHelper;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@EJB
	private ExternalSubjectServiceLocal subjectService;

	@EJB
	private PermissionServiceLocal permissionService;

	@EJB
	private PalmsRealmDao palmsRealmDao;

	@EJB
	private RoleContextDao roleContextDao;

	@EJB
	private RoleDao roleDao;

	@EJB
	private GlobalRolesBean rolesBean;

	@EJB
	private ExternalSubjectCache subjectCache;
	
	@EJB
	private APIAuthorizationServiceLocal apiAuthorizationService;

	@Resource(name = "application/config_properties")
	private Properties configProperties;

	private boolean firstTime = true;

	public void initView() {
		log.debug("In Init View, SysAdminControllerBean");
		Subject subject = SecurityUtils.getSubject();
		if (firstTime) {
			List<String> realmNames = Arrays.asList(this.properties.getProperty("palmsRealmNames").split(","));
			// List<ExternalSubjectDataObj> subjects =
			// subjectService.getExternalSubjects(realmNames, "", false);
			List<ExternalSubjectDataObj> subjects = subjectCache.getAllSubjects(realmNames);

			/*
			List<PalmsRealm> realms = palmsRealmDao.findRealmsInListOfNames(realmNames);
			//PalmsRealm realm = palmsRealmDao.findRealmByName(this.properties.getProperty("palmsRealmName"));
			//sysAdminViewBean.setRealm(realms.get(0));
			List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();
			for (PalmsRealm realm : realms){
				subjects.addAll(subjectService.getExternalSubjects(realm, "", false));
			}
			*/
			sysAdminViewBean.setSubjects(subjects);
			PalmsRealm realm = palmsRealmDao.findRealmByName(this.properties.getProperty("primaryRealmName"));
			List<Role> roles = realm.getRoleTemplate().getAllowedRoles();
			sysAdminViewBean.setRoles(roles);
			List<RoleColumnModel> roleColumns = new ArrayList<RoleColumnModel>();
			for (Role role : roles) {
				roleColumns.add(new RoleColumnModel(role.getName(), role.getLabel()));
			}
			sysAdminViewBean.setRoleColumns(roleColumns);
			if (subject.isPermitted("editRoles")) {
				sysAdminViewBean.setReinitRolesButtonDisabled(false);
			}
			
			//systemHeartbeatControllerBean.checkHeartbeat(true, true);
			
			getSessionBean().clearProject();

			setFirstTime(false);
		}
	}
	
	public void handleRowSelect(SelectEvent event) {
		log.debug("Handle Row Select here. {} rows selected", sysAdminViewBean.getSelectedSubjects().size());

		disableToolBarButtons(false);
		if (sysAdminViewBean.getSelectedSubjects().size() == 1) {
			sysAdminViewBean.setLinkedAccountButtonDisabled(false);
			List<ExternalSubjectDataObj> selectedUsers = sysAdminViewBean.getSelectedSubjects();
			linkedAccountViewBean.setSelectedUser(selectedUsers.get(0));
			// log.debug("Selected User {}, Realm {}({})",
			// selectedUsers.get(0).getUsername(),
			// selectedUsers.get(0).getRealmName(),
			// selectedUsers.get(0).getRealm_id());
		} else {
			sysAdminViewBean.setLinkedAccountButtonDisabled(true);
		}
		/*
		List<ExternalSubjectDataObj> selected = sysAdminViewBean.getSelectedSubjects();
		ExternalSubjectDataObj eventObj = (ExternalSubjectDataObj) event.getObject();
		RoleContext context = roleContextDao.findDefaultRoleContextBySubjectAndRealm(eventObj.getUsername(), sysAdminViewBean.getRealm().getRealm_id());
		eventObj.setRoles(context.getRoles());
		sysAdminViewBean.getSubjectsWithRoles().add(eventObj);
		for (ExternalSubjectDataObj o : selected){
			log.debug("Selected Subject: {}", o.getUsername());
			
			
		}
		*/
	}

	public void handleRowUnselect() {
		log.debug("Handle Row UN select here");
		disableToolBarButtons(true);
		if (sysAdminViewBean.getSelectedSubjects().size() > 0 && sysAdminViewBean.getSelectedSubjects().size() < 2) {
			sysAdminViewBean.setLinkedAccountButtonDisabled(false);
		}
	}

	private void disableToolBarButtons(boolean disable) {
		sysAdminViewBean.setAuditLogButtonDisabled(disable);
		sysAdminViewBean.setAssignRolesButtonDisabled(disable);
		sysAdminViewBean.setCompareRolesButtonDisabled(disable);
	}

	public void handleCompareRoles(ActionEvent event) {
		compateRoles();
		sysAdminViewBean.setRenderedCompareRoles(true);
	}

	public void handleSaveRoles(ActionEvent event) {
		editRolesControllerBean.handleSaveRoles();
		compateRoles();
	}

	private void compateRoles() {
		List<ExternalSubjectDataObj> selectedList = sysAdminViewBean.getSelectedSubjects();
		List<ExternalSubjectDataObj> compareList = sysAdminViewBean.getSubjectsWithRoles();
		int compareItemIndex = 0;

		for (ExternalSubjectDataObj o : selectedList) {
			RoleContext context = roleContextDao.findDefaultRoleContextBySubjectAndRealm(o.getUsername(),
					o.getRealm_id());
			if (context == null) {
				PalmsSubject subject = subjectService.createSubjectDefaultContext(o.getUsername(), o.getRealm_id(),
						o.getEntryDN(), o.getExternal_id());
				context = subjectService.getDefaultRoleContext(subject.getPrincipal(),
						subject.getRealm().getRealm_id(), subject.getEntryDN(), subject.getExt_id());
			}
			o.setRoles(context.getRoles());
			if (!compareList.contains(o)) {
				compareList.add(o);
			} else {
				compareItemIndex = compareList.lastIndexOf(o);
				compareList.remove(o);
				compareList.add(compareItemIndex, o);
			}

		}
		sysAdminViewBean.setSubjectsWithRoles(compareList);
	}

	public void handleInitAuditLog() {
		List<String> loggerNames = new ArrayList<String>();
		loggerNames.add("AUDIT-ADMIN");
		loggerNames.add("AUDIT-AUTHORIZATION");
		loggerNames.add("AUDIT-LRM");
		loggerNames.add("AUDIT-SCORECARD");
		List<ExternalSubjectDataObj> selectedSubjects = sysAdminViewBean.getSelectedSubjects();
		log.debug("about to search audit log");
		if (selectedSubjects.size() == 1) {
			log.debug("Searching with single user selected");
			auditLogControllerBean.initAuditLog(auditLogControllerBean.AUDIT_TYPE_INTERNAL, selectedSubjects.get(0)
					.getUsername(), loggerNames);
			log.debug("Finished Search with single user");
		} else if (selectedSubjects.size() > 1) {
			log.debug("Searching with {} users ", selectedSubjects.size());
			List<String> usernames = new ArrayList<String>();
			for (ExternalSubjectDataObj user : selectedSubjects) {
				usernames.add(user.getUsername());
			}
			auditLogControllerBean.initMultiLog(usernames, loggerNames);
			log.debug("Finished Audit log search");
		}
	}

	public void handleInitRolesDlg() {
		log.debug("Handle Edit Roles here");
		// int realm_id =
		// palmsRealmDao.findRealmByName("PalmsRealm").getRealm_id();
		editRolesControllerBean.initRoles(sysAdminViewBean.getSelectedSubjects());
		// PalmsRealm realm =
		// realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));

	}

	public void handleGetUsersInRole(ValueChangeEvent e) {
		int roleId = (Integer) e.getNewValue();
		Role role = roleDao.findById(roleId);
		List<String> realmNames = Arrays.asList(properties.getProperty("palmsRealmNames").split(","));
		if (role == null) {
			List<ExternalSubjectDataObj> subjects = subjectService.getExternalSubjects(realmNames, "", false);
			sysAdminViewBean.setLinkedIdColumnRendered(false);
			sysAdminViewBean.setSubjects(subjects);
		} else {
			sysAdminViewBean.setSelectedRole(role);

			log.debug("Selected Role is now: {}", role.getName());
			if (role.isRequires_linked_id()) {
				sysAdminViewBean.setSubjects(subjectService.getSubjectsAndLinkedIdsWithRole(realmNames, role));
				sysAdminViewBean.setLinkedIdColumnRendered(true);
			} else {
				sysAdminViewBean.setLinkedIdColumnRendered(false);
				sysAdminViewBean.setSubjects(subjectService.getSubjectsWithRole(realmNames, role));
			}
		}
	}

	public void handleResetCompareRoles() {
		sysAdminViewBean.setSubjectsWithRoles(new ArrayList<ExternalSubjectDataObj>());
		sysAdminViewBean.setRenderedCompareRoles(false);
	}

	public void handleReinitRoles() {
		rolesBean.init();

		FacesContext.getCurrentInstance().addMessage(
				"Successfully Re-Initialized System Roles",
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"The System roles have been synchronized with the datastore", ""));
	}

	public void handleInitLinkedAccounts() {
		List<ExternalSubjectDataObj> selectedList = sysAdminViewBean.getSelectedSubjects();
		int count = 0;
		for (ExternalSubjectDataObj o : selectedList) {
			if (count == 1) {
				FacesContext.getCurrentInstance().addMessage(
						"Multipe Users Selected.  This action can only handle 1 user at a time.",
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please Select only one user and try again", ""));
				return;
			}
			RoleContext context = roleContextDao.findDefaultRoleContextBySubjectAndRealm(o.getUsername(),
					o.getRealm_id());
			if (context == null) {
				PalmsSubject subject = subjectService.createSubjectDefaultContext(o.getUsername(), o.getRealm_id(),
						o.getEntryDN(), o.getExternal_id());
				context = subjectService.getDefaultRoleContext(subject.getPrincipal(),
						subject.getRealm().getRealm_id(), subject.getEntryDN(), subject.getExt_id());
			}
			count++;
		}
		linkedAccountControllerBean.initView();
	}

	public void handlePalmsUsers() {
		sysAdminViewBean.setPalmsUsersRendered(true);
		sysAdminViewBean.setSystemStatusRendered(false);
		sysAdminViewBean.setActiveIndex(0);
	}

	public void handleSystemStatus() {
		sysAdminViewBean.setSystemStatusRendered(true);
		sysAdminViewBean.setPalmsUsersRendered(false);
		sysAdminViewBean.setActiveIndex(1);
		systemHeartbeatControllerBean.checkHeartbeat(true, true);
	}

	public SysAdminViewBean getSysAdminViewBean() {
		return sysAdminViewBean;
	}

	public void setSysAdminViewBean(SysAdminViewBean sysAdminViewBean) {
		this.sysAdminViewBean = sysAdminViewBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public AuditLogControllerBean getAuditLogControllerBean() {
		return auditLogControllerBean;
	}

	public void setAuditLogControllerBean(AuditLogControllerBean auditLogControllerBean) {
		this.auditLogControllerBean = auditLogControllerBean;
	}

	public EditRolesControllerBean getEditRolesControllerBean() {
		return editRolesControllerBean;
	}

	public void setEditRolesControllerBean(EditRolesControllerBean editRolesControllerBean) {
		this.editRolesControllerBean = editRolesControllerBean;
	}

	public RoleConverter getRoleConverter() {
		return roleConverter;
	}

	public void setRoleConverter(RoleConverter roleConverter) {
		this.roleConverter = roleConverter;
	}

	public LinkedAccountViewBean getLinkedAccountViewBean() {
		return linkedAccountViewBean;
	}

	public void setLinkedAccountViewBean(LinkedAccountViewBean linkedAccountViewBean) {
		this.linkedAccountViewBean = linkedAccountViewBean;
	}

	public LinkedAccountControllerBean getLinkedAccountControllerBean() {
		return linkedAccountControllerBean;
	}

	public void setLinkedAccountControllerBean(LinkedAccountControllerBean linkedAccountControllerBean) {
		this.linkedAccountControllerBean = linkedAccountControllerBean;
	}

	public SystemHeartbeatControllerBean getSystemHeartbeatControllerBean() {
		return systemHeartbeatControllerBean;
	}

	public void setSystemHeartbeatControllerBean(SystemHeartbeatControllerBean systemHeartbeatControllerBean) {
		this.systemHeartbeatControllerBean = systemHeartbeatControllerBean;
	}

	public ReportRequestHelper getReportRequestHelper() {
		return reportRequestHelper;
	}

	public void setReportRequestHelper(ReportRequestHelper reportRequestHelper) {
		this.reportRequestHelper = reportRequestHelper;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

}
