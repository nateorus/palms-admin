package com.pdinh.presentation.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.kf.fsm.service.FileService;
import com.kf.fsm.view.S3File;
import com.kf.queuemanager.service.MessageService;
import com.kf.queuemanager.view.SQSSendRequest;
import com.kf.uffda.data.dao.ErrorSeverityDao;
import com.kf.uffda.data.dao.JobStatusDao;
import com.kf.uffda.data.dao.UploadImportJobErrorDao;
import com.kf.uffda.data.dao.UploadImportRecordDao;
import com.kf.uffda.dto.ImportJobDto;
import com.kf.uffda.dto.ImportJobLogDto;
import com.kf.uffda.dto.ImportJobResultDto;
import com.kf.uffda.persistence.ImportFileType;
import com.kf.uffda.persistence.ImportType;
import com.kf.uffda.persistence.JobStatus;
import com.kf.uffda.persistence.UploadImportJob;
import com.kf.uffda.persistence.UploadImportJobError;
import com.kf.uffda.persistence.UploadImportRecord;
import com.kf.uffda.service.BulkUploadService;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.jaxb.lrm.LrmObjectExistResponse;
import com.pdinh.data.jaxb.lrm.LrmObjectServiceLocal;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.helper.UrlGenerator;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ImportManagementViewBean;

@ManagedBean
@ViewScoped
public class ImportManagementControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(ImportManagementControllerBean.class);

	// Valid values for the HANDLER_APP key.
	public static final String PALMS = "PALMS";
	public static final String PDA = "PDA";

	boolean firstTime = true;
	Project currentProject;
	Subject subject;
	int counter = 1;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "importManagementViewBean", value = "#{importManagementViewBean}")
	private ImportManagementViewBean viewBean;

	@ManagedProperty(name = "systemHeartbeatControllerBean", value = "#{systemHeartbeatControllerBean}")
	private SystemHeartbeatControllerBean systemHeartbeatControllerBean;

	@EJB
	ProjectDao projectDao;

	@EJB
	JobStatusDao jobStatusDao;

	@EJB
	ErrorSeverityDao errorSeverityDao;

	@EJB
	BulkUploadService bulkUploadService;

	@EJB
	FileService fileService;

	@EJB
	UploadImportJobErrorDao uploadImportJobErrorDao;

	@EJB
	UploadImportRecordDao uploadImportRecordDao;

	@EJB
	private LrmObjectServiceLocal lrmObjectjectService;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	@EJB
	AuthorizationServiceLocal authorizationService;

	@EJB
	MessageService messageService;

	private S3File fileDownload;

	public void initView() {

		logger.trace("In ImportManagementControllerBean initView");

		if (isFirstTime()) {

			logger.trace("Reloading data for Import Management page ...");

			setSubject(SecurityUtils.getSubject());

			Project project = null;

			if (viewBean.getProjectId() != null) {
				logger.trace("Got project id {} as parameter.", viewBean.getProjectId());
				project = projectDao.findById(viewBean.getProjectId());
			} else {
				if (getSessionBean().getProject() != null) {
					logger.trace("Got project id {} from session.", getSessionBean().getProject().getProjectId());
					project = projectDao.findById(getSessionBean().getProject().getProjectId());
				}
				logger.trace("Project is not in Session.");
			}

			if (project != null) {
				getSessionBean().setProject(project);
				getSessionBean().setCompany(project.getCompany());
				getSessionBean().refreshProjects(getSessionBean().isIncludeClosedProjects());
				setCurrentProject(getSessionBean().getProject());
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Project context is missing.", ""));
				return;
			}

			viewBean.initialize();

			viewBean.setProjectId(getCurrentProject().getProjectId());
			viewBean.setProjectName(getCurrentProject().getName());
			viewBean.setProjectType(getCurrentProject().getProjectType().getName());
			viewBean.setProjectCode(getCurrentProject().getProjectCode());
			viewBean.setTargetLevel(projectDao.resolveTargetLevel(getCurrentProject()));
			viewBean.setCreatedBy(getCurrentProject().getExtAdminId());
			viewBean.setCreatedDate(getCurrentProject().getDateCreated());
			viewBean.setDueDate(getCurrentProject().getDueDate());
			viewBean.setProjectTypeCode(getCurrentProject().getProjectType().getProjectTypeCode());

			// Set the label flag
			viewBean.setHasProjectCodeLabel(false);
			if (ProjectType.ASSESSMENT_BY_DESIGN_ONLINE.equals(viewBean.getProjectTypeCode())
					|| ProjectType.GLGPS.equals(viewBean.getProjectTypeCode())
					|| ProjectType.READINESS_ASSMT_BUL.equals(viewBean.getProjectTypeCode())
					|| ProjectType.ERICSSON.equals(viewBean.getProjectTypeCode())) {
				viewBean.setHasProjectCodeLabel(true);
			}

			viewBean.setJobStatuses(jobStatusDao.findAllActive());
			viewBean.setStatusOptions();

			viewBean.setErrorSeverity(errorSeverityDao.findAllActive());
			viewBean.setErrorSeverityOptions();
			viewBean.setLrmProjectId(isLrmProjectExist());
			viewBean.setLrmProjectExist(viewBean.getLrmProjectId() > 0);
			sessionBean.setLrmProjectId(viewBean.getLrmProjectId());

			systemHeartbeatControllerBean.checkHeartbeat(true, false);

			loadImportJobs();

			setFirstTime(false);

		}

		if (viewBean.getSelectedJob() != null) {
			viewBean.setSelectedImportName(viewBean.getSelectedJob().getName());
		}
	}

	private void loadImportJobs() {

		ImportJobDto importJobObj = null;
		viewBean.setImportJobs(new ArrayList<ImportJobDto>());

		List<UploadImportJob> jobs = bulkUploadService.getAllJobsByProjectId(getCurrentProject().getProjectId());

		for (UploadImportJob job : jobs) {

			importJobObj = makeImportJobDto(job);

			if (viewBean.getSelectedJob() != null) {
				if (viewBean.getSelectedJob().getId() == importJobObj.getId()) {
					viewBean.setSelectedJob(importJobObj);
					sessionBean.setImportJobId(importJobObj.getId());
				}
			}

			viewBean.getImportJobs().add(importJobObj);
		}
	}

	private ImportJobDto makeImportJobDto(UploadImportJob job) {

		Integer lastActionBy = null;
		Date lastActionDate = null;
		ImportJobLogDto mostRecentLogObj = null;

		ImportJobDto importJobObj = bulkUploadService.transformImportJob(job);
		mostRecentLogObj = importJobObj.getMostRecentLog();

		// TODO: AV This not good needs to be revisited
		if (mostRecentLogObj != null) {
			if (isJobCanceled(importJobObj)) {
				lastActionDate = importJobObj.getCanceledDate();
				lastActionBy = importJobObj.getCanceledById();
			} else {
				lastActionDate = mostRecentLogObj.getLastActionDate();
				lastActionBy = mostRecentLogObj.getLastActionById();
			}
			importJobObj.setLastActionBy(getFullName(lastActionBy));
			importJobObj.setLastActionDate(lastActionDate);

			// fetching log records for the selected job
			for (ImportJobLogDto attempt : importJobObj.getLog()) {

				if (attempt.getId() == mostRecentLogObj.getId()) {
					attempt.setMostRecent(true);
				} else {
					attempt.setMostRecent(false);
				}

				// TODO: AV We need to have a better way to figure out
				// import attempt phase this temporary and needs to go away
				if (attempt.getPhase(viewBean.isLrmProjectExist()).equals(ImportJobLogDto.VALIDATION_PHASE)) {
					attempt.setSevereErrorCount(attempt.getValidationSevereErrorCount());
					attempt.setWarningErrorCount(attempt.getValidationWarningErrorCount());
					attempt.setInfoErrorCount(attempt.getValidationInfoErrorCount());

				} else if (attempt.getPhase(viewBean.isLrmProjectExist()).equals(ImportJobLogDto.COMMIT_PHASE)
						|| attempt.getPhase(viewBean.isLrmProjectExist()).equals(ImportJobLogDto.CREATE_REQUEST_PHASE)) {
					attempt.setSevereErrorCount(attempt.getImportSevereErrorCount());
					attempt.setWarningErrorCount(attempt.getImportWarningErrorCount());
					attempt.setInfoErrorCount(attempt.getImportInfoErrorCount());
				}

				attempt.setLastActionByName(getFullName(attempt.getLastActionById()));

			}

		}
		return importJobObj;
	}

	public void reloadImportJobs() {

		loadImportJobs();

		if (viewBean.getSelectedJob() != null) {
			updateJobLogToolbar(viewBean.getSelectedJob());
		}

	}

	public void refreshSelectedJob() {

		// Update the Selected Job
		if (viewBean.getSelectedJob() != null) {
			UploadImportJob job = bulkUploadService.getImportJobById(viewBean.getSelectedJob().getId());
			if (job != null) {
				ImportJobDto updatedJob = makeImportJobDto(job);
				viewBean.setSelectedJob(updatedJob);
				sessionBean.setImportJobId(updatedJob.getId());

				int index = 0;
				for (ImportJobDto oldJob : viewBean.getImportJobs()) {
					if (oldJob.getId() == updatedJob.getId()) {
						viewBean.getImportJobs().set(index, updatedJob);
						break;
					}
					index++;
				}
				updateJobLogToolbar(updatedJob);
			}
		}

		// Update the Selected Job Log
		if (viewBean.getSelectedJobLog() != null) {
			ImportJobLogDto updatedJobLog = bulkUploadService.getImportJobLogById(viewBean.getSelectedJobLog().getId());
			if (updatedJobLog != null) {
				viewBean.setSelectedJobLog(updatedJobLog);

				int index = 0;
				for (ImportJobLogDto oldJobLog : viewBean.getImportJobLog()) {
					if (oldJobLog.getId() == updatedJobLog.getId()) {
						viewBean.getImportJobLog().set(index, updatedJobLog);
						break;
					}
					index++;
				}
			}
		}
	}

	private String getFullName(Integer subjectId) {
		String fullName = "";
		if (subjectId != null) {
			ExternalSubjectDataObj subject = authorizationService.getSubjectMetadata(subjectId);
			if (subject != null) {
				fullName = subject.getFirstLastEmail();
			}
		}
		return fullName;
	}

	private boolean isJobCanceled(ImportJobDto job) {
		if (job.getStatusCode().equalsIgnoreCase(JobStatus.CANCELLED)) {
			return true;
		}
		return false;
	}

	public void handleChangeProject(AjaxBehaviorEvent event) {
		viewBean.setProjectId(getSessionBean().getSelectedProject());
		logger.trace("In importManagementControllerBean.handleChangeProject, selected project: {}",
				viewBean.getProjectId());
		refreshView();
	}

	public void cancelImport() {
		// Logic to handle Import Cancellation
		logger.trace("Cancel Import");
		bulkUploadService.cancelImportJob(viewBean.getSelectedJob().getId(), sessionBean.getSubject_id(), new Date());
		EntityAuditLogger.auditLog.info("{} performed {} operation on import job ({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_IMPORT_CANCEL_IMPORT_JOB,
						viewBean.getSelectedJob().getId() });
		reloadImportJobs();
	}

	public void commitImport() {
		int jobId = viewBean.getSelectedJob().getId();
		boolean success = addMessageToQueue(jobId, JobStatus.IMPORT_PENDING, PALMS);

		if (!success) {
			LabelUtils.addErrorMessage("importJobsMessages", "Commit Latest Import File:",
					"Failed to communicate with queuing service.  Please try again later.");
			logger.error("Oopsie! Failed to communicate with queuing service.");
			bulkUploadService.recordJobError(jobId, "Commit Latest Import File:"
					+ "Something bad happened trying to communicate to queuing service.");
		} else {

			logger.trace("We added a message to the SQS queue!!!!  Job should be picked up for import at any time now!!!!");

			success = bulkUploadService.moveJobToImportQueue(jobId, true, sessionBean.getSubject_id());

			if (!success) {
				LabelUtils.addErrorMessage("importJobsMessages", "Commit Latest Import File:",
						": Failed to activate commit for import job (" + jobId + "). Please try again later.");
				logger.error("Oopsie! Failed to activate commit for import job ({}). Please try again later.", jobId);
				bulkUploadService.recordJobError(jobId,
						"Commit Latest Import File: Failed to activate commit for import job (" + jobId + ").");
			} else {

				LabelUtils.addMessage("importJobsMessages", "Commit Latest Import File:",
						"Successfully added request to queue.  Commit should start soon.");

			}

		}

		EntityAuditLogger.auditLog.info("{} performed {} operation on import job ({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_IMPORT_COMMIT_IMPORT_JOB, jobId });

		reloadImportJobs();

		viewBean.setImportJobLogRendered(false);
		viewBean.setImportJobsListRendered(true);

	}

	public void handleCreateRequest() {
		StringBuilder urlBuilder = new StringBuilder(
				"postTo.xhtml?faces-redirect=true&path=/clientadmin/faces/transfer.xhtml");
		urlBuilder.append("&sys=");
		urlBuilder.append("clientadmin");
		urlBuilder.append("&mode=");
		urlBuilder.append("internal");
		urlBuilder.append("&page=");
		urlBuilder.append("newNamedRequest");
		urlBuilder.append("&returnURL=");
		urlBuilder.append("EXIT");

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute(UrlGenerator.getPopupJS("intakeWindow", urlBuilder.toString(), false, false));

		int jobId = viewBean.getSelectedJob().getId();
		bulkUploadService.updateSubmitter(jobId, false, sessionBean.getSubject_id());

		reloadImportJobs();

		viewBean.setImportJobLogRendered(false);
		viewBean.setImportJobsListRendered(true);
	}

	private int isLrmProjectExist() {
		int lrmProjectId = 0;
		int projectId = sessionBean.getProject().getProjectId();
		LrmObjectExistResponse prjExtResp = null;
		try {
			prjExtResp = lrmObjectjectService.projectExist(String.valueOf(projectId));
		} catch (Exception e) {
			return 0;
		}

		if (prjExtResp.getRequestResult().equalsIgnoreCase("SUCCESS") && prjExtResp.isExist()) {
			lrmProjectId = prjExtResp.getLrmProjectId() != null ? prjExtResp.getLrmProjectId().intValue() : 0;
		}
		return lrmProjectId;
	}

	public void initNewImportJob() {
		viewBean.setRestartImportJob(false);
		viewBean.initImportJob();
	}

	public void initRestartImportJob() {

		viewBean.setRestartImportJob(true);
		viewBean.initImportJob();

		ImportJobDto job = viewBean.getSelectedJob();

		if (job != null) {
			viewBean.setImportJobId(job.getId());
			viewBean.setImportName(job.getName());
			viewBean.setClientContactName(job.getLog().get(0).getClientContactName());
			viewBean.setClientContactPhone(job.getLog().get(0).getClientContactPhone());
			viewBean.setClientContactEmail(job.getLog().get(0).getClientContactEmail());
		}

	}

	public void createImportJob() {

		if (!validateImportJob(viewBean.getImportJobId())) {
			return;
		}

		int companyId = sessionBean.getCompany().getCompanyId();
		int projectId = sessionBean.getProject().getProjectId();
		int projectTypeId = sessionBean.getProject().getProjectType().getProjectTypeId();

		String importType = viewBean.isLrmProjectExist() ? ImportType.PDA_PROJECT : ImportType.PALMS_USER_PROJECT;
		String jobName = viewBean.getImportName();
		Integer jobId = null;
		String operationName;

		if (viewBean.getImportJobId() == -1) {
			if (viewBean.isLrmProjectExist()) {
				jobId = bulkUploadService.initiatePdaUpload(jobName, companyId, projectId, viewBean.getLrmProjectId(),
						projectTypeId, importType, ImportFileType.PDA_TEMPLATE);
			} else {
				jobId = bulkUploadService.initiatePalmsUpload(jobName, companyId, projectId, projectTypeId, importType,
						ImportFileType.PALMS_TEMPLATE);
			}

			operationName = EntityAuditLogger.OP_IMPORT_CREATE_IMPORT_JOB;

		} else {
			// Need to reset job status to UPLOAD_IN_PROGRESS.
			jobId = viewBean.getImportJobId();
			bulkUploadService.setStatus(jobId, JobStatus.UPLOAD_IN_PROGRESS);
			operationName = EntityAuditLogger.OP_IMPORT_UPLOAD_IMPORT_FILE;
		}

		if (jobId == null) {
			// TODO: Something bad happened. Need to figure out how to handle
			// it.
			logger.error("Oopsie! Job Id is null after initiate palms upload.");
			return;
		}

		Date beginUpload = new Date();
		UploadedFile file = viewBean.getUploadedFile();
		String fileName = viewBean.getUploadedFilename();
		String fileKeyName = addFileToS3(jobId, companyId, importType, file, fileName);

		if (fileKeyName == null) {
			bulkUploadService.recordJobError(jobId, "Unable to add upload file " + fileName + " to S3.");
			LabelUtils.addErrorMessage("importJobsMessages", "File Upload: ", "Unable to move '" + fileName
					+ "' import file to the storage location. Please try again later.");

		} else {

			Date endUpload = new Date();

			boolean success = addMessageToQueue(jobId, JobStatus.VALIDATION_PENDING, viewBean.isLrmProjectExist() ? PDA
					: PALMS);

			if (!success) {
				logger.error("Oopsie! Unable to communicate with the queuing service.");
				LabelUtils.addErrorMessage("importJobsMessages", "Queuing Service: ",
						"Unable to communicate with the queuing service. Please try again later.");
				bulkUploadService.recordJobError(jobId, "Queuing Service: Unable to communicate to queuing service.");

			} else {

				logger.trace("Successfully uploaded import file. Validation should start soon.");

				success = bulkUploadService.moveJobToValidationQueue(jobId, false, true, fileName, fileKeyName,
						sessionBean.getSubject_id(), viewBean.getClientContactName(), viewBean.getClientContactEmail(),
						viewBean.getClientContactPhone(), beginUpload, endUpload);

				if (!success) {

					if (viewBean.isRestartImportJob()) {
						LabelUtils
								.addErrorMessage("importJobsMessages", "Upload New Import File: ",
										"Failed to activate validation for import job (" + jobId
												+ "). Please try again later.");
					} else {
						LabelUtils
								.addErrorMessage("importJobsMessages", "Create Import File: ",
										"Failed to activate validation for import job (" + jobId
												+ "). Please try again later.");
					}

					bulkUploadService.recordJobError(jobId, "Failed to activate validation for import job (" + jobId
							+ ").");
					logger.error("Oopsie! Failed to move import job ({}) to validation queue.", jobId);

				} else {

					if (viewBean.isRestartImportJob()) {
						LabelUtils.addMessage("importJobsMessages", "Upload New Import File: ",
								"Successfully uploaded import file. Validation should start soon.");
					} else {
						LabelUtils.addMessage("importJobsMessages", "Create Import File: ",
								"Successfully uploaded import file. Validation should start soon.");
					}

					logger.trace("Successfully uploaded import file for job ({}). Validation should start soon.", jobId);
				}
			}
		}

		EntityAuditLogger.auditLog.info("{} performed {} operation on import job ({})",
				new Object[] { subject.getPrincipal(), operationName, jobId });

		reloadImportJobs();

		// Redirecting back to Import Jobs list so, user can see the new status.
		viewBean.setImportJobLogRendered(false);
		viewBean.setImportJobsListRendered(true);

		logger.trace("Create new import request {}", viewBean.getImportName());
	}

	public void cancelCreateImportJob() {
		viewBean.setUploadedFile(null);
	}

	private String addFileToS3(int jobId, int companyId, String importType, UploadedFile file, String fileName) {

		boolean fileUploadResult = false;
		String bucketName = configProperties.getProperty("bulkUploadS3Bucket");
		String fileKey = "files/" + jobId + "_" + companyId + "_" + importType + "_" + new Date().getTime();

		S3File s3File = new S3File();
		try {
			s3File.setFileName(fileName);
			s3File.setFileKey(fileKey);
			s3File.setFileSize(String.valueOf(file.getSize()));
			s3File.setBucketName(bucketName);
			s3File.setInputStream(file.getInputstream());
			s3File.setEncrypted(true);

			fileUploadResult = fileService.uploadFile(s3File, configProperties);

		} catch (IOException e) {
			e.printStackTrace();
			return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		} finally {
			try {
				s3File.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		logger.info("File Upload Result = {}", ((fileUploadResult) ? "Successful" : "Failed"));

		return (fileUploadResult) ? fileKey : null;

	}

	private boolean addMessageToQueue(int jobId, String jobStatus, String routeToApp) {

		boolean result = false;
		String queueName = configProperties.getProperty("bulkUploadQueue");
		Map<String, MessageAttributeValue> messageAttributes = new HashMap<String, MessageAttributeValue>();

		messageAttributes.put("HANDLER_APP",
				new MessageAttributeValue().withDataType("String").withStringValue(routeToApp));
		messageAttributes.put("JOB_STATUS",
				new MessageAttributeValue().withDataType("String").withStringValue(jobStatus));

		SQSSendRequest request = new SQSSendRequest(String.valueOf(jobId), messageAttributes);

		try {
			result = messageService.sendMessage(queueName, request, configProperties);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void prepareToDownload(ImportJobLogDto log) throws AbortProcessingException {

		String bucketName = configProperties.getProperty("bulkUploadS3Bucket");
		S3File file = null;

		try {

			logger.info("Attempting to download the file {} with key {} from the {} bucket.",
					log.getOriginalFilename(), log.getFileKeyName(), bucketName);

			file = fileService.downloadFile(log.getOriginalFilename(), log.getFileKeyName(), bucketName,
					configProperties);

		} catch (Exception e) {
			logger.error("Unable to download the file {} with key {} from the {} bucket because it cannot be located.",
					log.getOriginalFilename(), log.getFileKeyName(), bucketName);

			LabelUtils.addErrorMessage("importJobLogMessages", "Download Import File: ", "Unable to download <b>'"
					+ log.getOriginalFilename() + "'</b> file with <b>'" + log.getFileKeyName()
					+ "'</b> file key from the <b>'" + bucketName + "'</b> bucket because it cannot be located.");

			throw new AbortProcessingException(
					"Aborting import file download, somthing went wrong when attempting to download this '"
							+ log.getFileKeyName() + "' file from '" + bucketName + "' bucket.");

		}

		if (file != null) {
			if (file.getStorageType() != null && file.getStorageType().equals(S3File.STORAGE_TYPE_GLACIER)) {
				LabelUtils
						.addWarningMessage(
								"importJobLogMessages",
								"Download Import File: ",
								"This file is archived and needs to be restored before it can be accessed. Please email  Client Technology Services "
										+ "at <i>cts-support@kornferry.com</i> with the following information: <b>Bucket Name ("
										+ bucketName + ");  File Key (" + file.getFileKey()
										+ ")</b>.  You will be notified when the file is restored.");

				file = null;

				throw new AbortProcessingException("Aborting import file download because file is stored in Glacier "
						+ "and needs to restored before it can be downloaded.");
			}
		}

		EntityAuditLogger.auditLog.info("{} performed {} operation on import job log ({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_IMPORT_DOWNLOAD_IMPORT_FILE, log.getId() });

		setFileDownload(file);
	}

	public StreamedContent downloadFile() throws Exception {

		StreamedContent download = null;
		S3File file = getFileDownload();

		if (file != null) {

			String mimeType = (file.findFileExtention().equals("xlsx") ? "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
					: "application/vnd.ms-excel");

			InputStream input = file.getInputStream();
			download = new DefaultStreamedContent(input, mimeType, file.getFileName());
			logger.trace("download = {}", download.getName());
			logger.trace("file storage type = {}", file.getStorageType());

		}

		return download;
	}

	public void handleJobRowSelect(SelectEvent event) {
		viewBean.setImportJobId(-1);
		logger.trace("Selecting Import Job Row");
	}

	public void handleJobRowUnselect(UnselectEvent event) {
		viewBean.setImportJobId(-1);
		viewBean.setCommitDisabled(true);
		viewBean.setCreateRequestDisabled(true);
		viewBean.setRestartImportDisabled(true);
		logger.trace("Unselecting Import Job Row");
	}

	public void handleJobLogRowSelect(SelectEvent event) {
		viewBean.setViewMenuDisabled(false);
		logger.trace("Selecting Import Job Log Row");
	}

	public void handleJobLogRowUnselect(UnselectEvent event) {
		viewBean.setViewMenuDisabled(true);
		logger.trace("Unselecting Import Job Log Row");
	}

	public void handleFileUpload(FileUploadEvent event) {
		viewBean.setUploadedFile(event.getFile());
		LabelUtils.addMessage("createImportJob", "Upload Import File:", "File (" + viewBean.getUploadedFilename()
				+ ") was stored successfully.");
	}

	private void updateJobLogToolbar(ImportJobDto job) {

		sessionBean.setImportJobId(job.getId());
		if (job.isActive()
				&& (job.getStatusCode().equals(JobStatus.VALIDATION_PENDING)
						|| job.getStatusCode().equals(JobStatus.VALIDATION_IN_PROGRESS) || job.getStatusCode().equals(
						JobStatus.VALIDATION_COMPLETE))) {
			viewBean.setRestartImportDisabled(false);
		} else {
			viewBean.setRestartImportDisabled(true);
		}

		if (job.isActive() && job.getStatusCode().equals(JobStatus.VALIDATION_COMPLETE)) {
			viewBean.setCommitDisabled(false);
			viewBean.setCreateRequestDisabled(!viewBean.isLrmProjectExist());
		} else {
			viewBean.setCommitDisabled(true);
			viewBean.setCreateRequestDisabled(true);
		}

		if (job.isActive() && job.getStatusCode().equals(JobStatus.VALIDATION_COMPLETE)) {
			viewBean.setCancelOperationDisabled(false);
		} else {
			viewBean.setCancelOperationDisabled(true);
		}

		if (job.getStatusCode().equals(JobStatus.VALIDATION_COMPLETE) || job.getStatusCode().equals(JobStatus.ERROR)) {
			viewBean.setCloseJobDisabled(false);
		} else {
			viewBean.setCloseJobDisabled(true);
		}

		viewBean.setJobClosed(!job.isActive());
	}

	public void showImportJobLog() {
		ImportJobDto job = viewBean.getSelectedJob();

		updateJobLogToolbar(job);

		viewBean.setSelectedImportName(job.getName());
		viewBean.setImportJobLogRendered(true);
		viewBean.setImportJobsListRendered(false);
		viewBean.setSelectedJobLog(null);
		viewBean.setViewMenuDisabled(true);

		logger.trace("Selected Job: {}", viewBean.getSelectedJob());
		logger.trace("Total Log Records: {}", viewBean.getImportJobLog().size());

	}

	public void exitImportJobLog() {
		viewBean.setImportJobLogRendered(false);
		viewBean.setImportJobsListRendered(true);
		viewBean.setImportJobLog(new ArrayList<ImportJobLogDto>());
		viewBean.setSelectedJobLog(null);
		viewBean.setViewMenuDisabled(true);
	}

	public void loadLogErrors() {

		ImportJobLogDto log = viewBean.getSelectedJobLog();

		if (log != null) {
			List<UploadImportJobError> errors = uploadImportJobErrorDao.findErrorsByLogId(log.getId());

			log.clearErrors();

			for (UploadImportJobError error : errors) {
				log.getErrors().add(bulkUploadService.transformImportJobError(error));
			}
			Collections.sort(log.getErrors());

			viewBean.setSelectedJobLog(log);
		} else {
			logger.error("Exception: ImportJobLog is null in loadLogErrors method (This should never happen).");
		}
	}

	public void showExceptions() {
		viewBean.setImportJobLogRendered(false);
		viewBean.setImportJobLogResultsRendered(false);
		viewBean.setImportJobLogExceptionsRendered(true);
	}

	public void exitExceptions() {
		viewBean.setImportJobLogRendered(true);
		viewBean.setImportJobLogResultsRendered(false);
		viewBean.setImportJobLogExceptionsRendered(false);
	}

	public void loadLogResults() {
		ImportJobLogDto log = viewBean.getSelectedJobLog();
		List<UploadImportRecord> results = uploadImportRecordDao.findResultsByJobId(log.getJobId());

		log.clearResults();

		for (UploadImportRecord result : results) {
			ImportJobResultDto resultDto = bulkUploadService.transformImportJobResult(result);

			if (resultDto.getUpdatedById() != null) {
				resultDto.setLastActionByName(getFullName(resultDto.getUpdatedById()));

			} else if (resultDto.getCreatedById() != null) {
				resultDto.setLastActionByName(getFullName(resultDto.getCreatedById()));
			}

			log.getResults().add(resultDto);
		}
		Collections.sort(log.getResults());

		viewBean.setSelectedJobLog(log);

	}

	public void showResults() {
		viewBean.setImportJobLogRendered(false);
		viewBean.setImportJobLogResultsRendered(true);
		viewBean.setImportJobLogExceptionsRendered(false);
	}

	public void exitResults() {
		viewBean.setImportJobLogRendered(true);
		viewBean.setImportJobLogResultsRendered(false);
		viewBean.setImportJobLogExceptionsRendered(false);
	}

	private boolean validateImportJob(int importJobId) {
		boolean valid = true;

		if (viewBean.getImportName().isEmpty()) {
			LabelUtils.addErrorMessage("createImportJob", "Import Name:", "Please enter Job Name.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		} else {
			if (importJobId == -1) {
				List<UploadImportJob> jobs = bulkUploadService.findAllJobsByProjectIdAndName(getCurrentProject()
						.getProjectId(), viewBean.getImportName());

				if (jobs != null && jobs.size() > 0) {
					LabelUtils.addErrorMessage("createImportJob", "Import Name:",
							"This Job Name already exists for this project.  Please enter a different name.");
					RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
					valid = false;
				}
			}
		}

		if (viewBean.getUploadedFile() == null) {
			LabelUtils.addErrorMessage("createImportJob", "Upload Import File:", "Please upload import file.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		if (StringUtils.isNotBlank(viewBean.getClientContactEmail())) {
			if (!EmailValidator.getInstance().isValid(viewBean.getClientContactEmail())) {
				LabelUtils.addErrorMessage("createImportJob", "Contact Email:", "Please enter a valid email address.");
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
				valid = false;
			}
		}

		return valid;
	}

	public void openImportJob() {
		// Logic to handle open Import Job
		viewBean.setJobClosed(false);
		bulkUploadService.openImportJob(viewBean.getSelectedJob().getId());
		EntityAuditLogger.auditLog.info("{} performed {} operation on import job ({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_IMPORT_OPEN_IMPORT_JOB,
						viewBean.getSelectedJob().getId() });
		reloadImportJobs();
	}

	public void closeImportJob() {
		// Logic to handle close Import Job
		viewBean.setJobClosed(true);
		bulkUploadService.closeImportJob(viewBean.getSelectedJob().getId(), sessionBean.getSubject_id(), new Date());
		EntityAuditLogger.auditLog.info("{} performed {} operation on import job ({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_IMPORT_CLOSE_IMPORT_JOB,
						viewBean.getSelectedJob().getId() });
		reloadImportJobs();
	}

	public void refreshView() {
		logger.trace(">>>>>>>>>>  Manually refreshing view (Start) <<<<<<<<<<");
		setFirstTime(true);
		initView();
		logger.trace(">>>>>>>>>>  Manually refreshing view (End) <<<<<<<<<<");
	}

	public void doNothing() {

	}

	// Getters/Setters
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ImportManagementViewBean getImportManagementViewBean() {
		return viewBean;
	}

	public void setImportManagementViewBean(ImportManagementViewBean importManagementViewBean) {
		this.viewBean = importManagementViewBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public int getCounter() {
		return counter++;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public SystemHeartbeatControllerBean getSystemHeartbeatControllerBean() {
		return systemHeartbeatControllerBean;
	}

	public void setSystemHeartbeatControllerBean(SystemHeartbeatControllerBean systemHeartbeatControllerBean) {
		this.systemHeartbeatControllerBean = systemHeartbeatControllerBean;
	}

	public S3File getFileDownload() {
		return fileDownload;
	}

	public void setFileDownload(S3File fileDownload) {
		this.fileDownload = fileDownload;
	}
}