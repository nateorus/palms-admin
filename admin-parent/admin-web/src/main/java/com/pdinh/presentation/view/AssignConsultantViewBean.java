package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean
@ViewScoped
public class AssignConsultantViewBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ExternalSubjectDataObj> selections;
	private List<ExternalSubjectDataObj> consultants;
	private List<ExternalSubjectDataObj> selectedConsultants;
	private List<ExternalSubjectDataObj> filteredConsultants;
	private List<ExternalSubjectDataObj> removedConsultants;
	private List<ParticipantObj> participants;
	private String scroll;
	
	private boolean addToSelectionDisabled = true;

	public List<ExternalSubjectDataObj> getSelections() {
		return selections;
	}

	public void setSelections(List<ExternalSubjectDataObj> selections) {
		this.selections = selections;
	}

	public List<ExternalSubjectDataObj> getConsultants() {
		return consultants;
	}

	public void setConsultants(List<ExternalSubjectDataObj> consultants) {
		this.consultants = consultants;
	}

	public List<ExternalSubjectDataObj> getSelectedConsultants() {
		return selectedConsultants;
	}

	public void setSelectedConsultants(
			List<ExternalSubjectDataObj> selectedConsultants) {
		this.selectedConsultants = selectedConsultants;
	}

	public List<ExternalSubjectDataObj> getFilteredConsultants() {
		return filteredConsultants;
	}

	public void setFilteredConsultants(
			List<ExternalSubjectDataObj> filteredConsultants) {
		this.filteredConsultants = filteredConsultants;
	}

	public boolean isAddToSelectionDisabled() {
		return addToSelectionDisabled;
	}

	public void setAddToSelectionDisabled(boolean addToSelectionDisabled) {
		this.addToSelectionDisabled = addToSelectionDisabled;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ExternalSubjectDataObj> getRemovedConsultants() {
		return removedConsultants;
	}

	public void setRemovedConsultants(List<ExternalSubjectDataObj> removedConsultants) {
		this.removedConsultants = removedConsultants;
	}

	public String getScroll() {
		return scroll;
	}

	public void setScroll(String scroll) {
		this.scroll = scroll;
	}
	
}
