package com.pdinh.presentation.converter;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.pdinh.presentation.domain.CourseNormsTripletObj;
import com.pdinh.presentation.domain.NormGroupObj;
import com.pdinh.presentation.view.ProjectNormsViewBean;

@ManagedBean(name = "normGroupDtoConverter")
@ViewScoped
public class NormGroupDtoConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;

	@ManagedProperty(name = "projectNormsViewBean", value = "#{projectNormsViewBean}")
	private ProjectNormsViewBean projectNormsViewBean;

	@Override
	public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String value) {
		NormGroupObj foundNormGroupDto = null;
		for (CourseNormsTripletObj cn3o : projectNormsViewBean.getProjectNormsDefaultsObj().getTriplets()) {
			for (NormGroupObj normGroupObj : cn3o.getCourse().getNormGroups()) {
				// if (value.equals(normGroupObj.getId().toString() + "-" +
				// normGroupObj.getType() + "-"
				// + normGroupObj.getValue())) {
				if (value.equals(normGroupObj.hashCode() + "")) {
					foundNormGroupDto = normGroupObj;
					break;
				}
			}
		}
		return foundNormGroupDto;
	}

	@Override
	public String getAsString(FacesContext ctx, UIComponent uiComponent, Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			NormGroupObj normGroupDto = (NormGroupObj) value;
			// return normGroupDto.getId().toString() + "-" +
			// normGroupDto.getType() + "-" + normGroupDto.getValue();
			return normGroupDto.hashCode() + "";
		}
	}

	public ProjectNormsViewBean getProjectNormsViewBean() {
		return projectNormsViewBean;
	}

	public void setProjectNormsViewBean(ProjectNormsViewBean projectNormsViewBean) {
		this.projectNormsViewBean = projectNormsViewBean;
	}

}
