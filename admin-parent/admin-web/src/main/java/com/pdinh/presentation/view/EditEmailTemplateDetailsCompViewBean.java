package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;

@ManagedBean(name = "editEmailTemplateDetailsCompViewBean")
@ViewScoped
public class EditEmailTemplateDetailsCompViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String editEmailBody;
	private String editEmailSubject;
	private String templateType;
	private String templateTitle;
	private String selectedLanguage; // lang code
	private List<ListItemObj> templateLanguages;
	private List<ListItemObj> newLangs;
	private String newLanguage;
	private String newLangCode;
	private List<String> copyPasteKeywords;

	private List<ListItemObj> senderKeyList;

	private String selectedSenderKey;
	private String customEmailFrom;
	private String customEmailReplyTo;
	private List<String> emailRuleNames;

	private List<ListItemObj> templateTypes; // called Prod or Product in UI
	private String allProducts;
	private Boolean renderTemplateTypesList = false;
	private Boolean renderTemplateTypeOutput = true;
	private String selectedTemplateType;

	private List<ListItemObj> templateConstants; // called Style in UI
	private Boolean renderTemplateConstantsList = false;
	private String selectedConstantType;

	private boolean renderedCustomFields = false;
	private boolean renderedIncludedInRules = false;

	private boolean disabledAddTranslation = true;
	private boolean disabledNewLangs = false;
	private boolean disabledDeleteTranslation = true;
	private EmailTemplateObj currentTemplate;
	private EmailTextObj currentText;

	public String getEditEmailBody() {
		return editEmailBody;
	}

	public void setEditEmailBody(String editEmailBody) {
		this.editEmailBody = editEmailBody;
	}

	public String getEditEmailSubject() {
		return editEmailSubject;
	}

	public void setEditEmailSubject(String editEmailSubject) {
		this.editEmailSubject = editEmailSubject;
	}

	public String getNewLangCode() {
		return newLangCode;
	}

	public void setNewLangCode(String newLangCode) {
		this.newLangCode = newLangCode;
	}

	public List<ListItemObj> getNewLangs() {
		return newLangs;
	}

	public void setNewLangs(List<ListItemObj> newLangs) {
		this.newLangs = newLangs;
	}

	public String getNewLanguage() {
		return newLanguage;
	}

	public void setNewLanguage(String newLanguage) {
		this.newLanguage = newLanguage;
	}

	public List<String> getCopyPasteKeywords() {
		return copyPasteKeywords;
	}

	public void setCopyPasteKeywords(List<String> copyPasteKeywords) {
		this.copyPasteKeywords = copyPasteKeywords;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getTemplateTitle() {
		return templateTitle;
	}

	public void setTemplateTitle(String templateTitle) {
		this.templateTitle = templateTitle;
	}

	public String getSelectedSenderKey() {
		return selectedSenderKey;
	}

	public void setSelectedSenderKey(String selectedSenderKey) {
		this.selectedSenderKey = selectedSenderKey;
	}

	public String getCustomEmailFrom() {
		return customEmailFrom;
	}

	public void setCustomEmailFrom(String customEmailFrom) {
		this.customEmailFrom = customEmailFrom;
	}

	public String getCustomEmailReplyTo() {
		return customEmailReplyTo;
	}

	public void setCustomEmailReplyTo(String customEmailReplyTo) {
		this.customEmailReplyTo = customEmailReplyTo;
	}

	public List<ListItemObj> getSenderKeyList() {
		return senderKeyList;
	}

	public void setSenderKeyList(List<ListItemObj> senderKeyList) {
		this.senderKeyList = senderKeyList;
	}

	public List<ListItemObj> getTemplateTypes() {
		return templateTypes;
	}

	public void setTemplateTypes(List<ListItemObj> templateTypes) {
		this.templateTypes = templateTypes;
	}

	public Boolean getRenderTemplateTypesList() {
		return renderTemplateTypesList;
	}

	public void setRenderTemplateTypesList(Boolean renderTemplateTypesList) {
		this.renderTemplateTypesList = renderTemplateTypesList;
	}

	public Boolean getRenderTemplateTypeOutput() {
		return renderTemplateTypeOutput;
	}

	public void setRenderTemplateTypeOutput(Boolean renderTemplateTypeOutput) {
		this.renderTemplateTypeOutput = renderTemplateTypeOutput;
	}

	public String getSelectedTemplateType() {
		return selectedTemplateType;
	}

	public void setSelectedTemplateType(String selectedTemplateType) {
		this.selectedTemplateType = selectedTemplateType;
	}

	public List<ListItemObj> getTemplateConstants() {
		return templateConstants;
	}

	public void setTemplateConstants(List<ListItemObj> templateConstants) {
		this.templateConstants = templateConstants;
	}

	public Boolean getRenderTemplateConstantsList() {
		return renderTemplateConstantsList;
	}

	public void setRenderTemplateConstantsList(Boolean renderTemplateConstantsList) {
		this.renderTemplateConstantsList = renderTemplateConstantsList;
	}

	public String getSelectedConstantType() {
		return selectedConstantType;
	}

	public void setSelectedConstantType(String selectedConstantType) {
		this.selectedConstantType = selectedConstantType;
	}

	public boolean isRenderedCustomFields() {
		return renderedCustomFields;
	}

	public void setRenderedCustomFields(boolean renderedCustomFields) {
		this.renderedCustomFields = renderedCustomFields;
	}

	public boolean isRenderedIncludedInRules() {
		return renderedIncludedInRules;
	}

	public void setRenderedIncludedInRules(boolean renderedIncludedInRules) {
		this.renderedIncludedInRules = renderedIncludedInRules;
	}

	public boolean isDisabledDeleteTranslation() {
		return disabledDeleteTranslation;
	}

	public void setDisabledDeleteTranslation(boolean disabledDeleteTranslation) {
		this.disabledDeleteTranslation = disabledDeleteTranslation;
	}

	public boolean isDisabledAddTranslation() {
		return disabledAddTranslation;
	}

	public void setDisabledAddTranslation(boolean disabledAddTranslation) {
		this.disabledAddTranslation = disabledAddTranslation;
	}

	public EmailTemplateObj getCurrentTemplate() {
		return currentTemplate;
	}

	public void setCurrentTemplate(EmailTemplateObj currentTemplate) {
		this.currentTemplate = currentTemplate;
	}

	public List<ListItemObj> getTemplateLanguages() {
		return templateLanguages;
	}

	public void setTemplateLanguages(List<ListItemObj> templateLanguages) {
		this.templateLanguages = templateLanguages;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public EmailTextObj getCurrentText() {
		return currentText;
	}

	public void setCurrentText(EmailTextObj currentText) {
		this.currentText = currentText;
	}

	public boolean isDisabledNewLangs() {
		return disabledNewLangs;
	}

	public void setDisabledNewLangs(boolean disabledNewLangs) {
		this.disabledNewLangs = disabledNewLangs;
	}

	public List<String> getEmailRuleNames() {
		return emailRuleNames;
	}

	public void setEmailRuleNames(List<String> emailRuleNames) {
		this.emailRuleNames = emailRuleNames;
	}

	public String getAllProducts() {
		return allProducts;
	}

	public void setAllProducts(String allProducts) {
		this.allProducts = allProducts;
	}

}
