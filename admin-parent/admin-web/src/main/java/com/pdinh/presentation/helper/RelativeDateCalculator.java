package com.pdinh.presentation.helper;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RelativeDateCalculator {
		
	private static final Logger log = LoggerFactory.getLogger(RelativeDateCalculator.class);
	
	public static Date calculateRelativeDate(boolean daysAfter, int days, Date dueDate) {
		//Calendar cal = Calendar.getInstance();
		Calendar cal = new GregorianCalendar();
		days = getDays(daysAfter, days);
		
		if(dueDate != null) {
			//cal.setTimeInMillis(dueDate.getTime());
			cal.setTime(dueDate);
			cal.add(Calendar.DATE, days);
		}
		
		return cal.getTime();
	}	
	
	public static Date calculateRelativeDateWithTime(boolean daysAfter, int days, Date dueDate, int hour, int minute, TimeZone zone){
		
		Calendar cal = new GregorianCalendar();
		days = getDays(daysAfter, days);
		
		if(dueDate != null) {
			TimeZone serverZone = TimeZone.getDefault();

			cal.setTimeZone(zone);
			cal.setTime(dueDate);
			//Dont mess with this...it works somehow
			log.trace("Due date before server offset: {}", cal.getTimeInMillis());
			cal.add(Calendar.MILLISECOND, serverZone.getOffset(dueDate.getTime()));
			log.trace("Due date after server offset: {}", cal.getTimeInMillis());
			cal.add(Calendar.MILLISECOND, -zone.getOffset(dueDate.getTime()));
			log.debug("Adjusted Due Date: {}.  Hour: {}", cal.getTime(), cal.get(Calendar.HOUR_OF_DAY));

			cal.add(Calendar.DATE, days);
			cal.add(Calendar.HOUR_OF_DAY, hour);
			log.trace("Hour of Day after ADD: {}.  Hours Added: {}", cal.get(Calendar.HOUR_OF_DAY), hour);
			cal.add(Calendar.MINUTE, minute);
			
		}
		
		return cal.getTime();
	}

	private static int getDays(boolean daysAfter, int days) {
		return (daysAfter) ? days :  (days *= -1);
	}

	public static boolean validDueDate(Date dueDate) {
		boolean valid = false;
		if(dueDate != null) {
			if(dueDate.before(new Date())) {
				valid = false;
			} else {
				valid = true;
			}
		}
		return valid;	
	}
	
}
