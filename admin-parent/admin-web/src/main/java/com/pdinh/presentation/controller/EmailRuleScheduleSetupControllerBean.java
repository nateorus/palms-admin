package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleScheduleTypeDao;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleType;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.presentation.domain.EmailRuleScheduleTypeObj;
import com.pdinh.presentation.domain.FixedDateObj;
import com.pdinh.presentation.domain.RelativeDateObj;
import com.pdinh.presentation.helper.RelativeDateCalculator;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EmailRuleScheduleSetupViewBean;
import com.pdinh.presentation.view.EmailRuleSetupViewBean;

@ManagedBean
@ViewScoped
public class EmailRuleScheduleSetupControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(EmailRuleScheduleSetupControllerBean.class);
	
	@EJB
	EmailRuleScheduleTypeDao emailRuleScheduleTypeDao;
	
	@ManagedProperty(name = "emailRuleScheduleSetupViewBean", value = "#{emailRuleScheduleSetupViewBean}")
	private EmailRuleScheduleSetupViewBean emailRuleScheduleSetupViewBean;	

	@ManagedProperty(name = "emailRuleSetupViewBean", value = "#{emailRuleSetupViewBean}")
	private EmailRuleSetupViewBean emailRuleSetupViewBean;	

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	private boolean firstTime = true;
	private boolean inNew;
	private boolean validationErrors;
	
	public void initView(){
		if (firstTime) {
			firstTime = false;
			emailRuleScheduleSetupViewBean.setScheduleTypeMenuItems(fetchRuleScheduleTypes());
			emailRuleScheduleSetupViewBean.setAvailableTimeZones(getTimezoneIds());
		}
	}
	
	public void initForNew() {
		setInNew(true);
		
		emailRuleScheduleSetupViewBean.ClearAllFields();
		emailRuleScheduleSetupViewBean.hideAllSections();
		emailRuleScheduleSetupViewBean.setScheduleTypeDisabled(false);
		emailRuleScheduleSetupViewBean.setSelectedTimeZone(emailRuleScheduleSetupViewBean.getTimeZone().getID());
		
		if(sessionBean.getProject() != null) {
			emailRuleScheduleSetupViewBean.setProjectDueDate(sessionBean.getProject().getDueDate());
			calculateRelativeDate();
		}
		
	}

	public void initForEdit(Object obj) {
		setInNew(false);
		
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> initForEdit {}", obj);
		
		emailRuleScheduleSetupViewBean.setScheduleItemForEdit(obj);

		FixedDateObj fixedDate = new FixedDateObj();
		RelativeDateObj relativeDate = new RelativeDateObj();
		
		emailRuleScheduleSetupViewBean.setScheduleTypeDisabled(true);
		
		if (obj.getClass().equals(fixedDate.getClass())) {
			fixedDate = (FixedDateObj) obj;
			emailRuleScheduleSetupViewBean.setFixedDate(fixedDate.getDate());
			if (!(fixedDate.getTimezone() == null)){
				emailRuleScheduleSetupViewBean.setSelectedTimeZone(fixedDate.getTimezone().getID());
				emailRuleScheduleSetupViewBean.setTimeZone(fixedDate.getTimezone());
			}else{
				emailRuleScheduleSetupViewBean.setSelectedTimeZone(emailRuleScheduleSetupViewBean.getTimeZone().getID());
			}
			emailRuleScheduleSetupViewBean.showFixedSection();
		} else if (obj.getClass().equals(relativeDate.getClass())) {
			relativeDate = (RelativeDateObj) obj;
			emailRuleScheduleSetupViewBean.setRelativeDays(relativeDate.getDays());
			emailRuleScheduleSetupViewBean.setDaysAfter(relativeDate.getDaysAfter());
			emailRuleScheduleSetupViewBean.setProjectDueDate(relativeDate.getRelativeToDate());
			if (!(relativeDate.getTimezone() == null)){
				emailRuleScheduleSetupViewBean.setSelectedTimeZone(relativeDate.getTimezone().getID());
				emailRuleScheduleSetupViewBean.setTimeZone(relativeDate.getTimezone());
			}else{
				emailRuleScheduleSetupViewBean.setSelectedTimeZone(emailRuleScheduleSetupViewBean.getTimeZone().getID());
			}
			
			emailRuleScheduleSetupViewBean.setRelativeTime(relativeDate.getTime());
			emailRuleScheduleSetupViewBean.showRelativeSection();
			calculateRelativeDate();
		}
	}
	
	private List<String> getTimezoneIds(){
		String[] timezones = TimeZone.getAvailableIDs();
		return Arrays.asList(timezones);
	}
	
	public void handleSelectZone(AjaxBehaviorEvent event){
		log.debug("Handling Time Zone Selection");
		log.debug("Selected Time Zone: {}", emailRuleScheduleSetupViewBean.getSelectedTimeZone());
		TimeZone zone = TimeZone.getTimeZone(emailRuleScheduleSetupViewBean.getSelectedTimeZone());
		emailRuleScheduleSetupViewBean.setTimeZone(zone);
		if (emailRuleScheduleSetupViewBean.getScheduleType() == 2){
			calculateRelativeDate();
		}
	}
	
	private List<EmailRuleScheduleTypeObj> fetchRuleScheduleTypes() {
		List<EmailRuleScheduleTypeObj> ruleScheduleTypeObjs = new ArrayList<EmailRuleScheduleTypeObj>();
		List<EmailRuleScheduleType> ruleScheduleTypes = emailRuleScheduleTypeDao.findAll();
		for(EmailRuleScheduleType ruleScheduleType : ruleScheduleTypes) {
			EmailRuleScheduleTypeObj ruleScheduleTypeObj = new EmailRuleScheduleTypeObj();
			ruleScheduleTypeObj.setId(ruleScheduleType.getId());
			ruleScheduleTypeObj.setName(ruleScheduleType.getName());
			ruleScheduleTypeObj.setCode(ruleScheduleType.getCode());
			ruleScheduleTypeObjs.add(ruleScheduleTypeObj);
		}
		return ruleScheduleTypeObjs;
	}

	private boolean validProjectDueDate() {
		Project project = sessionBean.getProject();
		if(project != null) {
			if(project.getDueDate() != null) {
				return RelativeDateCalculator.validDueDate(project.getDueDate());
			}
		}
		return false;
	}
	
	public void handleScheduleType(AjaxBehaviorEvent event) {
		EmailRuleScheduleSetupViewBean view = emailRuleScheduleSetupViewBean;
		int type = view.getScheduleType();
		
		// reseting the errors flag
		setValidationErrors(false);
		
		switch (type) {
			case 1:
				view.showFixedSection();
				break;
			case 2:
				if(validProjectDueDate()) {
					view.showRelativeSection();	
				} else {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "Invalid Project due date, it is either in the past or is not set.", ""));
					setValidationErrors(true);
					view.hideAllSections();
				}
				break;
			default:
				view.hideAllSections();
				break;
		}
	}
	
	public void handleScheduleSave(ActionEvent event) {
		log.debug(">>>>>>>>>>>>>>>>> isValidationErrors: {}", isValidationErrors());
		if(!isValidationErrors()) {
			save();
		} else {
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
		}
	}
	
	private void save() {
		int type = emailRuleScheduleSetupViewBean.getScheduleType();
		FixedDateObj fixedDateObj = new FixedDateObj();
		RelativeDateObj relativeDateObj = new RelativeDateObj();
		Date fixedDate = emailRuleScheduleSetupViewBean.getFixedDate();
		
		if (type == 1) {
			if(fixedDate != null) {
				fixedDateObj.setDate(emailRuleScheduleSetupViewBean.getFixedDate());
				fixedDateObj.setTimezone(emailRuleScheduleSetupViewBean.getTimeZone());
				emailRuleSetupViewBean.getScheduledItems().add(fixedDateObj);	
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Date cannot be empty.", ""));
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			}
		} else if (type == 2) {
			relativeDateObj.setDays(emailRuleScheduleSetupViewBean.getRelativeDays());
			relativeDateObj.setDaysAfter(emailRuleScheduleSetupViewBean.getDaysAfter());
			//relativeDateObj.setRelativeToDate(emailRuleScheduleSetupViewBean.getProjectDueDate());
			relativeDateObj.setTimezone(emailRuleScheduleSetupViewBean.getTimeZone());
			if (emailRuleScheduleSetupViewBean.getRelativeTime() == null){
				log.debug("Relative Time is NULL");
			}else{
				log.debug("Relative Time is {}", emailRuleScheduleSetupViewBean.getRelativeDate());
				relativeDateObj.setTime(emailRuleScheduleSetupViewBean.getRelativeDate());
			}
			
			
			relativeDateObj.setRelativeToDate(addTimeToProjectDueDate(relativeDateObj));
			
			emailRuleSetupViewBean.getScheduledItems().add(relativeDateObj);
		}
		debugListOfSchedules(emailRuleSetupViewBean.getScheduledItems());		
	}	
	
	private Date addTimeToProjectDueDate(RelativeDateObj relativeDateObj){
		Calendar cal = new GregorianCalendar();
		cal.setTime(sessionBean.getProject().getDueDate());
		if (!(relativeDateObj.getTime() == null)){
			Calendar time = new GregorianCalendar();
			time.setTime(relativeDateObj.getTime());
			log.debug("Relative Date Obj Time: {}", relativeDateObj.getTime());
			cal.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
		}else{
			log.debug("Time is NULL");
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
		}
		return cal.getTime();
	}
	
	public void handleScheduleEdit(ActionEvent event) {
		Object obj = emailRuleScheduleSetupViewBean.getScheduleItemForEdit();
		
		FixedDateObj fixedDateObj = new FixedDateObj();
		RelativeDateObj relativeDateObj = new RelativeDateObj();
		
		if (obj.getClass().equals(fixedDateObj.getClass())) {
			fixedDateObj = (FixedDateObj) obj;
			fixedDateObj.setDate(emailRuleScheduleSetupViewBean.getFixedDate());
			fixedDateObj.setTimezone(emailRuleScheduleSetupViewBean.getTimeZone());
			obj = fixedDateObj;
		} else if (obj.getClass().equals(relativeDateObj.getClass())) {
			relativeDateObj = (RelativeDateObj) obj;
			relativeDateObj.setDays(emailRuleScheduleSetupViewBean.getRelativeDays());
			relativeDateObj.setDaysAfter(emailRuleScheduleSetupViewBean.getDaysAfter());
		//	relativeDateObj.setRelativeToDate(emailRuleScheduleSetupViewBean.getProjectDueDate());
			relativeDateObj.setTimezone(emailRuleScheduleSetupViewBean.getTimeZone());
			if (emailRuleScheduleSetupViewBean.getRelativeTime() == null){
				log.debug("Relative Time is NULL");
			}else{
				log.debug("Relative Time is {}", emailRuleScheduleSetupViewBean.getRelativeDate());
				relativeDateObj.setTime(emailRuleScheduleSetupViewBean.getRelativeDate());
			}
			
			relativeDateObj.setRelativeToDate(addTimeToProjectDueDate(relativeDateObj));
			obj = relativeDateObj;
		}		
	}
	
	public void calculateRelativeDate() {
		if(validProjectDueDate()){
			int days = emailRuleScheduleSetupViewBean.getRelativeDays();
			Boolean daysAfter = emailRuleScheduleSetupViewBean.getDaysAfter();
			Date projectDueDate = sessionBean.getProject().getDueDate();
			
			TimeZone zone = emailRuleScheduleSetupViewBean.getTimeZone();
			
			Date relativeDate;
			
			Calendar time = new GregorianCalendar();
			if (!(emailRuleScheduleSetupViewBean.getRelativeTime() == null)){
				time.setTime(emailRuleScheduleSetupViewBean.getRelativeTime());
			}else{
				time.setTime(projectDueDate);
			}
			time.setTimeZone(zone);
			
			int hour = time.get(Calendar.HOUR_OF_DAY);
			int minute = time.get(Calendar.MINUTE);
			relativeDate = RelativeDateCalculator.calculateRelativeDateWithTime(daysAfter, days, projectDueDate, hour, minute, zone);
			
			SimpleDateFormat displayDate = new SimpleDateFormat("dd-MMM-yyyy hh:mm a z");
			displayDate.setTimeZone(emailRuleScheduleSetupViewBean.getTimeZone());
			
			emailRuleScheduleSetupViewBean.setRelativeDate(relativeDate);
			
			emailRuleScheduleSetupViewBean.setEstimatedDateString(displayDate.format(relativeDate));
			log.debug(">>>>>>>>>>>>>>>>>>>> calculateRelativeDate = {}", relativeDate);
		}
	}
	
	private void debugListOfSchedules(List<Object> objs) {
		for(Object obj : objs) {
			log.debug(">>>>> Object: {} Class: {}", obj, obj.getClass());
		}
	}
	
	
	public boolean getInNew() {
		return inNew;
	}

	public void setInNew(boolean inNew) {
		this.inNew = inNew;
	}

	public EmailRuleScheduleSetupViewBean getEmailRuleScheduleSetupViewBean() {
		return emailRuleScheduleSetupViewBean;
	}

	public void setEmailRuleScheduleSetupViewBean(
			EmailRuleScheduleSetupViewBean emailRuleScheduleSetupViewBean) {
		this.emailRuleScheduleSetupViewBean = emailRuleScheduleSetupViewBean;
	}

	public EmailRuleSetupViewBean getEmailRuleSetupViewBean() {
		return emailRuleSetupViewBean;
	}

	public void setEmailRuleSetupViewBean(EmailRuleSetupViewBean emailRuleSetupViewBean) {
		this.emailRuleSetupViewBean = emailRuleSetupViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean isValidationErrors() {
		return validationErrors;
	}

	public void setValidationErrors(boolean validationErrors) {
		this.validationErrors = validationErrors;
	}
}
