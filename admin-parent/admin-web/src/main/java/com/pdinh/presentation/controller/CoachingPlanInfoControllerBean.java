package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.ms.dao.CoachingPlanDao;
import com.pdinh.data.ms.dao.CoachingPlanStatusTypeDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.CoachingPlan;
import com.pdinh.persistence.ms.entity.CoachingPlanStatusType;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.CoachingPlanInfoViewBean;
import com.pdinh.presentation.view.ParticipantInfoViewBean;
import com.pdinh.presentation.view.ParticipantListViewBean;

@ManagedBean(name = "coachingPlanInfoControllerBean")
@ViewScoped
public class CoachingPlanInfoControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Resource(name = "application/config_properties")
	private Properties configProperties;
	final static Logger logger = LoggerFactory.getLogger(CoachingPlanInfoControllerBean.class);
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "participantInfoViewBean", value = "#{participantInfoViewBean}")
	private ParticipantInfoViewBean participantInfoViewBean;

	@ManagedProperty(name = "coachingPlanInfoViewBean", value = "#{coachingPlanInfoViewBean}")
	private CoachingPlanInfoViewBean coachingPlanInfoViewBean;

	@ManagedProperty(name = "participantListViewBean", value = "#{participantListViewBean}")
	private ParticipantListViewBean participantListViewBean;

	@EJB
	private CoachingPlanDao coachingPlanDao;

	@EJB
	private CoachingPlanStatusTypeDao coachingPlanStatusTypeDao;

	@EJB
	private ProjectDao projectDao;
	
	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private UserDao userDao;
	
	@EJB
	private ExternalSubjectServiceLocal externalSubjectService;
	
	@EJB
	private AuthorizationServiceLocal authorizationService;
	
	@EJB
	private EntityServiceLocal entityService;
	
	@EJB
	private PermissionServiceLocal permissionService;
	
	@EJB
	private PalmsRealmDao realmDao;
	
	@EJB
	private GlobalRolesBean rolesBean;
	
	@EJB
	private PalmsSubjectDao subjectDao;

	private boolean isUpdate = false;

	private CoachingPlan previousCoachingPlan;

	/*
	 * Initializations
	 */
	public void initCoaches() {
		logger.debug("In CoachingPlanInfoController init coaches:");
		//List<ParticipantObj> coaches = findCoachesInCompany();
		//coachingPlanInfoViewBean.setCoaches(coaches);
	}

	public void initSetCoachAndTimeFrame() {
		logger.debug("In CoachingPlanInfoController init:");

		//List<ParticipantObj> coaches = findCoachesInCompany();
		List<ExternalSubjectDataObj> coaches = findAllCoaches();
		coachingPlanInfoViewBean.setCoachSubjects(coaches);
		logger.debug("\tCoach size: {}", coaches.size());
		coachingPlanInfoViewBean.setDisableSave(true);
		boolean isCoachingProject = sessionBean.getProject().getProjectType().getProjectTypeCode().equals(ProjectType.COACH);
		coachingPlanInfoViewBean.setIsCoachingProject(isCoachingProject);
		participantInfoViewBean.setBottomButtonLabel("Save");
		// return if no user is selected
		if (sessionBean.getUser() == null)
			return;
		// check to see if coaching plan already exists
		previousCoachingPlan = coachingPlanDao.findByProjectAndParticipant(sessionBean.getProject().getProjectId(),
				sessionBean.getUser().getUsersId());
		if (previousCoachingPlan == null) {
			// populate for new coaching plan
			logger.debug("\tNew Coaching Plan");
			coachingPlanInfoViewBean.setStartDate(new Date());
			coachingPlanInfoViewBean.setEndDate(new Date());
			//coachingPlanInfoViewBean.setSelectedCoach(null);
			coachingPlanInfoViewBean.setTypeText("");
			coachingPlanInfoViewBean.setTargetLevelTypeId(-1);
			coachingPlanInfoViewBean.setSelectedCoachObj(null);
			isUpdate = false;
			// might have to query again to see if new coaches have been added
		} else {
			// populate with existing coaching plan info
			logger.debug("\tUpdate Coaching Plan:\n\told - {}", previousCoachingPlan.toString());
			// loop through coaches to find match to set previously selected
			// coach
			logger.debug("\t\tloop through coaches to find match to set previously selected coach: {}",
					previousCoachingPlan.getCoach().getUsersId());
			PalmsSubject prevCoach = subjectDao.findSubjectByLinkedId(previousCoachingPlan.getCoach().getUsersId());
			if (prevCoach == null){
				prevCoach = subjectDao.findSubjectByExternalId(previousCoachingPlan.getCoach().getUsersId());
			}
			if (!(prevCoach == null)){
				
				boolean authorized = entityService.isAuthorized(entityService.getDefaultContext(prevCoach), EntityType.PROJECT_TYPE, previousCoachingPlan.getProject().getProjectId(), false);
				for (ExternalSubjectDataObj c : coaches) {
					logger.debug("\t\tcoach: {}", c.getUsername());
					if (c.getUsername().equals(prevCoach.getPrincipal()) && c.getRealm_id() == prevCoach.getRealm().getRealm_id()) {
						coachingPlanInfoViewBean.setSelectedCoachObj(c);
						coachingPlanInfoViewBean.setSelectedCoachAuthorized(!authorized);
						break;
					}
	
				}
			}
			logger.debug("\tUpdate Coaching Plan:\n\tnew - {}", previousCoachingPlan.toString());

			coachingPlanInfoViewBean.setStartDate(previousCoachingPlan.getStartDate());
			coachingPlanInfoViewBean.setEndDate(previousCoachingPlan.getEndDate());
			coachingPlanInfoViewBean.setTypeText(previousCoachingPlan.getTypeText());
			try {
				coachingPlanInfoViewBean.setTargetLevelTypeId(previousCoachingPlan.getTargetLevelType()
						.getTargetLevelTypeId());
			} catch (Exception e) {
				coachingPlanInfoViewBean.setTargetLevelTypeId(-1);
			}
			coachingPlanInfoViewBean.setDisableSave(false);
			isUpdate = true;
			logger.debug("CoachingPlanInfoController init END");
		}

	}

	/*
	 * Handlers
	 */

	public void handleSetCoachAndTimeFrameSave() {
		logger.info("In CoachingPlanInfoController save:");
		logger.info("CoachingPlanInfoController: finished init-ing obj from view");
		logger.info("CoachingPlanInfoViewBean selectedCoach: {}, {}", coachingPlanInfoViewBean.getSelectedCoachObj().getFirstname(), coachingPlanInfoViewBean.getSelectedCoachObj().getLastname());
		Subject subject = SecurityUtils.getSubject();
		ExternalSubjectDataObj userObj = coachingPlanInfoViewBean.getSelectedCoachObj();
		PalmsRealm realm = realmDao.findById(userObj.getRealm_id());
		User coach = new User();
		PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(userObj.getUsername(), userObj.getRealm_id());
		if (realm.getRealm_name().equals(configProperties.getProperty("clientRealmName"))){
			coach = userDao.findById(userObj.getExternal_id());
		}else {
			
			coach = userDao.findById(subj.getLinked_id());
		}
		
		authorizationService.grantProjectUserAccess(subj, sessionBean.getProject(), sessionBean.getUser(), subject.getPrincipal().toString(), "coach", EntityRule.ALLOW);
		if (coach == null){
			logger.error("Unable to locate Linked account for coach: {}", userObj.getFirstLastEmail());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "No Linked Account for Coach.", ""));
			RequestContext requestContext = RequestContext.getCurrentInstance();
			requestContext.addCallbackParam("notValid", true);
		}else{
			logger.info("\tcoach: {}, {}",coach.getFirstname(), coach.getLastname());
		}
		
		Date startDate = coachingPlanInfoViewBean.getStartDate();
		logger.info("\tstartdate: {}", startDate.toString());
		Date endDate = coachingPlanInfoViewBean.getEndDate();
		logger.info("\tendDate: {}", endDate.toString());
		String typeText = coachingPlanInfoViewBean.getTypeText();
		logger.info("\ttypeText: {}", typeText.toString());

		// validate dates
		if (startDate.after(endDate)) {
			logger.info("CoachingPlanInfoController: FAIL - dates validated");
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Dates not chronological.", ""));
			RequestContext requestContext = RequestContext.getCurrentInstance();
			requestContext.addCallbackParam("notValid", true);

			return;
		}
		logger.info("CoachingPlanInfoController: PASS - dates validated");
		// make sure that user is not their own coach
		if (coach.getUsersId() == sessionBean.getUser().getUsersId()) {
			logger.info("CoachingPlanInfoController: FAIL - coach is not their own coach ");
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "You can not be your own coach.", ""));
			RequestContext requestContext = RequestContext.getCurrentInstance();
			requestContext.addCallbackParam("notValid", true);
			return;
		}
		logger.info("CoachingPlanInfoController: PASS - coach is not their own coach ");

		if (isUpdate) {
			logger.info("CoachingPlanInfoController: performing update");
			logger.info("\told coachingPlan - " + previousCoachingPlan.toString());
			previousCoachingPlan.setCoach(coach);
			previousCoachingPlan.setStartDate(startDate);
			previousCoachingPlan.setEndDate(endDate);
			previousCoachingPlan.setTypeText(typeText);
			logger.info("\tnew coachingPlan - " + previousCoachingPlan.toString());
			logger.info("CoachingPlanInfoController: targetLevelType - "
					+ coachingPlanInfoViewBean.getTargetLevelTypeId());
			if (coachingPlanInfoViewBean.getTargetLevelTypeId() != null
					&& coachingPlanInfoViewBean.getTargetLevelTypeId() > 0) {
				previousCoachingPlan.setTargetLevelType(new TargetLevelType(coachingPlanInfoViewBean
						.getTargetLevelTypeId()));
			} else {
				previousCoachingPlan.setTargetLevelType(null);
			}
			coachingPlanDao.update(previousCoachingPlan);
		} else {
			CoachingPlan coachingPlan = new CoachingPlan();
			User participant = sessionBean.getUser();
			Project currentProject = sessionBean.getProject();
			CoachingPlanStatusType coachingPlanStatusType = coachingPlanStatusTypeDao
					.findById(CoachingPlanStatusType.NOT_STARTED);
			coachingPlan.setCoach(coach);
			coachingPlan.setParticipant(participant);
			coachingPlan.setStartDate(startDate);
			coachingPlan.setEndDate(endDate);
			coachingPlan.setProject(currentProject);
			coachingPlan.setCoachingPlanStatusType(coachingPlanStatusType);
			coachingPlan.setTypeText(typeText);
			if (coachingPlanInfoViewBean.getTargetLevelTypeId() != null
					&& coachingPlanInfoViewBean.getTargetLevelTypeId() > 0) {
				coachingPlan.setTargetLevelType(new TargetLevelType(coachingPlanInfoViewBean.getTargetLevelTypeId()));
			} else {
				coachingPlan.setTargetLevelType(null);
			}
			coachingPlanDao.create(coachingPlan);
		}
	}

	public void handleRowSelect(SelectEvent event) {
		coachingPlanInfoViewBean.setDisableSave(false);
		coachingPlanInfoViewBean.setSelectedCoachObj((ExternalSubjectDataObj) event.getObject());
		coachingPlanInfoViewBean.setSelectedCoachAuthorized(false);
	}

	public void handleRowUnselect(UnselectEvent event) {
		coachingPlanInfoViewBean.setDisableSave(true);
		coachingPlanInfoViewBean.setSelectedCoachObj(null);
	}

	/*
	 * helper functions
	 */
	private List<ParticipantObj> findCoachesInCompany() {
		logger.debug("In CoachingPlanInfoController find coaches in co.:");
		List<User> coUsers = userDao.findAllCoaches();
		List<ParticipantObj> coaches = new ArrayList<ParticipantObj>();
		for (User user : coUsers) {
			coaches.add(userToParticipantObject(user));
		}
		return coaches;
	}
	
	private List<ExternalSubjectDataObj> findAllCoaches(){
		List<ExternalSubjectDataObj> clientCoaches = new ArrayList<ExternalSubjectDataObj>();
		List<ExternalSubjectDataObj> internalCoaches = new ArrayList<ExternalSubjectDataObj>();
		
		List<String> listRealmNames = Arrays.asList(configProperties.getProperty("palmsRealmNames").split(","));

		PalmsRealm clientRealm = realmDao.findRealmByName(configProperties.getProperty("clientRealmName"));
	
		Role coach = rolesBean.getRole("Coach");
		
		internalCoaches.addAll(externalSubjectService.getSubjectsWithRole(listRealmNames, coach));
		clientCoaches = externalSubjectService.getSubjectsWithRole(clientRealm, coach, false);
		clientCoaches.addAll(internalCoaches);
		return clientCoaches;
		
	}

	private ParticipantObj userToParticipantObject(User user) {
		ParticipantObj po = new ParticipantObj();
		po.setFirstName(user.getFirstname());
		po.setLastName(user.getLastname());
		po.setUserName(user.getUsername());
		po.setUserId(user.getUsersId());
		po.setPassword(user.getPassword());
		po.setEmail(user.getEmail());
		po.setLanguageId(user.getLangPref());
		po.setOptional1(user.getOptional1());
		po.setOptional2(user.getOptional2());
		po.setOptional3(user.getOptional3());
		po.setSupervisorName(user.getSupervisorName());
		po.setSupervisorEmail(user.getSupervisorEmail());
		return po;
	}

	/*
	 * Setters/Getters
	 */
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ParticipantInfoViewBean getParticipantInfoViewBean() {
		return participantInfoViewBean;
	}

	public void setParticipantInfoViewBean(ParticipantInfoViewBean participantInfoViewBean) {
		this.participantInfoViewBean = participantInfoViewBean;
	}

	public CoachingPlanInfoViewBean getCoachingPlanInfoViewBean() {
		return coachingPlanInfoViewBean;
	}

	public void setCoachingPlanInfoViewBean(CoachingPlanInfoViewBean coachingPlanInfoViewBean) {
		this.coachingPlanInfoViewBean = coachingPlanInfoViewBean;
	}

	public CoachingPlanDao getCoachingPlanDao() {
		return coachingPlanDao;
	}

	public void setCoachingPlanDao(CoachingPlanDao coachingPlanDao) {
		this.coachingPlanDao = coachingPlanDao;
	}

	public ParticipantListViewBean getParticipantListViewBean() {
		return participantListViewBean;
	}

	public void setParticipantListViewBean(ParticipantListViewBean participantListViewBean) {
		this.participantListViewBean = participantListViewBean;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	@Override
	public String toString() {
		return "CoachingPlanInfoControllerBean [sessionBean=" + sessionBean + ", participantInfoViewBean="
				+ participantInfoViewBean + ", coachingPlanInfoViewBean=" + coachingPlanInfoViewBean
				+ ", participantListViewBean=" + participantListViewBean + ", coachingPlanDao=" + coachingPlanDao
				+ ", coachingPlanStatusTypeDao=" + coachingPlanStatusTypeDao + ", projectDao=" + projectDao
				+ ", userDao=" + userDao + ", isUpdate=" + isUpdate + ", previousCoachingPlan=" + previousCoachingPlan
				+ "]";
	}
}
