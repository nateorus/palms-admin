package com.pdinh.presentation.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean(name = "exportCredentials")
@ViewScoped
public class ExportCredentialsControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	// private static final Logger log =
	// LoggerFactory.getLogger(ExportCredentialsControllerBean.class);

	@ManagedProperty("#{msg}")
	ResourceBundle message;

	@Resource(mappedName = "custom/tlt_properties")
	private Properties tltProperties;

	@EJB
	LanguageSingleton languageSingleton;

	public void postProcessXLS(Object document) {
		HSSFWorkbook workbook = (HSSFWorkbook) document;
		HSSFSheet sheetCredentials = workbook.getSheetAt(0);
		HSSFSheet sheetDisclaimer = workbook.createSheet(getMsg("export.credentials.tabename.1"));

		workbook.setSheetName(workbook.getSheetIndex(sheetCredentials), getMsg("export.credentials.tabename.2"));

		HSSFRow disclaimerRow = sheetDisclaimer.createRow(sheetDisclaimer.getLastRowNum());

		HSSFCell disclaimerCell = disclaimerRow.createCell(0);
		HSSFCellStyle disclaimerCellStyle = workbook.createCellStyle();
		disclaimerCell.setCellValue(getMsg("export.credentials.disclaimer"));

		disclaimerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		disclaimerCellStyle.setWrapText(true);
		disclaimerCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		disclaimerCellStyle.setFillForegroundColor(new HSSFColor.YELLOW().getIndex());
		disclaimerCellStyle.setFillBackgroundColor(new HSSFColor.WHITE().getIndex());
		disclaimerCell.setCellStyle(disclaimerCellStyle);

		sheetDisclaimer.setColumnWidth(0, 32000);

		for (int i = 0; i < sheetCredentials.getRow(0).getLastCellNum(); i++) {
			sheetCredentials.autoSizeColumn(i);
		}

		workbook.setSheetOrder(getMsg("export.credentials.tabename.2"), 1);
	}

	public void preProcessXLS(Object document) {
		Subject subject = SecurityUtils.getSubject();
		HSSFWorkbook workbook = (HSSFWorkbook) document;
		workbook.createInformationProperties();
		SummaryInformation summaryInfo = workbook.getSummaryInformation();
		DocumentSummaryInformation documentSummaryInfo = workbook.getDocumentSummaryInformation();

		summaryInfo.setAuthor(subject.getPrincipal().toString());
		summaryInfo.setCreateDateTime(new Date());
		summaryInfo.setComments(getMsg("export.credentials.disclaimer"));

		documentSummaryInfo.setCompany(getMsg("company.name"));
	}

	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
		Subject subject = SecurityUtils.getSubject();
		Document pdf = (Document) document;
		pdf.setPageSize(PageSize.LETTER.rotate());
		pdf.addCreator(subject.getPrincipal().toString());
		pdf.open();
	}

	public String getParticipantExperienceUrl(ParticipantObj user) {

		if (user == null) {
			return "";
		}

		String lang = (user.getLanguageId() == null) ? "en" : user.getLanguageId();
		String url = tltProperties.getProperty("tltParticipantExperienceUrl", "http://default.url") + "?lang=" + lang;

		return url;
	}

	public String getParticipantLanguage(ParticipantObj user) {

		if (user == null) {
			return "";
		}

		String lang = (user.getLanguageId() == null) ? "en" : user.getLanguageId();
		LanguageObj langObj = languageSingleton.getLangByCode(lang);
		String languageLabel = "";

		if (langObj != null) {
			languageLabel = langObj.getName() + " (" + langObj.getCode() + ")";
		} else {
			languageLabel = "not available";
		}

		return languageLabel;
	}

	private String getMsg(String key) {
		return getMessage().getString(key);
	}

	public ResourceBundle getMessage() {
		return message;
	}

	public void setMessage(ResourceBundle message) {
		this.message = message;
	}
}
