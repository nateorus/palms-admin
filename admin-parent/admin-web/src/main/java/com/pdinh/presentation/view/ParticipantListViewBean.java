package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean(name = "participantListViewBean")
@ViewScoped
public class ParticipantListViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;
	private String firstName;
	private String lastName;
	private String email1;
	private List<ParticipantObj> participants;
	private ParticipantObj selectedParticipant;
	private ParticipantObj[] selectedParticipants;
	private Boolean editParticipantDisabled = true;
	private Boolean delParticipantDisabled = true;
	private Boolean addParticipantsToClipboardDisabled = true;
	private Boolean addParticipantButtonDisabled = true;
	private Boolean createReportsDisabled = true;
	private Boolean auditLogDisabled = true;
	private boolean renderSendEmails = true;
	private boolean sendEmailsDisabled = true;
	private String participantLoginUrl;
	private String loginToken;
	private String loginUsername;
	private boolean manageProjectDisabled = true;
	private List<ParticipantObj> filteredParticipants;
	private boolean reportUploadDisabled = true;
	private boolean renderReportUpload = true;
	private boolean downloadReportDisabled = true;
	private boolean renderDownloadReport = true;

	public void clear() {
		setUserId("");
		setFirstName("");
		setLastName("");
		setEmail1("");
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setSelectedParticipant(ParticipantObj selectedParticipant) {
		this.selectedParticipant = selectedParticipant;
	}

	public ParticipantObj getSelectedParticipant() {
		return selectedParticipant;
	}

	public void setDelParticipantDisabled(Boolean delParticipantDisabled) {
		this.delParticipantDisabled = delParticipantDisabled;
	}

	public Boolean getDelParticipantDisabled() {
		return delParticipantDisabled;
	}

	public void setEditParticipantDisabled(Boolean editParticipantDisabled) {
		// System.out.println("setEditParticipantDisabled " +
		// editParticipantDisabled);
		this.editParticipantDisabled = editParticipantDisabled;
	}

	public Boolean getEditParticipantDisabled() {
		return editParticipantDisabled;
	}

	public ParticipantObj[] getSelectedParticipants() {
		// System.out.println("ParticipantListViewBean.getSelectedParticipants is "
		// + (selectedParticipants != null ? selectedParticipants.length : 0) +
		// " long.");
		return selectedParticipants;
	}

	public void setSelectedParticipants(ParticipantObj[] selectedParticipants) {
		this.selectedParticipants = selectedParticipants;
	}

	public void setAddParticipantsToClipboardDisabled(Boolean addParticipantsToClipboardDisabled) {
		// System.out.println("setAddParticipantsToClipboardDisabled " +
		// addParticipantsToClipboardDisabled);
		this.addParticipantsToClipboardDisabled = addParticipantsToClipboardDisabled;
	}

	public Boolean getAddParticipantsToClipboardDisabled() {
		return addParticipantsToClipboardDisabled;
	}

	public void clearSelectedParticipants() {
		this.selectedParticipants = null;
	}

	public Boolean getCreateReportsDisabled() {
		return createReportsDisabled;
	}

	public void setCreateReportsDisabled(Boolean createReportsDisabled) {
		this.createReportsDisabled = createReportsDisabled;
	}

	public Boolean getAuditLogDisabled() {
		return auditLogDisabled;
	}

	public void setAuditLogDisabled(Boolean auditLogDisabled) {
		this.auditLogDisabled = auditLogDisabled;
	}

	public boolean isRenderSendEmails() {
		return renderSendEmails;
	}

	public void setRenderSendEmails(boolean renderSendEmails) {
		this.renderSendEmails = renderSendEmails;
	}

	public boolean isSendEmailsDisabled() {
		return sendEmailsDisabled;
	}

	public void setSendEmailsDisabled(boolean sendEmailsDisabled) {
		this.sendEmailsDisabled = sendEmailsDisabled;
	}

	public String getParticipantLoginUrl() {
		return participantLoginUrl;
	}

	public void setParticipantLoginUrl(String participantLoginUrl) {
		this.participantLoginUrl = participantLoginUrl;
	}

	public String getLoginToken() {
		return loginToken;
	}

	public void setLoginToken(String loginToken) {
		this.loginToken = loginToken;
	}

	public String getLoginUsername() {
		return loginUsername;
	}

	public void setLoginUsername(String loginUsername) {
		this.loginUsername = loginUsername;
	}

	public Boolean getAddParticipantButtonDisabled() {
		return addParticipantButtonDisabled;
	}

	public void setAddParticipantButtonDisabled(Boolean addParticipantButtonDisabled) {
		this.addParticipantButtonDisabled = addParticipantButtonDisabled;
	}

	public boolean isManageProjectDisabled() {
		return manageProjectDisabled;
	}

	public void setManageProjectDisabled(boolean manageProjectDisabled) {
		this.manageProjectDisabled = manageProjectDisabled;
	}

	public List<ParticipantObj> getFilteredParticipants() {
		return filteredParticipants;
	}

	public void setFilteredParticipants(List<ParticipantObj> filteredParticipants) {
		this.filteredParticipants = filteredParticipants;
	}

	public boolean isReportUploadDisabled() {
		return reportUploadDisabled;
	}

	public void setReportUploadDisabled(boolean reportUploadDisabled) {
		this.reportUploadDisabled = reportUploadDisabled;
	}

	public boolean isRenderReportUpload() {
		return renderReportUpload;
	}

	public void setRenderReportUpload(boolean renderReportUpload) {
		this.renderReportUpload = renderReportUpload;
	}

	public boolean isDownloadReportDisabled() {
		return downloadReportDisabled;
	}

	public void setDownloadReportDisabled(boolean downloadReportDisabled) {
		this.downloadReportDisabled = downloadReportDisabled;
	}

	public boolean isRenderDownloadReport() {
		return renderDownloadReport;
	}

	public void setRenderDownloadReport(boolean renderDownloadReport) {
		this.renderDownloadReport = renderDownloadReport;
	}
}
