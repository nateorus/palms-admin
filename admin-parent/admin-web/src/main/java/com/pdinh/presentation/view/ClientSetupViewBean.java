package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.presentation.domain.LanguageObj;

@ManagedBean
@ViewScoped
public class ClientSetupViewBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int clientId;
	private String clientName;
	private String mainContact;
	private String mainPhone;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private List<LanguageObj> languages;
	private String languageCode;
	private String countryCode;
	private List<ExternalSubjectDataObj> internalTeammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> clientTeammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> removeInternalTeammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> removeClientTeammembers = new ArrayList<ExternalSubjectDataObj>();

	private boolean renderedAddInternalSeparator = false;
	private boolean renderedAddClientSeparator = false;
	
	private boolean internalTeamTabDisabled = true;
	private boolean clientTeamTabDisabled = true;
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public List<LanguageObj> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageObj> languages) {
		this.languages = languages;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getMainContact() {
		return mainContact;
	}

	public void setMainContact(String mainContact) {
		this.mainContact = mainContact;
	}

	public String getMainPhone() {
		return mainPhone;
	}

	public void setMainPhone(String mainPhone) {
		this.mainPhone = mainPhone;
	}

	public boolean isRenderedAddClientSeparator() {
		return renderedAddClientSeparator;
	}

	public void setRenderedAddClientSeparator(boolean renderedAddClientSeparator) {
		this.renderedAddClientSeparator = renderedAddClientSeparator;
	}

	public boolean isRenderedAddInternalSeparator() {
		return renderedAddInternalSeparator;
	}

	public void setRenderedAddInternalSeparator(boolean renderedAddInternalSeparator) {
		this.renderedAddInternalSeparator = renderedAddInternalSeparator;
	}

	public List<ExternalSubjectDataObj> getInternalTeammembers() {
		return internalTeammembers;
	}

	public void setInternalTeammembers(
			List<ExternalSubjectDataObj> internalTeammembers) {
		this.internalTeammembers = internalTeammembers;
	}

	public List<ExternalSubjectDataObj> getClientTeammembers() {
		return clientTeammembers;
	}

	public void setClientTeammembers(List<ExternalSubjectDataObj> clientTeammembers) {
		this.clientTeammembers = clientTeammembers;
	}

	public List<ExternalSubjectDataObj> getRemoveInternalTeammembers() {
		return removeInternalTeammembers;
	}

	public void setRemoveInternalTeammembers(
			List<ExternalSubjectDataObj> removeInternalTeammembers) {
		this.removeInternalTeammembers = removeInternalTeammembers;
	}

	public List<ExternalSubjectDataObj> getRemoveClientTeammembers() {
		return removeClientTeammembers;
	}

	public void setRemoveClientTeammembers(List<ExternalSubjectDataObj> removeClientTeammembers) {
		this.removeClientTeammembers = removeClientTeammembers;
	}

	public boolean isInternalTeamTabDisabled() {
		return internalTeamTabDisabled;
	}

	public void setInternalTeamTabDisabled(boolean internalTeamTabDisabled) {
		this.internalTeamTabDisabled = internalTeamTabDisabled;
	}

	public boolean isClientTeamTabDisabled() {
		return clientTeamTabDisabled;
	}

	public void setClientTeamTabDisabled(boolean clientTeamTabDisabled) {
		this.clientTeamTabDisabled = clientTeamTabDisabled;
	}

}
