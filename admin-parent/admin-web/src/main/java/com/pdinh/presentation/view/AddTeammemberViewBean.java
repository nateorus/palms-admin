package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.Role;

@ManagedBean
@ViewScoped
public class AddTeammemberViewBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<ExternalSubjectDataObj> teammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> selectedTeammembers;
	private List<ExternalSubjectDataObj> filteredTeammembers;
	
	//TODO: AV: Needs to be a List of DTO objects and not JPA
	private List<Role> roles = new ArrayList<Role>();
	private Integer roleId;
	private Role selectedRole;
	
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public List<ExternalSubjectDataObj> getTeammembers() {
		return teammembers;
	}
	public void setTeammembers(List<ExternalSubjectDataObj> teammembers) {
		this.teammembers = teammembers;
	}
	public List<ExternalSubjectDataObj> getSelectedTeammembers() {
		return selectedTeammembers;
	}
	public void setSelectedTeammembers(
			List<ExternalSubjectDataObj> selectedTeammembers) {
		this.selectedTeammembers = selectedTeammembers;
	}
	public Role getSelectedRole() {
		return selectedRole;
	}
	public void setSelectedRole(Role selectedRole) {
		this.selectedRole = selectedRole;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public List<ExternalSubjectDataObj> getFilteredTeammembers() {
		return filteredTeammembers;
	}
	public void setFilteredTeammembers(List<ExternalSubjectDataObj> filteredTeammembers) {
		this.filteredTeammembers = filteredTeammembers;
	}
}
