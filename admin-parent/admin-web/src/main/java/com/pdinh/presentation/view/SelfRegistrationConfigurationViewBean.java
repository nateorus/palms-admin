package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.data.jaxb.admin.SelfRegConfigRepresentation;

@ManagedBean(name = "selfRegConfigViewBean")
@ViewScoped
public class SelfRegistrationConfigurationViewBean implements Serializable {

	private static final long serialVersionUID = 5449050803608164170L;

	private int id;
	private boolean enabled;
	private boolean unlimited;
	private int limit;
	private String domain;
	private SelfRegConfigRepresentation configuration;
	private List<String> removedDomains = new ArrayList<String>();
	private List<String> addedDomains = new ArrayList<String>();
	private List<String> domains = new ArrayList<String>();

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public SelfRegConfigRepresentation getConfiguration() {
		return configuration;
	}

	public void setConfiguration(SelfRegConfigRepresentation configuration) {
		this.configuration = configuration;
	}

	public List<String> getRemovedDomains() {
		return removedDomains;
	}

	public void setRemovedDomains(List<String> removedDomains) {
		this.removedDomains = removedDomains;
	}

	public List<String> getAddedDomains() {
		return addedDomains;
	}

	public void setAddedDomains(List<String> addedDomains) {
		this.addedDomains = addedDomains;
	}

	public List<String> getDomains() {
		return domains;
	}

	public void setDomains(List<String> domains) {
		this.domains = domains;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public boolean isUnlimited() {
		return unlimited;
	}

	public void setUnlimited(boolean unlimited) {
		this.unlimited = unlimited;
	}

}
