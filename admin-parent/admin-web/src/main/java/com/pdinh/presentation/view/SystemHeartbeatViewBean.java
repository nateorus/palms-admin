package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.SystemStatusObj;

@ManagedBean
@ViewScoped
public class SystemHeartbeatViewBean implements Serializable {

	private static final long serialVersionUID = 466172024074749009L;
	
	private final List<SystemStatusObj> systemStatus = new ArrayList<SystemStatusObj>();
	private boolean systemsStatusUknown = true;

	public void initialize() {
		getSystemStatus().clear();
	}

	public boolean isAllHealthy() {
		if (isBulkUploadHealthy() && isReportCenterHealthy()) {
			return true;
		}
		return false;
	}

	public boolean isBulkUploadHealthy() {
		for(SystemStatusObj system : systemStatus) {
			if (system.getApplication().equals(SystemStatusObj.APPLICATION_BULK_UPLOAD)) {
				if(!system.isRunning()) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isReportCenterHealthy() {
		for (SystemStatusObj system : systemStatus) {
			if (system.getApplication().equals(SystemStatusObj.APPLICATION_REPORT_CENTER)) {
				if (!system.isRunning()) {
					return false;
				}
			}
		}
		return true;
	}

	public List<SystemStatusObj> getSystemStatus() {
		return systemStatus;
	}

	public boolean isSystemsStatusUknown() {
		return systemsStatusUknown;
	}

	public void setSystemsStatusUknown(boolean systemsStatusUknown) {
		this.systemsStatusUknown = systemsStatusUknown;
	}

}
