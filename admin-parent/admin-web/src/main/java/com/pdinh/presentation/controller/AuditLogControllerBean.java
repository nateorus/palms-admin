package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.AuditEventDao;
import com.pdinh.auth.persistence.entity.AuditEvent;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.AuditLogViewBean;

@ManagedBean(name = "auditLogControllerBean")
@ViewScoped
public class AuditLogControllerBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AuditLogControllerBean.class);
	public static final int AUDIT_TYPE_PPT = 1;
	public static final int AUDIT_TYPE_INTERNAL = 2;
	public static final int AUDIT_TYPE_SEARCH = 3;
	
	@ManagedProperty(name="auditLogViewBean", value="#{auditLogViewBean}")
	private AuditLogViewBean auditLogViewBean;
	
	@EJB
	private AuditEventDao auditEventDao;
	
	public void initAuditLog(int auditType, String searchString, List<String> loggerNames){
		List <AuditEvent> events = new ArrayList<AuditEvent>();
		switch (auditType){
			case AUDIT_TYPE_PPT:
				log.debug("Running participant Log search");
				events = auditEventDao.findByArg2AndLoggerName(searchString, loggerNames.get(0));
				auditLogViewBean.setAuditEvents(events);
				break;
			case AUDIT_TYPE_INTERNAL:
				log.debug("Running admin Log search");
				events = auditEventDao.findByArg0AndLoggerName(searchString, loggerNames);
				auditLogViewBean.setAuditEvents(events);
				break;
			case AUDIT_TYPE_SEARCH:
				log.debug("Running audit log Log search");
				events = auditEventDao.findByAnyStringInManyLoggers(searchString, loggerNames);
				auditLogViewBean.setAuditEvents(events);
				break;
		}
		List<String> searchStrings = new ArrayList<String>();
		searchStrings.add(searchString);
		auditLogViewBean.setSearchStrings(searchStrings);
		
	}
	
	public void initMultiLog(List<String> searchStrings, List<String> loggerNames){
		List <AuditEvent> events = new ArrayList<AuditEvent>();
		for (String s : searchStrings){
			events.addAll(auditEventDao.findByAnyStringInManyLoggers(s, loggerNames));
		}
		auditLogViewBean.setAuditEvents(events);
		auditLogViewBean.setSearchStrings(searchStrings);
	}
	
	public void initLog2Strings(String anyString1, String anyString2, List<String> loggerNames){
		List<AuditEvent> events = new ArrayList<AuditEvent>();
		events.addAll(auditEventDao.findBy2StringsInManyLoggers(anyString1, anyString2, loggerNames));
		List<String> searchTerms = new ArrayList<String>();
		searchTerms.add(anyString1);
		searchTerms.add(anyString2);
		auditLogViewBean.setAuditEvents(events);
		auditLogViewBean.setSearchStrings(searchTerms);
	}

	public AuditLogViewBean getAuditLogViewBean() {
		return auditLogViewBean;
	}

	public void setAuditLogViewBean(AuditLogViewBean auditLogViewBean) {
		this.auditLogViewBean = auditLogViewBean;
	}


}
