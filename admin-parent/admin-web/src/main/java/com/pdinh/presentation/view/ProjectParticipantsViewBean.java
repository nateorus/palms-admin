package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean(name="projectParticipantsViewBean")
@ViewScoped
public class ProjectParticipantsViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<User> clientUsers;
	private List<ParticipantObj> selections;
	private List<ParticipantObj> participants;
	private List<ParticipantObj> selectedUsers;
	private List<ParticipantObj> filteredUsers;

	private Boolean addToSelectionDisabled = true;
	private Boolean addExistingParticipantsDisabled = true;
	private boolean isUpdatePDAError = false;

	public void setClientUsers(List<User> clientUsers) {
		this.clientUsers = clientUsers;
	}
	public List<User> getClientUsers() {
		return clientUsers;
	}
	public void setSelections(List<ParticipantObj> selections) {
		this.selections = selections;
	}
	public List<ParticipantObj> getSelections() {
		return selections;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

    public void setAddExistingParticipantsDisabled(
            Boolean addExistingParticipantsDisabled) {
        this.addExistingParticipantsDisabled = addExistingParticipantsDisabled;
    }

    public Boolean getAddExistingParticipantsDisabled() {
        return addExistingParticipantsDisabled;
    }

    public void setAddToSelectionDisabled(Boolean addToSelectionDisabled) {
        this.addToSelectionDisabled = addToSelectionDisabled;
    }

    public Boolean getAddToSelectionDisabled() {
        return addToSelectionDisabled;
    }
	public List<ParticipantObj> getSelectedUsers() {
		return selectedUsers;
	}
	public void setSelectedUsers(List<ParticipantObj> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}
	public List<ParticipantObj> getFilteredUsers() {
		return filteredUsers;
	}
	public void setFilteredUsers(List<ParticipantObj> filteredUsers) {
		this.filteredUsers = filteredUsers;
	}
	public boolean isUpdatePDAError() {
		return isUpdatePDAError;
	}
	public void setUpdatePDAError(boolean isUpdatePDAError) {
		this.isUpdatePDAError = isUpdatePDAError;
	}
}
