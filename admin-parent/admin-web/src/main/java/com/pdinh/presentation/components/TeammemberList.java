package com.pdinh.presentation.components;

import java.io.Serializable;
import java.util.List;

import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FacesComponent(value="teammemberListComponent")
@SuppressWarnings (value="unchecked")
public class TeammemberList extends UINamingContainer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(TeammemberList.class);

	public TeammemberList () {}
		
    public void remove(Object teammember) {  
 
        List<Object> addTeammembers = (List<Object>) getAttributes().get("addList");
        List<Object> removeTeammembers = (List<Object>) getAttributes().get("removeList");
        
        if(!removeTeammembers.contains(teammember)) {
        	removeTeammembers.add(teammember);
        	log.debug(">>>>>>>>>>>>>>>>>>>>>> adding teammember to remove list: {}", teammember);
        } else {
        	log.debug(">>>>>>>>>>>>>>>>>>>>>> already in the remove list: {}", teammember);
        }
        
        addTeammembers.remove(teammember);
        
        log.debug(">>>>>>>>>>>>>>>>>>>>>> REMOVING TEAMMEMBER: {}", teammember);
        
   }
   
    public String scrollOrNotToScroll() {
    	Integer numRows = (Integer) getAttributes().get("rowsBeforeScroll");
    	List<Object> addTeammembers = (List<Object>) getAttributes().get("addList");
    	String css = "";
    	
    	if(addTeammembers.size() > numRows) {
    		css = "height:" + (numRows * 34) + "px";
    	}
    	
    	return css;
    	
    }
    
}
