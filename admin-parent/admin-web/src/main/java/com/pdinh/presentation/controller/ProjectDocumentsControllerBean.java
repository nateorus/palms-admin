package com.pdinh.presentation.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectDocumentTypeDao;
import com.pdinh.enterprise.managedbean.form.LoginFormBean;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.ms.entity.FileAttachment;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectDocument;
import com.pdinh.persistence.ms.entity.ProjectDocumentType;
import com.pdinh.presentation.domain.FileAttachmentObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.ProjectDocumentObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ProjectDocumentsViewBean;

@ManagedBean(name = "projectDocumentsControllerBean")
@ViewScoped
public class ProjectDocumentsControllerBean extends DocumentsControllerBean {

	private static final long serialVersionUID = 1L;

	@EJB
	private ProjectDocumentTypeDao projectDocumentTypeDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "projectDocumentsViewBean", value = "#{projectDocumentsViewBean}")
	protected ProjectDocumentsViewBean projectDocumentsViewBean;

	@ManagedProperty(name = "loginFormBean", value = "#{loginFormBean}")
	private LoginFormBean loginFormBean;

	@Resource(name = "application/config_properties")
	private Properties appConfigProperties;

	@PostConstruct
	public void postConstruct() {
		super.setDocumentsViewBean(projectDocumentsViewBean);
		super.setFileAttachmentService(fileAttachmentService);
		super.setAppConfigProperties(appConfigProperties);
		super.setLoginFormBean(loginFormBean);
	}

	public void initForEdit() {
		populateDocuments();
	}

	public void addDocument() {
		System.out.println("IN ProjectDocumentsControllerBean.addDocument()");

		ProjectDocumentObj document = new ProjectDocumentObj();
		document.setDescription(projectDocumentsViewBean.getNewDocumentDescription());
		document.setShortDescription(projectDocumentsViewBean.getNewDocumentDescription());
		document.setType(projectDocumentsViewBean.getNewDocumentType());
		document.setTypeDisplay(projectDocumentsViewBean.getDocumentNameByType(document.getType()));
		document.setFileAttachment(projectDocumentsViewBean.getLastDocumentFileUploaded());
		document.setIsIntegrationGrid(projectDocumentsViewBean.getNewDocumentIsIntegrationGrid());

		// write this file to disk, will need to clean up any files that don't
		// actually get saved
		FileAttachmentObj fileAttachmentObj = projectDocumentsViewBean.getLastDocumentFileUploaded();
		try {
			fileAttachmentService.write(fileAttachmentObj.getPath() + "/" + fileAttachmentObj.getName(),
					fileAttachmentObj.getNewFileInputStream());
			projectDocumentsViewBean.getDocuments().add(document);
		} catch (IOException e) {
			e.printStackTrace();
		}

		projectDocumentsViewBean.clearNewDocumentData();
	}

	// Populate documents
	private void populateDocuments() {
		System.out.println("In ProjectDocumentsControllerBean populateDocuments()");
		projectDocumentsViewBean.clear();

		List<ListItemObj> documentTypes = new ArrayList<ListItemObj>();
		/**
		 * List assembled from embedded screen shot in User Stories v0.8
		 * http://mspwebdev6
		 * /aande/abydmigration/Shared%20Documents/Design%20Docs
		 * /AbyD%20migration_user%20stories_v0.8.doc Unsure whether this will
		 * live in here or in a table. Need similar item for projects too.
		 */

		List<ProjectDocumentType> projectDocumentTypes = projectDocumentTypeDao.findAll();
		for (int i = 0; i < projectDocumentTypes.size(); i++) {
			ProjectDocumentType pt = projectDocumentTypes.get(i);
			documentTypes.add(new ListItemObj(pt.getProjectDocumentTypeId(), pt.getName()));
		}

		projectDocumentsViewBean.setDocumentTypes(documentTypes);

		List documents = findAllDocumentsByProject();
		projectDocumentsViewBean.setDocuments(documents);
	}

	public List<ProjectDocumentObj> findAllDocumentsByProject() {
		System.out.println("IN ProjectDocumentsControllerBean.findAllDocumentsByProject()");
		Project project = sessionBean.getProject();
		List<ProjectDocumentObj> documents = new ArrayList<ProjectDocumentObj>();

		List<ProjectDocument> projectDocuments = project.getProjectDocuments();
		for (ProjectDocument projectDocument : projectDocuments) {

			FileAttachment fa = projectDocument.getFileAttachment();
			FileAttachmentObj fileAttachmentObj = new FileAttachmentObj(fa.getId(), fa.getName(), fa.getPath(),
					fa.getDateUploaded(), fa.getExtAdminId());

			ProjectDocumentObj doc = new ProjectDocumentObj(projectDocument.getDocumentId(),
					projectDocument.getDescription(), projectDocument.getIncludeInIntegrationGrid().booleanValue(),
					projectDocument.getProjectDocumentType().getProjectDocumentTypeId(), projectDocument
							.getProjectDocumentType().getName(), fileAttachmentObj, false);
			doc.setShortDescription(projectDocument.getDescription());
			// use path for now, ideally would just use id, but IDs are not
			// getting set to objects on save
			fileAttachmentObj.setUrl(getEncodedDocumentUrl(fileAttachmentObj.getPath() + "/"
					+ fileAttachmentObj.getName()));
			documents.add(doc);
		}

		return documents;
	}

	public ProjectDocumentTypeDao getProjectDocumentTypeDao() {
		return projectDocumentTypeDao;
	}

	public void setProjectDocumentTypeDao(ProjectDocumentTypeDao projectDocumentTypeDao) {
		this.projectDocumentTypeDao = projectDocumentTypeDao;
	}

	// need to extract this to helper method/class so it can be called to copy
	// view state into model objects
	public void save() {

		Project project = sessionBean.getProject();

		// delete docs
		List<ProjectDocument> docsToDelete = new ArrayList<ProjectDocument>();
		for (ProjectDocument pd : project.getProjectDocuments()) {
			boolean found = false;
			for (Object docObj : projectDocumentsViewBean.getDocuments()) {
				ProjectDocumentObj pdo = (ProjectDocumentObj) docObj;
				if (pd.getDocumentId() == pdo.getDocumentId()) {
					found = true;
					pd.setIncludeInIntegrationGrid(pdo.getIsIntegrationGrid());
				}
			}
			if (found == false) {
				docsToDelete.add(pd);
			}
		}

		// delete files from disk and remove from user
		for (ProjectDocument pd : docsToDelete) {
			try {
				fileAttachmentService.delete(pd.getFileAttachment());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		project.getProjectDocuments().removeAll(docsToDelete);

		// update / create
		for (Object docObj : projectDocumentsViewBean.getDocuments()) {
			ProjectDocumentObj pdo = (ProjectDocumentObj) docObj;
			for (ProjectDocument pd : project.getProjectDocuments()) {
				if (pd.getDocumentId() == pdo.getDocumentId()) {
					pd.setIncludeInIntegrationGrid(pdo.getIsIntegrationGrid());
				}
			}

			if (pdo.isNewDocument()) {
				// new docs
				ProjectDocument pd = new ProjectDocument();
				pd.setDescription(pdo.getDescription());
				pd.setProjectDocumentType(projectDocumentTypeDao.findById(pdo.getType()));
				pd.setIncludeInIntegrationGrid(pdo.getIsIntegrationGrid());
				pd.setProject(project);

				FileAttachmentObj fao = pdo.getFileAttachment();
				FileAttachment fileAttachment = new FileAttachment();
				fileAttachment.setDateUploaded(fao.getDateUploaded());
				fileAttachment.setName(fao.getName());
				fileAttachment.setPath(fao.getPath());
				fileAttachment.setExtAdminId(fao.getExtAdminId());

				pd.setFileAttachment(fileAttachment);
				project.getProjectDocuments().add(pd);
			}
		}
		projectDao.update(project);
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public ProjectDocumentsViewBean getProjectDocumentsViewBean() {
		return projectDocumentsViewBean;
	}

	public void setProjectDocumentsViewBean(ProjectDocumentsViewBean projectDocumentsViewBean) {
		this.projectDocumentsViewBean = projectDocumentsViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	@Override
	public FileAttachmentServiceLocal getFileAttachmentService() {
		return fileAttachmentService;
	}

	@Override
	public void setFileAttachmentService(FileAttachmentServiceLocal fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	@Override
	public Properties getAppConfigProperties() {
		return appConfigProperties;
	}

	@Override
	public void setAppConfigProperties(Properties appConfigProperties) {
		this.appConfigProperties = appConfigProperties;
	}

	@Override
	public LoginFormBean getLoginFormBean() {
		return loginFormBean;
	}

	@Override
	public void setLoginFormBean(LoginFormBean loginFormBean) {
		this.loginFormBean = loginFormBean;
	}
}
