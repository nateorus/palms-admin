package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.kf.uffda.vo.CustomFieldsVo;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.presentation.domain.ConsentHistoryObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.NoteObj;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;

@ManagedBean(name = "participantInfoViewBean")
@ViewScoped
public class ParticipantInfoViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	// project type controls disabled state of setting tab
	private Boolean settingsTabRendered;

	private String username;
	private String password;
	private Boolean showCopyString = false;
	private String copyString;

	private String firstName;
	private String lastName;
	private String email;
	private Integer userType;
	private String languageId;

	private String loginId;

	private String optional1;
	private String optional2;
	private String optional3;
	private String supervisorName;
	private String supervisorEmail;

	private boolean coachFlag;

	// validation variables
	private String firstNameMsg;
	private String lastNameMsg;
	private String emailMsg;
	private String loginIdMsg;
	private String officeLocationMsg;

	private List<NoteObj> notes;
	private NoteObj newNote;
	private String newNoteText;

	private String languageCHQ = null;
	private String languageFEX = null;

	private List<LanguageObj> languages;

	// lists for settings tab
	private List<LanguageObj> languagesCHQ;
	private List<LanguageObj> languagesFEX;
	// control sensitivity of lists in settings tab
	private Boolean languageCHQRendered = false;
	private Boolean languageFEXRendered = false;

	private List<ParticipantObj> participants;
	private ParticipantObj selectedParticipant;

	private List<ProjectObj> projects;

	private Boolean editParticipantDisabled = true;
	private Boolean delParticipantDisabled = true;
	private Boolean resetPasswordRendered = false;
	private Boolean generateUsernameRendered = false;
	private Boolean coachFlagRendered = false;

	//private List<OfficeLocationObj> officeLocations;
	//private Integer selectedOfficeLocationId;
	// Dialogue info
	private Integer activeTabIndex = 0;
	private List<Course> courses;

	// id of the course to post xml for
	private String xmlPostCourseId;
	private String xmlPostTrackingToken;
	private String xmlPostXmlContent;
	private String xmlPostOutput;

	private Boolean xmlPostFormRendered = false;

	// label for command button at bottom of edit user dialog
	private String bottomButtonLabel;

	// boolean to determine which command button (add or save) is
	// below the participant info dialog
	private Boolean isEditAction = false;

	// Disabled flag properties
	private Boolean firstNameDisabled = false;
	private Boolean lastNameDisabled = false;
	private Boolean emailDisabled = false;
	private Boolean usernameDisabled = false;
	private Boolean passwordDisabled = false;
	private boolean saveNoteDisabled = true;
	private boolean saveButtonDisabled = true;

	private String businessPhone;
	private boolean businessPhoneDisabled;
	private boolean user;

	private boolean isPDAProject = false;

	private CustomFieldsVo userCustomFields = new CustomFieldsVo();
	private CustomFieldsVo participantCustomFields = new CustomFieldsVo();

	private final CustomViewBean userView = new CustomViewBean();
	private final CustomViewBean participantView = new CustomViewBean();
	private List<ConsentHistoryObj> consentHistory = new ArrayList<ConsentHistoryObj>();

	public void clearInfo() {
		setFirstName("");
		setLastName("");
		setEmail("");
		setUsername("");
		setLanguageId("en");
		setCoachFlag(false);
		userCustomFields.clearAllValues();
		participantCustomFields.clearAllValues();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getOptional1() {
		return optional1;
	}

	public void setOptional1(String optional1) {
		this.optional1 = optional1;
	}

	public String getOptional2() {
		return optional2;
	}

	public void setOptional2(String optional2) {
		this.optional2 = optional2;
	}

	public String getOptional3() {
		return optional3;
	}

	public void setOptional3(String optional3) {
		this.optional3 = optional3;
	}

	public String getSupervisorName() {
		return supervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	public String getSupervisorEmail() {
		return supervisorEmail;
	}

	public void setSupervisorEmail(String supervisorEmail) {
		this.supervisorEmail = supervisorEmail;
	}

	public List<LanguageObj> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageObj> languages) {
		this.languages = languages;
	}

	public NoteObj getNewNote() {
		return newNote;
	}

	public void setNewNote(NoteObj newNote) {
		this.newNote = newNote;
	}

	public List<NoteObj> getNotes() {
		return notes;
	}

	public void setNotes(List<NoteObj> notes) {
		this.notes = notes;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setSelectedParticipant(ParticipantObj selectedParticipant) {
		this.selectedParticipant = selectedParticipant;
	}

	public ParticipantObj getSelectedParticipant() {
		return selectedParticipant;
	}

	public void setDelParticipantDisabled(Boolean delParticipantDisabled) {
		this.delParticipantDisabled = delParticipantDisabled;
	}

	public Boolean getDelParticipantDisabled() {
		return delParticipantDisabled;
	}

	public void setEditParticipantDisabled(Boolean editParticipantDisabled) {
		System.out.println("setEditParticipantDisabled " + editParticipantDisabled);
		this.editParticipantDisabled = editParticipantDisabled;
	}

	public Boolean getEditParticipantDisabled() {
		return editParticipantDisabled;
	}

	public void setProjects(List<ProjectObj> projects) {
		this.projects = projects;
	}

	public List<ProjectObj> getProjects() {
		return projects;
	}

	public void setNewNoteText(String newNoteText) {
		this.newNoteText = newNoteText;
	}

	public String getNewNoteText() {
		return newNoteText;
	}

	// public void setNewDocumentFilename(String newDocumentFilename) {
	// this.newDocumentFilename = newDocumentFilename;
	// }
	//
	// public String getNewDocumentFilename() {
	// return newDocumentFilename;
	// }

	public void setLanguagesCHQ(List<LanguageObj> languagesCHQ) {
		this.languagesCHQ = languagesCHQ;
	}

	public List<LanguageObj> getLanguagesCHQ() {
		return languagesCHQ;
	}

	public void setLanguagesFEX(List<LanguageObj> languagesFEX) {
		this.languagesFEX = languagesFEX;
	}

	public List<LanguageObj> getLanguagesFEX() {
		return languagesFEX;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setLanguageCHQ(String languageCHQ) {
		this.languageCHQ = languageCHQ;
	}

	public String getLanguageCHQ() {
		return languageCHQ;
	}

	public void setLanguageFEX(String languageFEX) {
		this.languageFEX = languageFEX;
	}

	public String getLanguageFEX() {
		return languageFEX;
	}

	public void setSettingsTabRendered(Boolean settingsTabRendered) {
		this.settingsTabRendered = settingsTabRendered;
	}

	public Boolean getSettingsTabRendered() {
		return settingsTabRendered;
	}

	public Boolean getLanguageCHQRendered() {
		return languageCHQRendered;
	}

	public void setLanguageCHQRendered(Boolean languageCHQRendered) {
		this.languageCHQRendered = languageCHQRendered;
	}

	public Boolean getLanguageFEXRendered() {
		return languageFEXRendered;
	}

	public void setLanguageFEXRendered(Boolean languageFEXRendered) {
		this.languageFEXRendered = languageFEXRendered;
	}

	public void setIsEditAction(Boolean isEditAction) {
		this.isEditAction = isEditAction;
	}

	public Boolean getIsEditAction() {
		return isEditAction;
	}

	public void setBottomButtonLabel(String bottomButtonLabel) {
		this.bottomButtonLabel = bottomButtonLabel;
	}

	public String getBottomButtonLabel() {
		return bottomButtonLabel;
	}

	public void setFirstNameMsg(String firstNameMsg) {
		this.firstNameMsg = firstNameMsg;
	}

	public String getFirstNameMsg() {
		return firstNameMsg;
	}

	public void setLastNameMsg(String lastNameMsg) {
		this.lastNameMsg = lastNameMsg;
	}

	public String getLastNameMsg() {
		return lastNameMsg;
	}

	public void setEmailMsg(String emailMsg) {
		this.emailMsg = emailMsg;
	}

	public String getLoginIdMsg() {
		return loginIdMsg;
	}

	public void setLoginIdMsg(String loginIdMsg) {
		this.loginIdMsg = loginIdMsg;
	}

	public String getEmailMsg() {
		return emailMsg;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setResetPasswordRendered(Boolean resetPasswordRendered) {
		this.resetPasswordRendered = resetPasswordRendered;
	}

	public Boolean getResetPasswordRendered() {
		return resetPasswordRendered;
	}

	public Boolean getGenerateUsernameRendered() {
		return generateUsernameRendered;
	}

	public void setGenerateUsernameRendered(Boolean generateUsernameRendered) {
		this.generateUsernameRendered = generateUsernameRendered;
	}

	public String getCopyString() {
		return copyString;
	}

	public void setCopyString(String copyString) {
		this.copyString = copyString;
	}

	public Boolean getShowCopyString() {
		return showCopyString;
	}

	public void setShowCopyString(Boolean showCopyString) {
		this.showCopyString = showCopyString;
	}

	public Integer getActiveTabIndex() {
		return activeTabIndex;
	}

	public void setActiveTabIndex(Integer activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}

	public String getXmlPostCourseId() {
		return xmlPostCourseId;
	}

	public void setXmlPostCourseId(String xmlPostCourseId) {
		this.xmlPostCourseId = xmlPostCourseId;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public String getXmlPostTrackingToken() {
		return xmlPostTrackingToken;
	}

	public void setXmlPostTrackingToken(String xmlPostTrackingToken) {
		this.xmlPostTrackingToken = xmlPostTrackingToken;
	}

	public String getXmlPostXmlContent() {
		return xmlPostXmlContent;
	}

	public void setXmlPostXmlContent(String xmlPostXmlContent) {
		this.xmlPostXmlContent = xmlPostXmlContent;
	}

	public String getXmlPostOutput() {
		return xmlPostOutput;
	}

	public void setXmlPostOutput(String xmlPostOutput) {
		this.xmlPostOutput = xmlPostOutput;
	}

	public Boolean getXmlPostFormRendered() {
		return xmlPostFormRendered;
	}

	public void setXmlPostFormRendered(Boolean xmlPostFormRendered) {
		this.xmlPostFormRendered = xmlPostFormRendered;
	}

	public Boolean getCoachFlagRendered() {
		return coachFlagRendered;
	}

	public void setCoachFlagRendered(Boolean coachFlagRendered) {
		this.coachFlagRendered = coachFlagRendered;
	}

	public boolean getCoachFlag() {
		return coachFlag;
	}

	public void setCoachFlag(boolean coachFlag) {
		this.coachFlag = coachFlag;
	}
/*
	public List<OfficeLocationObj> getOfficeLocations() {
		return officeLocations;
	}

	public void setOfficeLocations(List<OfficeLocationObj> officeLocation) {
		this.officeLocations = officeLocation;
	}

	public Integer getSelectedOfficeLocationId() {
		return selectedOfficeLocationId;
	}

	public void setSelectedOfficeLocationId(Integer officeLocationId) {
		this.selectedOfficeLocationId = officeLocationId;
	}
*/
	public String getOfficeLocationMsg() {
		return officeLocationMsg;
	}

	public void setOfficeLocationMsg(String officeLocationMsg) {
		this.officeLocationMsg = officeLocationMsg;
	}

	public Boolean getFirstNameDisabled() {
		return firstNameDisabled;
	}

	public void setFirstNameDisabled(Boolean firstNameDisabled) {
		this.firstNameDisabled = firstNameDisabled;
	}

	public Boolean getLastNameDisabled() {
		return lastNameDisabled;
	}

	public void setLastNameDisabled(Boolean lastNameDisabled) {
		this.lastNameDisabled = lastNameDisabled;
	}

	public Boolean getEmailDisabled() {
		return emailDisabled;
	}

	public void setEmailDisabled(Boolean emailDisabled) {
		this.emailDisabled = emailDisabled;
	}

	public Boolean getUsernameDisabled() {
		return usernameDisabled;
	}

	public void setUsernameDisabled(Boolean usernameDisabled) {
		this.usernameDisabled = usernameDisabled;
	}

	public Boolean getPasswordDisabled() {
		return passwordDisabled;
	}

	public void setPasswordDisabled(Boolean passwordDisabled) {
		this.passwordDisabled = passwordDisabled;
	}

	public boolean isSaveNoteDisabled() {
		return saveNoteDisabled;
	}

	public void setSaveNoteDisabled(boolean saveNoteDisabled) {
		this.saveNoteDisabled = saveNoteDisabled;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public boolean isBusinessPhoneDisabled() {
		return businessPhoneDisabled;
	}

	public void setBusinessPhoneDisabled(boolean businessPhoneDisabled) {
		this.businessPhoneDisabled = businessPhoneDisabled;
	}

	public boolean isUser() {
		return user;
	}

	public void setUser(boolean user) {
		this.user = user;
	}

	public boolean isSaveButtonDisabled() {
		return saveButtonDisabled;
	}

	public void setSaveButtonDisabled(boolean saveButtonDisabled) {
		this.saveButtonDisabled = saveButtonDisabled;
	}

	public boolean isPDAProject() {
		return isPDAProject;
	}

	public void setPDAProject(boolean isPDAProject) {
		this.isPDAProject = isPDAProject;
	}

	public CustomFieldsVo getUserCustomFields() {
		return userCustomFields;
	}

	public void setUserCustomFields(CustomFieldsVo userCustomFields) {
		this.userCustomFields = userCustomFields;
	}

	public void sortUserCustomFields() {
		userCustomFields.sortFields();
	}

	public boolean hasUserCustomFields() {
		if (userCustomFields != null) {
			return userCustomFields.hasFields();
		}
		return false;
	}

	public boolean isShowUserCustomFields() {
		return hasUserCustomFields();
	}

	public void clearUserCustomFields() {
		userCustomFields.clearFields();
	}

	public void clearUserCustomFieldValues() {
		userCustomFields.clearAllValues();
	}

	public CustomFieldsVo getParticipantCustomFields() {
		return participantCustomFields;
	}

	public void setParticipantCustomFields(CustomFieldsVo participantCustomFields) {
		this.participantCustomFields = participantCustomFields;
	}

	public void sortParticipantCustomFields() {
		participantCustomFields.sortFields();
	}

	public boolean hasParticipantCustomFields() {
		if (participantCustomFields != null) {
			return participantCustomFields.hasFields();
		}
		return false;
	}

	public boolean isShowParticipantCustomFields() {
		return hasParticipantCustomFields();
	}

	public void clearParticipantCustomFields() {
		participantCustomFields.clearFields();
	}

	public void clearParticipantCustomFieldValues() {
		participantCustomFields.clearAllValues();
	}

	public CustomViewBean getUserView() {
		return userView;
	}

	public CustomViewBean getParticipantView() {
		return participantView;
	}

	public List<ConsentHistoryObj> getConsentHistory() {
		return consentHistory;
	}

	public void setConsentHistory(List<ConsentHistoryObj> consentHistory) {
		this.consentHistory = consentHistory;
	}

}
