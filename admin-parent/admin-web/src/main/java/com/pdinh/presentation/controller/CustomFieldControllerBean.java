package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.data.dao.CustomFieldDao;
import com.kf.uffda.data.dao.CustomFieldMappingDao;
import com.kf.uffda.dto.CustomFieldChangeDto;
import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.CustomFieldListValueDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.dto.FieldNameDto;
import com.kf.uffda.persistence.CustomFieldMapping;
import com.kf.uffda.persistence.FieldEntityType;
import com.kf.uffda.service.UffdaService;
import com.kf.uffda.service.UffdaServiceImpl;
import com.kf.uffda.view.FieldViewBean;
import com.kf.uffda.vo.FieldAliasVo;
import com.kf.uffda.vo.FieldTransformationVo;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.managedbean.LanguageBean;
import com.pdinh.presentation.view.CustomFieldViewBean;
import com.pdinh.presentation.view.ProjectFieldsViewBean;

@ManagedBean(name = "customFieldControllerBean")
@ViewScoped
public class CustomFieldControllerBean extends FieldControllerBean implements Serializable {

	private static final long serialVersionUID = -6384497547356313146L;

	private static final Logger log = LoggerFactory.getLogger(CustomFieldControllerBean.class);

	@EJB
	CustomFieldDao customFieldDao;

	@EJB
	CustomFieldMappingDao customFieldMappingDao;

	@EJB
	UffdaService uffdaService;

	@ManagedProperty(name = "languageBean", value = "#{languageBean}")
	private LanguageBean languageBean;

	@ManagedProperty(name = "projectFieldsViewBean", value = "#{projectFieldsViewBean}")
	private ProjectFieldsViewBean projectFieldsViewBean;

	@ManagedProperty(name = "customFieldViewBean", value = "#{customFieldViewBean}")
	private CustomFieldViewBean viewBean;

	public CustomFieldControllerBean() {
		super("frmFieldDictionary");
	}

	@Override
	public void initView(ComponentSystemEvent event) {
		if (isFirstTime()) {
			super.initView(event);

			if (!viewBean.hasLanguages()) {
				viewBean.addLanguages(languageBean.getLanguageListForInternal());
			}
		}
	}

	public void handleSelectLanguage() {
		if (viewBean.getField() != null) {
			viewBean.getField().setSelectedLanguageCode(viewBean.getLanguageCode());
		}
	}

	public void handleDeleteField(ActionEvent event) {
		fieldDictionaryViewBean.setShowCustomField(false);
		fieldDictionaryViewBean.setClientSideValueChanged(true);

		FieldDto field = viewBean.getField();
		if (field != null) {
			EntityAuditLogger.auditLog.info("{} performed a {} for Custom Field [{}] for client {}.", new Object[] {
					getUsername(), EntityAuditLogger.OP_DELETE_FIELD, field.getName(), getCompanyName() });

			uffdaService.deleteCustomField(sessionBean.getSubject_id(), field);
			fieldDictionaryViewBean.getFields().remove(field);
			fieldDictionaryViewBean.sortFields();
			viewBean.init();
		}
	}

	public void handleSaveField(ActionEvent event) {
		boolean error = false;

		CustomFieldDto field = viewBean.getField();
		if (field != null) {

			if (StringUtils.isBlank(field.getName())) {
				LabelUtils.addErrorMessage("fieldLabel", "Internal Field Label - cannot be blank!");
				error = true;
			}

			if (field.getCode() == null || field.getCode().trim().isEmpty()) {
				LabelUtils.addErrorMessage("code", "Import Template Column Name - cannot be blank!");
				error = true;
			}

			if (field.isNew() && !viewBean.isEditOnly()) {
				int existingField = uffdaService.checkForExistingField(sessionBean.getCompany().getCompanyId(),
						field.getCode(), field.getName(), 0);

				if (existingField == UffdaServiceImpl.EXISTING_CUSTOM_FIELD_NAME) {
					LabelUtils.addErrorMessage("fieldLabel", "The Internal Field Label \"" + field.getName()
							+ "\" is already used in the Field Dictionary for a Custom Field!");
					error = true;

				} else if (existingField == UffdaServiceImpl.EXISTING_CUSTOM_FIELD_CODE) {
					LabelUtils.addErrorMessage("code", "The Import Template Column Name \"" + field.getCode()
							+ "\" is already used in the Field Dictionary for a Custom Field!");
					error = true;

				} else if (existingField == UffdaServiceImpl.EXISTING_ALIAS_NAME) {
					LabelUtils.addErrorMessage("fieldLabel", "The Internal Field Label \"" + field.getName()
							+ "\" is already used in the Field Dictionary for a Field Alias!");
					error = true;

				} else if (existingField == UffdaServiceImpl.EXISTING_ALIAS_CODE) {
					LabelUtils.addErrorMessage("fieldLabel", "The Import Template Column Name \"" + field.getCode()
							+ "\" is already used in the Field Dictionary for a Field Alias!");
					error = true;

				} else if (existingField == UffdaServiceImpl.EXISTING_CORE_FIELD_NAME) {
					LabelUtils.addErrorMessage("fieldLabel", "The Internal Field Label \"" + field.getName()
							+ "\" is already used in the Field Dictionary for a Standard Field!");
					error = true;

				} else if (existingField == UffdaServiceImpl.EXISTING_CORE_FIELD_CODE) {
					LabelUtils.addErrorMessage("fieldLabel", "The Import Template Column Name \"" + field.getCode()
							+ "\" is already used in the Field Dictionary for a Standard/Predefined Field!");
					error = true;
				}
			}

			if (StringUtils.isBlank(field.getTranslatedLabel())) {
				LabelUtils.addErrorMessage("translatedLabel", "External Field Label - cannot be blank!");
				error = true;
			}

			if (field.isOneChoice() || field.isYesNoChoice()) {
				if (!field.hasListValues()) {
					LabelUtils.addErrorMessage("choiceLabel", "You need to add at least 1 Choice Option!");
					error = true;

				} else {
					for (CustomFieldListValueDto listValue : field.getListValues()) {
						if (StringUtils.isBlank(listValue.getListValue())) {
							LabelUtils.addErrorMessage("choiceLabel", "Choice Option " + listValue.getSequenceOrder()
									+ " is missing a name!");
							error = true;
						}
					}
				}
			}

			if (field.isMultipleLineTextbox()) {
				if (field.getMinimum() < 1) {
					field.setMinimum(1);
				}
				if (field.getLength() < 1) {
					field.setLength(1);
				}
				if (field.getMinimum() > 10) {
					LabelUtils.addErrorMessage("minLines",
							"Please enter a Minimum number of lines value less than or equal to 10!");
					error = true;
				}
				if (field.getLength() > 4000) {
					LabelUtils.addErrorMessage("minLines",
							"Please enter a Maximum Characters value less than or equal to 4000!");
					error = true;
				}
			}

			if (field.isSingleLineTextbox()) {
				if (field.getMinimum() < 1) {
					field.setMinimum(1);
				}
				if (field.getLength() < 1) {
					field.setLength(1);
				}
				if (field.getMinimum() > 4000) {
					LabelUtils.addErrorMessage("minCharacters",
							"Please enter a Minimum Characters value less than or equal to 4000!");
					error = true;
				}
				if (field.getLength() > 4000) {
					LabelUtils.addErrorMessage("maxCharacters",
							"Please enter a Maximum Characters value less than or equal to 4000!");
					error = true;
				}
			}

			if (field.isNumberInput()) {
				if (field.getMinimum() < Long.MIN_VALUE) {
					LabelUtils.addErrorMessage("minNumber", "Please enter a Minimum Number greater than or equal to "
							+ Long.MIN_VALUE + "!");
					error = true;
				}

				if (field.getMaximum() > Long.MAX_VALUE) {
					LabelUtils.addErrorMessage("maxNumber", "Please enter a Maximum Number less than or equal to "
							+ Long.MAX_VALUE + "!");
					error = true;
				}

				if (field.getMinimum() > field.getMaximum()) {
					LabelUtils.addErrorMessage("minNumber",
							"Please enter a Minimum Number less than or equal to the Maximum Number!");
					error = true;
				}

				if (field.getMaximum() < field.getMinimum()) {
					LabelUtils.addErrorMessage("maxNumber",
							"Please enter a Maximum Number greater than or equal to the Minimum Number!");
					error = true;
				}

				if (field.getDefaultValue() != null && field.getDefaultValue().trim().length() > 0) {
					try {
						long defaultNumber = Long.parseLong(field.getDefaultValue());
						if (defaultNumber < field.getMinimum()) {
							LabelUtils.addErrorMessage("defaultNumber",
									"Please enter a Default Number greater than or equal to the Minimum Number!");
							error = true;
						}

						if (defaultNumber < Long.MIN_VALUE) {
							LabelUtils.addErrorMessage("defaultNumber",
									"Please enter a Default Number greater than or equal to " + Long.MIN_VALUE + "!");
							error = true;
						}

						if (defaultNumber > field.getMaximum()) {
							LabelUtils.addErrorMessage("defaultNumber",
									"Please enter a Default Number less than or equal to the Maximum Number!");
							error = true;
						}

						if (defaultNumber > Long.MAX_VALUE) {
							LabelUtils.addErrorMessage("defaultNumber",
									"Please enter a Default Number less than or equal to " + Long.MAX_VALUE + "!");
							error = true;
						}

					} catch (NumberFormatException e) {
						LabelUtils.addErrorMessage("defaultNumber", "Please enter a valid Default Number!");
						error = true;
					}
				}
			}

			// Check if the field is already assigned to a project
			if (!field.isNew() && field.isAddedToAllProjects()) {
				List<CustomFieldMapping> customFieldMappings = new ArrayList<CustomFieldMapping>();
				try {
					customFieldMappings = customFieldMappingDao.findEntitiesForCustomField(field.getDatabaseId());

				} catch (Exception e) {
					log.error("Error occurred while reading the Custom Field Mappings.  " + e);
					customFieldMappings = new ArrayList<CustomFieldMapping>();
				}

				for (CustomFieldMapping customFieldMapping : customFieldMappings) {
					if (!customFieldMapping.getFieldEntityType().getName()
							.equalsIgnoreCase(FieldEntityType.CLIENT_USERS)) {
						LabelUtils
								.addErrorMessage("includeInAllProjects",
										"You cannot include this field in all projects since it's already assigned to an individual project.");
						error = true;
						break;
					}
				}
			}
		}

		if (error || field == null) {
			return;
		}

		String whatChanged = field.getWhatChanged(null);

		EntityAuditLogger.auditLog.info("{} performed {} for Custom Field [{}] for client {}. {}", new Object[] {
				getUsername(),
				field.isEmpty() ? "a " + EntityAuditLogger.OP_CREATE_FIELD : "an " + EntityAuditLogger.OP_UPDATE_FIELD,
				field.getName(), getCompanyName(), whatChanged });

		fieldDictionaryViewBean.setShowStandardField(false);
		fieldDictionaryViewBean.setShowCustomField(false);
		fieldDictionaryViewBean.setClientSideValueChanged(true);
		field.setChanged(true);

		if (viewBean.isEditOnly()) {
			projectFieldsViewBean.addField(field);
			projectFieldsViewBean.sortFields();
			projectFieldsViewBean.setClientSideValueChanged(true);

			viewBean.init();
			if (field != null) {
				closeLightBox(field.getEditLightBoxId());
			}

		} else {
			if (field.isEmpty()) {
				field.setId(fieldDictionaryViewBean.getNextFieldId());
				if (field.isAddedToAllProjects()) {
					field.setSequenceOrder(fieldDictionaryViewBean.getNextSequence(
							CustomFieldDto.GLOBAL_CUSTOM_SEQ_BASE, CustomFieldDto.GLOBAL_CUSTOM_SEQ_MAX));
				} else {
					field.setSequenceOrder(fieldDictionaryViewBean.getNextSequence(CustomFieldDto.CUSTOM_SEQ_BASE,
							CustomFieldDto.CUSTOM_SEQ_MAX));
				}
			}

			if (field.isAddedToAllProjectsChanged()) {
				CustomFieldDto newField = new CustomFieldDto(field);

				if (field.isAddedToAllProjects()) {
					field.setSequenceOrder(fieldDictionaryViewBean.getNextSequence(
							CustomFieldDto.GLOBAL_CUSTOM_SEQ_BASE, CustomFieldDto.GLOBAL_CUSTOM_SEQ_MAX));
				} else {
					field.setSequenceOrder(fieldDictionaryViewBean.getNextSequence(CustomFieldDto.CUSTOM_SEQ_BASE,
							CustomFieldDto.CUSTOM_SEQ_MAX));
				}

				if (field.isAddedToAllProjects()) {
					if (!projectFieldsViewBean.hasField(field)) {
						projectFieldsViewBean.addField(newField);
					}

				} else {
					projectFieldsViewBean.removeField(field);
				}
				projectFieldsViewBean.setClientSideValueChanged(true);
			}

			uffdaService.saveCustomField(sessionBean.getSubject_id(), sessionBean.getCompany().getCompanyId(), field);
			fieldDictionaryViewBean.addField(field);
			updateFieldList();
			viewBean.init();
		}
	}

	public void updateFieldList() {
		List<FieldDto> fields = projectFieldsViewBean.getFields();

		for (FieldDto fieldDto : fields) {
			if (fieldDto.isCustomField()) {
				CustomFieldDto customFieldDto = (CustomFieldDto) fieldDto;
				CustomFieldDto dictionaryField = fieldDictionaryViewBean.findCustomFieldByDatabaseId(customFieldDto
						.getDatabaseId());

				if (dictionaryField != null) {
					customFieldDto.setName(dictionaryField.getName());
					customFieldDto.setCode(dictionaryField.getCode());
					customFieldDto.setDescription(dictionaryField.getDescription());
					customFieldDto.setLength(dictionaryField.getLength());
					customFieldDto.setInternal(dictionaryField.isInternal());
					customFieldDto.setParentId(dictionaryField.getParentId());
					customFieldDto.setMinimum(dictionaryField.getMinimum());
					customFieldDto.setMaximum(dictionaryField.getMaximum());
					customFieldDto.setDefaultValue(dictionaryField.getDefaultValue());
					customFieldDto.setDefaultDate(dictionaryField.getDefaultDate());
					customFieldDto.setDefaultChoice(dictionaryField.getDefaultChoice());
					customFieldDto.setAddedToAllProjects(dictionaryField.isAddedToAllProjects());
					customFieldDto.setInternalFieldVisibleToClient(dictionaryField.isInternalFieldVisibleToClient());

					customFieldDto.clearListValues();
					for (CustomFieldListValueDto lValue : dictionaryField.getListValues()) {
						customFieldDto.addListValue(new CustomFieldListValueDto(lValue));
					}

					customFieldDto.clearChanges();
					for (CustomFieldChangeDto cValue : dictionaryField.getChanges()) {
						customFieldDto.addChange(new CustomFieldChangeDto(cValue));
					}

					customFieldDto.clearFieldNames();
					for (Map.Entry<String, FieldNameDto> fieldNameSet : dictionaryField.getFieldNames().entrySet()) {
						customFieldDto.addFieldName(new FieldNameDto(fieldNameSet.getValue()));
					}

					customFieldDto.clearFieldNameAliases();
					for (FieldAliasVo fieldNameAlias : dictionaryField.getFieldNameAliases()) {
						customFieldDto.addFieldNameAlias(new FieldAliasVo(fieldNameAlias));
					}

					customFieldDto.clearFieldTransformations();
					for (FieldTransformationVo fieldTransformation : dictionaryField.getFieldTransformations()) {
						customFieldDto.addFieldTransformation(new FieldTransformationVo(fieldTransformation));
					}
				}
			}
		}

		// Sort the fields
		projectFieldsViewBean.sortFields();
	}

	public void handleCancelField(ActionEvent event) {
		FieldDto field = viewBean.getField();
		fieldDictionaryViewBean.setShowCustomField(false);
		fieldDictionaryViewBean.setShowStandardField(false);

		if (viewBean.isEditOnly()) {
			viewBean.init();
			if (field != null) {
				closeLightBox(field.getEditLightBoxId());
			}

		} else {
			viewBean.init();
		}
	}

	public void handleChangelog(ActionEvent event) {
		if (viewBean.isShowChangeLog()) {
			viewBean.setShowChangeLog(false);
		} else {
			viewBean.setShowChangeLog(true);
		}
	}

	public void handleSelectInputType(AjaxBehaviorEvent event) {
		CustomFieldDto field = viewBean.getField();
		if (field != null) {
			field.clearListValues();
			field.initField();
		}
	}

	public void handleAddChoice(ActionEvent event) {
		CustomFieldDto field = viewBean.getField();
		if (field != null) {
			CustomFieldListValueDto listValue = new CustomFieldListValueDto();
			listValue.setId(field.getNextListValueId());
			field.addListValue(listValue);
			field.updateListSequenceOrder();
		}
	}

	public void handleRemoveChoice(ActionEvent event) {
		Integer choiceId = (Integer) event.getComponent().getAttributes().get("choiceId");
		if (choiceId != null) {

			CustomFieldDto field = viewBean.getField();
			if (field != null) {
				CustomFieldListValueDto removedListValue = field.removeListValueById(choiceId.intValue());
				if (removedListValue != null && removedListValue.isExisting()) {
					field.addDeletedListValue(removedListValue);
				}
			}
		}
	}

	public void handleSelectChoice(SelectEvent event) {
		CustomFieldListValueDto listValue = viewBean.getSelectedChoice();
		CustomFieldDto field = viewBean.getField();

		viewBean.setDisableMoveChoiceTop(true);
		viewBean.setDisableMoveChoiceUp(true);
		viewBean.setDisableMoveChoiceDown(true);
		viewBean.setDisableMoveChoiceBottom(true);

		if (field.hasListValues()) {
			if (listValue.getSequenceOrder() > 1) {
				viewBean.setDisableMoveChoiceUp(false);
				viewBean.setDisableMoveChoiceTop(false);
			}

			if (listValue.getSequenceOrder() < field.getLastListValueSequenceOrder()) {
				viewBean.setDisableMoveChoiceDown(false);
				viewBean.setDisableMoveChoiceBottom(false);
			}
		}
	}

	public void handleMoveChoiceTop(ActionEvent event) {
		CustomFieldListValueDto listValue = viewBean.getSelectedChoice();
		CustomFieldDto field = viewBean.getField();

		if (field.hasListValues() && listValue.getSequenceOrder() > 1) {
			listValue.setSequenceOrder(0);
			field.sortListValues();
			field.updateListSequenceOrder();
			handleSelectChoice(null);
		}
	}

	public void handleMoveChoiceUp(ActionEvent event) {
		CustomFieldListValueDto listValue = viewBean.getSelectedChoice();
		CustomFieldDto field = viewBean.getField();

		if (field.hasListValues() && listValue.getSequenceOrder() > 1
				&& listValue.getSequenceOrder() <= field.getLastListValueSequenceOrder()) {

			CustomFieldListValueDto previousListValue = field
					.findListValueBySequenceOrder(listValue.getSequenceOrder() - 1);

			if (previousListValue != null) {
				int sequenceOrder = listValue.getSequenceOrder();
				listValue.setSequenceOrder(previousListValue.getSequenceOrder());
				previousListValue.setSequenceOrder(sequenceOrder);

				field.sortListValues();
				field.updateListSequenceOrder();
				handleSelectChoice(null);
			}
		}
	}

	public void handleMoveChoiceDown(ActionEvent event) {
		CustomFieldListValueDto listValue = viewBean.getSelectedChoice();
		CustomFieldDto field = viewBean.getField();

		if (field.hasListValues() && listValue.getSequenceOrder() >= 1
				&& listValue.getSequenceOrder() < field.getLastListValueSequenceOrder()) {

			CustomFieldListValueDto nextListValue = field
					.findListValueBySequenceOrder(listValue.getSequenceOrder() + 1);

			if (nextListValue != null) {
				int sequenceOrder = listValue.getSequenceOrder();
				listValue.setSequenceOrder(nextListValue.getSequenceOrder());
				nextListValue.setSequenceOrder(sequenceOrder);

				field.sortListValues();
				field.updateListSequenceOrder();
				handleSelectChoice(null);
			}
		}
	}

	public void handleMoveChoiceBottom(ActionEvent event) {
		CustomFieldListValueDto listValue = viewBean.getSelectedChoice();
		CustomFieldDto field = viewBean.getField();

		if (field.hasListValues() && listValue.getSequenceOrder() < field.getLastListValueSequenceOrder()) {
			listValue.setSequenceOrder(9999999);
			field.sortListValues();
			field.updateListSequenceOrder();
			handleSelectChoice(null);
		}
	}

	public boolean isChanged(String value1, String value2) {
		if ((value1 != null && value2 == null) || (value1 == null && value2 != null)
				|| !value1.equalsIgnoreCase(value2)) {
			return true;
		}
		return false;
	}

	@Override
	public FieldViewBean<?> getViewBean() {
		return viewBean;
	}

	public CustomFieldViewBean getCustomFieldViewBean() {
		return viewBean;
	}

	public void setCustomFieldViewBean(CustomFieldViewBean customFieldViewBean) {
		this.viewBean = customFieldViewBean;
	}

	public LanguageBean getLanguageBean() {
		return languageBean;
	}

	public void setLanguageBean(LanguageBean languageBean) {
		this.languageBean = languageBean;
	}

	public ProjectFieldsViewBean getProjectFieldsViewBean() {
		return projectFieldsViewBean;
	}

	public void setProjectFieldsViewBean(ProjectFieldsViewBean projectFieldsViewBean) {
		this.projectFieldsViewBean = projectFieldsViewBean;
	}

}