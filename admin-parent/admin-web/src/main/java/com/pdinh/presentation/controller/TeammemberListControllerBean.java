package com.pdinh.presentation.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.presentation.view.TeammemberListViewBean;

@ManagedBean
@ViewScoped
public class TeammemberListControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ClientListControllerBean.class);

	@ManagedProperty(name = "teammemberListViewBean", value = "#{teammemberListViewBean}")
	private TeammemberListViewBean teammemberListViewBean;

	public String getAccessIcon(ExternalSubjectDataObj subject) {
		String keyIconPath = "";

		if (subject.getCurrentRuleName() != null) {
			if (subject.getCurrentRuleName().equals("AllowChildren")) {
				keyIconPath = "images/key_gold.png";
			} else if (subject.getCurrentRuleName().equals("Allow")) {
				keyIconPath = "images/key_silver.png";
			}
		} else {
			keyIconPath = "images/key_glass.png";
		}

		return keyIconPath;
	}

	public String getAccessTooltip(ExternalSubjectDataObj subject) {
		String tooltip = "none";

		if (subject.getCurrentRuleName() != null) {
			if (subject.getCurrentRuleName().equals("AllowChildren")) {
				tooltip = "Full Access";
			} else if (subject.getCurrentRuleName().equals("Allow")) {
				tooltip = "Limited Access";
			}
		} else {
			tooltip = "No Access";
		}

		return tooltip;
	}

	public TeammemberListViewBean getTeammemberListViewBean() {
		return teammemberListViewBean;
	}

	public void setTeammemberListViewBean(TeammemberListViewBean teammemberListViewBean) {
		this.teammemberListViewBean = teammemberListViewBean;
	}

}
