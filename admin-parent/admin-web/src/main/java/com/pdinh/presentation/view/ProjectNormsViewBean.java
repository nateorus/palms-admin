package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.CourseNormsObj;
import com.pdinh.presentation.domain.NormGroupObj;
import com.pdinh.presentation.domain.ProjectNormsDefaultsObj;

@ManagedBean(name = "projectNormsViewBean")
@ViewScoped
public class ProjectNormsViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static String GEN_POP = NormGroupObj.GEN_POP;
	private static String SPEC_POP = NormGroupObj.SPEC_POP;

	private String projectName;
	private String projectTypeName;
	private boolean instrumentsVisible;

	private List<CourseNormsObj> courseNormsList;

	private ProjectNormsDefaultsObj projectNormsDefaultsObj;

	/*
	 * Setter Getter
	 */

	public String getProjectName() {
		return projectName;

	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;

	}

	public boolean isInstrumentsVisible() {
		return instrumentsVisible;
	}

	public void setInstrumentsVisible(boolean instrumentsVisible) {
		this.instrumentsVisible = instrumentsVisible;
	}

	public String getProjectTypeName() {
		return projectTypeName;
	}

	public void setProjectTypeName(String projectTypeName) {
		this.projectTypeName = projectTypeName;
	}

	public List<CourseNormsObj> getCourseNormsList() {
		return courseNormsList;
	}

	public void setCourseNormsList(List<CourseNormsObj> courseNormsList) {
		this.courseNormsList = courseNormsList;
	}

	public ProjectNormsDefaultsObj getProjectNormsDefaultsObj() {
		return projectNormsDefaultsObj;
	}

	public void setProjectNormsDefaultsObj(ProjectNormsDefaultsObj projectNormsObj) {
		this.projectNormsDefaultsObj = projectNormsObj;
	}

	public static String getGEN_POP() {
		return GEN_POP;
	}


	public static String getSPEC_POP() {
		return SPEC_POP;
	}

	public static void setSPEC_POP(String sPEC_POP) {
		SPEC_POP = sPEC_POP;
	}

}
