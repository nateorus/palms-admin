package com.pdinh.presentation.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;

import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.lookup.FieldCategoryMapper;
import com.kf.uffda.service.UffdaService;
import com.kf.uffda.service.UffdaServiceImpl;
import com.kf.uffda.view.FieldViewBean;
import com.kf.uffda.vo.FieldAliasVo;
import com.kf.uffda.vo.FieldTransformationTypeVo;
import com.kf.uffda.vo.FieldTransformationVo;
import com.kf.uffda.vo.FieldValueMapSetVo;
import com.kf.uffda.vo.FieldValueMapVo;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.view.FieldDictionaryViewBean;

public abstract class FieldControllerBean extends ControllerBeanBase {

	@EJB
	UffdaService uffdaService;

	@EJB
	FieldCategoryMapper fieldCategoryMapper;

	@ManagedProperty(name = "fieldDictionaryViewBean", value = "#{fieldDictionaryViewBean}")
	protected FieldDictionaryViewBean fieldDictionaryViewBean;

	public FieldControllerBean() {
		super();
	}

	public FieldControllerBean(String formName) {
		super(formName);
	}

	abstract public FieldViewBean<?> getViewBean();

	/**
	 * Initialize the view bean
	 */
	@Override
	public void initView(ComponentSystemEvent event) {
		if (isFirstTime()) {

			super.initView(event);
			if (getViewBean() != null) {
				getViewBean().setTransformationTypes(uffdaService.getFieldTransformationTypes());
			}
		}
	}

	/**
	 * Handle the Alias selection
	 * 
	 * @param event
	 */
	public void handleEditSelectedAlias(ActionEvent event) {
		setSelectedAlias(getFieldNameAlias(event));
		setAliasName(getSelectedAlias().getAliasName());
	}

	/**
	 * Create a new Alias
	 */
	public void handleNewSelectedAlias() {
		clearSelectedAlias();
	}

	/**
	 * Add the Alias to the field
	 */
	public void handleSaveAlias() {
		boolean error = false;
		if (getSelectedAlias() != null) {

			if (StringUtils.isBlank(getAliasName())) {
				LabelUtils.addErrorMessage("addAliasPanel", "Field Alias is required!");
				error = true;
			}

			int existingField = uffdaService.checkForExistingField(sessionBean.getCompany().getCompanyId(),
					getAliasName(), getAliasName(), getSelectedAlias().getId());

			if (existingField == UffdaServiceImpl.EXISTING_CUSTOM_FIELD_NAME
					|| existingField == UffdaServiceImpl.EXISTING_CUSTOM_FIELD_CODE) {
				LabelUtils.addErrorMessage("addAliasPanel", "The Field Alias \"" + getAliasName()
						+ "\" is already used in the Field Dictionary for a Custom Field!");
				error = true;

			} else if (existingField == UffdaServiceImpl.EXISTING_ALIAS_NAME
					|| existingField == UffdaServiceImpl.EXISTING_ALIAS_CODE) {
				LabelUtils.addErrorMessage("addAliasPanel", "The Field Alias \"" + getAliasName()
						+ "\" is already used in the Field Dictionary for a Field Alias!");
				error = true;

			} else if (existingField == UffdaServiceImpl.EXISTING_CORE_FIELD_NAME
					|| existingField == UffdaServiceImpl.EXISTING_CORE_FIELD_CODE) {
				LabelUtils.addErrorMessage("addAliasPanel", "The Field Alias \"" + getAliasName()
						+ "\" is already used in the Field Dictionary for a Standard/Predefined Field!");
				error = true;
			}

			if (error) {
				return;
			}

			getSelectedAlias().setAliasName(getAliasName());
			getSelectedAlias().setChanged(true);

			if (getSelectedAlias().isEmpty()) {
				getSelectedAlias().setId(getViewBean().getField().getNextFieldNameAliasId());
				getViewBean().getField().addFieldNameAlias(getSelectedAlias());
			}
		}
		getViewBean().getField().setChanged(true);
		clearSelectedAlias();
		RequestContext.getCurrentInstance().execute("hideAliasDialog();");
	}

	/**
	 * Select the Alias to be deleted
	 * 
	 * @param event
	 */
	public void handleDeleteSelectedAlias(ActionEvent event) {
		setSelectedAlias(getFieldNameAlias(event));
	}

	/**
	 * Remove the Alias from the field
	 * 
	 * @param event
	 */
	public void handleRemoveAlias(ActionEvent event) {
		if (getSelectedAlias() != null) {
			getViewBean().getField().getFieldNameAliases().remove(getSelectedAlias());
			getViewBean().getField().addDeletedAlias(getSelectedAlias());
		}
		getViewBean().getField().setChanged(true);
		clearSelectedAlias();
	}

	/**
	 * Find the Field Alias using the FieldAliasId
	 * 
	 * @param event
	 * @return
	 */
	public FieldAliasVo getFieldNameAlias(ActionEvent event) {
		FieldAliasVo field = null;
		if (event != null) {
			Integer fieldAliasId = (Integer) event.getComponent().getAttributes().get("fieldAliasId");
			if (fieldAliasId != null) {
				field = getViewBean().getField().findFieldAliasById(fieldAliasId);
			}
		}
		return field;
	}

	/**
	 * Create a new Transformation
	 */
	public void handleAddTransformation() {
		clearSelectedTransformation();

		getViewBean().setShowValueMap(false);
		getViewBean().setShowValueList(false);
		getViewBean().setShowAddTransformation(true);
		getViewBean().setTransformationSaveAllowed(false);

		setSelectedValueMapId(0);
		clearSelectedValueMap();
		setSelectedTransformationTypeId(0);
	}

	/**
	 * Handle the Alias selection
	 * 
	 * @param event
	 */
	public void handleEditTransformation(ActionEvent event) {
		getViewBean().setShowValueMap(false);
		getViewBean().setShowValueList(false);
		getViewBean().setShowAddTransformation(true);
		getViewBean().setTransformationSaveAllowed(false);

		setSelectedTransformationTypeId(0);
		setSelectedValueMapId(0);
		clearSelectedValueMap();

		setSelectedTransformation(getFieldTransformation(event));

		if (getSelectedTransformation() != null) {

			FieldTransformationTypeVo transformationType = getSelectedTransformation().getFieldTransformationType();
			if (transformationType != null) {
				setSelectedTransformationTypeId(transformationType.getId());

				if (transformationType.isValueMap()) {
					getViewBean().setShowValueMap(true);
				} else {
					getViewBean().setTransformationSaveAllowed(true);
				}
			}

			FieldValueMapSetVo valueMap = getSelectedTransformation().getFieldValueMapSet();
			if (valueMap != null) {
				setSelectedValueMapId(valueMap.getId());
				setSelectedValueMap(valueMap);
			}

			if (getSelectedValueMapId() != 0) {
				getViewBean().setShowValueList(true);
				getViewBean().setTransformationSaveAllowed(true);
			}
		}
	}

	/**
	 * Add the Transformation to the field
	 */
	public void handleSaveTransformation() {
		if (getSelectedTransformation() != null) {

			boolean error = false;
			if (!getSelectedTransformation().isExisting() && getSelectedTransformation().isValueMapTransformation()
					&& getViewBean().getField().hasValueMapTransformation()) {
				LabelUtils.addErrorMessage("addTransformationPanel",
						"Sorry, you can only have one Value Map Tranformation per field!");
				error = true;
			}

			if (error) {
				return;
			}

			getSelectedTransformation().setChanged(true);
			getSelectedTransformation().setFieldValueMapSet(getSelectedValueMap());

			if (getSelectedTransformation().isEmpty()) {
				getSelectedTransformation().setId(getViewBean().getField().getNextFieldTransformationId());
				getViewBean().getField().addFieldTransformation(getSelectedTransformation());
			}
		}
		getViewBean().getField().setChanged(true);
		clearSelectedTransformation();
		getViewBean().setShowAddTransformation(false);
		getViewBean().setTransformationSaveAllowed(false);
	}

	/**
	 * Add the Transformation to the field
	 */
	public void handleCancelTransformation() {
		getViewBean().setShowAddTransformation(false);
		getViewBean().setShowValueMap(false);
		getViewBean().setShowValueList(false);
		getViewBean().setTransformationSaveAllowed(false);

		setSelectedTransformationTypeId(0);
		setSelectedValueMapId(0);

		clearSelectedValueMap();
		clearSelectedTransformation();
	}

	/**
	 * Select the transformation to delete
	 * 
	 * @param event
	 */
	public void handleDeleteTransformation(ActionEvent event) {
		setSelectedTransformation(getFieldTransformation(event));
	}

	/**
	 * Remove the transformation from the field.
	 */
	public void handleRemoveTransformation() {
		if (getSelectedTransformation() != null) {
			getViewBean().getField().getFieldTransformations().remove(getSelectedTransformation());
			getViewBean().getField().addDeletedTransformation(getSelectedTransformation());
		}
		getViewBean().getField().setChanged(true);
		clearSelectedTransformation();
	}

	/**
	 * Get the transformation based on the specified fieldTransformationId
	 * 
	 * @param event
	 * @return
	 */
	public FieldTransformationVo getFieldTransformation(ActionEvent event) {
		FieldTransformationVo field = null;
		if (event != null) {
			Integer fieldTransformationId = (Integer) event.getComponent().getAttributes().get("fieldTransformationId");
			if (fieldTransformationId != null) {
				field = getViewBean().getField().findFieldTransformationById(fieldTransformationId);
			}
		}
		return field;
	}

	/**
	 * Handle Transformation Type change
	 */
	public void handleTransformationTypeChange() {
		getViewBean().setShowValueMap(false);
		getViewBean().setShowValueList(false);
		getViewBean().setTransformationSaveAllowed(false);

		setSelectedValueMapId(0);
		clearSelectedValueMap();

		FieldTransformationTypeVo transformationType = getViewBean().findTransformationTypeById(
				getSelectedTransformationTypeId());

		if (getSelectedTransformation() != null) {
			getSelectedTransformation().setFieldTransformationType(transformationType);

			if (transformationType != null) {
				if (transformationType.isValueMap()) {
					getViewBean().setShowValueMap(true);
				}
			}

			FieldValueMapSetVo valueMap = getSelectedTransformation().getFieldValueMapSet();
			if (valueMap != null) {
				setSelectedValueMapId(valueMap.getId());
				setSelectedValueMap(valueMap);

				if (getSelectedValueMapId() != 0) {
					getViewBean().setShowValueList(true);
				}
			}
		}

		if (getSelectedTransformationTypeId() != 0 && getSelectedValueMapId() != 0) {
			getViewBean().setTransformationSaveAllowed(true);
		}
	}

	/**
	 * Handle Value Map Change
	 */
	public void handleValueMapChange() {
		if (getSelectedValueMapId() != 0) {
			getViewBean().setShowValueList(true);
			setSelectedValueMap(getViewBean().findValueMapById(getSelectedValueMapId()));
			getSelectedValueMap().setFieldValueMapValues(
					uffdaService.getFieldValueMaps(getViewBean().getField().getCompanyId(), getSelectedValueMap()));
		} else {
			getViewBean().setShowValueList(false);
			clearSelectedValueMap();
		}

		if (getSelectedTransformationTypeId() != 0 && getSelectedValueMapId() != 0) {
			getViewBean().setTransformationSaveAllowed(true);
		} else {
			getViewBean().setTransformationSaveAllowed(false);
		}
	}

	/**
	 * Handle add the value map
	 */
	public void handleAddValueMap() {
		clearSelectedValueMap();
	}

	/**
	 * Handle Edit Value Map
	 * 
	 * @param event
	 */
	public void handleEditValueMap(ActionEvent event) {
		setSelectedValueMap(getValueMap(event));
		if (getSelectedValueMap() != null) {
			setName(getSelectedValueMap().getName());
			setDescription(getSelectedValueMap().getDescription());
		}
	}

	/**
	 * Handle Save Value Map
	 * 
	 * @param event
	 */
	public void handleSaveValueMap(ActionEvent event) {
		if (getSelectedValueMap() != null) {

			boolean error = false;
			if (StringUtils.isBlank(getName())) {
				LabelUtils.addErrorMessage("addValueMapPanel", "Name is required!");
				error = true;
			}

			if (StringUtils.isBlank(getDescription())) {
				LabelUtils.addErrorMessage("addValueMapPanel", "Description is required!");
				error = true;
			}

			if (error) {
				return;
			}

			getSelectedValueMap().setChanged(true);
			getSelectedValueMap().setName(getName());
			getSelectedValueMap().setDescription(getDescription());

			if (getSelectedValueMap().isEmpty()) {
				getSelectedValueMap().setId(getViewBean().getNextValueMapId());
				getViewBean().addValueMap(getSelectedValueMap());
			}

			// Save the Field Value Map Set to the database
			uffdaService.saveFieldValueMapSet(sessionBean.getSubject_id(), getViewBean().getField().getCompanyId(),
					getSelectedValueMap());
			setSelectedValueMapId(getSelectedValueMap().getId());
		}

		if (getSelectedValueMapId() != 0) {
			getViewBean().setTransformationSaveAllowed(true);
			getViewBean().setShowValueList(true);
		} else {
			getViewBean().setShowValueList(false);
		}

		RequestContext.getCurrentInstance().execute("hideValueMapDialog();");
	}

	/**
	 * Handle Delete Value Map
	 * 
	 * @param event
	 */
	public void handleDeleteValueMap(ActionEvent event) {
		setSelectedValueMap(getValueMap(event));
	}

	/**
	 * Handle Remove Value Map
	 */
	public void handleRemoveValueMap() {
		FieldValueMapSetVo valueMap = getSelectedValueMap();
		if (valueMap != null) {
			getViewBean().getValueMaps().remove(valueMap);
			uffdaService.deleteFieldValueMapSet(sessionBean.getSubject_id(), getSelectedValueMap());

			if (getSelectedTransformation() != null) {
				getViewBean().getField().getFieldTransformations().remove(getSelectedTransformation());
			}

			// Remove the Field Value Map Set from all fields
			for (FieldDto fieldDto : fieldDictionaryViewBean.getFieldDictionary()) {
				for (FieldTransformationVo fieldTransformation : fieldDto.getFieldTransformations()) {

					FieldValueMapSetVo fieldValueMapSetVo = fieldTransformation.getFieldValueMapSet();
					if (fieldValueMapSetVo != null && fieldValueMapSetVo.getId() == valueMap.getId()) {
						fieldDto.getFieldTransformations().remove(fieldTransformation);
						break;
					}
				}
			}
		}

		clearSelectedTransformation();
		setSelectedValueMapId(0);
		clearSelectedValueMap();
		getViewBean().setShowAddTransformation(false);
		getViewBean().setShowValueMap(false);
		getViewBean().setShowValueList(false);
		getViewBean().setTransformationSaveAllowed(false);
	}

	/**
	 * Get the value map
	 * 
	 * @param event
	 * @return
	 */
	public FieldValueMapSetVo getValueMap(ActionEvent event) {
		FieldValueMapSetVo valueMap = null;
		if (event != null) {
			Integer valueMapId = (Integer) event.getComponent().getAttributes().get("valueMapId");
			if (valueMapId != null) {
				valueMap = getViewBean().findValueMapById(valueMapId);
			}
		}
		return valueMap;
	}

	/**
	 * Handle Add the Value Map Value
	 */
	public void handleAddValue() {
		clearSelectedValue();
	}

	/**
	 * Handle Edit Value Map Value
	 * 
	 * @param event
	 */
	public void handleEditValue(ActionEvent event) {
		setSelectedValue(getValue(event));
		if (getSelectedValue() != null) {
			setFromValue(getSelectedValue().getFromValue());
			setToValue(getSelectedValue().getToValue());
		}
	}

	/**
	 * Handle Save the Value Map Value
	 * 
	 * @param event
	 */
	public void handleSaveValue(ActionEvent event) {
		if (getSelectedValue() != null) {

			boolean error = false;
			if (StringUtils.isBlank(getFromValue())) {
				LabelUtils.addErrorMessage("addValuePanel", "From Value is required!");
				error = true;
			}

			if (StringUtils.isBlank(getToValue())) {
				LabelUtils.addErrorMessage("addValuePanel", "To Value is required!");
				error = true;
			}

			if (error) {
				return;
			}

			getSelectedValue().setChanged(true);
			getSelectedValue().setFromValue(getFromValue());
			getSelectedValue().setToValue(getToValue());

			if (getSelectedValue().isEmpty()) {
				getSelectedValue().setId(getSelectedValueMap().getNextFieldValueMapValueId());
				getSelectedValueMap().addFieldValueMapValue(getSelectedValue());
			}

			// Save the Field Value Map to the database
			uffdaService.saveFieldValueMap(sessionBean.getSubject_id(), getSelectedValueMap().getId(),
					getSelectedValue());
		}
		clearSelectedValue();

		RequestContext.getCurrentInstance().execute("hideValueDialog();");
	}

	/**
	 * Handle Delete Value Map Value
	 * 
	 * @param event
	 */
	public void handleDeleteValue(ActionEvent event) {
		setSelectedValue(getValue(event));
	}

	/**
	 * Handle Remove Value Map Value
	 */
	public void handleRemoveValue() {
		FieldValueMapVo value = getSelectedValue();
		if (value != null) {
			// Get the value from the array list
			getSelectedValueMap().getFieldValueMapValues().remove(value);
			getSelectedValueMap().addDeletedFieldValueMapValue(value);

			// Delete the Field Value Map from the database
			uffdaService.deleteFieldValueMap(sessionBean.getSubject_id(), value);
		}
		this.clearSelectedValue();
	}

	/**
	 * Get the value map value
	 * 
	 * @param event
	 * @return
	 */
	public FieldValueMapVo getValue(ActionEvent event) {
		FieldValueMapVo value = null;
		if (event != null) {
			Integer valueId = (Integer) event.getComponent().getAttributes().get("valueId");
			if (valueId != null) {
				value = getSelectedValueMap().findFieldValueMapValueById(valueId);
			}
		}
		return value;
	}

	public FieldTransformationVo getSelectedTransformation() {
		return getViewBean().getSelectedTransformation();
	}

	public void setSelectedTransformation(FieldTransformationVo selectedTransformation) {
		getViewBean().setSelectedTransformation(selectedTransformation);
	}

	public FieldValueMapSetVo getSelectedValueMap() {
		return getViewBean().getSelectedValueMap();
	}

	public void setSelectedValueMap(FieldValueMapSetVo selectedValueMap) {
		getViewBean().setSelectedValueMap(selectedValueMap);
	}

	public int getSelectedValueMapId() {
		return getViewBean().getSelectedValueMapId();
	}

	public void setSelectedValueMapId(int id) {
		getViewBean().setSelectedValueMapId(id);
	}

	public int getSelectedTransformationTypeId() {
		return getViewBean().getSelectedTransformationTypeId();
	}

	public void setSelectedTransformationTypeId(int id) {
		getViewBean().setSelectedTransformationTypeId(id);
	}

	public FieldAliasVo getSelectedAlias() {
		return getViewBean().getSelectedAlias();
	}

	public void setSelectedAlias(FieldAliasVo selectedAlias) {
		getViewBean().setSelectedAlias(selectedAlias);
	}

	public FieldValueMapVo getSelectedValue() {
		return getViewBean().getSelectedValue();
	}

	public void setSelectedValue(FieldValueMapVo selectedValue) {
		getViewBean().setSelectedValue(selectedValue);
	}

	public String getAliasName() {
		return getViewBean().getAliasName();
	}

	public void setAliasName(String aliasName) {
		getViewBean().setAliasName(aliasName);
	}

	public String getName() {
		return getViewBean().getName();
	}

	public void setName(String name) {
		getViewBean().setName(name);
	}

	public String getDescription() {
		return getViewBean().getDescription();
	}

	public void setDescription(String description) {
		getViewBean().setDescription(description);
	}

	public String getFromValue() {
		return getViewBean().getFromValue();
	}

	public void setFromValue(String value) {
		getViewBean().setFromValue(value);
	}

	public String getToValue() {
		return getViewBean().getToValue();
	}

	public void setToValue(String value) {
		getViewBean().setToValue(value);
	}

	public void clearSelectedAlias() {
		setSelectedAlias(new FieldAliasVo(getViewBean().getField().getCompanyId(), getViewBean().getField().getId(),
				getViewBean().getField().getFieldCategory()));
		setAliasName("");
	}

	public void clearSelectedTransformation() {
		setSelectedTransformation(new FieldTransformationVo(getViewBean().getField().getCompanyId(), getViewBean()
				.getField().getId(), getViewBean().getField().getFieldCategory()));
		clearSelectedValueMap();
	}

	public void clearSelectedValueMap() {
		setSelectedValueMap(new FieldValueMapSetVo(getViewBean().getField().getCompanyId()));
		clearSelectedValue();
		setName("");
		setDescription("");
	}

	public void clearSelectedValue() {
		setSelectedValue(new FieldValueMapVo(getViewBean().getField().getCompanyId()));
		setFromValue("");
		setToValue("");
	}

	public FieldDictionaryViewBean getFieldDictionaryViewBean() {
		return fieldDictionaryViewBean;
	}

	public void setFieldDictionaryViewBean(FieldDictionaryViewBean fieldDictionaryViewBean) {
		this.fieldDictionaryViewBean = fieldDictionaryViewBean;
	}

	public boolean isEditable() {
		return sessionBean.isPermitted("editFieldDictionary");
	}

	public boolean isViewable() {
		return sessionBean.isPermitted("viewFieldDictionary");
	}

}