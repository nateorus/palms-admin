package com.pdinh.presentation.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.JaxbUtil;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRequestResponseList;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ReportSourceTypeDao;
import com.pdinh.data.ms.dao.ReportTypeDao;
import com.pdinh.data.ms.dao.TargetLevelTypeDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.oxcart.SortGroup;
import com.pdinh.data.oxcart.SortInfo;
import com.pdinh.data.oxcart.SortItem;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsException;
import com.pdinh.exception.ServerUnavailableException;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectTypeReportType;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportOptionsObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.helper.ReportRequestHelper;
import com.pdinh.presentation.helper.ReportWizardHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ParticipantClipboardViewBean;
import com.pdinh.presentation.view.ParticipantListViewBean;
import com.pdinh.presentation.view.ProjectManagementViewBean;
import com.pdinh.presentation.view.ReportWizardViewBean;
import com.pdinh.reporting.AggregatedReportStream;
import com.pdinh.reporting.PortalServletServiceLocal;
import com.pdinh.reporting.ReportingServiceLocal;

@ManagedBean(name = "reportWizardControllerBean")
@ViewScoped
public class ReportWizardControllerBean implements Serializable {

	private static final long serialVersionUID = 8471192735662705014L;
	private static final String RPT_DFLT_LANG = "en";
	private static final String SORT_DATA_REQUEST = "<FetchSortInfo type=\"analytics\"/>";
	final static Logger logger = LoggerFactory.getLogger(ReportWizardControllerBean.class);

	private Boolean populatedUI = false;

	@EJB
	private UserDao userDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private LanguageSingleton languageSingleton;

	@EJB
	private ReportTypeDao reportTypeDao;

	@EJB
	private TargetLevelTypeDao targetLevelTypeDao;

	@EJB
	private ReportSourceTypeDao reportSourceTypeDao;

	@EJB
	private ReportingServiceLocal reportingService;

	@EJB
	PortalServletServiceLocal portalServletService;

	@EJB
	private ReportingSingleton reportingSingleton;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "reportWizardViewBean", value = "#{reportWizardViewBean}")
	private ReportWizardViewBean reportWizardViewBean;

	@ManagedProperty(name = "participantClipboardViewBean", value = "#{participantClipboardViewBean}")
	private ParticipantClipboardViewBean participantClipboardViewBean;

	@ManagedProperty(name = "participantListViewBean", value = "#{participantListViewBean}")
	private ParticipantListViewBean participantListViewBean;

	@ManagedProperty(name = "projectManagementViewBean", value = "#{projectManagementViewBean}")
	private ProjectManagementViewBean projectManagementViewBean;

	@ManagedProperty(name = "reportRequestHelper", value = "#{reportRequestHelper}")
	private ReportRequestHelper reportRequestHelper;

	@ManagedProperty(name = "reportWizardHelper", value = "#{reportWizardHelper}")
	private ReportWizardHelper reportWizardHelper;

	@Resource(name = "custom/abyd_properties")
	private Properties abydProperties;

	@Resource(name = "application/config_properties")
	private Properties configProperties;

	private Boolean firstTime = true;

	public void initView() {
		logger.debug("reportWizard initView");
		if (firstTime) {
			logger.debug("reportWizard firstTime");
			firstTime = false;
			reportWizardViewBean.getLanguages().addAll(new ArrayList<LanguageObj>(languageSingleton.getLanguages()));

			/**
			 * We might want to parameterize the list of participants somehow --
			 * would be optimal to have this originate from the project mgmt
			 * page as well as the clipboard
			 */

			if (reportWizardViewBean.isShowReportTypesByProjectTypes()) {
				logger.debug("Loading Report Types by participant ...");
				reportWizardViewBean.setReportTypes(getReportTypeObjListByParticipantProjectTypes());
			} else {
				logger.debug("Loading Report Types by project ...");
				reportWizardViewBean.setReportTypes(getReportTypeObjListByProjectType(sessionBean.getProject()
						.getProjectType()));
			}

			// Process the rest of this only if we have a project to work with
			if (sessionBean.getProject() != null) {
				// PA-486 -- Take out the reports for viaEdge if viaEdge is not
				// selected
				adjustReportList();
				// NHN-2108 -- Add the Assessment Testing Extract to AbyD -
				// currently exists only in Assmt Testing Project type
				// Fixed the project type report type mapping so this is no
				// longer needed.
				/*
				if (sessionBean.getProject().getProjectType().getProjectTypeCode()
						.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
						|| sessionBean.getProject().getProjectType().getProjectTypeCode()
								.equals(ProjectType.READINESS_ASSMT_BUL)
						|| sessionBean.getProject().getProjectType().getProjectTypeCode().equals(ProjectType.ERICSSON)) {
					ReportTypeObj newRT = new ReportTypeObj("Assessment Extract", "ASSESSMENT_EXTRACT", false, false,
							false);
					newRT.setReportSourceTypeId(ReportSourceType.OXCART);
					reportWizardViewBean.getReportTypes().add(0, newRT);
				}
				*/
				// (NHN-3167) if there is no LEI in the project, do not show the
				// lei in the reports list. same with gpi, rv, wg
				boolean hasRavens1 = false;
				boolean hasRavens2 = false;
				boolean hasWG1 = false;
				boolean hasWG2 = false;

				for (ProjectCourse pc : sessionBean.getProject().getProjectCourses()) {
					// if it's and lei, and it's NOT selected...
					if (pc.getCourse().getAbbv().equals("lei") && !pc.getSelected()) {
						int leiIndex = -1;
						for (ReportTypeObj rt : reportWizardViewBean.getReportTypes()) {
							leiIndex++;
							if (rt.getCode().equals("LEI_GRAPHICAL_REPORT")) {
								reportWizardViewBean.getReportTypes().remove(leiIndex);
								break;
							}
						}
					}// end lei check

					if (pc.getCourse().getAbbv().equals("rv") || pc.getCourse().getAbbv().equals("rv2")) {

						if (pc.getSelected() && pc.getCourseFlowType().getCourseFlowTypeId() == 1) {
							hasRavens1 = true;
						}
						if (pc.getSelected() && pc.getCourseFlowType().getCourseFlowTypeId() == 2) {
							hasRavens2 = true;
						}
					}// end ravens check

					if (pc.getCourse().getAbbv().equals("wg") || pc.getCourse().getAbbv().equals("wg2")) {

						if (pc.getSelected() && pc.getCourseFlowType().getCourseFlowTypeId() == 1) {
							hasWG1 = true;
						}
						if (pc.getSelected() && pc.getCourseFlowType().getCourseFlowTypeId() == 2) {
							hasWG2 = true;
						}
					}// end watson II check

					if (pc.getCourse().getAbbv().equals("gpil") && !pc.getSelected()) {
						int index = -1;
						for (ReportTypeObj rt : reportWizardViewBean.getReportTypes()) {
							index++;
							if (rt.getCode().equals("GPI_GRAPHICAL_REPORT")) {
								reportWizardViewBean.getReportTypes().remove(index);
								break;
							}
						}
					} // end gpi check
				} // End the loop on courses

				// Finish up the ones that can be in two places (pre or day-of)
				if (!hasRavens1 && !hasRavens2) {
					int index = -1;
					for (ReportTypeObj rt : reportWizardViewBean.getReportTypes()) {
						index++;
						if (rt.getCode().equals("RAV_B_GRAPHICAL_REPORT")) {
							reportWizardViewBean.getReportTypes().remove(index);
							break;
						}
					}
				} // end rv

				if (!hasWG1 && !hasWG2) {
					int index = -1;
					// remove it from the list...
					for (ReportTypeObj rt : reportWizardViewBean.getReportTypes()) {
						index++;
						if (rt.getCode().equals("WGE_GRAPHICAL_REPORT")) {
							reportWizardViewBean.getReportTypes().remove(index);
							break;
						}
					}
				} // end WG
			} // End the project != null check

		} // end if first time

		// AV: This method is used to filter out ALP reports base on ppts
		// population (ppts who complete v1 or/and v2 version of ALP assessment)
		// PA-780
		reportWizardHelper.filterALPReports(reportWizardViewBean.getReportTypes(),
				reportWizardViewBean.getParticipants());

		// Initialization should happen very time report wizard launched.
		reportWizardViewBean.init();

	}

	private void prepopulateRequestName(ParticipantObj[] participants, String appSection) {
		String requestName = StringUtils.capitalize(appSection);
		List<ParticipantObj> ppts = Arrays.asList(participants);

		if (ppts != null) {
			if (ppts.size() > 1) {
				if (sessionBean.getProject() != null && sessionBean.getSelectedProject() != null) {
					requestName = sessionBean.getProject().getName();
				}
			} else if (ppts.size() == 1) {
				requestName = ppts.get(0).getLastName();
			}
		}

		reportWizardViewBean.setRequestName(requestName + " - ");
	}

	public void initViewClipboard() {
		logger.debug("initViewClipboard");
		ParticipantObj[] participants = participantClipboardViewBean.getSelectedParticipants();
		this.copyParticipants(participants);
		this.prepopulateRequestName(participants, "clipboard");
		reportWizardViewBean.setShowReportTypesByProjectTypes(true);
		reportWizardViewBean.setProjectColumnRendered(true);

		// need to refresh each time because the report list is determined by
		// the project types of the selected participants
		this.refresh();

		// per NHN-2816 - the abyd extract report should NOT be
		// available from the clipboard or participant tabs
		int index = -1;
		for (ReportTypeObj r : reportWizardViewBean.getReportTypes()) {
			index++;
			if (r.getCode().equals(ReportType.ABYD_EXTRACT)) {
				reportWizardViewBean.getReportTypes().remove(index);
				break;
			}
		}

	}

	public void initViewProjectMgmt() {
		logger.debug("In ReportWizardControllerBean, initViewProjectMgmt()");
		ParticipantObj[] participants = projectManagementViewBean.getSelectedParticipants();
		this.copyParticipants(participants);
		this.prepopulateRequestName(participants, "project");
		reportWizardViewBean.setShowReportTypesByProjectTypes(false);
		reportWizardViewBean.setProjectColumnRendered(false);
		// AV: Unfortunately we need to do full refresh to support ALP mixed
		// population. ReportTypes list is now determined by selected
		// participants for ALP (KFAP) project type (PA-780).
		this.refresh();
		// initView();
	}

	public String goToReportCenter(int requestId) {
		if (requestId < 1) {
			return "reportCenter.xhtml?faces-redirect=true";
		}
		if (reportWizardViewBean.getParticipants() != null && reportWizardViewBean.getParticipants().size() > 1) {
			logger.debug("Multiple Participants selected.  Checking Report Center Context permissions ");
			if (sessionBean.getSelectedProject() != null
					&& sessionBean.isProjectContextPermitted(sessionBean.getSelectedProject())) {
				logger.trace("Project context is permitted and available.  Clearing user");
				sessionBean.clearUser();
			} else if (sessionBean.isClientContextPermitted(sessionBean.getCompany().getCompanyId())) {
				logger.trace("Client context is permitted and available.  Clearing user");
				sessionBean.clearUser();
			} else {
				logger.trace("project and client context not permitted or available.  must load user context");
			}
		}
		return "reportCenter.xhtml?requestId=" + String.valueOf(requestId) + "&faces-redirect=true";
	}

	public void initViewParticipantList() {
		// System.out.println("initViewParticipantList");
		ParticipantObj[] participants = participantListViewBean.getSelectedParticipants();
		this.copyParticipants(participants);
		this.prepopulateRequestName(participants, "users");
		reportWizardViewBean.setShowReportTypesByProjectTypes(true);
		reportWizardViewBean.setProjectColumnRendered(true);

		// need to refresh each time because the report list is determined by
		// the project types of the selected participants
		this.refresh();

		// per NHN-2816 - the abyd extract report should NOT be
		// available from the clipboard or participant tabs
		int index = -1;
		for (ReportTypeObj r : reportWizardViewBean.getReportTypes()) {
			index++;
			if (r.getCode().equals("ABYD_EXTRACT")) {
				reportWizardViewBean.getReportTypes().remove(index);
				break;
			}
		}
	}

	public void resetFirstTime() {
		firstTime = true;
		reportWizardViewBean.setDisableParticipantLanguageTab(true);
	}

	private void refresh() {
		firstTime = true;
		initView();
	}

	/**
	 * adjustReportList compare the report list to the courses selected and
	 * remove reports that are not applicable for the course list
	 * 
	 * Note: Implemented for PA-486 so only viaEDGE is handled currently
	 */
	private void adjustReportList() {
		boolean resetList = false;
		List<ReportTypeObj> revised = new ArrayList<ReportTypeObj>();
		// Get the course abbreviations
		HashSet<String> courses = new HashSet<String>();
		for (ProjectCourse pc : sessionBean.getProject().getProjectCourses()) {
			if (pc.getSelected()) {
				courses.add(pc.getCourse().getAbbv());
			}
		}

		logger.debug("participants = {}", reportWizardViewBean.getParticipants().size());

		// // Get the report list
		for (ReportTypeObj rto : reportWizardViewBean.getReportTypes()) {
			// Do the compares
			if (rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_SUMMARY)
					|| rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_COACHING)
					|| rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)
					|| rto.getCode().equals(ReportType.VIAEDGE_GROUP_FEEDBACK)
					|| rto.getCode().equals(ReportType.VIAEDGE_GROUP_EXTRACT)) {
				// Found one of those reports... do I squash it?
				if (!(courses.contains(Course.VEDGE) || courses.contains(Course.VEDGE_NODEMO))) {
					// suppress it
					resetList = true;
					continue;
				}
			} else if (rto.getCode().equals(ReportType.CHQ_RPT)) {
				if (!courses.contains(Course.CHQ)) {
					// suppress it
					resetList = true;
					continue;
				}
			} else if (rto.getCode().equals(ReportType.GPI_RPT)) {
				if (!(courses.contains(Course.GPI) || courses.contains(Course.GPIL))) {
					// suppress it
					resetList = true;
					continue;
				}
			} else if (rto.getCode().equals(ReportType.LEI_RPT)) {
				if (!courses.contains(Course.LEI)) {
					// suppress it
					resetList = true;
					continue;
				}
			} else if (rto.getCode().equals(ReportType.RV_RPT)) {
				if (!(courses.contains(Course.RV) || courses.contains(Course.RV2))) {
					// suppress it
					resetList = true;
					continue;
				}
			} else if (rto.getCode().equals(ReportType.WG_RPT)) {
				if (!(courses.contains(Course.WG))) {
					// suppress it
					resetList = true;
					continue;
				}
			} else if (rto.getCode().equals(ReportType.FEX_RPT)) {
				if (!(courses.contains(Course.FINEX))) {
					// suppress it
					resetList = true;
					continue;
				}
			}
			// Default... just put the entry in the output list
			revised.add(rto);
		}

		// Update the report list if needed
		if (resetList) {
			reportWizardViewBean.setReportTypes(revised);
		}

		return;
	}

	private void copyParticipants(ParticipantObj[] participants) {
		List<ParticipantObj> participantObjects = new ArrayList<ParticipantObj>();
		if (null != participants) {
			// loop through the ppts to check/set up language

			for (int x = 0; x < participants.length; x++) {
				// Curently ignoring this logic
				// if (participants[x].getReportLanguageId() == null ||
				// participants[x].getReportLanguageId().isEmpty()) {
				// // Here because there is no lang set in ParticipantObject
				// String langPref = RPT_DFLT_LANG; // default to the default
				// // language
				// // We have no language... get one... Get the User to look at
				// // the company
				// User usr = userDao.findById(participants[x].getUserId());
				// if (usr != null) {
				// // Got a user... get the language
				// if (usr.getLangPref() == null || usr.getLangPref().isEmpty())
				// {
				// // No user language, get the company language
				// if (usr.getCompany() != null) {
				// // got the company... get the language
				// if (usr.getCompany().getLangPref() != null) {
				// // bingo! use it.
				// langPref = usr.getCompany().getLangPref();
				// }
				// }
				// } else {
				// // Use the user default (probably already in the ppt
				// // Object)
				// langPref = usr.getLangPref();
				// }
				// }
				// participants[x].setReportLanguageId(langPref);
				// }

				// We want to use the option "report Default"... set to
				// empty string. This works because of this definition in
				// reportWizard.xhtml:
				// <f:selectItem itemValue="" itemLabel="Report Default" />
				participants[x].setReportLanguageId("");

				participantObjects.add(participants[x]);
			}
		}
		reportWizardViewBean.setParticipants(participantObjects);
	}

	/**
	 * Called to generate some Oxcart reports - May no longer be used
	 * 
	 * @param event
	 */
	/*
	public void handleGenerateReports(ActionEvent event) {
		generateOxcartReports(event);
	}
	*/
	/**
	 * BEGIN REPORT SOURCE REFACTOR
	 */

	// private StreamedContent reportFiles;

	/**
	 * Report generation control method.
	 * 
	 * @return
	 */
	public StreamedContent getReportFiles() {
		List<Map<String, String>> urls = generateReportRequestUrlMaps();

		// for (Map<String, String> entry : urls) {
		// Set<Entry<String, String>> looky = entry.entrySet();
		// //System.out.println("KEITH! " + looky.toString());
		// }
		AggregatedReportStream ars = reportingService.getReports(urls);
		StreamedContent reportFiles;
		reportFiles = new DefaultStreamedContent(
				new ByteArrayInputStream(ars.getByteArrayOutputStream().toByteArray()), ars.getMimeType(),
				ars.getFilename());
		return reportFiles;
	}

	public void doAudit(List<ParticipantObj> participants, List<ReportTypeObj> reportTypes) {
		Subject subject = SecurityUtils.getSubject();

		String reportNames = "";
		String action = EntityAuditLogger.OP_DOWNLOAD_REPORT;
		boolean reportNamesTruncated = false;

		for (ReportTypeObj rpt : reportTypes) {
			reportNames = reportNames + "'" + rpt.getName() + "'";
		}
		// Truncate to avoid SQL error on insert

		if (reportNames.length() > 250) {
			reportNames = reportNames.substring(0, 250);
			reportNamesTruncated = true;
		}
		// Ensure that there is data for the audit call... Assumes that the
		// subject is golden (no checks)
		String selProj = sessionBean.getSelectedProject() == null ? "N/A" : "" + sessionBean.getSelectedProject();
		String projName = (sessionBean.getProject() == null || sessionBean.getProject().getName() == null) ? "None selected"
				: sessionBean.getProject().getName();
		EntityAuditLogger.auditLog
				.info("{} performed a {} for {} participants in project: {}({}), the following Reports were included: ({}), Report names truncated: {}",
						new Object[] { subject.getPrincipal(), action, participants.size(), selProj, projName,
								reportNames, reportNamesTruncated });
	}

	public List<Map<String, String>> generateReportRequestUrlMaps() {
		List<Map<String, String>> reportSourceUrls = new ArrayList<Map<String, String>>();
		List<ParticipantObj> participants = reportWizardViewBean.getParticipants();
		List<ReportTypeObj> selectedReportTypes = reportWizardViewBean.getSelectedReportTypes();
		doAudit(participants, selectedReportTypes);
		String projectId = "";
		if (!(sessionBean.getProject() == null)) {
			projectId = "" + sessionBean.getProject().getProjectId();
		}
		try {
			return reportingService.generateReportRequestUrlMaps(participants, selectedReportTypes, projectId);
		} catch (PalmsException p) {
			if (p instanceof ServerUnavailableException) {
				logger.error("Server: {} was unavailable.  Unable to get url maps",
						((ServerUnavailableException) p).getServerName());
			} else {
				logger.error("Exception {} occurred while generating report urls:  {}", p.getErrorCode(),
						p.getMessage());
			}
			return reportSourceUrls;
		}
		/*
		List<ReportSourceType> reportSourceTypes = new ArrayList<ReportSourceType>();

		Participants ppts = ReportingWizardHelper.createParticipantsObject(participants);

		// Get a list of all report source types
		reportSourceTypes = reportSourceTypeDao.findAll();
		List<ReportSourceTypeArrayItem> reportSourceTypeArrayItems = new ArrayList<ReportWizardControllerBean.ReportSourceTypeArrayItem>();
		for (ReportSourceType rst : reportSourceTypes) {
			reportSourceTypeArrayItems.add(new ReportSourceTypeArrayItem(rst));
		}

		// Loop through the list of types, adding the reports requested for each
		// type
		for (ReportTypeObj rto : selectedReportTypes) {
			for (ReportSourceTypeArrayItem rstai : reportSourceTypeArrayItems) {
				if (rto.getReportSourceTypeId() == rstai.getReportSourceType().getReportSourceTypeId()) {
					logger.debug("rto.code = " + rto.getCode() + ", rstai.source.name = "
							+ rstai.getReportSourceType().getName());
					rstai.addReportType(rto);
				}
			}
		}

		// Loop through the (now augmented) source types and process the
		// requests
		for (ReportSourceTypeArrayItem rstai : reportSourceTypeArrayItems) {
			if (rstai.getReportTypes().size() > 0) {
				switch (rstai.getReportSourceType().getReportSourceTypeId()) {
				case ReportSourceType.PALMS:
					genPalmsUrls(ppts, rstai, reportSourceUrls);
					break;
				case ReportSourceType.OXCART:
					genOxcartUrls(participants, rstai, reportSourceUrls);
					break;
				case ReportSourceType.COACHING_EXTRACT:
					genCoachingExtractUrls(participants, rstai, reportSourceUrls);
					break;
				case ReportSourceType.HYBRID:
					genHybridUrls(participants, rstai, reportSourceUrls);
					break;
				default:
				} // End of switch (<report type>)
			}
		}

		return reportSourceUrls;
		*/
	}

	/**
	 * gePalmsUrls runs through the list of selected PALMS reports and generates
	 * the appropriate URLS and parameter data for each of them.
	 * 
	 * @param ppts
	 *            - A List of report participant objects
	 * @param rstai
	 *            - A holder for the reports requested for a particular data
	 *            source type
	 * @param reportSourceUrls
	 *            - The generated parameter lists (including the URL)
	 */
	// MOVED TO REPORT SERVICE HELPER - admin-ejb
	//
	// private void genPalmsUrls(Participants ppts, ReportSourceTypeArrayItem
	// rstai,
	// List<Map<String, String>> reportSourceUrls) {
	// String xmlParamVal = null;
	// String xmlParamName = null;
	//
	// // loop through the requested reports in the PALMS type, generating the
	// // appropriate values in the urlMap
	// Map<String, String> urlMap = null;
	// for (ReportTypeObj rto : rstai.getReportTypes()) {
	// urlMap = new HashMap<String, String>();
	// if (rto.getCode().equals(ReportType.COACHING_PLAN_SUMMARY)) {
	// // urlMap = new HashMap<String, String>();
	// xmlParamVal = null;
	// xmlParamName = null;
	//
	// String ppParamVal = reportingService.generationParticipantsXml(ppts);
	// logger.debug(ppParamVal);
	//
	// String url = configProperties.getProperty("reportserverBaseUrlCoaching")
	// + "/CoachingSummaryRcmServlet";
	// String ppParamName = "participants";
	// Map<String, String> params = new HashMap<String, String>();
	// params.put(ppParamName, ppParamVal);
	// String rcm = "";
	// try {
	// rcm = ReportingWizardHelper.getRcm(url, params);
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// xmlParamVal = rcm;
	// xmlParamName = rstai.getReportSourceType().getXmlParamName();
	// urlMap.put(xmlParamName, xmlParamVal);
	// urlMap.put("url",
	// configProperties.getProperty(rstai.getReportSourceType().getUrlRef())
	// + "/GenerateReportFromRcm");
	// urlMap.put("type", ReportType.COACHING_PLAN_SUMMARY);
	// reportSourceUrls.add(urlMap);
	// } else if (ReportType.LVA_MLL_REPORTS_TYPES.contains(rto.getCode())) {
	//
	// // This works because these are not group reports. We'll have to
	// // pass participants in a different fashion for that
	// String url = configProperties.getProperty("reportserverBaseUrl") +
	// "/GenerateReport";
	// // This works because the code is defined the same as the type
	// String type = rto.getCode();
	//
	// // loop through the ppts, making the urls as we go
	// for (ReportParticipant ppt : ppts.getParticipants()) {
	// // New urlMap needed so we get a new one per ppt
	// urlMap = new HashMap<String, String>();
	// urlMap.put("url", url);
	// urlMap.put("pid", ppt.getParticipantId());
	// urlMap.put("type", type);
	// // Phase ID of 0 is a cue to the report generator to select
	// // the "highest" phase for reporting
	// urlMap.put("phase", "0");
	// reportSourceUrls.add(urlMap);
	// } // End of ppt for loop
	// } else if (rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_SUMMARY)
	// || rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_COACHING)
	// || rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
	// String rptUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/GenerateReportFromRcm";
	// String type = rto.getCode();
	// String rcm;
	// for (ReportParticipant ppt : ppts.getParticipants()) {
	// // New urlMap needed so we get a new one per ppt
	// urlMap = new HashMap<String, String>();
	// rcm = "";
	// String langCode = (ppt.getLangCode() == null) ? rto.getLanguageCode() :
	// ppt.getLangCode();
	// String pptId = ppt.getParticipantId();
	//
	// // get the RCM
	// String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/ViaEdgeRcmServlet";
	// Map<String, String> params = new HashMap<String, String>();
	// params.put("pptId", pptId);
	// params.put("langCode", langCode);
	// params.put("type", rto.getCode());
	// try {
	// rcm = ReportingWizardHelper.getRcm(rcmGenUrl, params);
	// } catch (Exception e) {
	// // TODO: handle exception
	// logger.warn("Unable to generate viaEdge Individual RCM for ppt " +
	// pptId);
	// continue;
	// }
	//
	// // Need to do the rpt URL gen now
	// urlMap.put("url", rptUrl);
	// urlMap.put("type", type);
	// urlMap.put("pid", pptId);
	// urlMap.put("rcm", rcm);
	// reportSourceUrls.add(urlMap);
	// } // End of ppt loop
	// } else if (rto.getCode().equals(ReportType.VIAEDGE_GROUP_FEEDBACK)
	// || rto.getCode().equals(ReportType.VIAEDGE_GROUP_EXTRACT)) {
	// urlMap = new HashMap<String, String>();
	// // Make an xml list of ppts
	// String pptList = reportingService.generationParticipantsXml(ppts);
	//
	// // Make the RCM
	// String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/ViaEdgeRcmServlet";
	// Map<String, String> params = new HashMap<String, String>();
	// params.put("pptList", pptList);
	// params.put("langCode", rto.getLanguageCode());
	// params.put("type", rto.getCode());
	// params.put("pptInfoType", rto.getRptIdNameOpt());
	// String rTitle = rto.getCode().equals(ReportType.VIAEDGE_GROUP_FEEDBACK) ?
	// rto.getRptTitle() : "";
	// params.put("rptTitle", rTitle);
	//
	// String rcm = null;
	// try {
	// rcm = ReportingWizardHelper.getRcm(rcmGenUrl, params);
	// } catch (Exception e) {
	// // TODO: handle exception
	// logger.warn("Unable to generate viaEdge Group RCM");
	// continue;
	// }
	//
	// // Generate the report or extract
	// String proj;
	// if (sessionBean.getProject() == null ||
	// sessionBean.getProject().getProjectId() == 0) {
	// proj = "NA";
	// } else {
	// proj = "" + sessionBean.getProject().getProjectId();
	// }
	//
	// String urlString = configProperties.getProperty("reportserverBaseUrl");
	// urlString += rto.getCode().equals(ReportType.VIAEDGE_GROUP_EXTRACT) ?
	// "/ViaEdgeExtractServlet"
	// : "/GenerateReportFromRcm";
	// urlMap.put("url", urlString);
	// urlMap.put("type", rto.getCode());
	// urlMap.put("proj", proj); // Added for report name
	// urlMap.put("rcm", rcm);
	// reportSourceUrls.add(urlMap);
	// } else if (rto.getCode().equals(ReportType.KFP_NARRATIVE)
	// || rto.getCode().equals(ReportType.KFP_INDIVIDUAL)) {
	// String rptUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/GenerateReportFromRcm";
	// String type = rto.getCode();
	// String rcm;
	// for (ReportParticipant ppt : ppts.getParticipants()) {
	// // New urlMap needed so we get a new one per ppt
	// urlMap = new HashMap<String, String>();
	// rcm = "";
	// String langCode = (ppt.getLangCode() == null) ? rto.getLanguageCode() :
	// ppt.getLangCode();
	// String pptId = ppt.getParticipantId();
	//
	// String strproj;
	// Integer intproj;
	// Integer inttgtRole;
	// String tgtRole;
	// String strtgtRole;
	//
	// if (sessionBean.getProject() == null ||
	// sessionBean.getProject().getProjectId() == 0) {
	// intproj = 0;
	// } else {
	// intproj = sessionBean.getProject().getProjectId();
	// }
	//
	// strproj = "" + intproj;
	//
	// Project proj = projectDao.findById(intproj);
	//
	// // get the TargetRoleIndex for the Project default
	// inttgtRole = proj.getTargetLevelType().getTargetLevelIndex();
	//
	// // get the TargetRoleIndex for the Override if one was
	// // selected
	// tgtRole = rto.getTargetLevelOverride();
	//
	// if (tgtRole != null) {
	// TargetLevelType TLT = targetLevelTypeDao.findByCode(tgtRole);
	//
	// if (TLT != null)
	// inttgtRole = TLT.getTargetLevelIndex();
	// }
	//
	// strtgtRole = "" + inttgtRole;
	//
	// // get the RCM
	// String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/KfapRcmServlet";
	// Map<String, String> params = new HashMap<String, String>();
	// params.put("pid", pptId);
	// params.put("langCode", langCode);
	// params.put("type", rto.getCode());
	// params.put("proj", strproj);
	// params.put("targ", strtgtRole);
	// try {
	// rcm = ReportingWizardHelper.getRcm(rcmGenUrl, params);
	// } catch (Exception e) {
	// // TODO: handle exception
	// logger.warn("Unable to generate KFAP RCM for ppt " + pptId);
	// continue;
	// }
	//
	// // Need to do the rpt URL gen now
	// urlMap.put("url", rptUrl);
	// urlMap.put("type", type);
	// urlMap.put("pid", pptId);
	// urlMap.put("rcm", rcm);
	// reportSourceUrls.add(urlMap);
	// } // End of ppt loop
	// } else {
	// logger.debug("Error processing PALMS report.  Invalid report name = " +
	// rto.getName());
	// continue;
	// }
	// } // End of for loop
	//
	// return;
	// }

	/**
	 * genOxcartUrls - Create a request per ppt per report. Essentially this
	 * takes the aggregation of reports out of the Oxcart realm and brings it to
	 * a common spot (here).
	 * 
	 * @param ppts
	 *            - A List of the select ppts in ParticipantObj objects
	 * @param rstai
	 *            - A ReportSourceTypeArrayItem object containing information
	 *            about the type and a list of reports to be generated
	 * @param reportSourceUrls
	 *            - A list of Maps that contain information about the outgoing
	 *            report requests
	 */
	// MOVED TO REPORT SERVICE HELPER -- admin-ejb
	//
	// private void genOxcartUrls(List<ParticipantObj> ppts,
	// ReportSourceTypeArrayItem rstai,
	// List<Map<String, String>> reportSourceUrls) {
	//
	// Map<String, String> urlMap = null;
	// // loop through the selected reports
	// for (ReportTypeObj rto : rstai.getReportTypes()) {
	// // Create a list of one rto
	// ReportTypeObj[] selectedReportTypes = new ReportTypeObj[1];
	// selectedReportTypes[0] = rto;
	// // handle group report types (single report for multiple ppts
	// // TODO: add these report types in ReportType
	// if (rto.getCode().equals(ReportType.TLT_GROUP_DETAIL_REPORT)
	// || rto.getCode().equals(ReportType.TLT_EXTRACT_REPORT)
	// || rto.getCode().equals(ReportType.TLT_EXTRACT_REPORT_RAW)
	// || rto.getCode().equals(ReportType.ABYD_EXTRACT)
	// || rto.getCode().equals(ReportType.ABYD_ANALYTICS_EXTRACT)
	// || rto.getCode().equals(ReportType.ABYD_RAW_EXTRACT)) {
	// urlMap = new HashMap<String, String>();
	// String paramXml =
	// reportingService.generationReportXml(ReportingWizardHelper.createReportsObject(ppts,
	// selectedReportTypes));
	//
	// // // Testing kludge - replaces generated parameter string with
	// // // a test one
	// // paramXml =
	// //
	// "<Reports><ReportRequest code=\"ABYD_ANALYTICS_EXTRACT\" langCode=\"en\" sortId=\"2\" showName=\"true\" showLci=\"true\" axisLabels=\"dLCI by Potential\"><Participants><Participant participantId=\"367974\" projectId=\"370\"/><Participant participantId=\"367714\" projectId=\"370\"/><Participant participantId=\"367961\" projectId=\"432\"/><Participant participantId=\"368013\" projectId=\"468\"/></Participants></ReportRequest></Reports>";
	// // int kdb = 0;
	//
	// String xmlParamName = rstai.getReportSourceType().getXmlParamName();
	// urlMap.put("url",
	// configProperties.getProperty(rstai.getReportSourceType().getUrlRef()));
	// urlMap.put(xmlParamName, paramXml);
	// reportSourceUrls.add(urlMap);
	// } else {
	// // Handle multiple ppts in individual reports (multiple reports
	// // per invocation possible)
	// // Loop through the ppts
	// for (ParticipantObj ppt : ppts) {
	// urlMap = new HashMap<String, String>();
	// // Create a list of 1 ppt
	// List<ParticipantObj> participants = new ArrayList<ParticipantObj>();
	// participants.add(ppt);
	// String paramXml =
	// reportingService.generationReportXml(ReportingWizardHelper.createReportsObject(
	// participants, selectedReportTypes));
	// String xmlParamName = rstai.getReportSourceType().getXmlParamName();
	// urlMap.put("url",
	// configProperties.getProperty(rstai.getReportSourceType().getUrlRef()));
	// urlMap.put(xmlParamName, paramXml);
	// reportSourceUrls.add(urlMap);
	// } // End of ppt for loop
	// } // End of else on if <group report type>
	// } // End of RTO for loop
	//
	// return;
	// }

	/**
	 * genCoachingExtractUrls - Runs through the list of selected coaching
	 * extract reports and generates the url data for them.
	 * 
	 * @param ppts
	 *            - A List of the select ppts in ParticipantObj objects
	 * @param rstai
	 *            - A ReportSourceTypeArrayItem object containing information
	 *            about the type and a list of reports to be generated
	 * @param reportSourceUrls
	 *            - A list of Maps that contain information about the outgoing
	 *            report requests (effective output)
	 */
	/*
	private void genCoachingExtractUrls(List<ParticipantObj> participants, ReportSourceTypeArrayItem rstai,
			List<Map<String, String>> reportSourceUrls) {
		// single report request - all of the participants are extracted to the
		// single generated report
		Map<String, String> urlMap = new HashMap<String, String>();
		String parmName = rstai.getReportSourceType().getXmlParamName();
		String parmVal = reportingService.generationCoachingPlanDetailedExtractXml(ReportingWizardHelper
				.createCoachingPlanDetailedExtractObject(participants));
		// .createCoachingPlanDetailedExtractObject(participants,
		// selectedReportTypes));
		urlMap.put("url", configProperties.getProperty(rstai.getReportSourceType().getUrlRef())
				+ "/CoachingPlanExtractServlet");

		urlMap.put(parmName, parmVal);
		reportSourceUrls.add(urlMap);

		return;
	}
	*/
	/**
	 * genHybridUrls - Creates URL parameters for Hybrid reports. Actually
	 * creates two separate sets of parameters in the same map with unique key
	 * names that are decoded at the generation of the actual reports to call
	 * the separate reports.
	 * 
	 * @param ppts
	 *            - A List of the select ppts in ParticipantObj objects
	 * @param rstai
	 *            - A ReportSourceTypeArrayItem object containing information
	 *            about the type and a list of reports to be generated
	 * @param reportSourceUrls
	 *            - A list of Maps that contain information about the outgoing
	 *            report requests (effective output)
	 */
	// MOVED TO REPORT SERVICE HELPER -- admin-ejb project
	//
	// private void genHybridUrls(List<ParticipantObj> participants,
	// ReportSourceTypeArrayItem rstai,
	// List<Map<String, String>> reportSourceUrls) {
	// // loop through the requested reports in the PALMS type, generating the
	// // appropriate values in the urlMap
	// Map<String, String> urlMap = null;
	// for (ReportTypeObj rto : rstai.getReportTypes()) {
	// // if (rto.getCode().equals(ReportType.VIAEDGE_TLT_COACHING)
	// // || rto.getCode().equals(ReportType.VIAEDGE_TLT_FEEDBACK)) {
	// if (rto.getCode().equals(ReportType.TLT_LAG_INDIVIDUAL_SUMMARY)) {
	// // Loop through the participants
	// for (ParticipantObj ppt : participants) {
	// urlMap = new HashMap<String, String>();
	// urlMap.put("hybrid", "yes");
	// urlMap.put("hybridType", rto.getCode());
	// urlMap.put("hybridPpt", "" + ppt.getUserId());
	//
	// // Figure out the language to use
	// // Use ppt, but if null use the rpt. RPT has a full list and
	// // no "xxx Default" so that should do it. And make SURE you
	// // use it for both reports
	// String langCode = ppt.getReportLanguageId() == null ?
	// rto.getLanguageCode() : ppt
	// .getReportLanguageId();
	//
	// /*
	// * Set up parms for the viaEDGE report
	// */
	// String prefix = "H2_";
	// // Create a ReportParticipant object
	// ReportParticipant rp = new ReportParticipant(ppt.getUserId().toString(),
	// ppt.getProject()
	// .getProjectId() + "", ppt.getProject().getName());
	// rp.setLangCode(langCode);
	// String pptId = rp.getParticipantId();
	// // // This works so long as there are only two valid types
	// // // (coaching and feedback)
	// // String rcmType =
	// // rto.getCode().equals(ReportType.VIAEDGE_TLT_COACHING) ?
	// // ReportType.VIAEDGE_INDIVIDUAL_COACHING
	// // : ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
	//
	// // This works because the feedback report is the only type
	// // allowed in the TLT/viaEDGE hybrid report
	// // String rcmType = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
	// String rcmType = ReportType.HYBRID_RPT;
	// // Set up to fetch the RCM
	// String rcm = "";
	// String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/ViaEdgeRcmServlet";
	// Map<String, String> params = new HashMap<String, String>();
	// params.put("pptId", pptId);
	// params.put("langCode", rp.getLangCode());
	// params.put("type", rcmType);
	// try {
	// rcm = ReportingWizardHelper.getRcm(rcmGenUrl, params);
	// } catch (Exception e) {
	// // TODO: handle exception
	// logger.warn("Unable to generate viaEdge Individual RCM for ppt " +
	// pptId);
	// continue;
	// }
	//
	// // Now set up the actual report generation parms (part 1)
	// String rptUrl = configProperties.getProperty("reportserverBaseUrl") +
	// "/GenerateReportFromRcm";
	// String type = rcmType;
	//
	// urlMap.put(prefix + "url", rptUrl);
	// urlMap.put(prefix + "type", type);
	// urlMap.put(prefix + "pid", pptId);
	// urlMap.put(prefix + "rcm", rcm);
	// // Dummy argument for the report subtype... only here on
	// // hybrid viaEdge reports
	// urlMap.put(prefix + "hybrid", "hybrid");
	//
	// /*
	// * Set up params for the TLT report
	// */
	// prefix = "H1_";
	// // Create a list of 1 ppt
	// List<ParticipantObj> hppts = new ArrayList<ParticipantObj>();
	// hppts.add(ppt);
	// // Create a list of one report type, modified to be a TLT
	// // Individual Summary report
	// ReportTypeObj newRto = new ReportTypeObj(rto);
	// newRto.setName(""); // wipe the hybrid name
	// newRto.setCode(ReportType.TLT_INDIVIDUAL_SUMMARY_REPORT);
	// newRto.setLanguageCode(langCode);
	// ReportTypeObj[] selectedReportTypes = new ReportTypeObj[1];
	// selectedReportTypes[0] = newRto;
	// // Create the Reports object with the single ReportRequest
	// // object in it. This is the TLT report that needs its
	// // visual aspects changed, so set the combinedReport flag
	// Reports rpts = ReportingWizardHelper.createReportsObject(hppts,
	// selectedReportTypes);
	// rpts.getReportRequests().get(0).setHybridReport("YES");
	// String oxcartXml = reportingService.generationReportXml(rpts);
	//
	// urlMap.put(prefix + "xml", oxcartXml);
	// urlMap.put(prefix + "url",
	// configProperties.getProperty("reportserverBaseUrlOxcart"));
	// // Fix above so following works (2nd half of the url ref)
	// //
	// configProperties.getProperty(rstai.getReportSourceType().getUrlRef()));
	//
	// reportSourceUrls.add(urlMap);
	// } // End of participant loop
	// } // End if code is VIAEDGE_TLT_COACHING or VIAEDGE_TLT_FEEDBACK
	// } // end of report types loop
	//
	// return;
	// }

	/**
	 * Private class used in the report generation logic
	 */
	// MOVED TO REPORT SERVICE HELPER -- admin-ejb
	//
	// private class ReportSourceTypeArrayItem {
	// private final List<ReportTypeObj> reportTypes;
	// private final ReportSourceType reportSourceType;
	//
	// public ReportSourceTypeArrayItem(ReportSourceType reportSourceType) {
	// this.reportSourceType = reportSourceType;
	// this.reportTypes = new ArrayList<ReportTypeObj>();
	// }
	//
	// public List<ReportTypeObj> getReportTypes() {
	// return reportTypes;
	// }
	//
	// // public void setReportTypes(List<ReportTypeObj> reportTypes) {
	// // this.reportTypes = reportTypes;
	// // }
	//
	// public ReportSourceType getReportSourceType() {
	// return reportSourceType;
	// }
	//
	// // public void setReportSourceType(ReportSourceType reportSourceType) {
	// // this.reportSourceType = reportSourceType;
	// // }
	//
	// public void addReportType(ReportTypeObj rto) {
	// this.reportTypes.add(rto);
	// }
	// }

	/**
	 * END REPORT SOURCE REFACTOR
	 */

	/**
	 * Old school generate of Oxcart reports - Only called from
	 * handleGenerateReports
	 * 
	 * @param event
	 */
	/*
	private void generateOxcartReports(ActionEvent event) {
		List<ParticipantObj> participants = reportWizardViewBean.getParticipants();
		ReportTypeObj[] selectedReportTypes = reportWizardViewBean.getSelectedReportTypes();

		StringBuffer detail = new StringBuffer();

		detail.append(selectedReportTypes.length + " reports selected. ");
		for (ParticipantObj p : participants) {
			detail.append(p.getUserId() + " selected lang " + p.getReportLanguageId() + ". ");
		}

		Reports reports = ReportingWizardHelper.createReportsObject(participants, selectedReportTypes);
		String xml = reportingService.generationReportXml(reports);
		reportWizardViewBean.setReportXml(xml);

		// this works but PrimeFaces doesn't escape xml entities < and >
		// properly right out of the box
		RequestContext ctx = RequestContext.getCurrentInstance();
		ctx.addCallbackParam("reportXml", Utils.escapeRequestContextCallbackParam(xml));
		ctx.addCallbackParam("reportingUrl", abydProperties.getProperty("reportingUrl"));

		logger.trace("Generated report xml:\n\n {} \n End Generated Report Xml\n", xml);
		logger.debug("generateOxcartReports {}", detail.toString());
	}
	*/
	public void handleSelectReportType(SelectEvent event) {
		this.updateTabAndButtonState();
	}

	public void handleUnselectReportType(UnselectEvent event) {
		this.updateTabAndButtonState();
	}

	public void handleReportNameChange() {
		this.updateTabAndButtonState();
	}

	private void updateTabAndButtonState() {

		reportWizardViewBean.setDisableParticipantLanguageTab(reportWizardViewBean
				.isParticipantReportLanguageDisabled());

		if (reportWizardViewBean.isDisableParticipantLanguageTab()) {
			RequestContext.getCurrentInstance().execute("wizardTabView.disable(1);");
		} else {
			RequestContext.getCurrentInstance().execute("wizardTabView.enable(1);");
		}

		logger.trace("participant language tab disabled = {}", reportWizardViewBean.isDisableParticipantLanguageTab());

		// reportWizardViewBean.setDisableDownloadButton(reportWizardViewBean.isCreateReportsDisabled());

	}

	public void handleSelectReportGlobalLanguageId(ValueChangeEvent event) {

	}

	public void handleRequestReports() {

		if (!validateCreateReports()) {
			return;
		}

		logger.debug("Handling report requests.");
		GenerateReportsRequest gReportRequest = new GenerateReportsRequest();
		if (!(reportWizardViewBean.getParticipants() == null) && reportWizardViewBean.getParticipants().size() > 1) {
			gReportRequest.setPriority(ReportGenerateRequest.REQUEST_PRIORITY_LOW);
		} else {
			// Single reports go high priority per the business requirements.
			gReportRequest.setPriority(ReportGenerateRequest.REQUEST_PRIORITY_HIGH);
		}
		Subject subject = SecurityUtils.getSubject();
		gReportRequest.setPrincipal(subject.getPrincipal().toString());
		gReportRequest.setSubjectId(sessionBean.getRoleContext().getSubject().getSubject_id());
		gReportRequest.setRequestName(reportWizardViewBean.getRequestName());
		List<ParticipantObj> pptList = reportWizardViewBean.getParticipants();
		List<ReportParticipantRepresentation> reportPptList = new ArrayList<ReportParticipantRepresentation>();
		for (ParticipantObj ppt : pptList) {
			ReportParticipantRepresentation pptRep = new ReportParticipantRepresentation();
			pptRep.setEmail(ppt.getEmail());
			pptRep.setFirstName(ppt.getFirstName());
			pptRep.setLastName(ppt.getLastName());
			pptRep.setReportLang(ppt.getReportLanguageId());
			logger.debug("ppt language id: {}", ppt.getReportLanguageId());
			pptRep.setUsersId(ppt.getUserId());
			pptRep.setProjectId(ppt.getProject().getProjectId());
			pptRep.setCourseVersionMap(ppt.courseVersionsToString());
			logger.debug("ppt course versions: {}", ppt.courseVersionsToString());
			reportPptList.add(pptRep);
		}
		List<ReportTypeObj> rptTypeObjArray = reportWizardViewBean.getSelectedReportTypes();
		List<ReportRepresentation> reportRepList = new ArrayList<ReportRepresentation>();
		for (ReportTypeObj obj : rptTypeObjArray) {
			ReportRepresentation reportRep = new ReportRepresentation();
			reportRep.setCreateType(ReportRequest.CREATE_TYPE_REQUEST);
			reportRep.setCode(obj.getCode());
			reportRep.setContentType(reportingSingleton.getReportType(obj.getCode()).getContentType());
			reportRep.setReportIdNameOption(obj.getRptIdNameOpt());
			reportRep.setReportLang(obj.getLanguageCode());
			reportRep.setTargetLevel(obj.getTargetLevelOverride());
			reportRep.setReportName(obj.getName());
			reportRep.setTitle(obj.getRptTitle());
			reportRepList.add(reportRep);
		}
		gReportRequest.setParticipants(reportPptList);
		gReportRequest.setReportTypeList(reportRepList);

		ReportRequestResponseList response = reportRequestHelper.submitReportsRequest(gReportRequest);
		reportWizardViewBean.setReportRequestId(response.getReportRequestId());
		reportWizardViewBean.setResponseList(response.getReportRequestResponseList());
	}

	public void handleFlow(FlowEvent event) {
		List<ReportTypeObj> selectedReportTypes = reportWizardViewBean.getSelectedReportTypes();
		StringBuffer detail = new StringBuffer();
		detail.append(selectedReportTypes.size() + " reports selected. ");
		logger.debug("handleFlow {}", detail.toString());
		// context.addMessage(null, new
		// FacesMessage("Report generation output: " + detail,
		// detail.toString()));
	}

	private List<ReportTypeObj> getReportTypeObjListByParticipantProjectTypes() {
		List<Integer> projectTypeIds = new ArrayList<Integer>();
		for (ParticipantObj participantObj : reportWizardViewBean.getParticipants()) {
			Integer projectTypeId = participantObj.getProject().getTypeId();
			if (!projectTypeIds.contains(projectTypeId)) {
				projectTypeIds.add(projectTypeId);
			}
		}
		return getReportTypeObjsFromList(reportTypeDao.findReportTypesByProjectTypeIds(projectTypeIds));
	}

	private List<ReportTypeObj> getReportTypeObjListByProjectType(ProjectType projectType) {
		List<ReportType> reportTypeList = new ArrayList<ReportType>();
		List<ProjectTypeReportType> projectTypeReportTypes = projectType.getProjectTypeReportTypes();
		for (ProjectTypeReportType ptrt : projectTypeReportTypes) {
			reportTypeList.add(ptrt.getReportType());
		}
		return getReportTypeObjsFromList(reportTypeList);
	}

	private List<ReportTypeObj> getReportTypeObjsFromList(List<ReportType> reportTypeList) {
		Subject subject = SecurityUtils.getSubject();
		List<ReportTypeObj> reportTypeObjList = new ArrayList<ReportTypeObj>();
		for (ReportType reportType : reportTypeList) {
			ReportTypeObj reportTypeObj = new ReportTypeObj(reportType.getName(), reportType.getCode(), false,
					reportType.getReportLanguageVisible().booleanValue(), reportType.getParticipantLanguageVisible()
							.booleanValue());

			// fill in target levels
			if (reportType.getTargetLevelGroup() != null) {
				// Put in the project default entry for all EXCEPT those project
				// types that don't have a project default
				// TODO Data drive the presence/absence of the project default
				// selection
				Project project = sessionBean.getProject();
				if (!(project == null)
						&& !project.getProjectType().getProjectTypeCode().equals(ProjectType.ASSESSMENT_TESTING)) {
					// Put in a project default.
					TargetLevelTypeObj tlto = new TargetLevelTypeObj(0, "Project Default", "");
					reportTypeObj.getTargetLevelTypes().add(tlto);
				}
				for (TargetLevelGroupType targetLevelGroupType : reportType.getTargetLevelGroup()
						.getTargetLevelGroupTypes()) {
					TargetLevelType targetLevelType = targetLevelGroupType.getTargetLevelType();
					TargetLevelTypeObj tlto = new TargetLevelTypeObj(targetLevelType.getTargetLevelTypeId(),
							targetLevelType.getName(), targetLevelType.getCode());
					reportTypeObj.getTargetLevelTypes().add(tlto);
				}
			}

			reportTypeObj.setReportSourceTypeId(reportType.getReportSourceType().getReportSourceTypeId());
			if (subject.isPermitted("generateAnyReport") || subject.isPermitted(reportTypeObj.getCode())) {
				reportTypeObjList.add(reportTypeObj);
			}

			// Get default language from client/company
			String rptLang = RPT_DFLT_LANG; // Set it to default
			if (sessionBean.getCompany() != null && sessionBean.getCompany().getLangPref() != null) {
				rptLang = sessionBean.getCompany().getLangPref();
			}
			reportTypeObj.setLanguageCode(rptLang);

			// See if there are other options
			// The first"report option"
			if (reportType.getCode().equals(ReportType.KFP_SLATE)
					|| reportType.getCode().equals(ReportType.ALP_TALENT_GRID_V2)) {
				reportTypeObj.setRptTitleVisible(true);

				reportTypeObj.setRptTitle(""); // defaults to empty string
			}
			if (reportType.getCode().equals(ReportType.VIAEDGE_GROUP_FEEDBACK)) {

				reportTypeObj.setRptIdNameOptVisible(true);
				reportTypeObj.setRptTitleVisible(true);
				reportTypeObj.setRptIdNameOpt(ReportOptionsObj.RO_NAMES);
				// create the list of options
				reportTypeObj.getRptIdNameList().add(
						new ReportOptionsObj(ReportOptionsObj.RO_NAMES, "Display Participant Names"));
				reportTypeObj.getRptIdNameList().add(
						new ReportOptionsObj(ReportOptionsObj.RO_IDS, "Display Participant IDs"));

				reportTypeObj.setRptTitle(""); // defaults to empty string
			}
			if (reportType.getCode().equals(ReportType.VIAEDGE_GROUP_EXTRACT)) {

				reportTypeObj.setRptIdNameOptVisible(true); // controls ppt opts
				reportTypeObj.setRptIdNameOpt(ReportOptionsObj.RO_NAMES);
				// create the list of options
				reportTypeObj.getRptIdNameList().add(
						new ReportOptionsObj(ReportOptionsObj.RO_NAMES, "Display Participant Names"));
				reportTypeObj.getRptIdNameList().add(
						new ReportOptionsObj(ReportOptionsObj.RO_IDS, "Display Participant IDs"));
			}
			if (reportType.getCode().equals(ReportType.ABYD_ANALYTICS_EXTRACT)) {
				reportTypeObj.setAnalyticsNameOptVisible(true);
				reportTypeObj.setAnalyticsShowLciOptVisible(true);
				reportTypeObj.setAnalyticsLabelsOptVisible(true);
				reportTypeObj.setAnalyticsSortOptVisible(true);
				reportTypeObj.setAnalyticsLabelsOpt(ReportOptionsObj.RO_POT_READI);
				// create the list of options, the value = the display name
				reportTypeObj.getAnalytics16bLabelsList().add(
						new ReportOptionsObj(ReportOptionsObj.RO_POT_READI, ReportOptionsObj.RO_POT_READI));
				reportTypeObj.getAnalytics16bLabelsList().add(
						new ReportOptionsObj(ReportOptionsObj.RO_DLCI_POT, ReportOptionsObj.RO_DLCI_POT));
				// get the sort stuff and build out the supporting data
				// structure
				String sortXml = portalServletService.portalServletRequest(SORT_DATA_REQUEST);
				SortInfo si = (SortInfo) JaxbUtil.unmarshal(sortXml, SortInfo.class);
				setUpSortStruct(reportTypeObj, si);
				// set default (1st group, first item)
				String grpKey = reportTypeObj.getAnalyticsSortGroups().get(0).getCode();
				reportTypeObj.setCurSortGroupId(grpKey);
				reportTypeObj.setAnalyticsSortItems(reportTypeObj.getSortOptions().get(grpKey));
				reportTypeObj.setAnalyticsSortIdOpt(reportTypeObj.getAnalyticsSortItems().get(0).getCode());
			}
		}
		return reportTypeObjList;
	}

	private void setUpSortStruct(ReportTypeObj rto, SortInfo si) {
		rto.getAnalyticsSortGroups().clear();
		// loop through the data making maps
		for (SortGroup sg : si.getSortGroup()) {
			// get the info and then loop through its sort items
			String id = sg.getId();
			rto.getAnalyticsSortGroups().add(new ReportOptionsObj(id, sg.getDisplayName()));

			// OK, loop through the sort items and make a table for each
			ArrayList<ReportOptionsObj> list = new ArrayList<ReportOptionsObj>();
			for (SortItem itm : sg.getSorts()) {
				list.add(new ReportOptionsObj(itm.getId(), itm.getDisplayName()));
			}
			rto.getSortOptions().put(id, list);
		}

		return;
	}

	private boolean validateCreateReports() {
		boolean valid = true;

		if (reportWizardViewBean.getRequestName().isEmpty()) {
			LabelUtils.addErrorMessage("wizardTabView", "Request Name:", "Please enter Request Name.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		if (reportWizardViewBean.getSelectedReportTypes().isEmpty()) {
			LabelUtils.addErrorMessage("wizardTabView", "Select Report:", "Please select at least one report.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		return valid;
	}

	public Boolean getPopulatedUI() {
		return populatedUI;
	}

	public void setPopulatedUI(Boolean populatedUI) {
		this.populatedUI = populatedUI;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao clientDao) {
		this.companyDao = clientDao;
	}

	public ReportTypeDao getReportTypeDao() {
		return reportTypeDao;
	}

	public void setReportTypeDao(ReportTypeDao reportTypeDao) {
		this.reportTypeDao = reportTypeDao;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ReportWizardViewBean getReportWizardViewBean() {
		return reportWizardViewBean;
	}

	public void setReportWizardViewBean(ReportWizardViewBean reportWizardViewBean) {
		this.reportWizardViewBean = reportWizardViewBean;
	}

	public void setProjectManagementViewBean(ProjectManagementViewBean projectManagementViewBean) {
		this.projectManagementViewBean = projectManagementViewBean;
	}

	public ProjectManagementViewBean getProjectManagementViewBean() {
		return projectManagementViewBean;
	}

	public ParticipantClipboardViewBean getParticipantClipboardViewBean() {
		return participantClipboardViewBean;
	}

	public void setParticipantClipboardViewBean(ParticipantClipboardViewBean participantClipboardViewBean) {
		this.participantClipboardViewBean = participantClipboardViewBean;
	}

	public ReportingServiceLocal getReportingService() {
		return reportingService;
	}

	public void setReportingService(ReportingServiceLocal reportingService) {
		this.reportingService = reportingService;
	}

	public Properties getAbydProperties() {
		return abydProperties;
	}

	public void setAbydProperties(Properties abydProperties) {
		this.abydProperties = abydProperties;
	}

	public ParticipantListViewBean getParticipantListViewBean() {
		return participantListViewBean;
	}

	public void setParticipantListViewBean(ParticipantListViewBean participantListViewBean) {
		this.participantListViewBean = participantListViewBean;
	}

	public ReportRequestHelper getReportRequestHelper() {
		return reportRequestHelper;
	}

	public void setReportRequestHelper(ReportRequestHelper reportRequestHelper) {
		this.reportRequestHelper = reportRequestHelper;
	}

	public ReportWizardHelper getReportWizardHelper() {
		return reportWizardHelper;
	}

	public void setReportWizardHelper(ReportWizardHelper reportWizardHelper) {
		this.reportWizardHelper = reportWizardHelper;
	}

}
