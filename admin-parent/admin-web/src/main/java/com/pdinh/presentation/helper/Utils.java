package com.pdinh.presentation.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;

public class Utils {

	private static String lCaseChars = "abcdefghjkmnpqrstuvwxyz";
	private static String uCaseChars = "ABCDEFGHJKMNPQRSTUVWXYZ";
	private static String numbers = "23456789";

	public static String rowsPerPageTemplate(List<?> list) {

		if (list == null) {
			return "0";
		}

		if (list.size() <= 5) {
			return String.valueOf(list.size());
		} else if (list.size() <= 10) {
			return "5," + String.valueOf(list.size());
		} else if (list.size() <= 15) {
			return "5,10," + String.valueOf(list.size());
		} else if (list.size() <= 20) {
			return "5,10,15," + String.valueOf(list.size());
		} else if (list.size() <= 25) {
			return "5,10,15,20," + String.valueOf(list.size());
		} else if (list.size() <= 30) {
			return "5,10,15,20,25," + String.valueOf(list.size());
		} else if (list.size() <= 50) {
			return "5,10,15,20,25,30," + String.valueOf(list.size());
		} else if (list.size() <= 100) {
			return "5,10,15,20,25,30,50," + String.valueOf(list.size());
		} else {
			return "5,10,15,20,25,30,50,100," + String.valueOf(list.size());
		}
	}

	public static String getNewPassword() {
		SecureRandom generator = new SecureRandom();
		String pw = "";
		int index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		index = generator.nextInt(numbers.length());
		pw += numbers.charAt(index);

		index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		index = generator.nextInt(uCaseChars.length());
		pw += uCaseChars.charAt(index);

		index = generator.nextInt(numbers.length());
		pw += numbers.charAt(index);

		index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		index = generator.nextInt(uCaseChars.length());
		pw += uCaseChars.charAt(index);

		index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		return pw;
	}

	public static String getServerAndContextPath() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String serverContextPath = request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + request.getContextPath();
		return serverContextPath;
	}

	public static String escapeRequestContextCallbackParam(String string) {
		return string.replace("<", "&lt;").replace(">", "&gt;");
	}

	// null-safe trim
	public static String trim(String s) {
		if (s != null)
			s = s.trim();
		return s;
	}

	public static boolean validatePassword(String password) {
		Pattern p;
		Matcher m;
		int testsPassed = 0;
		int testsRequired = 3;
		if (password.length() < 8) {
			return false;
		}
		if (password.length() > 50) {
			return false;
		}
		p = Pattern.compile(".??[A-Z]");
		m = p.matcher(password);
		while (m.find()) {
			testsPassed++;
			break;
		}
		p = Pattern.compile(".??[a-z]");
		m = p.matcher(password);
		while (m.find()) {
			testsPassed++;
			break;
		}
		p = Pattern.compile(".??[0-9]");
		m = p.matcher(password);
		while (m.find()) {
			testsPassed++;
			break;
		}
		if (testsPassed >= testsRequired) {
			return true;
		}
		return false;
	}

	public static boolean isEmptyString(String str) {
		if (str == null) {
			return true;
		}

		if (str.trim().equals("")) {
			return true;
		}

		return false;
	}

	public static byte[] generateHashedPassword(String password, String salt) {
		byte[] hashbyte = (password + salt).getBytes();
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update(hashbyte);
			return digest.digest();

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public static String convertNullToEmptyString(String s) {
		if (s == null) {
			return "";
		}
		return s;
	}

	@SuppressWarnings("deprecation")
	public static int getDateDiffPercent(Date startDate, Date endDate, Date currentDate) {
		Float pct = (float) 0.0;
		int pctInt;
		int elapsed = 0;
		int length = 0;

		GregorianCalendar g1, g2, g3, gc1, gc2, gc3, gce1;
		g1 = new GregorianCalendar(startDate.getYear(), startDate.getMonth(), startDate.getDate());
		g2 = new GregorianCalendar(endDate.getYear(), endDate.getMonth(), endDate.getDate());
		g3 = new GregorianCalendar(currentDate.getYear(), currentDate.getMonth(), currentDate.getDate());
		if (g2.after(g1)) {
			gc2 = (GregorianCalendar) g2.clone();
			gc1 = (GregorianCalendar) g1.clone();
		} else {
			gc2 = (GregorianCalendar) g1.clone();
			gc1 = (GregorianCalendar) g2.clone();
		}
		gc1.clear(Calendar.MILLISECOND);
		gc1.clear(Calendar.SECOND);
		gc1.clear(Calendar.MINUTE);
		gc1.clear(Calendar.HOUR_OF_DAY);
		gc2.clear(Calendar.MILLISECOND);
		gc2.clear(Calendar.SECOND);
		gc2.clear(Calendar.MINUTE);
		gc2.clear(Calendar.HOUR_OF_DAY);
		while (gc1.before(gc2)) {
			gc1.add(Calendar.DATE, 1);
			length++;
		}
		if (g3.after(g1)) {
			gc3 = (GregorianCalendar) g3.clone();
			gce1 = (GregorianCalendar) g1.clone();
		} else {
			gc3 = (GregorianCalendar) g1.clone();
			gce1 = (GregorianCalendar) g3.clone();
		}
		gc3.clear(Calendar.MILLISECOND);
		gc3.clear(Calendar.SECOND);
		gc3.clear(Calendar.MINUTE);
		gc3.clear(Calendar.HOUR_OF_DAY);
		while (gce1.before(gc3)) {
			gce1.add(Calendar.DATE, 1);
			elapsed++;
		}
		pct = (elapsed * 100.0f / length);
		pctInt = pct.intValue();
		if (pctInt > 100) {
			pctInt = 100;
		}
		if (pctInt < 0) {
			pctInt = 0;
		}

		return pctInt;
	}

	public static boolean validateEmailAddress(String emailAddress) {
		Pattern pattern = Pattern
				.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@+[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher matcher = pattern.matcher(emailAddress);
		return matcher.matches();
	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public static File convertInputStreamToFile(InputStream in, String filename, String tempLocation)
			throws IOException {
		File tempRoot = getTempRootDir(tempLocation);
		File tempFile = new File(tempRoot, filename);

		try {
			FileOutputStream out = new FileOutputStream(tempFile);
			IOUtils.copy(in, out);
		} catch (Exception e) {
			System.err.println("Failed to convert InputStream to File :" + e.getMessage());
		} finally {
			// tempFile.deleteOnExit();
			// tempRoot.deleteOnExit();
		}

		return tempFile;
	}

	private static File getTempRootDir(String targetDirectory) {
		File filesRoot = new File(targetDirectory);
		File filesTemp = new File(filesRoot, String.valueOf((new Date()).getTime()));

		if (!filesTemp.exists()) {
			filesTemp.mkdirs();
		}
		return filesTemp;
	}
}
