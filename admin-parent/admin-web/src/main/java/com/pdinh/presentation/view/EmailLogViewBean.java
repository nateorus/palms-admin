package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailLogObj;
import com.pdinh.presentation.domain.EmailTextObj;

@ManagedBean
@ViewScoped
public class EmailLogViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<EmailLogObj> log;
	private List<EmailLogObj> filteredRecords;
	private EmailLogObj selectedRecord;
	private boolean disabledResendButton = true;
	private EmailTextObj selectedEmailText;

	public boolean isDisabledResendButton() {
		return disabledResendButton;
	}

	public void setDisabledResendButton(boolean disabledResendButton) {
		this.disabledResendButton = disabledResendButton;
	}

	public EmailLogObj getSelectedRecord() {
		return selectedRecord;
	}

	public void setSelectedRecord(EmailLogObj selectedRecord) {
		this.selectedRecord = selectedRecord;
	}

	public EmailTextObj getSelectedEmailText() {
		return selectedEmailText;
	}

	public void setSelectedEmailText(EmailTextObj selectedEmailText) {
		this.selectedEmailText = selectedEmailText;
	}

	public List<EmailLogObj> getLog() {
		return log;
	}

	public void setLog(List<EmailLogObj> log) {
		this.log = log;
	}

	public List<EmailLogObj> getFilteredRecords() {
		return filteredRecords;
	}

	public void setFilteredRecords(List<EmailLogObj> filteredRecords) {
		this.filteredRecords = filteredRecords;
	}
}
