package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;

@ManagedBean(name = "coachingPlanInfoViewBean")
@ViewScoped
public class CoachingPlanInfoViewBean implements Serializable {
	private static final long serialVersionUID = 1L;
	// Setting up Coach and TimeFrame Dialogue

	//private List<ParticipantObj> coaches;
	private List<ExternalSubjectDataObj> coachSubjects;
	
	//private ParticipantObj selectedCoach;
	private ExternalSubjectDataObj selectedCoachObj;

	private Date startDate;
	private Date endDate;

	private String typeText;
	private Integer targetLevelTypeId = null;
	private List<TargetLevelTypeObj> targetLevelTypes;

	private boolean disableSave;
	private boolean isCoachingProject;
	
	private boolean selectedCoachAuthorized;

	private String printReportUrl = "";
/*
	public List<ParticipantObj> getCoaches() {
		return coaches;
	}

	public void setCoaches(List<ParticipantObj> coaches) {
		this.coaches = coaches;
	}

	public ParticipantObj getSelectedCoach() {
		return selectedCoach;
	}

	public void setSelectedCoach(ParticipantObj selectedCoach) {
		this.selectedCoach = selectedCoach;
	}
*/
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getTypeText() {
		return typeText;
	}

	public void setTypeText(String typeText) {
		this.typeText = typeText;
	}

	public Integer getTargetLevelTypeId() {
		return targetLevelTypeId;
	}

	public void setTargetLevelTypeId(Integer targetLevelTypeId) {
		this.targetLevelTypeId = targetLevelTypeId;
	}

	public List<TargetLevelTypeObj> getTargetLevelTypes() {
		return targetLevelTypes;
	}

	public void setTargetLevelTypes(List<TargetLevelTypeObj> targetLevelTypes) {
		this.targetLevelTypes = targetLevelTypes;
	}

	public boolean getDisableSave() {
		return disableSave;
	}

	public void setDisableSave(boolean disableSave) {
		this.disableSave = disableSave;
	}

	public boolean getIsCoachingProject() {
		return isCoachingProject;
	}

	public void setIsCoachingProject(boolean isCoachingProject) {
		this.isCoachingProject = isCoachingProject;
	}

	public String getPrintReportUrl() {
		return printReportUrl;
	}

	public void setPrintReportUrl(String printReportUrl) {
		this.printReportUrl = printReportUrl;
	}

	public List<ExternalSubjectDataObj> getCoachSubjects() {
		return coachSubjects;
	}

	public void setCoachSubjects(List<ExternalSubjectDataObj> coachSubjects) {
		this.coachSubjects = coachSubjects;
	}

	public ExternalSubjectDataObj getSelectedCoachObj() {
		return selectedCoachObj;
	}

	public void setSelectedCoachObj(ExternalSubjectDataObj selectedCoachObj) {
		this.selectedCoachObj = selectedCoachObj;
	}

	public boolean isSelectedCoachAuthorized() {
		return selectedCoachAuthorized;
	}

	public void setSelectedCoachAuthorized(boolean selectedCoachAuthorized) {
		this.selectedCoachAuthorized = selectedCoachAuthorized;
	}

}
