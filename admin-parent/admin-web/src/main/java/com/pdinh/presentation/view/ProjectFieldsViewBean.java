package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.view.FieldsViewBean;

@ManagedBean
@SessionScoped
public class ProjectFieldsViewBean extends FieldsViewBean<FieldDto> implements Serializable {

	private static final long serialVersionUID = 1124027567371576410L;

	private List<FieldDto> deletedFields;

	public ProjectFieldsViewBean() {
		init();
	}

	@Override
	public void init() {
		super.init();
		super.setField(new FieldDto());
		deletedFields = new ArrayList<FieldDto>();
	}

	public CustomFieldDto findFieldByCustomFieldId(int id) {
		for (FieldDto fieldDto : this.getFields()) {
			if (fieldDto.isCustomField()) {
				CustomFieldDto customFieldDto = (CustomFieldDto) fieldDto;
				if (customFieldDto.getId() == id) {
					return customFieldDto;
				}
			}
		}
		return null;
	}

	public void addDeletedField(FieldDto field) {
		if (field != null) {
			deletedFields.add(field);
		}
	}

	public void clearDeletedFields() {
		deletedFields.clear();
	}

	public boolean hasDeletedFields() {
		if (deletedFields.size() > 0) {
			return true;
		}
		return false;
	}

	public List<FieldDto> getDeletedFields() {
		return deletedFields;
	}
}
