package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.dto.StandardFieldDto;
import com.kf.uffda.persistence.CoreField;
import com.kf.uffda.persistence.FieldEntityType;
import com.kf.uffda.service.UffdaService;
import com.kf.uffda.utils.TextUtils;
import com.kf.uffda.vo.CoreFieldGroupVo;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.presentation.managedbean.LanguageBean;
import com.pdinh.presentation.view.ProjectFieldsViewBean;
import com.pdinh.presentation.view.ProjectSetupViewBean;

@ManagedBean(name = "projectFieldsControllerBean")
@ViewScoped
public class ProjectFieldsControllerBean extends ControllerBeanBase implements Serializable {

	private static final long serialVersionUID = 7038594041171742033L;

	private static final Logger log = LoggerFactory.getLogger(ProjectFieldsControllerBean.class);

	private FieldDto selectedField = null;

	@EJB
	UffdaService uffdaService;

	@ManagedProperty(name = "languageBean", value = "#{languageBean}")
	private LanguageBean languageBean;

	@ManagedProperty(name = "projectSetupViewBean", value = "#{projectSetupViewBean}")
	private ProjectSetupViewBean projectSetupViewBean;

	@ManagedProperty(name = "projectFieldsViewBean", value = "#{projectFieldsViewBean}")
	private ProjectFieldsViewBean viewBean;

	public ProjectFieldsControllerBean() {
		super("frmProjectLogisticsSetup");
	}

	@Override
	public void initView(ComponentSystemEvent event) {
		if (isFirstTime()) {
			setFirstTime(false);

			if (!viewBean.hasLanguages()) {
				viewBean.addLanguages(languageBean.getLanguageListForInternal());
			}

			if (!viewBean.hasFields()) {
				viewBean.addFields(uffdaService.getStandardFields(sessionBean.getCompany().getCompanyId(),
						CoreFieldGroupVo.PALMS, sessionBean.getProject().getProjectId(),
						FieldEntityType.PALMS_PROJECT_USERS, true, true, false, sessionBean.getProject()
								.isIncludeClientLogonIdPassword()));
			}
		}
	}

	public void handleSelectLanguage() {
		for (FieldDto field : viewBean.getFields()) {
			field.setSelectedLanguageCode(viewBean.getLanguageCode());
		}
	}

	public void handleAddField(ActionEvent event) {
	}

	public void handleEditField(ActionEvent event) {
	}

	public void saveChangedField(AjaxBehaviorEvent event) {
		if (event != null) {
			Integer fieldId = (Integer) event.getComponent().getAttributes().get("fieldId");
			if (fieldId != null) {
				selectedField = viewBean.findFieldById(fieldId);
			}
		}
	}

	public void saveSelectedField(ActionEvent event) {
		selectedField = getField(event);
	}

	public void handleRevertRequiredToAnswer(ActionEvent event) {
		if (selectedField != null) {
			selectedField.setRequired(!selectedField.isRequired());
		}
		selectedField = null;
	}

	public void handleUpdateRequiredToAnswer(ActionEvent event) {
		if (selectedField != null) {
			selectedField.setChanged(true);

			String whatChanged = TextUtils.getChangedMessage(formName, "Required", selectedField.isOriginalRequired(),
					selectedField.isRequired());

			EntityAuditLogger.auditLog.info(
					"{} performed a {} for Internal Custom Field [{}] for project {} for client {}.  {}", new Object[] {
							getUsername(), EntityAuditLogger.OP_UPDATE_FIELD, selectedField.getName(),
							projectSetupViewBean.getProjectName(), getCompanyName(), whatChanged });
		}
		viewBean.setClientSideValueChanged(true);
		selectedField = null;
	}

	public void handleRevertImportTemplateChange(ActionEvent event) {
		if (selectedField != null) {
			selectedField.setIncludeInImportTemplate(!selectedField.isIncludeInImportTemplate());
		}
		selectedField = null;
	}

	public void handleUpdateImportTemplateChange(ActionEvent event) {
		if (selectedField != null) {
			selectedField.setChanged(true);
			String whatChanged = TextUtils.getChangedMessage(formName, "Include in Template",
					selectedField.isOriginalIncludeInImportTemplate(), selectedField.isIncludeInImportTemplate());

			EntityAuditLogger.auditLog.info(
					"{} performed a {} for Internal Custom Field [{}] for project {} for client {}.  {}", new Object[] {
							getUsername(), EntityAuditLogger.OP_UPDATE_FIELD, selectedField.getName(),
							projectSetupViewBean.getProjectName(), getCompanyName(), whatChanged });
		}
		viewBean.setClientSideValueChanged(true);
		selectedField = null;
	}

	public void saveRemoveField(ActionEvent event) {
		selectedField = getField(event);
	}

	public void handleRemoveField() {
		if (selectedField != null) {
			viewBean.getFields().remove(selectedField);
			viewBean.addDeletedField(selectedField);

			EntityAuditLogger.auditLog.info("{} performed a {} for Custom Field [{}] for project {} for client {}.",
					new Object[] { getUsername(), EntityAuditLogger.OP_DELETE_FIELD, selectedField.getName(),
							projectSetupViewBean.getProjectName(), getCompanyName() });
		}
		viewBean.setClientSideValueChanged(true);
		selectedField = null;
	}

	public void handleRemoveLogonFields() {
		FieldDto logonField = viewBean.findFieldByName(CoreField.LOGON_ID);
		if (logonField != null) {
			viewBean.getFields().remove(logonField);
		}

		FieldDto passwordField = viewBean.findFieldByName(CoreField.PASSWORD);
		if (passwordField != null) {
			viewBean.getFields().remove(passwordField);
		}

		FieldDto updateLogonFields = viewBean.findFieldByName(CoreField.UPDATE_LOGON_FIELDS);
		if (updateLogonFields != null) {
			viewBean.getFields().remove(updateLogonFields);
		}
	}

	public void handleAddLogonFields(int projectId) {
		FieldDto logonField = viewBean.findFieldByName(CoreField.LOGON_ID);
		FieldDto passwordField = viewBean.findFieldByName(CoreField.PASSWORD);
		FieldDto updateLogonFields = viewBean.findFieldByName(CoreField.UPDATE_LOGON_FIELDS);

		if (logonField == null || passwordField == null || updateLogonFields == null) {
			List<FieldDto> fields = uffdaService.getStandardFields(sessionBean.getCompany().getCompanyId(),
					CoreFieldGroupVo.PALMS, projectId, FieldEntityType.PALMS_PROJECT_USERS, true, true, false, true);

			for (FieldDto field : fields) {
				if (field instanceof StandardFieldDto) {
					StandardFieldDto standardField = (StandardFieldDto) field;
					if (standardField.isLogonIDField() || standardField.isPassword()
							|| standardField.isUpdateLogonFields()) {
						viewBean.addField(field);
					}
				}
			}
			viewBean.sortFields();
		}
	}

	public FieldDto getField(ActionEvent event) {
		FieldDto field = null;
		if (event != null) {
			Integer fieldId = (Integer) event.getComponent().getAttributes().get("fieldId");
			if (fieldId != null) {
				field = viewBean.findFieldById(fieldId);
			}
		}
		return field;
	}

	public LanguageBean getLanguageBean() {
		return languageBean;
	}

	public void setLanguageBean(LanguageBean languageBean) {
		this.languageBean = languageBean;
	}

	public ProjectFieldsViewBean getProjectFieldsViewBean() {
		return viewBean;
	}

	public void setProjectFieldsViewBean(ProjectFieldsViewBean projectFieldsViewBean) {
		this.viewBean = projectFieldsViewBean;
	}

	public ProjectSetupViewBean getProjectSetupViewBean() {
		return projectSetupViewBean;
	}

	public void setProjectSetupViewBean(ProjectSetupViewBean projectSetupViewBean) {
		this.projectSetupViewBean = projectSetupViewBean;
	}

	public boolean isEditable() {
		return sessionBean.isPermitted("editParticipantInfoFields");
	}

	public boolean isViewable() {
		return sessionBean.isPermitted("viewParticipantInfoFields");
	}

}