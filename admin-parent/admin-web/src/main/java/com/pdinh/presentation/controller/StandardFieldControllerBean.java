package com.pdinh.presentation.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.dto.StandardFieldDto;
import com.kf.uffda.view.FieldViewBean;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.presentation.view.ProjectFieldsViewBean;
import com.pdinh.presentation.view.StandardFieldViewBean;

@ManagedBean(name = "standardFieldControllerBean")
@ViewScoped
public class StandardFieldControllerBean extends FieldControllerBean implements Serializable {

	private static final long serialVersionUID = -2433846720846252997L;

	private static final Logger log = LoggerFactory.getLogger(StandardFieldControllerBean.class);

	@ManagedProperty(name = "projectFieldsViewBean", value = "#{projectFieldsViewBean}")
	private ProjectFieldsViewBean projectFieldsViewBean;

	@ManagedProperty(name = "standardFieldViewBean", value = "#{standardFieldViewBean}")
	private StandardFieldViewBean standardFieldViewBean;

	public StandardFieldControllerBean() {
		super("frmFieldDictionary");
	}

	@Override
	public void initView(ComponentSystemEvent event) {
		if (isFirstTime()) {
			super.initView(event);
		}
	}

	public void handleSaveField(ActionEvent event) {
		StandardFieldDto field = standardFieldViewBean.getField();
		fieldDictionaryViewBean.setShowCustomField(false);
		fieldDictionaryViewBean.setShowStandardField(false);

		if (field != null) {
			field.setChanged(true);
			projectFieldsViewBean.addField(field);
			projectFieldsViewBean.sortFields();
			projectFieldsViewBean.setClientSideValueChanged(true);

			String whatChanged = field.getWhatChanged(null);

			EntityAuditLogger.auditLog.info("{} performed an {} for Standard Field [{}] for client {}.  {}",
					new Object[] { getUsername(), EntityAuditLogger.OP_UPDATE_FIELD, field.getName(), getCompanyName(),
							whatChanged });
		}

		if (standardFieldViewBean.isEditOnly()) {
			standardFieldViewBean.init();
			if (field != null) {
				closeLightBox(field.getEditLightBoxId());
			}

		} else {
			fieldDictionaryViewBean.addField(field);
			fieldDictionaryViewBean.sortFields();

			uffdaService.saveCoreField(sessionBean.getSubject_id(), sessionBean.getCompany().getCompanyId(), field);
			standardFieldViewBean.init();
		}
	}

	public void handleCancelField(ActionEvent event) {
		StandardFieldDto field = standardFieldViewBean.getField();
		fieldDictionaryViewBean.setShowCustomField(false);
		fieldDictionaryViewBean.setShowStandardField(false);

		if (standardFieldViewBean.isEditOnly()) {
			standardFieldViewBean.init();
			if (field != null) {
				closeLightBox(field.getEditLightBoxId());
			}

		} else {
			standardFieldViewBean.init();
		}
	}

	public StandardFieldViewBean getStandardFieldViewBean() {
		return standardFieldViewBean;
	}

	public void setStandardFieldViewBean(StandardFieldViewBean standardFieldViewBean) {
		this.standardFieldViewBean = standardFieldViewBean;
	}

	@Override
	public FieldViewBean<?> getViewBean() {
		return standardFieldViewBean;
	}

	public ProjectFieldsViewBean getProjectFieldsViewBean() {
		return projectFieldsViewBean;
	}

	public void setProjectFieldsViewBean(ProjectFieldsViewBean projectFieldsViewBean) {
		this.projectFieldsViewBean = projectFieldsViewBean;
	}

}