package com.pdinh.presentation.view;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FilenameUtils;
import org.apache.shiro.SecurityUtils;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.dto.ImportJobDto;
import com.kf.uffda.dto.ImportJobLogDto;
import com.kf.uffda.persistence.ErrorSeverity;
import com.kf.uffda.persistence.JobStatus;
import com.pdinh.presentation.helper.LabelUtils;

@ManagedBean
@ViewScoped
public class ImportManagementViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(ImportManagementViewBean.class);

	private int importJobId = -1;
	private Integer projectId;
	private String projectName;
	private String projectType;
	private String projectTypeCode;
	private String projectCode;
	private String targetLevel;
	private String createdBy;
	private Timestamp createdDate;
	private Timestamp dueDate;
	private String importName;
	private String selectedImportName;
	private List<ImportJobDto> importJobs = new ArrayList<ImportJobDto>();
	private List<ImportJobLogDto> importJobLog = new ArrayList<ImportJobLogDto>();
	private ImportJobLogDto selectedJobLog;
	private String clientContactName;
	private String clientContactPhone;
	private String clientContactEmail;
	private boolean createImportDisabled;
	private UploadedFile uploadedFile;
	private final List<SelectItem> statusOptions = new ArrayList<SelectItem>();
	private final List<SelectItem> errorSeverityOptions = new ArrayList<SelectItem>();
	private List<JobStatus> jobStatuses = new ArrayList<JobStatus>();
	private List<ErrorSeverity> errorSeverity = new ArrayList<ErrorSeverity>();
	private ImportJobDto selectedJob;
	private boolean commitDisabled;
	private boolean createRequestDisabled;
	private boolean backDisabled;
	private boolean nextDisabled;
	private boolean restartImportDisabled;
	private boolean restartImportJob;
	private boolean importJobLogRendered;
	private boolean importJobsListRendered;
	private boolean importJobLogExceptionsRendered;
	private boolean importJobLogResultsRendered;
	private ImportJobLogDto currentLog;
	private boolean jobClosed;
	private boolean closeJobDisabled;
	private boolean cancelOperationDisabled;
	private boolean viewMenuDisabled = true;
	private boolean lrmProjectExist;
	private int lrmProjectId;

	// Flag for Project Code' label... if false, label is 'Engagement ID'
	private boolean hasProjectCodeLabel = true;

	private int statisticsAccordionIndex;

	public void initImportJob() {
		if (!isRestartImportJob()) {
			setImportName(generateImportName());
			setClientContactName("");
			setClientContactEmail("");
			setClientContactPhone("");
		}
		setUploadedFile(null);
		setBackDisabled(true);
		setNextDisabled(false);
	}

	public void initialize() {
		initImportJob();
		setCommitDisabled(true);
		setCreateRequestDisabled(true);
		setRestartImportDisabled(true);
		setImportJobLogRendered(false);
		setImportJobsListRendered(true);
		setImportJobLogResultsRendered(false);
		setLrmProjectExist(false);
		statisticsAccordionIndex = 0;
		lrmProjectId = 0;
	}

	public static String generateImportName() {
		String name = SecurityUtils.getSubject().getPrincipal().toString();
		Date currentDate = new Date();
		String dateString = new SimpleDateFormat("dd_MMM_yyyy_HH_mm_ss").format(currentDate);

		name = name + "_" + dateString;

		return name;
	}

	public String exportErrorFileName() {
		return getImportName() + "_log_" + selectedJobLog.getId();
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public Timestamp getDueDate() {
		return dueDate;
	}

	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	public String getProjectTypeCode() {
		return projectTypeCode;
	}

	public void setProjectTypeCode(String projectTypeCode) {
		this.projectTypeCode = projectTypeCode;
	}

	public String getImportName() {
		return importName;
	}

	public void setImportName(String importName) {
		this.importName = importName;
	}

	public List<ImportJobDto> getImportJobs() {
		return importJobs;
	}

	public void setImportJobs(List<ImportJobDto> importJobs) {
		this.importJobs = importJobs;
	}

	public String getClientContactName() {
		return clientContactName;
	}

	public void setClientContactName(String clientContactName) {
		this.clientContactName = clientContactName;
	}

	public String getClientContactPhone() {
		return clientContactPhone;
	}

	public void setClientContactPhone(String clientContactPhone) {
		this.clientContactPhone = clientContactPhone;
	}

	public String getClientContactEmail() {
		return clientContactEmail;
	}

	public void setClientContactEmail(String clientContactEmail) {
		this.clientContactEmail = clientContactEmail;
	}

	public boolean isCreateImportDisabled() {
		return createImportDisabled;
	}

	public void setCreateImportDisabled(boolean createImportDisabled) {
		this.createImportDisabled = createImportDisabled;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getUploadedFilename() {
		if (uploadedFile != null) {
			return FilenameUtils.getName(uploadedFile.getFileName());
		}
		return "";
	}

	public boolean isUploadedFilePresent() {
		return uploadedFile != null;
	}

	public List<SelectItem> getStatusOptions() {
		return statusOptions;
	}

	public void setStatusOptions() {
		List<String> statusList = new ArrayList<String>();

		logger.trace("Status Size = {}", jobStatuses.size());

		for (JobStatus status : jobStatuses) {
			logger.debug("Status = {}", status.getCode());
			statusList.add(LabelUtils.getText(status.getCode()));
		}
		Collections.sort(statusList);

		statusOptions.clear();
		statusOptions.add(new SelectItem("", "All"));
		for (String status : statusList) {
			statusOptions.add(new SelectItem(status, status));
		}
	}

	public List<JobStatus> getJobStatuses() {
		return jobStatuses;
	}

	public void setJobStatuses(List<JobStatus> jobStatuses) {
		this.jobStatuses = jobStatuses;
	}

	public ImportJobDto getSelectedJob() {
		return selectedJob;
	}

	public void setSelectedJob(ImportJobDto selectedJob) {
		this.selectedJob = selectedJob;
	}

	public boolean isCommitDisabled() {
		return commitDisabled;
	}

	public void setCommitDisabled(boolean commitDisabled) {
		this.commitDisabled = commitDisabled;
	}

	public boolean isBackDisabled() {
		return backDisabled;
	}

	public void setBackDisabled(boolean backDisabled) {
		this.backDisabled = backDisabled;
	}

	public boolean isNextDisabled() {
		return nextDisabled;
	}

	public void setNextDisabled(boolean nextDisabled) {
		this.nextDisabled = nextDisabled;
	}

	public boolean isRestartImportJob() {
		return restartImportJob;
	}

	public void setRestartImportJob(boolean restartImportJob) {
		this.restartImportJob = restartImportJob;
	}

	public int getImportJobId() {
		return importJobId;
	}

	public void setImportJobId(int importJobId) {
		this.importJobId = importJobId;
	}

	public boolean isRestartImportDisabled() {
		return restartImportDisabled;
	}

	public void setRestartImportDisabled(boolean restartImportDisabled) {
		this.restartImportDisabled = restartImportDisabled;
	}

	public boolean isImportJobLogRendered() {
		return importJobLogRendered;
	}

	public void setImportJobLogRendered(boolean importJobLogRendered) {
		this.importJobLogRendered = importJobLogRendered;
	}

	public boolean isImportJobsListRendered() {
		return importJobsListRendered;
	}

	public void setImportJobsListRendered(boolean importJobsListRendered) {
		this.importJobsListRendered = importJobsListRendered;
	}

	public List<ImportJobLogDto> getImportJobLog() {
		return importJobLog;
	}

	public void setImportJobLog(List<ImportJobLogDto> importJobLog) {
		this.importJobLog = importJobLog;
	}

	public ImportJobLogDto getCurrentLog() {
		return currentLog;
	}

	public void setCurrentLog(ImportJobLogDto currentLog) {
		this.currentLog = currentLog;
	}

	public boolean isJobClosed() {
		return jobClosed;
	}

	public void setJobClosed(boolean jobClosed) {
		this.jobClosed = jobClosed;
	}

	public ImportJobLogDto getSelectedJobLog() {
		return selectedJobLog;
	}

	public void setSelectedJobLog(ImportJobLogDto selectedJobLog) {
		this.selectedJobLog = selectedJobLog;
	}

	public boolean isImportJobLogExceptionsRendered() {
		return importJobLogExceptionsRendered;
	}

	public void setImportJobLogExceptionsRendered(boolean importJobLogExceptionsRendered) {
		this.importJobLogExceptionsRendered = importJobLogExceptionsRendered;
	}

	public boolean isCloseJobDisabled() {
		return closeJobDisabled;
	}

	public void setCloseJobDisabled(boolean closeJobDisabled) {
		this.closeJobDisabled = closeJobDisabled;
	}

	public boolean isCancelOperationDisabled() {
		return cancelOperationDisabled;
	}

	public void setCancelOperationDisabled(boolean cancelOperationDisabled) {
		this.cancelOperationDisabled = cancelOperationDisabled;
	}

	public boolean isImportJobLogResultsRendered() {
		return importJobLogResultsRendered;
	}

	public void setImportJobLogResultsRendered(boolean importJobLogResultsRendered) {
		this.importJobLogResultsRendered = importJobLogResultsRendered;
	}

	public int getStatisticsAccordionIndex() {
		return statisticsAccordionIndex;
	}

	public void setStatisticsAccordionIndex(int statisticsAccordionIndex) {
		this.statisticsAccordionIndex = statisticsAccordionIndex;
	}

	public List<SelectItem> getErrorSeverityOptions() {
		return errorSeverityOptions;
	}

	public void setErrorSeverityOptions() {
		List<String> errorSeverityList = new ArrayList<String>();

		for (ErrorSeverity severity : errorSeverity) {
			errorSeverityList.add(LabelUtils.getText(severity.getCode()));
		}
		Collections.sort(errorSeverityList);

		errorSeverityOptions.clear();
		errorSeverityOptions.add(new SelectItem("", "All"));
		for (String severity : errorSeverityList) {
			errorSeverityOptions.add(new SelectItem(severity, severity));
		}
	}

	public List<ErrorSeverity> getErrorSeverity() {
		return errorSeverity;
	}

	public void setErrorSeverity(List<ErrorSeverity> errorSeverity) {
		this.errorSeverity = errorSeverity;
	}

	public boolean isViewMenuDisabled() {
		return viewMenuDisabled;
	}

	public void setViewMenuDisabled(boolean viewMenuDisabled) {
		this.viewMenuDisabled = viewMenuDisabled;
	}

	public String getSelectedImportName() {
		return selectedImportName;
	}

	public void setSelectedImportName(String selectedImportName) {
		this.selectedImportName = selectedImportName;
	}

	public boolean isLrmProjectExist() {
		return lrmProjectExist;
	}

	public void setLrmProjectExist(boolean lrmProjectExist) {
		this.lrmProjectExist = lrmProjectExist;
	}

	public boolean isCreateRequestDisabled() {
		return createRequestDisabled;
	}

	public void setCreateRequestDisabled(boolean createRequestDisabled) {
		this.createRequestDisabled = createRequestDisabled;
	}

	public int getLrmProjectId() {
		return lrmProjectId;
	}

	public void setLrmProjectId(int lrmProjectId) {
		this.lrmProjectId = lrmProjectId;
	}

	public boolean getHasProjectCodeLabel() {
		return this.hasProjectCodeLabel;
	}

	public void setHasProjectCodeLabel(boolean value) {
		this.hasProjectCodeLabel = value;
	}
}
