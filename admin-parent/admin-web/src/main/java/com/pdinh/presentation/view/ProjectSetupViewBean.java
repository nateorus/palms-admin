package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.persistence.ms.entity.ConfigurationType;
import com.pdinh.presentation.domain.InstrumentSelectionObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.NonBillableTypeObj;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.domain.ProjectReportObj;
import com.pdinh.presentation.domain.ScoringMethodObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;

@ManagedBean(name = "projectSetupViewBean")
@ViewScoped
public class ProjectSetupViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String projectName;
	private String projectCode;
	private boolean disabledProjectCode;
	private Integer projectTypeId = 1;
	private Integer nhnAdminId;
	private String reportContentManifestRef;
	private List<ListItemObj> projectTypes;
	private List<InstrumentSelectionObj> preworkInstruments;
	private List<InstrumentSelectionObj> assessmentDayInstruments;
	private List<InstrumentSelectionObj> preworkInstrumentSelections;
	private List<InstrumentSelectionObj> assessmentDayInstrumentSelections;
	private List<Map<String, InstrumentSelectionObj>> isoMaps;
	private List<ListItemObj> reportContentManifestRefs = new ArrayList<ListItemObj>();
	private List<ProjectReportObj> projectReportTemplates = new ArrayList<ProjectReportObj>();

	// explicit validation feedback
	private boolean validProjectName = true;
	private boolean validProjectCode = true;
	private boolean validOfficeLocation = true;
	private boolean validDueDate = true;
	private boolean validConfigurationType = true;
	private boolean validAssignedPmPc = true;
	private boolean validInternalScheduler = true;
	private boolean validTargetLevel = true;
	private boolean validScoringMethod = true;

	private Boolean hasProducts = false;

	private Boolean renderReportContentManifests = false;

	private int targetLevelTypeId;
	private List<TargetLevelTypeObj> targetLevelTypes;
	private boolean disabledLevelType;

	private Boolean instrumentsVisible = false;

	private Boolean hasCompanyProjectTypes = false;

	private List<OfficeLocationObj> officeLocations;
	private Integer selectedOfficeLocationId;

	private Boolean participantsAdded = false;
	private Boolean disallowEditProject = false;
	private Boolean enableAllowReportDownload;
	private boolean disallowEditTargetLevel = false;
	private boolean disallowEditScoring = false;
	private boolean disallowEditConfiguration = false;
	private boolean disabledProjectFields = false;

	private Date dueDate;
	private Date originalDueDate;

	private List<ExternalSubjectDataObj> internalTeammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> clientTeammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> removeInternalTeammembers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> removeClientTeammembers = new ArrayList<ExternalSubjectDataObj>();

	private List<ExternalSubjectDataObj> assignedPmPcs = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> internalSchedulers = new ArrayList<ExternalSubjectDataObj>();

	private ExternalSubjectDataObj assignedPM;
	private ExternalSubjectDataObj internalScheduler;

	private String assignedPmId;
	private String internalSchedulerId;

	private String initialAssignedPmId = "";
	private String initialInternalSchedulerId = "";

	private List<ScoringMethodObj> scoringMethods;
	private Integer scoringMethodId;
	private Integer configurationTypeId;
	private List<ConfigurationType> configurationTypes = new ArrayList<ConfigurationType>();
	private boolean disabledConfigurationType;

	private String languageCHQ = null;
	private List<LanguageObj> languagesCHQ;

	private boolean includeClientLogonIdPassword;
	private boolean originalIncludeClientLogonIdPassword;

	/*
	 * rendering boolean stubs
	 */
	private boolean renderAssignedPM;
	private boolean renderInternalScheduler;
	private boolean renderScoringMethod;
	private boolean renderConfigurationPanel;
	private boolean renderFCGrid;
	private boolean renderConfigurationSelection;
	private boolean renderTargetLevels;
	private boolean renderCHQlang;
	private boolean renderToggleProjectOpen;
	private boolean renderBillable;
	private boolean renderNoneBillableTypes;
	private boolean renderIncludeClientLogonIdPassword;
	/*
	 * end stubs
	 */

	private boolean createProject;
	private Integer projectId;

	private boolean clientTeamTabDisabled = false;
	private boolean internalTeamTabDisabled = false;

	private boolean doSaveInternalTeam = false;
	private boolean doSaveClientTeam = false;

	private Boolean renderAllowReportDownload = false;
	private Boolean allowReportDownload = false;

	private boolean projectOpen = true;
	private boolean viaEDGE;
	private boolean abyd;

	private boolean renderSaveAndManageDelivery;
	private Boolean billable = true;
	private List<NonBillableTypeObj> noneBillableTypes = new ArrayList<NonBillableTypeObj>();
	private Integer noneBillableTypeId;

	private boolean showProjectSetup = true;
	private boolean showParticipantInformationFields = false;

	private boolean isAbydProduct = false;
	private boolean isGlGpsProduct = false;

	public boolean isDoSaveInternalTeam() {
		return doSaveInternalTeam;
	}

	public void setDoSaveInternalTeam(boolean doSaveInternalTeam) {
		this.doSaveInternalTeam = doSaveInternalTeam;
	}

	public boolean isDoSaveClientTeam() {
		return doSaveClientTeam;
	}

	public void setDoSaveClientTeam(boolean doSaveClientTeam) {
		this.doSaveClientTeam = doSaveClientTeam;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public Integer getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(Integer projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public void setProjectTypes(List<ListItemObj> projectTypes) {
		this.projectTypes = projectTypes;
	}

	public List<ListItemObj> getProjectTypes() {
		return projectTypes;
	}

	public List<InstrumentSelectionObj> getPreworkInstruments() {
		return preworkInstruments;
	}

	public void setPreworkInstruments(List<InstrumentSelectionObj> preworkInstruments) {
		this.preworkInstruments = preworkInstruments;
	}

	public List<InstrumentSelectionObj> getAssessmentDayInstruments() {
		return assessmentDayInstruments;
	}

	public void setAssessmentDayInstruments(List<InstrumentSelectionObj> assessmentDayInstruments) {
		this.assessmentDayInstruments = assessmentDayInstruments;
	}

	public Boolean getInstrumentsVisible() {
		return instrumentsVisible;
	}

	public void setInstrumentsVisible(Boolean instrumentsVisible) {
		this.instrumentsVisible = instrumentsVisible;
	}

	public void setNhnAdminId(Integer nhnAdminId) {
		this.nhnAdminId = nhnAdminId;
	}

	public Integer getNhnAdminId() {
		return nhnAdminId;
	}

	public void setHasProducts(Boolean hasProducts) {
		this.hasProducts = hasProducts;
	}

	public Boolean getHasProducts() {
		return hasProducts;
	}

	/************************************************************/
	public void setRenderTargetLevels(boolean value) {
		this.renderTargetLevels = value;
	}

	public boolean isRenderTargetLevels() {
		return this.renderTargetLevels;
	}

	/************************************************************/
	public void setTargetLevelTypeId(int value) {
		this.targetLevelTypeId = value;
	}

	public int getTargetLevelTypeId() {
		return this.targetLevelTypeId;
	}

	/************************************************************/
	public void setTargetLevelTypes(List<TargetLevelTypeObj> value) {
		this.targetLevelTypes = value;
	}

	public List<TargetLevelTypeObj> getTargetLevelTypes() {
		return this.targetLevelTypes;
	}

	/************************************************************/
	public void setHasCompanyProjectTypes(Boolean value) {
		this.hasCompanyProjectTypes = value;
	}

	public Boolean getHasCompanyProjectTypes() {
		return this.hasCompanyProjectTypes;
	}

	/**************************************************************/
	public void setParticipantsAdded(Boolean value) {
		this.participantsAdded = value;
	}

	public Boolean getParticipantsAdded() {
		return this.participantsAdded;
	}

	/*
		public String getFromEmail() {
			return fromEmail;
		}
		
		public void setFromEmail(String fromEmail) {
			this.fromEmail = fromEmail;
		}
	*/
	public InstrumentSelectionObj findPreworkInstrumentSelectionObjById(Integer id) {
		if (preworkInstruments != null) {
			for (InstrumentSelectionObj instrumentSelectionObj : preworkInstruments) {
				if (instrumentSelectionObj.getId() == id) {
					return instrumentSelectionObj;
				}
			}
		}
		return null;
	}

	public InstrumentSelectionObj findAssessmentDayInstrumentSelectionObjById(Integer id) {
		if (assessmentDayInstruments != null) {
			for (InstrumentSelectionObj instrumentSelectionObj : assessmentDayInstruments) {
				if (instrumentSelectionObj.getId() == id) {
					return instrumentSelectionObj;
				}
			}
		}
		return null;
	}

	public String getReportContentManifestRef() {
		return reportContentManifestRef;
	}

	public void setReportContentManifestRef(String reportContentManifestRef) {
		this.reportContentManifestRef = reportContentManifestRef;
	}

	public Boolean getRenderReportContentManifests() {
		return renderReportContentManifests;
	}

	public void setRenderReportContentManifests(Boolean renderReportContentManifests) {
		this.renderReportContentManifests = renderReportContentManifests;
	}

	public List<ListItemObj> getReportContentManifestRefs() {
		return reportContentManifestRefs;
	}

	public void setReportContentManifestRefs(List<ListItemObj> reportContentManifestRefs) {
		this.reportContentManifestRefs = reportContentManifestRefs;
	}

	public List<OfficeLocationObj> getOfficeLocations() {
		return officeLocations;
	}

	public void setOfficeLocations(List<OfficeLocationObj> officeLocations) {
		this.officeLocations = officeLocations;
	}

	public Integer getSelectedOfficeLocationId() {
		return selectedOfficeLocationId;
	}

	public void setSelectedOfficeLocationId(Integer selectedOfficeLocationId) {
		this.selectedOfficeLocationId = selectedOfficeLocationId;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Boolean getDisallowEditProject() {
		return disallowEditProject;
	}

	public void setDisallowEditProject(Boolean disallowEditProject) {
		this.disallowEditProject = disallowEditProject;
	}

	public boolean isCreateProject() {
		return createProject;
	}

	public void setCreateProject(boolean createProject) {
		this.createProject = createProject;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public List<InstrumentSelectionObj> getPreworkInstrumentSelections() {
		return preworkInstrumentSelections;
	}

	public void setPreworkInstrumentSelections(List<InstrumentSelectionObj> preworkInstrumentSelections) {
		this.preworkInstrumentSelections = preworkInstrumentSelections;
	}

	public List<InstrumentSelectionObj> getAssessmentDayInstrumentSelections() {
		return assessmentDayInstrumentSelections;
	}

	public void setAssessmentDayInstrumentSelections(List<InstrumentSelectionObj> assessmentDayInstrumentSelections) {
		this.assessmentDayInstrumentSelections = assessmentDayInstrumentSelections;
	}

	public List<Map<String, InstrumentSelectionObj>> getIsoMaps() {
		return isoMaps;
	}

	public void setIsoMaps(List<Map<String, InstrumentSelectionObj>> isoMaps) {
		this.isoMaps = isoMaps;
	}

	public boolean isClientTeamTabDisabled() {
		return clientTeamTabDisabled;
	}

	public void setClientTeamTabDisabled(boolean clientTeamTabDisabled) {
		this.clientTeamTabDisabled = clientTeamTabDisabled;
	}

	public List<ExternalSubjectDataObj> getInternalTeammembers() {
		return internalTeammembers;
	}

	public void setInternalTeammembers(List<ExternalSubjectDataObj> internalTeammembers) {
		this.internalTeammembers = internalTeammembers;
	}

	public List<ExternalSubjectDataObj> getClientTeammembers() {
		return clientTeammembers;
	}

	public void setClientTeammembers(List<ExternalSubjectDataObj> clientTeammembers) {
		this.clientTeammembers = clientTeammembers;
	}

	public List<ExternalSubjectDataObj> getRemoveInternalTeammembers() {
		return removeInternalTeammembers;
	}

	public void setRemoveInternalTeammembers(List<ExternalSubjectDataObj> removeInternalTeammembers) {
		this.removeInternalTeammembers = removeInternalTeammembers;
	}

	public List<ExternalSubjectDataObj> getRemoveClientTeammembers() {
		return removeClientTeammembers;
	}

	public void setRemoveClientTeammembers(List<ExternalSubjectDataObj> removeClientTeammembers) {
		this.removeClientTeammembers = removeClientTeammembers;
	}

	public boolean isInternalTeamTabDisabled() {
		return internalTeamTabDisabled;
	}

	public void setInternalTeamTabDisabled(boolean internalTeamTabDisabled) {
		this.internalTeamTabDisabled = internalTeamTabDisabled;
	}

	public Boolean getAllowReportDownload() {
		return allowReportDownload;
	}

	public void setAllowReportDownload(Boolean allowReportDownload) {
		this.allowReportDownload = allowReportDownload;
	}

	public Boolean getRenderAllowReportDownload() {
		return renderAllowReportDownload;
	}

	public void setRenderAllowReportDownload(Boolean renderAllowReportDownload) {
		this.renderAllowReportDownload = renderAllowReportDownload;
	}

	public Boolean getEnableAllowReportDownload() {
		return enableAllowReportDownload;
	}

	public void setEnableAllowReportDownload(Boolean enableAllowReportDownload) {
		this.enableAllowReportDownload = enableAllowReportDownload;
	}

	public boolean isRenderAssignedPM() {
		return renderAssignedPM;
	}

	public void setRenderAssignedPM(boolean renderAssignedPM) {
		this.renderAssignedPM = renderAssignedPM;
	}

	public boolean isRenderInternalScheduler() {
		return renderInternalScheduler;
	}

	public void setRenderInternalScheduler(boolean renderInternalScheduler) {
		this.renderInternalScheduler = renderInternalScheduler;
	}

	public boolean isRenderScoringMethod() {
		return renderScoringMethod;
	}

	public void setRenderScoringMethod(boolean renderScoringMethod) {
		this.renderScoringMethod = renderScoringMethod;
	}

	public boolean isRenderFCGrid() {
		return renderFCGrid;
	}

	public void setRenderFCGrid(boolean renderFCGrid) {
		this.renderFCGrid = renderFCGrid;
	}

	public boolean isRenderConfigurationPanel() {
		return renderConfigurationPanel;
	}

	public void setRenderConfigurationPanel(boolean renderConfigurationPanel) {
		this.renderConfigurationPanel = renderConfigurationPanel;
	}

	public ExternalSubjectDataObj getAssignedPM() {
		return assignedPM;
	}

	public void setAssignedPM(ExternalSubjectDataObj assignedPM) {
		this.assignedPM = assignedPM;
	}

	public ExternalSubjectDataObj getInternalScheduler() {
		return internalScheduler;
	}

	public void setInternalScheduler(ExternalSubjectDataObj internalScheduler) {
		this.internalScheduler = internalScheduler;
	}

	public List<ScoringMethodObj> getScoringMethods() {
		return scoringMethods;
	}

	public void setScoringMethods(List<ScoringMethodObj> scoringMethods) {
		this.scoringMethods = scoringMethods;
	}

	public Integer getScoringMethodId() {
		return scoringMethodId;
	}

	public void setScoringMethodId(Integer scoringMethodId) {
		this.scoringMethodId = scoringMethodId;
	}

	public boolean isRenderConfigurationSelection() {
		return renderConfigurationSelection;
	}

	public void setRenderConfigurationSelection(boolean renderConfigurationSelection) {
		this.renderConfigurationSelection = renderConfigurationSelection;
	}

	public List<ConfigurationType> getConfigurationTypes() {
		return configurationTypes;
	}

	public void setConfigurationTypes(List<ConfigurationType> configurationTypes) {
		this.configurationTypes = configurationTypes;
	}

	public Integer getConfigurationTypeId() {
		return configurationTypeId;
	}

	public void setConfigurationTypeId(Integer configurationTypeId) {
		this.configurationTypeId = configurationTypeId;
	}

	public String getAssignedPmId() {
		return assignedPmId;
	}

	public void setAssignedPmId(String assignedPmId) {
		this.assignedPmId = assignedPmId;
	}

	public String getInternalSchedulerId() {
		return internalSchedulerId;
	}

	public void setInternalSchedulerId(String internalSchedulerId) {
		this.internalSchedulerId = internalSchedulerId;
	}

	public String getLanguageCHQ() {
		return languageCHQ;
	}

	public void setLanguageCHQ(String languageCHQ) {
		this.languageCHQ = languageCHQ;
	}

	public List<LanguageObj> getLanguagesCHQ() {
		return languagesCHQ;
	}

	public void setLanguagesCHQ(List<LanguageObj> languagesCHQ) {
		this.languagesCHQ = languagesCHQ;
	}

	public boolean isRenderCHQlang() {
		return renderCHQlang;
	}

	// Need this for the webpage
	public boolean getRenderCHQlang() {
		return this.renderCHQlang;
	}

	public void setRenderCHQlang(boolean renderCHQlang) {
		this.renderCHQlang = renderCHQlang;
	}

	public boolean getRenderToggleProjectOpen() {
		return renderToggleProjectOpen;
	}

	public void setRenderToggleProjectOpen(boolean renderToggleProjectOpen) {
		this.renderToggleProjectOpen = renderToggleProjectOpen;
	}

	public boolean isProjectOpen() {
		return projectOpen;
	}

	public void setProjectOpen(boolean projectOpen) {
		this.projectOpen = projectOpen;
	}

	public boolean isDisabledConfigurationType() {
		return disabledConfigurationType;
	}

	public void setDisabledConfigurationType(boolean disabledConfigurationType) {
		this.disabledConfigurationType = disabledConfigurationType;
	}

	public boolean isDisabledLevelType() {
		return disabledLevelType;
	}

	public void setDisabledLevelType(boolean disabledLevelType) {
		this.disabledLevelType = disabledLevelType;
	}

	public boolean isValidProjectName() {
		return validProjectName;
	}

	public void setValidProjectName(boolean validProjectName) {
		this.validProjectName = validProjectName;
	}

	public boolean isValidProjectCode() {
		return validProjectCode;
	}

	public void setValidProjectCode(boolean validProjectCode) {
		this.validProjectCode = validProjectCode;
	}

	public boolean isValidOfficeLocation() {
		return validOfficeLocation;
	}

	public void setValidOfficeLocation(boolean validOfficeLocation) {
		this.validOfficeLocation = validOfficeLocation;
	}

	public boolean isValidDueDate() {
		return validDueDate;
	}

	public void setValidDueDate(boolean validDueDate) {
		this.validDueDate = validDueDate;
	}

	public boolean isValidConfigurationType() {
		return validConfigurationType;
	}

	public void setValidConfigurationType(boolean validConfigurationType) {
		this.validConfigurationType = validConfigurationType;
	}

	public boolean isValidAssignedPmPc() {
		return validAssignedPmPc;
	}

	public void setValidAssignedPmPc(boolean validAssignedPmPc) {
		this.validAssignedPmPc = validAssignedPmPc;
	}

	public boolean isValidInternalScheduler() {
		return validInternalScheduler;
	}

	public void setValidInternalScheduler(boolean validInternalScheduler) {
		this.validInternalScheduler = validInternalScheduler;
	}

	public boolean isValidTargetLevel() {
		return validTargetLevel;
	}

	public void setValidTargetLevel(boolean validTargetLevel) {
		this.validTargetLevel = validTargetLevel;
	}

	public boolean isValidScoringMethod() {
		return validScoringMethod;
	}

	public void setValidScoringMethod(boolean validScoringMethod) {
		this.validScoringMethod = validScoringMethod;
	}

	public boolean isViaEDGE() {
		return viaEDGE;
	}

	public void setViaEDGE(boolean viaEDGE) {
		this.viaEDGE = viaEDGE;
	}

	public boolean isRenderSaveAndManageDelivery() {
		return renderSaveAndManageDelivery;
	}

	public void setRenderSaveAndManageDelivery(boolean renderSaveAndManageDelivery) {
		this.renderSaveAndManageDelivery = renderSaveAndManageDelivery;
	}

	public boolean isDisallowEditTargetLevel() {
		return disallowEditTargetLevel;
	}

	public void setDisallowEditTargetLevel(boolean disallowEditTargetLevel) {
		this.disallowEditTargetLevel = disallowEditTargetLevel;
	}

	public boolean isDisallowEditScoring() {
		return disallowEditScoring;
	}

	public void setDisallowEditScoring(boolean disallowEditScoring) {
		this.disallowEditScoring = disallowEditScoring;
	}

	public boolean isDisallowEditConfiguration() {
		return disallowEditConfiguration;
	}

	public void setDisallowEditConfiguration(boolean disallowEditConfiguration) {
		this.disallowEditConfiguration = disallowEditConfiguration;
	}

	public List<ExternalSubjectDataObj> getAssignedPmPcs() {
		return assignedPmPcs;
	}

	public void setAssignedPmPcs(List<ExternalSubjectDataObj> assignedPmPcs) {
		this.assignedPmPcs = assignedPmPcs;
	}

	public List<ExternalSubjectDataObj> getInternalSchedulers() {
		return internalSchedulers;
	}

	public void setInternalSchedulers(List<ExternalSubjectDataObj> internalSchedulers) {
		this.internalSchedulers = internalSchedulers;
	}

	public boolean isAbyd() {
		return abyd;
	}

	public void setAbyd(boolean abyd) {
		this.abyd = abyd;
	}

	public String getInitialAssignedPmId() {
		return initialAssignedPmId;
	}

	public void setInitialAssignedPmId(String initialAssignedPmId) {
		this.initialAssignedPmId = initialAssignedPmId;
	}

	public String getInitialInternalSchedulerId() {
		return initialInternalSchedulerId;
	}

	public void setInitialInternalSchedulerId(String initialInternalSchedulerId) {
		this.initialInternalSchedulerId = initialInternalSchedulerId;
	}

	public boolean isRenderBillable() {
		if (!renderBillable) {
			setDisabledProjectCode(false);
		}
		return renderBillable;
	}

	public void setRenderBillable(Boolean renderBillable) {
		this.renderBillable = renderBillable;
	}

	public Boolean getBillable() {
		if (billable) {
			setNoneBillableTypeId(-1);
			setRenderNoneBillableTypes(false);
			setDisabledProjectCode(false);
		} else {
			setDisabledProjectCode(true);
			setRenderNoneBillableTypes(true);
		}
		return billable;
	}

	public void setBillable(Boolean billable) {
		this.billable = billable;
	}

	public List<NonBillableTypeObj> getNoneBillableTypes() {
		return noneBillableTypes;
	}

	public void setNoneBillableTypes(List<NonBillableTypeObj> noneBillableTypes) {
		this.noneBillableTypes = noneBillableTypes;
	}

	public boolean isRenderNoneBillableTypes() {
		return renderNoneBillableTypes;
	}

	public void setRenderNoneBillableTypes(boolean renderNoneBillableTypes) {
		this.renderNoneBillableTypes = renderNoneBillableTypes;
	}

	public Integer getNoneBillableTypeId() {
		return noneBillableTypeId;
	}

	public void setNoneBillableTypeId(Integer noneBillableTypeId) {
		this.noneBillableTypeId = noneBillableTypeId;
	}

	public boolean isDisabledProjectCode() {
		return disabledProjectCode;
	}

	public void setDisabledProjectCode(boolean disabledProjectCode) {
		this.disabledProjectCode = disabledProjectCode;
	}

	public Date getOriginalDueDate() {
		return originalDueDate;
	}

	public void setOriginalDueDate(Date originalDueDate) {
		this.originalDueDate = originalDueDate;
	}

	public boolean isDisabledProjectFields() {
		return disabledProjectFields;
	}

	public void setDisabledProjectFields(boolean disabledProjectFields) {
		this.disabledProjectFields = disabledProjectFields;
	}

	public boolean isShowProjectSetup() {
		return showProjectSetup;
	}

	public void setShowProjectSetup(boolean showProjectSetup) {
		this.showProjectSetup = showProjectSetup;
	}

	public boolean isShowParticipantInformationFields() {
		return showParticipantInformationFields;
	}

	public void setShowParticipantInformationFields(boolean showParticipantInformationFields) {
		this.showParticipantInformationFields = showParticipantInformationFields;
	}

	public List<ProjectReportObj> getProjectReportTemplates() {
		return projectReportTemplates;
	}

	public void setProjectReportTemplates(List<ProjectReportObj> projectReportTemplates) {
		this.projectReportTemplates = projectReportTemplates;
	}

	public boolean getIsAbydProduct() {
		return this.isAbydProduct;
	}

	public void setIsAbydProduct(boolean value) {
		this.isAbydProduct = value;
	}

	public boolean getIsGlGpsProduct() {
		return this.isGlGpsProduct;
	}

	public void setIsGlGpsProduct(boolean value) {
		this.isGlGpsProduct = value;
	}

	public boolean isRenderIncludeClientLogonIdPassword() {
		return renderIncludeClientLogonIdPassword;
	}

	public void setRenderIncludeClientLogonIdPassword(boolean renderIncludeClientLogonIdPassword) {
		this.renderIncludeClientLogonIdPassword = renderIncludeClientLogonIdPassword;
	}

	public boolean isIncludeClientLogonIdPassword() {
		return includeClientLogonIdPassword;
	}

	public void setIncludeClientLogonIdPassword(boolean includeClientLogonIdPassword) {
		this.includeClientLogonIdPassword = includeClientLogonIdPassword;
	}

	public boolean isOriginalIncludeClientLogonIdPassword() {
		return originalIncludeClientLogonIdPassword;
	}

	public void setOriginalIncludeClientLogonIdPassword(boolean originalIncludeClientLogonIdPassword) {
		this.originalIncludeClientLogonIdPassword = originalIncludeClientLogonIdPassword;
	}

	public void resetOriginalIncludeClientLogonIdPassword() {
		this.originalIncludeClientLogonIdPassword = includeClientLogonIdPassword;
	}

}
