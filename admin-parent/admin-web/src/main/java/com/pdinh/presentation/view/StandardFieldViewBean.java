package com.pdinh.presentation.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.kf.uffda.dto.StandardFieldDto;
import com.kf.uffda.view.FieldViewBean;

@ManagedBean
@ViewScoped
public class StandardFieldViewBean extends FieldViewBean<StandardFieldDto> implements Serializable {

	private static final long serialVersionUID = 5001382516547804368L;

	public StandardFieldViewBean() {
		init();
	}

	@Override
	public void init() {
		super.init();
		setField(new StandardFieldDto());
	}
}
