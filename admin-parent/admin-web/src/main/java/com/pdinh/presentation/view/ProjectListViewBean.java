package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.domain.ProjectObj;

@ManagedBean(name = "projectListViewBean")
@ViewScoped
public class ProjectListViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Boolean editProjectDisabled = true;
	private ProjectObj selectedProject;
	private Boolean resetFiltersDisabled = true;
	private Boolean showResetFiltersDialog = false;
	private Boolean createProjectButtonDisabled = true;
	private Boolean manageProjectDisabled = true;
	private Boolean manageProjectEmailDisabled = true;
	private boolean manageProjectDeliveryDisabled = true;
	private Boolean copyProjectDisabled = true;
	private List<OfficeLocationObj> officeLocations;
	private Integer selectedOfficeLocationId;
	private String copyPrjNameValue = "mmmm";
	private String copyPrjCodeValue = "mmmm";
	private String copyPrjDeliveryName = "mmmm";
	private boolean includeLrm = false;
	private boolean includeAbydSetup = false;
	private boolean includeCustomFields = false;
	private boolean saveAndManageButtonDisabled = false;
	private boolean saveAndGoToDeliveryDisabled = false;
	private boolean saveAndStayDisabled = false;
	private String copyProjectStatus = "Not Attempted";
	private String copyLrmStatus = "Not Attempted";
	private String copyAbydStatus = "Not Attempted";
	private String newProjectName = "";
	private String newProjectId = "";
	private String officeLocationMsg;
	private String searchAllFields;
	private boolean includeAbyDDisabled = false;
	private boolean includeLrmDisabled = false;
	private boolean includeCustomFieldsDisabled = false;
	private boolean renderAssignedPM = false;
	private boolean renderInternalScheduler = false;
	private boolean renderDeliveryName = false;
	private List<ExternalSubjectDataObj> schedulers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> pmpcs = new ArrayList<ExternalSubjectDataObj>();
	public String assignedPmId;
	public String schedulerId;
	public boolean projectDeliveryNameDisabled = true;
	public boolean auditLogButtonDisabled = true;
	private boolean projectCodeRequired = true;
	private boolean generateBulkUploadTemplateDisabled = true;
	private boolean downloadReportsButtonDisabled = true;

	public void setEditProjectDisabled(Boolean editProjectDisabled) {
		this.editProjectDisabled = editProjectDisabled;
	}

	public Boolean getEditProjectDisabled() {
		return editProjectDisabled;
	}

	public void setSelectedProject(ProjectObj selectedProject) {
		this.selectedProject = selectedProject;
	}

	public ProjectObj getSelectedProject() {
		return selectedProject;
	}

	public Boolean getResetFiltersDisabled() {
		return resetFiltersDisabled;
	}

	public void setResetFiltersDisabled(Boolean resetFiltersDisabled) {
		this.resetFiltersDisabled = resetFiltersDisabled;
	}

	public Boolean getShowResetFiltersDialog() {
		return showResetFiltersDialog;
	}

	public void setShowResetFiltersDialog(Boolean showResetFiltersDialog) {
		this.showResetFiltersDialog = showResetFiltersDialog;
	}

	public Boolean getCreateProjectButtonDisabled() {
		return createProjectButtonDisabled;
	}

	public void setCreateProjectButtonDisabled(Boolean createProjectButtonDisabled) {
		this.createProjectButtonDisabled = createProjectButtonDisabled;
	}

	public Boolean getManageProjectDisabled() {
		return manageProjectDisabled;
	}

	public void setManageProjectDisabled(Boolean manageProjectDisabled) {
		this.manageProjectDisabled = manageProjectDisabled;
	}

	public Boolean getManageProjectEmailDisabled() {
		return manageProjectEmailDisabled;
	}

	public void setManageProjectEmailDisabled(Boolean manageProjectEmailDisabled) {
		this.manageProjectEmailDisabled = manageProjectEmailDisabled;
	}

	public String getSearchAllFields() {
		return searchAllFields;
	}

	public void setSearchAllFields(String searchAllFields) {
		this.searchAllFields = searchAllFields;
	}

	public Boolean getCopyProjectDisabled() {
		return copyProjectDisabled;
	}

	public void setCopyProjectDisabled(Boolean copyProjectDisabled) {
		this.copyProjectDisabled = copyProjectDisabled;
	}

	public List<OfficeLocationObj> getOfficeLocations() {
		return officeLocations;
	}

	public void setOfficeLocations(List<OfficeLocationObj> officeLocations) {
		this.officeLocations = officeLocations;
	}

	public Integer getSelectedOfficeLocationId() {
		return selectedOfficeLocationId;
	}

	public void setSelectedOfficeLocationId(Integer selectedOfficeLocationId) {
		this.selectedOfficeLocationId = selectedOfficeLocationId;
	}

	public String getCopyPrjNameValue() {
		return copyPrjNameValue;
	}

	public void setCopyPrjNameValue(String copyPrjNameValue) {
		this.copyPrjNameValue = copyPrjNameValue;
	}

	public String getCopyPrjCodeValue() {
		return copyPrjCodeValue;
	}

	public void setCopyPrjCodeValue(String copyPrjCodeValue) {
		this.copyPrjCodeValue = copyPrjCodeValue;
	}

	public boolean isIncludeLrm() {
		return includeLrm;
	}

	public void setIncludeLrm(boolean includeLrm) {
		this.includeLrm = includeLrm;
	}

	public boolean isIncludeAbydSetup() {
		return includeAbydSetup;
	}

	public void setIncludeAbydSetup(boolean includeAbydSetup) {
		this.includeAbydSetup = includeAbydSetup;
	}

	public String getOfficeLocationMsg() {
		return officeLocationMsg;
	}

	public void setOfficeLocationMsg(String officeLocationMsg) {
		this.officeLocationMsg = officeLocationMsg;
	}

	public boolean isSaveAndManageButtonDisabled() {
		return saveAndManageButtonDisabled;
	}

	public void setSaveAndManageButtonDisabled(boolean saveAndManageButtonDisabled) {
		this.saveAndManageButtonDisabled = saveAndManageButtonDisabled;
	}

	public boolean isSaveAndGoToDeliveryDisabled() {
		return saveAndGoToDeliveryDisabled;
	}

	public void setSaveAndGoToDeliveryDisabled(boolean saveAndGoToDeliveryDisabled) {
		this.saveAndGoToDeliveryDisabled = saveAndGoToDeliveryDisabled;
	}

	public boolean isSaveAndStayDisabled() {
		return saveAndStayDisabled;
	}

	public void setSaveAndStayDisabled(boolean saveAndStayDisabled) {
		this.saveAndStayDisabled = saveAndStayDisabled;
	}

	public String getCopyPrjDeliveryName() {
		return copyPrjDeliveryName;
	}

	public void setCopyPrjDeliveryName(String copyPrjDeliveryName) {
		this.copyPrjDeliveryName = copyPrjDeliveryName;
	}

	public String getCopyAbydStatus() {
		return copyAbydStatus;
	}

	public void setCopyAbydStatus(String copyAbydStatus) {
		this.copyAbydStatus = copyAbydStatus;
	}

	public String getCopyLrmStatus() {
		return copyLrmStatus;
	}

	public void setCopyLrmStatus(String copyLrmStatus) {
		this.copyLrmStatus = copyLrmStatus;
	}

	public String getCopyProjectStatus() {
		return copyProjectStatus;
	}

	public void setCopyProjectStatus(String copyProjectStatus) {
		this.copyProjectStatus = copyProjectStatus;
	}

	public String getNewProjectName() {
		return newProjectName;
	}

	public void setNewProjectName(String newProjectName) {
		this.newProjectName = newProjectName;
	}

	public String getNewProjectId() {
		return newProjectId;
	}

	public void setNewProjectId(String newProjectId) {
		this.newProjectId = newProjectId;
	}

	public boolean isIncludeAbyDDisabled() {
		return includeAbyDDisabled;
	}

	public void setIncludeAbyDDisabled(boolean includeAbyDDisabled) {
		this.includeAbyDDisabled = includeAbyDDisabled;
	}

	public boolean isIncludeLrmDisabled() {
		return includeLrmDisabled;
	}

	public void setIncludeLrmDisabled(boolean includeLrmDisabled) {
		this.includeLrmDisabled = includeLrmDisabled;
	}

	public List<ExternalSubjectDataObj> getSchedulers() {
		return schedulers;
	}

	public void setSchedulers(List<ExternalSubjectDataObj> schedulers) {
		this.schedulers = schedulers;
	}

	public List<ExternalSubjectDataObj> getPmpcs() {
		return pmpcs;
	}

	public void setPmpcs(List<ExternalSubjectDataObj> pmpcs) {
		this.pmpcs = pmpcs;
	}

	public boolean isRenderAssignedPM() {
		return renderAssignedPM;
	}

	public void setRenderAssignedPM(boolean renderAssignedPM) {
		this.renderAssignedPM = renderAssignedPM;
	}

	public boolean isRenderInternalScheduler() {
		return renderInternalScheduler;
	}

	public void setRenderInternalScheduler(boolean renderInternalScheduler) {
		this.renderInternalScheduler = renderInternalScheduler;
	}

	public String getAssignedPmId() {
		return assignedPmId;
	}

	public void setAssignedPmId(String assignedPmId) {
		this.assignedPmId = assignedPmId;
	}

	public String getSchedulerId() {
		return schedulerId;
	}

	public void setSchedulerId(String schedulerId) {
		this.schedulerId = schedulerId;
	}

	public boolean isRenderDeliveryName() {
		return renderDeliveryName;
	}

	public void setRenderDeliveryName(boolean renderDeliveryName) {
		this.renderDeliveryName = renderDeliveryName;
	}

	public boolean isProjectDeliveryNameDisabled() {
		return projectDeliveryNameDisabled;
	}

	public void setProjectDeliveryNameDisabled(boolean projectDeliveryNameDisabled) {
		this.projectDeliveryNameDisabled = projectDeliveryNameDisabled;
	}

	public boolean isAuditLogButtonDisabled() {
		return auditLogButtonDisabled;
	}

	public void setAuditLogButtonDisabled(boolean auditLogButtonDisabled) {
		this.auditLogButtonDisabled = auditLogButtonDisabled;
	}

	public boolean isProjectCodeRequired() {
		return projectCodeRequired;
	}

	public void setProjectCodeRequired(boolean projectCodeRequired) {
		this.projectCodeRequired = projectCodeRequired;
	}

	public boolean isManageProjectDeliveryDisabled() {
		return manageProjectDeliveryDisabled;
	}

	public void setManageProjectDeliveryDisabled(boolean manageProjectDeliveryDisabled) {
		this.manageProjectDeliveryDisabled = manageProjectDeliveryDisabled;
	}

	public boolean isGenerateBulkUploadTemplateDisabled() {
		return generateBulkUploadTemplateDisabled;
	}

	public void setGenerateBulkUploadTemplateDisabled(boolean generateBulkUploadTemplateDisabled) {
		this.generateBulkUploadTemplateDisabled = generateBulkUploadTemplateDisabled;
	}

	public boolean isDownloadReportsButtonDisabled() {
		return downloadReportsButtonDisabled;
	}

	public void setDownloadReportsButtonDisabled(boolean downloadReportsButtonDisabled) {
		this.downloadReportsButtonDisabled = downloadReportsButtonDisabled;
	}

	public boolean isIncludeCustomFields() {
		return includeCustomFields;
	}

	public boolean isIncludeCustomFieldsDisabled() {
		return includeCustomFieldsDisabled;
	}

	public void setIncludeCustomFields(boolean includeCustomFields) {
		this.includeCustomFields = includeCustomFields;
	}

	public void setIncludeCustomFieldsDisabled(boolean includeCustomFieldsDisabled) {
		this.includeCustomFieldsDisabled = includeCustomFieldsDisabled;
	}

}
