package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.data.jaxb.reporting.ReportRequestResponse;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportTypeObj;

@ManagedBean(name = "reportWizardViewBean")
@ViewScoped
public class ReportWizardViewBean implements Serializable {

	private static final long serialVersionUID = 783492078284387802L;
	private List<ParticipantObj> participants;
	private List<ReportTypeObj> reportTypes;
	private List<ReportTypeObj> selectedReportTypes = new ArrayList<ReportTypeObj>();
	private List<LanguageObj> languages = new ArrayList<LanguageObj>();
	private String reportXml;
	private String reportGlobalLanguageId;
	private boolean showReportTypesByProjectTypes = false;
	private boolean projectColumnRendered = false;
	private boolean disableParticipantLanguageTab;
	private boolean disableDownloadButton;
	private List<ReportRequestResponse> responseList = new ArrayList<ReportRequestResponse>();
	private String requestName = "";
	private int reportRequestId;
	private int activeTabIndex;

	public void init() {
		// this.setRequestName("");
		this.clearSelectedReportTypes();
		this.setDisableParticipantLanguageTab(true);
		this.setDisableDownloadButton(false);
		this.setActiveTabIndex(0);
	}

	public boolean isCreateReportsDisabled() {
		if (requestName.isEmpty() && selectedReportTypes.isEmpty()) {
			return true;
		} else if (!requestName.isEmpty() && selectedReportTypes.isEmpty()) {
			return true;
		} else if (requestName.isEmpty() && !selectedReportTypes.isEmpty()) {
			return true;
		} else if (!requestName.isEmpty() && !selectedReportTypes.isEmpty()) {
			return false;
		}
		return true;
	}

	public boolean isParticipantReportLanguageDisabled() {
		if (selectedReportTypes != null) {
			for (ReportTypeObj reportType : selectedReportTypes) {
				if (reportType.isParticipantLanguageVisible()) {
					return false;
				}
			}
		}
		return true;
	}

	public ParticipantObj findParticipantById(Integer participantId) {
		if (!participants.isEmpty()) {
			for (ParticipantObj participant : participants) {
				if (participant.getUserId().equals(participantId)) {
					return participant;
				}
			}
		}

		return null;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setReportTypes(List<ReportTypeObj> reportTypes) {
		this.reportTypes = reportTypes;
	}

	public List<ReportTypeObj> getReportTypes() {
		return reportTypes;
	}

	public void setLanguages(List<LanguageObj> languages) {
		this.languages = languages;
	}

	public List<LanguageObj> getLanguages() {
		return languages;
	}

	public void clearSelectedReportTypes() {
		this.selectedReportTypes = new ArrayList<ReportTypeObj>();
	}

	public void setReportGlobalLanguageId(String reportGlobalLanguageId) {
		this.reportGlobalLanguageId = reportGlobalLanguageId;
	}

	public String getReportGlobalLanguageId() {
		return reportGlobalLanguageId;
	}

	public String getReportXml() {
		return reportXml;
	}

	public void setReportXml(String reportXml) {
		this.reportXml = reportXml;
	}

	public boolean isProjectColumnRendered() {
		return projectColumnRendered;
	}

	public void setProjectColumnRendered(boolean projectColumnRendered) {
		this.projectColumnRendered = projectColumnRendered;
	}

	public boolean isDisableParticipantLanguageTab() {
		return disableParticipantLanguageTab;
	}

	public void setDisableParticipantLanguageTab(boolean disableParticipantLanguageTab) {
		this.disableParticipantLanguageTab = disableParticipantLanguageTab;
	}

	public boolean isDisableDownloadButton() {
		return disableDownloadButton;
	}

	public void setDisableDownloadButton(boolean disableDownloadButton) {
		this.disableDownloadButton = disableDownloadButton;
	}

	public boolean isShowReportTypesByProjectTypes() {
		return showReportTypesByProjectTypes;
	}

	public void setShowReportTypesByProjectTypes(boolean showReportTypesByProjectTypes) {
		this.showReportTypesByProjectTypes = showReportTypesByProjectTypes;
	}

	public List<ReportRequestResponse> getResponseList() {
		return responseList;
	}

	public void setResponseList(List<ReportRequestResponse> responseList) {
		this.responseList = responseList;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	public int getReportRequestId() {
		return reportRequestId;
	}

	public void setReportRequestId(int reportRequestId) {
		this.reportRequestId = reportRequestId;
	}

	public List<ReportTypeObj> getSelectedReportTypes() {
		return selectedReportTypes;
	}

	public void setSelectedReportTypes(List<ReportTypeObj> selectedReportTypes) {
		this.selectedReportTypes = selectedReportTypes;
	}

	public int getActiveTabIndex() {
		return activeTabIndex;
	}

	public void setActiveTabIndex(int activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}
}
