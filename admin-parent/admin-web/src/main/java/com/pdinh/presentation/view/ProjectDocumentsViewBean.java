package com.pdinh.presentation.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="projectDocumentsViewBean")
@ViewScoped
public class ProjectDocumentsViewBean extends DocumentsViewBean {
	private static final long serialVersionUID = 1L;
}
