package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.AsynchronousAuditLoggerLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.CompanyProductDao;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.CourseRosterDao;
import com.pdinh.data.ms.dao.CourseVisibilityDao;
import com.pdinh.data.ms.dao.ProductContentDao;
import com.pdinh.data.ms.dao.ProductDao;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.dto.CompanyProductStatus;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.CompanyProduct;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseRoster;
import com.pdinh.persistence.ms.entity.CourseVisibility;
import com.pdinh.persistence.ms.entity.Product;
import com.pdinh.persistence.ms.entity.ProductContent;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ProductEnablementViewBean;
import com.sun.jersey.spi.inject.Inject;

@ManagedBean(name = "productEnablementControllerBean")
@ViewScoped
public class ProductEnablementControllerBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ProductEnablementControllerBean.class);

	
	@ManagedProperty(name="productEnablementViewBean", value="#{productEnablementViewBean}")
	private ProductEnablementViewBean productEnablementViewBean;
	
	@EJB
	private CompanyProductDao companyProductDao;
	
	@EJB
	private ProductDao productDao;
	
	@EJB
	private ProductContentDao productContentDao;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	
	@EJB
	private CompanyDao companyDao;
	
	@EJB
	private CourseDao courseDao;
	
	@EJB
	private CourseVisibilityDao courseVisibilityDao;
	
	@EJB
	private CourseRosterDao courseRosterDao;

	@EJB
	private AsynchronousAuditLoggerLocal auditLogger;
	
	
	public void initProductStatus(int companyId, String companyName){
		List <CompanyProductStatus> cps = new ArrayList<CompanyProductStatus>();
		cps = companyProductDao.getAllProductStatusForCompany(companyId);
		
		List <CompanyProductStatus> customStatuses = new ArrayList<CompanyProductStatus>();
		List <CompanyProductStatus> nonCustomStatuses = new ArrayList<CompanyProductStatus>();
		
		for (CompanyProductStatus s : cps){
			if (s.isCustom()){
				customStatuses.add(s);
				
			}else{
				nonCustomStatuses.add(s);
			}
			
		}
		productEnablementViewBean.setProductStatuses(nonCustomStatuses);
		productEnablementViewBean.setCustomStatuses(customStatuses);
		productEnablementViewBean.setCompanyId(companyId);
		productEnablementViewBean.setCompanyName(companyName);
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("productTable.unselectAllRows()");
		context.execute("productTable.clearFilters()");
		
	}
	
	public void handleRowSelect(SelectEvent event){
		log.debug("handling select event for : {}", event.getSource().getClass());
		CompanyProductStatus status = (CompanyProductStatus) event.getObject();
		if (status.isEnabled()){
			productEnablementViewBean.setDisableButtonDisabled(false);
			productEnablementViewBean.setEnableButtonDisabled(true);
		}else{
			productEnablementViewBean.setDisableButtonDisabled(true);
			productEnablementViewBean.setEnableButtonDisabled(false);
		}
		
		
		
	}
	
	public void handleEnableProduct(){
		CompanyProductStatus status = productEnablementViewBean.getSelectedProduct();
		Product product = productDao.findById(status.getProductId());
		Company company = companyDao.findById(productEnablementViewBean.getCompanyId());
		
		List<CourseVisibility> visibilities = courseVisibilityDao.findByCompanyBatchCourses(company.getCompanyId());
		List<CourseRoster> rosters = courseRosterDao.getCourseRostersByCompanyIdBatchCourse(company.getCompanyId());
		//List<CompanyProduct> coProdList = company.getCompanyProducts();
		CompanyProduct newProd = new CompanyProduct();
		newProd.setCompany(company);
		newProd.setProductCode(product.getCode());
		newProd = companyProductDao.create(newProd);
		log.debug("Created new CompanyProduct entity: {}", newProd.getId());
		//handle IA product
		if (product.getCode().equals("IA")){
			company.setIadviceFlag((short)1);
			companyDao.update(company);
		}
		List<CourseVisibility> newVisList = new ArrayList<CourseVisibility>();
		List<CourseRoster> newRosterList = new ArrayList<CourseRoster>();
		List<Integer> courseIdList = new ArrayList<Integer>();
		for (ProductContent content : product.getProductContent()){
			if (content.getProductContentType().getCode().equals("COURSE")){
				courseIdList.add(content.getContentId());
			}
		}
		List<Course> courses = courseDao.findCoursesIn(courseIdList);
		for (ProductContent content : product.getProductContent()){
			if (content.getProductContentType().getCode().equals("COURSE")){
				log.debug("Got new course from product content: {}", content.getContentId());
				Course theCourse = null;
				for (Course c : courses){
					if (c.getCourse() == content.getContentId()){
						theCourse = c;
						break;
					}
				}
				if (theCourse == null){
					log.error("Hmmm, this is awkward.. something is wrong with product content configuration for {}", product.getCode());
					break;
				}
				boolean exists = false;
				for (CourseVisibility v : visibilities){
					if (v.getCourse().getCourse() == content.getContentId()){
						exists = true;
					}
				}
				if (!(exists)){
					CourseVisibility cv = new CourseVisibility();
					
					//theCourse = courseDao.findById(content.getContentId());
					if (theCourse.getRestricted() == 0){
						cv.setCompany(company);
						cv.setCourse(theCourse);
						UUID guid = UUID.randomUUID();
						String rowguid = guid.toString();
						cv.setRowguid(rowguid);
						//courseVisibilityDao.create(cv);
						newVisList.add(cv);
						log.debug("Created Course Visibility entity for course: {}", theCourse.getAbbv());
					}else{
						log.debug("Skipped Restricted course: {}", theCourse.getAbbv());
					}
				}
				exists = false;
				for (CourseRoster c : rosters){
					if (c.getCourse().getCourse() == content.getContentId()){
						exists = true;
					}
				}
				if (!(exists)){
					CourseRoster cr = new CourseRoster();
					cr.setCompany(company);
					//theCourse = courseDao.findById(content.getContentId());
					if (theCourse.getRestricted() == 0){
						cr.setCourse(theCourse);
						UUID guid = UUID.randomUUID();
						String rowguid = guid.toString();
						cr.setRowguid(rowguid);
						//courseRosterDao.create(cr);
						newRosterList.add(cr);
						log.debug("Created Course Roster entity: {}", theCourse.getAbbv());
					}
				}
			}
			if (content.getProductContentType().getCode().equals("IA_SET")){
				log.debug("Got IA set: {}", content.getContentId());
				
				int rows = productContentDao.enableIASetContent(content.getContentId(), company.getCompanyId());
				log.debug("Created {} IA Set entity", rows);
			}
		}
		log.debug("About to create batch of {} visibilities", newVisList.size());
		courseVisibilityDao.createBatch(newVisList);
		log.debug("Finished creating batch visibilities");
		log.debug("About to create batch of {} rosters", newRosterList.size());
		courseRosterDao.createBatch(newRosterList);
		log.debug("Finished creating batch rosters");
		Subject subject = SecurityUtils.getSubject();
		auditLogger.doAudit("{} performed {} action on Company: {}({}), Product: {}", new Object[]{subject.getPrincipal(), EntityAuditLogger.OP_ENABLE_PRODUCT, company.getCoName(), company.getCompanyId(), product.getCode()});
		sessionBean.setCompany(company);
		this.initProductStatus(company.getCompanyId(), company.getCoName());
	}
	
	public void handleDisableProduct(){
		CompanyProductStatus status = productEnablementViewBean.getSelectedProduct();
		Product product = productDao.findById(status.getProductId());
		Company company = companyDao.findById(productEnablementViewBean.getCompanyId());
		List<CompanyProduct> coProdList = companyProductDao.findByCompanyIdAndProductCode(product.getCode(), company.getCompanyId());
		for (CompanyProduct p : coProdList){
			companyProductDao.delete(p);
		}
		log.debug("deleted company product for {}", product.getCode());
		if (product.getCode().equals("IA")){
			company.setIadviceFlag((short)0);
			companyDao.update(company);
		}
		//now handle content
		List<Integer> courseIdsToRemove = new ArrayList<Integer>();
		for (ProductContent pcon : product.getProductContent()){
			log.debug("looping through product content with content id: {}", pcon.getContentId());
			int result = productContentDao.getContentProductStatus(company.getCompanyId(), pcon.getContentId(), pcon.getProductContentType().getProductContentTypeId());
			log.debug("Got Product Status: {}", result);
			if (result >= 0){
				if (pcon.getProductContentType().getCode().equals("COURSE")){
					courseIdsToRemove.add(pcon.getContentId());
					//courseVisibilityDao.deleteByCompanyIdAndCourseId(company.getCompanyId(), pcon.getContentId());
					//courseRosterDao.deleteCourseRosterByCompanyIdAndCourseId(company.getCompanyId(), pcon.getContentId());
					log.debug("deleted course vis and roster for course {}", pcon.getContentId());
				}
				if (pcon.getProductContentType().getCode().equals("IA_SET")){
					//TODO: update util_add_ia_set to do removal
					log.debug("Need stored proc to remove IA_SET");
				}
			}
		}
		courseVisibilityDao.deleteByCompanyIdAndCourseIds(company.getCompanyId(), courseIdsToRemove);
		courseRosterDao.deleteCourseRosterByCompanyIdAndCourseIds(company.getCompanyId(), courseIdsToRemove);
		auditLogger.doAudit("{} performed {} action on Company: {}({}), Product: {}", new Object[]{SecurityUtils.getSubject().getPrincipal(), EntityAuditLogger.OP_DISABLE_PRODUCT, company.getCoName(), company.getCompanyId(), product.getCode()});
		sessionBean.setCompany(company);
		this.initProductStatus(company.getCompanyId(), company.getCoName());
	}


	public ProductEnablementViewBean getProductEnablementViewBean() {
		return productEnablementViewBean;
	}

	public void setProductEnablementViewBean(
			ProductEnablementViewBean productEnablementViewBean) {
		this.productEnablementViewBean = productEnablementViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	

}
