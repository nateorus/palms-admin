package com.pdinh.presentation.view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.ParticipantObj;

@ManagedBean(name = "participantClipboardViewBean")
@ViewScoped
public class ParticipantClipboardViewBean {

	private List<ParticipantObj> participants = new ArrayList<ParticipantObj>();
	private ParticipantObj[] selectedParticipants;
	private List<ParticipantObj> filteredParticipants = new ArrayList<ParticipantObj>();
	private Boolean removeParticipantsDisabled = true;
	private Boolean clearParticipantsDisabled = true;
	private Boolean createReportsDisabled = true;
	private Boolean renderProjects = true;
	private boolean reportUploadDisabled = true;
	private boolean renderReportUpload = true;
	private boolean downloadReportDisabled = true;
	private boolean renderDownloadReport = true;
	private boolean renderSendEmails = true;
	private boolean sendEmailsDisabled = true;

	// 0=participants; 1=participants&projects
	private Integer viewType = 1;

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setSelectedParticipants(ParticipantObj[] selectedParticipants) {
		this.selectedParticipants = selectedParticipants;
	}

	public ParticipantObj[] getSelectedParticipants() {
		return selectedParticipants;
	}

	public void setRemoveParticipantsDisabled(Boolean b) {
		this.removeParticipantsDisabled = b;
	}

	public Boolean getRemoveParticipantsDisabled() {
		return removeParticipantsDisabled;
	}

	public void setClearParticipantsDisabled(Boolean clearParticipantsDisabled) {
		this.clearParticipantsDisabled = clearParticipantsDisabled;
	}

	public Boolean getClearParticipantsDisabled() {
		return clearParticipantsDisabled;
	}

	public void removeParticipant(ParticipantObj participant) {
		for (ParticipantObj p : participants) {
			if (participant.getUserId().equals(p.getUserId())) {
				getParticipants().remove(p);
				return;
			}
		}
	}

	public Boolean getCreateReportsDisabled() {
		return createReportsDisabled;
	}

	public void setCreateReportsDisabled(Boolean createReportsDisabled) {
		this.createReportsDisabled = createReportsDisabled;
	}

	public Boolean getRenderProjects() {
		return renderProjects;
	}

	public void setRenderProjects(Boolean renderProjects) {
		this.renderProjects = renderProjects;
	}

	public Integer getViewType() {
		return viewType;
	}

	public void setViewType(Integer viewType) {
		this.viewType = viewType;
	}

	public boolean isReportUploadDisabled() {
		return reportUploadDisabled;
	}

	public void setReportUploadDisabled(boolean reportUploadDisabled) {
		this.reportUploadDisabled = reportUploadDisabled;
	}

	public boolean isRenderReportUpload() {
		return renderReportUpload;
	}

	public void setRenderReportUpload(boolean renderReportUpload) {
		this.renderReportUpload = renderReportUpload;
	}

	public boolean isDownloadReportDisabled() {
		return downloadReportDisabled;
	}

	public void setDownloadReportDisabled(boolean downloadReportDisabled) {
		this.downloadReportDisabled = downloadReportDisabled;
	}

	public boolean isRenderDownloadReport() {
		return renderDownloadReport;
	}

	public void setRenderDownloadReport(boolean renderDownloadReport) {
		this.renderDownloadReport = renderDownloadReport;
	}

	public List<ParticipantObj> getFilteredParticipants() {
		return filteredParticipants;
	}

	public void setFilteredParticipants(List<ParticipantObj> filteredParticipants) {
		this.filteredParticipants = filteredParticipants;
	}

	public boolean isRenderSendEmails() {
		return renderSendEmails;
	}

	public void setRenderSendEmails(boolean renderSendEmails) {
		this.renderSendEmails = renderSendEmails;
	}

	public boolean isSendEmailsDisabled() {
		return sendEmailsDisabled;
	}

	public void setSendEmailsDisabled(boolean sendEmailsDisabled) {
		this.sendEmailsDisabled = sendEmailsDisabled;
	}

}
