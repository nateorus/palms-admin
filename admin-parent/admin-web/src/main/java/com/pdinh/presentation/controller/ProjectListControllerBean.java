package com.pdinh.presentation.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.event.data.FilterEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.ProjectCloneServiceLocal;
import com.pdinh.data.jaxb.admin.SelfRegConfigRepresentation;
import com.pdinh.data.jaxb.admin.SelfRegListItemRepresentation;
import com.pdinh.data.jaxb.lrm.CopyLrmProjectResponse;
import com.pdinh.data.jaxb.lrm.CopyProjectServiceLocal;
import com.pdinh.data.jaxb.lrm.LrmObjectExistResponse;
import com.pdinh.data.jaxb.lrm.LrmObjectServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.OfficeLocationDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.OfficeLocation;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.helper.SelfRegistrationHelper;
import com.pdinh.presentation.helper.TeammemberHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ProjectListViewBean;

@ManagedBean(name = "projectListControllerBean")
@ViewScoped
public class ProjectListControllerBean {
	private static final Logger log = LoggerFactory.getLogger(ProjectListControllerBean.class);

	@EJB
	private ProjectDao projectDao;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private OfficeLocationDao officeLocationDao;

	// TODO Refactor common data (data used in more than one contxt) out of
	// ProjectSetupSingleton and into one that is not contexts specific
	@EJB
	private ProjectSetupSingleton projectSetupSingleton;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private CopyProjectServiceLocal copyProjectService;

	@EJB
	private AuthorizationServiceLocal authService;

	@EJB
	private LrmObjectServiceLocal lrmObjectjectServiceLocal;

	@EJB
	private ProjectCloneServiceLocal projectCloneService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "projectListViewBean", value = "#{projectListViewBean}")
	private ProjectListViewBean projectListViewBean;

	@ManagedProperty(name = "teammemberHelper", value = "#{teammemberHelper}")
	private TeammemberHelper teammemberHelper;

	@ManagedProperty(name = "auditLogControllerBean", value = "#{auditLogControllerBean}")
	private AuditLogControllerBean auditLogControllerBean;
	
	@ManagedProperty(name = "selfRegistrationHelper", value = "#{selfRegistrationHelper}")
	private SelfRegistrationHelper selfRegistrationHelper;

	private Boolean firstTime = true;
	private Boolean useCompanyPermissions = false;

	ProjectObj selectedProject;

	public void initView() {
		if (firstTime) {
			Subject subject = SecurityUtils.getSubject();

			boolean[] createProject = subject.isPermitted("CreateProject", EntityType.COMPANY_TYPE + ":CreateProject:"
					+ sessionBean.getCompany().getCompanyId());

			if (createProject[0] || createProject[1]) {
				projectListViewBean.setCreateProjectButtonDisabled(false);

			} else {
				projectListViewBean.setCreateProjectButtonDisabled(true);

			}

			getSessionBean().clearProject();
			enableToolbarButtons(false);
			log.debug("In ProjectListControllerBean initView");
			firstTime = false;
			// NOT sure why we would need to refresh client here. We would do
			// that when we updated or selected the client
			// sessionBean.refreshClient();
			sessionBean.refreshProjects(sessionBean.isIncludeClosedProjects());
		}
		handleResetFiltersDialog();
	}

	public void refreshView() {
		firstTime = true;
		initView();
	}

	public void enableToolbarButtons(Boolean enabled) {
		Subject subject = SecurityUtils.getSubject();
		// if disabling, ignore permissions
		boolean[] editProjectSetup = subject.isPermitted(
				EntityType.PROJECT_TYPE + ":editProjectSetup:" + sessionBean.getSelectedProject(), "editProjectSetup");
		boolean[] manageProject = subject.isPermitted(
				EntityType.PROJECT_TYPE + ":manageProject:" + sessionBean.getSelectedProject(), "manageProject");
		boolean[] manageProjectDelivery = subject.isPermitted(EntityType.PROJECT_TYPE + ":manageDelivery:"
				+ sessionBean.getSelectedProject(), "manageProject");
		boolean[] accessManageProjectEmail = subject.isPermitted(EntityType.PROJECT_TYPE + ":accessManageProjectEmail:"
				+ sessionBean.getSelectedProject(), "accessManageProjectEmail");
		boolean[] createProject = subject.isPermitted("CreateProject", EntityType.COMPANY_TYPE + ":CreateProject:"
				+ sessionBean.getCompany().getCompanyId());
		// TODO: setup reporting permissions
		boolean[] downloadReports = subject.isPermitted(
				EntityType.PROJECT_TYPE + ":allowchildren:" + sessionBean.getSelectedProject(), EntityType.PROJECT_TYPE
						+ ":downloadReports:" + sessionBean.getSelectedProject(), "downloadReports", "ViewAllProjects");

		ProjectObj selectedProject = projectListViewBean.getSelectedProject();
		String projectTypeCode = (selectedProject != null) ? selectedProject.getTypeCode() : "";

		boolean lrmProjectExist = false;
		if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.ERICSSON)) {
			lrmProjectExist = isLrmProjectExist();
		}

		// Check for Gold key on project, Or ViewAllProjects
		if (!enabled || (downloadReports[0] && downloadReports[1]) || (downloadReports[2] && downloadReports[3])) {
			projectListViewBean.setDownloadReportsButtonDisabled(!enabled);
		} else {
			projectListViewBean.setDownloadReportsButtonDisabled(enabled);
		}

		if (!enabled || createProject[0] || createProject[1]) {
			projectListViewBean.setCopyProjectDisabled(!enabled);
		} else {
			projectListViewBean.setCopyProjectDisabled(enabled);
		}

		if (!enabled || editProjectSetup[0] || editProjectSetup[1]) {
			projectListViewBean.setEditProjectDisabled(!enabled);
		} else {
			log.debug("Subject {} denied editProjectSetup permission", subject.getPrincipal());
			projectListViewBean.setEditProjectDisabled(enabled);

		}
		if (!enabled || manageProject[0] || manageProject[1]) {
			projectListViewBean.setManageProjectDisabled(!enabled);
			if (lrmProjectExist) {
				projectListViewBean.setGenerateBulkUploadTemplateDisabled(true);
			} else {
				projectListViewBean.setGenerateBulkUploadTemplateDisabled(!enabled);
			}
		} else {
			projectListViewBean.setManageProjectDisabled(enabled);
			if (lrmProjectExist) {
				projectListViewBean.setGenerateBulkUploadTemplateDisabled(true);
			} else {
				projectListViewBean.setGenerateBulkUploadTemplateDisabled(enabled);
			}
		}
		if (!enabled || accessManageProjectEmail[0] || accessManageProjectEmail[1]) {
			projectListViewBean.setManageProjectEmailDisabled(!enabled);
		} else {
			projectListViewBean.setManageProjectEmailDisabled(enabled);
		}

		if (!enabled || manageProjectDelivery[0] || manageProjectDelivery[1]) {
			if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.ERICSSON)) {
				projectListViewBean.setManageProjectDeliveryDisabled(!lrmProjectExist);
			} else {
				projectListViewBean.setManageProjectDeliveryDisabled(true);
			}
		} else {
			projectListViewBean.setManageProjectDeliveryDisabled(enabled);
		}

		// permissions handled by render tag in jsf
		projectListViewBean.setAuditLogButtonDisabled(!enabled);
		// TODO: add permission to copy project

		// projectListViewBean.setEditProjectDisabled(!enabled);
		handleResetFiltersButton();
	}

	// Actions

	public String handleNavManageProject() {
		Integer pid = projectListViewBean.getSelectedProject().getProjectId();
		Project project = projectDao.findBatchById(pid);
		sessionBean.setProject(project);
		log.debug("Selected project: {}", pid);
		return "projectManagement.xhtml?faces-redirect=true";
	}

	public String handleNavManageProjectEmail() {
		Integer pid = projectListViewBean.getSelectedProject().getProjectId();
		Project project = projectDao.findBatchById(pid);
		sessionBean.setProject(project);
		log.debug("Selected project: {}", pid);
		return "projectEmail.xhtml?faces-redirect=true";
	}

	public String handleManageProjectDelivery() {
		Subject subject = SecurityUtils.getSubject();
		int projectId = projectListViewBean.getSelectedProject().getProjectId();
		Project project = projectDao.findBatchById(projectId);
		sessionBean.setProject(project);
		sessionBean.setLrmProjectId(0);
		sessionBean.setImportJobId(0);
		EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_ACCESS_LRM_PROJECT_MANAGMENT,
						sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
		log.debug("Selected project: {}", projectId);
		return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/deliveryManagment.xhtml&sys=lrm";
	}

	public String handleNavManageImports() {
		Integer pid = projectListViewBean.getSelectedProject().getProjectId();
		Project project = projectDao.findBatchById(pid);
		sessionBean.setProject(project);
		log.debug("Selected project: {}", pid);
		return "importManagement.xhtml?faces-redirect=true";
	}

	public String handleCreateProject() {
		return "projectSetup.xhtml?faces-redirect=true&create=true";
	}

	public String handleEditProject() {
		Project project = projectDao.findBatchById(selectedProject.getProjectId());
		sessionBean.setProject(project);
		return "projectSetup.xhtml?faces-redirect=true&create=false";
	}

	public void handleRowSelect(SelectEvent event) {

		// TODO: cch: not sure if this is a good idea
		// selecting project is a 'deep' selection. It should probably
		// be done when the user clicks the button
		selectedProject = (ProjectObj) event.getObject();
		log.debug("Selected project: {}", selectedProject.getProjectId());
		sessionBean.setSelectedProject(selectedProject.getProjectId());
		sessionBean.updateReportCenterAvailability();
		String prjType = selectedProject.getTypeCode();
		if (prjType.equalsIgnoreCase(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| prjType.equalsIgnoreCase(ProjectType.ERICSSON)) {
			projectListViewBean.setIncludeAbyDDisabled(false);
			projectListViewBean.setRenderDeliveryName(true);
			projectListViewBean.setIncludeLrmDisabled(false);
		} else if (prjType.equalsIgnoreCase(ProjectType.GLGPS)) {
			projectListViewBean.setIncludeAbyDDisabled(true);
			projectListViewBean.setRenderDeliveryName(true);
			projectListViewBean.setIncludeLrmDisabled(false);
		} else {
			projectListViewBean.setIncludeAbyDDisabled(true);
			projectListViewBean.setIncludeLrmDisabled(true);
			projectListViewBean.setRenderDeliveryName(false);
		}

		enableToolbarButtons(true);

	}

	private boolean isLrmProjectExist() {
		boolean exist = false;
		int projectId = projectListViewBean.getSelectedProject().getProjectId();
		LrmObjectExistResponse prjExtResp = null;
		try {
			prjExtResp = lrmObjectjectServiceLocal.projectExist(String.valueOf(projectId));
		} catch (Exception e) {
			return false;
		}

		if (prjExtResp.getRequestResult().equalsIgnoreCase("SUCCESS") && prjExtResp.isExist()) {
			exist = true;
		}
		return exist;
	}

	public void handleRowUnselect(UnselectEvent event) {
		enableToolbarButtons(false);
		sessionBean.clearProject();
		sessionBean.setSelectedProject(null);
		log.debug("Selected project: null");
	}

	public void handleAuditLogInit() {
		ProjectObj proj = projectListViewBean.getSelectedProject();
		// if (sParticipants.length == 1) {
		List<String> loggerNames = new ArrayList<String>();
		loggerNames.add("AUDIT-ADMIN");
		loggerNames.add("AUDIT-LRM");
		loggerNames.add("AUDIT-AUTHORIZATION");
		loggerNames.add("AUDIT-SCORECARD");
		log.debug("about to search audit log");
		/*
		List<String> searchStrings = new ArrayList<String>();
		for (ParticipantObj p : sParticipants){
			searchStrings.add(p.getUserId().toString());
		}
		*/
		auditLogControllerBean.initLog2Strings(String.valueOf(proj.getProjectId()), proj.getName(), loggerNames);
	}

	// Getters/Setters
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setProjectListViewBean(ProjectListViewBean projectListViewBean) {
		this.projectListViewBean = projectListViewBean;
	}

	public ProjectListViewBean getProjectListViewBean() {
		return projectListViewBean;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	private Boolean isFilteredDataExist() {
		return (getSessionBean().getFilteredProjects() == null) ? false : true;
	}

	private void handleResetFiltersButton() {
		getProjectListViewBean().setResetFiltersDisabled(!isFilteredDataExist());
	}

	public void handleCloneDialogInit() {
		log.debug("Handle copying a project here");
		// set delivery offices
		List<OfficeLocationObj> offices = new ArrayList<OfficeLocationObj>();

		// List<OfficeLocation> oL =
		// officeLocationDao.findAllOfficeLocationsOrdered();
		List<OfficeLocation> oL = projectSetupSingleton.getOfficeLocations();
		log.debug("office location size: {}", oL.size());
		ProjectObj pobj = projectListViewBean.getSelectedProject();

		ExternalSubjectDataObj assignedScheduler = authService.getSubjectMetadata(pobj.getInternalSchedulerId());
		if (assignedScheduler == null) {
			log.debug("Assigned Scheduler is null");
		}

		ExternalSubjectDataObj assignedPmPc = authService.getSubjectMetadata(pobj.getAssignedPmPcId());
		if (assignedPmPc == null) {
			log.debug("Pmpc is null");
		}

		pobj.setAssignedPmPc(assignedPmPc);
		pobj.setInternalScheduler(assignedScheduler);
		Project project = projectDao.findById(pobj.getProjectId());
		OfficeLocation selectedlocation = project.getOfficeLocation();
		if (!(selectedlocation == null)) {
			projectListViewBean.setSelectedOfficeLocationId(selectedlocation.getId());
			pobj.setClientMgmtOffice(project.getOfficeLocation().getLocation());
		}

		if (project.getBillable() != null && project.getBillable()) {
			log.debug("Enabling validation");
			projectListViewBean.setProjectCodeRequired(true);
		} else {
			log.debug("Disabling validation");
			projectListViewBean.setProjectCodeRequired(false);
		}

		if (!(projectListViewBean.isIncludeLrmDisabled())) {
			projectListViewBean.setRenderAssignedPM(true);
			projectListViewBean.setRenderInternalScheduler(true);
			List<ExternalSubjectDataObj> assignedPms = new ArrayList<ExternalSubjectDataObj>();
			List<ExternalSubjectDataObj> schedulers = new ArrayList<ExternalSubjectDataObj>();
			assignedPms.addAll(teammemberHelper.getAssignedPmPcs());
			schedulers.addAll(teammemberHelper.getSchedulers());
			projectListViewBean.setSchedulers(schedulers);
			projectListViewBean.setPmpcs(assignedPms);
			if (!(assignedScheduler == null)) {
				log.debug("assigned Scheduler is not null");
				projectListViewBean.setSchedulerId(assignedScheduler.getUsername());
			}
			if (!(assignedPmPc == null)) {
				projectListViewBean.setAssignedPmId(assignedPmPc.getUsername());
			}
		} else {
			projectListViewBean.setRenderAssignedPM(false);
			projectListViewBean.setRenderInternalScheduler(false);
		}

		for (OfficeLocation o : oL) {
			OfficeLocationObj location = new OfficeLocationObj(o);

			offices.add(location);
		}
		projectListViewBean.setOfficeLocations(offices);
		projectListViewBean.setCopyPrjCodeValue("");
		projectListViewBean.setCopyPrjNameValue("Copy Of " + pobj.getName());
		projectListViewBean.setCopyPrjDeliveryName("");

		projectListViewBean.setCopyAbydStatus("Not Attempted");
		projectListViewBean.setCopyProjectStatus("Not Attempted");
		projectListViewBean.setCopyLrmStatus("Not Attempted");

	}

	public void handleSelectIncludeLrm(ValueChangeEvent e) {
		log.debug("New Value: {}", e.getNewValue());
		if ((Boolean) e.getNewValue() == true) {
			projectListViewBean.setProjectDeliveryNameDisabled(false);

		} else {
			projectListViewBean.setProjectDeliveryNameDisabled(true);
		}
	}

	public String handleDoubleClickRow() {

		if (!projectListViewBean.getManageProjectDisabled()) {
			return this.handleNavManageProject();
		} else {
			return "projectList?faces-redirect=true";
		}
	}

	public String handleGoToManage() {
		log.debug("Clone the project here, then go to Project Management");
		Project newProject = projectDao.findById(Integer.valueOf(projectListViewBean.getNewProjectId()));
		if (newProject == null) {
			log.debug("In handleSaveAndManage(, New Project is NULL");
		}
		sessionBean.setSelectedProject(Integer.valueOf(projectListViewBean.getNewProjectId()));
		sessionBean.setProject(newProject);
		return "projectManagement.xhtml?faces-redirect=true";
	}

	public String handleGoToDelivery() {
		log.debug("Clone the project here, then go to Lrm, Project Logistics, Delivery, or whatever you call it");

		Project newProject = projectDao.findById(Integer.valueOf(projectListViewBean.getNewProjectId()));
		if (newProject == null) {
			log.debug("In handleSaveAndGoToDelivery(, New Project is NULL");
		}
		sessionBean.setSelectedProject(Integer.valueOf(projectListViewBean.getNewProjectId()));
		sessionBean.setProject(newProject);
		log.debug("About to post to LRM");
		return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/deliverySetup.xhtml&sys=lrm&returnTo=admin_proj_mgmt";

	}

	private boolean validateClone() {
		boolean valid = true;

		if (projectListViewBean.isIncludeLrm()) {
			if (projectListViewBean.getCopyPrjDeliveryName() != null
					&& StringUtils.isEmpty(projectListViewBean.getCopyPrjDeliveryName())) {
				LabelUtils.addErrorMessage("msgs", "Delivery Name in Contract/SOW:", "Please enter a delivery name.");
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
				valid = false;
			}
		}

		if (projectListViewBean.getSelectedOfficeLocationId() == null) {
			LabelUtils.addErrorMessage("msgs", "Client Mgmt Office:", "Please select client management office.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		if (projectListViewBean.isRenderInternalScheduler()) {
			if (projectListViewBean.getSchedulerId() == null) {
				LabelUtils.addErrorMessage("msgs", "Internal Scheduler:", "Please select internal scheduler.");
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
				valid = false;
			}
		}

		if (projectListViewBean.isRenderAssignedPM()) {
			if (projectListViewBean.getAssignedPmId() == null) {
				LabelUtils.addErrorMessage("msgs", "Assigned PM/PC:", "Please select assigned pm/pc.");
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
				valid = false;
			}
		}

		return valid;
	}

	public void handleClone() {

		if (!this.validateClone()) {
			return;
		}

		log.debug("Clone the project here, then reload this page and stay right here");
		Subject subject = SecurityUtils.getSubject();
		ProjectObj obj = projectListViewBean.getSelectedProject();

		Project project = projectDao.findById(obj.getProjectId());
		if (project == null) {
			log.debug("Project is NULL!!!!!!!!!!");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("The Selected project was not found", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"The selected Project could not be copied", "Project lookup failed for id: " + obj.getProjectId()));
			projectListViewBean.setCopyProjectStatus("FAILED:  the source project could not be found");
			return;
		}
		Project newproject = new Project(project);

		newproject.setDateCreated(new Timestamp(System.currentTimeMillis()));
		newproject.setExtAdminId((String) subject.getPrincipal());
		newproject.setName(projectListViewBean.getCopyPrjNameValue());
		newproject.setProjectCode(projectListViewBean.getCopyPrjCodeValue());
		newproject.setOfficeLocation(officeLocationDao.findById(projectListViewBean.getSelectedOfficeLocationId()));
		newproject.setClonedFromProjectId(project.getProjectId());

		PalmsSubject scheduler = null;
		if (projectListViewBean.isRenderInternalScheduler()) {
			scheduler = teammemberHelper.findSubjectByPrincipalAndRealmName(projectListViewBean.getSchedulerId());
			newproject.setInternalSchedulerId(scheduler.getSubject_id());
		}

		PalmsSubject pmpc = null;
		if (projectListViewBean.isRenderAssignedPM()) {
			pmpc = teammemberHelper.findSubjectByPrincipalAndRealmName(projectListViewBean.getAssignedPmId());
			newproject.setAssignedPmId(pmpc.getSubject_id());
		}

		newproject = projectDao.create(newproject);
		if (newproject.getAssignedPmId() > 0) {
			authService.grantProjectAccess(pmpc, newproject, subject.getPrincipal().toString(), null,
					EntityRule.ALLOW_CHILDREN);
		}

		if (newproject.getInternalSchedulerId() > 0) {
			authService.grantProjectAccess(scheduler, newproject, subject.getPrincipal().toString(), null,
					EntityRule.ALLOW_CHILDREN);
		}
		projectDao.evictAll();
		projectListViewBean.setCopyProjectStatus("Succeeded");
		projectListViewBean.setSaveAndGoToDeliveryDisabled(true);
		
		// -- Begin handle clone self Registration configuration
		try {
			SelfRegConfigRepresentation config = selfRegistrationHelper.getSelfRegConfig(obj.getProjectId());
			if (config.isEnabled()){
				SelfRegConfigRepresentation newConfig = selfRegistrationHelper.getCreateSelfRegConfig(newproject.getProjectId());
				selfRegistrationHelper.enableSelfRegistration(newConfig.getSelfRegConfigId(), true, config.getRegistrationLimit());
				List<String> allowedDomains = new ArrayList<String>();
				for (SelfRegListItemRepresentation domain : config.getAllowedEmailDomains()){
					allowedDomains.add(domain.getValue());
				}
				if (allowedDomains.size() > 0){
					selfRegistrationHelper.addAllowedDomains(newConfig.getSelfRegConfigId(), allowedDomains);
				}
			}
		} catch (PalmsException e) {
			//not enabled.  do nothing
		}
		// -- End handle self reg
		if (projectListViewBean.isIncludeLrm()) {

			projectListViewBean.setCopyLrmStatus("Not Started");
			if (projectListViewBean.getCopyPrjDeliveryName().length() < 1) {
				projectListViewBean.setCopyLrmStatus("FAILED:  Delivery name is required for copying LRM");

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage("The Selected project was not found", new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Delivery Name is required for copying Delivery Logistics Setup",
						"Delivery name was not provided"));

			} else {
				CopyLrmProjectResponse resp = copyProjectService.copyLrmProject(sessionBean.getRoleContext()
						.getSubject().getSubject_id(), project.getProjectId(), newproject.getProjectId(),
						projectListViewBean.getCopyPrjDeliveryName());

				if (resp.getRequestResult().equalsIgnoreCase("FAILED")) {
					projectListViewBean.setCopyLrmStatus("FAILED: " + resp.getMessage());

				} else {
					projectListViewBean.setCopyLrmStatus("Succeeded");
					projectListViewBean.setSaveAndGoToDeliveryDisabled(false);
				}
				log.debug("Response Status:  {}", resp.getRequestResult());
			}

		}

		if (projectListViewBean.isIncludeAbydSetup()) {
			// do abyd copy here
			projectListViewBean.setCopyAbydStatus("Not Started");
			if (copyProjectService.copyAbyDSetup(project.getProjectId(), newproject.getProjectId())) {
				projectListViewBean.setCopyAbydStatus("Succeeded");
			} else {
				projectListViewBean.setCopyAbydStatus("FAILED");
			}
		}

		// Check if we need to clone the custom fields
		if (projectListViewBean.isIncludeCustomFields()) {
			projectCloneService.cloneProjectCustomFields(sessionBean.getSubject_id(), project.getCompany()
					.getCompanyId(), project.getProjectId(), newproject.getProjectId());
		}

		projectListViewBean.setNewProjectId(String.valueOf(newproject.getProjectId()));
		projectListViewBean.setNewProjectName(newproject.getName());
		refreshView();

	}

	private void handleResetFiltersDialog() {
		getProjectListViewBean().setShowResetFiltersDialog(isFilteredDataExist());
	}

	public void handleDataTableFiltering(FilterEvent event) {
		handleResetFiltersButton();
		enableToolbarButtons(false);
		sessionBean.clearProject();
	}

	public void handleToggleIncludeClosedProjects() {
		boolean includeClosed = sessionBean.isIncludeClosedProjects();
		sessionBean.setIncludeClosedProjects(!includeClosed);
		refreshView();
	}

	public void resetFilteredProjects(ActionEvent event) {
		getSessionBean().setFilteredProjects(null);
		getProjectListViewBean().setResetFiltersDisabled(true);
		getProjectListViewBean().setShowResetFiltersDialog(false);
		getProjectListViewBean().setSearchAllFields(null);
		log.debug("Resetting 'Projects List' filters to null");
	}

	public Boolean getUseCompanyPermissions() {
		return useCompanyPermissions;
	}

	public void setUseCompanyPermissions(Boolean useCompanyPermissions) {
		this.useCompanyPermissions = useCompanyPermissions;
	}

	public TeammemberHelper getTeammemberHelper() {
		return teammemberHelper;
	}

	public void setTeammemberHelper(TeammemberHelper teammemberHelper) {
		this.teammemberHelper = teammemberHelper;
	}

	public AuditLogControllerBean getAuditLogControllerBean() {
		return auditLogControllerBean;
	}

	public void setAuditLogControllerBean(AuditLogControllerBean auditLogControllerBean) {
		this.auditLogControllerBean = auditLogControllerBean;
	}

	public SelfRegistrationHelper getSelfRegistrationHelper() {
		return selfRegistrationHelper;
	}

	public void setSelfRegistrationHelper(
			SelfRegistrationHelper selfRegistrationHelper) {
		this.selfRegistrationHelper = selfRegistrationHelper;
	}

}
