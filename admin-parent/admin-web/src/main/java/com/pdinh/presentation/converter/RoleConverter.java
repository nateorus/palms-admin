package com.pdinh.presentation.converter;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.RoleDao;
import com.pdinh.auth.persistence.entity.Role;

@ManagedBean(name = "roleConverter")
@ViewScoped
@FacesConverter(value = "roleConverter")
public class RoleConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RoleConverter.class);
	
	@EJB
	private RoleDao roleDao;
	
	public RoleConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		
		if (arg2 != null && (!(arg2.equals(String.valueOf(0)))) && (!(arg2.equals(""))) ){
			int pid = Integer.valueOf(arg2);
			log.trace("In getAsObject method of RoleConverter.  Argument: {}", pid);
			
			Role r = roleDao.findById(pid);
			log.trace("Returned Role Object: {}", r.getName());
			return r;

		}else{
			log.debug("Returning Null from RoleConverter getAsObjectMethod");
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		if (arg2 instanceof Role){
			String pid = "" + ((Role) arg2).getRole_id();
			log.trace("In getAsString method of RoleConverter.  Argument: {}", pid);
			return pid;
		} else {
			log.debug("Returning Null from RoleConverter getAsString class");
			return null;
		}
		
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
