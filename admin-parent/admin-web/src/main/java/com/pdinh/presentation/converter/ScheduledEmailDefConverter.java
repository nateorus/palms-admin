package com.pdinh.presentation.converter;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.presentation.view.EditPasswordPoliciesViewBean;

@ManagedBean(name = "scheduledEmailDefConverter")
@ViewScoped
public class ScheduledEmailDefConverter implements Converter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ScheduledEmailDefConverter.class);
	
	@EJB
	private ScheduledEmailDefDao scheduledEmailDefDao;
	
	@ManagedProperty(name = "editPasswordPoliciesViewBean", value = "#{editPasswordPoliciesViewBean}")
	private EditPasswordPoliciesViewBean editPasswordPoliciesViewBean;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2 != null){
			int pid = Integer.valueOf(arg2);
			log.debug("In getAsObject method of permissionConverter.  Argument: {}", pid);
			List<ScheduledEmailDef> defList = editPasswordPoliciesViewBean.getAvailableEmails();
			for (ScheduledEmailDef def:defList){
				if (def.getId() == pid){
					log.debug("Returning ScheduledEmail Object: {}", def.getTitle());
					return def;
				}
			}
			log.debug("Returning empty email object");
			
			return editPasswordPoliciesViewBean.getEmptyEmailTemplate();

		}
		return null;

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null){
			String pid = "" + ((ScheduledEmailDef) value).getId();
			log.debug("In getAsString method of ScheduledEmailDefConverter.  Argument: {}", pid);
			return pid;
		}
		return null;
	}

	public EditPasswordPoliciesViewBean getEditPasswordPoliciesViewBean() {
		return editPasswordPoliciesViewBean;
	}

	public void setEditPasswordPoliciesViewBean(
			EditPasswordPoliciesViewBean editPasswordPoliciesViewBean) {
		this.editPasswordPoliciesViewBean = editPasswordPoliciesViewBean;
	}

}
