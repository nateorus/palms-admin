package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.persistence.ms.entity.CompanyPasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;

@ManagedBean(name = "editPasswordPoliciesViewBean")
@ViewScoped
public class EditPasswordPoliciesViewBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CompanyPasswordPolicy> policies;
	private List<ScheduledEmailDef> availableEmails;
	private ScheduledEmailDef selectedEmailTemplate;
	private ScheduledEmailDef emptyEmailTemplate = new ScheduledEmailDef();
	private String selectedTypeId = "";
	private boolean saveButtonDisabled = true;
	private boolean customizeButtonDisabled = true;
	private boolean resetButtonDisabled = true;
	private CompanyPasswordPolicy selectedPolicy;
	private PasswordPolicy newPolicy;
	private String policyDescription = "";
	private int termInDays = 0;
	private int allowedChanges = 0;
	private int allowedChangesPeriodMinutes = 0;
	private int unsuccessfulAttempts = 0;
	private int unsuccessfulAttemptsPeriodMinutes = 0;
	private int maxPasswordsInHistory = 0;
	private int notificationDaysPriorToExpiration = 0;
	private int reminderNotificationPeriodDays = 0;
	private int lockoutDurationMinutes = 0;
	private String validationCustomRegex = "";
	private boolean generateRandom = false;
	private boolean isTemplate = false;
	private int minimumLength = 0;
	private int maximumLength = 0;
	private boolean requireLowercase = false;
	private boolean requireUppercase = false;
	private boolean requireNumeric = false;
	private boolean requireSpecial = false;
	
	private boolean descriptionInputDisabled = true;
	private boolean termInDaysInputDisabled = true;
	private boolean allowedChangesInputDisabled = true;
	private boolean allowedChangesPeriodMinutesInputDisabled = true;
	private boolean unsuccessfulAttemptsInputDisabled = true;
	private boolean unsuccessfulAttemptsPeriodMinutesInputDisabled = true;
	private boolean maxPasswordsInHistoryInputDisabled = true;
	private boolean notificationDaysPriorToExpirationInputDisabled = true;
	private boolean reminderNotificationPeriodDaysInputDisabled = true;
	private boolean lockoutDurationMinutesInputDisabled = true;
	private boolean validationCustomRegexInputDisabled = true;
	private boolean generateRandomInputDisabled = true;
	private boolean minimumLengthInputDisabled = true;
	private boolean maximumLengthInputDisabled = true;
	private boolean requireLowercaseInputDisabled = true;
	private boolean requireUppercaseInputDisabled = true;
	private boolean requireNumericInputDisabled = true;
	private boolean requireSpecialInputDisabled = true;
	private boolean emailTemplateInputDisabled = true;

	public List<CompanyPasswordPolicy> getPolicies() {
		return policies;
	}

	public void setPolicies(List<CompanyPasswordPolicy> policies) {
		this.policies = policies;
	}

	public String getSelectedTypeId() {
		return selectedTypeId;
	}

	public void setSelectedTypeId(String selectedTypeId) {
		this.selectedTypeId = selectedTypeId;
	}

	public CompanyPasswordPolicy getSelectedPolicy() {
		return selectedPolicy;
	}

	public void setSelectedPolicy(CompanyPasswordPolicy selectedPolicy) {
		this.selectedPolicy = selectedPolicy;
	}

	public String getPolicyDescription() {
		return policyDescription;
	}

	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}

	public int getTermInDays() {
		return termInDays;
	}

	public void setTermInDays(int termInDays) {
		this.termInDays = termInDays;
	}

	public int getAllowedChanges() {
		return allowedChanges;
	}

	public void setAllowedChanges(int allowedChanges) {
		this.allowedChanges = allowedChanges;
	}

	public int getAllowedChangesPeriodMinutes() {
		return allowedChangesPeriodMinutes;
	}

	public void setAllowedChangesPeriodMinutes(int allowedChangesPeriodMinutes) {
		this.allowedChangesPeriodMinutes = allowedChangesPeriodMinutes;
	}

	public int getUnsuccessfulAttempts() {
		return unsuccessfulAttempts;
	}

	public void setUnsuccessfulAttempts(int unsuccessfulAttempts) {
		this.unsuccessfulAttempts = unsuccessfulAttempts;
	}

	public int getUnsuccessfulAttemptsPeriodMinutes() {
		return unsuccessfulAttemptsPeriodMinutes;
	}

	public void setUnsuccessfulAttemptsPeriodMinutes(
			int unsuccessfulAttemptsPeriodMinutes) {
		this.unsuccessfulAttemptsPeriodMinutes = unsuccessfulAttemptsPeriodMinutes;
	}

	public int getMaxPasswordsInHistory() {
		return maxPasswordsInHistory;
	}

	public void setMaxPasswordsInHistory(int maxPasswordsInHistory) {
		this.maxPasswordsInHistory = maxPasswordsInHistory;
	}

	public int getNotificationDaysPriorToExpiration() {
		return notificationDaysPriorToExpiration;
	}

	public void setNotificationDaysPriorToExpiration(
			int notificationDaysPriorToExpiration) {
		this.notificationDaysPriorToExpiration = notificationDaysPriorToExpiration;
	}

	public int getReminderNotificationPeriodDays() {
		return reminderNotificationPeriodDays;
	}

	public void setReminderNotificationPeriodDays(int reminderNotificationPeriodDays) {
		this.reminderNotificationPeriodDays = reminderNotificationPeriodDays;
	}

	public int getLockoutDurationMinutes() {
		return lockoutDurationMinutes;
	}

	public void setLockoutDurationMinutes(int lockoutDurationMinutes) {
		this.lockoutDurationMinutes = lockoutDurationMinutes;
	}

	public String getValidationCustomRegex() {
		return validationCustomRegex;
	}

	public void setValidationCustomRegex(String validationCustomRegex) {
		this.validationCustomRegex = validationCustomRegex;
	}



	public boolean isTemplate() {
		return isTemplate;
	}

	public void setTemplate(boolean isTemplate) {
		this.isTemplate = isTemplate;
	}

	public int getMinimumLength() {
		return minimumLength;
	}

	public void setMinimumLength(int minimumLength) {
		this.minimumLength = minimumLength;
	}

	public int getMaximumLength() {
		return maximumLength;
	}

	public void setMaximumLength(int maximumLength) {
		this.maximumLength = maximumLength;
	}

	public boolean isRequireLowercase() {
		return requireLowercase;
	}

	public void setRequireLowercase(boolean requireLowercase) {
		this.requireLowercase = requireLowercase;
	}

	public boolean isRequireUppercase() {
		return requireUppercase;
	}

	public void setRequireUppercase(boolean requireUppercase) {
		this.requireUppercase = requireUppercase;
	}

	public boolean isRequireNumeric() {
		return requireNumeric;
	}

	public void setRequireNumeric(boolean requireNumeric) {
		this.requireNumeric = requireNumeric;
	}

	public boolean isRequireSpecial() {
		return requireSpecial;
	}

	public void setRequireSpecial(boolean requireSpecial) {
		this.requireSpecial = requireSpecial;
	}

	public boolean isGenerateRandom() {
		return generateRandom;
	}

	public void setGenerateRandom(boolean generateRandom) {
		this.generateRandom = generateRandom;
	}

	public boolean isDescriptionInputDisabled() {
		return descriptionInputDisabled;
	}

	public void setDescriptionInputDisabled(boolean descriptionInputDisabled) {
		this.descriptionInputDisabled = descriptionInputDisabled;
	}

	public boolean isTermInDaysInputDisabled() {
		return termInDaysInputDisabled;
	}

	public void setTermInDaysInputDisabled(boolean termInDaysInputDisabled) {
		this.termInDaysInputDisabled = termInDaysInputDisabled;
	}

	public boolean isAllowedChangesInputDisabled() {
		return allowedChangesInputDisabled;
	}

	public void setAllowedChangesInputDisabled(boolean allowedChangesInputDisabled) {
		this.allowedChangesInputDisabled = allowedChangesInputDisabled;
	}

	public boolean isUnsuccessfulAttemptsInputDisabled() {
		return unsuccessfulAttemptsInputDisabled;
	}

	public void setUnsuccessfulAttemptsInputDisabled(
			boolean unsuccessfulAttemptsInputDisabled) {
		this.unsuccessfulAttemptsInputDisabled = unsuccessfulAttemptsInputDisabled;
	}

	public boolean isUnsuccessfulAttemptsPeriodMinutesInputDisabled() {
		return unsuccessfulAttemptsPeriodMinutesInputDisabled;
	}

	public void setUnsuccessfulAttemptsPeriodMinutesInputDisabled(
			boolean unsuccessfulAttemptsPeriodMinutesInputDisabled) {
		this.unsuccessfulAttemptsPeriodMinutesInputDisabled = unsuccessfulAttemptsPeriodMinutesInputDisabled;
	}

	public boolean isMaxPasswordsInHistoryInputDisabled() {
		return maxPasswordsInHistoryInputDisabled;
	}

	public void setMaxPasswordsInHistoryInputDisabled(
			boolean maxPasswordsInHistoryInputDisabled) {
		this.maxPasswordsInHistoryInputDisabled = maxPasswordsInHistoryInputDisabled;
	}

	public boolean isNotificationDaysPriorToExpirationInputDisabled() {
		return notificationDaysPriorToExpirationInputDisabled;
	}

	public void setNotificationDaysPriorToExpirationInputDisabled(
			boolean notificationDaysPriorToExpirationInputDisabled) {
		this.notificationDaysPriorToExpirationInputDisabled = notificationDaysPriorToExpirationInputDisabled;
	}

	public boolean isReminderNotificationPeriodDaysInputDisabled() {
		return reminderNotificationPeriodDaysInputDisabled;
	}

	public void setReminderNotificationPeriodDaysInputDisabled(
			boolean reminderNotificationPeriodDaysInputDisabled) {
		this.reminderNotificationPeriodDaysInputDisabled = reminderNotificationPeriodDaysInputDisabled;
	}

	public boolean isLockoutDurationMinutesInputDisabled() {
		return lockoutDurationMinutesInputDisabled;
	}

	public void setLockoutDurationMinutesInputDisabled(
			boolean lockoutDurationMinutesInputDisabled) {
		this.lockoutDurationMinutesInputDisabled = lockoutDurationMinutesInputDisabled;
	}

	public boolean isValidationCustomRegexInputDisabled() {
		return validationCustomRegexInputDisabled;
	}

	public void setValidationCustomRegexInputDisabled(
			boolean validationCustomRegexInputDisabled) {
		this.validationCustomRegexInputDisabled = validationCustomRegexInputDisabled;
	}

	public boolean isGenerateRandomInputDisabled() {
		return generateRandomInputDisabled;
	}

	public void setGenerateRandomInputDisabled(boolean generateRandomInputDisabled) {
		this.generateRandomInputDisabled = generateRandomInputDisabled;
	}

	public boolean isMinimumLengthInputDisabled() {
		return minimumLengthInputDisabled;
	}

	public void setMinimumLengthInputDisabled(boolean minimumLengthInputDisabled) {
		this.minimumLengthInputDisabled = minimumLengthInputDisabled;
	}

	public boolean isMaximumLengthInputDisabled() {
		return maximumLengthInputDisabled;
	}

	public void setMaximumLengthInputDisabled(boolean maximumLengthInputDisabled) {
		this.maximumLengthInputDisabled = maximumLengthInputDisabled;
	}

	public boolean isRequireLowercaseInputDisabled() {
		return requireLowercaseInputDisabled;
	}

	public void setRequireLowercaseInputDisabled(
			boolean requireLowercaseInputDisabled) {
		this.requireLowercaseInputDisabled = requireLowercaseInputDisabled;
	}

	public boolean isRequireUppercaseInputDisabled() {
		return requireUppercaseInputDisabled;
	}

	public void setRequireUppercaseInputDisabled(
			boolean requireUppercaseInputDisabled) {
		this.requireUppercaseInputDisabled = requireUppercaseInputDisabled;
	}

	public boolean isRequireNumericInputDisabled() {
		return requireNumericInputDisabled;
	}

	public void setRequireNumericInputDisabled(boolean requireNumericInputDisabled) {
		this.requireNumericInputDisabled = requireNumericInputDisabled;
	}

	public boolean isRequireSpecialInputDisabled() {
		return requireSpecialInputDisabled;
	}

	public void setRequireSpecialInputDisabled(boolean requireSpecialInputDisabled) {
		this.requireSpecialInputDisabled = requireSpecialInputDisabled;
	}

	public boolean isAllowedChangesPeriodMinutesInputDisabled() {
		return allowedChangesPeriodMinutesInputDisabled;
	}

	public void setAllowedChangesPeriodMinutesInputDisabled(
			boolean allowedChangesPeriodMinutesInputDisabled) {
		this.allowedChangesPeriodMinutesInputDisabled = allowedChangesPeriodMinutesInputDisabled;
	}

	public boolean isSaveButtonDisabled() {
		return saveButtonDisabled;
	}

	public void setSaveButtonDisabled(boolean saveButtonDisabled) {
		this.saveButtonDisabled = saveButtonDisabled;
	}

	public boolean isCustomizeButtonDisabled() {
		return customizeButtonDisabled;
	}

	public void setCustomizeButtonDisabled(boolean customizeButtonDisabled) {
		this.customizeButtonDisabled = customizeButtonDisabled;
	}

	public PasswordPolicy getNewPolicy() {
		return newPolicy;
	}

	public void setNewPolicy(PasswordPolicy newPolicy) {
		this.newPolicy = newPolicy;
	}

	public boolean isResetButtonDisabled() {
		return resetButtonDisabled;
	}

	public void setResetButtonDisabled(boolean resetButtonDisabled) {
		this.resetButtonDisabled = resetButtonDisabled;
	}

	public List<ScheduledEmailDef> getAvailableEmails() {
		return availableEmails;
	}

	public void setAvailableEmails(List<ScheduledEmailDef> availableEmails) {
		this.availableEmails = availableEmails;
	}

	public ScheduledEmailDef getSelectedEmailTemplate() {
		return selectedEmailTemplate;
	}

	public void setSelectedEmailTemplate(ScheduledEmailDef selectedEmailTemplate) {
		this.selectedEmailTemplate = selectedEmailTemplate;
	}

	public ScheduledEmailDef getEmptyEmailTemplate() {
		return emptyEmailTemplate;
	}

	public void setEmptyEmailTemplate(ScheduledEmailDef emptyEmailTemplate) {
		this.emptyEmailTemplate = emptyEmailTemplate;
	}

	public boolean isEmailTemplateInputDisabled() {
		return emailTemplateInputDisabled;
	}

	public void setEmailTemplateInputDisabled(boolean emailTemplateInputDisabled) {
		this.emailTemplateInputDisabled = emailTemplateInputDisabled;
	}


}
