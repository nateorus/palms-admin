package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.manage.emailtemplates.EmailTemplatesService;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.helper.TextUtils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EmailTemplatesListCompViewBean;

@ManagedBean(name = "emailTemplateListControllerBean")
@ViewScoped
public class EmailTemplateListControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EmailTemplateListControllerBean.class);

	@EJB
	EmailTemplatesService emailTemplatesService;

	@EJB
	MailTemplatesServiceLocalImpl mailTemplatesService;

	@EJB
	ProjectDao projectDao;

	@ManagedProperty(name = "emailTemplatesListCompViewBean", value = "#{emailTemplatesListCompViewBean}")
	public EmailTemplatesListCompViewBean emailTemplatesListCompViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@Resource(mappedName = "custom/tlt_properties")
	private Properties tltProperties;

	private boolean firstTime = true;
	private boolean clientLevel = false;
	private boolean projectLevel = false;

	public void initForReadOnlyView() {

		// getSessionBean().clearProject(); // this breaks stuff when sending
		// emails from project managment, which uses the session getProject all
		// over the place

		Company company = sessionBean.getCompany();
		Project project = sessionBean.getProject();

		log.debug("initForReadOnlyView > company = {}", company);
		log.debug("initForReadOnlyView > project = {}", project);

		if (company != null && project == null) {
			setClientLevel(true);
			setProjectLevel(false);
		} else if (company != null && project != null) {
			setClientLevel(false);
			setProjectLevel(true);
		}

		if (isFirstTime()) {
			setFirstTime(false);
			List<EmailTemplateObj> templates = new ArrayList<EmailTemplateObj>();
			if (isClientLevel()) {
				templates = getListOfEmailTemplates(emailTemplatesService.getEmailTemplatesByLevel(
						ScheduledEmailDef.CLIENT, sessionBean.getCompany(), null));
			} else if (isProjectLevel()) {
				templates = getListOfEmailTemplates(emailTemplatesService.getEmailTemplatesByLevel(
						ScheduledEmailDef.PROJ, sessionBean.getCompany(), sessionBean.getProject()));
			}
			emailTemplatesListCompViewBean.setTemplates(templates);
		}

		emailTemplatesListCompViewBean.setSelectedTemplate(null);
		emailTemplatesListCompViewBean.setCurrentLanguage("en");

		emailTemplatesListCompViewBean.setFilteredTemplates(null);
		emailTemplatesListCompViewBean.setShowEmailPreview(false);

	}

	public void refreshView() {
		setFirstTime(true);
		initForReadOnlyView();
	}

	private List<EmailTemplateObj> getListOfEmailTemplates(List<ScheduledEmailDef> templates) {
		List<EmailTemplateObj> emailTemplates = new ArrayList<EmailTemplateObj>();
		for (ScheduledEmailDef sed : templates) {
			emailTemplates.add(emailTemplatesService.scheduledEmailDef2EmailTemplateObj(sed));
		}
		return emailTemplates;
	}

	public void handleCurrentTemplateChange(SelectEvent evt) {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		EmailTextObj defaultText = emailTemplatesService.findEmailTextByLangCode(template, "en");

		emailTemplatesListCompViewBean.setCurrentEmailLangs(emailTemplatesService.getTemplateLanguges(template));
		emailTemplatesListCompViewBean.setCurrentLanguage("en");
		emailTemplatesListCompViewBean.setEmailSubject(defaultText.getSubject());
		emailTemplatesListCompViewBean.setEmailBody(TextUtils.getHtmlText(defaultText.getEmailText()));
		emailTemplatesListCompViewBean.setShowEmailPreview(true);
	}

	public void handleCurrentTemplateUnselect(UnselectEvent evt) {
		emailTemplatesListCompViewBean.setShowEmailPreview(false);
		emailTemplatesListCompViewBean.setShowEmailPanel(true);
		emailTemplatesListCompViewBean.setSelectedTemplate(null);
		emailTemplatesListCompViewBean.setFilteredTemplates(null);
	}

	public void handleLanguageChange(AjaxBehaviorEvent evt) {
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		String currentLanguage = emailTemplatesListCompViewBean.getCurrentLanguage();
		EmailTextObj currentText = emailTemplatesService.findEmailTextByLangCode(template, currentLanguage);
		currentText.setEmailText(TextUtils.getHtmlText(currentText.getEmailText()));
		emailTemplatesListCompViewBean.setEmailSubject(currentText.getSubject());
		emailTemplatesListCompViewBean.setEmailBody(currentText.getEmailText());
		emailTemplatesListCompViewBean.setSelectedEmailText(currentText);
	}

	public void setSelectedEmailTemplate(EmailTemplateObj eto) {
		emailTemplatesListCompViewBean.setSelectedTemplate(eto);
	}

	public void sendEmailToParticipants(ParticipantObj[] participants) {

		List<ParticipantObj> participantObjs = Arrays.asList(participants);
		EmailTemplateObj template = emailTemplatesListCompViewBean.getSelectedTemplate();
		EmailTextObj templateText = new EmailTextObj();

		Company company = sessionBean.getCompany();
		Project project = null;
		String lang = "en";

		for (ParticipantObj participantObj : participantObjs) {

			if (participantObj.getLanguageId() != null) {
				lang = participantObj.getLanguageId();
			} else if (company.getLangPref() != null) {
				lang = company.getLangPref();
			}

			if (participantObj.getProject() != null) {
				project = projectDao.findById(participantObj.getProject().getProjectId());
			} else {
				project = null;
			}

			log.debug("Sending email to {} for project {}", participantObj.getLastName(), project);

			templateText = emailTemplatesService.findEmailTextByLangCode(template, lang);

			if (templateText == null) {
				templateText = emailTemplatesService.findEmailTextByLangCode(template, "en");
			}

			String emailUrl = getTltProperties().getProperty("tltParticipantExperienceUrl",
					"http://participant.experience.url")
					+ "?lang=" + lang;

			if (templateText != null) {
				mailTemplatesService.sendTemplate(templateText.getId(), participantObj, project, company, emailUrl);
			}

		}
	}

	public EmailTemplatesListCompViewBean getEmailTemplatesListCompViewBean() {
		return emailTemplatesListCompViewBean;
	}

	public void setEmailTemplatesListCompViewBean(EmailTemplatesListCompViewBean emailTemplatesListCompViewBean) {
		this.emailTemplatesListCompViewBean = emailTemplatesListCompViewBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public Properties getTltProperties() {
		return tltProperties;
	}

	public void setTltProperties(Properties tltProperties) {
		this.tltProperties = tltProperties;
	}

	public boolean isClientLevel() {
		return clientLevel;
	}

	public void setClientLevel(boolean clientLevel) {
		this.clientLevel = clientLevel;
	}

	public boolean isProjectLevel() {
		return projectLevel;
	}

	public void setProjectLevel(boolean projectLevel) {
		this.projectLevel = projectLevel;
	}
}