package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.fsm.service.FileService;
import com.kf.queuemanager.service.MessageService;
import com.pdinh.data.jaxb.reporting.SystemHealthCheckResponse;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.presentation.domain.SystemStatusObj;
import com.pdinh.presentation.helper.ReportRequestHelper;
import com.pdinh.presentation.helper.URLContextConstants;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.SystemHeartbeatViewBean;
import com.pdinh.rest.security.AuthorizationClientFilter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@ManagedBean
@ViewScoped
public class SystemHeartbeatControllerBean implements Serializable {
	private static final long serialVersionUID = -3557942362496552823L;
	final static Logger logger = LoggerFactory.getLogger(SystemHeartbeatControllerBean.class);

	@EJB
	FileService fileService;

	@EJB
	MessageService messageService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "systemHeartbeatViewBean", value = "#{systemHeartbeatViewBean}")
	private SystemHeartbeatViewBean viewBean;

	@ManagedProperty(name = "reportRequestHelper", value = "#{reportRequestHelper}")
	private ReportRequestHelper reportRequestHelper;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	private int counter;
	private boolean firstTime = true;
	private String importPollerURI;
	private String palmsRestApiURI;
	private String bucketName;
	private String queueName;

	private void initialize() {
		if (isFirstTime()) {

			setImportPollerURI(configProperties.getProperty("importPollerHost"));
			setBucketName(configProperties.getProperty("bulkUploadS3Bucket"));
			setQueueName(configProperties.getProperty("bulkUploadQueue"));
			String palmsRestApiHost = configProperties.getProperty("palmsRestApiHost");
			logger.debug("Palms rest api host: {}", palmsRestApiHost);
			if (palmsRestApiHost == null) {
				logger.error("Missing JNDI configuration property: palmsRestApiHost !!");
			} else {
				if (palmsRestApiHost.startsWith("http")) {
					this.setPalmsRestApiURI(palmsRestApiHost);
				} else {
					palmsRestApiHost = "http://" + palmsRestApiHost;
					this.setPalmsRestApiURI(palmsRestApiHost);
				}
			}

			logger.trace("System Heartbeat Settings:");
			logger.trace("\tImport Poller URI: {}", getImportPollerURI());
			logger.trace("\tPalms Rest API URI: {}", getPalmsRestApiURI());
			logger.trace("\tBucket Name: {}", getBucketName());
			logger.trace("\tQueue Name: {}", getQueueName());

			viewBean.initialize();

			setFirstTime(true);
		}
	}

	public void checkHeartbeat(boolean bulkUpload, boolean reporting) {

		initialize();

		if (bulkUpload) {
			viewBean.getSystemStatus().add(checkFileServiceHealth());
			viewBean.getSystemStatus().add(checkQueuingServiceHealth());

			try {
				viewBean.getSystemStatus().add(checkPollerHealth());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (reporting) {
			viewBean.getSystemStatus().addAll(checkReportCenterHealth());
		}

		viewBean.setSystemsStatusUknown(false);

		logger.trace("Checking health status ... ");
	}

	public String allHealthy(boolean bulkUpload, boolean reporting) {
		String iconName = "icon-bad";

		if (!viewBean.isSystemsStatusUknown()) {
			if (bulkUpload && reporting) {
				iconName = viewBean.isAllHealthy() ? "icon-good" : "icon-bad";
			} else if (bulkUpload && !reporting) {
				iconName = viewBean.isBulkUploadHealthy() ? "icon-good" : "icon-bad";
			} else if (!bulkUpload && reporting) {
				iconName = viewBean.isReportCenterHealthy() ? "icon-good" : "icon-bad";
			}
		} else {
			iconName = "icon-status-unknown";
		}

		return iconName;
	}

	private SystemStatusObj checkFileServiceHealth() {
		SystemStatusObj status = new SystemStatusObj();
		status.setRunning(fileService.isAmazonServiceRunning(getBucketName(), configProperties));
		status.setSystemComponentName("File Storage Service");
		status.setStatusChangeTime(new Date());
		status.setApplication(SystemStatusObj.APPLICATION_BULK_UPLOAD);
		return status;
	}

	private SystemStatusObj checkQueuingServiceHealth() {
		SystemStatusObj status = new SystemStatusObj();
		status.setRunning(messageService.isAmazonServiceRunning(getQueueName(), configProperties));
		status.setSystemComponentName("Queuing Service");
		status.setStatusChangeTime(new Date());
		status.setApplication(SystemStatusObj.APPLICATION_BULK_UPLOAD);
		return status;
	}

	private SystemStatusObj checkPollerHealth() throws JSONException {

		SystemStatusObj status = new SystemStatusObj();

		if (getImportPollerURI() == null) {
			return status;
		}

		String sqsStatus = "";
		long dateTime = 0;
		String resourceName = getImportPollerURI() + "/importpollerrest/jaxrs/heartbeat";
		Client client = Client.create();

		try {

			WebResource resource = client.resource(resourceName);
			ClientResponse response = resource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class);

			JSONObject document = response.getEntity(JSONObject.class);

			logger.trace("Poller: Response Status = {}; URI = {}", response.getStatus(), resourceName);

			if (response.getStatus() == 200) {
				sqsStatus = document.getString("SQS_STATUS");
				dateTime = Long.valueOf(document.getString("LAST_SQS_POLL"));
				logger.trace("Poller: SQS Status = {}; Last Poll = {}", sqsStatus, dateTime);
			}

		} catch (ClientHandlerException e) {
			logger.error("Could not communicate with Poller. {}", e.getMessage());
		} catch (UniformInterfaceException e) {
			logger.error("Could not communicate with Poller. {}", e.getMessage());
		} finally {
			client.destroy();
		}

		status.setRunning((dateTime == 0) ? false : (sqsStatus.equalsIgnoreCase("ACTIVE")) ? true : false);
		status.setSystemComponentName("Import Polling Service");
		status.setStatusChangeTime((dateTime == 0) ? new Date() : new Date(dateTime));
		status.setApplication(SystemStatusObj.APPLICATION_BULK_UPLOAD);

		return status;

	}

	private List<SystemStatusObj> checkReportCenterHealth() {

		List<SystemStatusObj> statuses = new ArrayList<SystemStatusObj>();

		if (getImportPollerURI() == null) {
			return statuses;
		}

		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);

		Client client = Client.create(config);
		String resourceName = getImportPollerURI() + URLContextConstants.IMPORTPOLLER_REST_CONTEXT
				+ "/report/healthcheck";
		SystemHealthCheckResponse document = null;
		ClientResponse response = null;
		try {
			WebResource resource = client.resource(resourceName);
			response = resource.type(MediaType.APPLICATION_XML).get(ClientResponse.class);
			document = response.getEntity(SystemHealthCheckResponse.class);
		} catch (ClientHandlerException e) {
			logger.error("Could not communicate with Poller.  {}", e.getMessage());
			document = this.getSystemStatusFromAdminRestApi();
		} catch (UniformInterfaceException e) {
			logger.error("Could not communicate with Poller.  {}", e.getMessage());
			document = this.getSystemStatusFromAdminRestApi();
		} finally {
			client.destroy();
		}

		// if (response.getStatus() == 200) {
		if (!(document == null)) {
			Map<String, Map<String, String>> pollerDetails = new LinkedHashMap<String, Map<String, String>>();
			pollerDetails.put("details", new LinkedHashMap<String, String>());
			pollerDetails.get("details").put("Polling Interval:", String.valueOf(document.getPollingInterval()));
			pollerDetails.get("details").put("Polling Thread Count:",
					String.valueOf(document.getReportPollingThreads()));
			pollerDetails.get("details").put("Message Batch Size:", String.valueOf(document.getMessageBatchSize()));

			Map<String, Map<String, String>> queueDetails = new LinkedHashMap<String, Map<String, String>>();
			queueDetails.put("details", new LinkedHashMap<String, String>());
			queueDetails.get("details").put("High Priority Queue:",
					String.valueOf(document.getCountHighPriorityQueue()));
			queueDetails.get("details").put("Low Priority Queue:", String.valueOf(document.getCountLowPriorityQueue()));

			Map<String, Map<String, String>> adminDetails = new LinkedHashMap<String, Map<String, String>>(sssss());

			// Oxcart Server
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getOxcart()), "Oxcart Server",
					document.getOxcartServerStatusChangeTime(), SystemStatusObj.APPLICATION_REPORT_CENTER));

			// Report Server
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getReportServer()),
					"Report Server", document.getReportServerStatusChangeTime(),
					SystemStatusObj.APPLICATION_REPORT_CENTER));

			// TicCan Server
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getTincan()), "TinCan Server",
					document.getTincanServerStatusChangeTime(), SystemStatusObj.APPLICATION_REPORT_CENTER));

			// Admin Server
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getAdmin()), "Admin Server",
					document.getAdminServerStatusChangeTime(), SystemStatusObj.APPLICATION_REPORT_CENTER, adminDetails));

			// KFO API
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getKfoReportApi()), "KFO API",
					document.getKfoReportApiStatusChangeTime(), SystemStatusObj.APPLICATION_REPORT_CENTER));

			// Report Queuing Service
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getSqsStatus()),
					"Report Queuing Service", null, SystemStatusObj.APPLICATION_REPORT_CENTER, queueDetails));

			// Report Polling Service
			statuses.add(new SystemStatusObj(SystemStatusObj.convertStatus(document.getPollingStatus()),
					"Report Polling Service", null, SystemStatusObj.APPLICATION_REPORT_CENTER, pollerDetails));

			logger.trace(
					"Report Center Health Status: Report = {}; Oxcart = {}; TinCan = {}; Admin = {}; KFO API = {}; SQS = {}; Polling = {}",
					document.getReportServer(), document.getOxcart(), document.getTincan(), document.getAdmin(),
					document.getKfoReportApi(), document.getSqsStatus(), document.getPollingStatus());

		}

		return statuses;

	}

	private Map<String, Map<String, String>> sssss() {
		Map<String, Map<String, String>> map = new LinkedHashMap<String, Map<String, String>>();
		List<ReportRequest> requests = new ArrayList<ReportRequest>(
				reportRequestHelper.getCurrentlyProcessingRequests());

		for (ReportRequest request : requests) {
			Map<String, String> miniMap = new LinkedHashMap<String, String>();
			miniMap.put("Client:", request.getReportParticipants().get(0).getProject().getCompany().getCoName());
			miniMap.put("Project:", request.getReportParticipants().get(0).getProject().getName());
			miniMap.put("Request Name:", request.getReportGenerateRequest().getRequestName() + " ["
					+ request.getReportGenerateRequest().getReportGenerateRequestId() + "]");
			miniMap.put("Report Type:", request.getReportType().getName());
			miniMap.put("Requester:", request.getReportGenerateRequest().getPrincipal());

			map.put(String.valueOf(request.getReportRequestId()), miniMap);

		}

		return map;
	}

	private SystemHealthCheckResponse getSystemStatusFromAdminRestApi() {
		logger.debug("Getting system status from palms rest api");
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);

		Client client = Client.create(config);
		String resourceName = this.getPalmsRestApiURI() + URLContextConstants.ADMIN_REST_CONTEXT + "/health/ping";
		logger.debug("Requesting system status from URI: {}", resourceName);
		SystemHealthCheckResponse document = null;
		ClientResponse response = null;

		try {
			client.addFilter(new AuthorizationClientFilter(configProperties));
			WebResource resource = client.resource(resourceName);
			response = resource.type(MediaType.APPLICATION_XML).get(ClientResponse.class);
			document = response.getEntity(SystemHealthCheckResponse.class);

			document.setPollingInterval(0);
			document.setCountHighPriorityQueue(0);
			document.setCountLowPriorityQueue(0);
			document.setPollingStatus("unknown");
			document.setSqsStatus("unknown");
			document.setMessageBatchSize(0);

		} catch (ClientHandlerException e) {
			logger.error("Could not communicate with Palms Rest Api.  {}", e.getMessage());

		} catch (UniformInterfaceException e) {
			logger.error("Could not communicate with Palms Rest Api.  {}", e.getMessage());

		} finally {
			client.destroy();
		}

		return document;
	}

	public void showDetails(SystemStatusObj status) {
		status.setDetailsVisible((status.isDetailsVisible()) ? false : true);
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SystemHeartbeatViewBean getSystemHeartbeatViewBean() {
		return viewBean;
	}

	public void setSystemHeartbeatViewBean(SystemHeartbeatViewBean systemHeartbeatViewBean) {
		this.viewBean = systemHeartbeatViewBean;
	}

	public int getCounter() {
		return counter++;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public String getImportPollerURI() {
		return importPollerURI;
	}

	public void setImportPollerURI(String importPollerURI) {
		this.importPollerURI = importPollerURI;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getPalmsRestApiURI() {
		return palmsRestApiURI;
	}

	public void setPalmsRestApiURI(String palmsRestApiURI) {
		this.palmsRestApiURI = palmsRestApiURI;
	}

	public ReportRequestHelper getReportRequestHelper() {
		return reportRequestHelper;
	}

	public void setReportRequestHelper(ReportRequestHelper reportRequestHelper) {
		this.reportRequestHelper = reportRequestHelper;
	}

}
