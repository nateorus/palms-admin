package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ParticipantClipboardViewBean;

@ManagedBean(name = "participantClipboardControllerBean")
@ViewScoped
public class ParticipantClipboardControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ParticipantClipboardControllerBean.class);

	@EJB
	private UserDao userDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private LanguageDao languageDao;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "emailTemplateListControllerBean", value = "#{emailTemplateListControllerBean}")
	private EmailTemplateListControllerBean emailTemplateListControllerBean;

	@ManagedProperty(name = "participantClipboardViewBean", value = "#{participantClipboardViewBean}")
	private ParticipantClipboardViewBean participantClipboardViewBean;

	private Boolean firstTime = true;

	public void initView() {
		if (firstTime) {
			firstTime = false;

			getSessionBean().clearProject();

			participantClipboardViewBean.getParticipants().clear();
			participantClipboardViewBean.setSelectedParticipants(null);
			participantClipboardViewBean.getFilteredParticipants().clear();

			if (participantClipboardViewBean.getViewType() == 0) {
				for (ParticipantObj user : sessionBean.getSelectedUsers()) {
					user.setRowKey(user.getUserId());
					participantClipboardViewBean.getParticipants().add(user);
				}
			} else {
				// this is bad, need to optimize
				List<User> users = sessionBean.getCompany().getUsers();
				for (User user : users) {
					for (ParticipantObj participant : getSessionBean().getSelectedUsers()) {
						if (user.getUsersId() == participant.getUserId() && user.getActive() == 1) {
							List<ProjectUser> projectUsers = user.getProjectUsers();
							for (ProjectUser pu : projectUsers) {
								Project project = pu.getProject();
								ParticipantObj participantWithProject = new ParticipantObj(participant.getUserId(),
										participant.getFirstName(), participant.getLastName(), participant.getEmail(),
										participant.getUserName());
								participantWithProject.setLanguageId(participant.getLanguageId());
								participantWithProject.setPassword(participant.getPassword());
								ProjectObj projectObj = new ProjectObj(project.getProjectId(), project.getName(),
										project.getProjectType().getName(), project.getProjectType().getProjectTypeId());
								participantWithProject.setProject(projectObj);
								participantWithProject.setRowKey(pu.getRowId());
								participantClipboardViewBean.getParticipants().add(participantWithProject);
							}
						}
					}
				}
			}
			// initializing filtering collection
			participantClipboardViewBean.setFilteredParticipants(participantClipboardViewBean.getParticipants());
		}

		enableButtonsBasedOnSelection();

	}

	public void refreshView() {
		firstTime = true;
		initView();
	}

	public void setParticipantClipboardViewBean(ParticipantClipboardViewBean participantClipboardViewBean) {
		this.participantClipboardViewBean = participantClipboardViewBean;
	}

	public ParticipantClipboardViewBean getParticipantClipboardViewBean() {
		return participantClipboardViewBean;
	}

	public void handleRemoveParticipants(ActionEvent event) {
		removeParticipants();
	}

	private void removeParticipants() {
		/**
		 * this appears to be problematic at the moment -- as i seem to have
		 * difficulty removing an item from an array :P
		 */
		ParticipantObj[] selectedParticipants = participantClipboardViewBean.getSelectedParticipants();
		for (ParticipantObj user : selectedParticipants) {
			getSessionBean().removeSelectedUser(user);
		}

		refreshView();
	}

	public void handleClearParticipants(ActionEvent event) {
		clearParticipants();
		enableButtonsBasedOnSelection();
	}

	private void clearParticipants() {
		getSessionBean().clearSelectedUsers();
		participantClipboardViewBean.getParticipants().clear();
		participantClipboardViewBean.setSelectedParticipants(null);
		participantClipboardViewBean.getFilteredParticipants().clear();
	}

	public void handleRowSelect(SelectEvent event) {
		ParticipantObj participant = (ParticipantObj) event.getObject();
		User user = findUserInCompany(getSessionBean().getCompany(), participant.getUserId());
		// Project project = (participant.getProject() != null) ?
		// projectDao.findById(participant.getProject()
		// .getProjectId()) : null;
		// getSessionBean().setProject(project);
		getSessionBean().setUser(user);
		enableButtonsBasedOnSelection();
	}

	public void handleRowUnselect(UnselectEvent event) {
		ParticipantObj[] participants = participantClipboardViewBean.getSelectedParticipants();
		if (participants == null || participants.length == 0) {
			getSessionBean().clearProject();
		} else if (participants.length == 1) {
			User user = userDao.findById(participants[0].getUserId());
			// Project project = (participants[0].getProject() != null) ?
			// projectDao.findById(participants[0].getProject()
			// .getProjectId()) : null;
			// getSessionBean().setProject(project);
			getSessionBean().setUser(user);
		}
		enableButtonsBasedOnSelection();
	}

	private User findUserInCompany(Company company, Integer userId) {
		Iterator<User> iterator = company.getUsers().iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			int id = user.getUsersId();
			if (id == userId) {
				return user;
			}
		}
		return null;
	}

	// Change columns dynamically
	public void handleChangeViewType(AjaxBehaviorEvent event) {

		// refresh view so that participantsobj list gets updated
		refreshView();

		if (participantClipboardViewBean.getViewType() == 0) {
			participantClipboardViewBean.setRenderProjects(false);
		} else if (participantClipboardViewBean.getViewType() == 1) {
			participantClipboardViewBean.setRenderProjects(true);
		}
	}

	public void handleSelectAll(ActionEvent event) {
		ParticipantObj[] parts = new ParticipantObj[participantClipboardViewBean.getParticipants().size()];

		Iterator<ParticipantObj> iterator = participantClipboardViewBean.getParticipants().iterator();
		int i = 0;
		while (iterator.hasNext()) {
			ParticipantObj part = iterator.next();
			parts[i] = part;
			i++;
		}
		participantClipboardViewBean.setSelectedParticipants(parts);
		enableButtonsBasedOnSelection();
	}

	public void handleSelectNone(ActionEvent event) {
		participantClipboardViewBean.setSelectedParticipants(null);
		enableButtonsBasedOnSelection();
	}

	private void enableButtonsBasedOnSelection() {
		Subject subject = SecurityUtils.getSubject();
		if (participantClipboardViewBean.getSelectedParticipants() != null
				&& participantClipboardViewBean.getSelectedParticipants().length >= 1) {
			if (participantClipboardViewBean.getRenderProjects()) {
				if (subject.isPermitted("createReportRequest")) {
					log.debug("Subject has createReportRequests permission");
					participantClipboardViewBean.setCreateReportsDisabled(false);
					participantClipboardViewBean.setReportUploadDisabled(false);
				} else {
					log.debug("Subject does not have createReportRequests permission");
				}
				if (subject.isPermitted("downloadReports")) {
					log.debug("Subject is Permitted to download Reports");
					participantClipboardViewBean.setDownloadReportDisabled(false);
				} else {
					log.debug("Subject is not permitted to download Reports");
				}

			}

			participantClipboardViewBean.setRemoveParticipantsDisabled(false);
			participantClipboardViewBean.setClearParticipantsDisabled(false);
			if (subject.isPermitted("sendClientEmails")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":sendClientEmails:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantClipboardViewBean.setSendEmailsDisabled(false);
			}

		} else {
			participantClipboardViewBean.setRemoveParticipantsDisabled(true);
			participantClipboardViewBean.setClearParticipantsDisabled(true);
			participantClipboardViewBean.setCreateReportsDisabled(true);
			participantClipboardViewBean.setReportUploadDisabled(true);
			participantClipboardViewBean.setSendEmailsDisabled(true);
		}
	}

	public void handleSendEmailsInit() {
		emailTemplateListControllerBean.refreshView();
	}

	public void handleSendEmails(ActionEvent event) {
		emailTemplateListControllerBean.sendEmailToParticipants(participantClipboardViewBean.getSelectedParticipants());
		refreshView();
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setClientDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public LanguageDao getLanguageDao() {
		return languageDao;
	}

	public void setLanguageDao(LanguageDao languageDao) {
		this.languageDao = languageDao;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public EmailTemplateListControllerBean getEmailTemplateListControllerBean() {
		return emailTemplateListControllerBean;
	}

	public void setEmailTemplateListControllerBean(EmailTemplateListControllerBean emailTemplateListControllerBean) {
		this.emailTemplateListControllerBean = emailTemplateListControllerBean;
	}

}
