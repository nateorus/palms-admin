package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.admin.SelfRegConfigRepresentation;
import com.pdinh.data.jaxb.admin.SelfRegListItemRepresentation;
import com.pdinh.presentation.helper.LabelUtils;
import com.pdinh.presentation.helper.SelfRegistrationHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.SelfRegistrationConfigurationViewBean;

@ManagedBean(name = "selfRegConfigControllerBean")
@ViewScoped
public class SelfRegistrationConfigurationControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SelfRegistrationConfigurationControllerBean.class);
	private static final String DOMAIN_NAME_PATTERN = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$";

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "selfRegistrationHelper", value = "#{selfRegistrationHelper}")
	private SelfRegistrationHelper selfRegistrationHelper;

	@ManagedProperty(name = "selfRegConfigViewBean", value = "#{selfRegConfigViewBean}")
	private SelfRegistrationConfigurationViewBean viewBean;

	public void initializeSelfRegistration() {
		SelfRegConfigRepresentation config = selfRegistrationHelper.getCreateSelfRegConfig(sessionBean.getProject()
				.getProjectId());
		this.setFieldValues(config);
	}

	public void addDomain() {
		String domain = viewBean.getDomain();
		if (domain != null && this.isValidDomain(domain)) {
			if (!this.itemExists(viewBean.getDomains(), domain)) {
				viewBean.getAddedDomains().add(domain);
				viewBean.getDomains().add(domain);
				viewBean.setDomain(null);
			} else {
				LabelUtils.addWarningMessage("validation", "Allowed Email Domains: ", "Domain with this name ("
						+ domain + ") already exists in the list.");
			}
		} else {
			LabelUtils.addErrorMessage("validation", "Allowed Email Domains: ", "Invalid domain name.");
		}

	}

	public void removeDomain(int index) {

		String domain = viewBean.getDomains().get(index);

		if (domain != null) {
			if (!this.itemExists(viewBean.getRemovedDomains(), domain)) {
				viewBean.getRemovedDomains().add(domain);
			}

			if (this.itemExists(viewBean.getAddedDomains(), domain)) {
				int index2 = this.indexOf(viewBean.getAddedDomains(), domain);
				viewBean.getAddedDomains().remove(index2);
			}

			viewBean.getDomains().remove(index);
		}
	}

	public void saveSettings() {

		if (!this.isValid()) {
			return;
		}

		List<Integer> removedDomainsIds = this.getRemovedDomainsIds();
		SelfRegConfigRepresentation configuration = null;
		int limit = viewBean.getLimit();

		if (viewBean.isUnlimited()) {
			limit = -1;
		}

		configuration = selfRegistrationHelper.enableSelfRegistration(viewBean.getId(), viewBean.isEnabled(), limit);

		if (removedDomainsIds != null && !removedDomainsIds.isEmpty()) {
			configuration = selfRegistrationHelper.removeAllowedDomains(viewBean.getId(), removedDomainsIds);
		}

		if (viewBean.getAddedDomains() != null && !viewBean.getAddedDomains().isEmpty()) {
			configuration = selfRegistrationHelper.addAllowedDomains(viewBean.getId(), viewBean.getAddedDomains());
		}

		if (configuration != null) {
			// LabelUtils.addMessage("global", "Save: ",
			// "All the settings were saved successfully.");
			this.setFieldValues(configuration);
		}
	}

	private boolean isValid() {
		boolean valid = true;

		if (viewBean.getDomains() != null && viewBean.getDomains().isEmpty()) {
			LabelUtils.addErrorMessage("validation", "Allowed Email Domains:", "Please add at least one domain name.");
			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			valid = false;
		}

		return valid;
	}

	public void resetUrl() {
		SelfRegConfigRepresentation configuration = null;
		configuration = selfRegistrationHelper.resetConfigUrl(viewBean.getId());
		if (configuration != null) {
			this.setFieldValues(configuration);
		}
	}

	public void openRegistrationUrl() {
		String url = viewBean.getConfiguration().getSelfRegistrationUrl();
		if (url != null) {
			RequestContext.getCurrentInstance().execute("window.open('" + url + "')");
		}
	}

	private void setFieldValues(SelfRegConfigRepresentation config) {
		if (config != null) {
			viewBean.setConfiguration(config);
			viewBean.setDomains(this.getDomains(config.getAllowedEmailDomains()));
			viewBean.setId(config.getSelfRegConfigId());
			viewBean.setEnabled(config.isEnabled());
			if (config.getRegistrationLimit() > -1) {
				viewBean.setUnlimited(false);
				viewBean.setLimit(config.getRegistrationLimit());
			} else {
				viewBean.setUnlimited(true);
				viewBean.setLimit(0);
			}
			viewBean.setRemovedDomains(new ArrayList<String>());
			viewBean.setAddedDomains(new ArrayList<String>());
		} else {
			logger.error("setFieldValues: Configuration representation object is null.");
		}
	}

	private List<Integer> getRemovedDomainsIds() {
		List<Integer> ids = new ArrayList<Integer>();
		for (SelfRegListItemRepresentation item : viewBean.getConfiguration().getAllowedEmailDomains()) {
			for (String domain : viewBean.getRemovedDomains()) {
				if (StringUtils.equalsIgnoreCase(item.getValue(), domain)) {
					ids.add(item.getId());
				}
			}
		}
		return ids;
	}

	private List<String> getDomains(List<SelfRegListItemRepresentation> list) {
		List<String> domains = new ArrayList<String>();
		for (SelfRegListItemRepresentation item : list) {
			domains.add(item.getValue());
		}
		return domains;
	}

	private boolean itemExists(List<String> list, String target) {
		for (String item : list) {
			if (StringUtils.equalsIgnoreCase(item, target)) {
				return true;
			}
		}
		return false;
	}

	private int indexOf(List<String> list, String target) {
		for (int i = 0; i < list.size(); i++) {
			if (StringUtils.equalsIgnoreCase(list.get(i), target)) {
				return i;
			}
		}
		return -1;
	}

	private boolean isValidDomain(String domain) {
		Pattern p = Pattern.compile(DOMAIN_NAME_PATTERN);
		return p.matcher(domain).find();
	}

	public SelfRegistrationHelper getSelfRegistrationHelper() {
		return selfRegistrationHelper;
	}

	public void setSelfRegistrationHelper(SelfRegistrationHelper selfRegistrationHelper) {
		this.selfRegistrationHelper = selfRegistrationHelper;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SelfRegistrationConfigurationViewBean getSelfRegConfigViewBean() {
		return viewBean;
	}

	public void setSelfRegConfigViewBean(SelfRegistrationConfigurationViewBean selfRegConfigViewBean) {
		this.viewBean = selfRegConfigViewBean;
	}

}
