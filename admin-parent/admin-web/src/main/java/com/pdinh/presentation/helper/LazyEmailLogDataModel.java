package com.pdinh.presentation.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.presentation.domain.EmailLogObj;

public class LazyEmailLogDataModel extends LazyDataModel<EmailLogObj> {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(LazyEmailLogDataModel.class);
	
	private List<EmailLogObj> data;
	
	public LazyEmailLogDataModel(List<EmailLogObj> records){
		this.data = records;
	}
	
	@Override  
    public EmailLogObj getRowData(String rowKey) {  
        for(EmailLogObj obj : data) {  
            if(obj.getId().equals(rowKey)){  
                return obj;  
            }
        }  
  
        return null;  
    }
	
	@Override  
    public Object getRowKey(EmailLogObj obj) {  
        return obj.getId();  
    }  
	
	@Override  
    public List<EmailLogObj> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,String> filters) {  
        List<EmailLogObj> records = new ArrayList<EmailLogObj>();  
  
        //filter  
        for(EmailLogObj record : data) {  
            boolean match = true;  
  
            for(Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {  
                try {  
                    String filterProperty = it.next();  
                    String filterValue = filters.get(filterProperty);  
                    String fieldValue = String.valueOf(record.getClass().getField(filterProperty).get(record));  
  
                    if(filterValue == null || fieldValue.startsWith(filterValue)) {  
                        match = true;  
                    }  
                    else {  
                        match = false;  
                        break;  
                    }  
                } catch(Exception e) {  
                    match = false;  
                }   
            }  
  
            if(match) {  
            	records.add(record);  
            }  
        }  
  
        //sort  
        if(sortField != null) {  
            Collections.sort(records, new LazySorter(sortField, sortOrder));  
        }  
  
        //rowCount  
        int dataSize = records.size();  
        this.setRowCount(dataSize);  
  
        //paginate  
        if(dataSize > pageSize) {  
            try {  
                return records.subList(first, first + pageSize);  
            }  
            catch(IndexOutOfBoundsException e) {  
                return records.subList(first, first + (dataSize % pageSize));  
            }  
        }  
        else {  
            return records;  
        }  
    }  

	public List<EmailLogObj> getData() {
		return data;
	}

	public void setData(List<EmailLogObj> data) {
		this.data = data;
	}

}
