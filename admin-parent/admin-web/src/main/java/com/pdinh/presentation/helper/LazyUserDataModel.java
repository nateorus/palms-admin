package com.pdinh.presentation.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;

public class LazyUserDataModel extends LazyDataModel<ParticipantObj> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LazyUserDataModel.class);
	
	private List<ParticipantObj> pptObjs;
	
	@Override  
    public ParticipantObj getRowData(String rowKey) {  
        for(ParticipantObj obj : pptObjs) {  
        	String uniqueId = obj.getUserId().toString();
            if(uniqueId.equals(rowKey)){  
                return obj;  
            }
        }  
  
        return null;  
    }
	
	@Override  
    public Object getRowKey(ParticipantObj obj) {  
        return obj.getUserId().toString();  
    }  
	
	@Override  
    public List<ParticipantObj> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,String> filters) {  
        List<ParticipantObj> ppts = new ArrayList<ParticipantObj>();  
        //int pageNumber = (first + 1) / pageSize;
        log.debug("First: {}", first);
        log.debug("Page Size: {}", pageSize);
        //log.debug("Page Number: {}", pageNumber);
        List<ParticipantObj> users = new ArrayList<ParticipantObj>();
        List<ParticipantObj> dataObjs = new ArrayList<ParticipantObj>();
        setPptObjs(dataObjs);
        //filter  
        log.debug("Set Data: {}", dataObjs.size());
        for(ParticipantObj user : this.pptObjs) {  
            boolean match = true;  
  
            for(Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {  
                try {  
                    String filterProperty = it.next();  
                    String filterValue = filters.get(filterProperty);  
                    String fieldValue = String.valueOf(user.getClass().getField(filterProperty).get(user));  
  
                    if(filterValue == null || fieldValue.startsWith(filterValue)) {  
                        match = true;  
                    }  
                    else {  
                        match = false;  
                        break;  
                    }  
                } catch(Exception e) {  
                    match = false;  
                }   
            }  
  
            if(match) {  
                users.add(user);  
            }  
        }  
  
        //sort  
        if(sortField != null) {  
            Collections.sort(users, new LazySorter(sortField, sortOrder));  
        }  
  
        //rowCount  
       // int dataSize = subjects.size();  
        int dataSize = 0;
       
        this.setRowCount(dataSize);  
        int currentDataSize = users.size();
        //paginate  
        if(currentDataSize > pageSize) {  
            try {  
                return users.subList(first, first + pageSize);  
            }  
            catch(IndexOutOfBoundsException e) {  
                return users.subList(first, first + (dataSize % pageSize));  
            }  
        }  
        else {  
            return users;  
        }  
    }  

	public List<ParticipantObj> getPptObjs() {
		return pptObjs;
	}

	public void setPptObjs(List<ParticipantObj> pptObjs) {
		this.pptObjs = pptObjs;
	}

}
