package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.auth.persistence.entity.RoleTemplate;
import com.pdinh.data.CreateUserServiceLocal;
import com.pdinh.data.UsernameServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.converter.RoleConverter;
import com.pdinh.presentation.view.EditRolesViewBean;
import com.pdinh.presentation.view.SysAdminViewBean;

@ManagedBean(name = "editRolesControllerBean")
@ViewScoped
public class EditRolesControllerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EditRolesControllerBean.class);
	
	@Resource(mappedName="application/config_properties")
    private Properties properties;
	
	@ManagedProperty(name = "editRolesViewBean", value = "#{editRolesViewBean}")
	private EditRolesViewBean editRolesViewBean;
	
	@ManagedProperty(name = "sysAdminViewBean", value = "#{sysAdminViewBean}")
	private SysAdminViewBean sysAdminViewBean;
	
	@ManagedProperty(name = "roleConverter", value="#{roleConverter}")
	private RoleConverter roleConverter;
	
	@EJB
	private PalmsSubjectDao subjectDao;
	
	@EJB
	private ExternalSubjectServiceLocal subjectService;
	
	@EJB
	private PalmsRealmDao realmDao;
	
	@EJB
	private PermissionServiceLocal permissionService;

	@EJB
	private UsernameServiceLocal usernameService;
	
	@EJB
	private UserDao userDao;
	
	@EJB
	private CompanyDao companyDao;
	
	@EJB
	private CreateUserServiceLocal createUserService;
	
	public void initRoles(List<ExternalSubjectDataObj> users){
		Subject subject = SecurityUtils.getSubject();
		//log.debug("Got Palms Realm ID: {}", realm_id);
		//editRolesViewBean.setRealm_id(realm_id);
    	RoleContext defaultContext = new RoleContext();
        List<Role> targetRoles = new ArrayList<Role>();
        RoleTemplate template = new RoleTemplate();
    	if (users.size() == 1){
    		ExternalSubjectDataObj user = users.get(0);
    		
	    	PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(user.getUsername(), user.getRealm_id());
	    	log.debug("Got Subject {}, Realm Id: {} - {}", subj, user.getRealm_id(), user.getRealmName());
	    	if (subj == null){
	    		subj = new PalmsSubject();
	    		
	    		
	    		subj = subjectService.createSubjectDefaultContext(user.getUsername(), user.getRealm_id(), user.getEntryDN(), user.getExternal_id());
	    		
	    		defaultContext = subj.getContextList().get(0);
	    		
	    		
	    		editRolesViewBean.setSubject(subj);
	    	}else{
	    		log.debug("About to get RoleContexts for subject");
	    		List<RoleContext> contexts = subj.getContextList();
	    		log.debug("Got {} Role Conexts", contexts.size());
	    		//in case subject was created without a role context
	    		if (contexts == null || contexts.isEmpty()){
	    			defaultContext = permissionService.createDefaultRoleContext(subj);
	    			contexts.add(defaultContext);
	    		}else{
	    			for (RoleContext context:contexts){
	    				if (context.isIsdefault()){
	    					defaultContext = context;
	    					break;
	    				}
	    			}
	    		}
	    		
	    		editRolesViewBean.setSubject(subj);
	    	}
    	
	    	editRolesViewBean.setSelectedContext(defaultContext);
	    	targetRoles = defaultContext.getRoles();
	    	template = subj.getRealm().getRoleTemplate();
    	}else{
    		List<String> realmNames = Arrays.asList(properties.getProperty("palmsRealmNames").split(","));
    		List<PalmsRealm> realms = realmDao.findRealmsInListOfNames(realmNames);
    		PalmsRealm realm = realms.get(0);
    		template = realm.getRoleTemplate();
    	}
	    
		List <Role> sourceRoles = new ArrayList<Role>();
		//Only add PalmsSysAdmin or PalmsSuperAdmin Roles if they are assigned.
		for (Role r: template.getAllowedRoles()){
			if (r.getName().equalsIgnoreCase("PalmsSysAdmin")){
				if (subject.isPermitted("assignPalmsSysAdminRole")){
					sourceRoles.add(r);
				}
			}else if (r.getName().equalsIgnoreCase("PalmsSuperAdmin")){
				if (subject.isPermitted("assignPalmsSuperAdminRole")){
					sourceRoles.add(r);
				}
			}else {
				sourceRoles.add(r);
			}
		}
		for (Role r: targetRoles){
			sourceRoles = removeRole(sourceRoles, r.getRole_id());
		}
		
		editRolesViewBean.setContextRoleLists(new DualListModel<Role>(sourceRoles, targetRoles));
    	editRolesViewBean.setAddedRoles(new ArrayList<Role>());
    	editRolesViewBean.setRemovedRoles(new ArrayList<Role>());
		log.debug("finished with handleEditRoles()");
	}
	public List<Role> removeRole(List<Role> list, Integer roleId){
		for (Role r:list){
			if (r.getRole_id() == roleId){
				list.remove(r);
				return list;
			}
		}
		return list;
	}
	 public void handleRoleTransfer(TransferEvent event){
	    	//RoleContext context = editRolesViewBean.getSelectedContext();
		    properties.getProperty("primaryRealmName");
	    	PalmsRealm realm = realmDao.findRealmByName(properties.getProperty("primaryRealmName"));
	    	Subject subject = SecurityUtils.getSubject();
	    	boolean requireLinked = false;
	    	if (event.isAdd()){
	    		List<Role> roles = (List<Role>)event.getItems();
	    		for (Role r : roles){
	    			//context = permissionService.grantRole(context, r, subject.getPrincipal().toString());
	    			editRolesViewBean.getAddedRoles().add(r);
	    			if (r.isRequires_linked_id()){
	    				requireLinked = true;
	    			}
	    		}
	    		//manageSubjectsViewBean.setSelectedContext(context);
	    	} else if(event.isRemove()){
	    		List<Role> roles = (List<Role>)event.getItems();
	    		for (Role r : roles){
	    			editRolesViewBean.getRemovedRoles().add(r);
	    			editRolesViewBean.getAddedRoles().remove(r);
	    			//context = permissionService.removeRole(context, r, subject.getPrincipal().toString());
	    		}
	    	}
	    	if (requireLinked){
	    		FacesContext facesContext = FacesContext.getCurrentInstance();
		    	facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "A role you have selected requires a linked account.  A linked account will be automatically created for selected users if one does not already exist, in company " + realm.getLinked_company_id(), "Click Cancel to abort this action."));

	    	}
	    }
	 
	 public void handleSaveRoles(){
		 //RoleContext context = editRolesViewBean.getSelectedContext();
	     Subject subject = SecurityUtils.getSubject();
	     List<Role> addedRoles = editRolesViewBean.getAddedRoles();
	     List<Role> removedRoles = editRolesViewBean.getRemovedRoles();
	     List<ExternalSubjectDataObj> selectedUsers = sysAdminViewBean.getSelectedSubjects();
	     for (ExternalSubjectDataObj o:selectedUsers){
	    	 PalmsSubject user = subjectDao.findSubjectByPrincipalAndRealm(o.getUsername(), o.getRealm_id());
	    	 if (user == null){
	    		 
 	    		user = new PalmsSubject();
 	    		user = subjectService.createSubjectDefaultContext(o.getUsername(), o.getRealm_id(), o.getEntryDN(), o.getExternal_id());

	    	 }
	    	 RoleContext context = new RoleContext();
	    	 for (RoleContext c:user.getContextList()){
 				if (c.isIsdefault()){
 					context = c;
 					break;
 				}
 			}
		     for (Role r: addedRoles){
		    	 if (r.isRequires_linked_id() && user.getLinked_id() < 1){
		    		User palms_user = new User();
		     		palms_user.setFirstname(o.getFirstname());
		     		palms_user.setLastname(o.getLastname());
		     		palms_user.setEmail(o.getEmail());
		     		palms_user.setUsername(usernameService.generateUsername(o.getEmail(), o.getFirstname(), o.getLastname()));
		     		palms_user.setExternallyManaged(true);
		     		palms_user.setMserverid((short) 1);
		     		palms_user.setType(1);
		     		
		     		if (userDao.findByEmailAndCompany(o.getEmail(), user.getRealm().getLinked_company_id()).isEmpty()){
		     			Company company = companyDao.findById(user.getRealm().getLinked_company_id());
		     			palms_user = createUserService.createNewUser(palms_user, company, null, null, null);
			    		log.debug("Created New User with id: {}", palms_user.getUsersId());
			    		//PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(linkedAccountViewBean.getSelectedUser().getUsername(), realm.getRealm_id());
			    		user.setLinked_id(palms_user.getUsersId());
			    		subjectDao.update(user);
			    		log.debug("Updated Link to subject id {}", user.getSubject_id());
			    		FacesContext facesContext = FacesContext.getCurrentInstance();
				    	facesContext.addMessage(null, new FacesMessage("Created externally managed User " + palms_user.getUsersId() + " in company " + company.getCompanyId(), "Updates to this record are limited"));

		    		}else{
		    			FacesContext facesContext = FacesContext.getCurrentInstance();
				    	facesContext.addMessage(null, new FacesMessage("A user with email " + o.getEmail() + " already exists in company " + user.getRealm().getLinked_company_id() + ". Link to this user or change the user email"));
		    		}
		    	 }
		    	 context = permissionService.grantRole(context, r, subject.getPrincipal().toString());
		     }
		     for (Role r: removedRoles){
		    	 context = permissionService.removeRole(context, r, subject.getPrincipal().toString());
		     }
	     }
	     //editRolesViewBean.setSelectedContext(context);
	 }

	public RoleConverter getRoleConverter() {
		return roleConverter;
	}

	public void setRoleConverter(RoleConverter roleConverter) {
		this.roleConverter = roleConverter;
	}
	public EditRolesViewBean getEditRolesViewBean() {
		return editRolesViewBean;
	}
	public void setEditRolesViewBean(EditRolesViewBean editRolesViewBean) {
		this.editRolesViewBean = editRolesViewBean;
	}
	public SysAdminViewBean getSysAdminViewBean() {
		return sysAdminViewBean;
	}
	public void setSysAdminViewBean(SysAdminViewBean sysAdminViewBean) {
		this.sysAdminViewBean = sysAdminViewBean;
	}

}
