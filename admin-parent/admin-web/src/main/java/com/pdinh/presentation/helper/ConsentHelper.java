package com.pdinh.presentation.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.data.ms.dao.ConsentDao;
import com.pdinh.data.ms.dao.ConsentLogDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.persistence.ms.entity.Consent;
import com.pdinh.persistence.ms.entity.ConsentLog;
import com.pdinh.persistence.ms.entity.ConsentText;
import com.pdinh.presentation.domain.ConsentHistoryObj;
import com.pdinh.presentation.domain.ConsentObj;
import com.pdinh.presentation.domain.ConsentTextObj;
import com.pdinh.presentation.domain.LanguageObj;

@ManagedBean
@ViewScoped
public class ConsentHelper implements Serializable {

	private static final long serialVersionUID = -7513314945306201407L;

	@EJB
	private ConsentDao consentDao;

	@EJB
	private ConsentLogDao consentLogDao;

	@EJB
	private LanguageSingleton languageSingleton;

	public List<ConsentHistoryObj> getConsentLogsForUser(int userId) {

		List<ConsentLog> logs = consentLogDao.findAllForParticipant(userId);
		List<ConsentHistoryObj> history = new ArrayList<ConsentHistoryObj>();

		if (!logs.isEmpty()) {
			for (ConsentLog log : logs) {
				ConsentHistoryObj record = new ConsentHistoryObj(log.getConsentLogId(), log.getUser().getUsersId(), log
						.getConsent().getConsentId(), log.getConsentText().getConsentTextId(), log.getConsentVersion(),
						log.getAccepted(), log.getLanguage(), languageSingleton.getLanguageByCode(
								log.getConsentText().getLang()).getName(), log.getDate());
				history.add(record);
			}
		}

		return history;
	}

	public ConsentObj getCurrentVersionForClient(int clientId) {
		Consent consent = consentDao.findDefaultConsent(clientId);
		ConsentObj consentObj = null;

		if (consent != null) {
			consentObj = new ConsentObj();
			consentObj.setId(consent.getConsentId());
			consentObj.setMajor(consent.isMajorVersion());
			consentObj.setCustom(consent.getIsCustom());
			consentObj.setCreated(consent.getDateActive());
			consentObj.setVersion(consent.getConsentVersion());
			consentObj.setTexts(this.transformConsentText(consent.getConsentTexts()));
			consentObj.setAvailableInLanguages(this.getConsentLanguagesForClient(consent));
		}

		return consentObj;
	}

	public List<LanguageObj> getConsentLanguagesForClient(Consent consent) {
		List<String> codes = new ArrayList<String>();
		List<LanguageObj> languages = new ArrayList<LanguageObj>();

		if (consent != null && consent.getConsentTexts() != null) {
			for (ConsentText text : consent.getConsentTexts()) {
				if (!codes.contains(text.getLang())) {
					codes.add(text.getLang());
				}
			}

			for (String code : codes) {
				languages.add(languageSingleton.getLangByCode(code));
			}

		}
		return languages;
	}

	public ConsentObj updateCurrentTextForLanguage(ConsentObj consent, String languageCode) {
		if (consent != null && languageCode != null) {
			consent.setCurrentTexts(new ArrayList<ConsentTextObj>(this.getTextByLanguageCode(consent.getTexts(),
					languageCode)));
		}
		return consent;
	}

	private List<ConsentTextObj> getTextByLanguageCode(List<ConsentTextObj> texts, String languageCode) {
		List<ConsentTextObj> textObjs = null;
		if (texts != null) {
			textObjs = new ArrayList<ConsentTextObj>();
			for (ConsentTextObj text : texts) {
				if (text.getLanguageCode().equalsIgnoreCase(languageCode)) {
					textObjs.add(new ConsentTextObj(text.getId(), this.textCleanUp(text.getText()), text
							.getLanguageName(), text.getLanguageCode(), text.getConstant()));
				}
			}
		}
		return textObjs;
	}

	private List<ConsentTextObj> transformConsentText(List<ConsentText> texts) {
		List<ConsentTextObj> textObjs = null;
		if (texts != null) {
			textObjs = new ArrayList<ConsentTextObj>();
			for (ConsentText text : texts) {
				textObjs.add(new ConsentTextObj(text.getConsentTextId(), this.textCleanUp(text.getConsentText()),
						languageSingleton.getLanguageByCode(text.getLang()).getName(), languageSingleton
								.getLanguageByCode(text.getLang()).getCode(), text.getConstant()));
			}
		}
		return textObjs;
	}

	private String textCleanUp(String originalText) {
		if (originalText != null) {
			originalText = originalText.replace("[pdinhnpartner]", "Korn Ferry");
			originalText = originalText.replace("[pdinhn]", "Korn Ferry");
		}
		return originalText;
	}

}
