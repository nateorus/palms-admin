package com.pdinh.presentation.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.bulkupload.GenerateTemplateService;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.GenerateBulkUploadTemplateViewBean;
import com.pdinh.presentation.view.ProjectListViewBean;

@ManagedBean
@ViewScoped
public class GenerateBulkUploadTemplateControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(GenerateBulkUploadTemplateControllerBean.class);

	@EJB
	GenerateTemplateService templateService;

	@EJB
	private ProjectDao projectDao;

	@ManagedProperty(name = "generateBulkUploadTemplateViewBean", value = "#{generateBulkUploadTemplateViewBean}")
	private GenerateBulkUploadTemplateViewBean viewBean;

	@ManagedProperty(name = "projectListViewBean", value = "#{projectListViewBean}")
	private ProjectListViewBean projectListViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	public GenerateBulkUploadTemplateControllerBean() {
		if (viewBean != null) {
			viewBean.init();
			viewBean.setMaxRecordsPerSheet(getMaxRecordsPerSheet());
		}
	}

	public StreamedContent download() throws Exception {

		int projectId = sessionBean.getSelectedProject();
		StreamedContent download = new DefaultStreamedContent();
		Subject subject = SecurityUtils.getSubject();
		templateService.setDefinitionsVisible(viewBean.isDefinitionsVisible());
		templateService.setDefaultNumberOfRows(viewBean.getNumberOfRows() + 1);
		templateService.setDefaultColumnWidth(viewBean.getColumnWidth());

		if (viewBean.isNewExcelFormat()) {
			// Microsoft Excel Worksheet
			templateService.setExcelFileFormat("xlsx");
		} else {
			// Microsoft Excel 97-2003 Worksheet
			templateService.setExcelFileFormat("xls");
		}

		File tempFile = File.createTempFile(generateFileName(), "." + templateService.getExcelFileFormat());
		String mimeType = (templateService.getExcelFileFormat().equals("xlsx") ? "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
				: "application/vnd.ms-excel");

		templateService.generateTemplate(tempFile, projectId);

		InputStream input = new FileInputStream(tempFile);
		download = new DefaultStreamedContent(input, mimeType, tempFile.getName());

		log.trace("download = {}", download.getName());

		tempFile.deleteOnExit();

		EntityAuditLogger.auditLog.info("{} performed {} operation on project ({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_IMPORT_GENERATE_IMPORT_FILE, projectId });

		return download;
	}

	private String generateFileName() {
		String fileName;
		String projectName = "";
		String clientName = "";
		Project project = projectDao.findBatchById(sessionBean.getSelectedProject());

		if (project != null) {
			projectName = cleanFileName(project.getName());
			clientName = cleanFileName(project.getCompany().getCoName());
		}

		fileName = clientName + "-" + projectName + "-";

		return fileName;
	}

	private String cleanFileName(String name) {
		return StringUtils.replaceChars(name.toLowerCase().replaceAll("[^a-zA-Z0-9]", " ").trim(), " ", "_");
	}

	public int getMaxRecordsPerSheet() {
		return Integer.valueOf(configProperties.getProperty("maxRecordsPerImportSheet", "1000"));
	}

	public GenerateBulkUploadTemplateViewBean getGenerateBulkUploadTemplateViewBean() {
		return viewBean;
	}

	public void setGenerateBulkUploadTemplateViewBean(
			GenerateBulkUploadTemplateViewBean generateBulkUploadTemplateViewBean) {
		this.viewBean = generateBulkUploadTemplateViewBean;
	}

	public ProjectListViewBean getProjectListViewBean() {
		return projectListViewBean;
	}

	public void setProjectListViewBean(ProjectListViewBean projectListViewBean) {
		this.projectListViewBean = projectListViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

}