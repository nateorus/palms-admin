package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.TokenServiceLocal;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.ContentToken;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.CoachingPlanInfoViewBean;
import com.pdinh.presentation.view.ParticipantInfoViewBean;
import com.pdinh.presentation.view.ParticipantListViewBean;

@ManagedBean(name = "participantListControllerBean")
@ViewScoped
public class ParticipantListControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ParticipantListControllerBean.class);
	User currentUser;
	Company currentCompany;
	private boolean firstTime = true;
	private int viewType = 1; // 0=participants; 1=participants&projects
	private boolean renderProjects = true;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "participantInfoViewBean", value = "#{participantInfoViewBean}")
	private ParticipantInfoViewBean participantInfoViewBean;

	@ManagedProperty(name = "participantListViewBean", value = "#{participantListViewBean}")
	private ParticipantListViewBean participantListViewBean;

	@ManagedProperty(name = "participantInfoControllerBean", value = "#{participantInfoControllerBean}")
	private ParticipantInfoControllerBean participantInfoControllerBean;

	@ManagedProperty(name = "participantClipboardControllerBean", value = "#{participantClipboardControllerBean}")
	private ParticipantClipboardControllerBean participantClipboardControllerBean;

	@ManagedProperty(name = "coachingPlanInfoViewBean", value = "#{coachingPlanInfoViewBean}")
	private CoachingPlanInfoViewBean coachingPlanInfoViewBean;

	@ManagedProperty(name = "auditLogControllerBean", value = "#{auditLogControllerBean}")
	private AuditLogControllerBean auditLogControllerBean;

	@ManagedProperty(name = "emailTemplateListControllerBean", value = "#{emailTemplateListControllerBean}")
	private EmailTemplateListControllerBean emailTemplateListControllerBean;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	@EJB
	private UserDao userDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private TokenServiceLocal tokenService;

	@EJB
	private GlobalRolesBean rolesBean;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	public void initView() {
		if (firstTime) {
			firstTime = false;
			getSessionBean().clearUser();
			getSessionBean().clearProject();
			participantListViewBean.setEditParticipantDisabled(true);
			log.debug("In ParticipantListControllerBean initView");
			currentCompany = getSessionBean().getCompany();
			participantListViewBean.setParticipants(null);
			participantListViewBean.setParticipants(new ArrayList<ParticipantObj>());
			participantListViewBean.clearSelectedParticipants();
			List<User> userList = new ArrayList<User>();

			List<Integer> allowedUserIds = new ArrayList<Integer>();
			Subject subject = SecurityUtils.getSubject();
			// You must be able to view all the users in a company in order to
			// see anything here.
			if (subject.isPermitted("viewAllUsers")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
							+ currentCompany.getCompanyId())
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":viewAllUsers:" + currentCompany.getCompanyId())) {
				userList = userDao.findActiveUsersByCompanyId(currentCompany.getCompanyId());
			} else {
				userList = authorizationService.getAllowedUsersInCompany(sessionBean.getRoleContext(),
						currentCompany.getCompanyId());
				if (!(userList == null)) {
					log.debug("Got Allowed Users from authorization service.  List size: {}", userList.size());
				} else {
					log.debug("There are no allowed users for {}", subject.getPrincipal());
				}
			}

			if (subject.isPermitted("addSingleParticipantToCompany")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":addSingleParticipantToCompany:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantListViewBean.setAddParticipantButtonDisabled(false);
			}

			for (User user : userList) {
				Integer userId = user.getUsersId();
				String firstName = user.getFirstname();
				String lastName = user.getLastname();
				String email = user.getEmail();
				String username = user.getUsername();
				String opt1 = user.getOptional1();
				String opt2 = user.getOptional2();
				String opt3 = user.getOptional3();
				String sn = user.getSupervisorName();
				String se = user.getSupervisorEmail();
				String pw = user.getPassword();
				boolean coachFlag = user.getCoachFlag();

				if (viewType == 0) {
					ParticipantObj participant = new ParticipantObj(userId, firstName, lastName, email, username, opt1,
							opt2, opt3, sn, se, coachFlag);
					participant.setLanguageId(user.getLangPref());
					participant.setRowKey(userId);
					participant.setPassword(pw);
					participantListViewBean.getParticipants().add(participant);

				} else {
					List<ProjectUser> projectUsers = user.getProjectUsers();
					for (ProjectUser projectUser : projectUsers) {
						Project project = projectUser.getProject();
						if (project != null) {
							ParticipantObj participant = new ParticipantObj(userId, firstName, lastName, email,
									username, opt1, opt2, opt3, sn, se, coachFlag);
							participant.setLanguageId(user.getLangPref());
							participant.setRowKey(projectUser.getRowId());
							participant.setPassword(pw);
							ProjectObj projectObj = new ProjectObj(project.getProjectId(), project.getName(), project
									.getProjectType().getName(), project.getProjectType().getProjectTypeId());
							participant.setProject(projectObj);
							participantListViewBean.getParticipants().add(participant);
						}
					}
				}
			}

			participantInfoViewBean.setUser(true);
			participantListViewBean.setFilteredParticipants(participantListViewBean.getParticipants());

		}
	}

	public void refreshView() {
		firstTime = true;
		initView();
	}

	public ParticipantListViewBean getParticipantListViewBean() {
		return participantListViewBean;
	}

	public void setParticipantListViewBean(ParticipantListViewBean participantListViewBean) {
		this.participantListViewBean = participantListViewBean;
	}

	public void handleInitForNew(ActionEvent event) {
		getParticipantInfoControllerBean().initForNew();
	}

	// Change columns dynamically
	public void handleChangeViewType(AjaxBehaviorEvent event) {

		// refresh view so that participantsobj list gets updated
		refreshView();

		participantListViewBean.setCreateReportsDisabled(true);
		participantListViewBean.setReportUploadDisabled(true);
		participantListViewBean.setRenderReportUpload(true);
		participantListViewBean.setManageProjectDisabled(true);
		participantListViewBean.setSendEmailsDisabled(true);
		participantListViewBean.setAddParticipantsToClipboardDisabled(true);
		participantListViewBean.setAuditLogDisabled(true);
	}

	public void handleInitAuditLog() {
		ParticipantObj[] sParticipants = participantListViewBean.getSelectedParticipants();
		// if (sParticipants.length == 1) {
		List<String> loggerNames = new ArrayList<String>();
		loggerNames.add("AUDIT-ADMIN");
		loggerNames.add("AUDIT-LRM");
		loggerNames.add("AUDIT-AUTHORIZATION");
		loggerNames.add("AUDIT-SCORECARD");
		log.debug("about to search audit log");
		List<String> searchStrings = new ArrayList<String>();
		for (ParticipantObj p : sParticipants) {
			searchStrings.add(p.getUserId().toString());
		}
		auditLogControllerBean.initMultiLog(searchStrings, loggerNames);
		// auditLogControllerBean.initAuditLog(auditLogControllerBean.AUDIT_TYPE_PPT,
		// sParticipants[0].getUserId()
		// .toString(), loggerNames);
		// }
	}

	// Unused
	public void handleParticipantInfoInitForNew(ActionEvent event) {
		getParticipantInfoControllerBean().initForNew();
	}

	public void handleParticipantInfoInitForEdit(ActionEvent event) {
		log.debug("In handleParticipantInfoInitForEdit");
		getParticipantInfoControllerBean().initForEdit();
	}

	public void handleParticipantClipboardInitView(ActionEvent event) {
		getParticipantClipboardControllerBean().initView();
	}

	public void handleRowSelect(SelectEvent event) {
		log.debug("In handleRowSelect");
		Subject subject = SecurityUtils.getSubject();

		// TODO: see if I can just have userDao.find();
		log.debug("Before find user");
		ParticipantObj participant = (ParticipantObj) event.getObject();
		User user = userDao.findById(((ParticipantObj) event.getObject()).getUserId());
		getSessionBean().setUser(user);

		// Get the project if we have one
		if (participant.getProject() != null) {
			Project project = projectDao.findById(participant.getProject().getProjectId());
			sessionBean.setProject(project);
		} else {
			sessionBean.setProject(null);
		}

		log.debug("Finished find user, put on session.  About to get selected participants");
		ParticipantObj[] sParticipants = participantListViewBean.getSelectedParticipants();
		log.debug("Got selected participants, about to enable/disable buttons");
		if (sParticipants != null) {
			int sParticipantsLength = sParticipants.length;
			// Grant access to features if permissions exist at global or client
			// level
			if (subject.isPermitted("accessPalmsAdminClipboard")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":accessPalmsAdminClipboard:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantListViewBean.setAddParticipantsToClipboardDisabled(sParticipantsLength == 0);
			}
			if (subject.isPermitted("viewParticipantDetails")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":viewParticipantDetails:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantListViewBean.setEditParticipantDisabled(sParticipantsLength != 1);
			}
			if (subject.isPermitted("createReportRequest")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":createReportRequest:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantListViewBean.setCreateReportsDisabled(sParticipantsLength == 0 || !isRenderProjects());
				participantListViewBean.setReportUploadDisabled(sParticipantsLength == 0 || !isRenderProjects());
			}

			if (subject.isPermitted("downloadReports")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":downloadReports:"
							+ sessionBean.getCompany().getCompanyId())) {
				log.trace("Subject is permitted to download Reports: {}", participant.getRowKey());
				participantListViewBean.setDownloadReportDisabled(sParticipantsLength != 1);
			}

			if (subject.isPermitted("reportRequestView")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":reportRequestView:"
							+ sessionBean.getCompany().getCompanyId())) {
				log.trace("Subject is permitted to download Reports: {}", participant.getRowKey());
				sessionBean.setReportCenterDisabled(sParticipantsLength != 1);
			}

			if (subject.isPermitted("viewParticipantAuditLog")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":viewParticipantAuditLog:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantListViewBean.setAuditLogDisabled(sParticipantsLength != 1
						&& !(subject.isPermitted("ViewAuditLog")) && isRenderProjects());
			}
			if (subject.isPermitted("sendClientEmails")
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":sendClientEmails:"
							+ sessionBean.getCompany().getCompanyId())) {
				participantListViewBean.setSendEmailsDisabled(false);
			}

			participantListViewBean.setManageProjectDisabled(sParticipants.length == 0 || !isRenderProjects());

		}

		log.debug("Done enabling/disabling buttons.  About to set pptInfoViewBean with ppts from list bean");
		participantInfoViewBean.setParticipants(participantListViewBean.getParticipants());

		log.debug("End handle Row Select");
	}

	public void handleRowUnselect(UnselectEvent event) {
		log.debug("In handleRowUnselect");
		ParticipantObj[] participants = participantListViewBean.getSelectedParticipants();
		log.debug("Num selected: {}", String.valueOf(participants.length));
		participantListViewBean.setEditParticipantDisabled(participants == null || participants.length == 0
				|| participants.length > 1);
		participantListViewBean.setDelParticipantDisabled(participants == null || participants.length == 0);
		participantListViewBean.setAddParticipantsToClipboardDisabled(participants == null || participants.length == 0);
		participantListViewBean.setCreateReportsDisabled(participants == null || participants.length == 0
				|| renderProjects == false);
		participantListViewBean.setDownloadReportDisabled(participants == null || participants.length == 0
				|| renderProjects == false);
		sessionBean
				.setReportCenterDisabled(participants == null || participants.length == 0 || renderProjects == false);
		participantListViewBean.setReportUploadDisabled(participants == null || participants.length == 0
				|| renderProjects == false);
		participantListViewBean.setSendEmailsDisabled(true);
		participantListViewBean.setManageProjectDisabled(participants == null || participants.length == 0
				|| renderProjects == false);

		if (participants == null || participants.length == 0) {
			getSessionBean().clearUser();
		} else if (participants.length == 1) {
			User user = userDao.findById(participants[0].getUserId());
			getSessionBean().setUser(user);
		}
	}

	public ParticipantInfoViewBean getParticipantInfoViewBean() {
		return participantInfoViewBean;
	}

	public void setParticipantInfoViewBean(ParticipantInfoViewBean participantInfoViewBean) {
		this.participantInfoViewBean = participantInfoViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User user) {
		this.currentUser = user;
	}

	public void handleParticipantInfoSave() {
		boolean success = false;
		if (sessionBean.getProject() != null) {
			success = getParticipantInfoControllerBean().saveForProject();
		} else {
			success = getParticipantInfoControllerBean().saveForClient();
		}
		if (success) {
			log.debug("Save Participant Information successful");
			refreshView();
		} else {
			log.error("Save Participant Information failed");
		}
	}

	public void setParticipantInfoControllerBean(ParticipantInfoControllerBean participantInfoControllerBean) {
		this.participantInfoControllerBean = participantInfoControllerBean;
	}

	public ParticipantInfoControllerBean getParticipantInfoControllerBean() {
		return participantInfoControllerBean;
	}

	public void handleAddToClipboard(ActionEvent event) {
		addToClipboard(event);
	}

	private void addToClipboard(ActionEvent event) {
		ParticipantObj[] participants = participantListViewBean.getSelectedParticipants();
		for (ParticipantObj p : participants) {
			sessionBean.addSelectedUser(p);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		StringBuffer detail = new StringBuffer();
		detail.append(participants.length + " participant" + (participants.length != 1 ? "s" : "") + " added.");
		context.addMessage(null, new FacesMessage("Clipboard update, " + detail, detail.toString()));
	}

	public void handleRemoveFromClipboard(ActionEvent event) {
		removeFromClipboard();
	}

	private void removeFromClipboard() {
		for (ParticipantObj p : participantListViewBean.getSelectedParticipants()) {
			sessionBean.removeSelectedUser(p);
		}
	}

	public void loginParticipant() {
		int usersId = getSessionBean().getUser().getUsersId();
		int companyId = getSessionBean().getUser().getCompany().getCompanyId();
		String contentId = "assessment";
		String username = getSessionBean().getUser().getUsername();
		ContentToken contentToken = tokenService.generateToken(usersId, companyId, contentId);
		String token = contentToken.getToken();
		getParticipantListViewBean().setParticipantLoginUrl(
				configProperties.getProperty("participantPortalUrl") + "/faces/tokenLogin.xhtml");
		getParticipantListViewBean().setLoginToken(token);
		getParticipantListViewBean().setLoginUsername(username);
		log.debug("Login URL: {}", getParticipantListViewBean().getParticipantLoginUrl());
	}

	public String getParticipantLoginUrl() {
		return getParticipantListViewBean().getParticipantLoginUrl();
	}

	public void setParticipantClipboardControllerBean(
			ParticipantClipboardControllerBean participantClipboardControllerBean) {
		this.participantClipboardControllerBean = participantClipboardControllerBean;
	}

	public ParticipantClipboardControllerBean getParticipantClipboardControllerBean() {
		return participantClipboardControllerBean;
	}

	public CoachingPlanInfoViewBean getCoachingPlanInfoViewBean() {
		return coachingPlanInfoViewBean;
	}

	public void setCoachingPlanInfoViewBean(CoachingPlanInfoViewBean coachingPlanInfoViewBean) {
		this.coachingPlanInfoViewBean = coachingPlanInfoViewBean;
	}

	public void handleSelectAll(ActionEvent event) {
		ParticipantObj[] parts = new ParticipantObj[participantListViewBean.getParticipants().size()];

		Iterator<ParticipantObj> iterator = participantListViewBean.getParticipants().iterator();
		int i = 0;
		while (iterator.hasNext()) {
			ParticipantObj part = iterator.next();
			parts[i] = part;
			i++;
		}
		participantListViewBean.setSelectedParticipants(parts);

		int j;
		for (j = 0; j < participantListViewBean.getSelectedParticipants().length; j++) {
			System.out.println(participantListViewBean.getSelectedParticipants()[j]);
		}
		System.out.println(participantListViewBean.getSelectedParticipants().length);
		participantListViewBean.setAddParticipantsToClipboardDisabled(false);
		participantListViewBean.setEditParticipantDisabled(true);
		participantListViewBean.setManageProjectDisabled(true);
		participantListViewBean.setCreateReportsDisabled(participantListViewBean.getSelectedParticipants().length == 0);
		participantListViewBean.setReportUploadDisabled(participantListViewBean.getSelectedParticipants().length == 0);
		participantListViewBean.setSendEmailsDisabled(participantListViewBean.getSelectedParticipants().length == 0);
		participantListViewBean.setAuditLogDisabled(true);
	}

	public void handleSelectNone(ActionEvent event) {
		participantListViewBean.setSelectedParticipants(null);
		participantListViewBean.setAddParticipantsToClipboardDisabled(true);
		participantListViewBean.setEditParticipantDisabled(true);
		participantListViewBean.setCreateReportsDisabled(true);
		participantListViewBean.setReportUploadDisabled(true);
		participantListViewBean.setSendEmailsDisabled(true);
		participantListViewBean.setManageProjectDisabled(true);
		participantListViewBean.setAuditLogDisabled(true);
		participantListViewBean.setDownloadReportDisabled(true);
	}

	public void handleSendEmailsInit(ActionEvent event) {
		emailTemplateListControllerBean.refreshView();
	}

	public void handleSendEmails(ActionEvent event) {
		emailTemplateListControllerBean.sendEmailToParticipants(participantListViewBean.getSelectedParticipants());
	}

	public String handleManageProject() {
		ParticipantObj[] participants = participantListViewBean.getSelectedParticipants();
		String navigator = "";

		if (participants.length == 1) {
			Project project = projectDao.findById(participants[0].getProject().getProjectId());
			System.out.println("project = " + project);
			sessionBean.setProject(project);
			if (sessionBean.getProject() != null) {
				navigator = "projectManagement.xhtml?faces-redirect=true";
			}

			System.out.println("user.project = " + participants[0].getProject().getName());
			System.out.println("sessionBean.user = " + sessionBean.getUser().getFirstname());
		}

		System.out.println("navigator1 = " + navigator);

		return navigator;
	}

	public void setViewType(int viewType) {
		this.viewType = viewType;
	}

	public int getViewType() {
		return viewType;
	}

	public void setRenderProjects(boolean renderProjects) {
		this.renderProjects = renderProjects;
	}

	public boolean isRenderProjects() {
		if (this.getViewType() == 0) {
			renderProjects = false;
		} else if (this.getViewType() == 1) {
			renderProjects = true;
		}
		return renderProjects;
	}

	public AuditLogControllerBean getAuditLogControllerBean() {
		return auditLogControllerBean;
	}

	public void setAuditLogControllerBean(AuditLogControllerBean auditLogControllerBean) {
		this.auditLogControllerBean = auditLogControllerBean;
	}

	public EmailTemplateListControllerBean getEmailTemplateListControllerBean() {
		return emailTemplateListControllerBean;
	}

	public void setEmailTemplateListControllerBean(EmailTemplateListControllerBean emailTemplateListControllerBean) {
		this.emailTemplateListControllerBean = emailTemplateListControllerBean;
	}

}
