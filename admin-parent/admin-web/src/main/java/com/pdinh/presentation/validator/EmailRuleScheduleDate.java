package com.pdinh.presentation.validator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FacesValidator("emailRuleScheduleDateValidator")
public class EmailRuleScheduleDate implements Validator {
	
	private static final Logger log = LoggerFactory.getLogger(EmailRuleScheduleDate.class);
	
	private String formatDate(Date source) {
		return new SimpleDateFormat("dd-MMM-yyyy").format(source); 
	}
	
	private Date stripTime(Date date) {
	    //Calendar calendar = Calendar.getInstance();
		
	    Calendar calendar = new GregorianCalendar();

	    calendar.setTime(date);
	    //calendar.set(Calendar.HOUR_OF_DAY, 0);
	    //calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);

	    return calendar.getTime();
	}
	
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		log.debug(">>>>>>>>>>>>>>>>>>>>>>>> value1: {}", value);
		
		Date today = stripTime(new Date());
		Date actualValue = stripTime((Date)value);
		boolean before = actualValue.before(today);
		
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>> value2: {}", actualValue);
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>  today: {}", today);
		
		if(before) {
			FacesMessage msg = new FacesMessage("Estimated schedule date (" + formatDate(actualValue) + ") is in the past.",
					"Please choose another date.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
 	}
}
