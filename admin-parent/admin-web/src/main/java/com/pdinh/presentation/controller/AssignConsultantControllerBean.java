package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.util.ComponentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.helper.TeammemberHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.AssignConsultantViewBean;

@ManagedBean
@ViewScoped
public class AssignConsultantControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AssignConsultantControllerBean.class);
	
	@EJB
	private ProjectUserDao projectUserDao;
	
	@ManagedProperty(name = "teammemberHelper", value = "#{teammemberHelper}")
	private	TeammemberHelper teammemberHelper;
		
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "assignConsultantViewBean", value = "#{assignConsultantViewBean}")
	private AssignConsultantViewBean assignConsultantViewBean;


	public void handleRowSelect(SelectEvent event) {
		updateAddSensitivity();
	}

	public void handleRowUnselect(UnselectEvent event) {
		updateAddSensitivity();
	}

	public void updateAddSensitivity() {
		List<ExternalSubjectDataObj> consultents = assignConsultantViewBean.getSelectedConsultants();
		if (consultents != null) {
			if (consultents.size() > 0) {
				assignConsultantViewBean.setAddToSelectionDisabled(false);
			} else {
				assignConsultantViewBean.setAddToSelectionDisabled(true);
			}
		}
	}

	public void initAvailableConsultants() {
		log.debug("assignConsultantControllerBean.initForSelection()");
		
		//Role consultantRole = teammemberHelper.findRoleByName("Consultant");
		List<ExternalSubjectDataObj> selections = new ArrayList<ExternalSubjectDataObj>();
		
		// Building the list of available consultant
		//teammemberHelper.init(EntityType.PROJECT_TYPE, sessionBean.getProject().getProjectId(), "palmsRealmNames");
		assignConsultantViewBean.setConsultants(getAvailableConsultants());
		
		// Checking if there is any assigned consultant and adding 
		// them to Assigned Consultants section
		List<ParticipantObj> participants = assignConsultantViewBean.getParticipants();
		
		for(ParticipantObj participant : participants) {
			List<ExternalSubjectDataObj> assignedConsultants = participant.getConsultants();
			if(assignedConsultants != null) {
				for(ExternalSubjectDataObj assignConsultant : assignedConsultants) {
					ExternalSubjectDataObj currentConsultant = findConsultant(assignConsultant.getUsername(), selections);
					if(currentConsultant == null) {
						selections.add(assignConsultant);
					}
				}
			}
		}

		// Making sure that Assigned won't appear in Available Consultants list
		if(selections != null) {
			for (ExternalSubjectDataObj record : selections) {
				ExternalSubjectDataObj consultant = findConsultant(record.getUsername(), assignConsultantViewBean.getConsultants());
				if(consultant != null) {
					assignConsultantViewBean.getConsultants().remove(consultant);
				}
			}
		}		
		
		assignConsultantViewBean.setSelections(selections);
		assignConsultantViewBean.setRemovedConsultants(new ArrayList<ExternalSubjectDataObj>());
		
		// reset pagination when dialog is displayed
		resetAvailableConsultants();
	}
	
	private List<ExternalSubjectDataObj> getAvailableConsultants() {
		// TODO: Nw: Make this based on a permission, not a role name
		Role consultantRole = teammemberHelper.findRoleByName("Consultant");
		List<ExternalSubjectDataObj> consultants = new ArrayList<ExternalSubjectDataObj>();
		List<ExternalSubjectDataObj> globalConsultants = new ArrayList<ExternalSubjectDataObj>();
		List<ExternalSubjectDataObj> projectTeam = new ArrayList<ExternalSubjectDataObj>();
		
		teammemberHelper.init(EntityType.PROJECT_TYPE, sessionBean.getProject().getProjectId(), "palmsRealmNames", EntityRule.ALLOW);
		projectTeam = teammemberHelper.fetchTeammembers(EntityType.PROJECT_TYPE, sessionBean.getProject().getProjectId(), "palmsRealmNames");
		globalConsultants = teammemberHelper.getSubjectsWithRole(consultantRole);
		
		// Getting Project lvl consultants
		if(projectTeam != null) {
			for(ExternalSubjectDataObj projectTeammember : projectTeam) {
				if(projectTeammember.getCurrentRole().getRole_id() == consultantRole.getRole_id()){
					consultants.add(projectTeammember);
				}
			}			
		}
		
		// Getting Global lvl consultants
		if(globalConsultants != null) {
			for(ExternalSubjectDataObj globalConsultant : globalConsultants) {
				if(findConsultant(globalConsultant.getUsername(), consultants) == null) {
					consultants.add(globalConsultant);
				}
			}
		}
		
		return consultants;
	}
	
	private ExternalSubjectDataObj findConsultant(String username, List<ExternalSubjectDataObj> consultants) {
		for (ExternalSubjectDataObj record : consultants) {
			if(record.getUsername().equalsIgnoreCase(username)) {
				return record;
			}
		}
		return null;
	}

	private void resetAvailableConsultants() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		DataTable dt = (DataTable) ComponentUtils.findComponent(facesContext.getViewRoot(), "tblAvailableConsultants");
		dt.reset();
	}

	public void remove(ExternalSubjectDataObj pp) {
		log.debug("assignConsultantControllerBean.remove username: {}", pp.getUsername());
		// remove from selection list
		assignConsultantViewBean.getSelections().remove(pp);
		assignConsultantViewBean.getRemovedConsultants().add(pp);
		
		// add back to available list
		assignConsultantViewBean.getConsultants().add(0, pp);

		int availsize = assignConsultantViewBean.getConsultants().size();
		int selsize = assignConsultantViewBean.getSelections().size();
		log.debug("\t in remove, avail count = {}, selected count = {}", availsize, selsize);
	}

	// add to bottom selection list and remove from available list
	public void handleAddToSelection(ActionEvent actionEvent) {
		log.debug("assignConsultantControllerBean.handleAddToSelection");

		List<ExternalSubjectDataObj> selectedConsultants = assignConsultantViewBean.getSelectedConsultants();
		List<ExternalSubjectDataObj> consultants = assignConsultantViewBean.getConsultants();
		List<ExternalSubjectDataObj> selections = assignConsultantViewBean.getSelections();
		
		if(selections == null) {
			selections = new ArrayList<ExternalSubjectDataObj>();
		}
		
		consultants.removeAll(selectedConsultants);
		selections.addAll(selectedConsultants);
		
		log.debug("selectedConsultants = {}", selectedConsultants.size());
		log.debug("selections = {}", selections.size());
		
		assignConsultantViewBean.setSelections(selections);
		assignConsultantViewBean.setSelectedConsultants(null);
		assignConsultantViewBean.setAddToSelectionDisabled(true);

		resetFilters();
	}

	public void resetFilters() {
		assignConsultantViewBean.setFilteredConsultants(null);
	}

	public void save() {
		log.debug("assignConsultantControllerBean.save");
		
		List<ParticipantObj> participants = assignConsultantViewBean.getParticipants();
		List<ExternalSubjectDataObj> consultants = assignConsultantViewBean.getSelections();
		List<ExternalSubjectDataObj> removeConsultants = assignConsultantViewBean.getRemovedConsultants();
		Integer projectId = sessionBean.getProject().getProjectId();
		Integer companyId = sessionBean.getCompany().getCompanyId();
		
		// Making sure that remove consultants list doesn't 
		// have same consultant to to be assign
		for(ExternalSubjectDataObj consultant : consultants) {
			ExternalSubjectDataObj candidate = findConsultant(consultant.getUsername(), removeConsultants);
			if(candidate != null) {
				log.debug("Removing candidate ({}) from removeConsultants list", candidate.getUsername());
				removeConsultants.remove(candidate);
			}
		}
		
		// Assigning and unassigning consultants  
		for(ParticipantObj participant : participants) {
			ProjectUser projectUser = projectUserDao.findByProjectIdAndUserId(projectId, participant.getUserId());
			if(projectUser != null) {
				teammemberHelper.init(EntityType.PROJECT_USER_TYPE, projectUser.getRowId(), "palmsRealmNames", EntityRule.ALLOW);
				if(removeConsultants.size() > 0) {
					teammemberHelper.unassignConsultants(removeConsultants, companyId, projectId, projectUser.getRowId());
				}
				teammemberHelper.assignConsultants(consultants, companyId, projectId, projectUser.getRowId());
				log.debug(">>>>>>>>>>>>>>>>>>>>>> participant.id = {}", participant.getUserId());
				log.debug(">>>>>>>>>>>>>>>>>>>>>> projectUser.id = {}", projectUser.getRowId());
			}
		}

		log.debug("End of save");
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public AssignConsultantViewBean getAssignConsultantViewBean() {
		return assignConsultantViewBean;
	}

	public void setAssignConsultantViewBean(AssignConsultantViewBean assignConsultantViewBean) {
		this.assignConsultantViewBean = assignConsultantViewBean;
	}

	public TeammemberHelper getTeammemberHelper() {
		return teammemberHelper;
	}

	public void setTeammemberHelper(TeammemberHelper teammemberHelper) {
		this.teammemberHelper = teammemberHelper;
	}

}
