package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.extensions.model.dynaform.DynaFormLabel;
import org.primefaces.extensions.model.dynaform.DynaFormModel;
import org.primefaces.extensions.model.dynaform.DynaFormRow;

import com.kf.uffda.model.FormField;
import com.kf.uffda.vo.CustomFieldListValueVo;
import com.kf.uffda.vo.CustomFieldVo;
import com.kf.uffda.vo.CustomFieldsVo;
import com.pdinh.presentation.helper.DateUtil;
import com.pdinh.presentation.helper.LabelUtils;

/**
 * The view bean for custom forms at the end of each section of the Intake form.
 * 
 * @author semrud
 */
public class CustomViewBean implements Serializable {

	private static final long serialVersionUID = 8567818488559217773L;

	private int customViewid;

	private DynaFormModel dynaFormModel;

	private List<FormField> customFormFields;

	private boolean renderCustomForm = false;
	private boolean renderCustomButtonBar = false;

	public CustomViewBean() {
		init();
	}

	public CustomViewBean(int customViewid) {
		super();
		this.customViewid = customViewid;
	}

	public CustomViewBean(int customViewid, boolean renderCustomForm) {
		super();
		this.customViewid = customViewid;
		this.renderCustomForm = renderCustomForm;
	}

	public void init() {
		customViewid = 0;
		dynaFormModel = new DynaFormModel();
		customFormFields = new ArrayList<FormField>();
		renderCustomForm = false;
		renderCustomButtonBar = false;
	}

	public void createCustomForm(CustomFieldsVo customFields, int numberOfColumns) {
		clearDynaFormModel();
		if (customFields != null && customFields.hasFields()) {
			setRenderCustomForm(true);
			addCustomFields(customFields, numberOfColumns);
		}
	}

	private void addCustomFields(CustomFieldsVo customFields, int numberOfColumns) {
		DynaFormModel model = getDynaFormModel();
		DynaFormRow labelRow = null;
		DynaFormRow fieldRow = null;
		DynaFormLabel field = null;
		FormField formfield = null;
		int colspan = 1;

		// Add the Custom Fields to the dynaForm
		customFormFields.clear();
		int column = 0;
		for (CustomFieldVo customField : customFields.getFields()) {
			if (column == 0) {
				if (numberOfColumns > 1) {
					labelRow = model.createRegularRow();
					fieldRow = model.createRegularRow();
				} else {
					fieldRow = model.createRegularRow();
					labelRow = fieldRow;
				}
			}

			if (customField.hasListValues()) {
				List<SelectItem> values = new ArrayList<SelectItem>();

				if (customField.isOneChoiceDropdown() || customField.isYesNoChoiceDropdown()) {
					values.add(new SelectItem("", LabelUtils.getText("SELECT_ONE_LABEL")));
				}

				for (CustomFieldListValueVo listValue : customField.getListValues()) {
					values.add(new SelectItem(Integer.toString(listValue.getId()), listValue.getName()));
				}

				formfield = new FormField(customField.getId(), customField.getWidgetId(), customField.getParentId(),
						customField.getName(), customField.getValue(), customField.isRequired(), values,
						customField.getWidgetType());

				for (CustomFieldListValueVo listValue : customField.getListValues()) {
					if (listValue.hasListValues()) {
						values = new ArrayList<SelectItem>();

						if (customField.isOneChoiceDropdown() || customField.isYesNoChoiceDropdown()) {
							values.add(new SelectItem("", LabelUtils.getText("SELECT_ONE_LABEL")));
						}

						for (CustomFieldListValueVo value : customField.getListValues()) {
							values.add(new SelectItem(Integer.toString(value.getId()), value.getName()));
						}

						formfield.addSelectItems(Integer.toString(listValue.getId()), values);
					}
				}

			} else {
				formfield = new FormField(customField.getId(), customField.getCode(), customField.getParentId(),
						customField.getName(), customField.getValue(), customField.isRequired(),
						customField.getLength(), customField.getMinimum(), customField.getMaximum(),
						customField.getWidgetType());
			}

			// Check if a default value is available
			if (customField.hasDefaultValue()) {

				if (customField.isCharValue()) {
					if (customField.getValue() == null || StringUtils.isBlank(customField.getValue().toString())) {
						formfield.setValue(customField.getDefaultValue());
					}

				} else if (customField.isIntValue()) {
					if (customField.getValue() == null) {
						formfield.setValue(Long.parseLong(customField.getDefaultValue()));
					}

				} else if (customField.isDateValue()) {
					if (customField.getValue() == null) {
						formfield.setValue(DateUtil.parseDate(customField.getDefaultValue()));
					}

				} else if (customField.isListValue()) {
					if (customField.getValue() == null) {
						formfield.setValue(customField.findDefaultListValue());
					}
				}
			}

			colspan = 1;
			if (numberOfColumns > 1
					&& numberOfColumns != (column + 1)
					&& (customField.isMultipleLineTextbox() || (customField.isOneChoiceRadio() && customField
							.getListValues().size() > 2))) {
				colspan = 2;
				column++;
			}

			if (customField.isRequired()) {
				field = labelRow.addLabel(StringEscapeUtils.escapeHtml4(customField.getName())
						+ " <font color='#ff0000'>*</font>", false, colspan, 1);
			} else {
				field = labelRow.addLabel(customField.getName(), colspan, 1);
			}

			field.setForControl(fieldRow.addControl(formfield, customField.getWidgetType(), colspan, 1));

			addCustomFormField(formfield);

			// Increment column
			column = (column + 1) % numberOfColumns;
		}

		// Setup the Parent-Child relationship
		FormField parentField = null;
		for (FormField formField : getCustomFormFields()) {
			if (formField.hasParent()) {
				parentField = findCustomFieldByFieldId(formField.getParentId());
				if (parentField != null) {
					parentField.addChildFormField(formField);
				}
			}
		}
	}

	public boolean validateCustomFormFields(boolean globalError, String errorMsgPrefix) {
		boolean error = false;

		for (FormField field : customFormFields) {
			String widgetId = null;
			if (!globalError) {
				widgetId = field.getWidgetId();
			}

			if (field.isRequired() && StringUtils.isBlank(field.getValueAsStr())) {

				if (field.getWidgetType() != null && StringUtils.isNotEmpty(field.getRequiredMessage())) {
					LabelUtils.addErrorMessage(widgetId, errorMsgPrefix + field.getRequiredMessage());
					error = true;

				} else if (field.getWidgetType() != null && StringUtils.isNotEmpty(field.getFieldLabel())) {
					LabelUtils.addFormattedErrorMessage(widgetId,
							errorMsgPrefix + LabelUtils.getText("MISSING_DATA_MSG"), field.getFieldLabel());
					error = true;
				}
			}

			if (field.getWidgetType().equalsIgnoreCase(CustomFieldVo.TEXTBOX_WIDGET)) {

				String fieldText = field.getValueAsStr();
				if (StringUtils.isNotBlank(fieldText) && fieldText.length() < field.getMinimum()) {
					LabelUtils.addFormattedErrorMessage(widgetId,
							errorMsgPrefix + LabelUtils.getText("NOT_ENOUGH_DATA_MSG"),
							Long.toString(field.getMinimum()), field.getFieldLabel());
					error = true;
				}
			}

			if (field.getWidgetType().equalsIgnoreCase(CustomFieldVo.SPINNER_WIDGET)) {

				if (StringUtils.isNotBlank(field.getValueAsStr())) {
					if (field.getValueAsInt() < field.getMinimum()) {
						LabelUtils.addFormattedErrorMessage(widgetId,
								errorMsgPrefix + LabelUtils.getText("MINIMUM_VALUE_MSG"),
								Long.toString(field.getMinimum()), field.getFieldLabel());
						error = true;

					} else if (field.getValueAsInt() > field.getMaximum()) {
						LabelUtils.addFormattedErrorMessage(widgetId,
								errorMsgPrefix + LabelUtils.getText("MAXIMUM_VALUE_MSG"),
								Long.toString(field.getMaximum()), field.getFieldLabel());
						error = true;
					}
				}
			}
		}

		return error;
	}

	public void updateCustomFieldData(CustomFieldsVo customFields) {
		// Setup the Parent-Child relationship
		for (FormField formField : getCustomFormFields()) {
			CustomFieldVo customField = customFields.findFieldById(formField.getFieldId());
			if (customField != null) {
				customField.setValue(formField.getValue());
			}
		}
	}

	public boolean hasCustomFormFields() {
		return customFormFields.size() > 0;
	}

	public DynaFormModel getDynaFormModel() {
		return dynaFormModel;
	}

	public void clearDynaFormModel() {
		setRenderCustomForm(false);
		dynaFormModel = new DynaFormModel();
	}

	public DynaFormModel createForm() {
		return dynaFormModel;
	}

	public boolean getRenderCustomForm() {
		return renderCustomForm;
	}

	public void setRenderCustomForm(boolean renderCustomForm) {
		this.renderCustomForm = renderCustomForm;
	}

	public boolean getRenderCustomButtonBar() {
		return renderCustomButtonBar;
	}

	public void setRenderCustomButtonBar(boolean renderCustomButtonBar) {
		this.renderCustomButtonBar = renderCustomButtonBar;
	}

	public int getCustomViewid() {
		return customViewid;
	}

	public void setCustomViewid(int customViewid) {
		this.customViewid = customViewid;
	}

	public String getBeanInfo() {
		return this.toString();
	}

	public List<FormField> getCustomFormFields() {
		return customFormFields;
	}

	public FormField addCustomFormField(FormField field) {
		customFormFields.add(field);
		return field;
	}

	public FormField findCustomFieldByWidgetId(String widgetId) {
		for (FormField field : customFormFields) {
			if (field.getWidgetId().equalsIgnoreCase(widgetId)) {
				return field;
			}
		}
		return null;
	}

	public FormField findCustomFieldByFieldId(int fieldId) {
		for (FormField field : customFormFields) {
			if (field.getFieldId() == fieldId) {
				return field;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return super.toString() + " [customViewid=" + customViewid + ", renderCustomForm=" + renderCustomForm + "]";
	}
}
