package com.pdinh.presentation.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.dto.StandardFieldDto;
import com.kf.uffda.view.FieldsViewBean;
import com.kf.uffda.vo.CoreFieldGroupVo;

@ManagedBean(name = "fieldDictionaryViewBean")
@ViewScoped
public class FieldDictionaryViewBean extends FieldsViewBean<FieldDto> implements Serializable {

	private static final long serialVersionUID = -3317081545538391699L;

	private boolean showCustomField;
	private boolean showStandardField;
	private boolean addIntakeFieldDisabled;
	private boolean editOnly;
	private String fieldGroupCode;

	public FieldDictionaryViewBean() {
		super();
		init();
	}

	@Override
	public void init() {
		super.init();
		super.setField(new FieldDto());
		super.setSortBySequenceOrder(false);

		showCustomField = false;
		showStandardField = false;
		addIntakeFieldDisabled = true;
		editOnly = false;
		fieldGroupCode = CoreFieldGroupVo.PDA;
	}

	public boolean isShowCustomField() {
		return showCustomField;
	}

	public void setShowCustomField(boolean showCustomField) {
		this.showCustomField = showCustomField;
	}

	public boolean isShowDictionary() {
		return !showStandardField && !showCustomField && !editOnly;
	}

	public boolean isAddIntakeFieldDisabled() {
		return addIntakeFieldDisabled;
	}

	public void setAddIntakeFieldDisabled(boolean addIntakeFieldDisabled) {
		this.addIntakeFieldDisabled = addIntakeFieldDisabled;
	}

	public boolean isEditOnly() {
		return editOnly;
	}

	public void setEditOnly(boolean editOnly) {
		this.editOnly = editOnly;
	}

	public String getCustomFieldHeader() {
		return "Field Dictionary";
	}

	public CustomFieldDto findCustomFieldByDatabaseId(int databaseId) {
		for (FieldDto field : getFields()) {
			if (field.isCustomField() && field.getDatabaseId() == databaseId) {
				return (CustomFieldDto) field;
			}
		}
		return null;
	}

	public StandardFieldDto findStandardFieldByDatabaseId(int databaseId) {
		for (FieldDto field : getFields()) {
			if (field.isStandardField() && field.getDatabaseId() == databaseId) {
				return (StandardFieldDto) field;
			}
		}
		return null;
	}

	public boolean isShowStandardField() {
		return showStandardField;
	}

	public void setShowStandardField(boolean showStandardField) {
		this.showStandardField = showStandardField;
	}

	public String getFieldGroupCode() {
		return fieldGroupCode;
	}

	public void setFieldGroupCode(String fieldGroupCode) {
		this.fieldGroupCode = fieldGroupCode;
	}

}
