package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.auth.persistence.entity.AuditEvent;
import com.pdinh.persistence.dto.CompanyProductStatus;

@ManagedBean(name = "productEnablementViewBean")
@ViewScoped
public class ProductEnablementViewBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CompanyProductStatus selectedProduct;
	private List<CompanyProductStatus> productStatuses;

	private List<CompanyProductStatus> customStatuses;

	private List<CompanyProductStatus> filteredProducts;
	private boolean enableButtonDisabled = true;
	private boolean disableButtonDisabled = true;
	private int companyId;
	private String companyName;
	
	public CompanyProductStatus getSelectedProduct() {
		return selectedProduct;
	}
	public void setSelectedProduct(CompanyProductStatus selectedProduct) {
		this.selectedProduct = selectedProduct;
	}
	public List<CompanyProductStatus> getProductStatuses() {
		return productStatuses;
	}
	public void setProductStatuses(List<CompanyProductStatus> productStatuses) {
		this.productStatuses = productStatuses;
	}
	
	public List<CompanyProductStatus> getFilteredProducts() {
		return filteredProducts;
	}
	public void setFilteredProducts(List<CompanyProductStatus> filteredProducts) {
		this.filteredProducts = filteredProducts;
	}
	public boolean isEnableButtonDisabled() {
		return enableButtonDisabled;
	}
	public void setEnableButtonDisabled(boolean enableButtonDisabled) {
		this.enableButtonDisabled = enableButtonDisabled;
	}
	public boolean isDisableButtonDisabled() {
		return disableButtonDisabled;
	}
	public void setDisableButtonDisabled(boolean disableButtonDisabled) {
		this.disableButtonDisabled = disableButtonDisabled;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<CompanyProductStatus> getCustomStatuses() {
		return customStatuses;
	}
	public void setCustomStatuses(List<CompanyProductStatus> customStatuses) {
		this.customStatuses = customStatuses;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


}
