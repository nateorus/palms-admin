package com.pdinh.presentation.helper;

import java.io.Serializable;

public class RoleColumnModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String roleName;
	private String header;
	
	public RoleColumnModel(String roleName, String header){
		this.roleName = roleName;
		this.header = header;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}

}
