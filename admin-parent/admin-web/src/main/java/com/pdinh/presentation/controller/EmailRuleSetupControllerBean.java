package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleCriteriaTypeDao;
import com.pdinh.data.ms.dao.EmailRuleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleTypeDao;
import com.pdinh.data.ms.dao.ProjectTypeDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.data.ms.dao.ScheduledEmailTextDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.manage.emailtemplates.EmailTemplatesService;
import com.pdinh.persistence.ms.entity.EmailRule;
import com.pdinh.persistence.ms.entity.EmailRuleCriteria;
import com.pdinh.persistence.ms.entity.EmailRuleCriteriaType;
import com.pdinh.persistence.ms.entity.EmailRuleParticipant;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleFixed;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleRelative;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleType;
import com.pdinh.persistence.ms.entity.EmailRuleToProcess;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.EmailRuleCriteriaObj;
import com.pdinh.presentation.domain.EmailRuleCriteriaTypeObj;
import com.pdinh.presentation.domain.EmailRuleScheduleStatusObj;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.FixedDateObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.RelativeDateObj;
import com.pdinh.presentation.helper.RelativeDateCalculator;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EmailRuleSetupViewBean;
import com.pdinh.presentation.view.EmailTemplatesListCompViewBean;
import com.pdinh.presentation.view.ProjectEmailViewBean;
import com.pdinh.presentation.view.ProjectParticipantsViewBean;

@ManagedBean
@ViewScoped
public class EmailRuleSetupControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(EmailRuleSetupControllerBean.class);

	@EJB
	UserDao userDao;

	@EJB
	EmailRuleDao emailRuleDao;

	@EJB
	EmailRuleCriteriaTypeDao emailRuleCriteriaTypeDao;

	@EJB
	EmailRuleScheduleTypeDao emailRuleScheduleTypeDao;

	@EJB
	EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	ScheduledEmailDefDao scheduledEmailDefDao;

	@EJB
	ScheduledEmailTextDao scheduledEmailTextDao;

	@EJB
	EmailRuleScheduleDao emailRuleScheduleDao;

	@EJB
	ProjectTypeDao projectTypeDao;

	@EJB
	EmailTemplatesService emailTemplatesService;

	@ManagedProperty(name = "emailRuleSetupViewBean", value = "#{emailRuleSetupViewBean}")
	private EmailRuleSetupViewBean emailRuleSetupViewBean;

	@ManagedProperty(name = "projectEmailViewBean", value = "#{projectEmailViewBean}")
	private ProjectEmailViewBean projectEmailViewBean;

	@ManagedProperty(name = "projectParticipantsControllerBean", value = "#{projectParticipantsControllerBean}")
	private ProjectParticipantsControllerBean projectParticipantsControllerBean;

	@ManagedProperty(name = "projectParticipantsViewBean", value = "#{projectParticipantsViewBean}")
	private ProjectParticipantsViewBean projectParticipantsViewBean;

	@ManagedProperty(name = "emailRuleScheduleSetupControllerBean", value = "#{emailRuleScheduleSetupControllerBean}")
	private EmailRuleScheduleSetupControllerBean emailRuleScheduleSetupControllerBean;

	@ManagedProperty(name = "editEmailTemplateDetailsCompControllerBean", value = "#{editEmailTemplateDetailsCompControllerBean}")
	private EditEmailTemplateDetailsCompControllerBean editEmailTemplateDetailsCompControllerBean;

	@ManagedProperty(name = "emailTemplatesListCompViewBean", value = "#{emailTemplatesListCompViewBean}")
	private EmailTemplatesListCompViewBean emailTemplatesListCompViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	private EmailRule currentEmailRule;

	private Boolean inNew = false;
	private List<EmailTemplateObj> templatesList;
	private Boolean firstTime = true;
	private final int NEW_RULE = 1;
	private final int EDIT_RULE = 2;
	private int action = NEW_RULE;

	public void initView() {
		if (firstTime) {

			firstTime = false;

			setTemplatesList(findProjectEmailTemplates());

			if (emailRuleSetupViewBean.isNewEmailRule()) {
				initForNew();
			} else {
				initForEdit();
			}

			emailRuleSetupViewBean.setCriteriaMenuItems(fetchCriteriaTypes());
			emailRuleScheduleSetupControllerBean.initView();

		}
	}

	private void refresh() {
		firstTime = true;
	}

	public void initForNew() {
		setInNew(true);
		action = NEW_RULE;

		emailRuleSetupViewBean.setName("");
		emailRuleSetupViewBean.setActive(true);
		emailRuleSetupViewBean.setTemplates(getTemplatesList());
		emailRuleSetupViewBean.setTemplateId(null);
		emailRuleSetupViewBean.setLanguages(null);
		emailRuleSetupViewBean.setLanguageCode(null);
		emailRuleSetupViewBean.setAllOrSelectedSelection(true);
		emailRuleSetupViewBean.setDisableExcludedSelection(false);
		emailRuleSetupViewBean.setExcludedSelection(false);
		emailRuleSetupViewBean.setCriteriaItems(new ArrayList<EmailRuleCriteriaObj>());
		emailRuleSetupViewBean.setScheduledItems(new ArrayList<Object>());
		emailRuleSetupViewBean.setDisableAddCriteria(false);
		emailRuleSetupViewBean.setDisableParticipantsTab(true);
		emailRuleSetupViewBean.setParticipants(new ArrayList<ParticipantObj>());
		emailRuleSetupViewBean.setDisableRemoveParticipants(true);
	}

	public void initForEdit() {
		setInNew(false);
		action = EDIT_RULE;
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> In EmailRuleSetupControllerBean.initForEdit() (START)");

		int emailRuleId = emailRuleSetupViewBean.getCurrectRuleId();
		int currentProjectId = sessionBean.getProject().getProjectId();
		EmailRule emailRule = emailRuleDao.findById(emailRuleId);

		log.debug(">>>>>>>>>>>>>>>> emailRule: {}", emailRule);

		if (emailRule != null) {

			// Checking if current project and email rule project ids are
			// match to prevent unwanted "email rule" access by modifying query
			// string.
			if (currentProjectId != emailRule.getProject().getProjectId()) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Nice try buddy, but you have no rights to edit this email rule!", ""));
				emailRuleSetupViewBean.setNoCriticalError(false);
				return;
			}

			List<EmailRuleCriteriaObj> emailRuleCirieriaObjs = new ArrayList<EmailRuleCriteriaObj>();
			List<Object> emailRuleScheduleObjs = new ArrayList<Object>();
			List<ParticipantObj> participantObjs = new ArrayList<ParticipantObj>();

			emailRuleSetupViewBean.setName(emailRule.getName());
			emailRuleSetupViewBean.setActive(emailRule.getActive());
			emailRuleSetupViewBean.setTemplates(getTemplatesList());
			emailRuleSetupViewBean.setTemplateId(emailRule.getEmailTemplate().getId());
			emailRuleSetupViewBean.setLanguages(getTemplateLanguages());
			emailRuleSetupViewBean.setLanguageCode(emailRule.getLanguage());
			emailRuleSetupViewBean.setAllOrSelectedSelection(emailRule.getAllParticipants());
			emailRuleSetupViewBean.setExcludedSelection(emailRule.getExcludedParticipants());

			if (!emailRuleSetupViewBean.getTemplateId().equals(-1)) {
				emailRuleSetupViewBean.setDisableEditTemplate(false);
				emailRuleSetupViewBean.setDisableCloneTemplate(false);
			}

			for (EmailRuleCriteria emailRuleCriteria : emailRule.getEmailRuleCiteria()) {
				EmailRuleCriteriaObj criteriaObj = new EmailRuleCriteriaObj();
				criteriaObj.setLabel(emailRuleCriteria.getEmailRuleCriteriaType().getName());
				criteriaObj.setValue(emailRuleCriteria.getEmailRuleCriteriaType().getId());
				criteriaObj.setId(emailRuleCriteria.getId());
				emailRuleCirieriaObjs.add(criteriaObj);
			}

			emailRuleSetupViewBean.setCriteriaItems(emailRuleCirieriaObjs);

			for (EmailRuleSchedule emailRuleSchedule : emailRule.getEmailRuleSchedules()) {
				Object emailRuleScheduleObj = new Object();

				EmailRuleScheduleStatusObj emailRuleScheduleStatusObj = new EmailRuleScheduleStatusObj(
						emailRuleSchedule.getEmailRuleScheduleStatus().getId(), emailRuleSchedule
								.getEmailRuleScheduleStatus().getName(), emailRuleSchedule.getEmailRuleScheduleStatus()
								.getCode());

				if (emailRuleSchedule.getEmailRuleScheduleType().getCode().equals("FIXED_DATE")) {
					FixedDateObj fixedDate = new FixedDateObj();
					fixedDate.setId(emailRuleSchedule.getId());
					fixedDate.setRuleId(emailRuleSchedule.getEmailRule().getId());
					fixedDate.setDate(((EmailRuleScheduleFixed) emailRuleSchedule).getDate());
					fixedDate.setComplete(emailRuleSchedule.isComplete());
					fixedDate.setError(emailRuleSchedule.isError());
					fixedDate.setCurrentStatus(emailRuleScheduleStatusObj);
					if (!(emailRuleSchedule.getTimezone() == null)){
						TimeZone zone = TimeZone.getTimeZone(emailRuleSchedule.getTimezone());
						fixedDate.setTimezone(zone);
					}
					
					emailRuleScheduleObj = fixedDate;
				} else if (emailRuleSchedule.getEmailRuleScheduleType().getCode().equals("RELATIVE_DATE")) {
					RelativeDateObj relativeDate = new RelativeDateObj();
					relativeDate.setId(emailRuleSchedule.getId());
					relativeDate.setRuleId(emailRuleSchedule.getEmailRule().getId());
					relativeDate.setDays(((EmailRuleScheduleRelative) emailRuleSchedule).getDays());
					relativeDate.setDaysAfter(((EmailRuleScheduleRelative) emailRuleSchedule).getDaysAfter());
					relativeDate.setRelativeToDate(((EmailRuleScheduleRelative) emailRuleSchedule).getRelativeToDate());
					relativeDate.setComplete(emailRuleSchedule.isComplete());
					relativeDate.setCurrentStatus(emailRuleScheduleStatusObj);
					TimeZone zone;
					if (!(emailRuleSchedule.getTimezone() == null)){
						zone = TimeZone.getTimeZone(emailRuleSchedule.getTimezone());
						relativeDate.setTimezone(zone);
					}else{
						zone = TimeZone.getDefault();
					}
					Calendar time = new GregorianCalendar();
					time.setTime(relativeDate.getRelativeToDate());
					time.setTimeZone(zone);
					//The minutes and hours are stored as part of the Relative to Date
					//Otherwise the relative to date is the project due date.
					int hour = time.get(Calendar.HOUR_OF_DAY);
					int minute = time.get(Calendar.MINUTE);
					boolean daysAfter = relativeDate.getDaysAfter();
					Date calcTime = RelativeDateCalculator.calculateRelativeDateWithTime(daysAfter, 
							relativeDate.getDays(), new Date(sessionBean.getProject().getDueDate().getTime()), hour, minute, zone);
					relativeDate.setTime(calcTime);
					emailRuleScheduleObj = relativeDate;
				}
				emailRuleScheduleObjs.add(emailRuleScheduleObj);
			}

			emailRuleSetupViewBean.setScheduledItems(emailRuleScheduleObjs);

			for (EmailRuleParticipant emailRuleParticipant : emailRule.getEmailRuleParticipants()) {
				ParticipantObj participant = new ParticipantObj();
				participant.setUserId(emailRuleParticipant.getUser().getUsersId());
				participant.setFirstName(emailRuleParticipant.getUser().getFirstname());
				participant.setLastName(emailRuleParticipant.getUser().getLastname());
				participant.setEmail(emailRuleParticipant.getUser().getEmail());
				participant.setUserName(emailRuleParticipant.getUser().getUsername());
				participantObjs.add(participant);
			}

			emailRuleSetupViewBean.setParticipants(participantObjs);

			if (!emailRuleSetupViewBean.getAllOrSelectedSelection()) {
				emailRuleSetupViewBean.setDisableExcludedSelection(true);
			}

			emailRuleSetupViewBean.setDisableParticipantsTab(!isParticipantsRequired());
			emailRuleSetupViewBean.setDisableRulesTable(false);

		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unfortunately email rule id #" + emailRuleId
							+ " doesn't exist in the system!", ""));
			emailRuleSetupViewBean.setNoCriticalError(false);
		}

	}

	// Disable or Enable Participants tab
	private boolean isParticipantsRequired() {
		if (emailRuleSetupViewBean.getAllOrSelectedSelection() && !emailRuleSetupViewBean.getExcludedSelection()) {
			return false;
		} else if (emailRuleSetupViewBean.getAllOrSelectedSelection() && emailRuleSetupViewBean.getExcludedSelection()) {
			return true;
		} else if (!emailRuleSetupViewBean.getAllOrSelectedSelection()
				&& !emailRuleSetupViewBean.getExcludedSelection()) {
			return true;
		}
		return false;
	}

	private List<EmailTemplateObj> findProjectEmailTemplates() {
		List<EmailTemplateObj> emailTemplateObjs = new ArrayList<EmailTemplateObj>();
		List<ScheduledEmailDef> templates = emailTemplatesService.getEmailTemplatesByLevel(ScheduledEmailDef.PROJ,
				sessionBean.getCompany(), sessionBean.getProject());

		for (ScheduledEmailDef template : templates) {
			emailTemplateObjs.add(emailTemplatesService.scheduledEmailDef2EmailTemplateObj(template));
		}

		return emailTemplateObjs;
	}

	public String getTemplateLvlInitial(EmailTemplateObj template) {
		String initial = "";
		if (template.getLevel().equals(ScheduledEmailDef.PROJ)) {
			initial = "P";
		} else if (template.getLevel().equals(ScheduledEmailDef.CLIENT)) {
			initial = "C";
		} else if (template.getLevel().equals(ScheduledEmailDef.GLOBAL)) {
			initial = "G";
		}
		return initial;
	}

	private List<ListItemObj> getTemplateLanguages() {
		Integer templateId = emailRuleSetupViewBean.getTemplateId();
		EmailTemplateObj template = getEmailTemplateObj(templateId);
		List<ListItemObj> templateLanguages = new ArrayList<ListItemObj>();
		templateLanguages = emailTemplatesService.getTemplateLanguges(template);
		return templateLanguages;
	}

	private EmailTemplateObj getEmailTemplateObj(Integer templateId) {
		List<EmailTemplateObj> templates = getTemplatesList();
		for (EmailTemplateObj template : templates) {
			log.trace(">>>>>>>>>>>>>> getEmailTemplateObj {} == {}", template.getId(), templateId);
			if (template.getId().equals(templateId)) {
				log.debug(">>>>> getEmailTemplateObj returning template id {}", templateId);
				return template;
			}
		}
		return null;
	}

	private List<EmailRuleCriteriaTypeObj> fetchCriteriaTypes() {
		List<EmailRuleCriteriaTypeObj> criteriaObjs = new ArrayList<EmailRuleCriteriaTypeObj>();
		List<EmailRuleCriteriaType> criteriaTypes = emailRuleCriteriaTypeDao.findAll();
		for (EmailRuleCriteriaType criteriaType : criteriaTypes) {
			EmailRuleCriteriaTypeObj criteriaObj = new EmailRuleCriteriaTypeObj();
			criteriaObj.setName(criteriaType.getName());
			criteriaObj.setId(criteriaType.getId());
			criteriaObj.setCode(criteriaType.getCode());
			criteriaObjs.add(criteriaObj);
		}
		return criteriaObjs;
	}

	public void handleTemplateSelection(AjaxBehaviorEvent event) {
		EmailTemplateObj template = getEmailTemplateObj(emailRuleSetupViewBean.getTemplateId());
		log.debug(">>>>>>>>>>>>>> handleTemplateSelection Template Id = {}", emailRuleSetupViewBean.getTemplateId());
		log.debug(">>>>>>>>>>>>>> handleTemplateSelection template = {}", template);

		if (template != null) {
			emailRuleSetupViewBean.setLanguages(getTemplateLanguages());
			emailRuleSetupViewBean.setDisableEditTemplate(false);
			emailRuleSetupViewBean.setDisableCloneTemplate(false);
		} else {
			emailRuleSetupViewBean.setLanguages(new ArrayList<ListItemObj>());
			emailRuleSetupViewBean.setDisableEditTemplate(true);
			emailRuleSetupViewBean.setDisableCloneTemplate(true);
		}
	}

	public void deleteSchedule(Object obj) {
		emailRuleSetupViewBean.getScheduledItems().remove(obj);
	}

	public void addCriteria(ActionEvent event) {
		emailRuleSetupViewBean.getCriteriaItems().add(new EmailRuleCriteriaObj(0, null));
		validateAddCriteriaButton();
	}

	public void editCriteria(AjaxBehaviorEvent event) {
	}

	public void deleteCritaria(Object obj) {
		emailRuleSetupViewBean.getCriteriaItems().remove(obj);
		validateAddCriteriaButton();
	}

	private void validateAddCriteriaButton() {
		if (emailRuleSetupViewBean.getCriteriaItems().size() >= 3) {
			emailRuleSetupViewBean.setDisableAddCriteria(true);
		} else {
			emailRuleSetupViewBean.setDisableAddCriteria(false);
		}
	}

	public void handleAllOrSelectedParticipants(AjaxBehaviorEvent event) {
		Boolean selected = emailRuleSetupViewBean.getAllOrSelectedSelection();
		emailRuleSetupViewBean.setExcludedSelection(false);
		emailRuleSetupViewBean.setDisableParticipantsTab(!isParticipantsRequired());
		emailRuleSetupViewBean.setDisableExcludedSelection(!selected);
	}

	public void handleExcludedSelection(AjaxBehaviorEvent event) {
		emailRuleSetupViewBean.setDisableParticipantsTab(!isParticipantsRequired());
	}

	public void handleParticipantsInitForSelection(ActionEvent event) {
		getProjectParticipantsControllerBean().initForSelection(true);
		emailRuleSetupViewBean.setSelectedParticipants(null);
	}

	public void handleParticipantsRowSelect(SelectEvent event) {
		emailRuleSetupViewBean.setDisableRemoveParticipants(disableRemoveParticipantsButton());
	}

	public void handleParticipantsRowUnselect(UnselectEvent event) {
		emailRuleSetupViewBean.setDisableRemoveParticipants(disableRemoveParticipantsButton());
	}

	private Boolean disableRemoveParticipantsButton() {
		int size = emailRuleSetupViewBean.getSelectedParticipants().size();
		return (size > 0) ? false : true;
	}

	private Boolean validateAndSave() {
		Boolean errors = requiredFieldsPresent();
		return (!errors) ? save() : false;
	}

	private Boolean requiredFieldsPresent() {
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rc = RequestContext.getCurrentInstance();
		Boolean error = false;
		String participantType = "";

		if (emailRuleSetupViewBean.getExcludedSelection()) {
			participantType = "Excluded";
		} else if (!emailRuleSetupViewBean.getAllOrSelectedSelection()) {
			participantType = "Included";
		}

		if (emailRuleSetupViewBean.getName().equals("")) {
			fc.addMessage("ruleErrors", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Rule Name required.", ""));
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		if (emailRuleSetupViewBean.getTemplateId() == -1) {
			fc.addMessage("ruleErrors", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email Template required.", ""));
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		if (emailRuleSetupViewBean.getScheduledItems().size() <= 0) {
			fc.addMessage("ruleErrors", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Please add at least one Schedule.", ""));
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		if (isParticipantsRequired() && emailRuleSetupViewBean.getParticipants().size() <= 0) {
			fc.addMessage("ruleErrors", new FacesMessage(FacesMessage.SEVERITY_ERROR, participantType
					+ " Participants list is empty, you need to create a list in " + participantType
					+ " Participants tab.", ""));
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		return error;
	}

	public Boolean getInNew() {
		return inNew;
	}

	public void setInNew(Boolean inNew) {
		this.inNew = inNew;
	}

	public void removeParticipants(ActionEvent event) {
		List<ParticipantObj> selected = emailRuleSetupViewBean.getSelectedParticipants();
		List<ParticipantObj> participants = emailRuleSetupViewBean.getParticipants();
		if (participants.removeAll(selected)) {
			emailRuleSetupViewBean.setDisableRemoveParticipants(true);
		}
	}

	public boolean save() {
		if (action == NEW_RULE) {
			EmailRule newEmailRule = new EmailRule();
			newEmailRule = setFields(newEmailRule, true);
			setCurrentEmailRule(emailRuleDao.create(newEmailRule));
		} else if (action == EDIT_RULE) {
			int emailRuleId = emailRuleSetupViewBean.getCurrectRuleId();
			EmailRule updateEmailRule = emailRuleDao.findById(emailRuleId);
			updateEmailRule = setFields(updateEmailRule, false);
			emailRuleDao.update(updateEmailRule);
		}
		return true;
	}

	private EmailRule setFields(EmailRule rule, Boolean isNew) {

		EmailRuleSetupViewBean ruleSetupView = getEmailRuleSetupViewBean();
		List<EmailRuleCriteria> emailRuleCriterias = new ArrayList<EmailRuleCriteria>();
		List<EmailRuleSchedule> emailRuleSchedules = new ArrayList<EmailRuleSchedule>();
		List<EmailRuleToProcess> emailRulesToProcess = new ArrayList<EmailRuleToProcess>();
		List<EmailRuleParticipant> emailRuleParticipants = new ArrayList<EmailRuleParticipant>();

		// Populating Email Rule with General Information
		rule.setName(ruleSetupView.getName());

		log.debug(">>>>>>>>>>>> ruleSetupView.getActive: {}", ruleSetupView.getActive());

		rule.setActive(ruleSetupView.getActive());

		ScheduledEmailDef emailTemplate = scheduledEmailDefDao.findById(ruleSetupView.getTemplateId());

		rule.setEmailTemplate(emailTemplate);
		rule.setLanguage(ruleSetupView.getLanguageCode());
		rule.setProject(sessionBean.getProject());
		rule.setCompany(sessionBean.getCompany());

		rule.setAllParticipants(ruleSetupView.getAllOrSelectedSelection());
		rule.setExcludedParticipants(ruleSetupView.getExcludedSelection());
		rule.setDateCreated(new Timestamp((new Date()).getTime()));

		// Creating Email Rule Criteria records
		if (ruleSetupView.getCriteriaItems().size() > 0) {
			for (EmailRuleCriteriaObj criteriaItem : ruleSetupView.getCriteriaItems()) {

				EmailRuleCriteria emailRuleCriteria = new EmailRuleCriteria();
				// If we have an ID then update the persistent object
				if (!(criteriaItem.getId() == null)) {
					List<EmailRuleCriteria> criteria = rule.getEmailRuleCiteria();
					for (EmailRuleCriteria criterium : criteria) {
						if (criteriaItem.getId() == criterium.getId()) {
							emailRuleCriteria = criterium;
							break;
						}
					}
				}
				emailRuleCriteria.setEmailRule(rule);
				EmailRuleCriteriaType criteriaType = emailRuleCriteriaTypeDao.findById(criteriaItem.getValue());
				emailRuleCriteria.setEmailRuleCriteriaType(criteriaType);
				emailRuleCriterias.add(emailRuleCriteria);
			}
			rule.setEmailRuleCiteria(emailRuleCriterias);
		}

		// Creating Email Rule Schedules records
		for (Object scheduledItem : ruleSetupView.getScheduledItems()) {
			if (getScheduleType(scheduledItem) == "fixed") {
				FixedDateObj fixedDateObj = (FixedDateObj) scheduledItem;
				EmailRuleScheduleFixed emailRuleScheduleFixed = new EmailRuleScheduleFixed();

				if (fixedDateObj.getId() == null) {
					emailRuleScheduleFixed.setEmailRule(rule);
					emailRuleScheduleFixed.setError(false);
					emailRuleScheduleFixed.setComplete(false);
					EmailRuleScheduleStatus scheduleStatus = emailRuleScheduleStatusDao.findById(2);
					emailRuleScheduleFixed.setEmailRuleScheduleStatus(scheduleStatus);
				} else {
					// If we have an ID then this is an edit.. Retrieve the
					// persistent object
					emailRuleScheduleFixed = (EmailRuleScheduleFixed) emailRuleScheduleDao.findById(fixedDateObj
							.getId());
					emailRuleScheduleFixed.setError(fixedDateObj.isError());
					emailRuleScheduleFixed.setComplete(fixedDateObj.isComplete());
					EmailRuleScheduleStatus scheduleStatus = emailRuleScheduleStatusDao.findById(fixedDateObj
							.getCurrentStatus().getId());
					emailRuleScheduleFixed.setEmailRuleScheduleStatus(scheduleStatus);
				}

				Timestamp fixedDate = new Timestamp(fixedDateObj.getDate().getTime());
				emailRuleScheduleFixed.setDate(fixedDate);
				if (!(fixedDateObj.getTimezone() == null)){
					emailRuleScheduleFixed.setTimezone(fixedDateObj.getTimezone().getID());
				}else{
					emailRuleScheduleFixed.setTimezone(TimeZone.getDefault().getID());
				}
				EmailRuleScheduleType scheduleType = emailRuleScheduleTypeDao.findById(1);
				emailRuleScheduleFixed.setEmailRuleScheduleType(scheduleType);

				log.debug(">>>>>>>>>>>>>>>>>>>>>>> Fixed Date Id: {}", fixedDateObj.getId());
				emailRuleSchedules.add(emailRuleScheduleFixed);
			} else if (getScheduleType(scheduledItem) == "relative") {
				RelativeDateObj relativeDateObj = (RelativeDateObj) scheduledItem;
				EmailRuleScheduleRelative emailRuleScheduleRelative = new EmailRuleScheduleRelative();

				if (relativeDateObj.getId() == null) {
					emailRuleScheduleRelative.setEmailRule(rule);
					emailRuleScheduleRelative.setError(false);
					emailRuleScheduleRelative.setComplete(false);
					EmailRuleScheduleStatus scheduleStatus = emailRuleScheduleStatusDao.findById(2);
					emailRuleScheduleRelative.setEmailRuleScheduleStatus(scheduleStatus);
				} else {
					// if we have an ID then update the persistent object
					emailRuleScheduleRelative = (EmailRuleScheduleRelative) emailRuleScheduleDao
							.findById(relativeDateObj.getId());
					emailRuleScheduleRelative.setError(relativeDateObj.isError());
					emailRuleScheduleRelative.setComplete(relativeDateObj.isComplete());
					EmailRuleScheduleStatus scheduleStatus = emailRuleScheduleStatusDao.findById(relativeDateObj
							.getCurrentStatus().getId());
					emailRuleScheduleRelative.setEmailRuleScheduleStatus(scheduleStatus);
				}

				emailRuleScheduleRelative.setDays(relativeDateObj.getDays());
				emailRuleScheduleRelative.setDaysAfter(relativeDateObj.getDaysAfter());
				Calendar cal = new GregorianCalendar();
				cal.setTime(sessionBean.getProject().getDueDate());
				if (!(relativeDateObj.getTime() == null)){
					Calendar time = new GregorianCalendar();
					time.setTime(relativeDateObj.getTime());
					log.debug("Relative Date Obj Time: {}", relativeDateObj.getTime());
					cal.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
					cal.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
				}else{
					log.debug("Time is NULL");
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
				}
					
				
				emailRuleScheduleRelative.setRelativeToDate(new Timestamp(cal.getTimeInMillis()));
				emailRuleScheduleRelative.setDays(relativeDateObj.getDays());
				if (!(relativeDateObj.getTimezone() == null)){
					emailRuleScheduleRelative.setTimezone(relativeDateObj.getTimezone().getID());
				}else{
					emailRuleScheduleRelative.setTimezone(TimeZone.getDefault().getID());
				}
				EmailRuleScheduleType scheduleType = emailRuleScheduleTypeDao.findById(2);
				emailRuleScheduleRelative.setEmailRuleScheduleType(scheduleType);

				log.debug(">>>>>>>>>>>>>>>>>>>>>>> Relative Date Id: {}", relativeDateObj.getId());
				emailRuleSchedules.add(emailRuleScheduleRelative);
			}
		}

		rule.setEmailRuleSchedules(emailRuleSchedules);

		// Populating Email Rule to Process
		if (rule.getActive()){ //Pointless to add rule to process entities for inactive rules.. add them when it becomes active
			for (EmailRuleSchedule emailRuleSchedule : rule.getEmailRuleSchedules()) {
				EmailRuleToProcess emailRuleToProcess = new EmailRuleToProcess();
				List<EmailRuleToProcess> processList = rule.getEmailRuleToProcess();
				if (processList != null) {
					log.debug("Found {} items in processList", processList.size());
	
					// Find out if this schedule already has a rule to process, if
					// so update persistent object
					for (EmailRuleToProcess process : processList) {
						if (process.getEmailRuleSchedule().getId() == emailRuleSchedule.getId()) {
							emailRuleToProcess = process;
							break;
						}
					}
				}
	
				// EmailRuleToProcess emailRuleToProcess = new EmailRuleToProcess();
				Date sendDate = new Date();
				emailRuleToProcess.setEmailRule(rule);
				emailRuleToProcess.setEmailRuleSchedule(emailRuleSchedule);
	
				if (emailRuleSchedule.getEmailRuleScheduleType().getCode().equals("FIXED_DATE")) {
					sendDate = ((EmailRuleScheduleFixed) emailRuleSchedule).getDate();
				} else if (emailRuleSchedule.getEmailRuleScheduleType().getCode().equals("RELATIVE_DATE")) {
					EmailRuleScheduleRelative relative = (EmailRuleScheduleRelative) emailRuleSchedule;
					sendDate = RelativeDateCalculator.calculateRelativeDate(relative.getDaysAfter(), relative.getDays(),
							relative.getRelativeToDate());
				}
				emailRuleToProcess.setSendDate(new Timestamp(sendDate.getTime()));
				emailRulesToProcess.add(emailRuleToProcess);
			}
		}

		rule.setEmailRuleToProcess(emailRulesToProcess);

		// Create Email Rule Participants
		Boolean included = false;

		if (rule.getAllParticipants() && rule.getExcludedParticipants()) {
			included = false;
		} else if (!rule.getAllParticipants() && !rule.getExcludedParticipants()) {
			included = true;
		} else if (rule.getAllParticipants() && !rule.getExcludedParticipants()) {
			ruleSetupView.setParticipants(null);
		}

		if (ruleSetupView.getParticipants() != null) {
			for (ParticipantObj participant : ruleSetupView.getParticipants()) {
				EmailRuleParticipant emailRuleParticipant = new EmailRuleParticipant();
				User user = userDao.findById(participant.getUserId());
				emailRuleParticipant.setEmailRule(rule);
				emailRuleParticipant.setUser(user);
				emailRuleParticipant.setIncluded(included);
				emailRuleParticipants.add(emailRuleParticipant);
			}
		}

		rule.setEmailRuleParticipants(emailRuleParticipants);

		return rule;
	}

	private String getScheduleType(Object obj) {

		FixedDateObj fixedDate = new FixedDateObj();
		RelativeDateObj relativeDate = new RelativeDateObj();

		if (obj.getClass().equals(fixedDate.getClass())) {
			fixedDate = (FixedDateObj) obj;
			return fixedDate.getType();
		} else if (obj.getClass().equals(relativeDate.getClass())) {
			relativeDate = (RelativeDateObj) obj;
			return relativeDate.getType();
		}

		return null;
	}

	public void handleAddParticipants(ActionEvent event) {
		List<ParticipantObj> users = projectParticipantsViewBean.getSelections();
		List<ParticipantObj> ruleUsers = emailRuleSetupViewBean.getParticipants();

		for (ParticipantObj user : users) {
			if (!ruleUsers.contains(user)) {
				ruleUsers.add(user);
			}
		}
	}

	public String handleCancelRuleSetup() {
		return "projectEmail.xhtml?faces-redirect=true";
	}

	public String handleSaveRule() {
		if (validateAndSave()) {
			return "projectEmail.xhtml?faces-redirect=true";
		}
		return null;
	}

	public void cancelEditTemplate(ActionEvent event) {
		refreshTemplatesList();
	}

	public void saveEditTemplate(ActionEvent event) {
		EmailTemplateObj template = getEmailTemplateObj(getEmailRuleSetupViewBean().getTemplateId());
		if (template.getLevel().equals(ScheduledEmailDef.GLOBAL)) {
			editEmailTemplateDetailsCompControllerBean.createTemplate(template, ScheduledEmailDef.PROJ);
			log.info(">>>>>>> Attempting to create template for {}", ScheduledEmailDef.PROJ);
		} else {
			editEmailTemplateDetailsCompControllerBean.updateTemplate(template, null);
			log.info(">>>>>>> Attempting to update template.");

		}
		refreshTemplatesList();
		refreshLanguages();
	}

	public void initEditTemplate(ActionEvent event) {
		EmailTemplateObj template = getEmailTemplateObj(getEmailRuleSetupViewBean().getTemplateId());
		editEmailTemplateDetailsCompControllerBean.initForEdit(template);
	}

	public void initCloneTemplate(ActionEvent event) {
		EmailTemplateObj template = getEmailTemplateObj(emailRuleSetupViewBean.getTemplateId());
		emailTemplatesListCompViewBean.setCloneTemplateTitle(template.getTitle());
	}

	public void saveCloneTemplate(ActionEvent event) {
		EmailTemplateObj template = getEmailTemplateObj(emailRuleSetupViewBean.getTemplateId());
		String templateTitle = emailTemplatesListCompViewBean.getCloneTemplateTitle();
		if (!validateCloneTemplate(templateTitle)) {
			emailTemplatesService.cloneTemplate(template, templateTitle, ScheduledEmailDef.PROJ,
					sessionBean.getCompany(), sessionBean.getProject());
			refreshTemplatesList();

		}
	}

	private boolean validateCloneTemplate(String templateName) {
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rc = RequestContext.getCurrentInstance();
		boolean error = false;

		if (templateName.trim().isEmpty()) {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Name required.", ""));
			log.error("Template Name required.");
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		return error;
	}

	private void refreshLanguages() {
		emailRuleSetupViewBean.setLanguages(getTemplateLanguages());
		emailRuleSetupViewBean.setLanguageCode(null);
	}

	private void refreshTemplatesList() {
		List<EmailTemplateObj> templates = findProjectEmailTemplates();
		emailRuleSetupViewBean.setTemplates(templates);
		setTemplatesList(templates);
	}

	/* SETTERS AND GETTERS */

	public ProjectEmailViewBean getProjectEmailViewBean() {
		return projectEmailViewBean;
	}

	public void setProjectEmailViewBean(ProjectEmailViewBean projectEmailViewBean) {
		this.projectEmailViewBean = projectEmailViewBean;
	}

	public EmailRuleSetupViewBean getEmailRuleSetupViewBean() {
		return emailRuleSetupViewBean;
	}

	public void setEmailRuleSetupViewBean(EmailRuleSetupViewBean emailRuleSetupViewBean) {
		this.emailRuleSetupViewBean = emailRuleSetupViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public List<EmailTemplateObj> getTemplatesList() {
		return templatesList;
	}

	public void setTemplatesList(List<EmailTemplateObj> templatesList) {
		this.templatesList = templatesList;
	}

	public ProjectParticipantsControllerBean getProjectParticipantsControllerBean() {
		return projectParticipantsControllerBean;
	}

	public void setProjectParticipantsControllerBean(ProjectParticipantsControllerBean projectParticipantsControllerBean) {
		this.projectParticipantsControllerBean = projectParticipantsControllerBean;
	}

	public ProjectParticipantsViewBean getProjectParticipantsViewBean() {
		return projectParticipantsViewBean;
	}

	public void setProjectParticipantsViewBean(ProjectParticipantsViewBean projectParticipantsViewBean) {
		this.projectParticipantsViewBean = projectParticipantsViewBean;
	}

	public EmailRule getCurrentEmailRule() {
		return currentEmailRule;
	}

	public void setCurrentEmailRule(EmailRule currentEmailRule) {
		this.currentEmailRule = currentEmailRule;
	}

	public EmailRuleScheduleSetupControllerBean getEmailRuleScheduleSetupControllerBean() {
		return emailRuleScheduleSetupControllerBean;
	}

	public void setEmailRuleScheduleSetupControllerBean(
			EmailRuleScheduleSetupControllerBean emailRuleScheduleSetupControllerBean) {
		this.emailRuleScheduleSetupControllerBean = emailRuleScheduleSetupControllerBean;
	}

	public EditEmailTemplateDetailsCompControllerBean getEditEmailTemplateDetailsCompControllerBean() {
		return editEmailTemplateDetailsCompControllerBean;
	}

	public void setEditEmailTemplateDetailsCompControllerBean(
			EditEmailTemplateDetailsCompControllerBean editEmailTemplateDetailsCompControllerBean) {
		this.editEmailTemplateDetailsCompControllerBean = editEmailTemplateDetailsCompControllerBean;
	}

	public EmailTemplatesListCompViewBean getEmailTemplatesListCompViewBean() {
		return emailTemplatesListCompViewBean;
	}

	public void setEmailTemplatesListCompViewBean(EmailTemplatesListCompViewBean emailTemplatesListCompViewBean) {
		this.emailTemplatesListCompViewBean = emailTemplatesListCompViewBean;
	}

}
