package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.presentation.helper.TeammemberHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.AddTeammemberViewBean;

@ManagedBean
@ViewScoped
public class AddTeammemberControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AddTeammemberControllerBean.class);

	@ManagedProperty(name = "addTeammemberViewBean", value = "#{addTeammemberViewBean}")
	private AddTeammemberViewBean addTeammemberViewBean;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "teammemberHelper", value = "#{teammemberHelper}")
	private TeammemberHelper teammemberHelper;

	private boolean fistTime = true;

	public void initAvailableTeammembers(String entityTypeCode, Integer entityId, String realmPropertyName,
			String filter, boolean populateTeammembers) {

		teammemberHelper.init(entityTypeCode, entityId, realmPropertyName, EntityRule.ALLOW);

		if (teammemberHelper.getRealm() != null) {
			addTeammemberViewBean.setRoles(teammemberHelper.getRoles(entityTypeCode));
			if (populateTeammembers) {
				addTeammemberViewBean.setTeammembers(teammemberHelper.getAllSubjects(filter));
			}
		} else {
			log.debug("REALM IS NULL !!!");
		}

		resetDefaults(!populateTeammembers);
	}

	private void resetDefaults(boolean clearTeammembers) {
		if (clearTeammembers) {
			addTeammemberViewBean.setTeammembers(new ArrayList<ExternalSubjectDataObj>());
		}

		addTeammemberViewBean.setRoleId(-1);
		addTeammemberViewBean.setSelectedRole(null);
		addTeammemberViewBean.setFilteredTeammembers(null);
		addTeammemberViewBean.setSelectedTeammembers(new ArrayList<ExternalSubjectDataObj>());

	}

	public boolean validateSelections() {
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rc = RequestContext.getCurrentInstance();
		boolean valid = true;

		if (addTeammemberViewBean.getRoleId() == -1) {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Role is required.", ""));
			rc.addCallbackParam("notValid", true);
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> role is not vaild");
			valid = false;
		}

		if (addTeammemberViewBean.getSelectedTeammembers().size() == 0) {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"At least one participant need to be selected.", ""));
			rc.addCallbackParam("notValid", true);
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> teammember is not vaild");
			valid = false;
		}

		return valid;
	}

	public void handleRoleSelect(AjaxBehaviorEvent event) {
		log.debug(">>>>>>>>>>>>>>> handleRoleSelect");
		addTeammemberViewBean.setSelectedRole(getSelectedRole());
	}

	private Role getSelectedRole() {
		if (addTeammemberViewBean.getRoleId() != -1) {
			return teammemberHelper.findRoleById(addTeammemberViewBean.getRoleId());
		} else {
			return null;
		}
	}

	public void handleRoleSelectWithFilter(AjaxBehaviorEvent event) {
		log.debug(">>>>>>>>>>>>>>> handleRoleSelectWithFilter");
		Role role = getSelectedRole();
		if (role != null) {
			List<ExternalSubjectDataObj> teammembers = teammemberHelper.getSubjectsWithRole(role);
			if (teammembers != null) {
				addTeammemberViewBean.setTeammembers(teammembers);
			}
			addTeammemberViewBean.setSelectedRole(role);
		}
	}

	public void handleRowSelect(SelectEvent event) {

	}

	public void handleRowUnselect(UnselectEvent event) {

	}

	public AddTeammemberViewBean getAddTeammemberViewBean() {
		return addTeammemberViewBean;
	}

	public void setAddTeammemberViewBean(AddTeammemberViewBean addTeammemberViewBean) {
		this.addTeammemberViewBean = addTeammemberViewBean;
	}

	public boolean isFistTime() {
		return fistTime;
	}

	public void setFistTime(boolean fistTime) {
		this.fistTime = fistTime;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public TeammemberHelper getTeammemberHelper() {
		return teammemberHelper;
	}

	public void setTeammemberHelper(TeammemberHelper teammemberHelper) {
		this.teammemberHelper = teammemberHelper;
	}

}
