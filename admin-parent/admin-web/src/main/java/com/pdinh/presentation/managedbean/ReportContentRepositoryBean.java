package com.pdinh.presentation.managedbean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

@ManagedBean(name = "reportContentRepositoryBean")
@ApplicationScoped
public class ReportContentRepositoryBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Resource(name = "application/config_properties")
	private Properties appConfigProperties;

	/**
	 * Find manifests that derive from the passed in manifestId
	 */
	public List<String> findDerivedManifestIds(String manifestId) {
		List<String> manifestIds = new ArrayList<String>();

		String repoManifestDir = appConfigProperties.getProperty("reportContentRepository") + "/manifest";
		// TODO: Needs to be a better way to determine relationships, might have
		// to put child ID refs into base xml file
		System.out.println("Find derived manifests in report content repository: " + repoManifestDir);
		List<File> manifestFiles = getAllXmlFilesInDirectory(new File(repoManifestDir));

		for (File file : manifestFiles) {
			Document manifestDoc = loadXml(file);
			Element root = (Element) manifestDoc.getFirstChild();
			if (root.getAttribute("base").equals(manifestId)) {
				manifestIds.add(root.getAttribute("id"));
			}
		}

		return manifestIds;
	}

	public Properties getAppConfigProperties() {
		return appConfigProperties;
	}

	public void setAppConfigProperties(Properties appConfigProperties) {
		this.appConfigProperties = appConfigProperties;
	}

	private List<File> getAllXmlFilesInDirectory(File dir) {
		return (List<File>) FileUtils.listFiles(dir, new String[] { "xml" }, true);
	}

	private Document loadXml(File file) {
		Document doc = null;
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		try {
			builder = domFactory.newDocumentBuilder();
			doc = builder.parse(file);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}

}
