package com.pdinh.presentation.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.util.ComponentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.presentation.managedbean.SessionBean;

public abstract class ControllerBeanBase {

	protected static final Logger log = LoggerFactory.getLogger(ControllerBeanBase.class);

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	protected SessionBean sessionBean;

	@ManagedProperty("#{msg}")
	ResourceBundle msg;

	protected boolean firstTime;
	protected String formName;

	public ControllerBeanBase() {
		firstTime = true;
		formName = "";
	}

	public ControllerBeanBase(String formName) {
		firstTime = true;
		this.formName = formName;
	}

	public void initView(ComponentSystemEvent event) {
		if (isFirstTime()) {
			setFirstTime(false);
		}
	}

	public void refresh() {
		firstTime = true;
		initView(null);
	}

	public String getMsg(String key) {
		return getMsg().getString(key);
	}

	public void clearRenderIds() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getPartialViewContext().getRenderIds().clear();
	}

	public void addRenderId(String renderId) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getPartialViewContext().getRenderIds().add(renderId);
	}

	public String findClientIds(ActionEvent event, String list) {
		return ComponentUtils.findClientIds(FacesContext.getCurrentInstance(), event.getComponent(), list);
	}

	public UIComponent findComponent(String componentId) {
		FacesContext context = FacesContext.getCurrentInstance();
		UIViewRoot root = context.getViewRoot();

		return ComponentUtils.findComponent(root, componentId);
	}

	public void resetFormComponents() {
		resetComponents(getFormName());
	}

	public void resetComponents(String parentComponentId) {
		if (parentComponentId != null && parentComponentId.length() > 0) {
			UIComponent parentComponent = findComponent(parentComponentId);
			if (parentComponent != null) {
				resetComponents(parentComponent);
			}
		}
	}

	private void resetComponents(UIComponent pComponent) {
		if (pComponent.isRendered()) {
			if (pComponent instanceof EditableValueHolder) {
				EditableValueHolder editableValueHolder = (EditableValueHolder) pComponent;
				editableValueHolder.setSubmittedValue(null);
				editableValueHolder.setValue(null);
				editableValueHolder.setLocalValueSet(false);
				editableValueHolder.setValid(true);
			}

			for (Iterator<UIComponent> iterator = pComponent.getFacetsAndChildren(); iterator.hasNext();) {
				resetComponents(iterator.next());
			}
		}
	}

	public void replacePage(String url) {
		RequestContext.getCurrentInstance().execute("window.location.replace('" + url + "')");
	}

	public void redirect(String url) {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRequestParameter(String param) {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> map = context.getExternalContext().getRequestParameterMap();
		return map.get(param);
	}

	public Locale getLocale() {
		return FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}

	public void closeLightBox(String lightBoxId) {
		RequestContext.getCurrentInstance().execute("parent." + lightBoxId + ".hide();");
	}

	public ResourceBundle getMsg() {
		return msg;
	}

	public void setMsg(ResourceBundle msg) {
		this.msg = msg;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean isFirstTime() {
		return firstTime;
	}

	public void setFirstTime(boolean firstTime) {
		this.firstTime = firstTime;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getUsername() {
		if (sessionBean.getRoleContext() != null && sessionBean.getRoleContext().getSubject() != null) {
			return sessionBean.getRoleContext().getSubject().getPrincipal();
		}
		return "";
	}

	public String getCompanyName() {
		if (sessionBean.getCompany() != null) {
			return sessionBean.getCompany().getCoName();
		}
		return "";
	}
}
