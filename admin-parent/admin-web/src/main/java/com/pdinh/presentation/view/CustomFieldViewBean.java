package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.StringUtils;

import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.CustomFieldListValueDto;
import com.kf.uffda.dto.ListItemDto;
import com.kf.uffda.view.FieldViewBean;
import com.pdinh.presentation.managedbean.LanguageBean;

@ManagedBean
@ViewScoped
public class CustomFieldViewBean extends FieldViewBean<CustomFieldDto> implements Serializable {

	private static final long serialVersionUID = 4672859319846320179L;

	private final List<ListItemDto> languages;
	private String languageCode;

	private CustomFieldListValueDto selectedChoice;

	private boolean disableMoveChoiceTop;
	private boolean disableMoveChoiceUp;
	private boolean disableMoveChoiceDown;
	private boolean disableMoveChoiceBottom;

	private boolean showChangeLog;

	public CustomFieldViewBean() {
		init();
		languages = new ArrayList<ListItemDto>();
	}

	@Override
	public void init() {
		super.init();
		setField(new CustomFieldDto());
		showChangeLog = false;
		languageCode = "en";
		selectedChoice = new CustomFieldListValueDto();

		disableMoveChoiceTop = true;
		disableMoveChoiceUp = true;
		disableMoveChoiceDown = true;
		disableMoveChoiceBottom = true;
	}

	public boolean isShowChangeLog() {
		return showChangeLog;
	}

	public void setShowChangeLog(boolean showChangeLog) {
		this.showChangeLog = showChangeLog;
	}

	public List<ListItemDto> getLanguages() {
		return languages;
	}

	public void addLanguages(List<ListItemDto> newLanguages) {
		if (newLanguages != null) {
			languages.clear();
			languages.addAll(newLanguages);
		}
	}

	public void addLanguage(ListItemDto language) {
		if (language != null) {
			languages.add(language);
		}
	}

	public void clearLanguages() {
		languages.clear();
	}

	public boolean hasLanguages() {
		if (languages.size() > 0) {
			return true;
		}
		return false;
	}

	public ListItemDto findLanguageByCode(String code) {
		for (ListItemDto language : languages) {
			if (language.getCode().equalsIgnoreCase(code)) {
				return language;
			}
		}
		return null;
	}

	public int getNextLanguageId() {
		return -languages.size() - 1;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public boolean isLanguageCodeEnglish() {
		if (StringUtils.isBlank(languageCode)) {
			return false;
		}
		return languageCode.equalsIgnoreCase(LanguageBean.ENGLISH_LANGUAGE_CODE);
	}

	public CustomFieldListValueDto getSelectedChoice() {
		return selectedChoice;
	}

	public void setSelectedChoice(CustomFieldListValueDto selectedChoice) {
		this.selectedChoice = selectedChoice;
	}

	public boolean isDisableMoveChoiceTop() {
		return disableMoveChoiceTop;
	}

	public void setDisableMoveChoiceTop(boolean disableMoveChoiceTop) {
		this.disableMoveChoiceTop = disableMoveChoiceTop;
	}

	public boolean isDisableMoveChoiceUp() {
		return disableMoveChoiceUp;
	}

	public void setDisableMoveChoiceUp(boolean disableMoveChoiceUp) {
		this.disableMoveChoiceUp = disableMoveChoiceUp;
	}

	public boolean isDisableMoveChoiceDown() {
		return disableMoveChoiceDown;
	}

	public void setDisableMoveChoiceDown(boolean disableMoveChoiceDown) {
		this.disableMoveChoiceDown = disableMoveChoiceDown;
	}

	public boolean isDisableMoveChoiceBottom() {
		return disableMoveChoiceBottom;
	}

	public void setDisableMoveChoiceBottom(boolean disableMoveChoiceBottom) {
		this.disableMoveChoiceBottom = disableMoveChoiceBottom;
	}
}
