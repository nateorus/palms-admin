package com.pdinh.presentation.helper;

import java.util.Date;
import java.util.Map;

public class UrlGenerator {

	public static String getURL(String protocol, String server, int port, String baseContextPath, String page,
			Map<String, String> parameters) {
		StringBuilder builder = new StringBuilder(protocol);
		builder.append("://");
		builder.append(server);

		if (port > 0) {
			builder.append(":");
			builder.append(port);
		}

		builder.append("/");
		builder.append(baseContextPath);
		builder.append("/");
		builder.append(page);

		if (!parameters.isEmpty()) {
			builder.append("?");
			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				builder.append(entry.getKey());
				builder.append("=");
				builder.append(entry.getValue());
				builder.append("&");
			}
			builder.deleteCharAt(builder.lastIndexOf("&"));
		}

		return builder.toString();
	}

	/**
	 * 
	 * @param windowName
	 * @param url
	 * @param newWindow
	 *            If <strong>false</strong> the resulting JavaScript String will
	 *            merely repaint an open popup window with a handle of 'popup',
	 *            if one already exists (e.g., if the same caller has previously
	 *            called this method with the <code>newWindow</code> parameter
	 *            set to <strong>false</strong>). Otherwise, the resulting
	 *            JavaScript will pop up a new browser window which cannot be
	 *            referenced in future calls.
	 * @param useChromeWorkaround
	 * @return A String which is a valid JavaScript String to open/repaint a
	 *         popup in a separate browser window.
	 */
	public static String getPopupJS(String windowName, String url, boolean newWindow, boolean useChromeWorkaround) {
		final int width = 980;
		final int height = 550;
		String popupName = "popup";

		if (newWindow) {
			popupName = "popupWin" + String.valueOf(new Date().getTime());
		}

		StringBuilder builder = new StringBuilder();
		builder.append(getBrowserDefinitions());
		builder.append(getWidthHeightVars(width, height));

		if (!newWindow && useChromeWorkaround) {
			builder.append(getChromeWorkaround());
		}

		if (newWindow) {
			builder.append(getPopup(popupName, url, "", width, height));
			builder.append(getReposition(popupName));
			builder.append("}");
		} else {
			builder.append(getPopup(popupName, url, windowName, width, height));

			// We're using 'useChromeWorkaround' as a proxy for 'this request is
			// coming from the Portal app'.
			if (useChromeWorkaround) {
				builder.append(getReposition(popupName));
			}

			builder.append("}");
			builder.append(getRepaint(popupName, url));
		}

		builder.append("if (typeof ");
		builder.append(popupName);
		builder.append(" != 'undefined') { ");
		builder.append(popupName);
		builder.append(".focus(); }");

		System.out.println("Javascript is: " + builder.toString());

		return builder.toString();
	}

	private static String getBrowserDefinitions() {
		return "var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; "
				+ "var isFF = typeof InstallTrigger !== 'undefined'; " //
				+ "var isChrome = !!window.chrome && !isOpera; ";
	}

	private static String getWidthHeightVars(int width, int height) {
		return "var leftPos = 0; var topPos = 0; var height = " + height + "; var width = " + width + "; ";
	}

	// Workaround for a Chrome bug. If the target window is already open but
	// hidden, calling 'popup.focus' still doesn't give it focus. So we
	// close the window and reopen it with new content. Obviously this is a
	// bad idea if you want to retain any of the data currently in the
	// target window.
	//
	// Additionally, as of 05/23/2014, this workaround causes a problem in
	// LRM Internal which is worse than the bug we're trying to work around.
	// This is due to a conflict in protocols between LRM Internal (http:)
	// and LRM External (https:). So we don't apply it when the call is
	// coming from LRM Internal.
	//
	// Note that this method should only be called, and is only applicable, if
	// the user is trying to repaint an already-open popup window with a handle
	// of 'popup'.
	private static String getChromeWorkaround() {
		StringBuilder builder = new StringBuilder("if (isChrome && typeof(popup) == 'object') { ");
		builder.append("leftPos = popup.screenLeft; topPos = popup.screenTop; ");
		builder.append("console.log('Chrome browser detected, attempting to close and delete existing popup window.'); ");
		builder.append("popup.close(); delete popup;} ");
		return builder.toString();
	}

	private static String getPopup(String popupName, String url, String windowName, int width, int height) {
		StringBuilder builder = new StringBuilder();
		builder.append("if (typeof(");
		builder.append(popupName);
		builder.append(") != 'object' || ");
		builder.append(popupName);
		builder.append(".closed) {");
		builder.append("  console.log('About to open new popup window with handle [");
		builder.append(popupName);
		builder.append("].');  ");
		builder.append(popupName);
		builder.append(" = window.open('");
		builder.append(url);
		builder.append("', '");
		builder.append(windowName);
		builder.append("', 'menubar=0,resizable=1,location=0,status=1,toolbar=0,scrollbars=1,width=");
		builder.append(width);
		builder.append(",height=");
		builder.append(height);
		builder.append("');");

		return builder.toString();
	}

	private static String getReposition(String popupName) {
		// The leftBoundary/leftPos nonsense makes sure the target window is
		// centered on the screen, even when the user has dual monitors.
		StringBuilder builder = new StringBuilder("var leftBoundary;");
		builder.append("if (window.screenLeft) {");
		builder.append("  leftBoundary = window.screenLeft;");
		builder.append("} else {");
		builder.append("  leftBoundary = window.screenX;");
		builder.append("}");
		builder.append("console.log('leftPos is: ' + leftPos + ', topPos is: ' + topPos);");
		builder.append("if (leftPos <= 0) { ");
		builder.append("  leftPos = (window.screen.width/2) - (width/2); ");
		builder.append("  if (leftBoundary > window.screen.width) {");
		builder.append("    leftPos += window.screen.width;");
		builder.append("  }");
		builder.append("}");
		builder.append("if (topPos <= 0) { topPos = (window.screen.height/2) - (height/2); }");
		builder.append("var historyLength = ");
		builder.append(popupName);
		builder.append(".history.length;");

		// History in Chrome is 0-based; in IE and FF it is 1-based.
		builder.append("if (isChrome) { historyLength++; }");
		builder.append("console.log('History length is: ' + historyLength);");
		builder.append("if (historyLength == 1) { ");
		builder.append("console.log('Moving popup to ' + leftPos + ', ' + topPos);");
		builder.append(popupName);
		builder.append(".moveTo(leftPos, topPos);");
		builder.append("}");

		return builder.toString();
	}

	private static String getRepaint(String popupName, String url) {
		// For browsers OTHER THAN Chrome, this is all we need to do when the
		// target window is already open.
		StringBuilder builder = new StringBuilder(" else {");
		builder.append("console.log('Popup window is already open, repainting with new content.');  ");
		builder.append(popupName);
		builder.append(".location.href = '");
		builder.append(url);
		builder.append("';");
		builder.append("}");

		return builder.toString();
	}
}
