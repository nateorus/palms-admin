package com.pdinh.presentation.listener;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.PalmsSessionDao;
import com.pdinh.auth.listener.PDINHSessionListener;
import com.pdinh.auth.persistence.entity.PalmsSession;

/**
 * Application Lifecycle Listener implementation class cleanupSessionOnDestroy
 *
 */
@WebListener
public class CleanupSessionOnDestroy implements HttpSessionListener {
	
	private String appName;
	private String moduleName;
	private static final Logger log = LoggerFactory.getLogger(CleanupSessionOnDestroy.class);
    
	private String sessionDaoContext;
	
	private PalmsSessionDao palmsSessionDao;

	 public void initializeEjb(){
			try {
				
				String appName = (String) new InitialContext().lookup("java:app/AppName");
				this.setAppName(appName);
				String moduleName = (String) new InitialContext().lookup("java:module/ModuleName");
				this.setModuleName(moduleName);
				
				Context ctx = (Context)new InitialContext().lookup("java:global/" + appName);
				getEJBContexts(ctx, "");
				
				palmsSessionDao = (PalmsSessionDao) new InitialContext().lookup("java:app/" + getSessionDaoContext());
				
			} catch (NamingException e) {
				log.error("Error looking up Ejb reference; {}", e.getMessage());
				e.printStackTrace();
			}
		}
	    public void getEJBContexts(Context ctx, String indent) {
			try {   
				NamingEnumeration list = ctx.listBindings("");
				while (list.hasMore()) {
					Binding item = (Binding) list.next();
					
					String name = item.getName();
					log.trace("{}{} ", new Object[]{indent, name});  
					
					if (name.endsWith("PalmsSessionDaoImpl")){
						setSessionDaoContext(indent + name);
						break;
					}
					
					Object o = item.getObject();
					if (o instanceof javax.naming.Context) {
						getEJBContexts((Context) o, name + "/");
					}   
				}
				
				} catch (NamingException ex) {
					log.debug("JNDI failure While initializing EJB references: {}", ex);
				}
			
		}
    /**
     * Default constructor. 
     */
    public CleanupSessionOnDestroy() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent arg0) {
        // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent arg0) {
       // HttpSession session = arg0.getSession();
        if (palmsSessionDao == null){
        	this.initializeEjb();
        }
        PalmsSession psession = palmsSessionDao.findSessionById(arg0.getSession().getId());
        if (!(psession == null)){
        	palmsSessionDao.delete(psession);
        }
    }
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getSessionDaoContext() {
		return sessionDaoContext;
	}
	public void setSessionDaoContext(String sessionDaoContext) {
		this.sessionDaoContext = sessionDaoContext;
	}
	
}
