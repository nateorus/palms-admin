package com.pdinh.presentation.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.presentation.domain.EmailRuleScheduleObj;
import com.pdinh.presentation.domain.EmailRuleScheduleStatusObj;
import com.pdinh.presentation.view.EmailRuleScheduleStatusViewBean;


@ManagedBean
@ViewScoped
public class EmailRuleScheduleStatusControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(EmailRuleScheduleStatusControllerBean.class);

	@ManagedProperty(name = "emailRuleScheduleStatusViewBean", value = "#{emailRuleScheduleStatusViewBean}")
	private EmailRuleScheduleStatusViewBean emailRuleScheduleStatusViewBean;	
	
	public String styleSatus(EmailRuleScheduleStatusObj status) {
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> styleSatus:  Setting currentSchedule to {}", emailRuleScheduleStatusViewBean.getCurrentSchedule());
		EmailRuleScheduleObj currentScheduleObj = emailRuleScheduleStatusViewBean.getCurrentSchedule();
		String styleClassName = null;
		
		if(currentScheduleObj != null) {
			if(currentScheduleObj.getCurrentStatus().getId() == status.getId()) {
				styleClassName = "schedule-status-current";
			} else if (currentScheduleObj.getCurrentStatus().getId() > status.getId()) {
				styleClassName = "schedule-status-complete";
			}

			if(currentScheduleObj.isError() && 
					(currentScheduleObj.getCurrentStatus().getId() == status.getId())) {
				styleClassName = "schedule-status-error";
			}
			log.debug(">>>>>>>>>>>>>>>>>> currentScheduleObj = {} <<<<<<<<<<<<<<<<<<<<<<<<<<<<<", 
					currentScheduleObj.getCurrentStatus().getName());
		} else {
			log.debug(">>>>>>>>>>>>>>>>>> currentScheduleObj is NULL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		}
		
		return styleClassName;
	}

	public EmailRuleScheduleStatusViewBean getEmailRuleScheduleStatusViewBean() {
		return emailRuleScheduleStatusViewBean;
	}

	public void setEmailRuleScheduleStatusViewBean(
			EmailRuleScheduleStatusViewBean emailRuleScheduleStatusViewBean) {
		this.emailRuleScheduleStatusViewBean = emailRuleScheduleStatusViewBean;
	}

}
