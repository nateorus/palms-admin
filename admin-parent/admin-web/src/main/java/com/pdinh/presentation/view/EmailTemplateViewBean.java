package com.pdinh.presentation.view;


import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;



@ManagedBean
@ViewScoped
public class EmailTemplateViewBean implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<EmailTemplateObj> emailTemplates;
	private List<EmailTextObj> emailTexts;
	private List<ListItemObj> currentEmailLangs;
	private List<ListItemObj> newLangs;
	private List<String> copyPasteKeywords;
	
	private EmailTemplateObj currentTemplate;
	private EmailTextObj currentEmailTextObj;
	private String currentLanguage;
	private String newlanguage;
	private String emailBody;
	private String emailSubject;
	private String editEmailBody;
	private String editEmailSubject;
	
	private String newSubject;
	private String newText;
	private String newLangCode;
	
	private String templateTitle;
	private String templateType;
	private String templateConst;
	private Integer templateIsTemplate;
	
	private String cloneTemplateTitle;
	
	private String newTemplateTitle;
	private String newTemplateType;
	private String newTemplateConst;
	private Integer newTemplateIsTemplate;
	private EmailTemplateObj selectedEmailTemplate;
	
	
	public String getTemplateTitle() {
		return templateTitle;
	}

	public void setTemplateTitle(String templateTitle) {
		this.templateTitle = templateTitle;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getTemplateConst() {
		return templateConst;
	}

	public void setTemplateConst(String templateConst) {
		this.templateConst = templateConst;
	}

	public Integer getTemplateIsTemplate() {
		return templateIsTemplate;
	}

	public void setTemplateIsTemplate(Integer templateIsTemplate) {
		this.templateIsTemplate = templateIsTemplate;
	}

	public void setEmailTemplates(List<EmailTemplateObj> emailTemplates) {
		this.emailTemplates = emailTemplates;
	}

	public List<EmailTemplateObj> getEmailTemplates() {
		return emailTemplates;
	}

	public EmailTemplateObj getCurrentTemplate() {
		return currentTemplate;
	}

	public void setCurrentTemplate(EmailTemplateObj currentTemplate) {
		this.currentTemplate = currentTemplate;
	}

	public List<EmailTextObj> getEmailTexts() {
		return emailTexts;
	}

	public void setEmailTexts(List<EmailTextObj> emailTexts) {
		this.emailTexts = emailTexts;
	}

	public List<ListItemObj> getCurrentEmailLangs() {
		return currentEmailLangs;
	}

	public void setCurrentEmailLangs(List<ListItemObj> currentEmailLangs) {
		this.currentEmailLangs = currentEmailLangs;
	}

	public String getCurrentLanguage() {
		return currentLanguage;
	}

	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public EmailTextObj getCurrentEmailTextObj() {
		return currentEmailTextObj;
	}

	public void setCurrentEmailTextObj(EmailTextObj currentEmailTextObj) {
		this.currentEmailTextObj = currentEmailTextObj;
	}

	public void setNewlanguage(String newlanguage) {
		this.newlanguage = newlanguage;
	}

	public String getNewlanguage() {
		return newlanguage;
	}

	public void setNewSubject(String newSubject) {
		this.newSubject = newSubject;
	}

	public String getNewSubject() {
		return newSubject;
	}

	public void setNewText(String newText) {
		this.newText = newText;
	}

	public String getNewText() {
		return newText;
	}

	public void setNewLangCode(String newLangCode) {
		this.newLangCode = newLangCode;
	}

	public String getNewLangCode() {
		return newLangCode;
	}

	public void setCloneTemplateTitle(String cloneTemplateTitle) {
		this.cloneTemplateTitle = cloneTemplateTitle;
	}

	public String getCloneTemplateTitle() {
		return cloneTemplateTitle;
	}

	public void setNewTemplateTitle(String newTemplateTitle) {
		this.newTemplateTitle = newTemplateTitle;
	}

	public String getNewTemplateTitle() {
		return newTemplateTitle;
	}

	public void setNewTemplateType(String newTemplateType) {
		this.newTemplateType = newTemplateType;
	}

	public String getNewTemplateType() {
		return newTemplateType;
	}

	public void setNewTemplateConst(String newTemplateConst) {
		this.newTemplateConst = newTemplateConst;
	}

	public String getNewTemplateConst() {
		return newTemplateConst;
	}

	public void setNewTemplateIsTemplate(Integer newTemplateIsTemplate) {
		this.newTemplateIsTemplate = newTemplateIsTemplate;
	}

	public Integer getNewTemplateIsTemplate() {
		return newTemplateIsTemplate;
	}

	public void setNewLangs(List<ListItemObj> newLangs) {
		this.newLangs = newLangs;
	}

	public List<ListItemObj> getNewLangs() {
		return newLangs;
	}

	public void setCopyPasteKeywords(List<String> copyPasteKeywords) {
		this.copyPasteKeywords = copyPasteKeywords;
	}

	public List<String> getCopyPasteKeywords() {
		return copyPasteKeywords;
	}

	public String getEditEmailBody() {
		return editEmailBody;
	}

	public void setEditEmailBody(String editEmailBody) {
		this.editEmailBody = editEmailBody;
	}

	public String getEditEmailSubject() {
		return editEmailSubject;
	}

	public void setEditEmailSubject(String editEmailSubject) {
		this.editEmailSubject = editEmailSubject;
	}

	public EmailTemplateObj getSelectedEmailTemplate() {
		return selectedEmailTemplate;
	}

	public void setSelectedEmailTemplate(EmailTemplateObj selectedEmailTemplate) {
		this.selectedEmailTemplate = selectedEmailTemplate;
	}
	
//	public String getSendToAddress() {
//		return sendToAddress;
//	}
//
//	public void setSendToAddress(String sendToAddress) {
//		this.sendToAddress = sendToAddress;
//	}
//
//	public String getSentFromAddress() {
//		return sentFromAddress;
//	}
//
//	public void setSentFromAddress(String sentFromAddress) {
//		this.sentFromAddress = sentFromAddress;
//	}
}
