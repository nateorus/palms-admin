package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailRuleScheduleTypeObj;

@ManagedBean
@ViewScoped
public class EmailRuleScheduleSetupViewBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer scheduleType;
	
	private Date fixedDate;
	
	private int relativeDays;
	private Boolean daysAfter;
	private Date projectDueDate;
	private Date relativeDate;
	
	private int recurrenceRange;
	private Date recurrenceStartDate;
	private Date recurrenceEndDate;

	private Boolean fixedDateVisible;
	private Boolean relativeDateVisible;
	private Boolean recurringDateVisible;
	private Boolean scheduleSettingVisible;
	
	private Boolean scheduleTypeDisabled;
	private Object scheduleItemForEdit;
	
	List<EmailRuleScheduleTypeObj> scheduleTypeMenuItems;
	List<String> availableTimeZones;
	private String selectedTimeZone;
	private TimeZone timeZone = TimeZone.getDefault();
	private Date relativeTime;
	private String estimatedDateString;
	
	public void ClearAllFields() {
		this.setScheduleType(0);
		this.setFixedDate(null);
		this.setRelativeDays(0);
		this.setDaysAfter(false);
		this.setProjectDueDate(null); 
		this.setRecurrenceRange(0);
		this.setRecurrenceStartDate(null);
		this.setRecurrenceEndDate(null);
		this.setSelectedTimeZone(null);
	}
	
	public void showFixedSection() {
		this.hideAllSections();
		this.setScheduleType(1);
		this.setScheduleSettingVisible(true);
		this.setFixedDateVisible(true);

	}

	public void showRelativeSection() {
		this.hideAllSections();
		this.setScheduleType(2);
		this.setScheduleSettingVisible(true);
		this.setRelativeDateVisible(true);
	}

	public void showRecurrenceSection() {
		this.hideAllSections();
		this.setScheduleType(3);
		this.setScheduleSettingVisible(true);
		this.setRelativeDateVisible(true);
	}
	
	public void hideAllSections() {
		this.setScheduleType(0);
		this.setFixedDateVisible(false);
		this.setRelativeDateVisible(false);
		this.setRecurringDateVisible(false);
		this.setScheduleSettingVisible(false);
	}
	
	public Integer getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}
	public Date getFixedDate() {
		return fixedDate;
	}
	public void setFixedDate(Date fixedDate) {
		this.fixedDate = fixedDate;
	}
	public int getRelativeDays() {
		return relativeDays;
	}
	public void setRelativeDays(int relativeDays) {
		this.relativeDays = relativeDays;
	}
	public Date getProjectDueDate() {
		return projectDueDate;
	}
	public void setProjectDueDate(Date projectDueDate) {
		this.projectDueDate = projectDueDate;
	}

	public int getRecurrenceRange() {
		return recurrenceRange;
	}
	public void setRecurrenceRange(int recurrenceRange) {
		this.recurrenceRange = recurrenceRange;
	}	
	
	public Date getRecurrenceStartDate() {
		return recurrenceStartDate;
	}
	public void setRecurrenceStartDate(Date recurrenceStartDate) {
		this.recurrenceStartDate = recurrenceStartDate;
	}
	public Date getRecurrenceEndDate() {
		return recurrenceEndDate;
	}
	public void setRecurrenceEndDate(Date recurrenceEndDate) {
		this.recurrenceEndDate = recurrenceEndDate;
	}
	public Boolean getFixedDateVisible() {
		return fixedDateVisible;
	}
	public void setFixedDateVisible(Boolean fixedDateVisible) {
		this.fixedDateVisible = fixedDateVisible;
	}
	public Boolean getRelativeDateVisible() {
		return relativeDateVisible;
	}
	public void setRelativeDateVisible(Boolean relativeDateVisible) {
		this.relativeDateVisible = relativeDateVisible;
	}
	public Boolean getRecurringDateVisible() {
		return recurringDateVisible;
	}
	public void setRecurringDateVisible(Boolean recurringDateVisible) {
		this.recurringDateVisible = recurringDateVisible;
	}
	public Boolean getScheduleTypeDisabled() {
		return scheduleTypeDisabled;
	}
	public void setScheduleTypeDisabled(Boolean scheduleTypeDisabled) {
		this.scheduleTypeDisabled = scheduleTypeDisabled;
	}
	public Boolean getScheduleSettingVisible() {
		return scheduleSettingVisible;
	}
	public void setScheduleSettingVisible(Boolean scheduleSettingVisible) {
		this.scheduleSettingVisible = scheduleSettingVisible;
	}
	public Object getScheduleItemForEdit() {
		return scheduleItemForEdit;
	}
	public void setScheduleItemForEdit(Object scheduleItemForEdit) {
		this.scheduleItemForEdit = scheduleItemForEdit;
	}

	public Boolean getDaysAfter() {
		return daysAfter;
	}

	public void setDaysAfter(Boolean daysAfter) {
		this.daysAfter = daysAfter;
	}

	public List<EmailRuleScheduleTypeObj> getScheduleTypeMenuItems() {
		return scheduleTypeMenuItems;
	}

	public void setScheduleTypeMenuItems(
			List<EmailRuleScheduleTypeObj> scheduleTypeMenuItems) {
		this.scheduleTypeMenuItems = scheduleTypeMenuItems;
	}

	public Date getRelativeDate() {
		return relativeDate;
	}

	public void setRelativeDate(Date relativeDate) {
		this.relativeDate = relativeDate;
	}

	public List<String> getAvailableTimeZones() {
		return availableTimeZones;
	}

	public void setAvailableTimeZones(List<String> availableTimeZones) {
		this.availableTimeZones = availableTimeZones;
	}

	public String getSelectedTimeZone() {
		return selectedTimeZone;
	}

	public void setSelectedTimeZone(String selectedTimeZone) {
		this.selectedTimeZone = selectedTimeZone;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public Date getRelativeTime() {
		return relativeTime;
	}

	public void setRelativeTime(Date relativeTime) {
		this.relativeTime = relativeTime;
	}

	public String getEstimatedDateString() {
		return estimatedDateString;
	}

	public void setEstimatedDateString(String estimatedDateString) {
		this.estimatedDateString = estimatedDateString;
	}

}
