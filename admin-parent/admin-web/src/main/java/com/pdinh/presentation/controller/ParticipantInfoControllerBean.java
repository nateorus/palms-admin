package com.pdinh.presentation.controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.persistence.FieldEntityType;
import com.kf.uffda.persistence.ValueEntityType;
import com.kf.uffda.service.UffdaService;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.CreateUserServiceLocal;
import com.pdinh.data.EnrollmentServiceLocalImpl;
import com.pdinh.data.TokenServiceLocalImpl;
import com.pdinh.data.UsernameServiceLocal;
import com.pdinh.data.jaxb.lrm.AddUserResponse;
import com.pdinh.data.jaxb.lrm.AddUserServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ConsentLogDao;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.ms.dao.OfficeLocationDao;
import com.pdinh.data.ms.dao.PhoneNumberDao;
import com.pdinh.data.ms.dao.PhoneNumberTypeDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectCourseLangPrefDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.RosterDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.ms.dao.UserDocumentTypeDao;
import com.pdinh.data.ms.dao.UserNoteDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.FileAttachment;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.OfficeLocation;
import com.pdinh.persistence.ms.entity.PhoneNumber;
import com.pdinh.persistence.ms.entity.PhoneNumberType;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectCourseLangPref;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.Roster;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.persistence.ms.entity.UserCoursePrefs;
import com.pdinh.persistence.ms.entity.UserDocument;
import com.pdinh.persistence.ms.entity.UserNote;
import com.pdinh.presentation.domain.AddUserResultObj;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.FileAttachmentObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.NoteObj;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.domain.ParticipantDocumentObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.helper.ConsentHelper;
import com.pdinh.presentation.helper.ParticipantValidator;
import com.pdinh.presentation.helper.Utils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ParticipantInfoViewBean;
import com.pdinh.presentation.view.UserDocumentsViewBean;

@ManagedBean(name = "participantInfoControllerBean")
@ViewScoped
public class ParticipantInfoControllerBean extends CustomFieldControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private final static int CREATE_ACTION = 1;
	private final static int SAVE_ACTION = 2;
	private static final Integer VALIDATION_ERROR = -1;
	private static String chqAbbv = "chq";
	private static String fexAbbv = "finex";

	private static final Logger log = LoggerFactory.getLogger(ParticipantInfoControllerBean.class);

	User currentUser;
	Company currentCompany;

	@EJB
	private EnrollmentServiceLocalImpl enrollmentService;

	@EJB
	private UserDao userDao;

	@EJB
	private UsernameServiceLocal usernameService;

	@EJB
	private CreateUserServiceLocal createUserService;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@EJB
	private LanguageSingleton languageSingleton;

	@EJB
	private ProjectSetupSingleton setupSingleton;

	@EJB
	private UserNoteDao userNoteDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private LanguageDao languageDao;

	@EJB
	private UserDocumentTypeDao userDocumentTypeDao;

	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;

	@EJB
	private CourseDao courseDao;

	@EJB
	private RosterDao rosterDao;

	@EJB
	private TokenServiceLocalImpl tokenService;

	@EJB
	private OfficeLocationDao officeLocationDao;

	// TODO Refactor common data (data used in more than one contxt) out of
	// ProjectSetupSingleton and into one that is not contexts specific
	@EJB
	private ProjectSetupSingleton projectSetupSingleton;

	@EJB
	private PhoneNumberTypeDao phoneNumberTypeDao;

	@EJB
	private PhoneNumberDao phoneNumberDao;

	@EJB
	private ProjectCourseLangPrefDao projectCourseLangPrefDao;

	@EJB
	private ConsentLogDao consentLogDao;

	@EJB
	private AddUserServiceLocal addUserService;

	@EJB
	private UffdaService uffdaService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "userDocumentsViewBean", value = "#{userDocumentsViewBean}")
	private UserDocumentsViewBean userDocumentsViewBean;

	@ManagedProperty(name = "participantInfoViewBean", value = "#{participantInfoViewBean}")
	private ParticipantInfoViewBean participantInfoViewBean;

	@ManagedProperty(name = "userDocumentsControllerBean", value = "#{userDocumentsControllerBean}")
	private UserDocumentsControllerBean userDocumentsControllerBean;

	@ManagedProperty(name = "consentHelper", value = "#{consentHelper}")
	private ConsentHelper consentHelper;

	// TODO: Temporary...move to proper place
	// - switch
	private Integer action = CREATE_ACTION;

	@Resource(name = "application/config_properties")
	private Properties appConfigProperties;

	// called by ParticipantListControllerBean on command button
	public void initForNew() {
		log.debug("In ParticipantInfoControllerBean initForNew()");
		currentUser = new User();
		currentCompany = getSessionBean().getCompany();

		action = CREATE_ACTION;

		populateInfo();
		populateSettings();

		participantInfoViewBean.setBottomButtonLabel("Add");
		participantInfoViewBean.setSaveButtonDisabled(false);
		participantInfoViewBean.setIsEditAction(false);
		participantInfoViewBean.setResetPasswordRendered(false);
		participantInfoViewBean.setGenerateUsernameRendered(true);
		participantInfoViewBean.setShowCopyString(false);

		resetValidationErrors();
	}

	// c
	public void initForAutoScore() {

	}

	// called by ParticipantListControllerBean and
	// ProjectManagementControllerBean
	// on command button
	public void initForEdit() {
		log.debug("In ParticipantInfoControllerBean initForEdit()");
		getSessionBean().refreshCurrentUser();

		currentUser = getSessionBean().getUser();
		currentCompany = getSessionBean().getCompany();
		action = SAVE_ACTION;

		resetValidationErrors();

		// Populate tabs
		log.debug("populating tabs");
		populateInfo();
		populateSettings();
		populateNotes();
		populateProjects();
		populateAutoScoreCourses();
		populateConsentHistory();

		log.debug("setting button label");
		participantInfoViewBean.setBottomButtonLabel("Save");
		participantInfoViewBean.setSaveButtonDisabled(true);
		participantInfoViewBean.setIsEditAction(true);
		if (participantInfoViewBean.getPassword() != "********") {
			participantInfoViewBean.setShowCopyString(true);
			participantInfoViewBean.setCopyString("username : " + participantInfoViewBean.getUsername()
					+ "  |  password : " + participantInfoViewBean.getPassword());
		} else {
			participantInfoViewBean.setShowCopyString(false);
		}
		participantInfoViewBean.setResetPasswordRendered(currentUser.getLoginStatus() > 0);
		participantInfoViewBean.setGenerateUsernameRendered(false);

		// delegate to common doc controller for setup
		userDocumentsControllerBean.initForEdit();

		handleExternallyManagedParticipants();

	}

	private void populateConsentHistory() {
		participantInfoViewBean.setConsentHistory(consentHelper.getConsentLogsForUser(currentUser.getUsersId()));
	}

	// Populate the courses for Auto Score
	public void populateAutoScoreCourses() {
		log.debug("In populateAutoScoreCourses()");
		List<Course> courses = new ArrayList<Course>();
		// default item for courses list
		Course dummyCourse = new Course();
		dummyCourse.setCourse(0);
		dummyCourse.setCourseLabel("-------------");
		courses.add(dummyCourse);

		// Added new typed query for getting rosters by user id -- DAO loop
		// selects each one at a time by course and user, which was very slow
		// -DW
		List<Roster> rosters = rosterDao.findRostersByUserId(currentUser.getUsersId());
		// List<Roster> rosters = new ArrayList<Roster>();
		// int maxCourseId = courseDao.getMaxCourseId();
		// for (int courseId = 1; courseId <= maxCourseId; courseId++) {
		// rosters.add(rosterDao.findByUserIdAndCourseId(currentUser.getUsersId(),
		// courseId));
		// }
		currentUser.setRosters(rosters);

		if (!rosters.isEmpty()) {
			for (Roster roster : currentUser.getRosters()) {
				if (roster != null) {
					if (roster.getCourse() != null && roster.getCourse().getType() > 20) {
						courses.add(roster.getCourse());
						log.debug("adding course " + roster.getCourse().getAbbv());
					}
				}
			}
		}

		participantInfoViewBean.setCourses(courses);

	}

	private String getBusinessPhoneNumber(User user) {
		List<PhoneNumber> busPhones = user.getBusinessPhones();
		log.debug("in getBusPhones with {} busPhones", busPhones.size());
		if (busPhones != null && (!busPhones.isEmpty())) {
			for (PhoneNumber number : busPhones) {

				if (number.isPrimary()) {
					return number.getNumber();
				}
			}
		}

		return "";
	}

	// Populate the info tab
	public void populateInfo() {

		log.debug("In ParticipantInfoControllerBean populateInfo()");
		if (action == SAVE_ACTION) {
			log.debug("first: {}", currentUser.getFirstname());
			log.debug("last: {}", currentUser.getLastname());
			log.debug("loginStatus : {}", currentUser.getLoginStatus());
			// Edit: Populate metadata
			participantInfoViewBean.setFirstName(currentUser.getFirstname());
			participantInfoViewBean.setLastName(currentUser.getLastname());
			participantInfoViewBean.setUsername(currentUser.getUsername());
			if (currentUser.getLoginStatus() == 0) {
				participantInfoViewBean.setPassword(currentUser.getPassword());
			} else {
				participantInfoViewBean.setPassword("********");
			}
			participantInfoViewBean.setEmail(currentUser.getEmail());
			participantInfoViewBean.setLanguageId(currentUser.getLangPref());
			participantInfoViewBean.setBusinessPhone(this.getBusinessPhoneNumber(currentUser));
			participantInfoViewBean.setOptional1(currentUser.getOptional1());
			participantInfoViewBean.setOptional2(currentUser.getOptional2());
			participantInfoViewBean.setOptional3(currentUser.getOptional3());
			participantInfoViewBean.setSupervisorName(currentUser.getSupervisorName());
			participantInfoViewBean.setSupervisorEmail(currentUser.getSupervisorEmail());

			// participantInfoViewBean.setLoginId(currentUser.getEmail());
			// //This is temporary, as we'll be making a switch to more sensible
			// username/email use
			participantInfoViewBean.setLoginId(currentUser.getUsername());

			// initialize field validation messages

			if (participantInfoViewBean.getFirstName().equals(""))
				participantInfoViewBean.setFirstNameMsg("required");
			else
				participantInfoViewBean.setFirstNameMsg("");

			if (participantInfoViewBean.getLastName().equals(""))
				participantInfoViewBean.setLastNameMsg("required");
			else
				participantInfoViewBean.setLastNameMsg("");

			if (participantInfoViewBean.getEmail().equals(""))
				participantInfoViewBean.setEmailMsg("required");
			else
				participantInfoViewBean.setEmailMsg("");
/*
			if (participantInfoViewBean.getSelectedOfficeLocationId() == VALIDATION_ERROR)
				participantInfoViewBean.setOfficeLocationMsg("required");
			else
				participantInfoViewBean.setOfficeLocationMsg("");
*/
			// delivery office
			//OfficeLocation dO = currentUser.getOfficeLocation();
			//OfficeLocationObj deliveryOfficeObj = new OfficeLocationObj();
			//if (dO != null) {
				//deliveryOfficeObj = new OfficeLocationObj(dO.getId(), dO.getLocation());
			//}
			//participantInfoViewBean.setSelectedOfficeLocationId(deliveryOfficeObj.getOfficeLocationId());
			validateSelectedLanguage();
		} else {
			// Create
			participantInfoViewBean.clearInfo();
			// for new users, these fields have a message
			participantInfoViewBean.setLoginId("Click 'Add' to see username");
			participantInfoViewBean.setPassword("Click 'Add' to see password");
			participantInfoViewBean.setFirstNameMsg("");
			participantInfoViewBean.setLastNameMsg("");
			participantInfoViewBean.setEmailMsg("");
			participantInfoViewBean.setBusinessPhone("");
			participantInfoViewBean.setOfficeLocationMsg("");

			participantInfoViewBean.setOptional1("");
			participantInfoViewBean.setOptional2("");
			participantInfoViewBean.setOptional3("");
			participantInfoViewBean.setSupervisorName("");
			participantInfoViewBean.setSupervisorEmail("");

			//participantInfoViewBean.setSelectedOfficeLocationId(VALIDATION_ERROR);

		}
		participantInfoViewBean.setCoachFlag(currentUser.getCoachFlag());

		// build view language list from distinct project languages in session
		// bean
		List<LanguageObj> languages = new ArrayList<LanguageObj>();
		/*
		 * to just add project lanugages
		for (Map.Entry<String, String> entry : getSessionBean().getProjectLanguages().entrySet()) {
		    languages.add(new LanguageObj(entry.getKey(),entry.getValue()));
		}
		 */

		List<Language> langs = languageDao.findAll();
		for (Language lang : langs) {
			languages.add(new LanguageObj(lang.getCode(), lang.getName()));
		}

		participantInfoViewBean.setLanguages(languages);

		Boolean coachFlagRendered = currentCompany.getCoachFlag();
		participantInfoViewBean.setCoachFlagRendered(coachFlagRendered);

		// set delivery offices
		/*
		List<OfficeLocationObj> offices = new ArrayList<OfficeLocationObj>();
		if (currentUser.getOfficeLocation() == null || action == CREATE_ACTION) {
			offices.add(new OfficeLocationObj(VALIDATION_ERROR, "No office location selected"));
		}
		*/
		// List<OfficeLocation> oL =
		// officeLocationDao.findAllOfficeLocationsOrdered();
		/*
		List<OfficeLocation> oL = projectSetupSingleton.getOfficeLocations();
		for (OfficeLocation o : oL) {
			offices.add(new OfficeLocationObj(o));
		}
		participantInfoViewBean.setOfficeLocations(offices);
*/
		// Get the participant custom fields
		participantInfoViewBean.clearParticipantCustomFields();
		participantInfoViewBean.getParticipantView().init();
		Project project = getSessionBean().getProject();
		if (project != null) {
			uffdaService.getCustomFields(project.getCompany().getCompanyId(), project.getProjectId(),
					FieldEntityType.PALMS_PROJECT_USERS, currentUser.getUsersId(), ValueEntityType.PROJECT_USER_ID,
					participantInfoViewBean.getParticipantCustomFields(), "en", false, new Date(), true, false, true);

			participantInfoViewBean.getParticipantView().createCustomForm(
					participantInfoViewBean.getParticipantCustomFields(), 1);
		}
		if (participantInfoViewBean.hasParticipantCustomFields()) {
			participantInfoViewBean.getParticipantView().setRenderCustomForm(true);
		}

		// Get the user custom fields
		participantInfoViewBean.clearUserCustomFields();
		participantInfoViewBean.getUserView().init();
		uffdaService.getCustomFields(getSessionBean().getCompany().getCompanyId(), getSessionBean().getCompany()
				.getCompanyId(), FieldEntityType.CLIENT_USERS, currentUser.getUsersId(),
				ValueEntityType.PROJECT_USER_ID, participantInfoViewBean.getUserCustomFields(), "en", false,
				new Date(), true, false, true);

		participantInfoViewBean.getUserView().createCustomForm(participantInfoViewBean.getUserCustomFields(), 1);
		if (participantInfoViewBean.hasUserCustomFields()) {
			participantInfoViewBean.getUserView().setRenderCustomForm(true);
		}
	}

	// Populate the settings tab
	public void populateSettings() {
		log.debug("In ParticipantInfoControllerBean populateSettings()");

		Project project = getSessionBean().getProject();
		if (project == null)
			return;

		// check if project has chq or finex selected to determine if settings
		// tab is rendered
		List<ProjectCourse> projectCourseList = projectCourseDao.findProjectCourseByProjectId(project.getProjectId());
		boolean hasChq = false;
		boolean hasFinex = false;

		for (ProjectCourse pc : projectCourseList) {
			if (pc.getCourse().getAbbv().equals(chqAbbv) && pc.getSelected()) {
				hasChq = true;
			}
			if (pc.getCourse().getAbbv().equals(fexAbbv) && pc.getSelected()) {
				hasFinex = true;
			}
		}

		if (hasChq || hasFinex) {
			log.debug("populateSettings() hasChq: {}, hasFinex: {}", hasChq, hasFinex);
			participantInfoViewBean.setSettingsTabRendered(true);

			Course chq = setupSingleton.findCourseByAbbv("chq");
			ProjectCourseLangPref pclp = projectCourseLangPrefDao.findByProjectAndCourse(project.getProjectId(),
					chq.getCourse());
			String projectChqLang = null;
			if (pclp != null) {
				projectChqLang = pclp.getLanguage().getCode();
			}

			// make sure these start null
			participantInfoViewBean.setLanguageFEX(null);
			participantInfoViewBean.setLanguageCHQ(projectChqLang);

			List<LanguageObj> chqLangs = new ArrayList<LanguageObj>();
			List<LanguageObj> fexLangs = new ArrayList<LanguageObj>();

			// get users language preferences and put into viewbean state
			List<UserCoursePrefs> ucpList = userDao.findUserCoursePrefs(currentUser.getUsersId());
			for (UserCoursePrefs ucp : ucpList) {
				String abbv = ucp.getCourse().getAbbv();
				log.debug(" course: {} code: {} name: {}", new Object[] { ucp.getCourse().getAbbv(),
						ucp.getLanguage().getCode(), ucp.getLanguage().getName() });
				if (abbv.equals(fexAbbv)) {
					participantInfoViewBean.setLanguageFEX(ucp.getLanguage().getCode());
					log.debug("set fex to {}", ucp.getLanguage().getCode());
				} else if (abbv.equals(chqAbbv)) {
					participantInfoViewBean.setLanguageCHQ(ucp.getLanguage().getCode());
					log.debug("set chq to {}", ucp.getLanguage().getCode());
				}
			}

			// if language wasn't set for an instrument, set it to users default
			// language
			// or to english if default language is not associated with
			// instrument
			if (participantInfoViewBean.getLanguageFEX() == null) {
				log.debug("fex not set ");
				participantInfoViewBean.setLanguageFEX("en");
				log.debug("fex not set, set to 'en' ");
			}

			if (participantInfoViewBean.getLanguageCHQ() == null) {
				log.debug("chq not set ");
				participantInfoViewBean.setLanguageCHQ("en");
				log.debug("chq not set, set to 'en' ");
			}

			// temporarily use existing
			List<LanguageObj> chqLangList = languageSingleton.getCourseLangs(chqAbbv);
			List<LanguageObj> fexLangList = languageSingleton.getCourseLangs(chqAbbv);
			// List<Language> chqList = languageDao.findByCourseAbbv(chqAbbv);
			// List<Language> fexList = languageDao.findByCourseAbbv(fexAbbv);
			log.debug("chq size: {}", chqLangList.size());
			log.debug("fex size: {}", fexLangList.size());

			// System.out.println("chq languages");
			for (LanguageObj lang : chqLangList) {
				// System.out.println(lang.getCode());
				chqLangs.add(lang);
			}
			// System.out.println("fex languages");
			for (LanguageObj lang : fexLangList) {
				// System.out.println(lang.getCode());
				fexLangs.add(lang);
			}
			participantInfoViewBean.setLanguagesCHQ(chqLangs);
			participantInfoViewBean.setLanguagesFEX(fexLangs);

			// initialize dropdown disabled states
			log.debug("initialize dropdown disabled states");

			participantInfoViewBean.setLanguageCHQRendered(hasChq);
			participantInfoViewBean.setLanguageFEXRendered(hasFinex);

		} else {
			log.debug("populateSettings(), no chq or finex so don't render settings tab");
			participantInfoViewBean.setSettingsTabRendered(false);
		}

		log.debug("END populateSettings()");

	}

	// Populate the notes tab
	public void populateNotes() {
		log.debug("In ParticipantInfoControllerBean populateNotes()");

		List<NoteObj> nol = new ArrayList<NoteObj>();
		List<UserNote> pnList = userNoteDao.findNotesByUserId(currentUser.getUsersId());

		log.debug(" notes list size = {}", pnList.size());

		UserNote pn;
		Iterator<UserNote> iterator = pnList.iterator();
		while (iterator.hasNext()) {
			pn = iterator.next();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z Z");
			String datestr = sdf.format(pn.getDateAdded());

			String name = pn.getExtAdminId().trim();
			if (name.length() < 1) {
				name = NoteObj.NO_AUTHOR;
			}
			nol.add(new NoteObj(pn.getNote(), name, datestr));
		}
		participantInfoViewBean.setNotes(nol);
		participantInfoViewBean.setNewNote(new NoteObj(""));

	}

	// Populate the assigned projects tab
	public void populateProjects() {
		log.debug("In ParticipantInfoControllerBean populateProjects()");

		List<Project> projectList = projectDao.findAllByUserId(currentUser.getUsersId());
		List<ProjectObj> pol = new ArrayList<ProjectObj>();
		String projectTypeName = "";
		Project p;
		Iterator<Project> iterator = projectList.iterator();
		while (iterator.hasNext()) {
			p = iterator.next();

			if (p.getProjectType().getProjectTypeCode()
					.equals(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES)) {
				projectTypeName = "TLT with Cognitives";
			} else if (p.getProjectType().getProjectTypeCode()
					.equals(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES)) {
				projectTypeName = "TLT without Cognitives";
			} else {
				projectTypeName = p.getProjectType().getName();
			}
			String consultants = "";
			List<String> realmNames = Arrays.asList(appConfigProperties.getProperty("palmsRealmNames").split(","));
			List<ExternalSubjectDataObj> consultantObjs = authorizationService.getAssignedUsersForProjectUser(
					p.getProjectId(), currentUser.getUsersId(), realmNames);
			for (ExternalSubjectDataObj extSub : consultantObjs) {
				consultants = consultants + extSub.getUsername() + ", ";
			}
			if (consultants.length() > 0) {
				consultants = consultants.substring(0, consultants.lastIndexOf(","));
			}
			/*
						pol.add(new ProjectObj(p.getProjectId(), p.getName(), p.getProjectCode(), projectTypeName, p
								.getProjectType().getProjectTypeId(), projectDao.resolveTargetLevel(p), p.getExtAdminId(), 0, p
								.getDateCreated(), p.getFromEmail(), p.getDueDate()));
			*/
			pol.add(new ProjectObj(p.getProjectId(), p.getName(), p.getProjectCode(), projectTypeName, p
					.getProjectType().getProjectTypeId(), projectDao.resolveTargetLevel(p), p.getExtAdminId(), 0, p
					.getDateCreated(), p.getDueDate(), consultants));

		}

		participantInfoViewBean.setProjects(pol);
	}

	public void selectPostCourse(AjaxBehaviorEvent event) {
		log.debug("IN ParticipantInfoControllerBean.selectPostCourse()");

		String xmlPostTrackingToken = getTrackingToken();

		participantInfoViewBean.setXmlPostTrackingToken(xmlPostTrackingToken);
		participantInfoViewBean.setXmlPostFormRendered(true);
	}

	private String getTrackingToken() {
		// ContentToken ct = new ContentToken();
		log.debug("In getTrackingToken().  usersId: {}, companyId: {}, courseid: {}",
				new Object[] { Integer.toString(currentUser.getUsersId()), currentUser.getCompany().getCompanyId(),
						participantInfoViewBean.getXmlPostCourseId() });
		String xmlPostTrackingToken = tokenService.generateToken(currentUser.getUsersId(),
				currentUser.getCompany().getCompanyId(), participantInfoViewBean.getXmlPostCourseId()).getToken();

		// String xmlPostTrackingToken = ct
		// .makeNewContentToken(Integer.toString(currentUser.getUsersId()),
		// Integer.toString(currentUser.getCompany().getCompanyId()),
		// participantInfoViewBean.getXmlPostCourseId());
		return xmlPostTrackingToken;
	}

	public boolean saveForProject() {
		log.debug("IN ParticipantInfoControllerBean.saveForProject()");

		boolean isSuccess = false;

		if (requiredFieldsPresent()) {

			AddUserResultObj result = save();

			// get the current user from the one last created
			User user = currentUser;

			if (!result.getSuccess()) {
				log.debug("isSuccess = false");
				isSuccess = false;

				for (ErrorObj errorObj : result.getErrors()) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, errorObj.getErrorMsg(), ""));
					log.debug("errorMsg = {}", errorObj.getErrorMsg());
				}

				// tell browser it wasn't successful
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);

			} else {

				isSuccess = true;
				Project project = getSessionBean().getProject();

				log.debug("isSuccess , saved user, id={}", user.getUsersId());
				if (action == CREATE_ACTION) {
					log.debug("Save for new");
					// Create Project User
					// Moved to create user service
					/*
					ProjectUser projectUser = new ProjectUser();
					projectUser.setProject(project);
					projectUser.setUser(user);
					project.getProjectUsers().add(projectUser);
					projectUserDao.create(projectUser);
					*/
					// projectDao.update(project);

					// populate other tabs
					populateProjects();
					populateNotes();
					userDocumentsControllerBean.initForEdit();

					// now put button in "Save" mode
					participantInfoViewBean.setBottomButtonLabel("Save");
					participantInfoViewBean.setGenerateUsernameRendered(false);

					// tell browser not to close
					// TODO: use better param string
					RequestContext.getCurrentInstance().addCallbackParam("notValid", true);

				} else if (action == SAVE_ACTION) {
					log.debug("Save for edit");
					// JJB: Seemed to be adding the entity again...but seemed
					// like only when changing language. Look at.
					// projectDao.update(project);
				}

				action = SAVE_ACTION;

			}

		} else {
			log.debug("failed requiredFieldsPresent()");
		}

		return isSuccess;
	}

	public boolean saveForClient() {
		log.debug("IN ParticipantInfoControllerBean.saveForClient()");
		getSessionBean().setProject(null);
		if (requiredFieldsPresent()) {
			log.debug("calling save");
			AddUserResultObj result = save();

			if (result.getSuccess()) {
				if (action == CREATE_ACTION) {
					// this is done in save
					// TODO: does companyDao.update do anything here?
					; // result.getUser().setCompany(getSessionBean().getCompany());
				} else if (action == SAVE_ACTION) {
					companyDao.update(getSessionBean().getCompany());
				}
				action = SAVE_ACTION;
				return true;
			} else {
				for (ErrorObj errorObj : result.getErrors()) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, errorObj.getErrorMsg(), ""));
				}
				// tell browser it wasn't successful
				RequestContext.getCurrentInstance().addCallbackParam("notValid", true);

				return false;
			}

		} else {
			log.debug("failed requiredFieldsPresent()");
		}
		return false;
	}

	private Boolean requiredFieldsPresent() {
		log.debug("IN ParticipantInfoControllerBean.requiredFieldsPresent()");
		Boolean retval = true;
		ParticipantInfoViewBean pivb = getParticipantInfoViewBean();

		String firstName = pivb.getFirstName();
		String lastName = pivb.getLastName();
		String email = pivb.getEmail();
		String username = pivb.getUsername();
		String userOrParticipant = (pivb.isUser()) ? "User" : "User";
		//Integer officeLocation = pivb.getSelectedOfficeLocationId();

		this.resetValidationErrors();

		// Validate Participant Custom Fields
		boolean participantCustomFieldValidationError = participantInfoViewBean.getParticipantView()
				.validateCustomFormFields(true, "Participant Info Tab - ");

		boolean userCustomFieldValidationError = participantInfoViewBean.getUserView().validateCustomFormFields(true,
				"User Info Tab - ");

		if (firstName.equals("") || lastName.equals("") || email.equals("") || username.equals("")
				|| participantCustomFieldValidationError
				|| userCustomFieldValidationError) {

			if (firstName.equals("")) {
				pivb.setFirstNameMsg("required");
			}

			if (lastName.equals("")) {
				pivb.setLastNameMsg("required");
			}

			if (email.equals("")) {
				pivb.setEmailMsg("required");
			}

			if (username.equals("")) {
				pivb.setLoginIdMsg("required");
			}

			RequestContext.getCurrentInstance().addCallbackParam("notValid", true);
			log.debug("set callback param notValid");
			retval = false;
		}

		return retval;

	}

	private void resetValidationErrors() {
		getParticipantInfoViewBean().setFirstNameMsg("");
		getParticipantInfoViewBean().setLastNameMsg("");
		getParticipantInfoViewBean().setEmailMsg("");
		getParticipantInfoViewBean().setLoginIdMsg("");
		getParticipantInfoViewBean().setOfficeLocationMsg("");
	}

	private AddUserResultObj save() {
		log.debug("IN ParticipantInfoControllerBean.save()");
		ParticipantValidator pv = new ParticipantValidator(userDao);
		Subject subject = SecurityUtils.getSubject();
		// TODO: Move this to businesss logic tier
		// 6/21/2013: NW: Moved
		// May also be able to do using cascading persist. For now this is
		// easier.
		log.debug("IN save");
		// Still trying to figure out pattern here, but working

		User user = null;
		AddUserResultObj result = null;
		log.debug("action={}", action);

		if (action == CREATE_ACTION) {
			FacesContext.getCurrentInstance().getAttributes()
					.put("updateOperationName", EntityAuditLogger.OP_CREATE_USER);
			log.debug("create action");

			// Create User

			// TODO: reduce code repoeat for create and edit cases
			String email = Utils.trim(participantInfoViewBean.getEmail());
			log.debug("userName={},firstName={},lastName={},email={},langId={}",
					new Object[] { participantInfoViewBean.getUsername(), participantInfoViewBean.getFirstName(),
							participantInfoViewBean.getLastName(), email, participantInfoViewBean.getLanguageId() });

			user = participantInfoViewBeanToUser();
			user.setPassword(Utils.getNewPassword());

			log.debug("Coach Flag {}", participantInfoViewBean.getCoachFlag());

			user.setCompany(getSessionBean().getCompany());

			// result = pv.validateUserObj(user);
			result = (AddUserResultObj) createUserService.validateUser(user);

			if (!result.getSuccess()) {
				return result;
			}
			String CHQlanguage = null;
			String FEXlanguage = null;
			if (participantInfoViewBean.getLanguageCHQ() != null) {
				if (participantInfoViewBean.getLanguageCHQRendered().booleanValue()) {

					CHQlanguage = participantInfoViewBean.getLanguageCHQ();
				}
			}
			if (participantInfoViewBean.getLanguageFEX() != null) {
				if (participantInfoViewBean.getLanguageFEXRendered().booleanValue()) {
					FEXlanguage = participantInfoViewBean.getLanguageFEX();
				}
			}
			// the service may not assume these values, but in this context we
			// can
			user.setType(1);
			user.setMserverid((short) 1);
			currentUser = createUserService.createNewUser(user, getSessionBean().getCompany(), getSessionBean()
					.getProject(), CHQlanguage, FEXlanguage);
			if (getSessionBean().getProject() == null) {
				if (!(subject.isPermitted("viewAllUsers")
						|| subject.isPermitted(EntityType.COMPANY_TYPE + ":viewAllUsers:"
								+ getSessionBean().getCompany().getCompanyId()) || subject
							.isPermitted(EntityType.COMPANY_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
									+ getSessionBean().getCompany().getCompanyId()))) {
					RoleContext context = authorizationService.grantUserAccess(sessionBean.getRoleContext()
							.getSubject(), getSessionBean().getCompany(), user, subject.getPrincipal().toString(),
							null, EntityRule.ALLOW_CHILDREN);
				}
			} else {

				if (!(subject.isPermitted("viewAllUsers")
						|| subject.isPermitted(EntityType.PROJECT_TYPE + ":viewAllUsers:"
								+ getSessionBean().getProject().getProjectId()) || subject
							.isPermitted(EntityType.PROJECT_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
									+ getSessionBean().getProject().getProjectId()))) {
					RoleContext context = authorizationService.grantProjectUserAccess(sessionBean.getRoleContext()
							.getSubject(), getSessionBean().getProject(), currentUser, subject.getPrincipal()
							.toString(), null, EntityRule.ALLOW_CHILDREN);
				}
			}
			// Moved to CreateUserService
			/*
			user.setActive((short) 1);
			user.setType(1);
			user.setMserverid((short) 1);
			user.setLearningType((short) 1);

			user.setPositions(new ArrayList<Position>());
			user.setRosters(new ArrayList<Roster>());

			// Loop through courses in project and add the position entries
			Project project = getSessionBean().getProject();
			if (project != null) {
				List<ProjectCourse> projectCourses = project.getProjectCourses();
				if (projectCourses != null) {
					for (ProjectCourse projectCourse : projectCourses) {
						if (projectCourse.getSelected()) {
							user = getEnrollmentService().enrollUserInCourse(sessionBean.getCompany(), projectCourse,
									user);
							getEnrollmentService().activateDeactivateInstrument(sessionBean.getCompany(),
									projectCourse, user);
						}
					}
				}
			}
			// no project keep calm and carry on

			currentUser = userDao.create(user);
			// NW: Must set password after user is created in order to use
			// rowguid as salt. Uses first 10 characters of rowguid as salt.
			currentUser.setHashedpassword(Utils.generateHashedPassword(currentUser.getPassword(), currentUser
					.getRowguid().substring(0, 10).toUpperCase()));

			userDao.update(currentUser);

			// JJB: Moved this to after the create because needs the user id
			// Think need to check if course is in project_content
			// This needs looked at more thoroughly
			if (participantInfoViewBean.getLanguageCHQ() != null) {
				if (participantInfoViewBean.getLanguageCHQRendered().booleanValue()) {
					System.out.println("saving chq override: uid=" + user.getUsersId() + " abbv=" + chqAbbv
							+ " chqlang=" + participantInfoViewBean.getLanguageCHQ());
					Course chq = courseDao.findByAbbv(chqAbbv);
					userDao.updateCourseLanguagePref(user, chq, participantInfoViewBean.getLanguageCHQ());
				}
			}
			if (participantInfoViewBean.getLanguageFEX() != null) {
				if (participantInfoViewBean.getLanguageFEXRendered().booleanValue()) {
					System.out.println("saving fex override: uid=" + user.getUsersId() + " abbv=" + fexAbbv
							+ " fexlang=" + participantInfoViewBean.getLanguageFEX());
					Course fex = courseDao.findByAbbv(fexAbbv);
					userDao.updateCourseLanguagePref(user, fex, participantInfoViewBean.getLanguageFEX());
				}
			}

			System.out.println("done saving language overrides");
			System.out.flush();
			*/
			// put new user in session
			getSessionBean().setUser(currentUser);
			List<User> singleton = new ArrayList<User>();
			singleton.add(currentUser);

			if (participantInfoViewBean.isPDAProject()) {
				AddUserResponse response = addUserService.addUsersToLrm(singleton, getSessionBean().getProject()
						.getProjectId(), getSessionBean().getSubjectId());
				if (response.getPostResult().equalsIgnoreCase("FAILED")) {
					String msg = "User was added to the project, but the attempt to update the user record in PDA failed.";
					FacesContext.getCurrentInstance().addMessage("firstName",
							new FacesMessage(FacesMessage.SEVERITY_WARN, null, msg));
				}
			}

			// change button from "Add" to "Save"
			participantInfoViewBean.setIsEditAction(true);

			participantInfoViewBean.setGenerateUsernameRendered(false);
			// do this later
			// action = SAVE_ACTION;

			// get info back from saved user into view
			participantInfoViewBean.setUsername(currentUser.getUsername());
			participantInfoViewBean.setLoginId(currentUser.getEmail());

			if (currentUser.getLoginStatus() == 0) {
				participantInfoViewBean.setPassword(currentUser.getPassword());
			} else {
				participantInfoViewBean.setPassword("********");
			}
			participantInfoViewBean.setShowCopyString(true);
			participantInfoViewBean.setCopyString("username : " + participantInfoViewBean.getUsername()
					+ "  |  password : " + participantInfoViewBean.getPassword());

		} else if (action == SAVE_ACTION) {
			FacesContext.getCurrentInstance().getAttributes()
					.put("updateOperationName", EntityAuditLogger.OP_UPDATE_USER);
			log.debug("save action");
			ParticipantInfoViewBean pivb = getParticipantInfoViewBean();

			log.debug(
					"View Bean User Data: userName: {}, firstName: {}, lastName: {}, email: {}, langId: {}",
					new Object[] { pivb.getUsername(), pivb.getFirstName(), pivb.getLastName(), pivb.getEmail(),
							pivb.getLanguageId() });

			user = getSessionBean().getUser();
			log.debug("Do we have a user? ", (null != user));
			copyParticipantInfoViewBeanToUser(user);

			// TODO: needs work
			// TMO: works now. see validator
			result = pv.validateUserObj(user);
			if (!result.getSuccess()) {
				return result;
			}

			log.debug("saving language overrides");
			// System.out.flush();

			if (pivb.getLanguageCHQ() != null) {
				if (pivb.getLanguageCHQRendered().booleanValue()) {
					log.debug("saving chq override: uid: {}, abbv: {}, chqlang: {}", new Object[] { user.getUsersId(),
							chqAbbv, pivb.getLanguageCHQ() });

					Course chq = setupSingleton.findCourseByAbbv(chqAbbv);
					userDao.updateCourseLanguagePref(user, chq, pivb.getLanguageCHQ());
				}
			}

			if (pivb.getLanguageFEX() != null) {
				if (pivb.getLanguageFEXRendered().booleanValue()) {
					log.debug("saving fex override: uid: {}, abbv: {}, chqlang: {}", new Object[] { user.getUsersId(),
							fexAbbv, pivb.getLanguageFEX() });

					Course fex = setupSingleton.findCourseByAbbv(fexAbbv);
					userDao.updateCourseLanguagePref(user, fex, pivb.getLanguageFEX());
				}
			}

			log.debug("done saving language overrides");
			// System.out.flush();

			// nothing has to be done with notes since they are saved when user
			// clicks "add note" in the tab

			// only do Document stuff if documents have been added or removed
			if (userDocumentsViewBean.getDoSave()) {
				// delete docs
				List<UserDocument> docsToDelete = new ArrayList<UserDocument>();
				for (UserDocument ud : user.getUserDocuments()) {
					boolean found = false;
					for (Object docObj : userDocumentsViewBean.getDocuments()) {
						ParticipantDocumentObj pdo = (ParticipantDocumentObj) docObj;
						if (ud.getDocumentId() == pdo.getDocumentId()) {
							found = true;
							ud.setIncludeInIntegrationGrid(pdo.getIsIntegrationGrid());
						}
					}
					if (found == false) {
						docsToDelete.add(ud);
					}
				}

				// delete files from disk and remove from user
				for (UserDocument ud : docsToDelete) {
					try {
						fileAttachmentService.delete(ud.getFileAttachment());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				user.getUserDocuments().removeAll(docsToDelete);

				// update / create
				for (Object docObj : userDocumentsViewBean.getDocuments()) {
					ParticipantDocumentObj pdo = (ParticipantDocumentObj) docObj;
					for (UserDocument ud : user.getUserDocuments()) {
						if (ud.getDocumentId() == pdo.getDocumentId()) {
							ud.setIncludeInIntegrationGrid(pdo.getIsIntegrationGrid());
						}
					}

					if (pdo.isNewDocument()) {
						// new docs
						UserDocument ud = new UserDocument();
						ud.setDescription(pdo.getDescription());
						ud.setUserDocumentType(userDocumentTypeDao.findById(pdo.getType()));
						ud.setIncludeInIntegrationGrid(pdo.getIsIntegrationGrid());
						ud.setUser(user);

						FileAttachmentObj fao = pdo.getFileAttachment();
						FileAttachment fileAttachment = new FileAttachment();
						fileAttachment.setDateUploaded(fao.getDateUploaded());
						fileAttachment.setName(fao.getName());
						fileAttachment.setPath(fao.getPath());
						fileAttachment.setExtAdminId(fao.getExtAdminId());

						ud.setFileAttachment(fileAttachment);
						user.getUserDocuments().add(ud);
					}
				}
			}

			userDao.update(user);

		}

		if (StringUtils.isEmpty(participantInfoViewBean.getBusinessPhone())) {
			participantInfoViewBean.setBusinessPhone(null);
		} else {
			createUserService.addPhoneNumber(user.getUsersId(), participantInfoViewBean.getBusinessPhone(),
					PhoneNumberType.BUISINESS_PHONE_ABBV, true, false, "");
			// this.crudPhoneNumber(user,
			// participantInfoViewBean.getBusinessPhone(),
			// PhoneNumberType.BUISINESS_PHONE_ABBV,
			// true, null);
		}

		// Save Participant Custom Fields
		Project project = getSessionBean().getProject();
		if (project != null) {
			participantInfoViewBean.getParticipantView().updateCustomFieldData(
					participantInfoViewBean.getParticipantCustomFields());

			uffdaService.persistCustomFields(user.getUsersId(), project.getProjectId(),
					FieldEntityType.PALMS_PROJECT_USERS, user.getUsersId(), ValueEntityType.PROJECT_USER_ID,
					participantInfoViewBean.getParticipantCustomFields(), user.getCompany().getCompanyId(), false);
		}

		// Save the User Custom Fields
		participantInfoViewBean.getUserView().updateCustomFieldData(participantInfoViewBean.getUserCustomFields());

		uffdaService.persistCustomFields(user.getUsersId(), user.getCompany().getCompanyId(),
				FieldEntityType.CLIENT_USERS, user.getUsersId(), ValueEntityType.PROJECT_USER_ID,
				participantInfoViewBean.getUserCustomFields(), user.getCompany().getCompanyId(), false);
		return result;
	}

	public Boolean isInParticipantList(List<ParticipantObj> participants, Integer userId) {
		log.debug("IN ParticipantInfoControllerBean.isInParticipantList()");
		Iterator<ParticipantObj> iterator = participants.iterator();
		while (iterator.hasNext()) {
			ParticipantObj participant = iterator.next();
			// if (participant.getUserId().equalsIgnoreCase(userId)) {
			if (participant.getUserId() == userId) {
				return true;
			}
		}
		return false;
	}

	/*
	 * Notes management functions
	 */

	private void addNote() {
		log.debug("IN ParticipantInfoControllerBean.addNote()");
		ParticipantInfoViewBean pivb = getParticipantInfoViewBean();
		String text = pivb.getNewNoteText().toString();

		if (!text.isEmpty()) {
			Timestamp dateAdded = new java.sql.Timestamp(new java.util.Date().getTime());
			String admin = sessionBean.getUsername();

			// add the UserNote entity
			UserNote userNote = new UserNote();
			userNote.setDateAdded(dateAdded);
			User user = getSessionBean().getUser();
			userNote.setUser(user);
			userNote.setNote(text);
			userNote.setExtAdminId(admin);
			userNoteDao.create(userNote);

			// add to view bean
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z Z");
			String datestr = sdf.format(cal.getTime());
			List<NoteObj> notes = pivb.getNotes();
			notes.add(new NoteObj(text, admin, datestr));

			// clear note field to prepare for a new note
			pivb.setNewNoteText(null);
		}
	}

	public void handleAddNote(ActionEvent event) {
		log.debug("IN ParticipantInfoControllerBean.handleAddNote()");
		addNote();
	}

	public void handleAjax() {
		log.debug("Handling Ajax");
		participantInfoViewBean.setSaveButtonDisabled(false);
	}

	private void resetPassword() {
		// Generate hashed password... uses first 10 characters of rowguid as
		// Salt
		String password = Utils.getNewPassword();
		byte[] hash = Utils.generateHashedPassword(password, currentUser.getRowguid().substring(0, 10).toUpperCase());
		currentUser = userDao.resetPassword(hash, password, currentUser);
		try {
			currentUser.setLoginStatus((short) 0);
			currentUser.setPassword(password);
			currentUser.setHashedpassword(hash);
			participantInfoViewBean.setPassword(password);
			participantInfoViewBean.setResetPasswordRendered(false);
			participantInfoViewBean.setShowCopyString(true);
			participantInfoViewBean.setCopyString("username : " + participantInfoViewBean.getUsername()
					+ "  |  password : " + password);
			log.debug("Updated password to {}", password);
			log.debug("User {} loginStatus = {}", currentUser.getUsername(), currentUser.getLoginStatus());
			setCurrentUser(currentUser);
			// userDao.update(user); // NOT calling update, since the rest of
			// the data gets submitted and needs to be validated -- confuses
			// things
		} catch (Exception e) {
			log.debug("Oops exception resetting password - {}", e.getCause());
		}
	}

	public void handleResetPassword(ActionEvent event) {
		FacesContext.getCurrentInstance().getAttributes()
				.put("updateOperationName", EntityAuditLogger.OP_RESET_PASSWORD);
		resetPassword();
	}

	private void validateSelectedLanguage() {
		log.debug("IN ParticipantInfoControllerBean.selectLanguage()");
		String newLang = participantInfoViewBean.getLanguageId();
		log.debug("newLang: {}", newLang);

		if (getSessionBean().getProjectLanguages() != null) {
			Boolean isProjectLanguage = getSessionBean().getProjectLanguages().containsKey(newLang) == true;
			log.debug("result {}", isProjectLanguage);

			if (!isProjectLanguage) {
				FacesContext
						.getCurrentInstance()
						.addMessage(
								"langPref",
								new FacesMessage(
										FacesMessage.SEVERITY_WARN,
										"WARNING:",
										"There are no instruments in the project setup with the language you \n"
												+ "have selected for the participant. You can still add this language for the participant. \n"
												+ "If you do so, on first launch of the participant experience it will be presented with \n"
												+ "English and the participant will have the option at that time to select their language \n"
												+ "preference from a dropdown."));
			}
		}
	}

	public void handleLanguageChange(AjaxBehaviorEvent event) {
		validateSelectedLanguage();
	}

	public void handleGenerateUsername(ActionEvent event) {
		log.debug(">>>> handleGenerateUsername");
		generateUsername();
	}

	private void generateUsername() {
		log.debug(">>In ParticipantInfoControllerBean.generateUsername()");
		ParticipantInfoViewBean pivb = getParticipantInfoViewBean();
		if (null != currentUser) {
			log.debug("# generateUsername - currentUser found");
			String firstName = pivb.getFirstName();
			String lastName = pivb.getLastName();
			String email = pivb.getEmail();
			String username = pivb.getUsername();
			if ("".equals(username)) {
				log.debug("## generateUsername - currentUser username is blank");
				if (!"".equals(firstName) && !"".equals(lastName) && !"".equals(email)) {
					log.debug("ParticipantInfoControllerBean.generateUsername now firing");
					pivb.setUsername(getUsernameService().generateUsername(email, firstName, lastName));
				}
			} else {
				log.debug("## generateUsername - currentUser username = {}", username);
			}
		} else {
			log.debug("### generateUsername - currentUser is null");
		}
	}

	public void handleRefreshUsername(ActionEvent event) {
		log.debug(">>>> handleGenerateUsername");
		refreshUsername();
	}

	private void refreshUsername() {
		log.debug(">>In ParticipantInfoControllerBean.refreshUsername()");
		ParticipantInfoViewBean pivb = getParticipantInfoViewBean();
		if (null != currentUser) {
			log.debug("# refreshUsername - currentUser found");
			String firstName = pivb.getFirstName();
			String lastName = pivb.getLastName();
			String email = pivb.getEmail();
			// String username = pivb.getUsername();
			// if ("".equals(username)) {
			// System.out.println("## generateUsername - currentUser username is blank");
			// if(!"".equals(firstName)
			// && !"".equals(lastName)
			// && !"".equals(email)) {
			// System.out.println("ParticipantInfoControllerBean.generateUsername now firing");
			pivb.setUsername(getUsernameService().generateUsername(email, firstName, lastName));
			// }
			// }else{
			// System.out.println("## generateUsername - currentUser username = "
			// + username);
			// }
		} else {
			log.debug("### refreshUsername - currentUser is null");
		}
	}

	public User participantInfoViewBeanToUser() {
		User user = new User();
		copyParticipantInfoViewBeanToUser(user);
		return user;
	}

	public void copyParticipantInfoViewBeanToUser(User user) {
		user.setUsername(participantInfoViewBean.getUsername());
		user.setFirstname(participantInfoViewBean.getFirstName());
		user.setLastname(participantInfoViewBean.getLastName());
		user.setEmail(Utils.trim(participantInfoViewBean.getEmail()));
		user.setLangPref(participantInfoViewBean.getLanguageId());
		user.setOptional1(participantInfoViewBean.getOptional1());
		user.setOptional2(participantInfoViewBean.getOptional2());
		user.setOptional3(participantInfoViewBean.getOptional3());
		user.setSupervisorName(participantInfoViewBean.getSupervisorName());
		user.setSupervisorEmail(participantInfoViewBean.getSupervisorEmail());
		user.setCoachFlag(participantInfoViewBean.getCoachFlag());
	
	}

	// Checking if account is managed by an external system such as active
	// directory and disabling some of the fields.
	private void handleExternallyManagedParticipants() {
		Boolean disabled = getCurrentUser().isExternallyManaged();
		getParticipantInfoViewBean().setFirstNameDisabled(disabled);
		getParticipantInfoViewBean().setLastNameDisabled(disabled);
		getParticipantInfoViewBean().setEmailDisabled(disabled);
		getParticipantInfoViewBean().setUsernameDisabled(disabled);
		getParticipantInfoViewBean().setPasswordDisabled(disabled);

		if (disabled) {
			FacesContext.getCurrentInstance().addMessage(
					"firstName",
					new FacesMessage(FacesMessage.SEVERITY_INFO, null, "This is an externally managed "
							+ "participant so some of the information cannot be changed."));
		}
	}

	/*
	 * Project management functions
	 */

	public void setProjectId(ActionEvent event) {
		log.debug("IN ParticipantInfoControllerBean.setProjectId()");
	}

	/*
	 * Getters/setters
	 */
	@Override
	public boolean isEditable() {
		return sessionBean.isPermitted("editParticipantInfoFields");
	}

	@Override
	public boolean isViewable() {
		return sessionBean.isPermitted("viewParticipantInfoFields");
	}

	public ParticipantInfoViewBean getParticipantInfoViewBean() {
		return participantInfoViewBean;
	}

	public void setParticipantInfoViewBean(ParticipantInfoViewBean participantInfoViewBean) {
		this.participantInfoViewBean = participantInfoViewBean;
	}

	public UserDocumentTypeDao getUserDocumentTypeDao() {
		return userDocumentTypeDao;
	}

	public void setUserDocumentTypeDao(UserDocumentTypeDao userDocumentTypeDao) {
		this.userDocumentTypeDao = userDocumentTypeDao;
	}

	@Override
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	@Override
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User user) {
		this.currentUser = user;
	}

	public UserDocumentsViewBean getUserDocumentsViewBean() {
		return userDocumentsViewBean;
	}

	public void setUserDocumentsViewBean(UserDocumentsViewBean userDocumentsViewBean) {
		this.userDocumentsViewBean = userDocumentsViewBean;
	}

	public UserDocumentsControllerBean getUserDocumentsControllerBean() {
		return userDocumentsControllerBean;
	}

	public void setUserDocumentsControllerBean(UserDocumentsControllerBean userDocumentsControllerBean) {
		this.userDocumentsControllerBean = userDocumentsControllerBean;
	}

	public void setAppConfigProperties(Properties appConfigProperties) {
		this.appConfigProperties = appConfigProperties;
	}

	public Properties getAppConfigProperties() {
		return appConfigProperties;
	}

	public void setEnrollmentService(EnrollmentServiceLocalImpl enrollmentService) {
		this.enrollmentService = enrollmentService;
	}

	public UsernameServiceLocal getUsernameService() {
		return usernameService;
	}

	public void setUsernameService(UsernameServiceLocal usernameService) {
		this.usernameService = usernameService;
	}

	public EnrollmentServiceLocalImpl getEnrollmentService() {
		return enrollmentService;
	}

	public ProjectCourseDao getProjectCourseDao() {
		return projectCourseDao;
	}

	public void setProjectCourseDao(ProjectCourseDao projectCourseDao) {
		this.projectCourseDao = projectCourseDao;
	}

	public UffdaService getUffdaService() {
		return uffdaService;
	}

	public void setUffdaService(UffdaService uffdaService) {
		this.uffdaService = uffdaService;
	}

	public ConsentHelper getConsentHelper() {
		return consentHelper;
	}

	public void setConsentHelper(ConsentHelper consentHelper) {
		this.consentHelper = consentHelper;
	}

}
