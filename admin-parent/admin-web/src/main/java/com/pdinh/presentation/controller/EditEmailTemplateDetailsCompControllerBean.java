package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.manage.emailtemplates.EmailTemplatesService;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.helper.TextUtils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.EditEmailTemplateDetailsCompViewBean;

@ManagedBean(name = "editEmailTemplateDetailsCompControllerBean")
@ViewScoped
public class EditEmailTemplateDetailsCompControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	final static Logger log = LoggerFactory.getLogger(EditEmailTemplateDetailsCompControllerBean.class);

	@EJB
	private EmailTemplatesService emailTemplatesService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "editEmailTemplateDetailsCompViewBean", value = "#{editEmailTemplateDetailsCompViewBean}")
	private EditEmailTemplateDetailsCompViewBean editEmailTemplateDetailsCompViewBean;

	private boolean newTemplate = false;

	public void resetEmailTemplateData() {

		int companyId = sessionBean.getCompany().getCompanyId();

		getEditEmailTemplateDetailsCompViewBean().setTemplateTitle(null);

		if (editEmailTemplateDetailsCompViewBean.getTemplateType() == null) {
			editEmailTemplateDetailsCompViewBean.setTemplateTypes(emailTemplatesService.getTemplateProducts(companyId));
			log.debug(">>>>>>>>>>>>>>>>>> Setting template type ...");
		}

		if (editEmailTemplateDetailsCompViewBean.getTemplateConstants() == null) {
			editEmailTemplateDetailsCompViewBean.setTemplateConstants(emailTemplatesService.getTemplateStyles());
			log.debug(">>>>>>>>>>>>>>>>>> Setting constants  ...");
		}

		if (editEmailTemplateDetailsCompViewBean.getSenderKeyList() == null) {
			editEmailTemplateDetailsCompViewBean.setSenderKeyList(emailTemplatesService.getEmailSenderKeys());
			log.debug(">>>>>>>>>>>>>>>>>> Setting sender keys  ...");
		}

		if (editEmailTemplateDetailsCompViewBean.getCopyPasteKeywords() == null) {
			editEmailTemplateDetailsCompViewBean.setCopyPasteKeywords(emailTemplatesService.getEmailMergeFields());
			log.debug(">>>>>>>>>>>>>>>>>> Setting merge fields  ...");
		}

		editEmailTemplateDetailsCompViewBean.setSelectedTemplateType("-1");
		editEmailTemplateDetailsCompViewBean.setSelectedConstantType("-1");

		editEmailTemplateDetailsCompViewBean.setEmailRuleNames(new ArrayList<String>());
		editEmailTemplateDetailsCompViewBean.setEditEmailSubject(null);
		editEmailTemplateDetailsCompViewBean.setEditEmailBody(null);
		editEmailTemplateDetailsCompViewBean.setSelectedLanguage("en");
		editEmailTemplateDetailsCompViewBean.setSelectedSenderKey("-1");
		editEmailTemplateDetailsCompViewBean.setRenderedCustomFields(false);
		editEmailTemplateDetailsCompViewBean.setCustomEmailFrom(null);
		editEmailTemplateDetailsCompViewBean.setCustomEmailReplyTo(null);

		editEmailTemplateDetailsCompViewBean.setRenderTemplateTypesList(false);
		editEmailTemplateDetailsCompViewBean.setRenderTemplateConstantsList(false);
		editEmailTemplateDetailsCompViewBean.setRenderTemplateTypeOutput(false);
		editEmailTemplateDetailsCompViewBean.setRenderedIncludedInRules(false);

	}

	private String findLabel(List<ListItemObj> items, String itemToFind) {
		for (ListItemObj item : items) {
			if (item.getId().equals(itemToFind)) {
				return item.getName();
			}
		}
		return null;
	}

	public void iniForNew() {

		resetEmailTemplateData();
		setNewTemplate(true);

		editEmailTemplateDetailsCompViewBean.setRenderTemplateTypesList(true);
		editEmailTemplateDetailsCompViewBean.setRenderTemplateConstantsList(true);
		editEmailTemplateDetailsCompViewBean.setTemplateLanguages(emailTemplatesService.getTemplateLanguges(null));
		editEmailTemplateDetailsCompViewBean.setDisabledAddTranslation(true);
		editEmailTemplateDetailsCompViewBean.setDisabledDeleteTranslation(true);

	}

	public void initForEdit(EmailTemplateObj template) {

		resetEmailTemplateData();
		setNewTemplate(false);

		log.debug(">>>>>>>>>>>>>>>>>>>>> initForEdit template = {}", template);

		editEmailTemplateDetailsCompViewBean.setTemplateTitle(template.getTitle());
		editEmailTemplateDetailsCompViewBean.setTemplateType(template.getEmailTypeProdLabel());
		editEmailTemplateDetailsCompViewBean.setRenderTemplateTypeOutput(true);
		editEmailTemplateDetailsCompViewBean.setSelectedTemplateType(template.getType());
		editEmailTemplateDetailsCompViewBean.setSelectedConstantType(template.getStyle());

		if (template.getFromKeyCode() == null) {
			template.setFromKeyCode(emailTemplatesService.getEmailSenderKeys().get(0).getName());
		} else if (template.getFromKeyCode().equals("CUSTOM")) {
			editEmailTemplateDetailsCompViewBean.setCustomEmailFrom(template.getCustomFrom());
			editEmailTemplateDetailsCompViewBean.setCustomEmailReplyTo(template.getCustomReplyTo());
			editEmailTemplateDetailsCompViewBean.setRenderedCustomFields(true);
		}

		editEmailTemplateDetailsCompViewBean.setSelectedSenderKey(template.getFromKeyCode());

		List<String> templateIsUsedInEmailRules = emailTemplatesService.getEmailRuleNamesByTemplate(template);

		if (!templateIsUsedInEmailRules.isEmpty()) {
			getEditEmailTemplateDetailsCompViewBean().setEmailRuleNames(templateIsUsedInEmailRules);
			editEmailTemplateDetailsCompViewBean.setRenderedIncludedInRules(true);
		}

		editEmailTemplateDetailsCompViewBean.setTemplateLanguages(emailTemplatesService.getTemplateLanguges(template));
		editEmailTemplateDetailsCompViewBean.setDisabledDeleteTranslation(true);

		EmailTextObj defaultTextObj = emailTemplatesService.findEmailTextByLangCode(template, "en");
		editEmailTemplateDetailsCompViewBean.setEditEmailSubject(defaultTextObj.getSubject());
		editEmailTemplateDetailsCompViewBean.setEditEmailBody(TextUtils.getHtmlText(defaultTextObj.getEmailText()));
		editEmailTemplateDetailsCompViewBean.setSelectedLanguage(defaultTextObj.getLangCode());
		editEmailTemplateDetailsCompViewBean.setCurrentText(defaultTextObj);
		editEmailTemplateDetailsCompViewBean.setDisabledAddTranslation(false);
		editEmailTemplateDetailsCompViewBean.setCurrentTemplate(template);
	}

	public void handleLanguageChange(AjaxBehaviorEvent evt) {
		EmailTemplateObj template = editEmailTemplateDetailsCompViewBean.getCurrentTemplate();
		String currentLanguage = editEmailTemplateDetailsCompViewBean.getSelectedLanguage();
		EmailTextObj currentText = emailTemplatesService.findEmailTextByLangCode(template, currentLanguage);
		editEmailTemplateDetailsCompViewBean.setEditEmailSubject(currentText.getSubject());
		editEmailTemplateDetailsCompViewBean.setEditEmailBody(TextUtils.getHtmlText(currentText.getEmailText()));
		editEmailTemplateDetailsCompViewBean.setCurrentText(currentText);
		if (currentLanguage.equals("en")) {
			editEmailTemplateDetailsCompViewBean.setDisabledDeleteTranslation(true);
		} else {
			editEmailTemplateDetailsCompViewBean.setDisabledDeleteTranslation(false);
		}
	}

	public void handleSenderKeyChange(AjaxBehaviorEvent evt) {
		String senderKey = editEmailTemplateDetailsCompViewBean.getSelectedSenderKey();
		if (senderKey.equals("CUSTOM")) {
			editEmailTemplateDetailsCompViewBean.setRenderedCustomFields(true);
		} else {
			editEmailTemplateDetailsCompViewBean.setRenderedCustomFields(false);
		}
	}

	public void saveTextChanges() {
		EmailTextObj currentText = editEmailTemplateDetailsCompViewBean.getCurrentText();
		if (currentText != null) {
			currentText.setSubject(editEmailTemplateDetailsCompViewBean.getEditEmailSubject());
			currentText.setEmailText(editEmailTemplateDetailsCompViewBean.getEditEmailBody());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>> saveTextChanges getSubject {}",
					editEmailTemplateDetailsCompViewBean.getCurrentText().getSubject());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>> saveTextChanges getEmailText {}",
					editEmailTemplateDetailsCompViewBean.getCurrentText().getEmailText());
		} else {
			log.warn("currentText is null in saveTextChanges method");
		}
	}

	public void initAddTranslation() {
		EmailTemplateObj template = editEmailTemplateDetailsCompViewBean.getCurrentTemplate();
		editEmailTemplateDetailsCompViewBean.setNewLangs(emailTemplatesService.getUnusedLanguages(template));
		if (!editEmailTemplateDetailsCompViewBean.getNewLangs().isEmpty()) {
			editEmailTemplateDetailsCompViewBean.setDisabledNewLangs(false);
		} else {
			FacesContext.getCurrentInstance().addMessage("newLangs",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No new languages available.", ""));
			editEmailTemplateDetailsCompViewBean.setDisabledNewLangs(true);
		}

	}

	public void addTranslation(ActionEvent event) {
		EmailTemplateObj template = editEmailTemplateDetailsCompViewBean.getCurrentTemplate();
		String newLanguage = editEmailTemplateDetailsCompViewBean.getNewLangCode();
		template.getEmailText().add(new EmailTextObj("", "", newLanguage));
		editEmailTemplateDetailsCompViewBean.setDisabledDeleteTranslation(false);
		editEmailTemplateDetailsCompViewBean.setDisabledNewLangs(false);
		refreshEmailContent(template, newLanguage);
	}

	public void deleteTranslation(ActionEvent event) {
		EmailTemplateObj template = editEmailTemplateDetailsCompViewBean.getCurrentTemplate();
		String currentLanguage = editEmailTemplateDetailsCompViewBean.getSelectedLanguage();
		EmailTextObj currentText = emailTemplatesService.findEmailTextByLangCode(template, currentLanguage);
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>> deleteTranslation currentText {}", currentText);

		for (EmailTextObj text : template.getEmailText()) {
			if (currentText.getLangCode().equals(text.getLangCode())) {
				template.getEmailText().remove(currentText);
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>> deleteTranslation currentText.langCode {}",
						currentText.getLangCode());
				break;
			}
		}
		editEmailTemplateDetailsCompViewBean.setDisabledDeleteTranslation(true);
		refreshEmailContent(template, "en");
	}

	private void refreshEmailContent(EmailTemplateObj template, String language) {
		EmailTextObj text = emailTemplatesService.findEmailTextByLangCode(template, language);
		editEmailTemplateDetailsCompViewBean.setTemplateLanguages(emailTemplatesService.getTemplateLanguges(template));
		editEmailTemplateDetailsCompViewBean.setSelectedLanguage(language);
		editEmailTemplateDetailsCompViewBean.setEditEmailSubject(text.getSubject());
		editEmailTemplateDetailsCompViewBean.setEditEmailBody(text.getEmailText());
		editEmailTemplateDetailsCompViewBean.setCurrentText(text);
	}

	private EmailTemplateObj setFields(EmailTemplateObj template) {

		List<EmailTextObj> textObjs = new ArrayList<EmailTextObj>();

		log.debug(">>>>>>>>>>>>>>>>>>>>> setFields template = {}", template);
		log.debug(">>>>>>>>>>>>>>>>>>>>> setFields getStyle = {}",
				editEmailTemplateDetailsCompViewBean.getSelectedConstantType());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setFields getType = {}",
				editEmailTemplateDetailsCompViewBean.getSelectedTemplateType());

		template.setTitle(editEmailTemplateDetailsCompViewBean.getTemplateTitle());
		template.setType(editEmailTemplateDetailsCompViewBean.getSelectedTemplateType());
		template.setStyle(editEmailTemplateDetailsCompViewBean.getSelectedConstantType());
		template.setConstantStyleLabel(findLabel(editEmailTemplateDetailsCompViewBean.getTemplateConstants(),
				editEmailTemplateDetailsCompViewBean.getSelectedConstantType()));
		template.setEmailTypeProdLabel(findLabel(editEmailTemplateDetailsCompViewBean.getTemplateTypes(),
				editEmailTemplateDetailsCompViewBean.getSelectedTemplateType()));
		template.setActive(true);
		log.debug("IS this a Template? {}", template.getIsTemplate());
		// template.setIsTemplate(template.getIsTemplate());
		if (isNewTemplate()) {
			template.setLevel(sessionBean.getTemplateLevel());
		}
		template.setFromKeyCode(editEmailTemplateDetailsCompViewBean.getSelectedSenderKey());
		template.setCustomFrom(editEmailTemplateDetailsCompViewBean.getCustomEmailFrom());
		template.setCustomReplyTo(editEmailTemplateDetailsCompViewBean.getCustomEmailReplyTo());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setFields getEmailText = {}", template.getEmailText());

		// Should be used only in case when creating to Create New Template.
		if (template.getEmailText() == null) {
			textObjs.add(new EmailTextObj(editEmailTemplateDetailsCompViewBean.getEditEmailSubject(),
					cleanText(editEmailTemplateDetailsCompViewBean.getEditEmailBody()),
					editEmailTemplateDetailsCompViewBean.getSelectedLanguage()));
			log.debug(">>>>>>>>>>>>>>>>>>>>> setFields textObjs.size = {}", textObjs.size());
			template.setEmailText(textObjs);
		} else {
			for (EmailTextObj text : template.getEmailText()) {
				text.setEmailText(cleanText(text.getEmailText()));
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setFields text.lang = {}", text.getLangCode());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setFields text.subject = {}", text.getSubject());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setFields text.text = {}", text.getEmailText());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			}

		}

		return template;
	}

	public void handleCreateTemplate() {
		EmailTemplateObj template = new EmailTemplateObj();
		createTemplate(template, sessionBean.getTemplateLevel());
	}

	public void createTemplate(EmailTemplateObj template, String level) {
		if (!validateEmailTemplate()) {
			template = setFields(template);
			emailTemplatesService
					.createNewTemplate(template, level, sessionBean.getCompany(), sessionBean.getProject());
		} else {
			log.error(">>>>>>>> Validation failed skipping create template ...");
		}
	}

	public void handleUpdateTemplate() {
		EmailTemplateObj template = editEmailTemplateDetailsCompViewBean.getCurrentTemplate();
		updateTemplate(template, null);
	}

	public void updateTemplate(EmailTemplateObj template, String level) {
		if (!validateEmailTemplate()) {
			template = setFields(template);
			emailTemplatesService.updateTemplate(template, level);
		} else {
			log.error(">>>>>>>> Validation failed skipping update template ...");
		}
	}

	private boolean validateEmailTemplate() {
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rc = RequestContext.getCurrentInstance();
		boolean error = false;

		if (this.getEditEmailTemplateDetailsCompViewBean().getTemplateTitle().isEmpty()) {
			fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Name required.", ""));
			log.error("Template Name required.");
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		if (isNewTemplate()) {
			if (editEmailTemplateDetailsCompViewBean.getRenderTemplateConstantsList()) {
				if (editEmailTemplateDetailsCompViewBean.getSelectedTemplateType() != null) {
					if (editEmailTemplateDetailsCompViewBean.getSelectedTemplateType().equals("-1")) {
						fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Template Product required.", ""));
						log.error("Template Product required.");
						rc.addCallbackParam("notValid", true);
						error = true;
					}
				}
			}
		}

		if (isNewTemplate()) {
			if (editEmailTemplateDetailsCompViewBean.getSelectedConstantType().equals("-1")) {
				fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Style required.",
						""));
				log.error("Template Style required.");
				rc.addCallbackParam("notValid", true);
				error = true;
			}
		}

		if (editEmailTemplateDetailsCompViewBean.getSelectedSenderKey().equals("-1")) {
			fc.addMessage("contentPanel",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Sender required.", ""));
			log.error("Template Sender required.");
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		if (editEmailTemplateDetailsCompViewBean.getSelectedSenderKey().equals("CUSTOM")) {

			if (editEmailTemplateDetailsCompViewBean.getCustomEmailFrom().equals("")) {
				fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Custom From address required.", ""));
				log.error("Custom From address required.");
				rc.addCallbackParam("notValid", true);
				error = true;
			}

			if (editEmailTemplateDetailsCompViewBean.getCustomEmailReplyTo().isEmpty()) {
				fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Custom Reply To address required.", ""));
				log.error("Custom Reply To address required.");
				rc.addCallbackParam("notValid", true);
				error = true;
			}
		}

		if (editEmailTemplateDetailsCompViewBean.getEditEmailSubject().isEmpty()) {
			fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Subject required.",
					""));
			log.error("Template Subject required.");
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		if (editEmailTemplateDetailsCompViewBean.getEditEmailBody().isEmpty()) {
			fc.addMessage("contentPanel", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Template Body required.", ""));
			log.error("Template Body required.");
			rc.addCallbackParam("notValid", true);
			error = true;
		}

		return error;
	}

	private String cleanText(String cleanMe) {
		return cleanMe.replaceAll("(<span)[^>]*(>)", "$1$2");
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public EditEmailTemplateDetailsCompViewBean getEditEmailTemplateDetailsCompViewBean() {
		return editEmailTemplateDetailsCompViewBean;
	}

	public void setEditEmailTemplateDetailsCompViewBean(
			EditEmailTemplateDetailsCompViewBean editEmailTemplateDetailsCompViewBean) {
		this.editEmailTemplateDetailsCompViewBean = editEmailTemplateDetailsCompViewBean;
	}

	public boolean isNewTemplate() {
		return newTemplate;
	}

	public void setNewTemplate(boolean newTemplate) {
		this.newTemplate = newTemplate;
	}

}
