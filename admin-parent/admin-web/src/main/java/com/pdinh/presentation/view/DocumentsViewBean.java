package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.DocumentObj;
import com.pdinh.presentation.domain.FileAttachmentObj;
import com.pdinh.presentation.domain.ListItemObj;

@ManagedBean(name="documentsViewBean")
@ViewScoped
public abstract class DocumentsViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<DocumentObj> documents = new ArrayList<DocumentObj>();
	private DocumentObj[] selectedDocuments;
	private DocumentObj newDocument;
	private Integer newDocumentType;
	private String newDocumentDescription;
	private Boolean newDocumentIsIntegrationGrid;
	private List<ListItemObj> documentTypes;
	private Boolean removeDocumentsDisabled = true;
	private FileAttachmentObj lastDocumentFileUploaded;
	private Boolean addDocumentDisabled = true;	
	private String username;
	private int users_id;
	private Boolean doSave = false;
	
	public void clearNewDocumentData() {
		newDocumentDescription = "";
		newDocumentIsIntegrationGrid = false;
		newDocumentType = null;
		lastDocumentFileUploaded = new FileAttachmentObj();
		addDocumentDisabled = true;
	}
	
	public List<DocumentObj> getDocuments() {
		return documents;
	}
	public void setDocuments(List<DocumentObj> documents) {
		this.documents = documents;
	}
	public DocumentObj[] getSelectedDocuments() {
		return selectedDocuments;
	}
	public void setSelectedDocuments(DocumentObj[] selectedDocuments) {
		this.selectedDocuments = selectedDocuments;
	}
	public DocumentObj getNewDocument() {
		return newDocument;
	}
	public void setNewDocument(DocumentObj newDocument) {
		this.newDocument = newDocument;
	}
	public Integer getNewDocumentType() {
		return newDocumentType;
	}
	public void setNewDocumentType(Integer newDocumentType) {
		this.newDocumentType = newDocumentType;
	}
	public String getNewDocumentDescription() {
		return newDocumentDescription;
	}
	public void setNewDocumentDescription(String newDocumentDescription) {
		this.newDocumentDescription = newDocumentDescription;
	}
	public Boolean getNewDocumentIsIntegrationGrid() {
		return newDocumentIsIntegrationGrid;
	}
	public void setNewDocumentIsIntegrationGrid(Boolean newDocumentIsIntegrationGrid) {
		this.newDocumentIsIntegrationGrid = newDocumentIsIntegrationGrid;
	}
	public List<ListItemObj> getDocumentTypes() {
		return documentTypes;
	}
	public void setDocumentTypes(List<ListItemObj> documentTypes) {
		this.documentTypes = documentTypes;
	}
	public Boolean getRemoveDocumentsDisabled() {
		return removeDocumentsDisabled;
	}
	public void setRemoveDocumentsDisabled(Boolean removeDocumentsDisabled) {
		this.removeDocumentsDisabled = removeDocumentsDisabled;
	}
	public FileAttachmentObj getLastDocumentFileUploaded() {
		return lastDocumentFileUploaded;
	}
	public void setLastDocumentFileUploaded(
			FileAttachmentObj lastDocumentFileUploaded) {
		this.lastDocumentFileUploaded = lastDocumentFileUploaded;
	}
	public Boolean getAddDocumentDisabled() {
		return addDocumentDisabled;
	}
	public void setAddDocumentDisabled(Boolean addDocumentDisabled) {
		this.addDocumentDisabled = addDocumentDisabled;
	}
	
	public String getDocumentNameByType(int type) {
		for(ListItemObj obj : documentTypes) {
			if(obj.getType() == type) {
				return obj.getName();
			}
		}
		return "";
	}
	
	public void clear() {
		newDocument = null;
		newDocumentType = -1;
		newDocumentDescription = "";
		documentTypes = new ArrayList();
		removeDocumentsDisabled = true;
		lastDocumentFileUploaded = new FileAttachmentObj();
		selectedDocuments = null;
		documents = new ArrayList();						
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getUsers_id() {
		return users_id;
	}

	public void setUsers_id(int users_id) {
		this.users_id = users_id;
	}

	public Boolean getDoSave() {
		return doSave;
	}

	public void setDoSave(Boolean doSave) {
		this.doSave = doSave;
	}
}
