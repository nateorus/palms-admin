package com.pdinh.presentation.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	public static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MMM-yyyy");
	public static SimpleDateFormat FULL_MONTH_DATE_FORMATTER = new SimpleDateFormat("yyyy-MMMMM-dd");
	public static SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("MMM dd yyyy HH:mm:ss");

	public static String getFormattedDate(Date date) {
		if (date != null) {
			return DATE_FORMATTER.format(date);
		}
		return "";
	}

	public static String getFormattedDate_FullMonth(Date date) {
		if (date != null) {
			return FULL_MONTH_DATE_FORMATTER.format(date);
		}
		return "";
	}

	public static String getFormattedDate(Date date, Locale locale) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy", locale);
		if (date != null) {
			return dateFormat.format(date);
		}
		return "";
	}

	public static String getFormattedDateTime(Date date) {
		if (date != null) {
			return DATE_FORMATTER.format(date);
		}
		return "";
	}

	public static String getFormattedDateTime(Date date, Locale locale) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm:ss", locale);
		if (date != null) {
			return dateFormat.format(date);
		}
		return "";
	}

	public static Date parseDate(String dateStr) {
		Date myDate = null;
		try {
			myDate = DateUtil.DATE_FORMATTER.parse(dateStr);
		} catch (ParseException e) {
		}
		return myDate;
	}

}
