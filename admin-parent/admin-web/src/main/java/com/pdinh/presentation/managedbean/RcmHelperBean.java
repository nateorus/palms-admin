package com.pdinh.presentation.managedbean;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.pdinh.data.ms.dao.CoachingPlanDao;
import com.pdinh.persistence.ms.entity.CoachingPlan;

@ManagedBean(name = "rcmHelperBean")
@SessionScoped
public class RcmHelperBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private CoachingPlanDao coachingPlanDao;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	public void generateCoachingPlanRcm() {
		if (sessionBean.getProject() != null && sessionBean.getUser() != null) {
			CoachingPlan coachingPlan = coachingPlanDao.findByProjectAndParticipant(sessionBean.getProject()
					.getProjectId(), sessionBean.getUser().getUsersId());
			if (coachingPlan != null) {
				try {
					String urlStr = configProperties.getProperty("coachingPlanRcmGenerationUrl") + "?coachingPlanId="
							+ coachingPlan.getCoachingPlanId();
					URL url = new URL(urlStr);

					System.out.println("Generation coaching plan RCM: " + urlStr);

					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					int code = conn.getResponseCode();

					if (code != HttpURLConnection.HTTP_OK) {
						System.out.println("RCM Generation failed for coaching plan id: "
								+ coachingPlan.getCoachingPlanId() + " error code: " + code);
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public CoachingPlanDao getCoachingPlanDao() {
		return coachingPlanDao;
	}

	public void setCoachingPlanDao(CoachingPlanDao coachingPlanDao) {
		this.coachingPlanDao = coachingPlanDao;
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}
}
