package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.UploadedFile;

import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;

@ManagedBean
@ViewScoped
public class ReportUploadViewBean implements Serializable {

	private static final long serialVersionUID = -4488556734695427958L;
	private List<ReportTypeObj> reportTypes = new ArrayList<ReportTypeObj>();
	private String reportCode;
	private String requestName;
	private UploadedFile uploadedFile;
	private List<ParticipantObj> participants;
	private String targetLevel;
	private List<TargetLevelTypeObj> targetLevels = new ArrayList<TargetLevelTypeObj>();
	private String reportLanguage;
	private List<LanguageObj> reportLanguages = new ArrayList<LanguageObj>();
	private int reportRequestId;

	public void init() {
		setReportCode("");
		// setRequestName("");
		setUploadedFile(null);
		setReportTypes(new ArrayList<ReportTypeObj>());
		setTargetLevels(new ArrayList<TargetLevelTypeObj>());
		setParticipants(new ArrayList<ParticipantObj>());
	}

	public List<ReportTypeObj> getReportTypes() {
		return reportTypes;
	}

	public void setReportTypes(List<ReportTypeObj> reportTypes) {
		this.reportTypes = reportTypes;
	}

	public String getReportCode() {
		return reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String reportName) {
		this.requestName = reportName;
	}

	public boolean isUploadedFilePresent() {
		return uploadedFile != null;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getUploadedFilename() {
		if (uploadedFile != null) {
			return FilenameUtils.getName(uploadedFile.getFileName());
		}
		return "";
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

	public List<TargetLevelTypeObj> getTargetLevels() {
		return targetLevels;
	}

	public void setTargetLevels(List<TargetLevelTypeObj> targetLevels) {
		this.targetLevels = targetLevels;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public List<LanguageObj> getReportLanguages() {
		return reportLanguages;
	}

	public void setReportLanguages(List<LanguageObj> reportLanguages) {
		this.reportLanguages = reportLanguages;
	}

	public String getReportLanguage() {
		return reportLanguage;
	}

	public void setReportLanguage(String reportLanguage) {
		this.reportLanguage = reportLanguage;
	}

	public int getReportRequestId() {
		return reportRequestId;
	}

	public void setReportRequestId(int reportRequestId) {
		this.reportRequestId = reportRequestId;
	}

}
