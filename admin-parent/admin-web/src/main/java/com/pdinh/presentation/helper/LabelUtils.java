package com.pdinh.presentation.helper;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LabelUtils {

	private static final Logger log = LoggerFactory.getLogger(LabelUtils.class);

	public static void addMessage(String fieldName, String message) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, message, "");
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addMessage(String fieldName, String message, String detail) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, message, detail);
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addMessage(String fieldName, String message, Object... params) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, getFormatted(message, params), "");
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addWarningMessage(String fieldName, String message, String detail) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, message, detail);
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addWarningMessage(String fieldName, String message, Object... params) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, getFormatted(message, params), "");
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addErrorMessage(String fieldName, String message) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, "");
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addErrorMessage(String fieldName, String message, String detail) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, detail);
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static void addFormattedErrorMessage(String fieldName, String message, Object... params) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, getFormatted(message, params), "");
		FacesContext.getCurrentInstance().addMessage(fieldName, msg);
	}

	public static String getFormatted(String messageText, Object... params) {
		return String.format(messageText, params);
	}

	public static String getLabel(String messageText, boolean required) {
		return messageText + ": " + (required ? "*" : "");
	}

	public static String getText(String key) {
		if (StringUtils.isBlank(key)) {
			return "";
		}

		ResourceBundle bundle = ResourceBundle.getBundle("com.kf.palms.admin.messages");
		if (!bundle.containsKey(key)) {
			return key;
		}

		String messageText = bundle.getString(key.trim());
		if (messageText == null) {
			messageText = key;
		}

		return messageText;
	}
}
