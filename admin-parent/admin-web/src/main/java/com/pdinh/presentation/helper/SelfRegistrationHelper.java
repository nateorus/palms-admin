package com.pdinh.presentation.helper;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.admin.SelfRegConfigRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.rest.security.AuthorizationClientFilter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HttpURLConnectionFactory;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;

@ManagedBean
@ViewScoped
public class SelfRegistrationHelper implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SelfRegistrationHelper.class);
	private static final Logger auditLog = LoggerFactory.getLogger("AUDIT_ADMIN");

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@Resource(name = "application/config_properties")
	private Properties configProperties;

	private Client client;

	@PostConstruct
	private void init() {
		log.debug("Initializing Jersey Client");
		DefaultClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
		//client = Client.create(config);
		client = new Client(new URLConnectionClientHandler(
		        new HttpURLConnectionFactory() {
		    Proxy p = null;
		    @Override
		    public HttpURLConnection getHttpURLConnection(URL url)
		            throws IOException {
		        if (p == null) {
		            if (configProperties.containsKey("http.proxyHost")){
		            	log.debug("Initializing Proxy for Jersey Client.  Host: {}, Port: {}",
		            			configProperties.getProperty("http.proxyHost"),
		            			configProperties.getProperty("http.proxyPort"));
		                p = new Proxy(Proxy.Type.HTTP,
	                        new InetSocketAddress(
	                        		configProperties.getProperty("http.proxyHost"),
	                        Integer.valueOf(configProperties.getProperty("http.proxyPort"))));
		            } else {
		            	p = Proxy.NO_PROXY;
		            }

		        }
		        return (HttpURLConnection) url.openConnection(p);
		    }
		}), config);
		client.addFilter(new AuthorizationClientFilter(configProperties));
	}

	public SelfRegConfigRepresentation getCreateSelfRegConfig(int projectId){
		Subject subject = SecurityUtils.getSubject();
		String urlString = "/selfReg/getCreateSelfRegConfigByProjectId/" + projectId + "/" + subject.getPrincipal();
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}

		WebResource resource = client.resource(urlString);
		SelfRegConfigRepresentation response = null;
		try {
			response = resource.post(SelfRegConfigRepresentation.class);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());
			return null;

		}
		return response;
	}
	
	public SelfRegConfigRepresentation getSelfRegConfig(int projectId) throws PalmsException{
		String urlString = "/selfReg/getSelfRegConfigByProjectId/" + projectId;
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}

		WebResource resource = client.resource(urlString);
		SelfRegConfigRepresentation response = null;
		try {
			response = resource.post(SelfRegConfigRepresentation.class);

		} catch (UniformInterfaceException uife) {
			log.info("No self reg config exists for this project.  Caught UniformInterfaceException: {}", uife.getMessage());
			throw new PalmsException("No self registration configuration exists for this project.");

		}
		return response;
	}

	public SelfRegConfigRepresentation enableSelfRegistration(int selfRegConfigId, boolean enabled, int registrationLimit){
		Subject subject = SecurityUtils.getSubject();
		String urlString = "/selfReg/updateProjectSelfRegConfig/" + selfRegConfigId + "/" + registrationLimit + "/" + enabled + "/" + subject.getPrincipal();
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}

		WebResource resource = client.resource(urlString);
		SelfRegConfigRepresentation response = null;
		try {
			response = resource.post(SelfRegConfigRepresentation.class);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());
			return null;

		}
		/*
		Subject subject = SecurityUtils.getSubject();
		
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Enabled is {}, Reg Limit is {}.", 
				new Object[]{subject.getPrincipal(), EntityAuditLogger.OP_UPDATE_SELF_REG_CONFIG, 
					sessionBean.getCompany().getCoName(), sessionBean.getCompany().getCompanyId(),
					sessionBean.getSelectedProject(), sessionBean.getProject().getName(),
					enabled, registrationLimit});
					*/
		return response;
	}
	/**
	 * This method adds the provided domainStrings to the list of allowed domains for the Self Registration Configuration.
	 * It does not recognize duplicates or remove existing strings not found in the supplied list.  Duplicates should
	 * be removed programatically before calling this method or by calling the removeAllowedDomain/ method
	 * @param selfRegConfigId
	 * @param domainStrings
	 * @return
	 */

	public SelfRegConfigRepresentation addAllowedDomains(int selfRegConfigId, List<String> domainStrings){
		Subject subject = SecurityUtils.getSubject();
		String urlString = "/selfReg/addAllowedDomains/" + selfRegConfigId + "/" + subject.getPrincipal();
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		//for audit log
		String domainString = "";
		for (String domain : domainStrings){
			urlString = urlString + "/" + domain;
			domainString = domainString + ", " + domain;
		}

		WebResource resource = client.resource(urlString);
		SelfRegConfigRepresentation response = null;
		try {
			response = resource.post(SelfRegConfigRepresentation.class);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());
			return null;

		}
		/*
		Subject subject = SecurityUtils.getSubject();
		
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Added allowed domains: {}", 
				new Object[]{subject.getPrincipal(), EntityAuditLogger.OP_UPDATE_SELF_REG_CONFIG, 
					sessionBean.getCompany().getCoName(), sessionBean.getCompany().getCompanyId(),
					sessionBean.getSelectedProject(), sessionBean.getProject().getName(),
					domainString});
					*/
		return response;
	}
	/**
	 * This method removes the specified domains from the allowed email domain list of this self Registration Configuration.
	 * The domains to remove are specified by the ids in the list passed to the method.  The ids are the ids of
	 * the SelfRegListItem previously persisted for this configuration.
	 * @param selfRegConfigId
	 * @param allowedDomainIds
	 * @return
	 */

	public SelfRegConfigRepresentation removeAllowedDomains(int selfRegConfigId, List<Integer> allowedDomainIds){
		Subject subject = SecurityUtils.getSubject();
		String urlString = "/selfReg/removeAllowedDomains/" + selfRegConfigId + "/" + subject.getPrincipal();
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		String domainString = "";
		for (Integer domain : allowedDomainIds){
			urlString = urlString + "/" + domain;
			domainString = domainString + ", " + domain;
		}

		WebResource resource = client.resource(urlString);
		SelfRegConfigRepresentation response = null;
		try {
			response = resource.post(SelfRegConfigRepresentation.class);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());
			return null;

		}
		/*
		Subject subject = SecurityUtils.getSubject();
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Removed allowed domains: {}", 
				new Object[]{subject.getPrincipal(), EntityAuditLogger.OP_UPDATE_SELF_REG_CONFIG, 
					sessionBean.getCompany().getCoName(), sessionBean.getCompany().getCompanyId(),
					sessionBean.getSelectedProject(), sessionBean.getProject().getName(),
					domainString});
		*/
		return response;
	}

	public SelfRegConfigRepresentation resetConfigUrl(int selfRegConfigId){
		String urlString = "/selfReg/resetConfigUrl/" + selfRegConfigId;
		urlString = configProperties.getProperty("palmsRestApiHost") + URLContextConstants.ADMIN_REST_CONTEXT + urlString;
		if (!urlString.startsWith("http")){
			urlString = "http://" + urlString;
		}
		WebResource resource = client.resource(urlString);
		SelfRegConfigRepresentation response = null;
		try {
			response = resource.post(SelfRegConfigRepresentation.class);

		} catch (UniformInterfaceException uife) {
			log.error("Caught UniformInterfaceException: {}", uife.getMessage());
			return null;

		}
		return response;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
