package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pdinh.presentation.domain.EmailRuleScheduleObj;
import com.pdinh.presentation.domain.EmailRuleScheduleStatusObj;

@ManagedBean
@ViewScoped
public class EmailRuleScheduleStatusViewBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<EmailRuleScheduleStatusObj> statusList;
	private EmailRuleScheduleObj currentSchedule;
	
	public List<EmailRuleScheduleStatusObj> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<EmailRuleScheduleStatusObj> statusList) {
		this.statusList = statusList;
	}

	public EmailRuleScheduleObj getCurrentSchedule() {
		return currentSchedule;
	}

	public void setCurrentSchedule(EmailRuleScheduleObj currentSchedule) {
		this.currentSchedule = currentSchedule;
	}


}
