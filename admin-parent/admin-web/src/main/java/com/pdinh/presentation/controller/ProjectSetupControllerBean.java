package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.component.tabview.Tab;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.persistence.FieldEntityType;
import com.kf.uffda.service.UffdaService;
import com.kf.uffda.vo.CoreFieldGroupVo;
import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.jaxb.lrm.LrmObjectExistResponse;
import com.pdinh.data.jaxb.lrm.LrmObjectServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.CourseFlowTypeDao;
import com.pdinh.data.ms.dao.CourseGroupTemplateDao;
import com.pdinh.data.ms.dao.CourseRosterDao;
import com.pdinh.data.ms.dao.OfficeLocationDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectCourseLangPrefDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectReportDao;
import com.pdinh.data.ms.dao.ProjectTypeDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.data.singleton.ProjectTemplateSingleton;
import com.pdinh.enterprise.managedbean.form.LoginFormBean;
import com.pdinh.mail.RelativeScheduleDateUpdaterLocal;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.CompanyProduct;
import com.pdinh.persistence.ms.entity.CompanyProjectType;
import com.pdinh.persistence.ms.entity.ConfigurationType;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseFlowType;
import com.pdinh.persistence.ms.entity.CourseGroupTemplate;
import com.pdinh.persistence.ms.entity.CourseRoster;
import com.pdinh.persistence.ms.entity.CourseVisibility;
import com.pdinh.persistence.ms.entity.OfficeLocation;
import com.pdinh.persistence.ms.entity.Product;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectCourseLangPref;
import com.pdinh.persistence.ms.entity.ProjectReport;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.InstrumentSelectionObj;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.domain.NonBillableTypeObj;
import com.pdinh.presentation.domain.OfficeLocationObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.domain.ProjectReportObj;
import com.pdinh.presentation.domain.ProjectTemplateRuleObj;
import com.pdinh.presentation.domain.ScoringMethodObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;
import com.pdinh.presentation.helper.TeammemberHelper;
import com.pdinh.presentation.managedbean.ReportContentRepositoryBean;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.AddTeammemberViewBean;
import com.pdinh.presentation.view.ProjectFieldsViewBean;
import com.pdinh.presentation.view.ProjectSetupViewBean;

@ManagedBean(name = "projectSetupControllerBean")
@ViewScoped
public class ProjectSetupControllerBean implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(ProjectSetupControllerBean.class);

	private static final long serialVersionUID = 1L;

	private static final int PREWORK = 1;

	Project currentProject;

	PalmsSubject scheduler;
	PalmsSubject pmPc;

	@Resource(mappedName = "application/config_properties")
	private Properties properties;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private ProjectTypeDao projectTypeDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private CourseGroupTemplateDao courseGroupTemplateDao;

	@EJB
	private CourseDao courseDao;

	@EJB
	private CourseFlowTypeDao courseFlowTypeDao;

	@EJB
	private UserDao userDao;

	@EJB
	private CourseRosterDao courseRosterDao;

	@EJB
	private OfficeLocationDao officeLocationDao;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private GlobalRolesBean rolesBean;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@EJB
	private ProjectSetupSingleton setupSingleton;

	@EJB
	private ProjectCourseLangPrefDao projectCourseLangPrefDao;

	@EJB
	private LanguageSingleton langSingleton;

	@EJB
	private ProjectTemplateSingleton projectTemplateSingleton;

	@EJB
	private LrmObjectServiceLocal lrmObjectServiceLocal;

	@EJB
	private RelativeScheduleDateUpdaterLocal relativeScheduleDateUpdater;

	@EJB
	private UffdaService uffdaService;

	@EJB
	private ProjectReportDao projectReportDao;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "loginFormBean", value = "#{loginFormBean}")
	private LoginFormBean loginFormBean;

	@ManagedProperty(name = "reportContentRepositoryBean", value = "#{reportContentRepositoryBean}")
	private ReportContentRepositoryBean reportContentRepositoryBean;

	@ManagedProperty(name = "projectSetupViewBean", value = "#{projectSetupViewBean}")
	private ProjectSetupViewBean projectSetupViewBean;

	@ManagedProperty(name = "addTeammemberControllerBean", value = "#{addTeammemberControllerBean}")
	private AddTeammemberControllerBean addTeammemberControllerBean;

	@ManagedProperty(name = "addTeammemberViewBean", value = "#{addTeammemberViewBean}")
	private AddTeammemberViewBean addTeammemberViewBean;

	@ManagedProperty(name = "teammemberHelper", value = "#{teammemberHelper}")
	private TeammemberHelper teammemberHelper;

	@ManagedProperty(name = "projectFieldsViewBean", value = "#{projectFieldsViewBean}")
	private ProjectFieldsViewBean projectFieldsViewBean;

	@ManagedProperty(name = "projectFieldsControllerBean", value = "#{projectFieldsControllerBean}")
	private ProjectFieldsControllerBean projectFieldsControllerBean;

	// TODO: Temporary
	private final int NEW_PROJECT = 1;
	private final int SAVE_PROJECT = 2;
	private Integer action = NEW_PROJECT;
	private Boolean firstTime = true;
	private Boolean hasParticipants = false;
	private Boolean inNew = false;

	private boolean clientRolesListExist = false;
	private boolean internalRolesListExist = false;
	private List<ExternalSubjectDataObj> internalRolesList;
	private List<ExternalSubjectDataObj> clientRolesList;

	public void initView() {
		if (firstTime) {
			firstTime = false;
			if (projectSetupViewBean.isCreateProject()) {
				initForNew();
			} else {
				initForEdit();
			}
		}
	}

	public void initForNew() {
		inNew = true;

		action = NEW_PROJECT;

		log.debug("In ProjectSetupControllerBean initForNew ()");

		sessionBean.setProject(null);

		// Should only have to do once
		populateUi();

		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		ProjectType projectType = null;

		handleShowProjectSetup();

		psvb.setProjectName("");
		psvb.setProjectCode("");
		psvb.setDueDate(null);
		psvb.setReportContentManifestRef(null);
		psvb.setRenderReportContentManifests(false);
		psvb.setRenderToggleProjectOpen(false);

		// Pre-loading dropdown lists so the switching
		// between project types will be fast.
		this.populateNoneBillableTypes();
		this.populateAssignedPMs();
		this.populateInternalSchedulers();
		this.populateScoringMethods();
		this.populateCHQLanguages();

		// TODO: Get rid of hard-coded product types
		if (hasProduct(Product.PROD_TLT)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(
					ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES).getProjectTypeId());
			// Default to FLL
			populateInstruments(setupSingleton.findProjectTypeByCode(
					ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES).getProjectTypeId());
			this.renderConfigurationPanel(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES);

		} else if (hasProduct(Product.PROD_ABYD)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					.getProjectTypeId());
			populateInstruments(setupSingleton.findProjectTypeByCode(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					.getProjectTypeId());
			this.renderConfigurationPanel(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE);

		} else if (hasProduct(Product.PROD_GLGPS)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(ProjectType.GLGPS).getProjectTypeId());
			populateInstruments(setupSingleton.findProjectTypeByCode(ProjectType.GLGPS).getProjectTypeId());
			populateReportContentManifests(psvb.getProjectTypeId());
			psvb.setRenderReportContentManifests(true);
			ProjectType pt = setupSingleton.findProjectTypeById(psvb.getProjectTypeId());
			psvb.setReportContentManifestRef(pt.getReportContentManifestRef());
			this.renderConfigurationPanel(ProjectType.GLGPS);

		} else if (hasProduct(Product.PROD_COACH)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(ProjectType.COACH).getProjectTypeId());
			populateInstruments(setupSingleton.findProjectTypeByCode(ProjectType.COACH).getProjectTypeId());
			populateTargetLevelTypes(psvb.getProjectTypeId());

		} else if (hasProduct(Product.PROD_TLTDEMO)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(ProjectType.TLTDEMO).getProjectTypeId());
			// Default to FLL
			populateInstruments(setupSingleton.findProjectTypeByCode(ProjectType.TLTDEMO).getProjectTypeId());
			this.renderConfigurationPanel(ProjectType.TLTDEMO);

		} else if (hasProduct(Product.PROD_VIAEDGE)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(ProjectType.VIAEDGE).getProjectTypeId());
			populateInstruments(setupSingleton.findProjectTypeByCode(ProjectType.VIAEDGE).getProjectTypeId());
			populateTargetLevelTypes(psvb.getProjectTypeId());
			psvb.setRenderAllowReportDownload(true);
			psvb.setEnableAllowReportDownload(true);
			psvb.setViaEDGE(true);

		} else if (hasProduct(Product.PROD_KFP)) {
			psvb.setHasProducts(true);
			psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(ProjectType.KF_ASSMT_POTENTIAL)
					.getProjectTypeId());
			populateInstruments(setupSingleton.findProjectTypeByCode(ProjectType.KF_ASSMT_POTENTIAL).getProjectTypeId());
			ProjectType pt = setupSingleton.findProjectTypeById(psvb.getProjectTypeId());
			// psvb.setRenderAllowReportDownload(true);
			// psvb.setEnableAllowReportDownload(true);
			this.renderConfigurationPanel(ProjectType.KF_ASSMT_POTENTIAL);

		} else if (psvb.getHasCompanyProjectTypes()) {
			psvb.setHasProducts(true);
			// Get the first item in the list that is there (cause
			// hasCompanyProjectTypes was true)
			int pTyp = psvb.getProjectTypes().get(0).getType();
			populateInstruments(pTyp);
			psvb.setProjectTypeId(pTyp);

			// populate target levels if necessary
			ProjectType pt = setupSingleton.findProjectTypeById(pTyp);
			psvb.setRenderTargetLevels(pt.getTargetLevelVisible());
			if (pt.getTargetLevelVisible()) {
				populateTargetLevelTypes(pt.getProjectTypeId());
			}

			// deal with report content manifests
			if (isRenderReportContentManifests(pt.getProjectTypeId())) {
				psvb.setRenderReportContentManifests(true);
				psvb.setReportContentManifestRef(pt.getReportContentManifestRef());
				populateReportContentManifests(pt.getProjectTypeId());
			} else {
				psvb.getReportContentManifestRefs().clear();
			}

		} else {
			psvb.setHasProducts(false);
			psvb.setRenderTargetLevels(false);
			log.debug("This company has no Assessment products or custom project types.");
		}

		projectType = setupSingleton.findProjectTypeById(psvb.getProjectTypeId());

		// Set up product flags for rendering flags
		String projTypeCode = projectType.getProjectTypeCode();
		if (projTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projTypeCode.equals(ProjectType.READINESS_ASSMT_BUL) || projTypeCode.equals(ProjectType.ERICSSON)) {
			psvb.setIsAbydProduct(true);
		} else {
			psvb.setIsAbydProduct(false);
		}
		if (projTypeCode.equals(ProjectType.GLGPS)) {
			psvb.setIsGlGpsProduct(true);
		} else {
			psvb.setIsGlGpsProduct(false);
		}

		psvb.setRenderBillable(projectType.getBillableVisible());
		psvb.setBillable(psvb.isRenderBillable());

		psvb.setRenderIncludeClientLogonIdPassword(true);
		psvb.setIncludeClientLogonIdPassword(false);
		psvb.resetOriginalIncludeClientLogonIdPassword();
		getProjectSetupViewBean().setInternalTeamTabDisabled(true);
		getProjectSetupViewBean().setClientTeamTabDisabled(true);

		psvb.setRenderSaveAndManageDelivery(allowSaveAndManageDelivery(psvb.getProjectTypeId()));

		this.doProjectTypeChange();

		// Setup up the Standard Project Fields
		projectFieldsViewBean.clearFields();
		projectFieldsViewBean.addFields(uffdaService.getStandardFields(sessionBean.getCompany().getCompanyId(),
				CoreFieldGroupVo.PALMS, 0, null, true, true, false, false));
	}

	private void doProjectTypeChange() {
		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		ProjectType projectType = setupSingleton.findProjectTypeById(psvb.getProjectTypeId());
		String projectTypeCode = projectType.getProjectTypeCode();

		log.debug("Changed project type:  {}", psvb.getProjectTypeId());
		log.debug("Got Project Type from singleton: {}", projectTypeCode);

		// Rest options should be done first
		this.resetOptions();

		// Set target level widget render variable
		psvb.setRenderTargetLevels(isRenderTargetLevels(psvb.getProjectTypeId()));

		// Get the proper list of target levels
		this.populateTargetLevelTypes(psvb.getProjectTypeId());

		// Clear the target level type ID
		psvb.setTargetLevelTypeId(setupSingleton.getDefaultTargetLevelTypeId(psvb.getProjectTypeId()));

		psvb.setRenderConfigurationPanel(setupSingleton.isConfigPanelVisible(projectType.getProjectTypeId()));
		psvb.setRenderAssignedPM(setupSingleton.isPmSchedulerSetupVisible(psvb.getProjectTypeId()));
		psvb.setRenderInternalScheduler(setupSingleton.isPmSchedulerSetupVisible(psvb.getProjectTypeId()));
		psvb.setRenderConfigurationSelection(projectType.getConfigurationTypeVisible());
		psvb.setRenderScoringMethod(projectType.getScoringMethodVisible());

		if (projectTypeCode.equals(ProjectType.GLGPS)
				|| projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
				|| projectTypeCode.equals(ProjectType.ERICSSON)) {
			this.populateConfigurationTypes();
			psvb.setInstrumentsVisible(false);
		} else {
			this.populateInstruments(psvb.getProjectTypeId());
		}

		// Set up product flags for rendering flags
		String projTypeCode = projectType.getProjectTypeCode();
		if (projTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projTypeCode.equals(ProjectType.READINESS_ASSMT_BUL) || projTypeCode.equals(ProjectType.ERICSSON)) {
			psvb.setIsAbydProduct(true);
		} else {
			psvb.setIsAbydProduct(false);
		}
		if (projTypeCode.equals(ProjectType.GLGPS)) {
			psvb.setIsGlGpsProduct(true);
		} else {
			psvb.setIsGlGpsProduct(false);
		}

		// Handle report content manifests
		psvb.setRenderReportContentManifests(isRenderReportContentManifests(psvb.getProjectTypeId()));

		// Populate if manifests available
		if (psvb.getRenderReportContentManifests()) {
			populateReportContentManifests(psvb.getProjectTypeId());
			psvb.setReportContentManifestRef(projectType.getReportContentManifestRef());
		} else {
			psvb.setReportContentManifestRef(null);
			psvb.getReportContentManifestRefs().clear();
		}

		psvb.setViaEDGE(projectTypeCode.equalsIgnoreCase(ProjectType.VIAEDGE));
		psvb.setAbyd(projectTypeCode.equalsIgnoreCase(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE));

		psvb.setRenderSaveAndManageDelivery(allowSaveAndManageDelivery(psvb.getProjectTypeId()));
		List<ProjectReportObj> projectReportTemplates = setupSingleton.getProjectReportTemplatesForProjectType(psvb
				.getProjectTypeId());
		Collections.sort(projectReportTemplates);
		psvb.setProjectReportTemplates(projectReportTemplates);

		this.showReportDownloads(false);

		// Billable
		psvb.setRenderBillable(projectType.getBillableVisible());
		psvb.setBillable(psvb.isRenderBillable());

		log.debug(">>>>>> projectType.getBillableVisible() = {}", projectType.getBillableVisible());
		log.debug(">>>>>> projectType.getProjectTypeCode() = {}", projectType.getProjectTypeCode());

		resetValidationFlags();
	}

	public void initForEdit() {
		inNew = false;

		setCurrentProject(getSessionBean().getProject());
		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		String projectTypeCode = getCurrentProject().getProjectType().getProjectTypeCode();

		// Set up product flags for rendering flags
		if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
				|| projectTypeCode.equals(ProjectType.ERICSSON)) {
			psvb.setIsAbydProduct(true);
		} else {
			psvb.setIsAbydProduct(false);
		}
		if (projectTypeCode.equals(ProjectType.GLGPS)) {
			psvb.setIsGlGpsProduct(true);
		} else {
			psvb.setIsGlGpsProduct(false);
		}

		if (getCurrentProject().getProjectUsers().size() > 0) {
			this.setHasParticipants(true);
		} else {
			this.setHasParticipants(false);
		}

		action = SAVE_PROJECT;

		log.debug("In ProjectSetupControllerBean initForEdit()");
		log.debug("Project ID: {}", getCurrentProject().getProjectId());

		// TODO AV: I'm not sure if we need to check or assume if
		// project exist it should have products and company types.
		projectSetupViewBean.setHasProducts(true);
		projectSetupViewBean.setHasCompanyProjectTypes(true);

		// Should only have to do once
		populateUi();

		// Populate metadata
		psvb.setProjectName(getCurrentProject().getName());
		psvb.setProjectCode(getCurrentProject().getProjectCode());
		// psvb.setFromEmail(getCurrentProject().getFromEmail());
		psvb.setDueDate(getCurrentProject().getDueDate());
		psvb.setOriginalDueDate(getCurrentProject().getDueDate());
		psvb.setReportContentManifestRef(getCurrentProject().getReportContentManifestRef());

		psvb.setProjectTypeId(setupSingleton.findProjectTypeByCode(projectTypeCode).getProjectTypeId());
		psvb.setNhnAdminId(getCurrentProject().getAdmin().getUsersId());
		psvb.setParticipantsAdded(false);
		if (getCurrentProject().getUsersCount() > 0) {
			psvb.setParticipantsAdded(true);
		}
		psvb.setDisallowEditProject(false);

		try {
			psvb.setConfigurationTypeId(getCurrentProject().getConfigurationTypeId());
		} catch (Exception e) {
			psvb.setConfigurationTypeId(-1);
		}
		psvb.setScoringMethodId(getCurrentProject().getScoringMethodId());

		// Populate instrument selections
		List<InstrumentSelectionObj> preworkInstruments = new ArrayList<InstrumentSelectionObj>();
		List<InstrumentSelectionObj> assessmentDayInstruments = new ArrayList<InstrumentSelectionObj>();

		List<ProjectCourse> projectCourses = getCurrentProject().getProjectCourses();
		boolean instrumentsVisible = false;

		for (ProjectCourse projectCourse : projectCourses) {
			if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == PREWORK) {
				addInstrumentSelection(preworkInstruments, projectCourse);
			} else {
				addInstrumentSelection(assessmentDayInstruments, projectCourse);
			}
		}

		if (!preworkInstruments.isEmpty() || !assessmentDayInstruments.isEmpty()) {
			instrumentsVisible = true;
		}

		psvb.setPreworkInstruments(preworkInstruments);
		psvb.setAssessmentDayInstruments(assessmentDayInstruments);
		psvb.setInstrumentsVisible(instrumentsVisible);

		// TODO: Code like this is all over the place, need to consolidate

		// Get the proper list of target levels
		populateTargetLevelTypes(psvb.getProjectTypeId());

		// Set the target level if exists
		if (getCurrentProject().getTargetLevelType() != null) {
			psvb.setTargetLevelTypeId(getCurrentProject().getTargetLevelType().getTargetLevelTypeId());
		} else {
			psvb.setTargetLevelTypeId(-1);
		}

		psvb.setRenderConfigurationPanel(setupSingleton.isConfigPanelVisible(getCurrentProject().getProjectType()
				.getProjectTypeId()));

		// Set the target level render flag
		psvb.setRenderTargetLevels(getCurrentProject().getProjectType().getTargetLevelVisible());
		psvb.setRenderConfigurationSelection(getCurrentProject().getProjectType().getConfigurationTypeVisible());
		psvb.setRenderScoringMethod(getCurrentProject().getProjectType().getScoringMethodVisible());

		if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
				|| projectTypeCode.equals(ProjectType.ERICSSON)) {
			psvb.setRenderCHQlang(true);
		} else {
			psvb.setRenderCHQlang(false);
		}

		psvb.setConfigurationTypes(setupSingleton.getConfigurationTypesForProjectType(getCurrentProject()
				.getProjectType(), psvb.getTargetLevelTypeId()));
		this.populateScoringMethods();
		psvb.setConfigurationTypeId(getCurrentProject().getConfigurationTypeId());
		psvb.setScoringMethodId(getCurrentProject().getScoringMethodId());

		// Billable
		psvb.setRenderBillable(getCurrentProject().getProjectType().getBillableVisible());
		psvb.setNoneBillableTypeId(getCurrentProject().getNonBillableTypeId());

		if (getCurrentProject().getBillable() != null) {
			psvb.setBillable(getCurrentProject().getBillable());
			psvb.setRenderNoneBillableTypes(!getCurrentProject().getBillable());
		} else {
			psvb.setBillable(true);
			psvb.setRenderNoneBillableTypes(false);
		}

		// Report content manifests
		psvb.setRenderReportContentManifests(isRenderReportContentManifests(psvb.getProjectTypeId()));

		if (psvb.getRenderReportContentManifests()) {
			populateReportContentManifests(psvb.getProjectTypeId());
		} else {
			psvb.setReportContentManifestRef(null);
			psvb.getReportContentManifestRefs().clear();
		}

		psvb.setRenderToggleProjectOpen(true);
		psvb.setProjectOpen(getCurrentProject().getActive() == 1);

		// delivery office
		OfficeLocation dO = getCurrentProject().getOfficeLocation();
		OfficeLocationObj deliveryOfficeObj = new OfficeLocationObj();
		if (dO != null) {
			deliveryOfficeObj = new OfficeLocationObj(dO.getId(), dO.getLocation());
		}
		psvb.setSelectedOfficeLocationId(deliveryOfficeObj.getOfficeLocationId());

		psvb.setRenderIncludeClientLogonIdPassword(true);
		psvb.setIncludeClientLogonIdPassword(getCurrentProject().isIncludeClientLogonIdPassword());
		psvb.resetOriginalIncludeClientLogonIdPassword();

		// Initializing teammembers

		setClientRolesListExist(false);
		setInternalRolesListExist(false);

		projectSetupViewBean.setInternalTeammembers(new ArrayList<ExternalSubjectDataObj>());
		projectSetupViewBean.setRemoveInternalTeammembers(new ArrayList<ExternalSubjectDataObj>());
		projectSetupViewBean.setInternalTeamTabDisabled(false);

		projectSetupViewBean.setClientTeammembers(new ArrayList<ExternalSubjectDataObj>());
		projectSetupViewBean.setRemoveClientTeammembers(new ArrayList<ExternalSubjectDataObj>());
		projectSetupViewBean.setClientTeamTabDisabled(false);

		projectSetupViewBean.setRenderAssignedPM(setupSingleton.isPmSchedulerSetupVisible(projectSetupViewBean
				.getProjectTypeId()));
		projectSetupViewBean.setRenderInternalScheduler(setupSingleton.isPmSchedulerSetupVisible(projectSetupViewBean
				.getProjectTypeId()));

		projectSetupViewBean.setLanguagesCHQ(langSingleton.getCourseLangs(Course.CHQ));
		ProjectCourseLangPref pclp = projectCourseLangPrefDao.findByProjectAndCourse(
				getCurrentProject().getProjectId(), setupSingleton.findCourseByAbbv(Course.CHQ).getCourse());
		if (pclp != null) {
			projectSetupViewBean.setLanguageCHQ(pclp.getLanguage().getCode());
		}

		disallowEditProject(getCurrentProject());

		if (projectTypeCode.equals(ProjectType.VIAEDGE)) {
			psvb.setViaEDGE(true);
		} else {
			psvb.setViaEDGE(false);
		}

		// Setup Report Configuration Panel
		psvb.setAllowReportDownload(getCurrentProject().getAllowReportDownload());

		List<ProjectReportObj> rptObjList = new ArrayList<ProjectReportObj>();
		if (currentProject.getProjectReportConfigs() == null || currentProject.getProjectReportConfigs().isEmpty()) {
			rptObjList = setupSingleton.getProjectReportTemplatesForProjectType(psvb.getProjectTypeId());
			log.debug("Project has no project report config, getting defaults for project type");

		} else {
			for (ProjectReport pr : getCurrentProject().getProjectReportConfigs()) {
				rptObjList.add(new ProjectReportObj(pr));
			}
		}
		Collections.sort(rptObjList);
		psvb.setProjectReportTemplates(rptObjList);

		this.showReportDownloads(false);

		// End Setup Report Configuration Panel

		psvb.setRenderSaveAndManageDelivery(allowSaveAndManageDelivery(psvb.getProjectTypeId()));

		this.populateAssignedPMs();
		this.populateInternalSchedulers();
		this.populateNoneBillableTypes();

		// Setup up the Standard Project Fields
		projectFieldsViewBean.clearFields();
		projectFieldsViewBean.addFields(uffdaService.getStandardFields(sessionBean.getCompany().getCompanyId(),
				CoreFieldGroupVo.PALMS, getCurrentProject().getProjectId(), FieldEntityType.PALMS_PROJECT_USERS, true,
				true, false, psvb.isIncludeClientLogonIdPassword()));

		// Get the Custom Project Fields
		uffdaService.getListOfCustomFields(sessionBean.getCompany().getCompanyId(), getCurrentProject().getProjectId(),
				projectFieldsViewBean.getFields(), FieldEntityType.PALMS_PROJECT_USERS, false, true, true, false);
		projectFieldsViewBean.sortFields();

		if (projectSetupViewBean.isShowParticipantInformationFields()) {
			handleShowParticipantInfomationFields();
		} else {
			handleShowProjectSetup();
		}

	}

	// This should support for legacy projects which doesn't have new features
	private void allowConfigureNewOptions(Project project) {
		if (project != null) {

			ProjectSetupViewBean view = getProjectSetupViewBean();
			String projectTypeCode = project.getProjectType().getProjectTypeCode();
			boolean disallowEditTargetLevel = true;
			boolean disallowEditScoring = true;
			boolean disallowEditConfiguration = true;

			if (projectTypeCode.equals(ProjectType.GLGPS)
					|| projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					|| projectTypeCode.equals(ProjectType.ERICSSON)) {
				if (project.getTargetLevelType() == null) {
					disallowEditTargetLevel = false;
				}
				if (project.getScoringMethodId() <= 0) {
					// Default to Standard
					projectSetupViewBean.setScoringMethodId(1);
					disallowEditScoring = false;
				}
				if (project.getConfigurationTypeId() <= 0) {
					projectSetupViewBean.setConfigurationTypeId(setupSingleton.findConfigurationTypeByCode("FC00")
							.getConfigurationTypeId());
					// disallowEditConfiguration = false;
				}
				view.setDisallowEditTargetLevel(disallowEditTargetLevel);
				view.setDisallowEditScoring(disallowEditScoring);
				view.setDisallowEditConfiguration(disallowEditConfiguration);
			}

			log.debug(
					"disallowEditTargetLevel = {}, disallowEditScoring = {}, disallowEditConfiguration = {} -- disallowEditProject = {}",
					view.isDisallowEditTargetLevel(), view.isDisallowEditScoring(), view.isDisallowEditConfiguration(),
					view.getDisallowEditProject());
		}

	}

	private void disallowEditProject(Project project) {

		FacesContext faceCtx = FacesContext.getCurrentInstance();
		ProjectSetupViewBean view = getProjectSetupViewBean();
		boolean lockIt = false;
		String projectTypeCode = project.getProjectType().getProjectTypeCode();

		// Lock project when participants added to the project
		if (this.getHasParticipants()) {
			faceCtx.addMessage("projectInfoMsgs", new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Project setup is locked:", "Participants have been added."));
			lockIt = true;
		}

		if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
				|| projectTypeCode.equals(ProjectType.ERICSSON)) {

			LrmObjectExistResponse prjExtResp = lrmObjectServiceLocal.projectExist(String.valueOf(project
					.getProjectId()));

			// Project lock when project delivery setup exists in LRM system
			if (prjExtResp.getRequestResult().equalsIgnoreCase("SUCCESS") && prjExtResp.isExist()) {
				faceCtx.addMessage("projectInfoMsgs", new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Project setup is locked:", "Delivery Setup is in progress."));
				lockIt = true;
			} else if (prjExtResp.getRequestResult().equalsIgnoreCase("FAILED")) {
				faceCtx.addMessage("projectInfoMsgs", new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Project setup is locked:", "We cannot determine if Delivery Setup is in progress."));
				lockIt = true;
			}
		}

		view.setDisallowEditProject(lockIt);
		allowConfigureNewOptions(project);
	}

	private void addInstrumentSelection(List<InstrumentSelectionObj> instruments, ProjectCourse projectCourse) {
		InstrumentSelectionObj instrumentSelection = new InstrumentSelectionObj();
		instrumentSelection.setId(projectCourse.getCourse().getCourse());
		instrumentSelection.setCode(projectCourse.getCourse().getAbbv());
		instrumentSelection.setName(projectCourse.getCourse().getCourseLabel());
		instrumentSelection.setEnabled(projectCourse.getEnabled());
		instrumentSelection.setSelected(projectCourse.getSelected());
		instrumentSelection.setType(projectCourse.getCourse().getType());

		List<CourseGroupTemplate> cgts = courseGroupTemplateDao.findCourseGroupTemplatesByProjectTypeId(projectCourse
				.getProject().getProjectType().getProjectTypeId());
		for (CourseGroupTemplate cgt : cgts) {
			if (cgt.getCourse().getCourse() == instrumentSelection.getId()) {
				// assume selection type and obj name will be the same
				// regardless of flow type
				instrumentSelection.setSelectionType(cgt.getSelectionType());
				instrumentSelection.setObjectName(cgt.getObjectName());
				instrumentSelection.setDownloadReportTypeId(cgt.getDownloadReportTypeId());
				break;
			}
		}
		System.out.println("instrumentSelection = " + instrumentSelection.getCode());
		// this.showReportDownloads(instrumentSelection, null);
		this.showReportDownloads(false);
		instruments.add(instrumentSelection);
	}

	/**
	 * addProjectTypes
	 * 
	 * @param s
	 *            - The proDUCT type code to get proJECT types for
	 * @param projectTypes
	 */
	private void addProjectTypes(String s, List<ListItemObj> projectTypes) {
		System.out.println(">_>_>_ addProjectTypes Product code " + s);
		for (ProjectType projectType : setupSingleton.findAllByProductCode(s)) {
			projectTypes.add(new ListItemObj(projectType.getProjectTypeId(), projectType.getName()));
			log.debug("Project Type: {}", projectType.getName());
		}
	}

	private void populateUi() {
		// Why are we doing it like this where we have another setup of objects
		// for presentation?
		// This keeps our presentation tier independent of business logic tier
		// As soon as we start binding things to presentation tier we introduce
		// tight coupling
		// We are using the controller to do the mapping of business objects to
		// presentation
		//
		// A benefit of doing it like this is presentation engineer and business
		// object engineer can work very independently
		// Only loading data for view and saving data is where they interact
		// Also things like initializing views are not doing much db work
		// compared to full binding

		// Course Flow Id: 1=prework, 2=assessment day

		log.debug("Before project types");
		// Populate Project Types
		List<ListItemObj> projectTypes = new ArrayList<ListItemObj>();

		if (hasProduct(Product.PROD_TLT)) {
			addProjectTypes(Product.PROD_TLT, projectTypes);
			// Removed per PA-421
			// addProjectTypes("TLT_LAG", projectTypes);
			// addProjectTypes("TLT_LAG_COG", projectTypes);
		}
		if (hasProduct(Product.PROD_ABYD)) {
			addProjectTypes(Product.PROD_ABYD, projectTypes);
			addProjectTypes(Product.PROD_ASMT, projectTypes);
		}
		if (hasProduct(Product.PROD_GLGPS)) {
			addProjectTypes(Product.PROD_GLGPS, projectTypes);
		}
		if (hasProduct(Product.PROD_COACH)) {
			addProjectTypes(Product.PROD_COACH, projectTypes);
		}
		if (hasProduct(Product.PROD_TLTDEMO)) {
			addProjectTypes(Product.PROD_TLTDEMO, projectTypes);
		}
		if (hasProduct(Product.PROD_VIAEDGE)) {
			addProjectTypes(Product.PROD_VIAEDGE, projectTypes);
		}
		if (hasProduct(Product.PROD_KFP)) {
			addProjectTypes(Product.PROD_KFP, projectTypes);
		}

		// Now set the flag and add company project types (if any) here
		getProjectSetupViewBean().setHasCompanyProjectTypes(
				(sessionBean.getCompany().getCompanyProjectTypes().size() > 0));
		for (CompanyProjectType cpt : sessionBean.getCompany().getCompanyProjectTypes()) {
			projectTypes.add(new ListItemObj(cpt.getProjectType().getProjectTypeId(), cpt.getProjectType().getName()));
		}
		Collections.sort(projectTypes);

		getProjectSetupViewBean().setProjectTypes(projectTypes);

		// Getting the list of Super Admins (type 4), picking first from the
		// list and assigning to the project
		List<User> superAdmins = new ArrayList<User>();
		superAdmins = userDao.findSuperAdminUsers(sessionBean.getCompany().getCompanyId());
		if (!superAdmins.isEmpty()) {
			getProjectSetupViewBean().setNhnAdminId(superAdmins.get(0).getUsersId());
		}

		// set delivery offices
		List<OfficeLocationObj> offices = new ArrayList<OfficeLocationObj>();
		List<OfficeLocation> oL = setupSingleton.getOfficeLocations();
		log.debug("office location size: {}", oL.size());

		for (OfficeLocation o : oL) {
			offices.add(new OfficeLocationObj(o));
		}
		getProjectSetupViewBean().setOfficeLocations(offices);

	}

	private void populateInternalSchedulers() {
		List<ExternalSubjectDataObj> schedulers = new ArrayList<ExternalSubjectDataObj>(
				teammemberHelper.getSchedulers());
		if (getCurrentProject() != null) {
			if (getCurrentProject().getInternalSchedulerId() != 0) {
				List<Integer> schedIds = new ArrayList<Integer>();
				schedIds.add(getCurrentProject().getInternalSchedulerId());
				List<PalmsSubject> ps = teammemberHelper.findSubjectsById(schedIds);
				if (!ps.isEmpty()) {
					projectSetupViewBean.setInternalSchedulerId(ps.get(0).getPrincipal());
					projectSetupViewBean.setInitialInternalSchedulerId(ps.get(0).getPrincipal());
					log.debug("internalSchedulerId = {}", projectSetupViewBean.getInternalSchedulerId());
				}
			}
		}
		projectSetupViewBean.setInternalSchedulers(schedulers);
	}

	private void populateAssignedPMs() {
		List<ExternalSubjectDataObj> assignedPms = new ArrayList<ExternalSubjectDataObj>(
				teammemberHelper.getAssignedPmPcs());

		if (getCurrentProject() != null) {
			if (getCurrentProject().getAssignedPmId() != 0) {
				List<Integer> pmIds = new ArrayList<Integer>();
				pmIds.add(getCurrentProject().getAssignedPmId());
				List<PalmsSubject> ps = teammemberHelper.findSubjectsById(pmIds);
				if (!ps.isEmpty()) {
					projectSetupViewBean.setAssignedPmId(ps.get(0).getPrincipal());
					projectSetupViewBean.setInitialAssignedPmId(ps.get(0).getPrincipal());
					log.debug("assignedPmId = {}", projectSetupViewBean.getAssignedPmId());
				}
			}
		}
		projectSetupViewBean.setAssignedPmPcs(assignedPms);
	}

	private void populateInstruments(Integer projectTypeId) {
		// Populate Prework Instruments
		log.debug("Before prework");
		List<InstrumentSelectionObj> preworkInstruments = new ArrayList<InstrumentSelectionObj>();
		boolean instrumentsVisible = false;

		Iterator<CourseGroupTemplate> iteratorPrework = setupSingleton.findCourseGroupByProjectTypeFlowType(
				projectTypeId, CourseFlowType.PREWORK);
		while (iteratorPrework.hasNext()) {
			CourseGroupTemplate courseGroupTemplate = iteratorPrework.next();
			InstrumentSelectionObj iso = new InstrumentSelectionObj(courseGroupTemplate.getCourse().getCourse(),
					courseGroupTemplate.getCourse().getAbbv(), courseGroupTemplate.getCourse().getCourseLabel(),
					courseGroupTemplate.getEnabled(), courseGroupTemplate.getSelected(), true);
			iso.setSelectionType(courseGroupTemplate.getSelectionType());
			iso.setObjectName(courseGroupTemplate.getObjectName());
			iso.setDownloadReportTypeId(courseGroupTemplate.getDownloadReportTypeId());

			log.debug(">>>>>>>>>> Defaults from datamodel: selected = {}, enabled = {}, visible = {}",
					iso.isSelected(), iso.isEnabled(), iso.isVisible());

			log.debug(">>>>>>>>>> Before prework setDownloadReportTypeId = {}",
					courseGroupTemplate.getDownloadReportTypeId());

			iso = applyTemplateRuleToInstrument(iso);

			if (iso.isVisible()) {
				preworkInstruments.add(iso);
			}

		}

		log.debug("Before assessment day");
		// Populate Assessment Day Instruments
		List<InstrumentSelectionObj> assessmentDayInstruments = new ArrayList<InstrumentSelectionObj>();
		Iterator<CourseGroupTemplate> iteratorAssessmentDay = setupSingleton.findCourseGroupByProjectTypeFlowType(
				projectTypeId, CourseFlowType.ASSESSMENT_DAY);
		while (iteratorAssessmentDay.hasNext()) {
			CourseGroupTemplate courseGroupTemplate = iteratorAssessmentDay.next();
			InstrumentSelectionObj iso = new InstrumentSelectionObj(courseGroupTemplate.getCourse().getCourse(),
					courseGroupTemplate.getCourse().getAbbv(), courseGroupTemplate.getCourse().getCourseLabel(),
					courseGroupTemplate.getEnabled(), courseGroupTemplate.getSelected(), true);
			iso.setSelectionType(courseGroupTemplate.getSelectionType());
			iso.setObjectName(courseGroupTemplate.getObjectName());
			iso.setDownloadReportTypeId(courseGroupTemplate.getDownloadReportTypeId());

			log.debug(">>>>>>>>>> Defaults from datamodel: selected = {}, enabled = {}, visible = {}",
					iso.isSelected(), iso.isEnabled(), iso.isVisible());

			log.debug(">>>>>>>>>> Before assessment day setDownloadReportTypeId = {}",
					courseGroupTemplate.getDownloadReportTypeId());

			iso = applyTemplateRuleToInstrument(iso);

			if (iso.isVisible()) {
				assessmentDayInstruments.add(iso);
			}
		}

		for (InstrumentSelectionObj preworkInstrument : preworkInstruments) {
			for (InstrumentSelectionObj assessmentDayInstrument : assessmentDayInstruments) {
				if (preworkInstrument.getCode().equalsIgnoreCase(assessmentDayInstrument.getCode())) {
					assessmentDayInstrument.setSelected(false);
					if (preworkInstrument.isSelected()) {
						assessmentDayInstrument.setEnabled(false);
					} else {
						assessmentDayInstrument.setEnabled(true);
					}
					preworkInstrument.setEnabled(true);
					break;
				}
			}
		}

		if (!preworkInstruments.isEmpty() || !assessmentDayInstruments.isEmpty()) {
			instrumentsVisible = true;
		}

		getProjectSetupViewBean().setPreworkInstruments(preworkInstruments);
		getProjectSetupViewBean().setAssessmentDayInstruments(assessmentDayInstruments);
		getProjectSetupViewBean().setInstrumentsVisible(instrumentsVisible);
	}

	private InstrumentSelectionObj applyTemplateRuleToInstrument(InstrumentSelectionObj instrument) {

		if (instrument != null) {
			String instrumentCode = "*";
			String projectTypeCode = "*";
			String configurationCode = "*";
			String targetLevel = "*";

			instrumentCode = instrument.getCode();
			int targetLevelTypeId = projectSetupViewBean.getTargetLevelTypeId();

			ProjectType projectType = setupSingleton.findProjectTypeById(projectSetupViewBean.getProjectTypeId());

			projectTypeCode = (projectType != null) ? projectType.getProjectTypeCode() : "*";

			if (projectSetupViewBean.getConfigurationTypeId() != null
					&& (projectSetupViewBean.getConfigurationTypeId() > 0)) {
				ConfigurationType configuration = setupSingleton.findConfigurationTypeById(projectSetupViewBean
						.getConfigurationTypeId());
				configurationCode = (configuration != null) ? configuration.getCode() : "*";
			}

			if (targetLevelTypeId > 0) {
				if (targetLevelTypeId == TargetLevelType.MLL) {
					targetLevel = "MLL";
				} else if (targetLevelTypeId == TargetLevelType.BUL) {
					targetLevel = "BUL";
				}
			}

			ProjectTemplateRuleObj rule = projectTemplateSingleton.getTemplateRule("Instruments", instrumentCode,
					projectTypeCode, configurationCode, targetLevel, "*", "*");

			instrument.setSelected(rule.selected());
			instrument.setEnabled(rule.enabled());
			instrument.setVisible(rule.visible());

			log.debug(">>>>>>>>>> instrumentCode = {}, projectTypeCode = {}, configurationCode = {}", instrumentCode,
					projectTypeCode, configurationCode);

		}

		return instrument;
	}

	private void populateReportContentManifests(int projectTypeId) {
		ProjectType projectType = setupSingleton.findProjectTypeById(projectTypeId);

		getProjectSetupViewBean().getReportContentManifestRefs().clear();

		List<String> reportContentManifestRefs = reportContentRepositoryBean.findDerivedManifestIds(projectType
				.getReportContentManifestRef());

		reportContentManifestRefs.add(projectType.getReportContentManifestRef());

		Collections.sort(reportContentManifestRefs);

		for (String reportContentManifestRef : reportContentManifestRefs) {
			ListItemObj item = new ListItemObj(reportContentManifestRef, reportContentManifestRef);
			getProjectSetupViewBean().getReportContentManifestRefs().add(item);
		}
	}

	private Boolean isRenderTargetLevels(int projectTypeId) {
		return setupSingleton.findProjectTypeById(projectTypeId).getTargetLevelVisible();
	}

	private Boolean isRenderReportContentManifests(int projectTypeId) {
		boolean render = true;
		ProjectType projectType = setupSingleton.findProjectTypeById(projectTypeId);
		if (projectType.getReportContentManifestRef() == null
				|| projectType.getReportContentManifestRef().length() == 0) {
			render = false;
		}
		return render;
	}

	// TODO: Not perfect yet to combine these two in one method. Action not
	// perfect either. Need to do for now because seems like can only have one
	// component.

	public Boolean save() {
		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		log.debug("IN ProjectSetupController.save()");
		log.debug("Project Name: {}, Project Code: {}, Project Type: {}",
				new Object[] { psvb.getProjectName(), psvb.getProjectCode(), psvb.getProjectTypeId() });
		Boolean isNew = true;
		Subject subject = SecurityUtils.getSubject();
		if (action == NEW_PROJECT) {

			log.debug("New project");
			Project newProject = new Project();
			newProject = setFields(newProject, isNew);
			newProject.setExtAdminId((String) subject.getPrincipal());

			currentProject = projectDao.create(newProject);
			sessionBean.setProject(currentProject);
			log.debug("Created project with id: {}", currentProject.getProjectId());
			// Check user permissions. Add Authorized entity if needed.
			if (!(subject.isPermitted("ViewAllProjects"))
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
							+ currentProject.getCompany().getCompanyId())
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":ViewAllProjects:"
							+ currentProject.getCompany().getCompanyId())) {
				log.debug("Subject is not permitted to view All Projects");

				RoleContext context = authorizationService.grantProjectAccess(
						sessionBean.getRoleContext().getSubject(), currentProject, subject.getPrincipal().toString(),
						null, EntityRule.ALLOW_CHILDREN);

				sessionBean.setRoleContext(context);

			}
			// TODO: JJB - Do I need to add to session? Seems like could be
			// better way to do this
			/*
						ProjectObj projectObj = new ProjectObj(currentProject.getProjectId(), currentProject.getName(),
								currentProject.getProjectCode(), currentProject.getProjectType().getName(), currentProject
										.getProjectType().getProjectTypeId(), projectDao.resolveTargetLevel(currentProject),
								currentProject.getExtAdminId(), currentProject.getUsersCount(), currentProject.getDateCreated(),
								currentProject.getFromEmail(), currentProject.getDueDate());
			*/
			ProjectObj projectObj = new ProjectObj(currentProject.getProjectId(), currentProject.getName(),
					currentProject.getProjectCode(), currentProject.getProjectType().getName(), currentProject
							.getProjectType().getProjectTypeId(), projectDao.resolveTargetLevel(currentProject),
					currentProject.getExtAdminId(), currentProject.getUsersCount(), currentProject.getDateCreated(),
					currentProject.getDueDate());

			projectObj.setConfigurationId(currentProject.getConfigurationTypeId());
			projectObj.setScoringMethodId(currentProject.getScoringMethodId());
			projectObj.setIncludeClientLogonIdPassword(currentProject.isIncludeClientLogonIdPassword());

			if (psvb.getLanguageCHQ() != null) {
				Course chq = setupSingleton.findCourseByAbbv("chq");
				projectDao.setProjectCourseLanguage(currentProject, chq, psvb.getLanguageCHQ());
			}

			try {
				sessionBean.getProjects().add(projectObj);
			} catch (Exception e) {
				log.debug("sessionBean.getProjects() = null - project not added to session");
			}
			// addProjectCoursePostProcess(psvb, currentProject.getProjectId());
			if (!(this.getPmPc() == null)) {
				authorizationService.grantProjectAccess(this.getPmPc(), currentProject, subject.getPrincipal()
						.toString(), null, EntityRule.ALLOW_CHILDREN);
			}
			if (!(this.getScheduler() == null)) {
				authorizationService.grantProjectAccess(this.getScheduler(), currentProject, subject.getPrincipal()
						.toString(), null, EntityRule.ALLOW_CHILDREN);
			}
			/*
			if (!(psvb.getProjectReportTemplates() == null) && !psvb.getProjectReportTemplates().isEmpty()){
				log.debug("Got Reports!!!");
				for (ProjectReport prjRpt : psvb.getProjectReportTemplates()){
					if (!prjRpt.isAllowAutogenerate() && !prjRpt.isAllowUserDownload()){
						continue;
					}else{
						ProjectReport rpt = new ProjectReport(prjRpt);
						rpt.setProject(currentProject);
						projectReportDao.create(rpt);
						
					}
				}
			}
			*/

		} else if (action == SAVE_PROJECT) {
			isNew = false;
			log.debug("Update project");
			Project updateProject = projectDao.findBatchById(currentProject.getProjectId());
			updateProject = setFields(updateProject, isNew);

			// TODO: Have to rethink
			projectCourseDao.deleteAllForProject(updateProject.getProjectId());

			projectDao.update(updateProject);
			sessionBean.setProject(updateProject);

			// projectCoursePostProcessDao.deleteAllForProject(updateProject.getProjectId());
			// addProjectCoursePostProcess(psvb, updateProject.getProjectId());
			if (projectSetupViewBean.isDoSaveInternalTeam()
					|| projectSetupViewBean.getRemoveInternalTeammembers().size() > 0) {
				saveInternalTeammembers();
			}
			if (projectSetupViewBean.isDoSaveClientTeam()
					|| projectSetupViewBean.getRemoveClientTeammembers().size() > 0) {
				saveClientTeammembers();
			}
			Course chq = setupSingleton.findCourseByAbbv("chq");
			projectDao.setProjectCourseLanguage(currentProject, chq, psvb.getLanguageCHQ());
			// handled by the project list controller bean
			// sessionBean.updateProject(updateProject);
			if (getProjectSetupViewBean().isRenderAssignedPM()) {
				String pmid = projectSetupViewBean.getAssignedPmId();
				if (!(pmid == null) && !(pmid.equalsIgnoreCase(projectSetupViewBean.getInitialAssignedPmId()))) {
					authorizationService.grantProjectAccess(this.getPmPc(), currentProject, subject.getPrincipal()
							.toString(), null, EntityRule.ALLOW_CHILDREN);
				}
			}

			if (getProjectSetupViewBean().isRenderInternalScheduler()) {
				String sid = projectSetupViewBean.getInternalSchedulerId();
				if (!(sid == null) && !(sid.equalsIgnoreCase(projectSetupViewBean.getInitialInternalSchedulerId()))) {
					authorizationService.grantProjectAccess(this.getScheduler(), currentProject, subject.getPrincipal()
							.toString(), null, EntityRule.ALLOW_CHILDREN);
				}
			}
			if (!(psvb.getOriginalDueDate() == null)
					&& !updateProject.getDueDate().equals(new Timestamp(psvb.getOriginalDueDate().getTime()))) {
				// Update relative email rule schedules to reflect changed
				// project due date
				Date oldDate = psvb.getOriginalDueDate();
				Date newDate = new Date(updateProject.getDueDate().getTime());
				relativeScheduleDateUpdater.updateRelativeEmailScheduleDatesForProject(newDate, oldDate,
						updateProject.getProjectId());
			}
			if (!(psvb.getOriginalDueDate() == null)
					&& !updateProject.getDueDate().equals(new Timestamp(psvb.getOriginalDueDate().getTime()))) {
				// Update relative email rule schedules to reflect changed
				// project due date
				Date oldDate = psvb.getOriginalDueDate();
				Date newDate = new Date(updateProject.getDueDate().getTime());
				relativeScheduleDateUpdater.updateRelativeEmailScheduleDatesForProject(newDate, oldDate,
						updateProject.getProjectId());
			}
		}

		if (projectSetupViewBean.isIncludeClientLogonIdPassword()) {
			projectFieldsControllerBean.handleAddLogonFields(getCurrentProject().getProjectId());
		} else {
			projectFieldsControllerBean.handleRemoveLogonFields();
		}
		projectSetupViewBean.resetOriginalIncludeClientLogonIdPassword();

		uffdaService.saveCoreFieldsToFieldMap(sessionBean.getSubject_id(), getCurrentProject().getProjectId(),
				FieldEntityType.PALMS_PROJECT_USERS, projectFieldsViewBean.getFields(),
				projectFieldsViewBean.getDeletedFields());

		uffdaService.saveCustomFieldsToFieldMap(sessionBean.getSubject_id(), getCurrentProject().getProjectId(),
				FieldEntityType.PALMS_PROJECT_USERS, projectFieldsViewBean.getFields(),
				projectFieldsViewBean.getDeletedFields());

		return true;
	}

	private Project setFields(Project project, Boolean isNew) {
		Subject subject = SecurityUtils.getSubject();
		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		log.debug("Project Name: {}, Project Code: {}, Project Type: {}",
				new Object[] { psvb.getProjectName(), psvb.getProjectCode(), psvb.getProjectTypeId() });

		project.setName(psvb.getProjectName());
		project.setProjectCode(psvb.getProjectCode());
		// project.setFromEmail(psvb.getFromEmail());
		// project.setDueDate(new Timestamp(psvb.getDueDate().getTime()));
		project.setCompany(companyDao.findById(sessionBean.getCompany().getCompanyId()));
		project.setProjectType(setupSingleton.findProjectTypeById(psvb.getProjectTypeId()));
		project.setReportContentManifestRef(psvb.getReportContentManifestRef());
		project.setIncludeClientLogonIdPassword(psvb.isIncludeClientLogonIdPassword());

		if (psvb.isRenderConfigurationSelection()) {
			if (psvb.getConfigurationTypeId() != null) {
				project.setConfigurationTypeId(psvb.getConfigurationTypeId());
			}
		} else {
			project.setConfigurationTypeId(0);
		}

		if (psvb.isRenderScoringMethod()) {
			if (psvb.getScoringMethodId() != null) {
				project.setScoringMethodId(psvb.getScoringMethodId());
			}
		} else {
			project.setScoringMethodId(0);
		}

		log.debug("Project Due Date: {}", psvb.getDueDate());
		if (psvb.getDueDate() != null) {
			project.setDueDate(new Timestamp(psvb.getDueDate().getTime()));
		}

		OfficeLocation ol = officeLocationDao.findById(psvb.getSelectedOfficeLocationId());
		project.setOfficeLocation(ol);
		// NHN-2760 added check for isNew (project update) resolves AbyD project
		// edit -ky 1.10.13
		if (project.getProjectType().getTargetLevelVisible().booleanValue() || !isNew) {
			if (psvb.getTargetLevelTypeId() > 0) {
				project.setTargetLevelType(new TargetLevelType(psvb.getTargetLevelTypeId()));
			}
		} else {
			project.setTargetLevelType(null);
		}

		// TODO: AV: Do we still need this ?
		if (psvb.getProjectTypeId() == 1 || psvb.getProjectTypeId() == 2) {
			project.setProjectTypeOld(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES);
		} else if (psvb.getProjectTypeId() == 3) {
			project.setProjectTypeOld(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE);
		} else if (psvb.getProjectTypeId() == 4) {
			project.setProjectTypeOld(ProjectType.ASSESSMENT_TESTING);
		}

		// Billable
		if (psvb.isRenderBillable()) {
			project.setBillable(psvb.getBillable());
			if (!psvb.getBillable()) {
				project.setNonBillableTypeId(psvb.getNoneBillableTypeId());
			} else {
				project.setNonBillableTypeId(0);
			}
		} else {
			project.setBillable(null);
			project.setNonBillableTypeId(null);
		}

		User admin = userDao.findById(psvb.getNhnAdminId());
		project.setAdmin(admin);

		if (psvb.isRenderAssignedPM()) {
			int pmId = 0;
			this.setPmPc(null);
			if (psvb.getAssignedPmId() != null) {
				this.setPmPc(teammemberHelper.findSubjectByPrincipalAndRealmName(psvb.getAssignedPmId()));
				// authorizationService.grantProjectAccess(palmsSubject,
				// currentProject, subject.getPrincipal().toString(), null,
				// EntityRule.ALLOW_CHILDREN);
				pmId = this.getPmPc().getSubject_id();
			}
			project.setAssignedPmId(pmId);
		} else {
			project.setAssignedPmId(0);
		}

		if (psvb.isRenderInternalScheduler()) {
			int schedId = 0;
			this.setScheduler(null);
			if (psvb.getInternalSchedulerId() != null) {
				this.setScheduler(teammemberHelper.findSubjectByPrincipalAndRealmName(psvb.getInternalSchedulerId()));
				// authorizationService.grantProjectAccess(palmsSubject,
				// currentProject, subject.getPrincipal().toString(), null,
				// EntityRule.ALLOW_CHILDREN);
				schedId = this.getScheduler().getSubject_id();
			}
			project.setInternalSchedulerId(schedId);
		} else {
			project.setInternalSchedulerId(0);
		}

		// Hardwired to 1
		// TODO: cch
		// project.setTargetLevel(targetLevelDao.findById(1));

		if (project.getProjectUsers() == null) {
			project.setProjectUsers(new ArrayList<ProjectUser>());
		}

		// -----------------------------------------------
		// Move this section below the next... It relies on allowReportDownload
		/*
		project.setProjectCourses(new ArrayList<ProjectCourse>());
		List<ProjectCourse> projectCourses = project.getProjectCourses();

		// Loop through instrument selections and add to ProjectCourses
		Iterator<InstrumentSelectionObj> iteratorPrework = psvb.getPreworkInstruments().iterator();
		Integer cntPrework = 1;
		while (iteratorPrework.hasNext()) {
			InstrumentSelectionObj instrument = iteratorPrework.next();

			addProjectCourse(projectCourses, project, instrument, 1, cntPrework);
			cntPrework++;
		}

		Iterator<InstrumentSelectionObj> iteratorAssessmentDay = psvb.getAssessmentDayInstruments().iterator();
		Integer cntAssessmentDay = 1;
		while (iteratorAssessmentDay.hasNext()) {
			InstrumentSelectionObj instrument = iteratorAssessmentDay.next();

			addProjectCourse(projectCourses, project, instrument, 2, cntAssessmentDay);
			cntAssessmentDay++;
		}
		 */
		// -----------------------------------------------

		boolean allowReportDownload = false;
		if (!(psvb.getProjectReportTemplates() == null) && !psvb.getProjectReportTemplates().isEmpty()) {
			log.debug("Got Reports!!!");
			List<ProjectReport> pRptList = new ArrayList<ProjectReport>();
			if (isNew || project.getProjectReportConfigs() == null || project.getProjectReportConfigs().size() == 0) {
				for (ProjectReportObj prjRpt : psvb.getProjectReportTemplates()) {
					if (prjRpt.isSelected() && prjRpt.isAllowUserDownload()) {
						allowReportDownload = true;
					}
					ProjectReport rpt = prjRpt.createEntityFromObj();
					rpt.setProject(project);
					// projectReportDao.create(rpt);
					pRptList.add(rpt);

				}
				project.setProjectReportConfigs(pRptList);
			} else {
				log.debug("Project is NOT New, updating {} project report configs", project.getProjectReportConfigs()
						.size());

				for (ProjectReport pr : project.getProjectReportConfigs()) {
					log.debug("Searching for matching DTO: {}", pr.getId());
					for (ProjectReportObj pro : psvb.getProjectReportTemplates()) {
						log.debug("DTO: {}", pro.getProjectReportId());
						if (pro.getProjectReportId() == pr.getId()) {
							log.debug("Updating ProjectReport entity: {}", pr.getId());
							if (pro.isSelected() && pro.isAllowUserDownload()) {
								allowReportDownload = true;
							}
							pro.updateEntityFromObj(pr);
							break;
						}
					}
				}

			}
		}
		project.setAllowReportDownload(allowReportDownload);

		project.setProjectCourses(new ArrayList<ProjectCourse>());
		List<ProjectCourse> projectCourses = project.getProjectCourses();

		// Loop through instrument selections and add to ProjectCourses
		Iterator<InstrumentSelectionObj> iteratorPrework = psvb.getPreworkInstruments().iterator();
		Integer cntPrework = 1;
		while (iteratorPrework.hasNext()) {
			InstrumentSelectionObj instrument = iteratorPrework.next();

			addProjectCourse(projectCourses, project, instrument, 1, cntPrework);
			cntPrework++;
		}

		Iterator<InstrumentSelectionObj> iteratorAssessmentDay = psvb.getAssessmentDayInstruments().iterator();
		Integer cntAssessmentDay = 1;
		while (iteratorAssessmentDay.hasNext()) {
			InstrumentSelectionObj instrument = iteratorAssessmentDay.next();

			addProjectCourse(projectCourses, project, instrument, 2, cntAssessmentDay);
			cntAssessmentDay++;
		}

		return project;
	}

	private void addProjectCourse(List<ProjectCourse> projectCourses, Project project,
			InstrumentSelectionObj instrument, Integer courseFlowTypeId, Integer seq) {
		ProjectCourse projectCourse = new ProjectCourse();
		// Course course = courseDao.findById(instrument.getId());
		Course course = setupSingleton.findCourseById(instrument.getId());
		projectCourse.setCourse(course);
		CourseFlowType courseFlowType = courseFlowTypeDao.findById(courseFlowTypeId);
		projectCourse.setCourseFlowType(courseFlowType);
		projectCourse.setEnabled(instrument.isEnabled());
		projectCourse.setSelected(instrument.isSelected());
		projectCourse.setSequence(seq);
		projectCourse.setProject(project);
		if (project.getAllowReportDownload()) {
			projectCourse.setDownloadReportTypeId(instrument.getDownloadReportTypeId());
		}
		projectCourses.add(projectCourse);
	}

	// Add following 2 method to deal with grouping courses they must all
	// complete before starting score.
	/*
	private void addProjectCoursePostProcess(ProjectSetupViewBean pv, int projectId) {

		List<InstrumentSelectionObj> preworkInstr = pv.getPreworkInstruments();

		for (InstrumentSelectionObj prewkinstrObj : preworkInstr) {
			if (prewkinstrObj.isSelected()) {
				log.debug("preWork instrument: {} selected?? {}", prewkinstrObj.getId(), 
						prewkinstrObj.isSelected());
				createUpdateProjectCoursePostProcess(projectId, prewkinstrObj.getId());

			}
		}

		List<InstrumentSelectionObj> assessDayInstr = pv.getAssessmentDayInstruments();

		for (InstrumentSelectionObj assessDayinstrObj : assessDayInstr) {
			if (assessDayinstrObj.isSelected()) {
				log.debug("what is assessday instrument: {} selected?? {}", assessDayinstrObj.getId(),
						assessDayinstrObj.isSelected());
				createUpdateProjectCoursePostProcess(projectId, assessDayinstrObj.getId());
			}
		}
	}
	*/
	/*
		public void createUpdateProjectCoursePostProcess(int projectId, int courseId) {
			ProjectCoursePostProcess projectCoursePostProcess = new ProjectCoursePostProcess();
			// Course course = courseDao.findById(courseId);
			//Course course = setupSingleton.findCourseById(courseId);
			//CourseScorecard cs = courseScorecardDao.findByCourse(course.getCourse());

			if (setupSingleton.isScorecardCourse(courseId)) {
				projectCoursePostProcess.setProjectId(projectId);
				projectCoursePostProcess.setCourseId(courseId);
				projectCoursePostProcessDao.create(projectCoursePostProcess);
			}
		}
	*/
	// Actions
	public void handleInitForNew(ActionEvent event) {
		initForNew();
	}

	public void handleInitForEdit(ActionEvent event) {
		initForEdit();
	}

	// JJB: More generic logic would be
	// Scan lists...and see if both have one with same course id
	// If both sides are enabled and one has check then disable other one
	// This would get rid of relying on ID's and offsets

	// Another approach...
	// Change to have handlePreworkCheckInstrument and
	// handleAssessmentDayCheckInstrument
	// Put course abbv in id
	// Should know if check/uncheck based on current value
	// Do appropriate things to course abbv on other side

	// useful pattern shown here: cast event source to its type and then get id
	public void handleCheckInstrument(InstrumentSelectionObj instrumentSelectionObj, Integer courseFlowType) {
		ConfigurationType configuration = new ConfigurationType();

		if (projectSetupViewBean.getConfigurationTypeId() != null) {
			configuration = setupSingleton.findConfigurationTypeById(projectSetupViewBean.getConfigurationTypeId());
		}

		// Showing or hiding Report Downloads section
		this.showReportDownloads(false);
		/*
		if (instrumentSelectionObj.getCode().equals(Course.VEDGE_NODEMO)
				|| instrumentSelectionObj.getCode().equals(Course.VEDGE)) {
			// || instrumentSelectionObj.getCode().equals(Course.KFP)) {
			getProjectSetupViewBean().setRenderAllowReportDownload(instrumentSelectionObj.isSelected());
			getProjectSetupViewBean().setAllowReportDownload(false);
		}*/

		if (courseFlowType == 1) {
			InstrumentSelectionObj iso = projectSetupViewBean
					.findAssessmentDayInstrumentSelectionObjById(instrumentSelectionObj.getId());

			if (iso != null) {
				iso.setEnabled(!instrumentSelectionObj.isSelected());
				// We need to allow to unselect instruments like (rv, wg, finex)
				// when configuration type is 'tailored or custom'
				if (configuration.getCode() != null) {
					if (!configuration.getCode().equals("FC00")) {
						iso.setSelected(iso.isEnabled());
						instrumentSelectionObj.setEnabled(!iso.isSelected());
					}
				}
			}
		} else {
			InstrumentSelectionObj iso = projectSetupViewBean
					.findPreworkInstrumentSelectionObjById(instrumentSelectionObj.getId());
			if (iso != null) {
				iso.setEnabled(!instrumentSelectionObj.isSelected());
				// We need to allow to unselect instruments like (rv, wg, finex)
				// when configuration type is 'tailored or custom'
				if (configuration.getCode() != null) {
					if (!configuration.getCode().equals("FC00")) {
						iso.setSelected(iso.isEnabled());
						instrumentSelectionObj.setEnabled(!iso.isSelected());
					}
				}
			}
		}

		/*
		 * ViaEDGE exclusivity logic
		 */

		if (instrumentSelectionObj.getSelectionType().equalsIgnoreCase("RADIO")) {
			if (courseFlowType == 1) {
				for (InstrumentSelectionObj iso : projectSetupViewBean.getPreworkInstruments()) {
					if (iso.getObjectName() == null) {
						continue;
					}
					if (iso.getObjectName().equalsIgnoreCase(instrumentSelectionObj.getObjectName())
							&& !iso.getCode().equalsIgnoreCase(instrumentSelectionObj.getCode())) {
						log.debug("iso {} setSelected to {}", iso.getCode(), !instrumentSelectionObj.isSelected());
						// iso.setSelected(!instrumentSelectionObj.isSelected());
						iso.setEnabled(!instrumentSelectionObj.isSelected());
					}
				}
			}
		}
	}

	private void showReportDownloads(Boolean forceShow) {
		List<InstrumentSelectionObj> allInst = new ArrayList<InstrumentSelectionObj>();
		if (!(getProjectSetupViewBean().getAssessmentDayInstruments() == null)) {
			// allInst.addAll(getProjectSetupViewBean().getAssessmentDayInstruments());
			for (InstrumentSelectionObj obj : getProjectSetupViewBean().getAssessmentDayInstruments()) {
				if (obj.isSelected()) {
					allInst.add(obj);
				}
			}
		} else {
			log.debug("assessment day selections are null");
		}
		if (!(getProjectSetupViewBean().getPreworkInstruments() == null)) {
			// allInst.addAll(getProjectSetupViewBean().getPreworkInstruments());
			for (InstrumentSelectionObj obj : getProjectSetupViewBean().getPreworkInstruments()) {
				if (obj.isSelected()) {
					allInst.add(obj);
					log.debug("Adding Selected Instrument {}", obj.getCode());
				}
			}
		} else {
			log.debug("Prework instrument selections are null");
		}
		// build project report objects based on report templates for project
		// type and selected content
		boolean atLeastOneSelected = false;
		boolean showReportConfig = false;
		for (ProjectReportObj pro : getProjectSetupViewBean().getProjectReportTemplates()) {
			log.debug("Checking Project Report template: {}", pro.getProjectReportId());
			// Only one course in collection required to be selected
			if (pro.getContentRule() == ProjectReport.CONTENT_RULE_ANY) {
				log.debug("Content Rule: ANY");
				for (InstrumentSelectionObj obj : allInst) {
					if (pro.getCourseIdMap().containsKey(obj.getId())) {
						log.debug("Found selected instrument: {}", obj.getCode());
						pro.setSelected(true);

						atLeastOneSelected = true;

						break;
					}
				}
				// All required courses must be selected
			} else if (pro.getContentRule() == ProjectReport.CONTENT_RULE_ALL) {
				showReportConfig = true;
				for (Object i : pro.getCourseIdMap().keySet()) {
					// first check if it's required
					if (pro.getCourseIdMap().get(i)[1]) {
						boolean found = false;

						// now see if it's selected
						for (InstrumentSelectionObj obj : allInst) {

							if (obj.getId() == i) {
								found = true;
								break;
							}
						}

						if (!found) {
							showReportConfig = false;
							break;
						}
					}
				}
				pro.setSelected(showReportConfig);

			}

		}
		if (atLeastOneSelected || showReportConfig) {
			log.debug("Setting Render AllowReportDownload true");
			getProjectSetupViewBean().setRenderAllowReportDownload(true);
		} else {
			log.debug("Setting Render AllowReportDownload false");
			getProjectSetupViewBean().setRenderAllowReportDownload(false);
		}

	}

	public void handleChangeProjectType(AjaxBehaviorEvent event) {
		this.doProjectTypeChange();

	}

	private void resetOptions() {
		getProjectSetupViewBean().setAssignedPmId("0");
		getProjectSetupViewBean().setInternalSchedulerId("0");
		getProjectSetupViewBean().setTargetLevelTypeId(0);
		getProjectSetupViewBean().setScoringMethodId(0);
		getProjectSetupViewBean().setConfigurationTypeId(0);
		getProjectSetupViewBean().setIncludeClientLogonIdPassword(false);
		getProjectSetupViewBean().resetOriginalIncludeClientLogonIdPassword();
	}

	private boolean allowSaveAndManageDelivery(int projectTypeId) {
		boolean allow = false;
		String projectTypeCode = setupSingleton.findProjectTypeById(projectTypeId).getProjectTypeCode();

		if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.ERICSSON)) {
			allow = true;
		}

		log.debug("allowSaveAndManageDelivery projectTypeCode = {}, allow = {}", projectTypeCode, allow);

		return allow;
	}

	public void handleChangeConfigurationType() {
		populateInstruments(projectSetupViewBean.getProjectTypeId());
		this.showReportDownloads(false);
	}

	public void handleChangeTargetLevel(AjaxBehaviorEvent event) {
		populateConfigurationTypes();
		this.showReportDownloads(false);
	}

	public void handleChangeBillable() {
		getProjectSetupViewBean().setRenderNoneBillableTypes(!getProjectSetupViewBean().getBillable());
		getProjectSetupViewBean().setDisabledProjectCode(!getProjectSetupViewBean().getBillable());
		getProjectSetupViewBean().setNoneBillableTypeId(0);
	}

	public void handleChangeNoneBillableType() {
		getProjectSetupViewBean().setProjectCode(
				NonBillableTypeObj.getTypeName(getProjectSetupViewBean().getNoneBillableTypeId()));
	}

	private void populateTargetLevelTypes(Integer projectTypeId) {
		List<TargetLevelTypeObj> typeList = new ArrayList<TargetLevelTypeObj>();

		// get the render to see if we need to populate the list
		if (isRenderTargetLevels(getProjectSetupViewBean().getProjectTypeId())) {
			// It's on, populate the empty list
			ProjectType pt = setupSingleton.findProjectTypeById(projectTypeId);
			for (TargetLevelGroupType typ : pt.getTargetLevelGroup().getTargetLevelGroupTypes()) {
				typeList.add(new TargetLevelTypeObj(typ.getTargetLevelType().getTargetLevelTypeId(), typ
						.getTargetLevelType().getName(), typ.getTargetLevelType().getCode()));
			}
		}

		// put the list into the bean and scram
		getProjectSetupViewBean().setTargetLevelTypes(typeList);
	}

	private void populateConfigurationTypes() {
		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		ProjectType projectType = setupSingleton.findProjectTypeById(psvb.getProjectTypeId());
		log.debug("In PopulateConfigurationTypes() ...  Got project type: {} Got TargetLevel: {}",
				projectType.getProjectTypeCode(), psvb.getTargetLevelTypeId());
		List<ConfigurationType> cts = new ArrayList<ConfigurationType>();
		ConfigurationType tailoredCustom = setupSingleton.findConfigurationTypeByCode("FC00");
		cts = setupSingleton.getConfigurationTypesForProjectType(projectType, psvb.getTargetLevelTypeId());
		log.debug("Got {} Configuration Types.", cts.size());
		psvb.setConfigurationTypes(cts);

		// Reset selection if configuration type is something other than
		// Tailored or Custom
		if (psvb.getConfigurationTypeId() != null) {
			log.trace("Configuration Type id : {}", psvb.getConfigurationTypeId());
			if (psvb.getConfigurationTypeId() != tailoredCustom.getConfigurationTypeId()) {
				psvb.setConfigurationTypeId(-1);
			}
		}

		// Do not reset instruments if configuration type disabled or if the
		// project setup is locked
		if (!this.getHasParticipants() && psvb.isRenderConfigurationSelection()) {
			if (!psvb.isDisallowEditConfiguration() || !psvb.getDisallowEditProject()) {
				log.trace("Populating instruments");
				populateInstruments(projectSetupViewBean.getProjectTypeId());
			}
		}
	}

	private void populateScoringMethods() {
		getProjectSetupViewBean().setScoringMethods(ScoringMethodObj.scoringMethodDb);
	}

	private void populateCHQLanguages() {
		getProjectSetupViewBean().setLanguagesCHQ(langSingleton.getCourseLangs(Course.CHQ));
	}

	private void populateNoneBillableTypes() {
		getProjectSetupViewBean().setNoneBillableTypes(NonBillableTypeObj.nonBillableTypes);
	}

	// unused?
	public void handleSave(ActionEvent event) {
		save();
	}

	// NOTE: called from ProjectListControllerBean.handleProjectSetupSave()
	// and from ProjectManagementControllerBean.handleProjectSetupSave()

	public void validateAndSave() {
		log.debug("in validateAndSave");
		if (requiredFieldsPresent()) {
			log.debug("valid");
			save();
		} else {
			log.debug("INVALID");
		}
	}

	public String navNewProjectSetup() {
		return "projectSetup.xhtml?faces-redirect=true&create=true";
	}

	public Boolean passedValidation() {
		log.debug("in passedValidation");

		Boolean cve = isContentVisibleAndEnabled();
		Boolean rfp = requiredFieldsPresent();

		return cve && rfp;
	}

	// TODO AV: Validation needs to be changed this is not the right way to do
	// it.

	public Boolean requiredFieldsPresent() {
		log.debug("IN requiredFieldsPresent");

		boolean retval = true;
		String projectName = projectSetupViewBean.getProjectName();
		Integer projectOfficeLocation = projectSetupViewBean.getSelectedOfficeLocationId();
		FacesContext fctx = FacesContext.getCurrentInstance();
		boolean isContentSelected = false;

		resetValidationFlags();

		if (projectName.equals("")) {
			retval = false;
			fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Project Name required.", ""));
			projectSetupViewBean.setValidProjectName(false);
		}

		if (projectOfficeLocation <= 0) {
			retval = false;
			fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Client Mgmt Office required.", ""));
			projectSetupViewBean.setValidOfficeLocation(false);
		}

		if (projectSetupViewBean.isRenderBillable()) {
			if (projectSetupViewBean.getProjectCode().trim().equals("")) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Engagement ID required.", ""));
				projectSetupViewBean.setValidProjectCode(false);
			}
		}

		if (projectSetupViewBean.isRenderTargetLevels()) {
			if (projectSetupViewBean.getTargetLevelTypeId() <= 0) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Target Level required.", ""));
				projectSetupViewBean.setValidTargetLevel(false);
			}
		}

		// Check the due date if a viaEdge project
		String projectTypeCode = setupSingleton.findProjectTypeById(projectSetupViewBean.getProjectTypeId())
				.getProjectTypeCode();
		if (projectTypeCode.equals(ProjectType.VIAEDGE)) {
			if (projectSetupViewBean.getDueDate() == null) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Due date required.", ""));
				projectSetupViewBean.setValidDueDate(false);
			}
		}

		if (projectTypeCode.equals(ProjectType.GLGPS)
				|| projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.ERICSSON)) {

			if (projectSetupViewBean.getAssignedPmId() == null || projectSetupViewBean.getAssignedPmId().isEmpty()) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Assigned PM/PC required.", ""));
				projectSetupViewBean.setValidAssignedPmPc(false);
			}

			if (projectSetupViewBean.getInternalSchedulerId() == null
					|| projectSetupViewBean.getInternalSchedulerId().isEmpty()) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Internal Scheduler required.", ""));
				projectSetupViewBean.setValidInternalScheduler(false);
			}

			if (projectSetupViewBean.isRenderScoringMethod()
					&& (projectSetupViewBean.getScoringMethodId() == null || projectSetupViewBean.getScoringMethodId() <= 0)) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Scoring Method required.", ""));
				projectSetupViewBean.setValidScoringMethod(false);
			}

			if (projectSetupViewBean.getConfigurationTypeId() == null
					|| projectSetupViewBean.getConfigurationTypeId() <= 0) {
				retval = false;
				fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Configuration Type required.", ""));
				projectSetupViewBean.setValidConfigurationType(false);
			}

		}

		for (InstrumentSelectionObj iso : projectSetupViewBean.getPreworkInstruments()) {
			if (iso.isSelected()) {
				isContentSelected = true;
				break;
			}
		}
		if (!isContentSelected) {
			for (InstrumentSelectionObj iso : projectSetupViewBean.getAssessmentDayInstruments()) {
				if (iso.isSelected()) {
					isContentSelected = true;
					break;
				}
			}
		}
		if (!isContentSelected) {
			retval = false;
			fctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Please select at least one instrument.", ""));
		}

		if (retval == false) {
			handleShowProjectSetup();
		}
		return retval;

	}

	private void resetValidationFlags() {
		projectSetupViewBean.setValidProjectName(true);
		projectSetupViewBean.setValidProjectCode(true);
		projectSetupViewBean.setValidDueDate(true);
		projectSetupViewBean.setValidOfficeLocation(true);
		projectSetupViewBean.setValidAssignedPmPc(true);
		projectSetupViewBean.setValidInternalScheduler(true);
		projectSetupViewBean.setValidTargetLevel(true);
		projectSetupViewBean.setValidScoringMethod(true);
		projectSetupViewBean.setValidConfigurationType(true);
	}

	public String handleNavSave() {
		log.debug("in handleNavSave");
		String retval = "";
		if (passedValidation()) {
			retval = "projectManagement.xhtml?faces-redirect=true";
			save();
		}
		return retval;
	}

	public String handleNavSaveAndReturn() {
		log.debug("in handleNavSaveAndReturn");
		String retval = "";
		if (passedValidation()) {
			retval = "projectList.xhtml?faces-redirect=true";
			save();
		}
		return retval;
	}

	public String handleNavCancel() {
		return "projectList.xhtml?faces-redirect=true";
	}

	public String handleNavSaveDelivery() {
		log.debug("in handleNavSaveDelivery");
		String operationName = "";

		if (passedValidation()) {
			save();
			if (action == NEW_PROJECT) {
				operationName = EntityAuditLogger.OP_CREATE_PROJECT_AND_ACCESS_LRM_SETUP;
			} else if (action == SAVE_PROJECT) {
				operationName = EntityAuditLogger.OP_UPDATE_PROJECT_AND_ACCESS_LRM_SETUP;
			}
			Subject subject = SecurityUtils.getSubject();

			EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({})",
					new Object[] { subject.getPrincipal(), operationName, sessionBean.getProject().getProjectId(),
							sessionBean.getProject().getName() });
			sessionBean.setLrmProjectId(0);
			sessionBean.setImportJobId(0);
			return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/deliverySetup.xhtml&sys=lrm&returnTo=admin_proj_mgmt";
		}
		return "";
	}

	public void handleTabChange(TabChangeEvent event) {
		Tab currentTab = event.getTab();
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> currentTab.getId = {}", currentTab.getId());

		if (currentTab.getId().equalsIgnoreCase("clientTeam")) {
			if (!isClientRolesListExist()) {
				projectSetupViewBean.setClientTeammembers(fetchClientTeammembers());
				setClientRolesList(projectSetupViewBean.getClientTeammembers());
			} else {
				projectSetupViewBean.setClientTeammembers(getClientRolesList());
			}
		} else if (currentTab.getId().equalsIgnoreCase("internalTeam")) {
			if (!isInternalRolesListExist()) {
				projectSetupViewBean.setInternalTeammembers(fetchInternalTeammembers());
				setInternalRolesList(projectSetupViewBean.getInternalTeammembers());
			} else {
				projectSetupViewBean.setInternalTeammembers(getInternalRolesList());
			}
		}
	}

	public void handleToggleProjectOpen() {
		Boolean isOpen = currentProject.getActive() == 1 ? true : false;
		currentProject.setActive(isOpen ? 0 : 1);
		projectDao.update(currentProject);
		projectSetupViewBean.setProjectOpen(!isOpen);
	}

	/* Internal Teammembers */

	// private void saveTeammembers() {
	// teammemberHelper.validateRemoveList(projectSetupViewBean.getInternalTeammembers(),
	// projectSetupViewBean.getRemoveInternalTeammembers());
	// teammemberHelper.removeTeammembers(projectSetupViewBean.getRemoveInternalTeammembers());
	// teammemberHelper.saveTeammembers(projectSetupViewBean.getInternalTeammembers(),
	// sessionBean.getCompany()
	// .getCompanyId(), getCurrentProject().getProjectId());
	// }

	private void saveInternalTeammembers() {
		Integer projectId = currentProject.getProjectId();
		Integer companyId = currentProject.getCompany().getCompanyId();
		teammemberHelper.init(EntityType.PROJECT_TYPE, projectId, "palmsRealmNames", EntityRule.ALLOW_CHILDREN);
		teammemberHelper
				.validateRemoveList(getInternalRolesList(), projectSetupViewBean.getRemoveInternalTeammembers());
		teammemberHelper.removeTeammembers(projectSetupViewBean.getRemoveInternalTeammembers());
		teammemberHelper.saveTeammembers(getInternalRolesList(), companyId, projectId);

	}

	private List<ExternalSubjectDataObj> fetchInternalTeammembers() {
		setInternalRolesListExist(true);
		return teammemberHelper.fetchTeammembers(EntityType.PROJECT_TYPE, getCurrentProject().getProjectId(),
				"palmsRealmNames");
	}

	public void handleAddInternalTeammembers() {
		if (addTeammemberControllerBean.validateSelections()) {
			for (ExternalSubjectDataObj teammember : addTeammemberViewBean.getSelectedTeammembers()) {
				teammember.setCurrentRole(addTeammemberViewBean.getSelectedRole());
			}
			teammemberHelper.finalizeTeammemberList(projectSetupViewBean.getInternalTeammembers(),
					addTeammemberViewBean.getSelectedTeammembers());
			addTeammemberViewBean.setSelectedTeammembers(null);
			projectSetupViewBean.setDoSaveInternalTeam(true);
		}
	}

	public void handleInitInternalTeammembers(ActionEvent event) {
		addTeammemberControllerBean.initAvailableTeammembers(EntityType.PROJECT_TYPE, getCurrentProject()
				.getProjectId(), "palmsRealmNames", "", false);
	}

	/* Client Teammembers */

	private void saveClientTeammembers() {
		Integer projectId = currentProject.getProjectId();
		Integer companyId = currentProject.getCompany().getCompanyId();
		teammemberHelper.init(EntityType.PROJECT_TYPE, projectId, "clientRealmName", EntityRule.ALLOW_CHILDREN);
		teammemberHelper.validateRemoveList(getClientRolesList(), projectSetupViewBean.getRemoveClientTeammembers());
		teammemberHelper.removeTeammembers(projectSetupViewBean.getRemoveClientTeammembers());
		teammemberHelper.saveTeammembers(getClientRolesList(), companyId, projectId);
	}

	private List<ExternalSubjectDataObj> fetchClientTeammembers() {
		setClientRolesListExist(true);
		int projectId = sessionBean.getProject().getProjectId();
		return teammemberHelper.fetchTeammembers(EntityType.PROJECT_TYPE, projectId, "clientRealmName");
	}

	public void handleAddClientTeammemebers(ActionEvent event) {
		if (addTeammemberControllerBean.validateSelections()) {
			for (ExternalSubjectDataObj teammember : addTeammemberViewBean.getSelectedTeammembers()) {
				teammember.setCurrentRole(addTeammemberViewBean.getSelectedRole());
			}
			teammemberHelper.finalizeTeammemberList(projectSetupViewBean.getClientTeammembers(),
					addTeammemberViewBean.getSelectedTeammembers());
			setClientRolesList(projectSetupViewBean.getClientTeammembers());
			addTeammemberViewBean.setSelectedTeammembers(null);
			projectSetupViewBean.setDoSaveClientTeam(true);
		}
	}

	public void handleInitClientTeammembers(ActionEvent event) {
		int companyId = sessionBean.getCompany().getCompanyId();
		String filter = "select @ from users u, company c where u.company_id = c.company_id and u.company_id = "
				+ companyId;
		addTeammemberControllerBean.initAvailableTeammembers(EntityType.COMPANY_TYPE, companyId, "clientRealmName",
				filter, true);
	}

	// Validation
	// Need to check to see if this company has the right to create content for
	// this project type
	private Boolean isContentVisibleAndEnabled() {
		ProjectSetupViewBean psvb = getProjectSetupViewBean();
		boolean isContentVisibleAndEnabled = true;

		if (!isContentEnabled(psvb.getProjectTypeId())) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "All content for project type is not enabled.", ""));
			isContentVisibleAndEnabled = false;
		}
		if (!isContentVisible(psvb.getProjectTypeId())) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "All content for this project type is not visible.",
							""));
			isContentVisibleAndEnabled = false;
		}

		return isContentVisibleAndEnabled;
	}

	public Boolean isContentEnabled(Integer projectTypeId) {
		List<CourseGroupTemplate> courseGroupTemplates = courseGroupTemplateDao
				.findCourseGroupTemplatesByProjectTypeId(projectTypeId);
		// List<CourseRoster> courseRosters =
		// sessionBean.getCompany().getCourseRosters();
		List<CourseRoster> courseRosters = courseRosterDao.getCourseRostersByCompanyIdBatchCourse(sessionBean
				.getCompany().getCompanyId());

		// Loop through courses for projectType and courseFlowType
		for (CourseGroupTemplate courseGroupTemplate : courseGroupTemplates) {
			boolean courseExists = false;
			for (CourseRoster courseRoster : courseRosters) {
				if (courseGroupTemplate.getCourse().getAbbv()
						.equalsIgnoreCase(courseRoster.getCourse().getAbbv().toLowerCase())) {
					courseExists = true;
				}
			}
			if (!courseExists) {
				return false;
			}
		}

		return true;
	}

	public Boolean isContentVisible(Integer projectTypeId) {
		List<CourseGroupTemplate> courseGroupTemplates = courseGroupTemplateDao
				.findCourseGroupTemplatesByProjectTypeId(projectTypeId);
		List<CourseVisibility> courseVisibilities = sessionBean.getCompany().getCourseVisiblities();

		// Loop through courses for projectType and courseFlowType
		for (CourseGroupTemplate courseGroupTemplate : courseGroupTemplates) {
			boolean courseExists = false;
			for (CourseVisibility courseVisibility : courseVisibilities) {
				if (courseGroupTemplate.getCourse().getAbbv()
						.equalsIgnoreCase(courseVisibility.getCourse().getAbbv().toLowerCase())) {
					courseExists = true;
				}
			}
			if (!courseExists) {
				return false;
			}
		}

		return true;
	}

	public Boolean hasProduct(String productCode) {
		for (CompanyProduct companyProduct : sessionBean.getCompany().getCompanyProducts()) {
			if (companyProduct.getProductCode().equalsIgnoreCase(productCode.toLowerCase())) {
				return true;
			}
		}

		return false;
	}

	private void renderConfigurationPanel(String projectTypeCode) {

		getProjectSetupViewBean().setRenderConfigurationPanel(true);

		if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
				|| projectTypeCode.equals(ProjectType.ERICSSON)) {

			// Render fields
			getProjectSetupViewBean().setRenderAssignedPM(true);
			getProjectSetupViewBean().setRenderInternalScheduler(true);
			getProjectSetupViewBean().setRenderTargetLevels(true);
			getProjectSetupViewBean().setRenderScoringMethod(true);
			getProjectSetupViewBean().setRenderConfigurationSelection(true);
			getProjectSetupViewBean().setRenderCHQlang(true);
			getProjectSetupViewBean().setRenderAllowReportDownload(false);

			// Setting defaults
			getProjectSetupViewBean().setConfigurationTypeId(-1);
			getProjectSetupViewBean().setScoringMethodId(-1);
			getProjectSetupViewBean().setLanguageCHQ(null);
			getProjectSetupViewBean().setTargetLevelTypeId(
					setupSingleton.getDefaultTargetLevelTypeId(setupSingleton.findProjectTypeByCode(projectTypeCode)
							.getProjectTypeId()));

			// Populate dropdown lists
			this.populateTargetLevelTypes(setupSingleton.findProjectTypeByCode(projectTypeCode).getProjectTypeId());
			this.populateConfigurationTypes();

		} else if (projectTypeCode.equals(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES)
				|| projectTypeCode.equals(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES)
				|| projectTypeCode.equals(ProjectType.TLT_LAG) || projectTypeCode.equals(ProjectType.TLT_LAG_COG)
				|| projectTypeCode.equals(ProjectType.TLTDEMO)) {

			// Render field
			getProjectSetupViewBean().setRenderTargetLevels(true);

			// Setting defaults
			getProjectSetupViewBean().setTargetLevelTypeId(
					setupSingleton.getDefaultTargetLevelTypeId(setupSingleton.findProjectTypeByCode(projectTypeCode)
							.getProjectTypeId()));

			// Populate dropdown list
			this.populateTargetLevelTypes(setupSingleton.findProjectTypeByCode(projectTypeCode).getProjectTypeId());

		} else if (projectTypeCode.equals(ProjectType.KF_ASSMT_POTENTIAL)) {
			// Render fields
			getProjectSetupViewBean().setRenderAssignedPM(false);
			getProjectSetupViewBean().setRenderInternalScheduler(false);
			getProjectSetupViewBean().setRenderTargetLevels(true);
			getProjectSetupViewBean().setRenderScoringMethod(false);
			getProjectSetupViewBean().setRenderConfigurationSelection(false);
			getProjectSetupViewBean().setRenderCHQlang(false);
			// getProjectSetupViewBean().setRenderAllowReportDownload(true);
			getProjectSetupViewBean().setRenderAllowReportDownload(false);

			// Setting defaults
			getProjectSetupViewBean().setScoringMethodId(-1);
			getProjectSetupViewBean().setLanguageCHQ(null);
			getProjectSetupViewBean().setTargetLevelTypeId(
					setupSingleton.getDefaultTargetLevelTypeId(setupSingleton.findProjectTypeByCode(projectTypeCode)
							.getProjectTypeId()));

			// Populate dropdown lists
			this.populateTargetLevelTypes(setupSingleton.findProjectTypeByCode(projectTypeCode).getProjectTypeId());
		}
	}

	public void handleShowProjectSetup() {
		projectSetupViewBean.setShowProjectSetup(true);
		projectSetupViewBean.setShowParticipantInformationFields(false);
	}

	public void handleShowParticipantInfomationFields() {
		projectSetupViewBean.setShowProjectSetup(false);
		projectSetupViewBean.setShowParticipantInformationFields(true);

		if (projectSetupViewBean.isIncludeClientLogonIdPassword() != projectSetupViewBean
				.isOriginalIncludeClientLogonIdPassword()) {
			if (projectSetupViewBean.isIncludeClientLogonIdPassword()) {
				if (getCurrentProject() == null) {
					projectFieldsControllerBean.handleAddLogonFields(0);
				} else {
					projectFieldsControllerBean.handleAddLogonFields(getCurrentProject().getProjectId());
				}
			} else {
				projectFieldsControllerBean.handleRemoveLogonFields();
			}
			projectSetupViewBean.resetOriginalIncludeClientLogonIdPassword();
		}
	}

	// Getters/Setters
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ProjectSetupViewBean getProjectSetupViewBean() {
		return projectSetupViewBean;
	}

	public void setProjectSetupViewBean(ProjectSetupViewBean projectSetupViewBean) {
		this.projectSetupViewBean = projectSetupViewBean;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

	public void setHasParticipants(Boolean hasParticipants) {
		this.hasParticipants = hasParticipants;
	}

	public Boolean getHasParticipants() {
		return hasParticipants;
	}

	/*
	 * This getter used by the required attribute of text fields works by
	 * checking if one of the save buttons is in the request parameter list
	 */

	/*
	public Boolean getIsRequired() {
		System.out.println("In ProjectSetupControllerBean getIsRequired()");
		FacesContext fc = FacesContext.getCurrentInstance();
		String saveAndManageId = clientId("saveAndManage");
		String saveAndReturnId = clientId("saveAndReturn");
		System.out.println("saveAndManageId=" + saveAndManageId);
		System.out.println("saveAndReturnId=" + saveAndReturnId);
		Map<String, String> m = fc.getExternalContext().getRequestParameterMap();
		Boolean retval = m.containsKey(saveAndManageId) || m.containsKey(saveAndReturnId);
		// for (Map.Entry<String, String> entry : m.entrySet())
		// System.out.println("entry="+entry.getKey());
		System.out.println("retval=" + retval);
		return retval;
	}

	private String clientId(String id) {
		FacesContext context = FacesContext.getCurrentInstance();
		UIViewRoot root = context.getViewRoot();
		UIComponent c = findComponent(root, id);
		if (c != null) {
			return c.getClientId(context);
		} else {
			return "";
		}
	}

	private UIComponent findComponent(UIComponent c, String id) {
		if (id.equals(c.getId())) {
			return c;
		}
		Iterator<UIComponent> kids = c.getFacetsAndChildren();
		while (kids.hasNext()) {
			UIComponent found = findComponent(kids.next(), id);
			if (found != null) {
				return found;
			}
		}
		return null;
	}
	*/
	public Boolean getInNew() {
		return inNew;
	}

	public void setInNew(Boolean inNew) {
		this.inNew = inNew;
	}

	public ReportContentRepositoryBean getReportContentRepositoryBean() {
		return reportContentRepositoryBean;
	}

	public void setReportContentRepositoryBean(ReportContentRepositoryBean reportContentRepositoryBean) {
		this.reportContentRepositoryBean = reportContentRepositoryBean;
	}

	public LoginFormBean getLoginFormBean() {
		return loginFormBean;
	}

	public void setLoginFormBean(LoginFormBean loginFormBean) {
		this.loginFormBean = loginFormBean;
	}

	public AddTeammemberControllerBean getAddTeammemberControllerBean() {
		return addTeammemberControllerBean;
	}

	public void setAddTeammemberControllerBean(AddTeammemberControllerBean addTeammemberControllerBean) {
		this.addTeammemberControllerBean = addTeammemberControllerBean;
	}

	public AddTeammemberViewBean getAddTeammemberViewBean() {
		return addTeammemberViewBean;
	}

	public void setAddTeammemberViewBean(AddTeammemberViewBean addTeammemberViewBean) {
		this.addTeammemberViewBean = addTeammemberViewBean;
	}

	public TeammemberHelper getTeammemberHelper() {
		return teammemberHelper;
	}

	public void setTeammemberHelper(TeammemberHelper teammemberHelper) {
		this.teammemberHelper = teammemberHelper;
	}

	public List<ExternalSubjectDataObj> getInternalRolesList() {
		return internalRolesList;
	}

	public void setInternalRolesList(List<ExternalSubjectDataObj> internalRolesList) {
		this.internalRolesList = internalRolesList;
	}

	public List<ExternalSubjectDataObj> getClientRolesList() {
		return clientRolesList;
	}

	public void setClientRolesList(List<ExternalSubjectDataObj> clientRolesList) {
		this.clientRolesList = clientRolesList;
	}

	public boolean isClientRolesListExist() {
		return clientRolesListExist;
	}

	public void setClientRolesListExist(boolean clientRolesListExist) {
		this.clientRolesListExist = clientRolesListExist;
	}

	public boolean isInternalRolesListExist() {
		return internalRolesListExist;
	}

	public void setInternalRolesListExist(boolean internalRolesListExist) {
		this.internalRolesListExist = internalRolesListExist;
	}

	public PalmsSubject getScheduler() {
		return scheduler;
	}

	public void setScheduler(PalmsSubject scheduler) {
		this.scheduler = scheduler;
	}

	public PalmsSubject getPmPc() {
		return pmPc;
	}

	public void setPmPc(PalmsSubject pmPc) {
		this.pmPc = pmPc;
	}

	public ProjectFieldsControllerBean getProjectFieldsControllerBean() {
		return projectFieldsControllerBean;
	}

	public void setProjectFieldsControllerBean(ProjectFieldsControllerBean projectFieldsControllerBean) {
		this.projectFieldsControllerBean = projectFieldsControllerBean;
	}

	public UffdaService getUffdaService() {
		return uffdaService;
	}

	public void setUffdaService(UffdaService uffdaService) {
		this.uffdaService = uffdaService;
	}

	public ProjectFieldsViewBean getProjectFieldsViewBean() {
		return projectFieldsViewBean;
	}

	public void setProjectFieldsViewBean(ProjectFieldsViewBean projectFieldsViewBean) {
		this.projectFieldsViewBean = projectFieldsViewBean;
	}

}
