package com.pdinh.presentation.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.UUID;

import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.form.LoginFormBean;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.presentation.domain.DocumentObj;
import com.pdinh.presentation.domain.FileAttachmentObj;
import com.pdinh.presentation.helper.Utils;
import com.pdinh.presentation.view.DocumentsViewBean;

public abstract class DocumentsControllerBean {

	// private static final Logger auditLog =
	// LoggerFactory.getLogger("AUDIT-ADMIN");
	private static final Logger log = LoggerFactory.getLogger(DocumentsControllerBean.class);
	private FileAttachmentServiceLocal fileAttachmentService;
	private DocumentsViewBean documentsViewBean;
	private Properties appConfigProperties;
	private LoginFormBean loginFormBean;

	public void handleDocumentRowSelect(SelectEvent event) {
		disableRemoveDocuments();
	}

	public void handleDocumentRowUnselect(UnselectEvent event) {
		disableRemoveDocuments();
	}

	private void disableRemoveDocuments() {
		if (documentsViewBean.getSelectedDocuments() != null) {
			if (documentsViewBean.getSelectedDocuments().length > 0) {
				documentsViewBean.setRemoveDocumentsDisabled(false);
			} else {
				documentsViewBean.setRemoveDocumentsDisabled(true);
			}
		} else {
			documentsViewBean.setRemoveDocumentsDisabled(true);
		}
	}

	public void handleDownloadDocument() {
		log.debug("in handleDownloadDocument()");
		FacesContext context = FacesContext.getCurrentInstance();
		String user = context.getExternalContext().getRemoteUser();

		EntityAuditLogger.auditLog.info("{} performed a {} from participant documents panel of participant {}({})",
				new Object[] { user, EntityAuditLogger.OP_DOWNLOAD_USER_DOCUMENT, documentsViewBean.getUsers_id(),
						documentsViewBean.getUsername() });
	}

	public void handleRemoveDocuments() {
		documentsViewBean.setDoSave(true);
		if (documentsViewBean.getSelectedDocuments() != null) {
			for (int i = 0; i < documentsViewBean.getSelectedDocuments().length; i++) {
				DocumentObj document = documentsViewBean.getSelectedDocuments()[i];
				if (documentsViewBean.getDocuments().contains(document)) {
					documentsViewBean.getDocuments().remove(document);
					if (document.isNewDocument()) {
						FileAttachmentObj fao = document.getFileAttachment();
						// delete temp file
						try {
							fileAttachmentService.delete(fao.getPath() + "/" + fao.getName());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
			documentsViewBean.setSelectedDocuments(null);
		}
	}

	public void handleDocumentFileUpload(FileUploadEvent event) {
		System.out.println("IN DocumentsControllerBean.handleFileUpload()");

		UploadedFile file = event.getFile();

		FileAttachmentObj fileAttachment = new FileAttachmentObj();
		fileAttachment.setDateUploaded(new Timestamp(System.currentTimeMillis()));
		fileAttachment.setName(FilenameUtils.getName(file.getFileName()));
		fileAttachment.setPath(UUID.randomUUID().toString());
		fileAttachment.setExtAdminId(loginFormBean.getUsername());
		try {
			fileAttachment.setUrl(getEncodedDocumentUrl(fileAttachment.getPath() + "/" + fileAttachment.getName()));
			fileAttachment.setNewFileInputStream(file.getInputstream());
			documentsViewBean.setLastDocumentFileUploaded(fileAttachment);
			documentsViewBean.setAddDocumentDisabled(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DocumentsViewBean getDocumentsViewBean() {
		return documentsViewBean;
	}

	public void setDocumentsViewBean(DocumentsViewBean documentsViewBean) {
		this.documentsViewBean = documentsViewBean;
	}

	public FileAttachmentServiceLocal getFileAttachmentService() {
		return fileAttachmentService;
	}

	public void setFileAttachmentService(FileAttachmentServiceLocal fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	public Properties getAppConfigProperties() {
		return appConfigProperties;
	}

	public void setAppConfigProperties(Properties appConfigProperties) {
		this.appConfigProperties = appConfigProperties;
	}

	public LoginFormBean getLoginFormBean() {
		return loginFormBean;
	}

	public void setLoginFormBean(LoginFormBean loginFormBean) {
		this.loginFormBean = loginFormBean;
	}

	protected String getEncodedDocumentUrl(String path) {
		String urlEncodedPath = "";
		try {
			urlEncodedPath = URLEncoder.encode(path, "UTF-8").replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			urlEncodedPath = path;
			e.printStackTrace();
		}
		return Utils.getServerAndContextPath() + "/" + appConfigProperties.getProperty("getFileAttachmentServletPath")
				+ "?path=" + urlEncodedPath;
	}

}
