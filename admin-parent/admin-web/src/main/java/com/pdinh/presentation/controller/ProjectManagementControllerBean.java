package com.pdinh.presentation.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.TokenServiceLocal;
import com.pdinh.data.jaxb.lrm.LrmObjectExistResponse;
import com.pdinh.data.jaxb.lrm.LrmObjectServiceLocal;
import com.pdinh.data.ms.dao.CoachingPlanDao;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.TargetLevelTypeDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsException;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.manage.emailtemplates.EmailTemplatesService;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.CoachingPlan;
import com.pdinh.persistence.ms.entity.CoachingPlanStatusType;
import com.pdinh.persistence.ms.entity.ContentToken;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseVersion;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.CoachingPlanObj;
import com.pdinh.presentation.domain.CourseStatusObj;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;
import com.pdinh.presentation.helper.CourseStatusColumnModel;
import com.pdinh.presentation.helper.SelfRegistrationHelper;
import com.pdinh.presentation.helper.TeammemberHelper;
import com.pdinh.presentation.helper.Utils;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.AssignConsultantViewBean;
import com.pdinh.presentation.view.CoachingPlanInfoViewBean;
import com.pdinh.presentation.view.ParticipantInfoViewBean;
import com.pdinh.presentation.view.ParticipantListViewBean;
import com.pdinh.presentation.view.ProjectManagementViewBean;
import com.pdinh.presentation.view.SelfRegistrationConfigurationViewBean;
import com.pdinh.reporting.data.ViaEdgeScoredDataLocal;

@ManagedBean(name = "projectManagementControllerBean")
@ViewScoped
public class ProjectManagementControllerBean implements Serializable {
	final static Logger logger = LoggerFactory.getLogger(ProjectManagementControllerBean.class);

	private final static String[] statusValuesPrework;
	private final static String[] statusLabelsPrework;
	private final static String[] statusValuesAssessmentDay;
	private final static String[] statusLabelsAssessmentDay;
	// private final static String[] courses;
	private final static String[] statusLabelsCoaching;
	private final static String[] viaEdgeConfLevels;
	private final static String[] viaEdgeConfLabels;

	static {
		statusValuesPrework = new String[4];
		statusValuesPrework[0] = "";
		statusValuesPrework[1] = "1";
		statusValuesPrework[2] = "2";
		statusValuesPrework[3] = "3";

		statusLabelsPrework = new String[4];
		statusLabelsPrework[0] = "";
		statusLabelsPrework[1] = "N";
		statusLabelsPrework[2] = "S";
		statusLabelsPrework[3] = "C";

		statusValuesAssessmentDay = new String[5];
		statusValuesAssessmentDay[0] = "";
		statusValuesAssessmentDay[1] = "0";
		statusValuesAssessmentDay[2] = "1";
		statusValuesAssessmentDay[3] = "2";
		statusValuesAssessmentDay[4] = "3";

		statusLabelsAssessmentDay = new String[5];
		statusLabelsAssessmentDay[0] = "";
		statusLabelsAssessmentDay[1] = "-";
		statusLabelsAssessmentDay[2] = "N";
		statusLabelsAssessmentDay[3] = "S";
		statusLabelsAssessmentDay[4] = "C";
		/*
		 * courses = new String[10]; courses[0] = "demo"; courses[1] = "chq";
		 * courses[2] = "gpi"; courses[3] = "gpil"; courses[4] = "lei";
		 * courses[5] = "cs"; courses[6] = "cs2"; courses[7] = "rv"; courses[8]
		 * = "wg"; courses[9] = "finex";
		 */
		statusLabelsCoaching = new String[4];
		statusLabelsCoaching[0] = "Not yet configured";
		statusLabelsCoaching[1] = "Not Started";
		statusLabelsCoaching[2] = "In Progress";
		statusLabelsCoaching[3] = "Completed";

		viaEdgeConfLevels = new String[4];
		viaEdgeConfLevels[0] = "undefined";
		viaEdgeConfLevels[1] = "red"; // ugly (retake)
		viaEdgeConfLevels[2] = "yellow"; // bad
		viaEdgeConfLevels[3] = "green"; // good

		viaEdgeConfLabels = new String[4];
		viaEdgeConfLabels[0] = "";
		viaEdgeConfLabels[1] = "L"; // ugly (retake)
		viaEdgeConfLabels[2] = "M"; // bad
		viaEdgeConfLabels[3] = "H"; // good
	}
	private Map<String, String> viaEdgePartConfs = new HashMap<String, String>();
	private static final long serialVersionUID = 1L;
	private Boolean firstTime = true;
	Project currentProject;
	private Integer projectId;
	private Integer userId;
	private Boolean renderParticipants = false;
	private Boolean renderParticipantsAndOptionals = false;
	private Boolean renderDetailedStatus = true;
	private boolean renderAssignments = false;
	private Integer viewType = 2;
	private final SelectItem[] statusOptionsPrework;
	private SelectItem[] confOptionsPrework;
	private final SelectItem[] statusOptionsAssessmentDay;
	private List<CourseStatusColumnModel> preworkColumns = new ArrayList<CourseStatusColumnModel>();
	private String viaEdgeCourseAbbv;

	private Boolean selectAllRV = false;
	private Boolean selectAllWGE = false;
	private Boolean selectAllFIN = false;
	private Boolean selectAllLVAPM = false;
	private Boolean selectAllLVADR = false;
	private Boolean selectAllLVABM = false;
	private Boolean selectAllLVACI = false;
	private Boolean selectAllLIVSIM = false;
	private Boolean selectAllBRE = false;

	private Boolean displayAdobeSetup = false;
	private List<ProjectCourse> projectCourses;
	private String targetLevel;
	private List<CoachingPlanObj> coachingPlanObjs;
	private boolean isPDAProject = false;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private ProjectUserDao projectUserDao;

	// @EJB
	// private LanguageDao languageDao;

	@EJB
	private UserDao userDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private CoachingPlanDao coachingPlanDao;

	// @EJB
	// private ProjectTypeDao projectTypeDao;

	@EJB
	private ProjectSetupSingleton projectSetupSingleton;

	@EJB
	private TargetLevelTypeDao targetLevelTypeDao;

	@EJB
	private LanguageSingleton languageSingleton;

	@EJB
	private ReportingSingleton reportingSingleton;

	@EJB
	private TokenServiceLocal tokenService;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private LrmObjectServiceLocal lrmObjectServiceLocal;

	@EJB
	private ViaEdgeScoredDataLocal viaEdgeScoredData;

	@EJB
	private EmailTemplatesService emailTemplatesService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@EJB
	MailTemplatesServiceLocalImpl mailTemplatesService;

	@ManagedProperty(name = "projectManagementViewBean", value = "#{projectManagementViewBean}")
	private ProjectManagementViewBean projectManagementViewBean;

	@ManagedProperty(name = "projectSetupControllerBean", value = "#{projectSetupControllerBean}")
	private ProjectSetupControllerBean projectSetupControllerBean;

	@ManagedProperty(name = "participantInfoControllerBean", value = "#{participantInfoControllerBean}")
	private ParticipantInfoControllerBean participantInfoControllerBean;

	@ManagedProperty(name = "participantInfoViewBean", value = "#{participantInfoViewBean}")
	private ParticipantInfoViewBean participantInfoViewBean;

	@ManagedProperty(name = "participantListControllerBean", value = "#{participantListControllerBean}")
	private ParticipantListControllerBean participantListControllerBean;

	@ManagedProperty(name = "participantListViewBean", value = "#{participantListViewBean}")
	private ParticipantListViewBean participantListViewBean;

	@ManagedProperty(name = "projectParticipantsControllerBean", value = "#{projectParticipantsControllerBean}")
	private ProjectParticipantsControllerBean projectParticipantsControllerBean;

	@ManagedProperty(name = "projectDocumentsControllerBean", value = "#{projectDocumentsControllerBean}")
	private ProjectDocumentsControllerBean projectDocumentsControllerBean;

	@ManagedProperty(name = "emailTemplateListControllerBean", value = "#{emailTemplateListControllerBean}")
	private EmailTemplateListControllerBean emailTemplateListControllerBean;

	@ManagedProperty(name = "reportWizardControllerBean", value = "#{reportWizardControllerBean}")
	private ReportWizardControllerBean reportWizardControllerBean;

	@ManagedProperty(name = "coachingPlanInfoViewBean", value = "#{coachingPlanInfoViewBean}")
	private CoachingPlanInfoViewBean coachingPlanInfoViewBean;

	@ManagedProperty(name = "coachingPlanInfoControllerBean", value = "#{coachingPlanInfoControllerBean}")
	private CoachingPlanInfoControllerBean coachingPlanInfoControllerBean;

	@ManagedProperty(name = "projectNormsControllerBean", value = "#{projectNormsControllerBean}")
	private ProjectNormsControllerBean projectNormsControllerBean;

	@ManagedProperty(name = "assignConsultantControllerBean", value = "#{assignConsultantControllerBean}")
	private AssignConsultantControllerBean assignConsultantControllerBean;

	@ManagedProperty(name = "assignConsultantViewBean", value = "#{assignConsultantViewBean}")
	private AssignConsultantViewBean assignConsultantViewBean;

	@ManagedProperty(name = "teammemberHelper", value = "#{teammemberHelper}")
	private TeammemberHelper teammemberHelper;

	@ManagedProperty(name = "selfRegistrationHelper", value = "#{selfRegistrationHelper}")
	private SelfRegistrationHelper selfRegistrationHelper;

	@ManagedProperty(name = "selfRegConfigViewBean", value = "#{selfRegConfigViewBean}")
	private SelfRegistrationConfigurationViewBean selfRegConfigViewBean;

	@Resource(mappedName = "custom/abyd_properties")
	private Properties abydProperties;

	@Resource(mappedName = "custom/tlt_properties")
	private Properties tltProperties;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	public ProjectManagementControllerBean() {
		statusOptionsPrework = createFilterOptions(statusValuesPrework, statusLabelsPrework);
		setConfOptionsPrework(createFilterOptions(viaEdgeConfLevels, viaEdgeConfLabels));
		statusOptionsAssessmentDay = createFilterOptions(statusValuesAssessmentDay, statusLabelsAssessmentDay);
	}

	public void initView() {
		logger.trace("In ProjectManagementControllerBean initView");
		Subject subject = SecurityUtils.getSubject();
		if (firstTime) {
			// JJB: Seems like most effective way that when refresh is done all
			// data id refreshed
			// May be better way to do
			projectDao.evictAll();

			// After add user it refreshes the project management view
			// It then was clearing user so when go to add notes or documents
			// have an issue
			// getSessionBean().clearUser();

			firstTime = false;
			Project project = null;

			if (getProjectId() != null) {
				project = projectDao.findById(getProjectId());
			} else {
				if (getSessionBean().getProject() != null) {
					// JJB: Needed to add to make sure getting latest from DB
					project = projectDao.findById(getSessionBean().getProject().getProjectId());
				}
			}

			if (project != null) {
				getSessionBean().setProject(project);
				getSessionBean().setCompany(project.getCompany());
				getSessionBean().refreshProjects(getSessionBean().isIncludeClosedProjects());
				currentProject = getSessionBean().getProject();
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Project context is missing.", ""));
				return;
			}

			this.targetLevel = projectDao.resolveTargetLevel(currentProject);

			getProjectManagementViewBean().setProjectId(currentProject.getProjectId());
			getProjectManagementViewBean().setProjectName(currentProject.getName());
			getProjectManagementViewBean().setProjectType(currentProject.getProjectType().getName());
			getProjectManagementViewBean().setProjectCode(currentProject.getProjectCode());
			// getProjectManagementViewBean().setFromEmail(currentProject.getFromEmail());
			getProjectManagementViewBean().setRenderUrlGenerator(
					currentProject.getProjectType().getUrlGeneratorVisible());

			Boolean renderTestData = false;
			// NW: This batches the course entities.. much more efficient
			this.projectCourses = projectCourseDao.findProjectCourseByProjectId(currentProject.getProjectId());
			for (ProjectCourse projectCourse : projectCourses) {
				if (projectCourse.getCourse().getHasTestData()) {
					renderTestData = true;
					break;
				}
			}
			getProjectManagementViewBean().setRenderTestData(renderTestData);

			Boolean displayAdobeSetup = false;
			for (ProjectCourse projectCourse : projectCourses) {
				int cType = projectCourse.getCourse().getType();
				if (cType > 50 && cType <= 60) {
					displayAdobeSetup = true;
					break;
				}
			}
			setDisplayAdobeSetup(displayAdobeSetup);

			logger.debug("Project Type: " + currentProject.getProjectType());
			getProjectManagementViewBean().setTargetLevel(targetLevel);
			logger.debug("Target Level: " + targetLevel);
			getProjectManagementViewBean().setCreatedDate(currentProject.getDateCreated());
			getProjectManagementViewBean().setDueDate(currentProject.getDueDate());
			if (currentProject.getExtAdminId() != null) {
				getProjectManagementViewBean().setCreatedBy(currentProject.getExtAdminId());
			} else {
				// sillyness
				// User admin =
				// userDao.findById(currentProject.getAdmin().getUsersId());
				User admin = currentProject.getAdmin();
				if (admin != null) {
					getProjectManagementViewBean().setCreatedBy(admin.getEmail() + " (NHN Admin)");
				} else {
					getProjectManagementViewBean().setCreatedBy("n/a");
				}
			}

			getProjectManagementViewBean().setParticipants(new ArrayList<ParticipantObj>());
			List<ProjectUser> projectUsers = new ArrayList<ProjectUser>();
			if (subject.isPermitted("viewAllUsers")
					|| subject.isPermitted(EntityType.PROJECT_TYPE + ":viewAllUsers:" + currentProject.getProjectId())
					|| subject.isPermitted(EntityType.PROJECT_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
							+ currentProject.getProjectId())
					|| subject.isPermitted(EntityType.COMPANY_TYPE + ":" + EntityRule.ALLOW_CHILDREN + ":"
							+ currentProject.getCompany().getCompanyId())) {

				projectUsers = projectDao.findProjectParticipantsByProjectId(currentProject.getProjectId());
			} else {

				List<Integer> authorizedUsers = entityService.getAuthorizedEntities(sessionBean.getRoleContext(),
						EntityType.PROJECT_USER_TYPE, true);
				projectUsers = projectUserDao.findProjectUsersByIds(currentProject.getProjectId(), authorizedUsers);
			}
			// JJB: Not sure if I need to do this set. Seems to be working.
			currentProject.setProjectUsers(projectUsers);

			// set up dynamic columns
			projectManagementViewBean.getPreworkColumns().clear();
			for (ProjectCourse projectCourse : projectCourses) {
				if (projectCourse.getSelected()) {
					String abv = projectCourse.getCourse().getAbbv();
					if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 1) {
						projectManagementViewBean.getPreworkColumns().add(
								new CourseStatusColumnModel(projectCourse.getCourse().getAbbv(), projectCourse
										.getCourse().getAbbv()));
						// set show viaEDGE status column if vedge
						if (abv.equals("vedge") || abv.equals("vedge_nodemo") || abv.equals("lagil")) {
							viaEdgeCourseAbbv = abv;
							projectManagementViewBean.setRenderViaEdgeStatus(true);
							if (projectUsers.size() > 0) {
								Map<String, String> pConfs = viaEdgeScoredData.getViaEdgeConfidences(projectUsers);
								// change raw scores to display values

								if (pConfs != null) {
									for (Map.Entry<String, String> entry : pConfs.entrySet()) {
										if (!entry.getValue().equals("undefined")) {
											int score = Integer.parseInt(entry.getValue());
											if (score < -5) {
												entry.setValue(viaEdgeConfLevels[1]);
											} else if (score < -2 && score > -6) {
												entry.setValue(viaEdgeConfLevels[2]);
											} else if (score > -3) {
												entry.setValue(viaEdgeConfLevels[3]);
											}
										}
									}
									viaEdgePartConfs = pConfs;
								}
							}
						} else if (abv.equals("bre")) {
							projectManagementViewBean.setRenderBreCrc(true);
						}
						// End of if flow type == 1 (prework)
					} else {
						// Assumes flow type 2 (assessment day) is the only
						// other type. Broken out in case we need to do other
						// things in Asmnt Day instruments in the future
						if (abv.equals("bre")) {
							projectManagementViewBean.setRenderBreCrc(true);
						}
					}
				} // End of if selected
			}
			List<Integer> projectUserIds = new ArrayList<Integer>();
			for (ProjectUser projectUser : projectUsers) {
				// Moved this stuff to setupParticipantObject method which is
				// reused
				// when editing to avoid having to refresh the whole table
				projectUserIds.add(projectUser.getRowId());
				ParticipantObj participant = setupParticipantObject(projectUser, targetLevel);
				logger.trace("Added participant {} {}", participant.getLastName(), participant.getFirstName());

				getProjectManagementViewBean().getParticipants().add(participant);
			}
			projectManagementViewBean.setProjectUserIds(projectUserIds);
			logger.debug("done adding users {} users to view", projectUserIds.size());

			// clear selection and update button sensitivity
			// AV: Why are we clearing participants selections ?!
			// projectManagementViewBean.clearSelectedParticipants();
			updateAddEditSensitivity();

			// put in session a hashmap of distinct languages in projects
			// courses
			saveProjectDistinctLanguages(projectCourses);

			// set rendering of activate instruments and sending emails, need
			// better way
			// to data drive this but at least it's not in th UI anymore
			// int projectTypeId = sessionBean.getProject().getProjectType()
			// .getProjectTypeId();
			String projectTypeCode = sessionBean.getProject().getProjectType().getProjectTypeCode();
			logger.debug("projectTypeCode = " + projectTypeCode);
			// TODO: Get rid of hard-coding here
			getProjectManagementViewBean().setRenderActivateInstruments(true);
			// Add logic to enable the Activate button
			// Logic to activate this button was left out when this flag was
			// added... Adding now
			getProjectManagementViewBean().setActivateInstrumentsDisabled(!isAssessmentDayAggregate());

			// projectTypeId == ProjectType.ASSESSMENT_BY_DESIGN_ONLINE
			// || projectTypeId == ProjectType.READINESS_ASSMT_BUL
			// || projectTypeId == ProjectType.ASSESSMENT_TESTING ||
			// projectTypeId == ProjectType.MLL_LVA);
			// allow sending emails except for AbyD & Coaching
			getProjectManagementViewBean().setRenderSendEmails(
					!(projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
							&& projectTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
							&& projectTypeCode.equals(ProjectType.ERICSSON) && projectTypeCode
							.equals(ProjectType.COACH)));
			getProjectManagementViewBean().setRenderExportStatus(true); // (projectTypeId
																		// !=
																		// Project.COACHING);
			getProjectManagementViewBean().setRenderSetCoachAndTimeframe(projectTypeCode.equals(ProjectType.COACH));
			getProjectManagementViewBean().setRenderPrintCoachingPlan(projectTypeCode.equals(ProjectType.COACH));
			getProjectManagementViewBean().setRenderCreateReports(true);

			getProjectManagementViewBean().setRenderAssignConsultant(projectTypeCode.equals(ProjectType.GLGPS));

			if (projectTypeCode.equals(ProjectType.COACH)) {
				coachingPlanInfoViewBean.setTargetLevelTypes(new ArrayList<TargetLevelTypeObj>());
				ProjectType pt = projectSetupSingleton.findProjectTypeByCode(projectTypeCode);
				// ProjectType pt = projectTypeDao.findById(projectTypeId);
				coachingPlanInfoViewBean.getTargetLevelTypes().add(new TargetLevelTypeObj(-1, "[none]", ""));
				for (TargetLevelGroupType typ : pt.getTargetLevelGroup().getTargetLevelGroupTypes()) {
					coachingPlanInfoViewBean.getTargetLevelTypes().add(
							new TargetLevelTypeObj(typ.getTargetLevelType().getTargetLevelTypeId(), typ
									.getTargetLevelType().getName(), typ.getTargetLevelType().getCode()));
					logger.debug("leader level types {}", typ.getTargetLevelType().getName());
				}
			}

			// reset the report wizard view so it is refreshed every time a new
			// project is viewed
			reportWizardControllerBean.resetFirstTime();

			String base = Utils.getServerAndContextPath() + "/resources/documents";
			projectManagementViewBean.setAdobeConnectHelpFileUrl(base + "/adobeconnect_instructions.doc");
			logger.debug("setAdobeConnectHelpFileUrl {}", projectManagementViewBean.getAdobeConnectHelpFileUrl());

			// coaching plan initialization

			if (projectTypeCode.equals(ProjectType.COACH)) {
				logger.debug("ProjectManagementControllerBean: Startcoaching plan init");
				// NW: This is redundant. do this when click on button.. load
				// page faster
				// coachingPlanInfoControllerBean.initSetCoachAndTimeFrame();
				// coachingPlanInfoControllerBean.initCoaches();
				projectManagementViewBean.setPrintCoachingPlanDisabled(true);
				logger.debug("ProjectManagementControllerBean: END coaching plan init");
			}

			logger.debug("ProjectManagementControllerBean: START norms init");
			// project norms
			if (currentProject.getProjectType().getProjectTypeCode().equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					|| currentProject.getProjectType().getProjectTypeCode().equals(ProjectType.READINESS_ASSMT_BUL)
					|| currentProject.getProjectType().getProjectTypeCode().equals(ProjectType.ASSESSMENT_TESTING)
					|| currentProject.getProjectType().getProjectTypeCode().equals(ProjectType.ERICSSON)) {
				getProjectManagementViewBean().setRenderProjectNorms(true);
				// getProjectManagementViewBean().setProjectNormsDisabled(false);
				getProjectManagementViewBean().setNormHeaderTitle("Set Project Instrument Norms");
				// NW: This is redundant... does this when you click the button
				// projectNormsControllerBean.init();
			}
			logger.debug("ProjectManagementControllerBean: END norms init");

			isPDAProject = this.isLrmProjectExist();
			if (projectTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					|| projectTypeCode.equals(ProjectType.GLGPS) || projectTypeCode.equals(ProjectType.ERICSSON)) {
				if (!(subject.isPermitted("manageDelivery") || subject.isPermitted(EntityType.PROJECT_TYPE
						+ ":manageDelivery:" + currentProject.getProjectId()))) {
					projectManagementViewBean.setManageProjectDeliveryDisabled(true);
				} else {
					projectManagementViewBean.setManageProjectDeliveryDisabled(!isPDAProject);

				}
			}
			// this.applyPermissions();

			participantInfoViewBean.setUser(false);

			this.selectParticipantById(getUserId());

			this.updateSelectAllButton();

			// Set up product flags for rendering flags
			String projTypeCode = currentProject.getProjectType().getProjectTypeCode();
			if (projTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
					|| projTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)
					|| projTypeCode.equals(ProjectType.ERICSSON)) {
				projectManagementViewBean.setIsAbydProduct(true); // (true);
			} else {
				projectManagementViewBean.setIsAbydProduct(false);
			}
			if (projTypeCode.equals(ProjectType.GLGPS)) {
				projectManagementViewBean.setIsGlGpsProduct(true);
			} else {
				projectManagementViewBean.setIsGlGpsProduct(false);
			}

			// Self-Registration logic
			try {
				selfRegConfigViewBean.setConfiguration(selfRegistrationHelper.getSelfRegConfig(project.getProjectId()));
			} catch (PalmsException e) {
				logger.warn("Oops ... {}", e.getMessage());
			}

			logger.debug("ProjectManagementControllerBean: END init");
		}

	}

	private void updateSelectAllButton() {
		if (getProjectManagementViewBean().getParticipants().size() > 0) {
			getProjectManagementViewBean().setSelectAllDisabled(false);
		} else {
			getProjectManagementViewBean().setSelectAllDisabled(true);
		}
	}

	private void applyPermissions() {
		logger.debug("applying permissions....");
		Subject subject = SecurityUtils.getSubject();
		if (!(subject.isPermitted("addSingleParticipantToProject") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":addSingleParticipantToProject:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setAddSingleParticipantDisabled(true);
			logger.debug("Subject does not have add single participant permission. disabled menu button");
		}
		if (!(subject.isPermitted("addExistingParticipantToProject") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":addExistingParticipantToProject:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setAddExistingParticipantDisabled(true);
		}
		if (!(subject.isPermitted("importParticipantsToProject") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":importParticipantsToProject:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setImportParticipantsDisabled(true);
		}
		if (!(subject.isPermitted("modifyParticipantProperties") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":modifyParticipantProperties:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setEditParticipantDisabled(true);
		}
		if (!(subject.isPermitted("viewParticipantTracker") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":viewParticipantTracker:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setTrackParticipantDisabled(true);
		}
		if (!(subject.isPermitted("removePptFromProject") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":removePptFromProject:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setDelParticipantDisabled(true);
		}
		if (!(subject.isPermitted("activateInstruments") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":activateInstruments:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setActivateInstrumentsDisabled(true);
		}
		if (!(subject.isPermitted("accessTestData") || subject.isPermitted(EntityType.PROJECT_TYPE + ":accessTestData:"
				+ currentProject.getProjectId()))) {
			projectManagementViewBean.setRenderTestData(false);
		}
		if (!(subject.isPermitted("createReportRequest") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":createReportRequest:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setCreateReportsDisabled(true);
			projectManagementViewBean.setReportUploadDisabled(true);
		}
		if (!(subject.isPermitted("downloadReports") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":downloadReports:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setDownloadReportDisabled(true);
		}
		if (!(subject.isPermitted("manageDelivery") || subject.isPermitted(EntityType.PROJECT_TYPE + ":manageDelivery:"
				+ currentProject.getProjectId()))) {
			projectManagementViewBean.setManageProjectDeliveryDisabled(true);
		}
		if (!(subject.isPermitted("editDeliverySetup") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":editDeliverySetup:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setDeliverySetupLinkDisabled(true);
		}
		if (!(subject.isPermitted("editProjectSetup") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":editProjectSetup:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setEditProjectButtonDisabled(true);
			projectManagementViewBean.setProjectNormsDisabled(true);
		}
		if (!(subject.isPermitted("accessProjectDocuments") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":accessProjectDocuments:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setManageDocumentsLinkDisabled(true);
		}
		if (!(subject.isPermitted("accessManageProjectEmail") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":accessManageProjectEmail:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setProjectEmailItemDisabled(true);
		}
		if (!(subject.isPermitted("modifyAbyDSetup") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":modifyAbyDSetup:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setAbydSetupDisabled(true);
		}
		if (!(subject.isPermitted("sendClientEmails") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":sendClientEmails:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setSendEmailsButtonDisabled(true);
		}
		if (!(subject.isPermitted("generateAnyReport") || subject.isPermitted("COACHING_PLAN_SUMMARY") || subject
				.isPermitted(EntityType.PROJECT_TYPE + ":COACHING_PLAN_SUMMARY:" + currentProject.getProjectId()))) {
			projectManagementViewBean.setPrintCoachingPlanDisabled(true);
		}
		if (!(subject.isPermitted("manageSelfRegistration") || subject.isPermitted(EntityType.PROJECT_TYPE
				+ ":manageSelfRegistration:" + currentProject.getProjectId()))) {
			logger.trace("Subject is NOT permitted to manage self registration");
			projectManagementViewBean.setManageSelfRegDisabled(true);
		} else {
			logger.trace("Subject IS permitted to manage self registration.");
		}
	}

	public void refreshView() {
		firstTime = true;
		initView();
		logger.debug("EXIT refreshView");
	}

	private void selectParticipantById(Integer userId) {
		User user = null;

		if (userId != null) {
			user = userDao.findById(userId);
		} else {
			user = sessionBean.getUser();
		}

		if (user != null) {
			List<ParticipantObj> participants = projectManagementViewBean.getParticipants();
			ParticipantObj[] ppl = new ParticipantObj[1];
			for (ParticipantObj participant : participants) {
				if (participant.getUserId() == user.getUsersId()) {
					ppl[0] = participant;
					participants.remove(participant);
					participants.add(0, participant);
					break;
				}
			}
			projectManagementViewBean.setSelectedParticipants(ppl);
			sessionBean.setUser(user);
			this.updateAddEditSensitivity();
		}
	}

	private List<CoachingPlanObj> initCoachingPlans(int projectId) {
		logger.debug("Initializing Coaching Plans");
		List<CoachingPlan> coachingPlans = coachingPlanDao.findCoachingPlansByProjectId(projectId);
		List<CoachingPlanObj> cpoList = new ArrayList<CoachingPlanObj>();

		for (CoachingPlan cp : coachingPlans) {
			CoachingPlanObj cpo = new CoachingPlanObj();
			if (cp != null) {
				cpo.setCoachFirstname(cp.getCoach().getFirstname());
				cpo.setCoachLastname(cp.getCoach().getLastname());
				cpo.setStartDate(cp.getStartDate());
				cpo.setEndDate(cp.getEndDate());
				cpo.setTimeElapsed(Utils.getDateDiffPercent(cp.getStartDate(), cp.getEndDate(), new Date()));
				logger.debug("Coaching Time Elapsed: {} %", cpo.getTimeElapsed());
				cpo.setCoachingPlanStatusTypeId(cp.getCoachingPlanStatusType().getCoachingPlanStatusTypeId());
				cpo.setCompletionStatusLabel(statusLabelsCoaching[cpo.getCoachingPlanStatusTypeId() + 1]);
				cpo.setTypeText(cp.getTypeText());
				cpo.setParticipantId(cp.getParticipant().getUsersId());
				try {
					cpo.setTargetLevelTypeId(cp.getTargetLevelType().getTargetLevelTypeId());
					cpo.setTargetLevelTypeLabel(targetLevelTypeDao.findById(cpo.getTargetLevelTypeId()).getCode());
				} catch (Exception e1) {
					cpo.setTargetLevelTypeId(-1);
					cpo.setTargetLevelTypeLabel("");
				}
			} else {
				cpo.setCoachFirstname("");
				cpo.setCoachLastname("");
				cpo.setStartDate(null);
				cpo.setEndDate(null);
				cpo.setTimeElapsed(0);
				cpo.setCoachingPlanStatusTypeId(-1);
				cpo.setCompletionStatusLabel(statusLabelsCoaching[0]);
				cpo.setTypeText("");
				cpo.setTargetLevelTypeId(-1);
				cpo.setTargetLevelTypeLabel("");
			}
			cpoList.add(cpo);
		}
		return cpoList;
	}

	private CoachingPlanObj findCoachingPlanObjForUser(int usersId) {
		for (CoachingPlanObj obj : this.coachingPlanObjs) {
			logger.trace("Participant Id {}, UsersId: {}", obj.getParticipantId(), usersId);
			if (obj.getParticipantId() == usersId) {
				logger.trace("Found coaching plan object for user: {}", usersId);
				return obj;
			}
		}
		return new CoachingPlanObj();
	}

	private ParticipantObj setupParticipantObject(ProjectUser puser, String targetLevel) {

		Project project = puser.getProject();
		User user = puser.getUser();
		ProjectObj projectObj = new ProjectObj(project.getProjectId(), project.getName(), project.getProjectCode(),
				project.getProjectType().getName(), project.getProjectType().getProjectTypeId(), targetLevel,
				project.getExtAdminId(), project.getUsersCount(), project.getDateCreated(), project.getDueDate());

		if (!(project.getTargetLevelType() == null)) {
			projectObj.setTargetLevelIndex(project.getTargetLevelType().getTargetLevelIndex());
		}

		projectObj.setIncludeClientLogonIdPassword(project.isIncludeClientLogonIdPassword());

		Integer userId = user.getUsersId();

		String firstName = user.getFirstname();
		String lastName = user.getLastname();
		String email = user.getEmail();
		String username = user.getUsername();
		boolean coachFlag = user.getCoachFlag();
		Timestamp dateAdded = puser.getDateAdded();
		ParticipantObj participant = new ParticipantObj(userId, firstName, lastName, email, username, dateAdded,
				coachFlag);

		participant.setPassword(user.getPassword());
		participant.setOptional1(user.getOptional1());
		participant.setOptional2(user.getOptional2());
		participant.setOptional3(user.getOptional3());
		participant.setSupervisorName(user.getSupervisorName());
		participant.setSupervisorEmail(user.getSupervisorEmail());
		participant.setLanguageId(user.getLangPref());
		participant.setUserId(userId);
		participant.setRowKey(puser.getRowId());

		// NW: Now Consultants are added to view when you select the assignments
		// view.
		participant.setConsultants(new ArrayList<ExternalSubjectDataObj>());

		participant.setProject(projectObj);

		participant.setInitialEmailDate(userDao.resolveInitialEmail(user, project));
		participant.setReminderEmailDate(userDao.resolveReminderEmail(user, project));

		if (!project.getProjectType().getProjectTypeCode().equals(ProjectType.COACH)) {
			// JJB: Would be better if HashMap
			// Also should build from project_courses

			Map<String, CourseStatusObj> courseStatuses = new HashMap<String, CourseStatusObj>();
			for (ProjectCourse projectCourse : this.projectCourses) {
				String courseAbbv = projectCourse.getCourse().getAbbv();
				courseStatuses.put(courseAbbv, getCourseStatusForInstrument(user.getPositions(), courseAbbv));
			}

			// create the ve_both course type
			CourseStatusObj cso = new CourseStatusObj("ve_both");
			// if there is a noDemo in here, selected (not 999), and not done
			// (not 3), allow viaEDGE to overwrite it (but only it if is done).
			if (courseStatuses.get("vedge_nodemo") != null && courseStatuses.get("vedge_nodemo").getStatus() != 999) {
				cso.setStatus(courseStatuses.get("vedge_nodemo").getStatus());
				// if there is a completed regular one, it can overide the
				// nodemo one for display
				if (courseStatuses.get("vedge_nodemo").getStatus() != 3) {
					CourseStatusObj possible = getCourseStatusForInstrument(user.getPositions(), "vedge");
					int stat = possible.getStatus();
					// Override status if vedge is Started or Complete
					if (possible.getStatus() == 2 || possible.getStatus() == 3) {
						cso.setStatus(stat);
					}
				}
			} else if (courseStatuses.get("vedge") != null) {
				// ve_both = regular ve
				cso.setStatus(courseStatuses.get("vedge").getStatus());
			} else {
				cso.setStatus(999);
			}
			courseStatuses.put("ve_both", cso);

			// AV: They made me do it ...
			if (courseStatuses.get(Course.KFP) != null) {
				Integer courseVersion = this.getCourseVersion(participant.getUserId(), user.getPositions(), Course.KFP);
				if (courseVersion != null) {
					StringBuffer currentCourseAbbrv = new StringBuffer("kfp");
					currentCourseAbbrv.append(courseVersion);
					courseStatuses.put("kfp1", new CourseStatusObj("kfp1", 0));
					courseStatuses.put("kfp2", new CourseStatusObj("kfp2", 0));
					courseStatuses.get(currentCourseAbbrv.toString()).setStatus(
							courseStatuses.get(Course.KFP).getStatus());
				} else {
					logger.error("'{}' course version is null for '{}, {}'", Course.KFP, participant.getLastName(),
							participant.getFirstName());
				}
			}

			participant.setCourseStatuses(courseStatuses);
		} else {
			if (this.coachingPlanObjs == null) {
				this.setCoachingPlanObjs(this.initCoachingPlans(project.getProjectId()));
			}
			participant.setCoachingPlanObj(this.findCoachingPlanObjForUser(userId));
			/*
			CoachingPlanObj cpo = new CoachingPlanObj();
			CoachingPlan cp = coachingPlanDao.findByProjectAndParticipant(project.getProjectId(),
					participant.getUserId());
			if (cp != null) {
				cpo.setCoachFirstname(cp.getCoach().getFirstname());
				cpo.setCoachLastname(cp.getCoach().getLastname());
				cpo.setStartDate(cp.getStartDate());
				cpo.setEndDate(cp.getEndDate());
				cpo.setTimeElapsed(Utils.getDateDiffPercent(cp.getStartDate(), cp.getEndDate(), new Date()));
				logger.debug("Coaching Time Elapsed: {} %", cpo.getTimeElapsed());
				cpo.setCoachingPlanStatusTypeId(cp.getCoachingPlanStatusType().getCoachingPlanStatusTypeId());
				cpo.setCompletionStatusLabel(statusLabelsCoaching[cpo.getCoachingPlanStatusTypeId() + 1]);
				cpo.setTypeText(cp.getTypeText());
				try {
					cpo.setTargetLevelTypeId(cp.getTargetLevelType().getTargetLevelTypeId());
					cpo.setTargetLevelTypeLabel(targetLevelTypeDao.findById(cpo.getTargetLevelTypeId()).getCode());
				} catch (Exception e1) {
					cpo.setTargetLevelTypeId(-1);
					cpo.setTargetLevelTypeLabel("");
				}
			} else {
				cpo.setCoachFirstname("");
				cpo.setCoachLastname("");
				cpo.setStartDate(null);
				cpo.setEndDate(null);
				cpo.setTimeElapsed(0);
				cpo.setCoachingPlanStatusTypeId(-1);
				cpo.setCompletionStatusLabel(statusLabelsCoaching[0]);
				cpo.setTypeText("");
				cpo.setTargetLevelTypeId(-1);
				cpo.setTargetLevelTypeLabel("");
			}
			logger.debug("Coach for {} {}: {} {}",
					new Object[] { lastName, firstName, cpo.getCoachFirstname(), cpo.getCoachLastname() });
			participant.setCoachingPlanObj(cpo);
			*/
		}
		return participant;
	}

	private int getCourseVersion(int userId, List<Position> positions, String courseAbbv) {
		int courseVersionNumber = 1;
		for (Position position : positions) {
			if (position.getUser().getUsersId() == userId && position.getCourse().getAbbv().equals(courseAbbv)) {
				CourseVersion courseVersion = reportingSingleton.getCourseVersionByCompletionDate(
						position.getCompletionStatusDate(), position.getCourse().getCourse());
				if (courseVersion != null) {
					courseVersionNumber = courseVersion.getVersionNumber();
				}
			}
		}
		return courseVersionNumber;
	}

	public CourseStatusObj getCourseStatusForInstrument(List<Position> positions, String courseAbbv) {
		// System.out.println("IN ProjectManagementControllerBean.getCourseStatusForInstrument");
		Iterator<Position> iteratorPos = positions.iterator();
		while (iteratorPos.hasNext()) {
			Position position = iteratorPos.next();

			if (position.getCourse().getAbbv().equals(courseAbbv) && !position.getActivated()) {
				// System.out.println(position.getCourse().getAbbv() +
				// " not activated.");
				return new CourseStatusObj(position.getCourse().getAbbv(), 0);
			} else if (position.getCourse().getAbbv().equals(courseAbbv)) {
				// System.out.println(position.getUser().getEmail() + ";" +
				// position.getUser().getUsersId() + ";" +
				// position.getCourse().getAbbv() + ";" +
				// position.getCompletionStatus());
				return new CourseStatusObj(position.getCourse().getAbbv(), position.getCompletionStatus() + 1);
			}
		}

		return new CourseStatusObj(courseAbbv, 999);
	}

	public Boolean isInParticipantList(List<ParticipantObj> participants, Integer userId) {
		Iterator<ParticipantObj> iterator = participants.iterator();
		while (iterator.hasNext()) {
			ParticipantObj participant = iterator.next();
			if (participant.getUserId() == userId) {
				return true;
			}
		}
		return false;
	}

	public Integer getPreworkStatus(Map<String, CourseStatusObj> courseStatuses) {
		return getAggregateStatus(courseStatuses, 1);
	}

	public Integer getAssessmentDayStatus(Map<String, CourseStatusObj> courseStatuses) {
		return getAggregateStatus(courseStatuses, 2);
	}

	// Iterate through all instruments taking into consideration the ones
	// assigned to project
	// to determine aggregate status. Also need to take into consideration ones
	// assigned prework and assessment day
	public Integer getAggregateStatus(Map<String, CourseStatusObj> courseStatuses, Integer courseFlowTypeId) {

		int notAvailable = 0;
		int notActivated = 0;
		int notStarted = 0;
		int started = 0;
		int completed = 0;
		boolean hasAboth = false;
		if (courseStatuses.get("ve_both") != null && courseStatuses.get("ve_both").getStatus() != 999) {
			hasAboth = true;
		}
		Iterator<Entry<String, CourseStatusObj>> iterator = courseStatuses.entrySet().iterator();
		while (iterator.hasNext()) {
			CourseStatusObj courseStatus = iterator.next().getValue();
			// Need to take into consideration if instrument is in this project
			if (isInstrumentByCourseFlowType(courseStatus.getCourseCode(), courseFlowTypeId)) {

				// see if the viaEdge has been rolled up; if so, use it
				if (hasAboth
						&& (courseStatus.getCourseCode().equals("vedge") || courseStatus.getCourseCode().equals(
								"vedge_nodemo"))) {
					// Use the both
					courseStatus = courseStatuses.get("ve_both");
				}
				if (courseStatus.getStatus() == 999) {
					notAvailable++;
				} else if (courseStatus.getStatus() == 0) {
					notActivated++;
				} else if (courseStatus.getStatus() == 1) {
					notStarted++;
				} else if (courseStatus.getStatus() == 2) {
					started++;
				} else if (courseStatus.getStatus() == 3) {
					completed++;
				}
			}
		}

		int num = getSelectedCoursesCount(courseFlowTypeId);

		// logger.trace("Prework Count: {}; Not Available: {}; Not Started: {}; Started: {}; Completed: {};",
		// num, String.valueOf(notAvailable), String.valueOf(notStarted),
		// String.valueOf(started), String.valueOf(completed));

		if (notAvailable == num) {
			return 999;
		} else if (notActivated > 0) {
			return 0;
		} else if ((completed - notAvailable) == num) {
			return 3;
		} else if (started == 0 && completed == 0) {
			return 1;
		} else {
			return 2;
		}
	}

	public Boolean isPreworkInstrument(String courseCode) {
		return isInstrumentByCourseFlowType(courseCode, 1);
	}

	public Boolean isAssessmentDayInstrument(String courseCode) {
		return isInstrumentByCourseFlowType(courseCode, 2);
	}

	public Boolean isInstrumentByCourseFlowType(String courseCode, int courseFlowTypeId) {
		Iterator<ProjectCourse> iterator = getSessionBean().getProject().getProjectCourses().iterator();
		while (iterator.hasNext()) {
			ProjectCourse projectCourse = iterator.next();
			if (projectCourse.getCourse().getAbbv().equals(courseCode) && projectCourse.getSelected()
					&& projectCourse.getCourseFlowType().getCourseFlowTypeId() == courseFlowTypeId) {
				return true;
			}
		}

		return false;
	}

	public Boolean isInstrumentInProject(String courseCode) {
		Iterator<ProjectCourse> iterator = getSessionBean().getProject().getProjectCourses().iterator();
		while (iterator.hasNext()) {
			ProjectCourse projectCourse = iterator.next();
			if (projectCourse.getCourse().getAbbv().equals(courseCode) && projectCourse.getSelected()) {
				return true;
			}
		}

		return false;
	}

	public Boolean isPreworkAggregate() {
		return isAggregateByCourseFlowType(1);
	}

	public Boolean isAssessmentDayAggregate() {
		return isAggregateByCourseFlowType(2);
	}

	public Boolean isAggregateByCourseFlowType(int courseFlowTypeId) {
		Iterator<ProjectCourse> iterator = this.projectCourses.iterator();
		while (iterator.hasNext()) {
			ProjectCourse projectCourse = iterator.next();
			if (projectCourse.getSelected()
					&& projectCourse.getCourseFlowType().getCourseFlowTypeId() == courseFlowTypeId) {
				return true;
			}
		}

		return false;
	}

	private int getSelectedCoursesCount(Integer courseFlowTypeId) {
		int count = 0;

		Iterator<ProjectCourse> iterator = getSessionBean().getProject().getProjectCourses().iterator();
		while (iterator.hasNext()) {
			ProjectCourse projectCourse = iterator.next();
			if (projectCourse.getSelected()
					&& projectCourse.getCourseFlowType().getCourseFlowTypeId() == courseFlowTypeId) {
				count++;
			}
		}

		return count;
	}

	private SelectItem[] createFilterOptions(String[] values, String[] labels) {
		SelectItem[] options = new SelectItem[values.length];

		for (int i = 0; i < values.length; i++) {
			options[i] = new SelectItem(values[i], labels[i]);
		}

		return options;
	}

	public Boolean renderRV(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "rv");
	}

	public Boolean renderWGE(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "wg");
	}

	public Boolean renderFIN(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "finex");
	}

	public Boolean renderLVAPM(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "lvapm");
	}

	public Boolean renderLVADR(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "lvadr");
	}

	public Boolean renderLVABM(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "lvabm");
	}

	public Boolean renderLVACI(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "lvaci");
	}

	public Boolean renderLIVSIM(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "livsim");
	}

	public Boolean renderBRE(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "bre");
	}

	public Boolean renderVEDGE(Map<String, CourseStatusObj> courseStatuses) {
		return renderCheckbox(courseStatuses, "vedge");
	}

	private Boolean renderCheckbox(Map<String, CourseStatusObj> courseStatuses, String courseCode) {
		// Loop through participant course statuses...look for RV...if
		// courseStatus > 1 then don't render,
		// can deactivate courses if user has not started them yet
		Iterator<Entry<String, CourseStatusObj>> iterator = courseStatuses.entrySet().iterator();
		while (iterator.hasNext()) {
			CourseStatusObj courseStatus = iterator.next().getValue();
			// System.out.println(courseStatus);
			if (courseStatus.getCourseCode().equalsIgnoreCase(courseCode)) {
				if (courseStatus.getStatus() == 0 || courseStatus.getStatus() == 1) {
					return true;
				} else {
					return false;
				}
			}
		}

		return false;
	}

	/*
	 * public String getExcludeColumns(int startCnt) {
	 * logger.debug("In getExcludeColumns: {}", startCnt); Boolean first = true;
	 * int cnt = startCnt; String excludeColumns = ""; for (String courseCode :
	 * courses) { if (!isInstrumentInProject(courseCode)) { if (first) { first =
	 * false; excludeColumns = excludeColumns + String.valueOf(cnt); } else {
	 * excludeColumns = excludeColumns + "," + String.valueOf(cnt); } } cnt++; }
	 *
	 * System.out.println("Exclude Columns: " + excludeColumns); return
	 * excludeColumns; }
	 */
	// Actions
	public void handleChangeProject(AjaxBehaviorEvent event) {
		Integer pid = getSessionBean().getSelectedProject();
		Project project = projectDao.findBatchById(pid);
		sessionBean.setProject(project);
		System.out.println("IN ProjectManagementControllerBean.handleChangeProject, selected project: " + pid);

		refreshView();
	}

	// Change columns dynamically
	public void handleChangeViewType(AjaxBehaviorEvent event) {
		if (viewType == 0) {
			setRenderParticipants(true);
			setRenderParticipantsAndOptionals(false);
			setRenderDetailedStatus(false);
			setRenderAssignments(false);
		} else if (viewType == 1) {
			setRenderParticipants(false);
			setRenderParticipantsAndOptionals(true);
			setRenderDetailedStatus(false);
			setRenderAssignments(false);
		} else if (viewType == 2) {
			setRenderParticipants(false);
			setRenderParticipantsAndOptionals(false);
			setRenderDetailedStatus(true);
			setRenderAssignments(false);
		} else if (viewType == 3) {
			if (!(projectManagementViewBean.isConsultantsLoaded())) {
				this.addConsultantsToView();
				projectManagementViewBean.setConsultantsLoaded(true);
			}
			setRenderParticipants(false);
			setRenderParticipantsAndOptionals(false);
			setRenderDetailedStatus(false);
			setRenderAssignments(true);
		}
	}

	private void addConsultantsToView() {
		List<Integer> puserids = projectManagementViewBean.getProjectUserIds();
		logger.debug("in addConsultantsToView with {} project user ids", puserids.size());
		List<ParticipantObj> participants = projectManagementViewBean.getParticipants();
		logger.debug("Got {} Participants", participants.size());
		List<ExternalSubjectDataObj> consultants = this.getConsultants(puserids);
		logger.debug("Got {} Consultants", consultants.size());
		teammemberHelper.init(EntityType.PROJECT_USER_TYPE, 0, "palmsRealmNames", null);
		List<AuthorizedEntity> entities = teammemberHelper.getEntities(puserids);
		logger.debug("Got {} Entities", entities.size());
		List<ParticipantObj> pptWConsultants = new ArrayList<ParticipantObj>();
		Iterator pptiter = participants.iterator();

		while (pptiter.hasNext()) {
			ParticipantObj ppt = (ParticipantObj) pptiter.next();
			ppt.setConsultants(new ArrayList<ExternalSubjectDataObj>());
			for (AuthorizedEntity entity : entities) {
				logger.trace("Entity id: {}, Ppt rowKey: {}", entity.getEntity_id(), ppt.getRowKey());
				if (entity.getEntity_id() == ppt.getRowKey()) {
					for (ExternalSubjectDataObj obj : consultants) {

						if (entity.getSubject_id() == obj.getSubject_id()) {
							logger.trace("Found matching consultant {} entity {} ppt: {}", obj.getUsername(),
									entity.getEntity_id(), ppt.getEmail());
							ppt.getConsultants().add(obj);
							break;
						}

					}
				}
			}
			pptWConsultants.add(ppt);
		}
		logger.debug("Finished loop");
		projectManagementViewBean.setParticipants(pptWConsultants);
	}

	public void handleParticipantInfoInitForNew(ActionEvent event) {
		getParticipantInfoControllerBean().initForNew();
		getParticipantInfoViewBean().setIsEditAction(false);
		getParticipantInfoViewBean().setPDAProject(isPDAProject);
	}

	public void handleParticipantInfoInitForEdit(ActionEvent event) {
		getParticipantInfoControllerBean().initForEdit();
		getParticipantInfoViewBean().setIsEditAction(true);
	}

	public String handleTrackParticipant() {
		Subject subject = SecurityUtils.getSubject();

		EntityAuditLogger.auditLog.info(
				"{} redirecting to Participant Tracker for company {}({}), project {}({}), participant {}",
				new Object[] { subject.getPrincipal(), sessionBean.getCompany().getCompanyId(),
						sessionBean.getCompany().getCoName(), sessionBean.getProject().getProjectId(),
						sessionBean.getProject().getName(),
						projectManagementViewBean.getSelectedParticipants()[0].getUserName() });

		// TODO: 'returnTo' parameter?
		sessionBean.setLrmProjectId(0);
		sessionBean.setImportJobId(0);
		return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/deliveryManagment.xhtml&sys=lrm&palmsUserId="
				+ projectManagementViewBean.getSelectedParticipants()[0].getUserId();
	}

	public void handleProjectParticipantsInitForSelection(ActionEvent event) {
		/*
		 * FacesContext facesContext = FacesContext.getCurrentInstance();
		 * DataTable dt =
		 * (DataTable)ComponentUtils.findComponent(facesContext.getViewRoot(),
		 * "tblAvailableParticipants"); dt.resetValue();
		 * System.out.println("resetValue on datatable");
		 */
		getProjectParticipantsControllerBean().initForSelection(false);
		getProjectParticipantsControllerBean().setPDAProject(isPDAProject);
	}

	public void handleDeleteParticipants(ActionEvent event) {
		deleteParticipants();

		// update datatable
		/*
		 * FacesContext facesContext = FacesContext.getCurrentInstance();
		 * DataTable dt =
		 * (DataTable)ComponentUtils.findComponent(facesContext.getViewRoot(),
		 * "tblProjectManagement"); dt.resetValue();
		 * System.out.println("resetValue on datatable");
		 */
	}

	public void deleteParticipants() {
		logger.debug("handleDeleteParticipants");

		ParticipantObj[] ppl = projectManagementViewBean.getSelectedParticipants();
		logger.debug("handleDeleteParticipants, size: {}", ppl.length);

		List<ProjectUser> projectUsers = currentProject.getProjectUsers();
		ArrayList<ProjectUser> projectUsersToDelete = new ArrayList<ProjectUser>();

		for (int i = 0; i < ppl.length; ++i) {
			int pplId = ppl[i].getUserId();
			Iterator<ProjectUser> iterator = projectUsers.iterator();
			while (iterator.hasNext()) {
				ProjectUser projectUser = iterator.next();
				int uid = projectUser.getUser().getUsersId();
				logger.trace(" uid: {}", uid);
				if (uid == pplId) {
					projectUsersToDelete.add(projectUser);
				}
			}
		}
		Iterator<ProjectUser> iterator = projectUsersToDelete.iterator();
		while (iterator.hasNext()) {
			ProjectUser projectUser = iterator.next();
			projectUsers.remove(projectUser);
			projectUserDao.delete(projectUser);
		}

		projectDao.update(currentProject);

		projectManagementViewBean.setSelectedParticipants(null);
		sessionBean.clearUser();

		refreshView();
	}

	public void handleRowSelect(SelectEvent event) {
		// Loop through projectUsers until find
		this.projectManagementViewBean.setRetakeBtnDisabled(true);
		for (ParticipantObj partObj : this.projectManagementViewBean.getSelectedParticipants()) {
			if (getViaEdgePartConf(partObj.getUserId()).equals("red")
					&& (partObj.getCourseStatuses().get("ve_both").getStatus()) == 3) {
				this.projectManagementViewBean.setRetakeBtnDisabled(false);
				break;
			}
		}
		ParticipantObj eventPartObj = (ParticipantObj) event.getObject();
		User user = findUserInProject(getSessionBean().getProject(), (eventPartObj).getUserId());
		logger.debug("row select, userid={}", user.getUsersId());
		getSessionBean().setUser(user);
		getSessionBean().updateReportCenterAvailability();
		updateAddEditSensitivity();
		// create URL for printing coaching plan report
		if (sessionBean.getProject().getProjectType().getProjectTypeCode().equals(ProjectType.COACH)) {
			int participantId = sessionBean.getUser().getUsersId();
			String url = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReport?pid=" + participantId
					+ "&type=COACHING_PLAN&phase=30&includeOverview=true&includeGoals=true&includeJournal=false";
			coachingPlanInfoViewBean.setPrintReportUrl(url);
		}
	}

	private User findUserInProject(Project project, Integer userId) {
		Iterator<ProjectUser> iterator = project.getProjectUsers().iterator();
		while (iterator.hasNext()) {
			ProjectUser projectUser = iterator.next();
			if (projectUser.getUser().getUsersId() == userId) {
				return projectUser.getUser();
			}
		}

		return null;
	}

	public void handleRowUnselect(UnselectEvent event) {
		// Enable button only when a single selection
		// TODO: working but relook at this
		ParticipantObj partObj = (ParticipantObj) event.getObject();
		if (getViaEdgePartConf(partObj.getUserId()).equals("red")
				&& (partObj.getCourseStatuses().get("ve_both").getStatus()) == 3) {
			this.projectManagementViewBean.setRetakeBtnDisabled(true);
			for (ParticipantObj po : this.projectManagementViewBean.getSelectedParticipants()) {
				if (getViaEdgePartConf(partObj.getUserId()).equals("red")
						&& (partObj.getCourseStatuses().get("ve_both").getStatus()) == 3 && po != partObj) {
					this.projectManagementViewBean.setRetakeBtnDisabled(false);
					break;
				}
			}
		}

		updateAddEditSensitivity();

		if (getProjectManagementViewBean().getSelectedParticipants() == null
				|| getProjectManagementViewBean().getSelectedParticipants().length == 0) {
			getSessionBean().clearUser();
		} else if (getProjectManagementViewBean().getSelectedParticipants().length == 1) {
			// Since unselected, set the one to popup as the first one in list
			User user = userDao.findById(getProjectManagementViewBean().getSelectedParticipants()[0].getUserId());
			getSessionBean().setUser(user);
		}

	}

	public void handleCoachingPlanSave(ActionEvent event) {
		logger.info("In ProjectManagementControllerBean -> CoachingPlanInfoController save:");
		logger.info("\tCoachingPlanInfoControllerBean: " + coachingPlanInfoControllerBean.toString());
		coachingPlanInfoControllerBean.handleSetCoachAndTimeFrameSave();
		refreshView();
	}

	public String handleEditProject() {
		return "projectSetup.xhtml?faces-redirect=true&create=false";
	}

	public String handleManageDelivery() {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_ACCESS_LRM_PROJECT_MANAGMENT,
						sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
		sessionBean.setLrmProjectId(0);
		sessionBean.setImportJobId(0);
		return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/deliveryManagment.xhtml&sys=lrm";
	}

	public String handleNavManageImports() {
		return "importManagement.xhtml?faces-redirect=true";
	}

	public String handleNavManageEmail() {
		return "projectEmail.xhtml?faces-redirect=true";
	}

	public String handleNavManageParticipantInfoFields() {
		return "projectSetup.xhtml?faces-redirect=true&create=false&pif=true";
	}

	public void handleProjectDocumentsInitForEdit(ActionEvent event) {
		getProjectDocumentsControllerBean().initForEdit();
	}

	public void handleProjectSetupSave(ActionEvent event) {
		getProjectSetupControllerBean().validateAndSave();
		refreshView();
	}

	public void handleParticipantInfoSave(ActionEvent event) {
		logger.debug("IN ProjectManagementControllerBean.handleParticipantInfoSave");

		Boolean isSuccess = participantInfoControllerBean.saveForProject();
		if (isSuccess) {
			logger.debug("isSuccess, refreshView");
			List<ParticipantObj> ppts = projectManagementViewBean.getParticipants();
			User user = sessionBean.getUser();
			int index = 0;
			for (ParticipantObj obj : ppts) {
				if (obj.getUserId() == user.getUsersId()) {
					index = ppts.indexOf(obj);
					ppts.remove(obj);
					logger.debug("Located old record.  Removed {}", obj.getUserName());
					break;
				}
			}
			ProjectUser puser = projectUserDao.findByProjectIdAndUserId(currentProject.getProjectId(),
					user.getUsersId());

			ParticipantObj updatedObj = setupParticipantObject(puser, this.targetLevel);

			// refreshView();

			// keep things in order
			ppts.add(index, updatedObj);

			projectManagementViewBean.setParticipants(ppts);
			participantInfoViewBean.setSaveButtonDisabled(true);
			logger.debug("Added updated ppt to view {}", updatedObj.getUserName());
		}
	}

	public void handleProjectParticipantsSave(ActionEvent event) {
		getProjectParticipantsControllerBean().save();
		refreshView();
	}

	public void handleProjectDocumentsSave(ActionEvent event) {
		getProjectDocumentsControllerBean().save();
		refreshView();
	}

	public void handleProjectNormsInit(ActionEvent event) {
		// projectNormsControllerBean.init();
		ParticipantObj[] ppl = projectManagementViewBean.getSelectedParticipants();
		if (ppl.length > 0) {
			projectManagementViewBean.setNormHeaderTitle("Set Participant Instrument Norms");
			projectNormsControllerBean.initWithParticipants(ppl);

		} else {
			projectManagementViewBean.setNormHeaderTitle("Set Project Instrument Norms");
			projectNormsControllerBean.init();
		}
		logger.debug("set norm header to: {}", projectManagementViewBean.getNormHeaderTitle());
	}

	public void handleProjectNormsSave(ActionEvent event) {
		ParticipantObj[] selectedParticipants = projectManagementViewBean.getSelectedParticipants();
		logger.debug("handleDeleteParticipants, size: {}", selectedParticipants.length);
		Subject subject = SecurityUtils.getSubject();
		if (selectedParticipants.length == 0) {
			projectNormsControllerBean.saveProjectWideNorms();
			EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({})",
					new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_SAVE_PROJECT_NORMS,
							sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
		} else if (selectedParticipants.length > 0) {
			projectNormsControllerBean.saveParticipantNorms(selectedParticipants);
			EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({}) for {} participants",
					new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_SAVE_PARTICIPANT_NORMS,
							sessionBean.getProject().getProjectId(), sessionBean.getProject().getName(),
							selectedParticipants.length });
		}

	}

	// TODO: Get rid of hardcoding
	public void handleActivateInstrumentsInit(ActionEvent event) {
		selectAllRV = false;
		selectAllWGE = false;
		selectAllFIN = false;
		selectAllLVAPM = false;
		selectAllLVADR = false;
		selectAllLVABM = false;
		selectAllLVACI = false;
		selectAllLIVSIM = false;
		selectAllBRE = false;

		ParticipantObj[] participants = getProjectManagementViewBean().getSelectedParticipants();
		int i;
		for (i = 0; i < participants.length; i++) {
			ParticipantObj participant = participants[i];
			Map<String, CourseStatusObj> courseStatuses = participant.getCourseStatuses();
			Iterator<Entry<String, CourseStatusObj>> iterator = courseStatuses.entrySet().iterator();
			while (iterator.hasNext()) {
				CourseStatusObj courseStatus = iterator.next().getValue();
				if (courseStatus.getStatus() == 0) {
					courseStatus.setActivated(false);
				} else {
					courseStatus.setActivated(true);
				}
			}
		}
	}

	public void handleExportInit(ActionEvent event) {
		Subject subject = SecurityUtils.getSubject();
		getProjectManagementViewBean().setExportParticipants(null);
		getProjectManagementViewBean().setExportParticipants(new ArrayList<ParticipantObj>());

		ParticipantObj[] participants = getProjectManagementViewBean().getSelectedParticipants();
		int i;

		for (i = 0; i < participants.length; i++) {
			ParticipantObj participant = participants[i];

			getProjectManagementViewBean().getExportParticipants().add(participant);

		}

		EntityAuditLogger.auditLog.info("{} performed {} operation on {} participants in project {}({}). ",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_EXPORT_STATUS, participants.length,
						sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
	}

	public void handleSendEmailsInit() {
		emailTemplateListControllerBean.refreshView();
	}

	public void handleSendEmails(ActionEvent event) {
		emailTemplateListControllerBean.sendEmailToParticipants(getProjectManagementViewBean()
				.getSelectedParticipants());
		refreshView();
	}

	public void removeSelectedParticipant(ParticipantObj p) {
		ParticipantObj[] sp = projectManagementViewBean.getSelectedParticipants();
		sp = ArrayUtils.removeElement(sp, p);
		projectManagementViewBean.setSelectedParticipants(sp);
	}

	public void handleRepeatViaEdge(ParticipantObj[] peas) {
		Subject subject = SecurityUtils.getSubject();
		String icUrl = configProperties.getProperty("tinCanAdminBaseUrl") + "/RetakeServlet?participantIds=";
		int pNum = peas.length;
		int count = 0;
		if (pNum > 0) {
			for (int i = 0; i < pNum; i++) {
				if (getPreworkStatus(peas[i].getCourseStatuses()) == 3
						&& getViaEdgePartConf(peas[i].getUserId()).equals("red")) {
					icUrl += peas[i].getUserId();
					count++;
					EntityAuditLogger.auditLog.info("{} requesting {} operation on participant {}({})", new Object[] {
							subject.getPrincipal(), "PARTICIPANT_RETAKE", peas[i].getUserId(),
							sessionBean.getProject().getName() });
					if (i + 1 != pNum) {
						icUrl += ",";
					}
				} else {
					removeSelectedParticipant(peas[i]);
				}
			}
		}
		if (count == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Error- Cannot retake",
					"There were no users with a Low 'red' confidence score eligible for retake."));
		} else {
			if (viaEdgeCourseAbbv != null) {
				icUrl += "&instrumentId=" + viaEdgeCourseAbbv;
			}
			try {
				URL url = new URL(icUrl);
				logger.debug("RETAKE SERVLET URL = " + url);
				URLConnection conn;
				conn = url.openConnection();
				conn.setDoOutput(true);
				conn.setDefaultUseCaches(true);
				BufferedWriter bwOut = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
				bwOut.flush();
				bwOut.close();
				String inp;
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				String respStr = "";
				while ((inp = in.readLine()) != null) {
					respStr += inp;
				}
				EntityAuditLogger.auditLog.info("{} requested {} operation and result was {}",
						new Object[] { subject.getPrincipal(), "PARTICIPANT_RETAKE", respStr });
				in.close();
				handleSendEmailsInit();
				for (EmailTemplateObj etb : emailTemplateListControllerBean.getEmailTemplatesListCompViewBean()
						.getTemplates()) {
					System.out.println(etb.toString());
					// select viaEDGE standard retake template
					if (etb.getId() == 327) { // ("viaEDGE Initial Participant Notification")
						this.emailTemplateListControllerBean.setSelectedEmailTemplate(etb);
					}
				}
			} catch (IOException e) {
				EntityAuditLogger.auditLog.info("{} requested {} operation and result was {}",
						new Object[] { subject.getPrincipal(), "PARTICIPANT_RETAKE", "ERROR ERROR ERROR!" });
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage("Error-",
						"Tincan admin failed to complete the retake request."));
				e.printStackTrace();
			}
		}
	}

	public void handleActivateInstruments(ActionEvent event) {
		Subject subject = SecurityUtils.getSubject();

		// Loop through selected participants and any marked activate set status
		// to not started
		logger.debug("In handleActivateInstruments");
		ParticipantObj[] participants = getProjectManagementViewBean().getSelectedParticipants();
		int i;
		for (i = 0; i < participants.length; i++) {
			ParticipantObj participant = participants[i];
			Map<String, CourseStatusObj> courseStatuses = participant.getCourseStatuses();
			Iterator<Entry<String, CourseStatusObj>> iterator = courseStatuses.entrySet().iterator();
			while (iterator.hasNext()) {
				CourseStatusObj courseStatus = iterator.next().getValue();

				// activated checkbox checked, so set appropriate status, if
				// checkbox not checked
				// and instrument not started, deactivate it
				if (courseStatus.getActivated()) {
					// Only activate if not activated
					logger.debug("ACTIVATED: true -- courseStatus {}, {}", courseStatus.getCourseCode(),
							courseStatus.toString());
					if (courseStatus.getStatus() == 0) {
						setInstrumentActivated(participant.getUserId(), courseStatus.getCourseCode(), true);
						courseStatus.setStatus(1);
						EntityAuditLogger.auditLog.info("{} performed {} operation on user {}, course {}",
								new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_ACTIVATE_INSTRUMENT,
										participant.getUserId(), courseStatus.getCourseCode() });
					}
				} else if (courseStatus.getActivated() == false) {
					logger.debug("ACTIVATED: *false* -- courseStatus {}, {}", courseStatus.getCourseCode(),
							courseStatus.toString());
					if (courseStatus.getStatus() == 1) {
						setInstrumentActivated(participant.getUserId(), courseStatus.getCourseCode(), false);
						courseStatus.setStatus(0);
						EntityAuditLogger.auditLog.info("{} performed {} operation on user {}, course {}",
								new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_DEACTIVATE_INSTRUMENT,
										participant.getUserId(), courseStatus.getCourseCode() });
					}
				}
			}
		}
	}

	public void handleAssignConsultantInit(ActionEvent event) {
		logger.debug("Selected {} participants", getProjectManagementViewBean().getSelectedParticipants().length);
		this.addConsultantsToView();
		assignConsultantViewBean.setParticipants(Arrays
				.asList(getProjectManagementViewBean().getSelectedParticipants()));
		assignConsultantControllerBean.initAvailableConsultants();
	}

	public void handleAssignConsultant(ActionEvent event) {
		assignConsultantControllerBean.save();
		this.addConsultantsToView();
		// Nw: refreshing the entire view is overkill.. just update the one
		// ppt's consultants
		// refreshView();
	}

	public void handleClickTestData(int participantUserId) {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation on user {}, in project {}({})", new Object[] {
				subject.getPrincipal(), EntityAuditLogger.OP_ACCESS_TEST_DATA, participantUserId,
				sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
	}

	public void handleClickUrlGenerator(int participantUserId) {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation on user {}, in project {}({})", new Object[] {
				subject.getPrincipal(), EntityAuditLogger.OP_ACCESS_URL_GENERATOR, participantUserId,
				sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
	}

	public void handleClickBreReport(int participantUserId) {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation on user {}, in project {}({})", new Object[] {
				subject.getPrincipal(), EntityAuditLogger.OP_DOWNLOAD_REPORT, participantUserId,
				sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
	}

	public void handleClickAbyDSetup() {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({}",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_ACCESS_ABYD_SETUP,
						sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
	}

	public String handleClickDeliverySetup() {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation on project {}({})",
				new Object[] { subject.getPrincipal(), EntityAuditLogger.OP_ACCESS_LRM_SETUP,
						sessionBean.getProject().getProjectId(), sessionBean.getProject().getName() });
		sessionBean.setLrmProjectId(0);
		sessionBean.setImportJobId(0);
		return "postTo.xhtml?faces-redirect=true&path=/lrmweb/faces/deliverySetup.xhtml&sys=lrm&returnTo=admin_proj_mgmt";
	}

	public void handleRetakeClick(int i, String f, String l) {
		projectManagementViewBean.setRetakeParticipantName(f, l);
		projectManagementViewBean.setRetakeParticipantId(i);
	}

	// public void handleSendRepeatViaEdgeEmail(int partId) {
	// emailTemplateListControllerBean.initForReadOnlyView();
	// FacesContext context = FacesContext.getCurrentInstance();
	// for (EmailTemplateObj etb :
	// emailTemplateListControllerBean.getEmailTemplatesListCompViewBean().getTemplates())
	// {
	// System.out.println(etb.toString());
	// // TODO: is this value as constant anywhere? change value to that of
	// // new viaEDGE standard retake template
	// if (etb.getTitle().equals("viaEDGE Initial Participant Notification")) {
	// String lang = sessionBean.getCompany().getLangPref();
	// EmailTextObj templateText =
	// emailTemplatesService.findEmailTextByLangCode(etb, lang);
	// if (templateText != null) {
	// ParticipantObj[] participants =
	// getProjectManagementViewBean().getSelectedParticipants();
	// int i;
	// ParticipantObj participantObj;
	// for (i = 0; i < participants.length; i++) {
	// ParticipantObj participant = participants[i];
	// if (participant.getUserId().equals(partId)) {
	// participantObj = participant;
	// String emailUrl =
	// getTltProperties().getProperty("tltParticipantExperienceUrl",
	// "http://participant.experience.url")
	// + "?lang=" + lang;
	// mailTemplatesService.sendTemplate(templateText.getId(), participantObj,
	// sessionBean.getProject(), sessionBean.getCompany(), emailUrl);
	// break;
	// }
	// }
	//
	// } else {
	// context.addMessage(null, new FacesMessage("Error- Email Not Sent",
	// "We could not find the viaEDGE standard retake email template. Try using the 'Send Email"));
	// }
	// }
	// }
	// }

	/*
	 * -- some obsolete code for calling scorm tracking public void
	 * handleTrack() { // call servlet! String trackingToken =
	 * participantInfoViewBean.getXmlPostTrackingToken(); String courseToken =
	 * participantInfoViewBean.getXmlPostCourseId(); String xml =
	 * participantInfoViewBean.getXmlPostXmlContent(); String reqParameters =
	 * "?activity=" + courseToken + "&trackingToken=" + trackingToken; String
	 * req = configProperties.getProperty("scormXmlServlet");
	 *
	 * ContentToken ct = contentTokenDao.findByGuid(trackingToken);
	 * logger.debug("Handle track params: xml-\n {}\nparams-\n{}\nct dto:\n",
	 * new Object[]{xml, reqParameters, ct.toString()});
	 *
	 * try { URL url = new URL(req + reqParameters); HttpURLConnection
	 * connection = (HttpURLConnection) url.openConnection();
	 * connection.setDoOutput(true); connection.setDoInput(true);
	 * connection.setInstanceFollowRedirects(false);
	 * connection.setRequestMethod("POST");
	 * connection.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
	 * connection.setRequestProperty("Content-Length", "" +
	 * Integer.toString(xml.getBytes().length)); connection.setUseCaches(false);
	 *
	 * // DataOutputStream wr = new //
	 * DataOutputStream(connection.getOutputStream()); // wr.writeBytes(xml); //
	 * wr.writeBytes(reqParameters); // wr.flush();
	 *
	 * OutputStream out = connection.getOutputStream(); Writer writer = new
	 * OutputStreamWriter(out, "UTF-8"); writer.write(xml); writer.flush();
	 * writer.close();
	 *
	 * InputStreamReader isr = new
	 * InputStreamReader(connection.getInputStream()); BufferedReader br = new
	 * BufferedReader(isr); String output = ""; String line = ""; while ((line =
	 * br.readLine()) != null) { output += line; } System.out.println("output: "
	 * + output); System.out.println("len: " + output.length());
	 * participantInfoViewBean.setXmlPostOutput(output);
	 * connection.disconnect(); if (output.contains("errorCode")) {
	 * RequestContext requestContext = RequestContext.getCurrentInstance();
	 * requestContext.addCallbackParam("notValid", true); }
	 *
	 * } catch (Exception e) { e.printStackTrace(); } }
	 */
	public void setInstrumentActivated(Integer userId, String courseCode, boolean activated) {
		System.out.println("In activateInstrument for " + userId + "; courseCode: " + courseCode + "; activated: "
				+ activated);
		User user = findUserInProject(sessionBean.getProject(), userId);
		Iterator<Position> iterator = user.getPositions().iterator();
		while (iterator.hasNext()) {
			Position position = iterator.next();
			System.out.println("Position activated: " + position.getActivated().toString());
			if (position.getCourse().getAbbv().equals(courseCode)) {
				position.setActivated(activated);
				positionDao.update(position);
			}
		}
	}

	private List<ExternalSubjectDataObj> getConsultants(List<Integer> projectUserIds) {
		teammemberHelper.init(EntityType.PROJECT_USER_TYPE, 0, "palmsRealmNames", EntityRule.ALLOW);
		List<ExternalSubjectDataObj> consultants = teammemberHelper.getSubjectsWithEntities(projectUserIds,
				EntityType.PROJECT_USER_TYPE);
		return consultants;
	}

	public void handleCheckboxWGE(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxWGE");
		// selectAllWGE = !selectAllWGE;
		activateSelectAll("wg", selectAllWGE);
	}

	public void handleCheckboxRV(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxRV");
		// selectAllRV = !selectAllRV;
		activateSelectAll("rv", selectAllRV);
	}

	public void handleCheckboxFIN(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxFIN");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("finex", selectAllFIN);
	}

	public void handleCheckboxLVAPM(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxLVAPM");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("lvapm", selectAllLVAPM);
	}

	public void handleCheckboxLVADR(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxLVADR");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("lvadr", selectAllLVADR);
	}

	public void handleCheckboxLIVSIM(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxLIVSIM");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("livsim", selectAllLIVSIM);
	}

	public void handleCheckboxLVABM(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxLVABM");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("lvabm", selectAllLVABM);
	}

	public void handleCheckboxLVACI(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxLVACI");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("lvaci", selectAllLVACI);
	}

	public void handleCheckboxBRE(AjaxBehaviorEvent event) {
		System.out.println("In handleCheckboxBRE");
		// selectAllFIN = !selectAllFIN;
		activateSelectAll("bre", selectAllBRE);
	}

	public void activateSelectAll(String courseAbbv, Boolean selectAll) {
		System.out.println("In activateSelectAll for " + courseAbbv);
		ParticipantObj[] participants = getProjectManagementViewBean().getSelectedParticipants();
		int i;
		for (i = 0; i < participants.length; i++) {
			ParticipantObj participant = participants[i];
			Map<String, CourseStatusObj> courseStatuses = participant.getCourseStatuses();
			Iterator<Entry<String, CourseStatusObj>> iterator = courseStatuses.entrySet().iterator();
			while (iterator.hasNext()) {
				CourseStatusObj courseStatus = iterator.next().getValue();
				System.out.println(courseStatus.getCourseCode() + "; " + courseStatus.getActivated() + "; "
						+ courseStatus.getStatus());
				if (courseStatus.getCourseCode().equalsIgnoreCase(courseAbbv)
						&& renderCheckbox(courseStatuses, courseAbbv)) {
					courseStatus.setActivated(selectAll);
				}
			}
		}
	}

	public void handleAddToClipboard(ActionEvent event) {
		addToClipboard(event);
	}

	private void addToClipboard(ActionEvent event) {
		ParticipantObj[] participants = getProjectManagementViewBean().getSelectedParticipants();
		for (ParticipantObj p : participants) {
			sessionBean.addSelectedUser(p);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		StringBuffer detail = new StringBuffer();
		detail.append(participants.length + " participant" + (participants.length != 1 ? "s" : "") + " added.");
		context.addMessage(null, new FacesMessage("Clipboard update, " + detail, detail.toString()));
	}

	public void handleSelectAll(ActionEvent event) {

		if (getProjectManagementViewBean().isSelectAll()) {

			ParticipantObj[] parts = new ParticipantObj[getProjectManagementViewBean().getParticipants().size()];
			Iterator<ParticipantObj> iterator = getProjectManagementViewBean().getParticipants().iterator();

			int i = 0;

			while (iterator.hasNext()) {
				ParticipantObj part = iterator.next();
				parts[i] = part;
				i++;
			}

			getProjectManagementViewBean().setSelectedParticipants(parts);
			getProjectManagementViewBean().setSelectAll(false);

		} else {
			getProjectManagementViewBean().setSelectedParticipants(null);
			getProjectManagementViewBean().setSelectAll(true);
		}

		updateAddEditSensitivity();

	}

	public String goToDownloadReports() {
		if (projectManagementViewBean.getSelectedParticipants() == null
				|| projectManagementViewBean.getSelectedParticipants().length > 1) {
			return sessionBean.goToDownloadReports(sessionBean.getSelectedProject());
		} else {
			return sessionBean.goToDownloadReports(0);
		}
	}

	public void handleAutoScore() {
		getParticipantInfoControllerBean().initForEdit();
	}

	public void updateAddEditSensitivity() {
		int selLen = 0;
		ParticipantObj[] ppl = getProjectManagementViewBean().getSelectedParticipants();
		if (ppl != null)
			selLen = ppl.length;
		logger.debug("  selection length =  {}", selLen);

		if (selLen > 0) {
			if (selLen == 1) {

				getProjectManagementViewBean().setLogInDisabled(false);

				// if
				// (projTypeCode.equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)
				// || projTypeCode.equals(ProjectType.GLGPS)
				// || projTypeCode.equals(ProjectType.READINESS_ASSMT_BUL)) {
				// getProjectManagementViewBean().setTrackParticipantDisabled(false);
				// } else {
				// getProjectManagementViewBean().setTrackParticipantDisabled(true);
				// }
				int projId = sessionBean.getProject().getProjectId();
				int pptId = getProjectManagementViewBean().getSelectedParticipants()[0].getUserId();
				boolean exist = isLrmAsmtExist(projId, pptId);
				getProjectManagementViewBean().setTrackParticipantDisabled(!exist);

				String projTypeCode = sessionBean.getProject().getProjectType().getProjectTypeCode();

				// check to see if coaching plan is complete and printable
				if (projTypeCode.equals(ProjectType.COACH)) {
					int projectId = sessionBean.getProject().getProjectId();
					int participantId = sessionBean.getUser().getUsersId();
					CoachingPlan coachingPlan = coachingPlanDao.findByProjectAndParticipant(projectId, participantId);
					if (coachingPlan != null
							&& coachingPlan.getCoachingPlanStatusType().getCoachingPlanStatusTypeId() != CoachingPlanStatusType.NOT_STARTED) {
						getProjectManagementViewBean().setPrintCoachingPlanDisabled(false);
					} else {
						getProjectManagementViewBean().setPrintCoachingPlanDisabled(true);
					}
					getProjectManagementViewBean().setSetCoachAndTimeframeDisabled(false);
					// END: check to see if coaching plan is complete and
					// printable
				}
			} else {
				getProjectManagementViewBean().setTrackParticipantDisabled(true);
				getProjectManagementViewBean().setPrintCoachingPlanDisabled(true);
				getProjectManagementViewBean().setSetCoachAndTimeframeDisabled(true);
				getProjectManagementViewBean().setLogInDisabled(true);

			}
			getProjectManagementViewBean().setEditParticipantDisabled(false);
			getProjectManagementViewBean().setDelParticipantDisabled(false);
			getProjectManagementViewBean().setAddParticipantsToClipboardDisabled(false);
			getProjectManagementViewBean().setAssignConsultantDisabled(false);
			getProjectManagementViewBean().setReportUploadDisabled(false);
			getProjectManagementViewBean().setCreateReportsDisabled(false);
		} else {
			getProjectManagementViewBean().setEditParticipantDisabled(true);
			getProjectManagementViewBean().setTrackParticipantDisabled(true);
			getProjectManagementViewBean().setDelParticipantDisabled(true);
			getProjectManagementViewBean().setAddParticipantsToClipboardDisabled(true);
			getProjectManagementViewBean().setPrintCoachingPlanDisabled(true);
			getProjectManagementViewBean().setPrintCoachingPlanDisabled(true);
			getProjectManagementViewBean().setSetCoachAndTimeframeDisabled(true);
			getProjectManagementViewBean().setAssignConsultantDisabled(true);
			getProjectManagementViewBean().setReportUploadDisabled(true);
			getProjectManagementViewBean().setCreateReportsDisabled(true);
		}
		this.applyPermissions();
	}

	public void saveProjectDistinctLanguages(List<ProjectCourse> projectCourses) {
		logger.debug("IN saveProjectDistinctLanguages");
		HashMap<String, String> langsMap = new HashMap<String, String>();
		// Iterator<ProjectCourse> iterator =
		// getSessionBean().getProject().getProjectCourses().iterator();
		Iterator<ProjectCourse> iterator = projectCourses.iterator();
		List<String> courseAbbvs = new ArrayList<String>();
		while (iterator.hasNext()) {
			ProjectCourse projectCourse = iterator.next();

			if (projectCourse.getSelected()) {
				courseAbbvs.add(projectCourse.getCourse().getAbbv());
				/*
				 * Course c = projectCourse.getCourse(); List<Language>
				 * courseLangList = languageDao.findByCourseAbbv(c.getAbbv());
				 * for (Language lang : courseLangList) {
				 * langsMap.put(lang.getCode(), lang.getName()); }
				 */
			}
		}
		langsMap = languageSingleton.getDistinctLanguageList(courseAbbvs);
		getSessionBean().setProjectLanguages(langsMap);
	}

	public void loginParticipant() {
		int usersId = getSessionBean().getUser().getUsersId();
		int companyId = getSessionBean().getUser().getCompany().getCompanyId();
		String contentId = "assessment";
		String username = getSessionBean().getUser().getUsername();
		ContentToken contentToken = tokenService.generateToken(usersId, companyId, contentId);
		String token = contentToken.getToken();
		getProjectManagementViewBean().setParticipantLoginUrl(
				configProperties.getProperty("participantPortalUrl") + "/faces/tokenLogin.xhtml");
		getProjectManagementViewBean().setLoginToken(token);
		getProjectManagementViewBean().setLoginUsername(username);
		logger.debug("Login URL: {}", getProjectManagementViewBean().getParticipantLoginUrl());
	}

	private boolean isLrmProjectExist() {
		boolean exist = false;
		int projectId = getSessionBean().getProject().getProjectId();
		LrmObjectExistResponse prjExtResp = null;
		try {
			prjExtResp = lrmObjectServiceLocal.projectExist(String.valueOf(projectId));
		} catch (Exception e) {
			return false;
		}

		if (prjExtResp.getRequestResult().equalsIgnoreCase("SUCCESS") && prjExtResp.isExist()) {
			exist = true;
		}
		return exist;
	}

	/**
	 * doesLrmAsmtExist - Calls a service to see if an LRM Assessment object
	 * exists for the passed project and participant
	 * 
	 * @return boolean - True id assessment exists
	 */
	private boolean isLrmAsmtExist(int projectId, int partId) {
		boolean exist = false;
		LrmObjectExistResponse existResp = null;
		try {
			existResp = lrmObjectServiceLocal.assessmentExist(String.valueOf(projectId), String.valueOf(partId));
		} catch (Exception e) {
			return false;
		}

		if (existResp.getRequestResult().equalsIgnoreCase("SUCCESS") && existResp.isExist()) {
			exist = true;
		}
		return exist;
	}

	public String getViaEdgePartConf(int pid) {
		String conf = "";
		if (viaEdgePartConfs.size() > 0) {
			for (Entry<String, String> entry : this.viaEdgePartConfs.entrySet()) {
				if (pid == Integer.parseInt(entry.getKey())) {
					conf = entry.getValue();
				}
			}
		}
		return conf;
	}

	public String getViaEdgePartConfIdx(int pid) {
		String conf = "";
		if (viaEdgePartConfs.size() > 0) {
			for (Entry<String, String> entry : this.viaEdgePartConfs.entrySet()) {
				if (pid == Integer.parseInt(entry.getKey())) {
					for (int i = 0; 1 < viaEdgeConfLevels.length; i++) {
						if (viaEdgeConfLevels[i].equals(entry.getValue())) {
							return viaEdgeConfLabels[i];
						}
					}
				}
			}
		}
		return conf;
	}

	public String getCourseStatusLabel(String s) {
		// status labels coaching contains spelled out statuses that are generic
		return statusLabelsCoaching[Integer.parseInt(s)];
	}

	// Getters/Setters
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ProjectManagementViewBean getProjectManagementViewBean() {
		return projectManagementViewBean;
	}

	public void setProjectManagementViewBean(ProjectManagementViewBean projectManagementViewBean) {
		this.projectManagementViewBean = projectManagementViewBean;
	}

	public ProjectSetupControllerBean getProjectSetupControllerBean() {
		return projectSetupControllerBean;
	}

	public void setProjectSetupControllerBean(ProjectSetupControllerBean projectSetupControllerBean) {
		this.projectSetupControllerBean = projectSetupControllerBean;
	}

	public void setParticipantInfoControllerBean(ParticipantInfoControllerBean participantInfoControllerBean) {
		this.participantInfoControllerBean = participantInfoControllerBean;
	}

	public ParticipantInfoControllerBean getParticipantInfoControllerBean() {
		return participantInfoControllerBean;
	}

	public void setProjectParticipantsControllerBean(ProjectParticipantsControllerBean projectParticipantsControllerBean) {
		this.projectParticipantsControllerBean = projectParticipantsControllerBean;
	}

	public ProjectParticipantsControllerBean getProjectParticipantsControllerBean() {
		return projectParticipantsControllerBean;
	}

	public void setRenderParticipants(Boolean renderParticipants) {
		this.renderParticipants = renderParticipants;
	}

	public Boolean getRenderParticipants() {
		return renderParticipants;
	}

	public Boolean getRenderDetailedStatus() {
		return renderDetailedStatus;
	}

	public void setRenderDetailedStatus(Boolean renderDetailedStatus) {
		this.renderDetailedStatus = renderDetailedStatus;
	}

	public void setViewType(Integer viewType) {
		this.viewType = viewType;
	}

	public Integer getViewType() {
		return viewType;
	}

	public void setSelectAllWGE(Boolean selectAllWGE) {
		this.selectAllWGE = selectAllWGE;
	}

	public Boolean getSelectAllWGE() {
		return selectAllWGE;
	}

	public void setSelectAllRV(Boolean selectAllRV) {
		this.selectAllRV = selectAllRV;
	}

	public Boolean getSelectAllRV() {
		return selectAllRV;
	}

	public void setSelectAllFIN(Boolean selectAllFIN) {
		this.selectAllFIN = selectAllFIN;
	}

	public Boolean getSelectAllFIN() {
		return selectAllFIN;
	}

	public Boolean getSelectAllLVAPM() {
		return selectAllLVAPM;
	}

	public void setSelectAllLVAPM(Boolean selectAllLVAPM) {
		this.selectAllLVAPM = selectAllLVAPM;
	}

	public Boolean getSelectAllLVADR() {
		return selectAllLVADR;
	}

	public void setSelectAllLVADR(Boolean selectAllLVADR) {
		this.selectAllLVADR = selectAllLVADR;
	}

	public Boolean getSelectAllLVABM() {
		return selectAllLVABM;
	}

	public void setSelectAllLVABM(Boolean selectAllLVABM) {
		this.selectAllLVABM = selectAllLVABM;
	}

	public Boolean getSelectAllLVACI() {
		return selectAllLVACI;
	}

	public void setSelectAllLVACI(Boolean selectAllLVACI) {
		this.selectAllLVACI = selectAllLVACI;
	}

	public Boolean getSelectAllLIVSIM() {
		return selectAllLIVSIM;
	}

	public void setSelectAllLIVSIM(Boolean selectAllLIVSIM) {
		this.selectAllLIVSIM = selectAllLIVSIM;
	}

	public Boolean getSelectAllBRE() {
		return selectAllBRE;
	}

	public void setSelectAllBRE(Boolean selectAllBRE) {
		this.selectAllBRE = selectAllBRE;
	}

	public SelectItem[] getStatusOptionsPrework() {
		return statusOptionsPrework;
	}

	public SelectItem[] getStatusOptionsAssessmentDay() {
		return statusOptionsAssessmentDay;
	}

	public void setAbydProperties(Properties abydProperties) {
		this.abydProperties = abydProperties;
	}

	public Properties getAbydProperties() {
		return abydProperties;
	}

	public ProjectDocumentsControllerBean getProjectDocumentsControllerBean() {
		return projectDocumentsControllerBean;
	}

	public void setProjectDocumentsControllerBean(ProjectDocumentsControllerBean projectDocumentsControllerBean) {
		this.projectDocumentsControllerBean = projectDocumentsControllerBean;
	}

	public ReportWizardControllerBean getReportWizardControllerBean() {
		return reportWizardControllerBean;
	}

	public void setReportWizardControllerBean(ReportWizardControllerBean reportWizardControllerBean) {
		this.reportWizardControllerBean = reportWizardControllerBean;
	}

	public ParticipantInfoViewBean getParticipantInfoViewBean() {
		return participantInfoViewBean;
	}

	public void setParticipantInfoViewBean(ParticipantInfoViewBean participantInfoViewBean) {
		this.participantInfoViewBean = participantInfoViewBean;
	}

	public void setRenderParticipantsAndOptionals(Boolean renderParticipantsAndOptionals) {
		this.renderParticipantsAndOptionals = renderParticipantsAndOptionals;
	}

	public Boolean getRenderParticipantsAndOptionals() {
		return renderParticipantsAndOptionals;
	}

	/*
	 * public static String[] getCourses() { return courses; }
	 */
	public void setEmailTemplateListControllerBean(EmailTemplateListControllerBean emailTemplateListControllerBean) {
		this.emailTemplateListControllerBean = emailTemplateListControllerBean;
	}

	public EmailTemplateListControllerBean getEmailTemplateListControllerBean() {
		return emailTemplateListControllerBean;
	}

	public void setTltProperties(Properties tltProperties) {
		this.tltProperties = tltProperties;
	}

	public Properties getTltProperties() {
		return tltProperties;
	}

	public List<CourseStatusColumnModel> getPreworkColumns() {
		return preworkColumns;
	}

	public void setPreworkColumns(List<CourseStatusColumnModel> preworkColumns) {
		this.preworkColumns = preworkColumns;
	}

	public Boolean getDisplayAdobeSetup() {
		return displayAdobeSetup;
	}

	public void setDisplayAdobeSetup(Boolean displayAdobeSetup) {
		this.displayAdobeSetup = displayAdobeSetup;
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	public ParticipantListControllerBean getParticipantListControllerBean() {
		return participantListControllerBean;
	}

	public void setParticipantListControllerBean(ParticipantListControllerBean participantListControllerBean) {
		this.participantListControllerBean = participantListControllerBean;
	}

	public ParticipantListViewBean getParticipantListViewBean() {
		return participantListViewBean;
	}

	public void setParticipantListViewBean(ParticipantListViewBean participantListViewBean) {
		this.participantListViewBean = participantListViewBean;
	}

	public CoachingPlanInfoViewBean getCoachingPlanInfoViewBean() {
		return coachingPlanInfoViewBean;
	}

	public void setCoachingPlanInfoViewBean(CoachingPlanInfoViewBean coachingPlanInfoViewBean) {
		this.coachingPlanInfoViewBean = coachingPlanInfoViewBean;
	}

	public CoachingPlanInfoControllerBean getCoachingPlanInfoControllerBean() {
		return coachingPlanInfoControllerBean;
	}

	public void setCoachingPlanInfoControllerBean(CoachingPlanInfoControllerBean coachingPlanInfoControllerBean) {
		this.coachingPlanInfoControllerBean = coachingPlanInfoControllerBean;
	}

	/*
	 * public ContentTokenDao getContentTokenDao() { return contentTokenDao; }
	 *
	 * public void setContentTokenDao(ContentTokenDao contentTokenDao) {
	 * this.contentTokenDao = contentTokenDao; }
	 */
	public ProjectNormsControllerBean getProjectNormsControllerBean() {
		return projectNormsControllerBean;
	}

	public void setProjectNormsControllerBean(ProjectNormsControllerBean projectNormsControllerBean) {
		this.projectNormsControllerBean = projectNormsControllerBean;
	}

	public boolean isRenderAssignments() {
		return renderAssignments;
	}

	public void setRenderAssignments(boolean renderAssignments) {
		this.renderAssignments = renderAssignments;
	}

	public AssignConsultantControllerBean getAssignConsultantControllerBean() {
		return assignConsultantControllerBean;
	}

	public void setAssignConsultantControllerBean(AssignConsultantControllerBean assignConsultantControllerBean) {
		this.assignConsultantControllerBean = assignConsultantControllerBean;
	}

	public AssignConsultantViewBean getAssignConsultantViewBean() {
		return assignConsultantViewBean;
	}

	public void setAssignConsultantViewBean(AssignConsultantViewBean assignConsultantViewBean) {
		this.assignConsultantViewBean = assignConsultantViewBean;
	}

	public TeammemberHelper getTeammemberHelper() {
		return teammemberHelper;
	}

	public void setTeammemberHelper(TeammemberHelper teammemberHelper) {
		this.teammemberHelper = teammemberHelper;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public ViaEdgeScoredDataLocal getViaEdgeScoredData() {
		return viaEdgeScoredData;
	}

	public void setViaEdgeScoredData(ViaEdgeScoredDataLocal viaEdgeScoredData) {
		this.viaEdgeScoredData = viaEdgeScoredData;
	}

	public SelectItem[] getConfOptionsPrework() {
		return confOptionsPrework;
	}

	public void setConfOptionsPrework(SelectItem[] confOptionsPrework) {
		this.confOptionsPrework = confOptionsPrework;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<ProjectCourse> getProjectCourses() {
		return projectCourses;
	}

	public void setProjectCourses(List<ProjectCourse> projectCourses) {
		this.projectCourses = projectCourses;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public List<CoachingPlanObj> getCoachingPlanObjs() {
		return coachingPlanObjs;
	}

	public void setCoachingPlanObjs(List<CoachingPlanObj> coachingPlanObjs) {
		this.coachingPlanObjs = coachingPlanObjs;
	}

	public boolean isPDAProject() {
		return isPDAProject;
	}

	public SelfRegistrationHelper getSelfRegistrationHelper() {
		return selfRegistrationHelper;
	}

	public void setSelfRegistrationHelper(SelfRegistrationHelper selfRegistrationHelper) {
		this.selfRegistrationHelper = selfRegistrationHelper;
	}

	public SelfRegistrationConfigurationViewBean getSelfRegConfigViewBean() {
		return selfRegConfigViewBean;
	}

	public void setSelfRegConfigViewBean(SelfRegistrationConfigurationViewBean selfRegConfigViewBean) {
		this.selfRegConfigViewBean = selfRegConfigViewBean;
	}
}