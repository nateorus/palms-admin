package com.pdinh.presentation.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class GenerateBulkUploadTemplateViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	boolean newExcelFormat = true;
	boolean definitionsVisible;
	int numberOfRows;
	int columnWidth;
	int maxRecordsPerSheet;

	public GenerateBulkUploadTemplateViewBean() {
		init();
	}

	public void init() {
		numberOfRows = 50;
		columnWidth = 9000;
		maxRecordsPerSheet = 50000;
	}

	public boolean isNewExcelFormat() {
		return newExcelFormat;
	}

	public void setNewExcelFormat(boolean newExcelFormat) {
		this.newExcelFormat = newExcelFormat;
	}

	public boolean isDefinitionsVisible() {
		return definitionsVisible;
	}

	public void setDefinitionsVisible(boolean definitionsVisible) {
		this.definitionsVisible = definitionsVisible;
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public int getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(int columnWidth) {
		this.columnWidth = columnWidth;
	}

	public int getMaxRecordsPerSheet() {
		return maxRecordsPerSheet;
	}

	public void setMaxRecordsPerSheet(int maxRecordsPerSheet) {
		this.maxRecordsPerSheet = maxRecordsPerSheet;
	}

}
