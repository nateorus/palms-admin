package com.pdinh.presentation.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.dto.ListItemDto;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.presentation.domain.LanguageObj;

@ManagedBean
@SessionScoped
public class LanguageBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LanguageBean.class);
	public static final String ENGLISH_LANGUAGE_CODE = "en";

	@EJB
	private LanguageSingleton languageSingleton;

	public List<ListItemDto> getLanguageListForLocation() {
		List<ListItemDto> langs = new ArrayList<ListItemDto>();

		for (LanguageObj language : languageSingleton.getLanguages()) {
			langs.add(new ListItemDto(language.getName(), language.getCode()));
		}

		return langs;
	}

	public List<ListItemDto> getLanguageListForInternal() {
		List<ListItemDto> langs = new ArrayList<ListItemDto>();
		langs.add(new ListItemDto("English", "en", "en"));
		return langs;
	}

	public List<ListItemDto> getLanguageListForReport() {
		List<ListItemDto> langs = new ArrayList<ListItemDto>();
		for (LanguageObj langObj : languageSingleton.getAssessmentLanguages()) {
			langs.add(new ListItemDto(langObj.getName(), langObj.getCode(), langObj.getCode()));
		}
		return langs;
	}
}
