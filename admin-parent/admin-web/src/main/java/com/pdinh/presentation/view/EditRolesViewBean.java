package com.pdinh.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;

@ManagedBean(name = "editRolesViewBean")
@ViewScoped
public class EditRolesViewBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EditRolesViewBean.class);
	
	private PalmsSubject subject;
	
	private DualListModel<Role> contextRoleLists = new DualListModel<Role>();
	
	private RoleContext selectedContext;
	private int realm_id;
	private List<Role> addedRoles = new ArrayList<Role>();
	private List<Role> removedRoles = new ArrayList<Role>();

	public PalmsSubject getSubject() {
		return subject;
	}

	public void setSubject(PalmsSubject subject) {
		this.subject = subject;
	}

	public DualListModel<Role> getContextRoleLists() {
		return contextRoleLists;
	}

	public void setContextRoleLists(DualListModel<Role> contextRoleLists) {
		this.contextRoleLists = contextRoleLists;
	}

	public RoleContext getSelectedContext() {
		return selectedContext;
	}

	public void setSelectedContext(RoleContext selectedContext) {
		this.selectedContext = selectedContext;
	}

	public List<Role> getAddedRoles() {
		return addedRoles;
	}

	public void setAddedRoles(List<Role> addedRoles) {
		this.addedRoles = addedRoles;
	}

	public List<Role> getRemovedRoles() {
		return removedRoles;
	}

	public void setRemovedRoles(List<Role> removedRoles) {
		this.removedRoles = removedRoles;
	}

	public int getRealm_id() {
		return realm_id;
	}

	public void setRealm_id(int realm_id) {
		this.realm_id = realm_id;
	}

}
