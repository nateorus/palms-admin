package com.pdinh.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.data.AuthorizationServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.presentation.domain.ClientObj;
import com.pdinh.presentation.domain.ConsentObj;
import com.pdinh.presentation.domain.LanguageObj;
import com.pdinh.presentation.helper.ConsentHelper;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.ClientListViewBean;

@ManagedBean(name = "clientListControllerBean")
@ViewScoped
public class ClientListControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ClientListControllerBean.class);

	@EJB
	private CompanyDao companyDao;

	@EJB
	private LanguageDao languageDao;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private AuthorizationServiceLocal authorizationService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "clientListViewBean", value = "#{clientListViewBean}")
	private ClientListViewBean clientListViewBean;

	@ManagedProperty(name = "clientSetupControllerBean", value = "#{clientSetupControllerBean}")
	private ClientSetupControllerBean clientSetupControllerBean;

	@ManagedProperty(name = "editPasswordPoliciesControllerBean", value = "#{editPasswordPoliciesControllerBean}")
	private EditPasswordPoliciesControllerBean editPasswordPoliciesControllerBean;

	@ManagedProperty(name = "productEnablementControllerBean", value = "#{productEnablementControllerBean}")
	private ProductEnablementControllerBean productEnablementControllerBean;

	@ManagedProperty(name = "consentHelper", value = "#{consentHelper}")
	private ConsentHelper consentHelper;

	@EJB
	private LanguageSingleton languageSingleton;

	private Boolean firstTime = true;
	private Company currentCompany;

	// private List<Language> languages;

	public void initView() {
		// log.info("Top of ClientListControllerBean initView... Testing!!!!!!!!!!!!!!!!!!!!!!!!");
		if (firstTime) {
			// languages = languageDao.findAll();
			Subject subject = SecurityUtils.getSubject();

			getSessionBean().clearProject();

			if (getSessionBean().getCompany() == null) {
				getSessionBean().clearClient();
				enableToolbarButtons(false, 0);
			} else {
				currentCompany = getSessionBean().getCompany();
				getClientListViewBean().setSelectedClient(getClientObject(currentCompany));
				enableToolbarButtons(true, currentCompany.getCompanyId());
				sessionBean.updateReportCenterAvailability();
			}
			log.debug("In ClientListControllerBean initView. {}", subject.getPrincipal());
			firstTime = false;
			List<Company> companyList = authorizationService.getAllowedCompanyList(sessionBean.getRoleContext());
			List<ClientObj> cl = new ArrayList<ClientObj>();
			log.debug("size={}", companyList.size());
			// populate into view list
			Company c;
			Iterator<Company> iterator = companyList.iterator();
			while (iterator.hasNext()) {
				c = iterator.next();
				cl.add(new ClientObj(c.getCompanyId(), c.getCoState(), c.getCoName(),
						getLanguageString(c.getLangPref()), c.getAssessmentProductCount(), c.getProjectCount(), c
								.getUsersCount()));
			}
			clientListViewBean.setClientObjs(cl);
			clientListViewBean.setAssessmentLanguages(languageSingleton.getAssessmentLanguages());
		}

		log.debug("firstTime {}", firstTime);
	}

	public String getLanguageString(String languageCode) {
		LanguageObj lang = languageSingleton.getLangByCode(languageCode);
		if (!(lang == null)) {
			return lang.getName() + " (" + lang.getCode() + ")";
		} else {
			return "n/a";
		}

	}

	public void refreshView() {
		firstTime = true;
		initView();
	}

	public void enableToolbarButtons(Boolean enabled, Integer clientId) {
		Subject subject = SecurityUtils.getSubject();
		clientListViewBean.setManageProjectsDisabled(!enabled);
		clientListViewBean.setManageParticipantsDisabled(!enabled);
		clientListViewBean.setManageEmailTemplatesDisabled(!enabled);
		clientListViewBean.setEditClientInfoDisabled(!enabled);
		clientListViewBean.setProductEnableDisabled(!enabled);
		clientListViewBean.setConsentDisabled(!enabled);
		if (subject.isPermitted("viewPasswordPolicies")) {
			clientListViewBean.setEditPasswordPoliciesDisabled(!enabled);
		}
		// Must have downloadReports permission AND one of (allowChildren on
		// selected company, or ViewAllCompanies)

		if (subject.isPermitted("downloadReports")
				&& (subject.isPermitted(EntityType.COMPANY_TYPE + ":allowchildren:" + clientId) || subject
						.isPermitted("ViewAllCompanies"))) {
			clientListViewBean.setDownloadReportsButtonDisabled(!enabled);
		} else {
			clientListViewBean.setDownloadReportsButtonDisabled(true);
		}
		/*
		if (subject.isPermitted("reportRequestView") && (subject.isPermitted(EntityType.COMPANY_TYPE
				+ ":allowchildren:" + clientId) ||
				subject.isPermitted("ViewAllCompanies"))){
			sessionBean.setReportCenterDisabled(!enabled);
		}else{
			sessionBean.setReportCenterDisabled(true);
		}
		*/

	}

	// Actions
	public void handleRowSelect(SelectEvent event) {

		ClientObj clientObj = (ClientObj) event.getObject();
		currentCompany = companyDao.findById(clientObj.getClientId());
		clientListViewBean.setSelectedClient(clientObj);
		// clientListViewBean.setProductEnableDisabled(false);
		// clientListViewBean.setConsentDisabled(false);

		sessionBean.setCompany(currentCompany);
		sessionBean.setFilteredProjects(null);
		sessionBean.clearSelectedUsers();
		sessionBean.updateReportCenterAvailability();
		enableToolbarButtons(true, clientObj.getClientId());
		log.debug("Selected client: {}", currentCompany.getCompanyId());
	}

	public void handleRowUnselect(UnselectEvent event) {
		enableToolbarButtons(false, 0);

		currentCompany = null;
		sessionBean.clearClient();
		getClientListViewBean().setSelectedClient(null);
		log.debug("Selected client: null");
	}

	public void handleClientSetupInitForEdit(ActionEvent event) {
		log.debug("In Action Listener for Edit Client Info dialog");
		if (sessionBean.getCompany() == null) {
			log.debug("Session Company is NULL..  OH NO");
			sessionBean.setCompany(companyDao.findById(currentCompany.getCompanyId()));
		}

		getClientSetupControllerBean().initForEdit();
	}

	public void handlePasswordPolicyEdit(ActionEvent event) {
		log.debug("In handlePasswordPolicyEdit().  Initialize Password Policy Data Here");
		if (sessionBean.getCompany() == null) {
			log.debug("Session Company is NULL..  OH NO");
			sessionBean.setCompany(companyDao.findById(currentCompany.getCompanyId()));
		}
		editPasswordPoliciesControllerBean.initView();
	}

	public void handleProductEnablementInit() {
		log.debug("In handleProductEnalbement Init().  Initialize Product Enablement dialog");
		if (sessionBean.getCompany() == null) {
			log.debug("Session Company is NULL..  OH NO");
			sessionBean.setCompany(companyDao.findById(currentCompany.getCompanyId()));
		}
		productEnablementControllerBean.initProductStatus(sessionBean.getCompany().getCompanyId(), sessionBean
				.getCompany().getCoName());
	}

	public void handleConsentInit() {
		ConsentObj consent = consentHelper.getCurrentVersionForClient(sessionBean.getCompany().getCompanyId());
		consent = consentHelper.updateCurrentTextForLanguage(consent, "en");
		clientListViewBean.setConsent(consent);
	}

	public void handleConsentLanguageChange() {
		ConsentObj consent = clientListViewBean.getConsent();
		consent = consentHelper.updateCurrentTextForLanguage(consent, consent.getCurrentTexts().get(0)
				.getLanguageCode());
		clientListViewBean.setConsent(consent);
	}

	private ClientObj getClientObject(Company company) {
		return new ClientObj(company.getCompanyId(), company.getCoName(), company.getCoName(), company.getLangPref(),
				company.getAssessmentProductCount(), company.getProjectCount(), company.getUsersCount());
	}

	public void handleClientSetupSave(ActionEvent event) {
		getClientSetupControllerBean().validateAndSave();
		refreshView();
	}

	public String handleDoubleClickRow() {
		return sessionBean.handleNavProjectList();
	}

	public String handleNavManageProjects() {
		return "projectList.xhtml?faces-redirect=true";
	}

	public String handleNavManageParticipants() {
		return "participantList.xhtml?&faces-redirect=true";
	}

	public String handleNavManageEmailTemplates() {
		log.debug(">>>>>>>client list controller>>>>>>>>>>>>handleNavManageEmailTemplates()........ emailTemplatesList.xhtml?faces-redirect=true");
		this.getSessionBean().setTemplateLevel("CLIENT");
		return "emailTemplatesList.xhtml?&faces-redirect=true";
	}

	// Getters/Setters
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ClientListViewBean getClientListViewBean() {
		return clientListViewBean;
	}

	public void setClientListViewBean(ClientListViewBean clientListViewBean) {
		this.clientListViewBean = clientListViewBean;
	}

	public ClientSetupControllerBean getClientSetupControllerBean() {
		return clientSetupControllerBean;
	}

	public void setClientSetupControllerBean(ClientSetupControllerBean clientSetupControllerBean) {
		this.clientSetupControllerBean = clientSetupControllerBean;
	}

	public EditPasswordPoliciesControllerBean getEditPasswordPoliciesControllerBean() {
		return editPasswordPoliciesControllerBean;
	}

	public void setEditPasswordPoliciesControllerBean(
			EditPasswordPoliciesControllerBean editPasswordPoliciesControllerBean) {
		this.editPasswordPoliciesControllerBean = editPasswordPoliciesControllerBean;
	}

	public ProductEnablementControllerBean getProductEnablementControllerBean() {
		return productEnablementControllerBean;
	}

	public void setProductEnablementControllerBean(ProductEnablementControllerBean productEnablementControllerBean) {
		this.productEnablementControllerBean = productEnablementControllerBean;
	}

	public ConsentHelper getConsentHelper() {
		return consentHelper;
	}

	public void setConsentHelper(ConsentHelper consentHelper) {
		this.consentHelper = consentHelper;
	}

}
