package com.pdinh.presentation.controller;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.pdinh.data.TokenServiceLocalImpl;
import com.pdinh.data.TokenServiceLocal;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.ContentToken;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.SuperUserObj;
import com.pdinh.presentation.managedbean.SessionBean;
import com.pdinh.presentation.view.SuperUserListViewBean;

@ManagedBean(name = "superUserListControllerBean")
@ViewScoped
public class SuperUserListControllerBean {
	@EJB
	private UserDao userDao;
	
	@EJB
	private TokenServiceLocalImpl tokenService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "superUserListViewBean", value = "#{superUserListViewBean}")
	private SuperUserListViewBean superUserListViewBean;

	private Boolean firstTime = true;
	private Boolean loginDisabled = true;

	public void initView() {
		if (firstTime) {
			System.out.println("In SuperUserListControllerBean initView");
			firstTime = false;
			superUserListViewBean.setSuperUsers(null);
			superUserListViewBean.setSuperUsers(new ArrayList<SuperUserObj>());
			superUserListViewBean.setSelectedSuperUser(null);
			Iterator<User> iterator = userDao.findSuperAdminUsers(sessionBean.getCompany().getCompanyId()).iterator();
			while (iterator.hasNext()) {
				User user = iterator.next();
				Integer userId = user.getUsersId();
				String firstName = user.getFirstname();
				String lastName = user.getLastname();
				String username = user.getUsername();
				String password = user.getPassword();
			    SuperUserObj superUser = new SuperUserObj(userId,firstName,lastName,username,password);
			    superUser.setCompanyId(sessionBean.getCompany().getCompanyId());
			    superUserListViewBean.getSuperUsers().add(superUser);
			}    
		}
	}
	
	public void refreshView() {
		firstTime = true;
		initView();
	}

	public void enableToolbarButtons(Boolean enabled) {
	}

	// Actions
	public void handleRowSelect(SelectEvent event) {
		setLoginDisabled(false);
	}

	public void handleRowUnselect(UnselectEvent event) {
		setLoginDisabled(true);
	}
	
	public void handleLoginCommand(){
		Subject subject = SecurityUtils.getSubject();
		ContentToken token = tokenService.generateToken(superUserListViewBean.getSelectedSuperUser().getUserId(), superUserListViewBean.getSelectedSuperUser().getCompanyId(), "nhn");
		superUserListViewBean.setToken(token.getToken());
		EntityAuditLogger.auditLog.info("{} performed {} action using superadmin account {}({})", new Object[]{subject.getPrincipal(),
				EntityAuditLogger.OP_LOGON_AS_CLIENT_ADMIN,
				superUserListViewBean.getSelectedSuperUser().getUserId(),
				superUserListViewBean.getSelectedSuperUser().getUsername()});
	}
	
	// Getters/Setters
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public void setSuperUserListViewBean(SuperUserListViewBean superUserListViewBean) {
		this.superUserListViewBean = superUserListViewBean;
	}

	public SuperUserListViewBean getSuperUserListViewBean() {
		return superUserListViewBean;
	}

	public Boolean getLoginDisabled() {
		return loginDisabled;
	}

	public void setLoginDisabled(Boolean loginDisabled) {
		this.loginDisabled = loginDisabled;
	}

}
