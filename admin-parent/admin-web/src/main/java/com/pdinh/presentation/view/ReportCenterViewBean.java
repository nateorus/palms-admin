package com.pdinh.presentation.view;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.component.tabview.TabView;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.enterprise.presentation.util.ParticipantConverter;
import com.kf.enterprise.presentation.util.ReportParticipantRepComparer;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.persistence.ms.entity.PhoneNumber;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ListItemObj;
import com.pdinh.presentation.helper.LabelUtils;

@ManagedBean
@ViewScoped
public class ReportCenterViewBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = LoggerFactory.getLogger(ReportCenterViewBean.class);

	private static final List<ListItemObj> MAX_RECORDS_LIST = Collections
			.unmodifiableList(new ArrayList<ListItemObj>() {
				{
					add(new ListItemObj("10", "10"));
					add(new ListItemObj("100", "100"));
					add(new ListItemObj("200", "200"));
					add(new ListItemObj("500", "500"));
					add(new ListItemObj("1000", "1000"));
					add(new ListItemObj("2000", "2000"));
				}
			});

	private TabView tabView = new TabView();
	private String viewType = "0";
	private boolean selectAll = true;
	private boolean selectAllDisabled = false;
	private boolean initRepository = false;
	private boolean tabSwitcherRendered = true;
	private String pageTitle;
	private Integer projectId;
	private Integer requestId = 0;
	private String projectName;
	private String userName;
	private List<PhoneNumber> userPhones = new ArrayList<PhoneNumber>();
	private User user;
	private String userDeliveryOffice;
	private String userEmail;
	private String userLang;
	private String projectType;
	private String projectTypeCode;
	private String projectCode;
	private String targetLevel;
	private String createdBy;
	private Timestamp createdDate;
	private Timestamp dueDate;

	// Flag for Project Code' label... if false, label is 'Engagement ID'
	private boolean hasProjectCodeLabel = true;

	private Date fromDate;
	private Date toDate;
	private List<ReportParticipantRepresentation> allParticipants;
	private List<ReportParticipantRepresentation> allParticipantsBackup;

	@ManagedProperty(name = "participantConverter", value = "#{participantConverter}")
	private ParticipantConverter participantConverter = new ParticipantConverter();

	private ReportParticipantRepresentation autoCompPart;

	private String filterParticipantId;
	private boolean resetFiltersDisabled = true;

	private List<ReportRepresentation> repositoryRecords = new ArrayList<ReportRepresentation>();
	private ReportRepresentation selectedRepositoryRecord;
	private List<ReportRepresentation> selectedRepositoryRecords = new ArrayList<ReportRepresentation>();
	private List<ReportRepresentation> filteredRepositoryRecords = new ArrayList<ReportRepresentation>();

	private List<GenerateReportsRequest> requestRecords = new ArrayList<GenerateReportsRequest>();
	private List<GenerateReportsRequest> repositoryRequestRecords = new ArrayList<GenerateReportsRequest>();
	private List<GenerateReportsRequest> filteredRequestRecords = new ArrayList<GenerateReportsRequest>();
	private List<GenerateReportsRequest> filteredRepositoryRequestRecords = new ArrayList<GenerateReportsRequest>();
	private GenerateReportsRequest selectedRequestRecord;
	private GenerateReportsRequest selectedRepositoryRequestRecord;

	// Request Details Data
	private List<ReportParticipantRepresentation> requestParticipants = new ArrayList<ReportParticipantRepresentation>();
	private List<ReportRepresentation> requestDetailsReports = new ArrayList<ReportRepresentation>();
	private List<ReportRepresentation> filteredDetailRecords = new ArrayList<ReportRepresentation>();
	private List<ReportRepresentation> selectedDetailRecords = new ArrayList<ReportRepresentation>();
	private ReportRepresentation selectedDetailRecord;

	private ReportParticipantRepresentation selectedDetailPpt;

	private boolean projectContext = false;
	private boolean userContext = false;
	private boolean companyContext = false;
	private final List<SelectItem> statusOptions = new ArrayList<SelectItem>();
	private List<ReportRequestStatus> reportRequestStatuses = new ArrayList<ReportRequestStatus>();

	private StreamedContent reportDownload;
	private boolean cancelButtonDisabled = true;

	private String maxRecords = "500";
	private String page = "0";
	private String reportsPage = "0";
	private String oldestRecordDateString;
	private String newestRecordDateString;
	private String requestRecordCount;
	private String reportRecordCount;
	private boolean prevPageDisabled = true;
	private boolean nextPageDisabled = false;

	private String totalRecordCount;
	private String totalRequestCount;
	private String currentTabId;
	private String returnToTabId;
	private boolean downloadAllDisabled = true;

	private boolean reportManagerRendered = true;
	private boolean requestDetailsRendered = false;
	private boolean projectInfoCollapsed = false;

	private String recreateRequestName = "Recreate - ";

	private String requestProjectList = "";
	private int test;
	private boolean moreInfoDisabled = true;
	private boolean downloadReportDisabled = true;
	private boolean recreateDisabled = true;
	private boolean changeContextRendered;
	private boolean changeClientContextRendered;
	private boolean changeProjectContextRendered;
	private boolean mainContentRendered;
	private boolean recreateInDialogDisabled;
	private boolean recreateRequestNameDisabled;

	public void initialize() {
		// Create lists of all participants in requests records for AutoComplete
		if (repositoryRecords != null && repositoryRecords.size() > 0) {
			allParticipants = new ArrayList<ReportParticipantRepresentation>();
			allParticipantsBackup = new ArrayList<ReportParticipantRepresentation>();
			for (ReportRepresentation rr : repositoryRecords) {
				List<ReportParticipantRepresentation> rprs = rr.getParticipants();
				if (rprs != null && rprs.size() > 0) {
					for (ReportParticipantRepresentation rpr : rprs) {
						boolean addRecord = true;
						for (ReportParticipantRepresentation rpQ : allParticipants) {
							if (rpQ.getUsersId() == rpr.getUsersId()) {
								addRecord = false;
								break;
							}
						}
						if (addRecord) {
							allParticipantsBackup.add(rpr);
							allParticipants.add(rpr);
						}
					}
				}
			}
			ReportParticipantRepComparer comparer = new ReportParticipantRepComparer();
			Collections.sort(allParticipants, comparer);
			Collections.sort(allParticipantsBackup, comparer);
			this.participantConverter.setAllParticipantObjs(allParticipants);
		}
	}

	public List<ReportParticipantRepresentation> autoCompPartComplete(String query) {
		List<ReportParticipantRepresentation> participants = new ArrayList<ReportParticipantRepresentation>();
		for (ReportParticipantRepresentation rpr : allParticipants) {
			if (rpr.getFirstName().toUpperCase().startsWith(query.toUpperCase())
					|| rpr.getLastName().toUpperCase().startsWith(query.toUpperCase())) {
				if (!participants.contains(rpr)) {
					participants.add(rpr);
					logger.debug("KEITH! part = " + rpr.getUsersId());
				}
			}
		}
		return participants;
	}

	public void autoCompValueSelect(SelectEvent e) {
		// allParticipants is the recordset for the autoComplete, to keep from
		// allowing duplicates being selected we remove or add back the
		// participant when they are added or removed from the autoComplete
		// selection for when adding one is added
		// for (ReportParticipantRepresentation rpr : this.autoCompParts) {
		// if (this.allParticipants.contains(rpr)) {
		// logger.debug("AUTOCOMPLETE CHANGE : removing " + rpr.getFirstName() +
		// " " + rpr.getLastName()
		// + " from dataset");
		// // allParticipants.remove(rpr);
		// selectedAutoCompParts.add(rpr);
		// break;
		// }
		// }
	}

	public void autoCompValueUnselect(UnselectEvent e) {
		// The UnselectEvent object is null, we cannot do this...

		// for when one is removed
		// for (ReportParticipantRepresentation rpr :
		// this.allParticipantsBackup) {
		// if (!autoCompParts.contains(rpr) && !allParticipants.contains(rpr)) {
		// logger.debug("AUTOCOMPLETE CHANGE : adding " + rpr.getFirstName() +
		// " " + rpr.getLastName()
		// + " to dataset");
		// allParticipants.add(rpr);
		// selectedAutoCompParts.remove(rpr);
		// break;
		// }
		// }

	}

	public void initializeContext() {
		this.setUserContext(false);
		this.setProjectContext(false);
		this.setCompanyContext(false);
	}

	public List<ListItemObj> getMaxRecordsList() {
		return this.MAX_RECORDS_LIST;
	}

	public Date getToday() {
		return new Date();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public Timestamp getDueDate() {
		return dueDate;
	}

	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	public boolean getHasProjectCodeLabel() {
		return this.hasProjectCodeLabel;
	}

	public void setHasProjectCodeLabel(boolean value) {
		this.hasProjectCodeLabel = value;
	}

	public boolean getResetFiltersDisabled() {
		return resetFiltersDisabled;
	}

	public void setResetFiltersDisabled(boolean resetFiltersDisabled) {
		this.resetFiltersDisabled = resetFiltersDisabled;
	}

	public String getProjectTypeCode() {
		return projectTypeCode;
	}

	public void setProjectTypeCode(String projectTypeCode) {
		this.projectTypeCode = projectTypeCode;
	}

	public List<SelectItem> getStatusOptions() {
		return statusOptions;
	}

	public void setStatusOptions() {
		List<String> statusList = new ArrayList<String>();

		for (ReportRequestStatus status : reportRequestStatuses) {
			statusList.add(LabelUtils.getText(status.getCode()));
		}

		statusOptions.clear();
		statusOptions.add(new SelectItem("", "All"));

		for (String status : statusList) {
			statusOptions.add(new SelectItem(status, status));
		}
	}

	public boolean isProjectContext() {
		return projectContext;
	}

	public void setProjectContext(boolean projectContext) {
		this.projectContext = projectContext;
	}

	public boolean isUserContext() {
		return userContext;
	}

	public void setUserContext(boolean userContext) {
		this.userContext = userContext;
	}

	public List<ReportRepresentation> getRepositoryRecords() {
		return repositoryRecords;
	}

	public void setRepositoryRecords(List<ReportRepresentation> repositoryRecords) {
		this.repositoryRecords = repositoryRecords;
	}

	public List<ReportRepresentation> getSelectedRepositoryRecords() {
		return selectedRepositoryRecords;
	}

	public void setSelectedRepositoryRecords(List<ReportRepresentation> selectedRepositoryRecords) {
		this.selectedRepositoryRecords = selectedRepositoryRecords;
		if (selectedRepositoryRecords != null && selectedRepositoryRecords.size() == 1) {
			selectedRepositoryRecord = selectedRepositoryRecords.get(0);
		} else {
			selectedRepositoryRecord = null;
		}
	}

	public List<ReportRequestStatus> getReportRequestStatuses() {
		return reportRequestStatuses;
	}

	public void setReportRequestStatuses(List<ReportRequestStatus> reportRequestStatuses) {
		this.reportRequestStatuses = reportRequestStatuses;
	}

	public List<ReportRepresentation> getFilteredRepositoryRecords() {
		return filteredRepositoryRecords;
	}

	public void setFilteredRepositoryRecords(List<ReportRepresentation> filteredRepositoryRecords) {
		this.filteredRepositoryRecords = filteredRepositoryRecords;
	}

	public StreamedContent getReportDownload() {
		return reportDownload;
	}

	public void setReportDownload(StreamedContent reportDownload) {
		this.reportDownload = reportDownload;
	}

	public boolean isCancelButtonDisabled() {
		return cancelButtonDisabled;
	}

	public void setCancelButtonDisabled(boolean cancelButtonDisabled) {
		this.cancelButtonDisabled = cancelButtonDisabled;
	}

	public String getMaxRecords() {
		return maxRecords;
	}

	public void setMaxRecords(String maxRecords) {
		this.maxRecords = maxRecords;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getOldestRecordDateString() {
		return oldestRecordDateString;
	}

	public void setOldestRecordDateString(String oldestRecordDateString) {
		this.oldestRecordDateString = oldestRecordDateString;
	}

	public String getNewestRecordDateString() {
		return newestRecordDateString;
	}

	public void setNewestRecordDateString(String newestRecordDateString) {
		this.newestRecordDateString = newestRecordDateString;
	}

	public List<GenerateReportsRequest> getRequestRecords() {
		return requestRecords;
	}

	public void setRequestRecords(List<GenerateReportsRequest> requestRecords) {
		this.requestRecords = requestRecords;
	}

	public List<GenerateReportsRequest> getRepositoryRequestRecords() {
		return repositoryRequestRecords;
	}

	public void setRepositoryRequestRecords(List<GenerateReportsRequest> repositoryRequestRecords) {
		this.repositoryRequestRecords = repositoryRequestRecords;
	}

	public GenerateReportsRequest getSelectedRepositoryRequestRecord() {
		return selectedRepositoryRequestRecord;
	}

	public void setSelectedRepositoryRequestRecord(GenerateReportsRequest selectedRepositoryRequestRecord) {
		this.selectedRepositoryRequestRecord = selectedRepositoryRequestRecord;
	}

	public List<GenerateReportsRequest> getFilteredRepositoryRequestRecords() {
		return filteredRepositoryRequestRecords;
	}

	public void setFilteredRepositoryRequestRecords(List<GenerateReportsRequest> filteredRepositoryRequestRecords) {
		this.filteredRepositoryRequestRecords = filteredRepositoryRequestRecords;
	}

	public GenerateReportsRequest getSelectedRequestRecord() {
		return selectedRequestRecord;
	}

	public void setSelectedRequestRecord(GenerateReportsRequest selectedRequestRecord) {
		this.selectedRequestRecord = selectedRequestRecord;
	}

	public List<GenerateReportsRequest> getFilteredRequestRecords() {
		return filteredRequestRecords;
	}

	public void setFilteredRequestRecords(List<GenerateReportsRequest> filteredRequestRecords) {
		this.filteredRequestRecords = filteredRequestRecords;
	}

	public String getCurrentTabId() {
		return currentTabId;
	}

	public void setCurrentTabId(String currentTabId) {
		this.currentTabId = currentTabId;
	}

	public boolean isDownloadAllDisabled() {
		return downloadAllDisabled;
	}

	public void setDownloadAllDisabled(boolean downloadAllDisabled) {
		this.downloadAllDisabled = downloadAllDisabled;
	}

	public boolean isReportManagerRendered() {
		return reportManagerRendered;
	}

	public void setReportManagerRendered(boolean reportManagerRendered) {
		this.reportManagerRendered = reportManagerRendered;
	}

	public boolean isRequestDetailsRendered() {
		return requestDetailsRendered;
	}

	public void setRequestDetailsRendered(boolean requestDetailsRendered) {
		this.requestDetailsRendered = requestDetailsRendered;
	}

	public List<ReportParticipantRepresentation> getRequestParticipants() {
		return requestParticipants;
	}

	public void setRequestParticipants(List<ReportParticipantRepresentation> requestParticipants) {
		this.requestParticipants = requestParticipants;
	}

	public List<ReportRepresentation> getFilteredDetailRecords() {
		return filteredDetailRecords;
	}

	public void setFilteredDetailRecords(List<ReportRepresentation> filteredDetailRecords) {
		this.filteredDetailRecords = filteredDetailRecords;
	}

	public ReportRepresentation getSelectedDetailRecord() {

		return selectedDetailRecord;
	}

	public void setSelectedDetailRecord(ReportRepresentation selectedDetailRecord) {
		this.selectedDetailRecord = selectedDetailRecord;
	}

	public void setSelectAll(boolean sa) {
		selectAll = sa;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public boolean isSelectAllDisabled() {
		return selectAllDisabled;
	}

	public void setSelectAllDisabled(boolean sad) {
		selectAllDisabled = sad;
	}

	public List<ReportRepresentation> getRequestDetailsReports() {

		return requestDetailsReports;
	}

	public void setRequestDetailsReports(List<ReportRepresentation> requestDetailsReports) {
		this.requestDetailsReports = requestDetailsReports;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public boolean isProjectInfoCollapsed() {
		return projectInfoCollapsed;
	}

	public void setProjectInfoCollapsed(boolean projectInfoCollapsed) {
		this.projectInfoCollapsed = projectInfoCollapsed;
	}

	public ReportParticipantRepresentation getSelectedDetailPpt() {
		return selectedDetailPpt;
	}

	public void setSelectedDetailPpt(ReportParticipantRepresentation selectedDetailPpt) {
		this.selectedDetailPpt = selectedDetailPpt;
	}

	public List<ReportRepresentation> getSelectedDetailRecords() {
		return selectedDetailRecords;
	}

	public void setSelectedDetailRecords(List<ReportRepresentation> selectedDetailRecords) {
		this.selectedDetailRecords = selectedDetailRecords;
	}

	public List<PhoneNumber> getUserPhones() {
		return userPhones;
	}

	public void setUserPhones(List<PhoneNumber> userPhones) {
		this.userPhones = userPhones;
	}

	public String getUserDeliveryOffice() {
		return userDeliveryOffice;
	}

	public void setUserDeliveryOffice(String userDeliveryOffice) {
		this.userDeliveryOffice = userDeliveryOffice;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserLang() {
		return userLang;
	}

	public void setUserLang(String userLang) {
		this.userLang = userLang;
	}

	public boolean isInitRepository() {
		return initRepository;
	}

	public void setInitRepository(boolean initRepository) {
		this.initRepository = initRepository;
	}

	public TabView getTabView() {
		return tabView;
	}

	public void setTabView(TabView tabView) {
		this.tabView = tabView;
	}

	public String getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(String totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public void clearFilterDialog() {
		this.toDate = null;
		this.fromDate = null;
		if (autoCompPart != null)
			this.autoCompPart = null;
		initialize();
	}

	public String getFilterParticipant() {
		return filterParticipantId;
	}

	public void setFilterParticipant(String filterParticipant) {
		this.filterParticipantId = filterParticipant;
	}

	public List<ReportParticipantRepresentation> getAllParticipants() {
		return allParticipants;
	}

	public void setAllParticipants(List<ReportParticipantRepresentation> allParticipants) {
		this.allParticipants = allParticipants;
	}

	public ParticipantConverter getParticipantConverter() {
		return participantConverter;
	}

	public void setParticipantConverter(ParticipantConverter participantConverter) {
		this.participantConverter = participantConverter;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isCompanyContext() {
		return companyContext;
	}

	public void setCompanyContext(boolean companyContext) {
		this.companyContext = companyContext;
	}

	public String getReturnToTabId() {
		return returnToTabId;
	}

	public void setReturnToTabId(String returnToTabId) {
		this.returnToTabId = returnToTabId;
	}

	public ReportRepresentation getSelectedRepositoryRecord() {
		return selectedRepositoryRecord;
	}

	public void setSelectedRepositoryRecord(ReportRepresentation selectedRepositoryRecord) {
		this.selectedRepositoryRecord = selectedRepositoryRecord;
	}

	public String getTotalRequestCount() {
		return totalRequestCount;
	}

	public void setTotalRequestCount(String totalRequestCount) {
		this.totalRequestCount = totalRequestCount;
	}

	public boolean isPrevPageDisabled() {
		return prevPageDisabled;
	}

	public void setPrevPageDisabled(boolean prevPageDisabled) {
		this.prevPageDisabled = prevPageDisabled;
	}

	public boolean isNextPageDisabled() {
		return nextPageDisabled;
	}

	public void setNextPageDisabled(boolean nextPageDisabled) {
		this.nextPageDisabled = nextPageDisabled;
	}

	public String getReportsPage() {
		return reportsPage;
	}

	public void setReportsPage(String reportsPage) {
		this.reportsPage = reportsPage;
	}

	public String getReportRecordCount() {
		return reportRecordCount;
	}

	public void setReportRecordCount(String reportRecordCount) {
		this.reportRecordCount = reportRecordCount;
	}

	public String getRequestRecordCount() {
		return requestRecordCount;
	}

	public void setRequestRecordCount(String requestRecordCount) {
		this.requestRecordCount = requestRecordCount;
	}

	public String getRequestProjectList() {
		return requestProjectList;
	}

	public void setRequestProjectList(String requestProjectList) {
		this.requestProjectList = requestProjectList;
	}

	public ReportParticipantRepresentation getAutoCompPart() {
		return autoCompPart;
	}

	public void setAutoCompPart(ReportParticipantRepresentation autoCompPart) {
		this.autoCompPart = autoCompPart;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public boolean isTabSwitcherRendered() {
		return tabSwitcherRendered;
	}

	public void setTabSwitcherRendered(boolean tabSwitcherRendered) {
		this.tabSwitcherRendered = tabSwitcherRendered;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public int getTest() {
		return test++;
	}

	public boolean isMoreInfoDisabled() {
		return moreInfoDisabled;
	}

	public void setMoreInfoDisabled(boolean moreInfoDisabled) {
		this.moreInfoDisabled = moreInfoDisabled;
	}

	public boolean isDownloadReportDisabled() {
		return downloadReportDisabled;
	}

	public void setDownloadReportDisabled(boolean downloadReportDisabled) {
		this.downloadReportDisabled = downloadReportDisabled;
	}

	public String getRecreateRequestName() {
		return recreateRequestName;
	}

	public void setRecreateRequestName(String recreateRequestName) {
		this.recreateRequestName = recreateRequestName;
	}

	public boolean isChangeContextRendered() {
		return changeContextRendered;
	}

	public void setChangeContextRendered(boolean changeContextRendered) {
		this.changeContextRendered = changeContextRendered;
	}

	public boolean isChangeClientContextRendered() {
		return changeClientContextRendered;
	}

	public void setChangeClientContextRendered(boolean changeClientContextRendered) {
		this.changeClientContextRendered = changeClientContextRendered;
	}

	public boolean isChangeProjectContextRendered() {
		return changeProjectContextRendered;
	}

	public void setChangeProjectContextRendered(boolean changeProjectContextRendered) {
		this.changeProjectContextRendered = changeProjectContextRendered;
	}

	public boolean isMainContentRendered() {
		return mainContentRendered;
	}

	public void setMainContentRendered(boolean mainContentRendered) {
		this.mainContentRendered = mainContentRendered;
	}

	public boolean isRecreateDisabled() {
		return recreateDisabled;
	}

	public void setRecreateDisabled(boolean recreateDisabled) {
		this.recreateDisabled = recreateDisabled;
	}

	public boolean isRecreateInDialogDisabled() {
		return recreateInDialogDisabled;
	}

	public void setRecreateInDialogDisabled(boolean recreateInDialogDisabled) {
		this.recreateInDialogDisabled = recreateInDialogDisabled;
	}

	public boolean isRecreateRequestNameDisabled() {
		return recreateRequestNameDisabled;
	}

	public void setRecreateRequestNameDisabled(boolean recreateRequestNameDisabled) {
		this.recreateRequestNameDisabled = recreateRequestNameDisabled;
	}

}
