package com.pdinh.enterprise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.status.Status;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * Servlet implementation class ConfigureLogBack
 */
@WebServlet(
		urlPatterns = { "/ConfigureLogBack" }, 
		initParams = { 
				@WebInitParam(name = "logback-config", value = "logback-pdinh.xml", description = "configuration file location for logging system")
		},
		loadOnStartup = 1)
public class ConfigureLogBack extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = LoggerFactory.getLogger(ConfigureLogBack.class);
	@Resource(mappedName = "application/config_properties")
	private Properties properties;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConfigureLogBack() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		String configFilePath = config.getInitParameter("logback-config");

		StatusPrinter.print(this.configure(properties.getProperty("logback.config.file", configFilePath)));

	}

	private LoggerContext configure(String logbackConfigFilePath) {
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		if (logbackConfigFilePath.startsWith("http://")) {

			try {
				URL configFileURL = new URL(logbackConfigFilePath);
				JoranConfigurator configurator = new JoranConfigurator();
				configurator.setContext(context);
				context.reset();
				configurator.doConfigure(configFileURL.openStream());

			} catch (JoranException je) {
				logger.warn("JoranException occurred: {}", je.getMessage());

			} catch (MalformedURLException mfue) {
				logger.warn(
						"The logback configuration url provided {} was invalid. The default logback configuration will be used",
						logbackConfigFilePath);
				// logger.error("MalformedURLException {}", mfue);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.warn(
						"The logback configuration url provided {} was invalid. The default logback configuration will be used",
						logbackConfigFilePath);
			}
		} else {

			File configFile = new File(logbackConfigFilePath);
			if (configFile.exists()) {
				try {
					JoranConfigurator configurator = new JoranConfigurator();
					configurator.setContext(context);
					context.reset();
					configurator.doConfigure(logbackConfigFilePath);

				} catch (JoranException je) {

				}
			} else {
				logger.debug("Loading logback configuration from default logback config file in src/main/resources folder");
				JoranConfigurator jc = new JoranConfigurator();
				jc.setContext(context);
				context.reset();
				InputStream is = null;
				try {
					String appName = (String) new InitialContext().lookup("java:app/AppName");
					context.putProperty("application-name", appName);
					is = getClass().getClassLoader().getResourceAsStream(logbackConfigFilePath);
					if (is == null) {
						logger.warn("Could not find {} on classpath", logbackConfigFilePath);
					} else {
						jc.doConfigure(is);
					}
				} catch (NamingException ne) {
					logger.error("NamingException Occurred looking up application name: {}", ne.getMessage());
				} catch (JoranException je) {
					logger.error("Error configuring logback from input stream {}", logbackConfigFilePath);
					StatusPrinter.print(context);
					je.printStackTrace();
				} finally {
					try {
						if (is != null) {
							is.close();
						}
					} catch (IOException e) {
						is = null;
						e.printStackTrace();
					}
				}

			}
		}
		StatusPrinter.printInCaseOfErrorsOrWarnings(context);
		return context;
	}

	/**
	 * @see Servlet#getServletConfig()
	 */
	/*
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}
	*/

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String configFilePath = this.getInitParameter("logback-config");

		Boolean reconfig = false;
		String reconfigParam = request.getParameter("reconfig");
		if (reconfigParam != null) {
			reconfig = Boolean.valueOf(reconfigParam);
		}
		LoggerContext context;
		if (reconfig) {
			context = this.configure(properties.getProperty("logback.config.file", configFilePath));
		}

		context = (LoggerContext) LoggerFactory.getILoggerFactory();

		List<Status> statuses = context.getStatusManager().getCopyOfStatusList();
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><head><title>LogBack Status</title></head><body>");
		pw.println("<h3>LogBack Status Messages</h3><br>");
		pw.println("<table cellpadding=3 cellspacing=3 border=1><tr><th>Date</th><th>Level</th><th>Origin</th><th>Message</th><tr>");
		for (Status s : statuses) {
			pw.println("<tr><td>" + new Date(s.getDate()) + "</td><td>" + Level.toLevel(s.getLevel()) + "</td><td>"
					+ s.getOrigin() + "</td><td>" + s.getMessage() + "</td></tr>");
		}
		pw.println("</table></body></html>");
		pw.flush();
		pw.close();
	}

}
