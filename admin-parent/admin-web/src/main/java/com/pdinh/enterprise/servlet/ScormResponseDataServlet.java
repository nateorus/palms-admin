package com.pdinh.enterprise.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.pdinh.data.ScormServiceLocal;
import com.pdinh.data.instrumentresponses.Instrument;
import com.pdinh.data.instrumentresponses.InstrumentResponses;
import com.pdinh.data.instrumentresponses.Participant;
import com.pdinh.data.jaxb.JaxbUtil;

/**
 * Servlet implementation class ScormResponseDataServlet
 */
@WebServlet("/servlet/ScormResponseDataServlet")
public class ScormResponseDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private ScormServiceLocal scormService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ScormResponseDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String participantIdStr = request.getParameter("participantId");
		String instrumentId = request.getParameter("instrumentId");

		if (participantIdStr == null && instrumentId == null) {
			throw new ServletException("Error: participantId and instrumentId not specified");
		}

		if (participantIdStr == null) {
			throw new ServletException("Error: participantId not specified");
		}

		if (instrumentId == null) {
			throw new ServletException("Error: instrumentId not specified");
		}

		int participantId = Integer.parseInt(participantIdStr);

		InstrumentResponses instrumentResponses = scormService.getResponses(participantId, instrumentId);

		// Downstream processing treats the data as always coming from the
		// "vedge" instrument... transmogrify "lagil" to "vedge" wherever it is
		// found
		for (Participant ppt : instrumentResponses.getParticipants()) {
			for (Instrument inst : ppt.getInstruments()) {
				if (inst.getId().equals(Instrument.INST_LAGIL)) {
					inst.setId(Instrument.INST_VEDGE);
				}
			}
		}

		String xml = JaxbUtil.marshalToXmlString(instrumentResponses, InstrumentResponses.class);

		OutputStream os = response.getOutputStream();
		IOUtils.write(xml, os);
	}

	public ScormServiceLocal getScormService() {
		return scormService;
	}

	public void setScormService(ScormServiceLocal scormService) {
		this.scormService = scormService;
	}

}
