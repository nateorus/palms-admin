package com.pdinh.enterprise.managedbean.form;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.enterprise.realm.PDINHAuthenticationToken;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.presentation.managedbean.SessionBean;

@ManagedBean(name = "loginFormBean")
@ViewScoped
public class LoginFormBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(LoginFormBean.class);
	
	private String username = "";
	private String password = "";
	private Boolean loggedIn = false;
	private String userTime = "";
	
	//private SecurityManager securityManager;
	
	@EJB
	private PalmsSubjectDao subjectDao;
	
	@EJB
	private EntityServiceLocal entityService;


	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public void init(){
		log.debug("Initializing login form bean.  Killing existing session");
		sessionBean.setLoggedIn(false);
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		
	}
	public String loginShiro(){
    	//UsernamePasswordToken token = new UsernamePasswordToken(this.username, this.password);
    	PDINHAuthenticationToken token = new PDINHAuthenticationToken(this.username, this.password);
    	
    	FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getFlash().setKeepMessages(true);
		Subject subject = SecurityUtils.getSubject();
    	try{
    		subject.logout();
    		subject.login(token);
    		
    		if (subject.isAuthenticated()){
    			log.debug("Subject {} is authenticated", username);
	    		
	    		EntityAuditLogger.auditLog.info("{} performed {} action", username, EntityAuditLogger.OP_LOGIN_PALMS_ADMIN);
	    		
	    		//This triggers Authorization Info retrieval via doGetAuthorizationInfo method 
	    		//in PDINHJndiLdapRealm implementation.  To ensure Subject instance is created for
	    		//new users, always call isPermitted before retrieving subject and role context.
	    		
	    		if (subject.isPermitted("Login")){
	    			log.debug("Subject is Permitted to Login");
	    		}else{
	    			log.debug("Subject is Not permitted to Login");
	    			facesContext.addMessage(null, new FacesMessage("Logon Failed", "Logon Permission not granted"));
	    			return "login?faces-redirect=true";
	    		}
    		}else{
    			log.debug("Subject {} is not authenticated", username);
    		}
    	}catch (UnknownAccountException uae){
    		//uae.printStackTrace();
    		log.error("Unknown Account Exception: {}", uae.getMessage());
    		facesContext.addMessage(null, new FacesMessage("Logon Failed", "Unknown Account"));
    		return null;
    	}catch (IncorrectCredentialsException ice){
    		//ice.printStackTrace();
    		log.error("Incorrect Credentials Exception: {}", ice.getMessage());
    		facesContext.addMessage(null, new FacesMessage("Logon Failed", "Incorrect Credentials"));
    		return null;
    	}catch (AuthenticationException e){
    		log.error("Caught Authentication exception. Message: {} Cause: {}", e.getMessage(), e.getCause());
    		if (e.getMessage().contains("expired".subSequence(0, 6))){
    			facesContext.addMessage(null, new FacesMessage("Password Expired", "Please update your password to continue"));
    			return "password_expired?faces-redirect=true";
    		}else{
    			
    	    	facesContext.addMessage(null, new FacesMessage("Logon Failed", "Please try again"));
    			return null;
    		}
    	}
    		
		Set<?> realms = subject.getPrincipals().getRealmNames();
		String realm = "";
		if (realms.size() != 1){
			if (realms.isEmpty()){
				log.error("No Realms found...Subject Must belong to a Realm: {}", subject.getPrincipal());
				facesContext.addMessage(null, new FacesMessage("Logon Failed", "No Realm found"));
				return null;
			}else{
				log.error("Multiple realms detected...Subject Cannot belong to multiple realms: {}", subject.getPrincipal());
				facesContext.addMessage(null, new FacesMessage("Logon Failed", "Multiple Realm found"));
				return null;
			}
		}else{
    		Iterator<?> riter = realms.iterator();
    		
    		while (riter.hasNext()){
    			realm = (String)riter.next();
    			log.debug("Realm: " + realm);
    		}
		}
		setSessionPermissions(realm, subject);
		

    	
    	token.clear();
    	
    	return "clientList?faces-redirect=true";
    	
    }
	public void setSessionPermissions(String realmName, Subject subject){
    	log.debug("in SetSessionPermissions, Login Form Bean");
    	PalmsSubject palmsSubject = subjectDao.findSubjectByPrincipalAndRealmName(subject.getPrincipal().toString(), realmName);
    	RoleContext context = entityService.getDefaultContext(palmsSubject);
    	sessionBean.setRoleContext(context);
    	// Setup any app level permissions here
    	sessionBean.setLoggedIn(true);
    	sessionBean.setUsername(subject.getPrincipal().toString());
		
		
	}

/*
	public String navLogout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			Subject subject = SecurityUtils.getSubject();
			subject.logout();
			request.logout();
			loggedIn = false;
			return "login?faces-redirect=true";
		} catch (ServletException e) {
			e.printStackTrace();
		}

		return null;
	}
*/


	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getUserTime() {
		return userTime;
	}

	public void setUserTime(String userTime) {
		this.userTime = userTime;
	}


}
