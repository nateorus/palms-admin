package com.pdinh.enterprise.servlet;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pdinh.data.ms.dao.FileAttachmentDao;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.ms.entity.FileAttachment;

/**
 * Servlet implementation class GetLanguages
 */
@WebServlet(name = "FileAttachmentServlet", urlPatterns = {"/servlet/GetFileAttachment/*"})
public class GetFileAttachment extends HttpServlet {
	private static final long serialVersionUID = 1L;
	    	
	@EJB
	private FileAttachmentDao fileAttachmentDao;
	
	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;
	
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public GetFileAttachment() {
        super();
    }
    
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		if(request.getParameter("id") != null) {
			FileAttachment fa = fileAttachmentDao.findById(Integer.valueOf(request.getParameter("id")));			
			InputStream is = fileAttachmentService.getAsStream(fa);					
		    		
			response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fa.getName(),"UTF-8").replace("+","%20") + "\"");
					
		    writeInputStreamToOutputStream(is,response.getOutputStream());
		    close(is);
		    close(response.getOutputStream());

		} else if (request.getParameter("path") != null) {			
			String path = request.getParameter("path");
			String name = path.substring(path.lastIndexOf("/") + 1,path.length());
			InputStream is = fileAttachmentService.getAsStream(path);			
		
			response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(name,"UTF-8").replace("+","%20") + "\"");
									
			writeInputStreamToOutputStream(is,response.getOutputStream());
			close(is);
			close(response.getOutputStream());
		} else {
			
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}
	
	private void writeInputStreamToOutputStream(InputStream is, OutputStream os) throws IOException {
		
		byte[] bytes = new byte[1024];
		int bytesRead = 0;
		while((bytesRead = is.read(bytes)) > 0) {
			os.write(bytes,0,bytesRead);					
		}
	}
	
    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
