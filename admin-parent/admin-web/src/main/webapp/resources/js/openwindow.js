// Jeff Bonasso 
// Open window functions
var m_arrPopupWin = new Array();

function openWindow(url,windowNumber,windowName,replace,left,top,width,height,resizable,scrollbars,toolbar,menubar,location,status) {
	if (windowNumber==null || windowNumber==0 || windowNumber=="") windowNumber=1;
	if (windowName==null || windowName==0 || windowName=="") windowName="";
	if (replace==null || replace==0 || replace=="") replace='';
	if (String(replace).toLowerCase()=="yes") replace=true;
	if (String(replace).toLowerCase()=="no") replace=false;
	if (left==null || left==0 || left=="") left=0;
	if (top==null || top==0 || top=="") top=0;
	if (width==null || width==0 || width=="") width=800;
	if (height==null || height==0 || height=="") height=600;
	if (resizable==null || resizable==0 || resizable=="") resizable=0;
	if (scrollbars==null || scrollbars==0 || scrollbars=="") scrollbars=0;
	if (toolbar==null || toolbar==0 || toolbar=="") toolbar=0;
	if (menubar==null || menubar==0 || menubar=="") menubar=0;
	if (location==null || location==0 || location=="") location=0;
	if (status==null || status==0 || status=="") status=0;

	var options = "left=" + left + ",top=" + top + ",width=" + width + ",height=" + height + 
		",resizable=" + resizable + ",scrollbars=" + scrollbars +
		",toolbar=" + toolbar + ",menubar=" + menubar + ",location=" + location + ",status=" + status;

	if (windowName==null || windowName=="") {
		windowName=="";
	} else {
		windowName=windowName+"_"+windowNumber;
	}

	// no window or window closed
	if (m_arrPopupWin[windowNumber-1] == null || m_arrPopupWin[windowNumber-1].closed) {
		m_arrPopupWin[windowNumber-1]=window.open(url,windowName,options);
		return m_arrPopupWin[windowNumber-1];
	// already content in window
	} else {
		if (windowName=="") {
			m_arrPopupWin[windowNumber-1]=window.open(url,windowName,options);
			return m_arrPopupWin[windowNumber-1];
		} else if (replace) {
			m_arrPopupWin[windowNumber-1]=window.open(url,windowName,options);
			m_arrPopupWin[windowNumber-1].focus();
			return m_arrPopupWin[windowNumber-1];
		} else {
			m_arrPopupWin[windowNumber-1].focus();
		}
	}
}

function openWindowAndCenter(url,windowNumber,windowName,replace,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status) {
	var screentop;
	var screenleft;
	
	screenleft = (top.screen.width-width)/2-6;
		
	if (centertop == null) {
		centertop = false;
	}

	if (centertop == true) {
		screentop = 0;
	} else {
		screentop = (top.screen.height-height)/2-6;
	}
	if (resizable == null) resizable = "yes";
	if (scrollbars == null) scrollbars = "yes";
	if (toolbar == null) toolbar = "no";
	if (menubar == null) menubar = "no";
	if (location == null) location = "no";
	if (status == null) status = "no";
		
	return openWindow(url,windowNumber,windowName,replace,screenleft,screentop,width,height,resizable,scrollbars,toolbar,menubar,location,status);
}

function openPersistentWindowAndCenter(url,windowNumber,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status) {
	return openWindowAndCenter(url,windowNumber,"popup",false,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status);
}

function openReusableWindowAndCenter(url,windowNumber,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status) {
	return openWindowAndCenter(url,windowNumber,"popup",true,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status);
}

function openNewWindowAndCenter(url,windowNumber,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status) {
	return openWindowAndCenter(url,windowNumber,"",false,width,height,centertop,resizable,scrollbars,toolbar,menubar,location,status);
}
