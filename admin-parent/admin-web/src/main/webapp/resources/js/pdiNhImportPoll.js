function handleImportPollComplete(xhr, status, args) {
    if(args.stopPolling) {
    	this.window.status = args.stopPolling;
    	importPollWidget.stop();
    }
}