CKEDITOR.editorConfig = function( config )
{   
	config.language = 'en';
	config.uiColor = '#CCCCCC';
	config.forcePasteAsPlainText = true;
	config.pasteFromWordPromptCleanup = true;
};