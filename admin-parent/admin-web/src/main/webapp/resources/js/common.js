function postToReportingServlet(xhr, status, args) {  

	var form = document.createElement("form");
    form.target = "_blank";
    form.method = "POST";
    form.action = args.reportingUrl;
    
    var input = document.createElement("textarea");
    input.name = "xml";
    input.value = unescapeRequestContextCallbackParam(args.reportXml);
    form.appendChild(input);        
    
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);    
}  

function postToExternalPage(xhr, status, args){
	
	var form = document.createElement("form");
	form.method = "POST";
	form.action = args.url;
	
	var tokenInput = document.createElement("input");
	tokenInput.name = "sessionToken";
	tokenInput.value = args.sessionToken;
	form.appendChild(tokenInput);
	
	var subjectIdInput = document.createElement("input");
	subjectIdInput.name = "subjectId";
	subjectIdInput.value = args.subjectId;
	form.appendChild(subjectIdInput);

	if(args.projectId != undefined) {
		var projectIdInput = document.createElement("input");
		projectIdInput.name = "projectId";
		projectIdInput.value = args.projectId;
		form.appendChild(projectIdInput);
	}
	
	if(args.palmsUserId != undefined) {
		var palmsUserIdInput = document.createElement("input");
		palmsUserIdInput.name = "palmsUserId";
		palmsUserIdInput.value = args.palmsUserId;
		form.appendChild(palmsUserIdInput);
	}
	
	if(args.lrmprojectid != undefined) {
		var lrmprojectidInput = document.createElement("input");
		lrmprojectidInput.name = "lrmprojectid";
		lrmprojectidInput.value = args.lrmprojectid;
		form.appendChild(lrmprojectidInput);
	}
	
	if(args.importjobid != undefined) {
		var importjobidInput = document.createElement("input");
		importjobidInput.name = "importjobid";
		importjobidInput.value = args.importjobid;
		form.appendChild(importjobidInput);
	}
	
	if(args.mode != undefined) {
		var modeInput = document.createElement("input");
		modeInput.name = "mode";
		modeInput.value = args.mode;
		form.appendChild(modeInput);
	}
	
	if(args.returnURL != undefined) {
		var returnURLInput = document.createElement("input");
		returnURLInput.name = "returnURL";
		returnURLInput.value = args.returnURL;
		form.appendChild(returnURLInput);
	}
	
	if(args.page != undefined) {
		var pageInput = document.createElement("input");
		pageInput.name = "page";
		pageInput.value = args.page;
		form.appendChild(pageInput);
	}
	
	if(args.returnTo != undefined) {
		var returnToInput = document.createElement("input");
		returnToInput.
		name = "returnTo";
		returnToInput.value = args.returnTo;
		form.appendChild(returnToInput);	
	}
	
	document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);  
}

function unescapeRequestContextCallbackParam(str) {
	str = str.replace("&lt;","<");
	str = str.replace("&gt;",">");
	return str;
}
//These two functions used for submitting login to NHN from Super Admin tab..
//Must generate token before submitting form.  This can be re-used for any token based external login requirement
function getToken(){
	//alert('executing getToken()');
	runTokenService();
	//alert('Token value: ' + document.salogin.token.value);
}
function submitLoginForm(){
	//alert('About to submit form with token: ' + document.salogin.token.value);
	document.salogin.submit();
}

function handleCompletionRequest(xhr, status, args, dialogToClose) {
	  if( args.notValid || args.validationFailed )
		    return;
	  dialogToClose.hide();
}

function getDate(){
	var d = new Date();
	
	document.getElementById('loginForm:userTime').setAttribute('value', d.toDateString() + ' ' + d.toLocaleTimeString());

}

// This method is intended to show Please Wait 
// for none-ajax calls
function showPleaseWaitDialog() {
	pleaseWaitDialog.show();
    
	// hack to fix issue in IE with freezing animation gifs. 
	var isIE = /*@cc_on!@*/false;
    if(isIE){
    	setTimeout(function() {
    		if (document.getElementById("progressBar")) {
    			document.getElementById("progressBar").innerHTML = '<img src="/adminweb/faces/javax.faces.resource/images/ajaxloadingbar.gif"/>';
    		}
    	}, 500);
    	
    }   
}

function hidePleaseWaitDialog() {
	pleaseWaitDialog.hide();
}

function destroyCKEditor() {
	if(typeof CKEDITOR != 'undefined') {
		for(var name in CKEDITOR.instances) {
			//alert('Name: ' + CKEDITOR.instances[name].name);
			CKEDITOR.instances[name].destroy(false);
			//alert('Name: ' + CKEDITOR.instances[name]);
		}
	}
}


function startBulkUploadTemplateDownload() {  
	generateBulkUploadTemplateStatusDialog.show();  
}  
	  
function stopBulkUploadTemplateDownload() {  
	generateBulkUploadTemplateStatusDialog.hide();  
}

function startDownload() {  
	downloadDialog.show();  
}  
	  
function stopDownload() {  
	downloadDialog.hide();
}

function isBlank(str) {return (!str || /^\s*$/.test(str));}

function removeInvalidCharacters(field, formatType) {
	if (field !== undefined) {
		var str = field.value;
		if (!isBlank(str)) {
			if (formatType == 1) {
				field.value = str.replace(/[^A-Za-z0-9\u00C0-\u00FC:`'\-\\\/_\(\)\s\?]/g, '');	
				
			} else if (formatType == 2) {
				field.value = str.replace(/[^A-Za-z0-9\u00C0-\u00FC:`'\-\\\/_\(\)\s]/g, '');	
				
			} else if (formatType == 3) {
				field.value = str.replace(/^\D*$/g, '');								
				
			} else if (formatType == 4) {
				field.value = str.replace(/\D+/g, '');								
				
			} else if (formatType == 5) {
				field.value = str.replace(/[^0-9+\(\)#\.\s\/ext-]/g, '');								

			} else if (formatType == 6) {
				field.value = str.replace(/[^A-Za-z0-9\u00C0-\u1FFF\u2C00-\uD7FF:`'\-\\\/_\(\)\s\?]/g, '');
			}
			
		}
	}
}

function resetWizard(wizardVar) {
	wizardVar.loadStep(wizardVar.cfg.steps[0], true);
}

function handleReportSelectionChange(tabId) {
	tbvReportManager.select(tabId);
}

function putCursorAtEnd(input) {
	input.focus();
    if (input.setSelectionRange) {
		var len = input.value.length * 2;
		input.setSelectionRange(len, len);
    } else {
		input.value = input.value;
    }
	input.scrollTop = 999999;
}