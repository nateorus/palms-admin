<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>PDINH Server REST Test Page</title>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    	</script>
    	<script>
		$(document).ready(function(){
		  $("#submit").click(function(){
			xml=$("#xml").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/reports/requestReports",
		    	data:xml,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
  </head>
	<body>
		<h1>Queue Reports REST Service Test Page</h1>
		<h3>Send Report Request XML: /adminrest/jaxrs/reports/requestReports</h3>
		<a href="index.jsp">Home</a><br>
	    <form id="form" name="form" method="POST">
	    	<textarea id="xml" name="xml" rows="20" cols="100" name="data">
	    	
<ReportsRequest>
	<subject_id>1</subject_id>
	<principal>nwondra</principal>
	<priority>1</priority>
	<participants>
		<participant id="374135" projectId="862" projectName="my project" reportLang="en">
		</participant>
		<participant id="374124" projectId="862" projectName="my project" reportLang="fr">
		</participant>
	</participants>
	<reportTypes>
		<reportType name="TLT Individual Report(s)" code="TLT_INDIVIDUAL_SUMMARY_REPORT" targetLevel="FLL"  reportIdNameOption="" title="my title" reportLang="en"/>
		<reportType name="TLT Group Report(s)" code="TLT_GROUP_DETAIL_REPORT" targetLevel="FLL"  reportIdNameOption="" title="my title" reportLang="fr"/>
	</reportTypes>
</ReportsRequest>
				
	    	</textarea>
	    	<input id="submit" name="submit" type="button" value="submit"/>
	    </form>
	    <h2>Ajax post results</h2>
	    <textarea id="xmlresults" rows="5" cols="60" name="xmlresults">results</textarea>
	    <!-- iframe id="results" name="results" width="450" height="400" src="about:blank"/-->
		<!-- a href="/adminrest/jaxrs/courses/xml" target="_window">/PDINHServer-rest/jaxrs/courses/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/xml" target="_window">/PDINHServer-rest/jaxrs/languages/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/rv/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/rv/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/wg/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/wg/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/es/xml" target="_window">/PDINHServer-rest/jaxrs/languages/es/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/languages/en/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/languages/en/xml</a><br/-->
	</body>
</html> 
