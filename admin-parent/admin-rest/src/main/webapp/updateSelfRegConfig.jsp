<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>PDINH Server REST Test Page</title>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    	</script>
    	<!-- 
    	<script>
		$(document).ready(function(){
		  $("#submit").click(function(){
			xml=$("#xml").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/updateSelfRegConfig",
		    	data:xml,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
		-->
		<script>
		$(document).ready(function(){
		  $("#submit2").click(function(){
			id=$("#selfRegIdText").val();
			pattern=$("#domainText").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/addAllowedDomains/" + id + "/" + pattern,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
		<script>
		$(document).ready(function(){
		  $("#submit3").click(function(){
			id=$("#selfRegIdText2").val();
			itemId=$("#listItemId").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/removeAllowedDomains/" + id + "/" + itemId,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
		<script>
		$(document).ready(function(){
		  $("#submit4").click(function(){
			id=$("#selfRegIdText3").val();
			enabled=$("#enabledText").val();
			regLimit=$("#regLimitText").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/updateProjectSelfRegConfig/" + id + "/" + regLimit + "/" + enabled,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
		<script>
		$(document).ready(function(){
		  $("#submit5").click(function(){
			id=$("#projectIdText").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/getCreateSelfRegConfigByProjectId/" + id,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
		<script>
		$(document).ready(function(){
		  $("#submit6").click(function(){
			id=$("#selfRegIdText4").val();
			userid=$("#userId").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/addRegisteredUser/" + id + "/" + userid,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
		<script>
		$(document).ready(function(){
		  $("#submit7").click(function(){
			id=$("#selfRegIdText5").val();
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/selfReg/resetConfigUrl/" + id,
		    	contentType: "application/xml",
		    	dataType:"xml",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " Error XML: " + new XMLSerializer().serializeToString(xhr.responseXML);
		    	},
		    	success: function(xml){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		
		    		document.getElementById("xmlresults").value = new XMLSerializer().serializeToString(xml.documentElement);
		    	}	
		    });
		  });
		});

		</script>
  </head>
	<body>
		<h1>Self Reg REST Service Test Page (adminrest/jaxrs/selfReg)</h1>
		<h3>Update Config: /updateProjectSelfRegConfig/{selfRegConfigId}/{regLimit}/{enabled}</h3>
		<h3>Add Domain: /addAllowedDomains/{selfRegConfigId}/{allowedDomain : .+}</h3>
		<h3>Remove Domain: /removeAllowedDomains/{selfRegConfigId}/{allowedDomainId : .+}</h3>
		<a href="index.jsp">Home</a><br>
		<!-- 
	    <form id="form" name="form" method="POST">
	    	<textarea id="xml" name="xml" rows="10" cols="100" name="data">
	    	
<selfRegConfig
	selfRegConfigId="1"
	enabled="false"
	registrationLimit="-1">
		<allowedEmailDomains>
			<allowedEmailDomain>
				<selfRegConfigListItem value="9h.com" type="ALLOWED_DOMAIN" lang="en"></selfRegConfigListItem>
			</allowedEmailDomain>
		</allowedEmailDomains>
		<registeredUsers/>

</selfRegConfig>
				
	    	</textarea>
	    	<input id="submit" name="submit" type="button" value="submit"/>
	    </form>
	     -->
	     <table border="1" cellpadding="3" cellspacing="3">
	    <tr>
	    <td>
	     <h2>Get / Create Self Reg Config for Project</h2>
	     <h3>Note: It will be created if it doesn't exist</h3>
	    
	    <form id="form5" name="form5" method="POST">
	    	Project Id: <input id="projectIdText" name="selfRegIdText3" type="text"/><br/>

	    	<input id="submit5" name="submit5" type="button" value="submit"/>
	    </form>
	    </td>
	    <td>
	    <h2>Update Config</h2>
	    <form id="form4" name="form4" method="POST">
	    	Self Reg Config Id: <input id="selfRegIdText3" name="selfRegIdText3" type="text"/><br/>
	    	Enabled (true/false): <input id="enabledText" name="enabledText" type="text"/><br/>
	    	Registration Limit: <input id="regLimitText" name="regLimitText" type="text"/><br/>
	    	<input id="submit4" name="submit4" type="button" value="submit"/>
	    </form>
	    </td>
	    </tr>
	    <tr>
	    <td>
	    <h2>Add Domain</h2>
	    <form id="form2" name="form2" method="POST">
	    	Self Reg Config Id: <input id="selfRegIdText" name="selfRegIdText" type="text"/><br/>
	    	Email Domain Pattern: <input id="domainText" name="domainText" type="text"/><br/>
	    	<input id="submit2" name="submit2" type="button" value="submit"/>
	    </form>
	    </td>
	    <td>
	    <h2>Remove Domain</h2>
	    <form id="form3" name="form3" method="POST">
	    	Self Reg Config Id: <input id="selfRegIdText2" name="selfRegIdText2" type="text"/><br/>
	    	Domain List Item Id: <input id="listItemId" name="listItemId" type="text"/><br/>
	    	<input id="submit3" name="submit3" type="button" value="submit"/>
	    </form>
	    </td>
	    </tr>
	    <tr>
	    	<td>
		    	<h2>Add Registered User</h2>
			    <form id="form6" name="form6" method="POST">
			    	Self Reg Config Id: <input id="selfRegIdText4" name="selfRegIdText4" type="text"/><br/>
			    	User Id: <input id="userId" name="userId" type="text"/><br/>
			    	<input id="submit6" name="submit6" type="button" value="submit"/>
			    </form>
	    	</td>
	    	<td>
	    		<h2>Reset Config URL</h2>
	    		<form id = "form7" name="form7" method="POST">
	    			Self Reg Config Id: <input id="selfRegIdText5" name="selfRegIdText5" type="text"/><br/>
	    			
	    			<input id="submit7" name="submit7" type="button" value="submit"/>
				</form>
	    	</td>
	    </tr>
	    </table>
	    <h2>Ajax post results</h2>
	    <textarea id="xmlresults" rows="10" cols="100" name="xmlresults">results</textarea>
	   
	</body>
</html> 
