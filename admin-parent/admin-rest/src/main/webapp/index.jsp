<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>PDINH Server REST Test Page</title>
    	
  </head>
	<body>
		<h1>REST Service Test Pages</h1>
		<!-- h3>Send Report Request XML: /adminrest/jaxrs/getReports/requestReports</h3>
	    <form id="form" name="form" method="POST">
	    	<textarea id="xml" name="xml" rows="20" cols="100" name="data"></textarea>
	    	<input id="submit" name="submit" type="button" value="submit"/>
	    </form>
	    <h2>Ajax post results</h2>
	    <textarea id="xmlresults" rows="5" cols="60" name="xmlresults">results</textarea-->
	    <!-- iframe id="results" name="results" width="450" height="400" src="about:blank"/-->
		<!-- a href="/adminrest/jaxrs/courses/xml" target="_window">/PDINHServer-rest/jaxrs/courses/xml</a><br/-->
		<h3>POST pages</h3>
		<a href="/adminrest/queueReports.jsp" target="_window">Queue Reports Service</a><br/>
		<a href="/adminrest/generateReports.jsp" target="_window">Generate Report Service</a><br/>
		<a href="/adminrest/updateReportRequest.jsp" target="_window">Update Report Request Service</a><br/>
		<a href="/adminrest/downloadReports.jsp" target="_window">Download Report Service</a><br/>
		<hr>
		<h3>GET Report Request</h3>
		<ul>
			<li><a href="/adminrest/jaxrs/listreports/getRequest/af745ddf-b885-4876-8bba-40af1eda5771">Get JSON representation for a report request by Request Key</a></li>
			<li><a href="/adminrest/jaxrs/listreports/getRequest/100">Get JSON representation for a report request by ID</a></li>
		</ul>
		<h3>List Project Report API.  Examples:</h3>
		
		<ul>
			<li><a href="/adminrest/jaxrs/listreports/getProjectReportRequests/862/ALL">List all report requests in project 862</a></li>
			<li><a href="/adminrest/jaxrs/listreports/getProjectReportRequests/862/QUEUED/STORED">List report requests in project 862 with status QUEUED or STORED</a></li>
			<li><a href="/adminrest/jaxrs/listreports/getProjectReportRequests/862/ALL?max=5">List top 5 most recently updated requests in project 862</a>
			<li><a href="/adminrest/jaxrs/listreports/getProjectReportRequests/862/ALL?max=5&page=1">List NEXT 5 most recently updated requests in project 862</a>
		</ul>
		<h3>List Company Report API.  Examples:</h3>
		
		<ul>
			<li><a href="/adminrest/jaxrs/listreports/getCompanyReportRequests/1521/ALL">List all report requests in company 1521</a></li>
			<li><a href="/adminrest/jaxrs/listreports/getCompanyReportRequests/1521/QUEUED/STORED">List report requests in company 1521 with status QUEUED or STORED</a></li>
			<li><a href="/adminrest/jaxrs/listreports/getCompanyReportRequests/1521/ALL?max=5">List top 5 most recently updated requests in company 1521</a>
			<li><a href="/adminrest/jaxrs/listreports/getCompanyReportRequests/1521/ALL?max=5&page=1">List NEXT 5 most recently updated requests in company 1521</a>
		</ul>
		<h3>List Report Requests by User API. Examples:</h3>
		<ul>
			<li><a href="/adminrest/jaxrs/listreports/getUserReportRequests/374135/ALL">List all report requests for user 374135</a>
			<li><a href="/adminrest/jaxrs/listreports/getUserReportRequests/374135/STORED">List STORED reports for user 374135</a>
			<li><a href="/adminrest/jaxrs/listreports/getUserReportRequests/374135/ALL?max=10">List 10 most recently updated report requests for user 374135</a>
			<li><a href="/adminrest/jaxrs/listreports/getUserReportRequests/374135/ALL?max=10&page=1">List NEXT 10 most recently updated report requests for user 374135</a>
		</ul>
		<h3>List Report Requests Submitted By API.  Examples</h3>
		<ul>
			<li><a href="/adminrest/jaxrs/listreports/getSubjectReportRequests/1/ALL">List Report Requests Submitted by: (nwondra).  Any status, sorted by most recent update to status</a></li>
			<li><a href="/adminrest/jaxrs/listreports/getSubjectReportRequests/1/QUEUEING_FAILED">List Report Requests Submitted by: (nwondra).  QUEUEING_FAILED status, sorted by most recent update to status</a>
		</ul>
		<h3>Test KFO Authorization</h3>
		<a href="/adminrest/jaxrs/health/queueTest">Test KFO authorization</a>
		<!-- a href="/adminrest/jaxrs/courseLanguages/rv/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/rv/xml</a><br/>
		
		<a href="/adminrest/jaxrs/courseLanguages/wg/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/wg/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/es/xml" target="_window">/PDINHServer-rest/jaxrs/languages/es/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/languages/en/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/languages/en/xml</a><br/-->
	</body>
</html> 
