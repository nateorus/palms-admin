<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>PDINH Server REST Test Page</title>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    	</script>
    	<script>
		$(document).ready(function(){
		  $("#submit").click(function(){
			xml=$("#xml").val();
			var xhr = new XMLHttpRequest();
			xhr.open("POST", "/adminrest/jaxrs/generate/generateReportStream", true);
			xhr.setRequestHeader('Content-Type', 'application/json');
			xhr.setRequestHeader('Accept', 'application/json');
			xhr.setRequestHeader('Accept', 'application/octet-stream');
			xhr.setRequestHeader('Accept', 'application/xml');
			xhr.responseType = 'blob';
			xhr.onload = function(e){
				if (this.status == 200 || this.status == 500){
					var blob = this.response;
					alert("Got report file blob with size: " + blob.size + " and type: " + blob.type );
					//var pdf = document.createElement('pdf');
					var blobURL = window.URL.createObjectURL(blob);
					window.open(blobURL);
				}
			}
			xhr.send(xml);
			
		  });
		});

		</script>
  </head>
	<body>
		<h1>Generate Report REST Service Test Page</h1>
		<h3>Send Report Request XML: /adminrest/jaxrs/generate/generateReportStream</h3>
		
		<a href="index.jsp">Home</a><br>
		This Page opens the response blob in a new window.  Either the reportRequestID or reportRequestKey may be used.
		<br>Note:  this may not work in IE...use Chrome please
		<br>
		<h3>Successful Returns: </h3>
			<ul>
				<li>HTTP Status Code:  200</li>
				<li>ContentType:  application/pdf, application/excel</li>
				<li>Binary response content</li>
			</ul>
		<h3>UnSuccessful Returns: </h3>
		<ul>
				<li>HTTP Status Code:  500</li>
				<li>ContentType:  application/json, application/xml</li>
				<li>JSON/XML elements:  errorCode, errorMessage, errorDetail</li>
				<li>Error Codes:</li>
					<ul>
						<li>ECRPT006:  The report request could not be located.</li>
						<li>ECRPT003:  The report server was unavailable</li>
						<li>ECRPT004:  Error generating report content model</li>
						<li>ECRPT005:  Unknown Error</li>
						<li>ECRPT007:  The request has been cancelled</li>
						<li>ECRPT008:  The report failed to generate</li>
						<li>ECRPT001:  Failed to generate the URL for report generation</li>
					</ul>
			</ul>
		
	    <form id="form" name="form" method="POST" >
	    	<textarea id="xml" rows="8" cols="100" name="data">
{
	"reportRequestId" : "0",
	"reportRequestKey" : "af745ddf-b885-4876-8bba-40af1eda5771"
}
	    	</textarea>
	    	<input id="submit" name="submit" type="button" value="submit"/>
	    </form>
	    <!-- h2>Ajax post results</h2>
	    <textarea id="xmlresults" rows="5" cols="60" name="xmlresults">results</textarea-->
	    <!-- iframe id="results" name="results" width="450" height="400" src="about:blank"/-->
		<!-- a href="/adminrest/jaxrs/courses/xml" target="_window">/PDINHServer-rest/jaxrs/courses/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/xml" target="_window">/PDINHServer-rest/jaxrs/languages/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/rv/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/rv/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/wg/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/wg/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/es/xml" target="_window">/PDINHServer-rest/jaxrs/languages/es/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/languages/en/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/languages/en/xml</a><br/-->
	</body>
</html> 
