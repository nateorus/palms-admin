<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>PDINH Server REST Test Page</title>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    	</script>
    	<script>
		$(document).ready(function(){
		  $("#submit").click(function(){
			xml=$("#xml").val();
			var xhr = new XMLHttpRequest();
			xhr.open("POST", "/adminrest/jaxrs/download/downloadReports", true);
			xhr.setRequestHeader('Content-Type', 'application/xml');
			xhr.setRequestHeader('Accept', 'application/json');
			xhr.setRequestHeader('Accept', 'application/octet-stream');
			xhr.setRequestHeader('Accept', 'application/xml');
			xhr.setRequestHeader('Accept', 'text/html');
			xhr.responseType = 'blob';
			xhr.onload = function(e){
				if (this.status == 200 || this.status == 500){
					var blob = this.response;
					alert("Got report file blob with size: " + blob.size + " and type: " + blob.type );
					//var pdf = document.createElement('pdf');
					var blobURL = window.URL.createObjectURL(blob);
					window.open(blobURL);
				}
			}
			xhr.send(xml);
			
		  });
		});

		</script>
  </head>
	<body>
		<h1>Download Report REST Service Test Page</h1>
		<h3>Send Report Request XML: /adminrest/jaxrs/download/downloadReports</h3>
		<a href="index.jsp">Home</a><br>
	    <form id="form" name="form" method="POST" >
	    	<textarea id="xml" rows="20" cols="100" name="data">
<reportList>
	<report requestId="234" reportRequestKey="48751225-1d74-475c-9d5d-d63e7b53f32c" contentType="application/pdf"/>
	
</reportList>
	    	</textarea>
	    	<input id="submit" name="submit" type="button" value="submit"/>
	    </form>
	    <!-- h2>Ajax post results</h2>
	    <textarea id="xmlresults" rows="5" cols="60" name="xmlresults">results</textarea-->
	    <!-- iframe id="results" name="results" width="450" height="400" src="about:blank"/-->
		<!-- a href="/adminrest/jaxrs/courses/xml" target="_window">/PDINHServer-rest/jaxrs/courses/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/xml" target="_window">/PDINHServer-rest/jaxrs/languages/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/rv/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/rv/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/wg/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/wg/xml</a><br/>
		<a href="/adminrest/jaxrs/languages/es/xml" target="_window">/PDINHServer-rest/jaxrs/languages/es/xml</a><br/>
		<a href="/adminrest/jaxrs/courseLanguages/cs/languages/en/xml" target="_window">/PDINHServer-rest/jaxrs/courseLanguages/cs/languages/en/xml</a><br/-->
	</body>
</html> 
