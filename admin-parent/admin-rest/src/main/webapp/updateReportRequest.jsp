<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>PDINH Server REST Test Page</title>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    	</script>
    	<script>
		$(document).ready(function(){
		  $("#submit").click(function(){
			xml=$("#xml").val();
			//alert(xml);
		    $.ajax({ type: "POST",
		    	url:"/adminrest/jaxrs/reports/updateRequest",
		    	data:xml,
		    	contentType: "application/json",
		    	dataType:"json",
		    	cache: false,
		    	error: function(xhr){ 
		    		//alert("Error: " + xhr.status + ": " + xhr.statusText);
		    		document.getElementById("xmlresults").value = xhr.status + ": " + xhr.statusText + " :" + xhr.responseText;
		    	},
		    	success: function(xml, status){
		    		//alert("Returned Success: " + new XMLSerializer().serializeToString(xml.documentElement));
		    		//document.getElementById("xmlresults").value = xml.status + ": " + xml.statusText + " XML: " + xml.responseXML;
		    		document.getElementById("xmlresults").value = status;
		    	}	
		    });
		  });
		});

		</script>
  </head>
	<body>
		<h1>Update Report Request REST Service Test Page</h1>
		<h3>Send Report Request XML: /adminrest/jaxrs/reports/updateRequest</h3>
		<a href="index.jsp">Home</a><br>
		This service can be used to update the status of a report request or other metadata.<br>  
		Requests with status STORED may not be changed.<br>  RETURN VALUES:<br>
		Error Conditions, Response Code 500: <br><ul>
		<li>REQUEST_NOT_FOUND: ECRPT006, If the id or key provided did not return a request</li>
		<li>BAD_REQUEST: ECRPT002, If invalid values exist for status, language etc. </li>
		<li>ACTION_NOT_ALLOWED:  ECRPT010, if you attempt to update a STORED request</li>
		</ul>
		Sucessful responses<br><ul>
		<li>HTTP Status code 200 if update was successful.</li>
		<li>HTTP Status code 304, if the report request entity was not modified as a result of this action.</li>
		</ul>
	    <form id="form" name="form" method="POST">
	    	<textarea id="xml" name="xml" rows="10" cols="100" name="data">
{
	"@requestId" : "0",
	"@reportRequestKey" : "af745ddf-b885-4876-8bba-40af1eda5771",
	"@status" : "STORED",
	"@reportLang" : "en",
	"@title" : "my title"
}

				
	    	</textarea>
	    	<input id="submit" name="submit" type="button" value="submit"/>
	    </form>
	    <h2>Ajax post results</h2>
	    <textarea id="xmlresults" rows="5" cols="60" name="xmlresults">results</textarea>
	  </body>
</html> 
