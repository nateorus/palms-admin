package com.pdinh.enterprise.rest.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="userDocumentType")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDocumentTypeRepresentation extends Representation {

	public static final String RESOURCE_NAME = "userDocumentTypes";

	@XmlAttribute
	private int userDocumentTypeId;
	
	@XmlElement
	private String name;

	public UserDocumentTypeRepresentation() {		
	}
	
	public int getUserDocumentTypeId() {
		return userDocumentTypeId;
	}

	public void setUserDocumentTypeId(int userDocumentTypeId) {
		this.userDocumentTypeId = userDocumentTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
