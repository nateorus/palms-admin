package com.pdinh.enterprise.rest.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="fileAttachment")
@XmlAccessorType(XmlAccessType.FIELD)
public class FileAttachmentRepresentation extends Representation {
	public static final String RESOURCE_NAME = "fileAttachments";
	
	@XmlAttribute	
	private int id;
	
	@XmlElement	
	private String name;
	
	@XmlElement
	private String url;
	
	public FileAttachmentRepresentation() {		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
