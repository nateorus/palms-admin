package com.pdinh.enterprise.helper;

import javax.ejb.Local;

import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;

@Local
public interface ReportApiHelperLocal {

	public ReportGenerateRequest createReportGenerateRequestEntity(GenerateReportsRequest request) throws PalmsException;
}
