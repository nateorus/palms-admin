package com.pdinh.enterprise.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

public class Utils {

	public static String getStringFromInputStream(InputStream is) {
		StringWriter sw = new StringWriter();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		
		try {
			String line = "";
			while((line = br.readLine()) != null) {
				sw.write(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return sw.toString();
	}
	
	public static String getFileNameFromPath(String path) {
		path = path.replace("\\","/");
		if(path.lastIndexOf("/") != -1) {
			path = path.substring(path.lastIndexOf("/"),path.length());
		}
		return path;
	}
}
