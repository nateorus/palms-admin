package com.pdinh.enterprise.rest;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.JaxbUtil;
import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRequestResponse;
import com.pdinh.data.jaxb.reporting.ReportRequestResponseList;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ReportGenerateRequestDao;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.enterprise.helper.KFORequestDispatcherLocal;
import com.pdinh.enterprise.helper.KFOnlineReportApiHelperLocal;
import com.pdinh.enterprise.helper.ReportApiHelperLocal;
import com.pdinh.enterprise.rest.security.ReportAuthorizationBean;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.reporting.ReportingServiceLocal;
import com.sun.jersey.multipart.FormDataParam;

@Stateless
@Path("/reports")
public class ReportRequestResource {
	private static final Logger log = LoggerFactory.getLogger(ReportRequestResource.class);

	@EJB
	private UserDao userDao;

	@EJB
	private LanguageSingleton languageSingleton;

	//@EJB
	//private ProjectUserDao projectUserDao;

	@EJB
	private ProjectDao projectDao;
	
	//@EJB
	//private ProjectCourseDao projectCourseDao;

	@EJB
	private ReportingServiceLocal reportingService;


	@EJB
	private ReportGenerateRequestDao reportGenerateRequestDao;

	@EJB
	private ReportRequestDao reportRequestDao;
	
	@EJB
	private ReportingSingleton reportingSingleton;

	@EJB
	private KFOnlineReportApiHelperLocal kfOnlineReportApiHelper;
	
	@EJB
	private KFORequestDispatcherLocal dispatcher;

	@EJB
	private ReportAuthorizationBean reportAuthorizationBean;
	
	@EJB
	private ReportApiHelperLocal reportApiHelper;

	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;
	
	@GET
	@Produces({MediaType.TEXT_HTML})
	@Path("/updateRequestStatus/{reportRequestId}/{statusCode}")
	public Response updateRequestStatus(@PathParam(value = "reportRequestId") int reportRequestId, 
			@PathParam(value = "statusCode") String statusCode){
		ReportRequest request = reportRequestDao.findById(reportRequestId);
		if (request == null){
			return Response.serverError().build();
		}
		ReportRequestStatus status = reportingSingleton.getReportRequestStatus(statusCode);
		if (status == null){
			return Response.serverError().build();
		}
		Timestamp now = new Timestamp(new Date().getTime());
		request.setReportRequestStatus(status);
		request.setStatusUpdateTs(now);
		if (status.getCode().equals(ReportRequestStatus.STATUS_PROCESSING)){
			request.setProcessStartTime(now);
		}
		reportRequestDao.update(request);
		return Response.ok("success").build();
	}
	/**
	 * This method applies updates to existing ReportRequest entities.  Currently,
	 * The only fields you may update are status, reportLang, and reportTitle.
	 * 
	 * @param reportRequest a ReportRepresentation object, representing the report to be updated.
	 * @param hh
	 * @return an http status code 200 or 302 if successful.  500 and ErrorRepresentation object
	 * if unsuccessful
	 */
		
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/updateRequest")
	public Response updateRequest(ReportRepresentation reportRequest, @Context HttpHeaders hh){
		log.debug("In updateRequest()");

		final List<String> allowedStatuses = new ArrayList<String>(Arrays.asList(
				ReportRequestStatus.STATUS_QUEUED, 
				ReportRequestStatus.STATUS_REQUESTED,
				ReportRequestStatus.STATUS_ERROR,
				ReportRequestStatus.STATUS_GENERATED,
				ReportRequestStatus.STATUS_QUEUEING_FAILED,
				ReportRequestStatus.STATUS_STORED));
		ReportRequest requestEntity = null;
		log.debug("Getting report request by key: {}", reportRequest.getReportRequestKey());
		log.debug("report requestid: {}, Status: {}", reportRequest.getRequestId(), reportRequest.getStatus());
		if (reportRequest.getRequestId() > 0){
			requestEntity = reportRequestDao.getReportRequestBatchChildren(reportRequest.getRequestId());
		}else if (!(reportRequest.getReportRequestKey() == null || reportRequest.getReportRequestKey().isEmpty())){
			log.debug("Getting report request by key: {}", reportRequest.getReportRequestKey());
			requestEntity = reportRequestDao.getReportRequestByKey(reportRequest.getReportRequestKey());
		}
		if (requestEntity == null){
			log.error("Entity Not Found");
			ErrorRepresentation error = new ErrorRepresentation();
			error.setErrorCode(PalmsErrorCode.REQUEST_NOT_FOUND.getCode());
			error.setErrorMessage(PalmsErrorCode.REQUEST_NOT_FOUND.getMessage());
			error.setErrorDetail("Invalid report key or id supplied");
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}
		log.debug("Got Entity {}", requestEntity.getReportRequestId());
		
		if (!allowedStatuses.contains(requestEntity.getReportRequestStatus().getCode())){
			ErrorRepresentation error = new ErrorRepresentation();
			error.setErrorCode(PalmsErrorCode.ACTION_DISALLOWED.getCode());
			error.setErrorMessage(PalmsErrorCode.ACTION_DISALLOWED.getMessage());
			error.setErrorDetail("Cannot perform updates on report requests with status: " + requestEntity.getReportRequestStatus().getCode());
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}
		boolean changed = false;
		if (reportRequest.getStatus() != null && reportRequest.getStatus() != requestEntity.getReportRequestStatus().getCode()){
			ReportRequestStatus newStatus = reportingSingleton.getReportRequestStatus(reportRequest.getStatus());
			if (newStatus == null){
				ErrorRepresentation error = new ErrorRepresentation();
				error.setErrorCode(PalmsErrorCode.BAD_REQUEST.getCode());
				error.setErrorMessage(PalmsErrorCode.BAD_REQUEST.getMessage());
				error.setErrorDetail("Invalid Status value provided.");
				return error.buildErrorResponse(hh.getAcceptableMediaTypes());
			}
			requestEntity.setReportRequestStatus(newStatus);
			requestEntity.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			changed = true;
		}
		if (reportRequest.getReportLang() != null && reportRequest.getReportLang() != requestEntity.getLanguage().getCode()){
			Language language = languageSingleton.getLanguageByCode(reportRequest.getReportLang());
			if (language == null){
				ErrorRepresentation error = new ErrorRepresentation();
				error.setErrorCode(PalmsErrorCode.BAD_REQUEST.getCode());
				error.setErrorMessage(PalmsErrorCode.BAD_REQUEST.getMessage());
				error.setErrorDetail("Invalid Language value provided.");
				return error.buildErrorResponse(hh.getAcceptableMediaTypes());
			}
			requestEntity.setLanguage(language);
			changed = true;
		}
		
		if (reportRequest.getTitle() != null && reportRequest.getTitle() != requestEntity.getReportTitle()){
			requestEntity.setReportTitle(reportRequest.getTitle());
			changed = true;
		}
		if (changed){
			reportRequestDao.update(requestEntity);
			return Response.ok().build();
		}else{
			return Response.notModified().build();
		}
		
		
	}
	
	/**
	 * This service uploads a report file directly to the repository, bypassing the queue
	 * @param genRptReq
	 * @param request
	 * @return
	 */
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.MULTIPART_FORM_DATA})
	@Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/uploadReport")
	public Response uploadReport(@Context HttpHeaders hh, @FormDataParam("file") File file, 
			@FormDataParam("xml") String xmlData, @FormDataParam("fileName") String fileName){
		log.debug("In UPload Report");
		
		log.debug("Got XML Data: {}", xmlData);
			
		ReportGenerateRequest rptGenReq;
		ReportRequest rptReq = null;
		
		try {

			String requestKey = "";
			GenerateReportsRequest report = null;
			report = (GenerateReportsRequest) JaxbUtil.unmarshal(xmlData, GenerateReportsRequest.class);
			//File tempFile = file;
			log.debug("Got File part: {}, Size: {}", file.getName(), file.length());
			
			if (report.getReportTypeList() == null){
				ErrorRepresentation error = new ErrorRepresentation();
				error.setErrorCode(PalmsErrorCode.BAD_REQUEST.getCode());
				error.setErrorMessage("Missing report configuration");
				return error.buildErrorResponse(hh.getAcceptableMediaTypes());
				
			}else {
				if (report.getReportTypeList().size() != 1){
					ErrorRepresentation error = new ErrorRepresentation();
					error.setErrorCode(PalmsErrorCode.BAD_REQUEST.getCode());
					error.setErrorMessage("Upload only supports 1 report at a time");
					return error.buildErrorResponse(hh.getAcceptableMediaTypes());
				}
			}

			log.debug(">>>>>>>>> Before ContentType = {}", report.getReportTypeList().get(0).getContentType());
			//rptGenReq = this.setupReportGenerateRequestEntity(report);
			rptGenReq = reportApiHelper.createReportGenerateRequestEntity(report);
			
			log.debug("created new report request entities");
			rptReq = rptGenReq.getReportRequests().get(0);
			requestKey = kfOnlineReportApiHelper.uploadFile(rptReq, file, fileName);
			log.debug("Got response from kfonline helper");
			rptReq.setReportRequestKey(requestKey);
			rptReq.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_STORED));
			rptReq.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(rptReq);
			ReportRepresentation updatedRptReq = new ReportRepresentation(rptReq);
			log.debug("Sending Response");
			log.debug(">>>>>>>>> After ContentType = {}", updatedRptReq.getContentType());
			return Response.ok()
					.entity(updatedRptReq)
					.type(MediaType.APPLICATION_XML)
					.build();
			
		} catch (PalmsException e) {
			log.debug("Caught palms exception: {}", e.getMessage());
			ErrorRepresentation error = new ErrorRepresentation(e);
			rptReq.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			rptReq.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			rptReq.setErrorCode(PalmsErrorCode.UPLOAD_FAILED.getCode());
			rptReq.setErrorMessage(e.getErrorDetail());
			reportRequestDao.update(rptReq);
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}
		
		
	}
	/**
	 * This method adds new Report requests to the system, and calls the KFO Queuing api, once per report. 
	 * The request entities are created synchronously, but KFO calls occur asynchronously.  Therefore
	 * the returned response list will contain the database id of the requests but not any report request 
	 * keys or status of the Queuing operation.  You may get these later by calling the endpoints in the
	 * ReportListResource.
	 * 
	 * @summary Queue reports for processing
	 * @author nwondra
	 * @param rptRequest a GenerateReportsRequest object, containing the 
	 * metadata for one or more reports being requested.
	 * @param hh
	 * @return ReportRequestResponseList entity, xml response
	 */

	@POST
	@Consumes({MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/requestReports")
	public Response requestReports(GenerateReportsRequest rptRequest, @Context HttpHeaders hh){
		
		log.debug("Got rptRequest with {} ppts, and {} report types", rptRequest.getParticipants().size(), rptRequest.getReportTypeList().size());
		ReportGenerateRequest rptGenReq;
		try {
			//rptGenReq = this.setupReportGenerateRequestEntity(rptRequest);
			rptGenReq = reportApiHelper.createReportGenerateRequestEntity(rptRequest);
		} catch (PalmsException e) {
			log.debug("Caught palms exception: {}", e.getMessage());
			ErrorRepresentation error = new ErrorRepresentation(e);
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}

		ReportRequestResponseList respList = new ReportRequestResponseList();
		respList.setReportRequestId(rptGenReq.getReportGenerateRequestId());
		List<ReportRequestResponse> rrResponse = new ArrayList<ReportRequestResponse>();
		for (ReportRequest repReq : rptGenReq.getReportRequests()){
			reportingSingleton.getRequestQueueMap().put(repReq.getReportRequestId(), repReq.getStatusUpdateTs());
			ReportRequestResponse resp = new ReportRequestResponse();
			resp.setReportRequestId(repReq.getReportRequestId());
			resp.setReportRequestKey("unknown");
			resp.setReportType(repReq.getReportType().getCode());
			rrResponse.add(resp);
		}

		log.debug("Dispatching {} report requests to Queuing service", rptGenReq.getReportRequests().size());
	

		kfOnlineReportApiHelper.dispatchMessages(rptGenReq.getReportRequests());
		
		respList.setReportRequestResponseList(rrResponse);
		
		Response response = Response
				.ok()
				.entity(respList)
				.type(MediaType.APPLICATION_XML)
				.build();
		return response;
	}
	/* -- moved to ReportApiHelper bean
	private ReportGenerateRequest setupReportGenerateRequestEntity(GenerateReportsRequest rptRequest) throws PalmsException{
		log.debug("In setupReportGenerateRequestEntity()");
		ReportGenerateRequest rptGenReq = new ReportGenerateRequest();
		rptGenReq.setRequestTime(new Timestamp(new Date().getTime()));
		if (rptRequest.getPrincipal() == null || rptRequest.getPrincipal().isEmpty()){
			throw new PalmsException("No Request principal was supplied.", PalmsErrorCode.BAD_REQUEST.getCode(), "Principal is required to request reports.");
		}
		rptGenReq.setSubjectId(rptRequest.getSubjectId());
		rptGenReq.setPrincipal(rptRequest.getPrincipal());
		rptGenReq.setPriority(rptRequest.getPriority());
		String requestName = rptRequest.getRequestName();
		if (!(requestName == null)){
			rptGenReq.setRequestName(requestName);
		}
		
		List<ReportParticipant> rptPptList = new ArrayList<ReportParticipant>();
		for (ReportParticipantRepresentation rpr : rptRequest.getParticipants()){
			ReportParticipant ppt = new ReportParticipant();
			ppt.setReportGenerateRequest(rptGenReq);
			if (!(rpr.getReportLang() == null) && !rpr.getReportLang().isEmpty()){
				Language pptLang = languageSingleton.getLanguageByCode(rpr.getReportLang());
				
				ppt.setLanguage(pptLang);
			}
			User user = userDao.findById(rpr.getUsersId());
			if (user == null){
				throw new PalmsException("Invalid Participant ID specified.", PalmsErrorCode.BAD_REQUEST.getCode());
			}
			ppt.setUser(user);
			Project project = projectDao.findById(rpr.getProjectId());
			if (project == null){
				throw new PalmsException("Invalid Project ID specified.", PalmsErrorCode.BAD_REQUEST.getCode());
			}
			ppt.setProject(project);
			rptPptList.add(ppt);
		}
		List<ReportRequest> rptRequestList = new ArrayList<ReportRequest>();
		for (ReportRepresentation rpt : rptRequest.getReportTypeList()){
			log.debug("Report type: {}", rpt.getCode());
			log.debug("Report title: {}", rpt.getTitle());
			log.debug("Report target level: {}", rpt.getTargetLevel());
			log.debug("Report name: {}", rpt.getReportName());
			log.debug("Report lang: {}", rpt.getReportLang());
			TargetLevelType tltype = null;
			if (!((rpt.getTargetLevel() == null) || rpt.getTargetLevel().isEmpty())){
				tltype = reportingSingleton.getTargetLevelTypeByCode(rpt.getTargetLevel());
				if (tltype == null){
					throw new PalmsException("Invalid Target Level specified.", PalmsErrorCode.BAD_REQUEST.getCode());
				}
			}


			ReportType rptType = reportingSingleton.getReportType(rpt.getCode());
			if (rptType == null){
				log.error("Report type is null.  Throwing palms exception");
				throw new PalmsException("Invalid Report Type specified.", PalmsErrorCode.BAD_REQUEST.getCode());
			}
			if (rptType.isGroupReport()){
				ReportRequest rptRequestEntity = new ReportRequest();
				rptRequestEntity.setReportParticipants(rptPptList);
				if (rpt.getReportLang() == null){
					rptRequestEntity.setLanguage(languageSingleton.getLanguageByCode("en"));
				}else{
					Language rptLang = languageSingleton.getLanguageByCode(rpt.getReportLang());
					if (rptLang == null){
						throw new PalmsException("Invalid Report Language specified.", PalmsErrorCode.BAD_REQUEST.getCode());
					}
					rptRequestEntity.setLanguage(rptLang);
				}
				rptRequestEntity.setReportGenerateRequest(rptGenReq);
				if (!(rpt.getReportIdNameOption() == null || rpt.getReportIdNameOption().isEmpty())){
					rptRequestEntity.setReportIdNameOption(rpt.getReportIdNameOption());
				}
				if (!(rpt.getTitle() == null || rpt.getTitle().isEmpty())){
					rptRequestEntity.setReportTitle(rpt.getTitle());
				}
				if (!(rpt.getContentType() == null || rpt.getContentType().isEmpty())){
					rptRequestEntity.setContentType(rpt.getContentType());
				}
				rptRequestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_REQUESTED));
				rptRequestEntity.setStatusUpdateTs(new Timestamp(new Date().getTime()));
				rptRequestEntity.setReportType(rptType);
				if (!(tltype == null)){
					rptRequestEntity.setTargetLevelType(tltype);
				}
				rptRequestEntity.setCreateType(rpt.getCreateType());
				rptRequestList.add(rptRequestEntity);

			}else{
				for (ReportParticipant rptPpt : rptPptList){
					ReportRequest rptRequestEntity = new ReportRequest();
					List<ReportParticipant> singleUserList = new ArrayList<ReportParticipant>();
					singleUserList.add(rptPpt);
					rptRequestEntity.setReportParticipants(singleUserList);
					if (rpt.getReportLang() == null){
						if (rptPpt.getLanguage() == null){
							rptRequestEntity.setLanguage(languageSingleton.getLanguageByCode("en"));
						}else{
							rptRequestEntity.setLanguage(rptPpt.getLanguage());
						}
					}else{
						Language rptLang = languageSingleton.getLanguageByCode(rpt.getReportLang());
						if (rptLang == null){
							throw new PalmsException("Invalid Report Language specified.", PalmsErrorCode.BAD_REQUEST.getCode());
						}
						rptRequestEntity.setLanguage(rptLang);
					}
					rptRequestEntity.setReportGenerateRequest(rptGenReq);
					if (!(rpt.getReportIdNameOption() == null || rpt.getReportIdNameOption().isEmpty())){
						rptRequestEntity.setReportIdNameOption(rpt.getReportIdNameOption());
					}
					rptRequestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_REQUESTED));
					rptRequestEntity.setStatusUpdateTs(new Timestamp(new Date().getTime()));
					rptRequestEntity.setReportType(rptType);
					if (!(tltype == null)){
						rptRequestEntity.setTargetLevelType(tltype);
					}
					if (!(rpt.getTitle() == null || rpt.getTitle().isEmpty())){
						rptRequestEntity.setReportTitle(rpt.getTitle());
					}
					if (!(rpt.getContentType() == null || rpt.getContentType().isEmpty())){
						rptRequestEntity.setContentType(rpt.getContentType());
					}
					rptRequestEntity.setCreateType(rpt.getCreateType());
					rptRequestList.add(rptRequestEntity);
				}
			}

		}
		rptGenReq.setReportParticipants(rptPptList);
		rptGenReq.setReportRequests(rptRequestList);
		return reportGenerateRequestDao.create(rptGenReq);

	}
*/
}
