package com.pdinh.enterprise.rest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequestList;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentationList;
import com.pdinh.data.ms.dao.ReportGenerateRequestDao;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.enterprise.rest.representation.RepresentationFactory;
import com.pdinh.enterprise.rest.security.ReportAuthorizationBean;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;

@Stateless
@Path("/listreports")
public class ReportListResource {

	private static final Logger log = LoggerFactory.getLogger(ReportListResource.class);
	
	@EJB
	private ReportGenerateRequestDao reportGenerateRequestDao;
	
	@EJB
	private ReportRequestDao reportRequestDao;
	
	@EJB
	private ReportAuthorizationBean reportAuthorizationBean;
	
	@EJB
	private ReportingSingleton reportingSingleton;

	@GET
	@Produces({MediaType.APPLICATION_XML})
	@Path("/getCompanyGenerateRequests/{companyId}/{status : .+}")
	public GenerateReportsRequestList getCompanyGenerateRequests(@PathParam(value="companyId") int companyId, 
			@DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page,
			@PathParam("status") List<PathSegment> status){
		List<String> statuses = new ArrayList<String>();
		
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());

		}
		long count = 0;
		List<ReportGenerateRequest> listReq = new ArrayList<ReportGenerateRequest>();
		if (statuses.contains("ALL")){
			count = reportGenerateRequestDao.getCountReportGenerateRequestsByCompanyId(companyId);
			listReq = reportGenerateRequestDao.getReportGenerateRequestsByCompanyId(companyId, maxResults, page);
		}else{
			count = reportGenerateRequestDao.getCountGenerateRequestsByCompAndStat(companyId, statuses);
			listReq = reportGenerateRequestDao.getGenerateRequestsByCompAndStat(companyId, maxResults, page, statuses);
		}
		
		GenerateReportsRequestList list = new GenerateReportsRequestList();
		list.setTotalRecordCount(count);
		list.setReportsRequests(this.setupGenerateRequestReps(listReq));
		return list;
	}
	

	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/getCompanyReportRequests/{companyId}/{status : .+}")
	public ReportRepresentationList getCompanyReportRequests(@PathParam(value = "companyId") int companyId,
			@PathParam("status") List<PathSegment> status, @DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page){
		log.debug("in get companyreportrequests");
		List<String> statuses = new ArrayList<String>();
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());

		}
		List<ReportRequest> reportRequests = new ArrayList<ReportRequest>();
		Long count = (long) 0;
		if (statuses.contains("ALL")){
			count = reportRequestDao.getCountReportRequestsByCompanyId(companyId);
			log.debug("Got Total Count: {}", count);
			reportRequests = reportRequestDao.getReportRequestsByCompanyId(companyId, maxResults, page);
		}else{
			count = reportRequestDao.getCountReportRequestsByCompanyIdAndStatus(companyId, statuses);
			reportRequests = reportRequestDao.getReportRequestsByCompanyIdAndStatus(companyId, statuses, maxResults, page);
		}
		log.debug("Got {} Report Requests", reportRequests.size());
		List<ReportRepresentation> rptRequests = new ArrayList<ReportRepresentation>();
		for (ReportRequest rptReq : reportRequests){

			rptRequests.add(RepresentationFactory.createReportRepresentation(rptReq));
		}
		ReportRepresentationList list = new ReportRepresentationList();
		list.setTotalRecordCount(count);
		list.setReports(rptRequests);
		return list;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	@Path("/getGenerateRequestDetails/{requestId}")
	public GenerateReportsRequest getGenerateRequestDetails(@PathParam(value="requestId") int requestid,
			@DefaultValue("0") @QueryParam("subjectId") int subjectId){
		
		List<ReportRequest> requests = reportRequestDao.getReportsByParent(requestid);
		if (requests == null){
			log.error("Report list not found for request Id: {}", requestid);
			return new GenerateReportsRequest();
		}
		GenerateReportsRequest gRepReq = this.setupGenerateRequestRepFromReportList(requests);
		if (subjectId > 0){
			gRepReq = reportAuthorizationBean.authorizeReports(gRepReq, subjectId);
		}
		
		return gRepReq;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	@Path("/getProjectGenerateRequests/{projectId}/{status : .+}")
	public GenerateReportsRequestList getProjectGenerateRequests(@PathParam(value="projectId") int projectId, @DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page,
			@PathParam("status") List<PathSegment> status){
		
		List<String> statuses = new ArrayList<String>();
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());

		}
		long count = 0;
		List<ReportGenerateRequest> listReq = new ArrayList<ReportGenerateRequest>();
		if (statuses.contains("ALL")){
			count = reportGenerateRequestDao.getCountReportGenerateRequestsByProjectId(projectId);
			listReq = reportGenerateRequestDao.getReportGenerateRequestsByProjectId(projectId, maxResults, page);
		}else{
			count = reportGenerateRequestDao.getCountGenerateRequestsByProjAndStat(projectId, statuses);
			listReq = reportGenerateRequestDao.getGenerateRequestsByProjAndStat(projectId, maxResults, page, statuses);
		}
		
		GenerateReportsRequestList list = new GenerateReportsRequestList();
		list.setTotalRecordCount(count);
		list.setReportsRequests(this.setupGenerateRequestReps(listReq));
		return list;
	}
	

	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/getProjectReportRequests/{projectId}/{status : .+}")
	public ReportRepresentationList getProjectReportRequests(@PathParam(value = "projectId") int projectId,
			@PathParam("status") List<PathSegment> status, @DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page){
		log.debug("in get projectreportrequests");
		List<String> statuses = new ArrayList<String>();
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());

		}
		List<ReportRequest> reportRequests = new ArrayList<ReportRequest>();
		long count = 0;
		if (statuses.contains("ALL")){
			count = reportRequestDao.getCountReportRequestsByProjectId(projectId);
			reportRequests = reportRequestDao.getReportRequestsByProjectId(projectId, maxResults, page);
		}else{
			count = reportRequestDao.getCountReportRequestsByProjectIdAndStatus(projectId, statuses);
			reportRequests = reportRequestDao.getReportRequestsByProjectIdAndStatus(projectId, statuses, maxResults, page);
		}
		log.debug("Got {} Report Requests", reportRequests.size());
		List<ReportRepresentation> rptRequests = new ArrayList<ReportRepresentation>();
		for (ReportRequest rptReq : reportRequests){

			rptRequests.add(RepresentationFactory.createReportRepresentation(rptReq));
		}
		ReportRepresentationList list = new ReportRepresentationList();
		list.setTotalRecordCount(count);
		list.setReports(rptRequests);
		return list;
	}
	

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("getRequest/{requestId}")
	public Response getRequest(@PathParam(value = "requestId") String requestId, @Context HttpHeaders hh){
		
		log.debug("in get Request");
		ReportRequest request = null;
		try{
			int id = Integer.valueOf(requestId);
			request = reportRequestDao.getReportRequestBatchChildren(id);
		}catch (NumberFormatException n){
			request = reportRequestDao.getReportRequestByKey(requestId);
		}
		
		ReportRepresentation rep = RepresentationFactory.createReportRepresentation(request);
		
		String json = this.getJsonStringReportRep(rep);
        
		Response response = Response
				.ok()
				.entity(json)
				.type(MediaType.APPLICATION_JSON)
				.build();
				
		return response;
	}
	

	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/getRequestsByType/{subjectId}/{reportType : .+}")
	public ReportRepresentationList getRequestByType(@PathParam(value = "subjectId") int subjectId, 
			@PathParam("reportType") List<PathSegment> reportTypes){
		log.debug("in get User report requests by Type..");
		
		List<ReportRequest> reportRequests = new ArrayList<ReportRequest>();
		
		//Return the newest report request of each type
		for (PathSegment segment : reportTypes){
			log.debug("Got path segment path: {}", segment.getPath());
			List<ReportRequest> typedRequests = new ArrayList<ReportRequest>();
			typedRequests = reportRequestDao.getByTypeSubmittedBy(subjectId, segment.getPath());
			log.debug("Query returned {} results", typedRequests.size());
			if (!(typedRequests == null) && !typedRequests.isEmpty()){
				if (typedRequests.size() == 1){
					reportRequests.add(typedRequests.get(0));
				}else{
				
					ReportRequest newest = null;
					for (ReportRequest req : typedRequests){
						if (newest == null || (req.getStatusUpdateTs().after(newest.getStatusUpdateTs()))){
							newest = req;
						}
					}
					reportRequests.add(newest);
				}
			}
			
		}
		
		
		log.debug("Got {} Report Requests", reportRequests.size());
		List<ReportRepresentation> rptRequests = new ArrayList<ReportRepresentation>();
		for (ReportRequest rptReq : reportRequests){
			
			rptRequests.add(RepresentationFactory.createReportRepresentation(rptReq));
		}
		ReportRepresentationList list = new ReportRepresentationList();
		list.setReports(rptRequests);
		return list;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/getSubjectReportRequests/{subjectId}/{status : .+}")
	public ReportRepresentationList getSubjectReportRequests(@PathParam(value = "subjectId") int subjectId, 
			@PathParam("status") List<PathSegment> status, @DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page){
		log.debug("in get User report requests..");
		List<String> statuses = new ArrayList<String>();
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());
			
		}
		List<ReportRequest> reportRequests = new ArrayList<ReportRequest>();
		if (statuses.contains("ALL")){
			reportRequests = reportRequestDao.getRequestsSubmittedBy(subjectId, reportingSingleton.getAllReportRequestStatuses(), maxResults, page);
		}else{
			reportRequests = reportRequestDao.getRequestsSubmittedBy(subjectId, statuses, maxResults, page);
		}
		
		log.debug("Got {} Report Requests", reportRequests.size());
		List<ReportRepresentation> rptRequests = new ArrayList<ReportRepresentation>();
		for (ReportRequest rptReq : reportRequests){
			
			rptRequests.add(RepresentationFactory.createReportRepresentation(rptReq));
		}
		ReportRepresentationList list = new ReportRepresentationList();
		list.setReports(rptRequests);
		return list;
	}
	

	@GET
	@Produces({MediaType.APPLICATION_XML})
	@Path("/getUserGenerateRequests/{usersId}/{status : .+}")
	public GenerateReportsRequestList getUserGenerateRequests(@PathParam(value="usersId") int usersId, @DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page,
			@PathParam("status") List<PathSegment> status){
		log.debug("Got Users Id: {}", usersId);
		List<String> statuses = new ArrayList<String>();
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());

		}
		long count = 0;
		List<ReportGenerateRequest> listReq = new ArrayList<ReportGenerateRequest>();
		if (statuses.contains("ALL")){
			count = reportGenerateRequestDao.getCountReportGenerateRequestsByUserId(usersId);
			listReq = reportGenerateRequestDao.getReportGenerateRequestsByUserId(usersId, maxResults, page);
		}else{
			count = reportGenerateRequestDao.getCountGenerateRequestsByUserAndStat(usersId, statuses);
			listReq = reportGenerateRequestDao.getGenerateRequestsByUserAndStat(usersId, maxResults, page, statuses);
		}
		GenerateReportsRequestList list = new GenerateReportsRequestList();
		list.setTotalRecordCount(count);
		list.setReportsRequests(this.setupGenerateRequestReps(listReq));
		return list;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/getUserReportRequests/{userId}/{status : .+}")
	public ReportRepresentationList getUserReportRequests(@PathParam(value = "userId") int userId,
			@PathParam("status") List<PathSegment> status, @DefaultValue("100") @QueryParam("max") int maxResults,
			@DefaultValue("0") @QueryParam("page") int page,  
			@DefaultValue("0") @QueryParam("subjectId") int subjectId, 
			@Context HttpHeaders hh){
		log.debug("in get User report requests..");
		List<String> statuses = new ArrayList<String>();
		for (PathSegment segment : status){
			log.debug("Got path segment path: {}", segment.getPath());
			statuses.add(segment.getPath());

		}
		List<ReportRequest> reportRequests = new ArrayList<ReportRequest>();
		long count = 0;
		if (statuses.contains("ALL")){
			//TODO:  authorize group reports ... not needed right now
			count = reportRequestDao.getCountIndividualRequestsByUserId(userId);
			reportRequests = reportRequestDao.getIndividualRequestsByUserId(userId, maxResults, page);
		}else{
			//  reportAuthorizationBean.authorizeUserGroupReports(subjectId, userId)
			if (subjectId == 0 ||  reportAuthorizationBean.authorizeUserGroupReports(subjectId, userId)){
				count = reportRequestDao.getCountAllRequestsByUserIdAndStatus(userId, statuses);
				reportRequests = reportRequestDao.getAllRequestsByUserIdAndStatus(userId, statuses, maxResults, page);
			}else{
				//user is not authorized to see group reports for this user
				count = reportRequestDao.getCountIndividualRequestsByUserIdAndStatus(userId, statuses);
				reportRequests = reportRequestDao.getIndividualRequestsByUserIdAndStatus(userId, statuses, maxResults, page);
			}
		}

		log.debug("Got {} Report Requests", reportRequests.size());
		List<ReportRepresentation> rptRequests = new ArrayList<ReportRepresentation>();
		for (ReportRequest rptReq : reportRequests){

			rptRequests.add(RepresentationFactory.createReportRepresentation(rptReq));
		}
		if (subjectId > 0){
			rptRequests = reportAuthorizationBean.authorizeReports(rptRequests, subjectId);
		}
		ReportRepresentationList list = new ReportRepresentationList();
		list.setTotalRecordCount(count);
		list.setReports(rptRequests);
		
		if (hh.getAcceptableMediaTypes().contains(MediaType.APPLICATION_JSON)){
			//TODO: fix json response  
		}
		
		return list;
	}

	private String getJsonStringReportRep(ReportRepresentation rep){
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        mapper.setDateFormat(outputFormat);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        String json = "";
        try {
			mapper.writeValue(System.out, rep);
			json = mapper.writeValueAsString(rep);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
        
        log.debug("Returning json response: {}", json);
        
        return json;
	}
	
	private List<GenerateReportsRequest> setupGenerateRequestReps(List<ReportGenerateRequest> listReq){
		List<GenerateReportsRequest> genPojoList = new ArrayList<GenerateReportsRequest>();
		for (ReportGenerateRequest req : listReq){
			
			GenerateReportsRequest pojo = this.setupGenerateRequestRep(req, false);
			if (!(pojo == null)){
				genPojoList.add(pojo);
			}
		}
		return genPojoList;
	}
	
	private GenerateReportsRequest setupGenerateRequestRep(ReportGenerateRequest req, boolean includeChildren){
		
		GenerateReportsRequest pojo = new GenerateReportsRequest();
		pojo.setParticipantCount(req.getReportParticipants().size());
		pojo.setReportRequestCount(req.getReportRequests().size());
		
		//Status Logic -- Omit Requests where all reports have been deleted.
		//Otherwise, select the Status with the highest ID value (not including deleted)
		//I know this is brain-dead.  Give me business rules and i will fix.
		int statusId = 1;
		pojo.setStatus(ReportRequestStatus.STATUS_REQUESTED);
		boolean allDeleted = true;
		for (ReportRequest rptReq : req.getReportRequests()){
			if (rptReq.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_DELETED)){
				continue;
			}
			allDeleted = false;
			if (rptReq.getReportRequestStatus().getReportRequestStatusId() > statusId){
				pojo.setStatus(rptReq.getReportRequestStatus().getCode());
				statusId = rptReq.getReportRequestStatus().getReportRequestStatusId();
			}
		}
		if (allDeleted){
			return null;
		}
		pojo.setPrincipal(req.getPrincipal());
		pojo.setSubjectId(req.getSubjectId());
		pojo.setRequestTime(req.getRequestTime());
		pojo.setPriority(req.getPriority());
		pojo.setRequestId(req.getReportGenerateRequestId());
		pojo.setRequestName(req.getRequestName());
		
		if (includeChildren){
			pojo.setParticipants(new ArrayList<ReportParticipantRepresentation>());
			for (ReportParticipant participant : req.getReportParticipants()){
				ReportParticipantRepresentation repPptRep = new ReportParticipantRepresentation(participant);
				pojo.getParticipants().add(repPptRep);
			}
			pojo.setReportTypeList(new ArrayList<ReportRepresentation>());
			for (ReportRequest request : req.getReportRequests()){
				ReportRepresentation repRep = new ReportRepresentation(request);
				if (!request.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_DELETED)){
					pojo.getReportTypeList().add(repRep);
				}else{
					log.trace("Status is deleted.  Omitting");
				}
			}
		}
		
		return pojo;
	}

	private GenerateReportsRequest setupGenerateRequestRepFromReportList(List<ReportRequest> rptList){
		Map<Integer, ReportParticipant> pptMap = new HashMap<Integer, ReportParticipant>();
		for (ReportRequest rr : rptList){
			for (ReportParticipant p : rr.getReportParticipants()){
				//make unique key.  this handles same user from different projects.
				int index = (p.getUser().getUsername() + p.getProject().getProjectId()).hashCode();
				pptMap.put(index, p);
			}
		}
		GenerateReportsRequest pojo = new GenerateReportsRequest();
		pojo.setParticipantCount(pptMap.size());
		pojo.setReportRequestCount(rptList.size());
		
		//Status Logic -- Omit Requests where all reports have been deleted.
		//Otherwise, select the Status with the highest ID value (not including deleted)
		//I know this is brain-dead.  Give me business rules and i will fix.
		int statusId = 1;
		pojo.setStatus(ReportRequestStatus.STATUS_REQUESTED);
		boolean allDeleted = true;
		for (ReportRequest rptReq : rptList){
			if (rptReq.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_DELETED)){
				continue;
			}
			allDeleted = false;
			if (rptReq.getReportRequestStatus().getReportRequestStatusId() > statusId){
				pojo.setStatus(rptReq.getReportRequestStatus().getCode());
				statusId = rptReq.getReportRequestStatus().getReportRequestStatusId();
			}
		}
		if (allDeleted){
			return null;
		}
		ReportGenerateRequest req = rptList.get(0).getReportGenerateRequest();
		pojo.setPrincipal(req.getPrincipal());
		pojo.setSubjectId(req.getSubjectId());
		pojo.setRequestTime(req.getRequestTime());
		pojo.setPriority(req.getPriority());
		pojo.setRequestId(req.getReportGenerateRequestId());
		pojo.setRequestName(req.getRequestName());
		
	
		pojo.setParticipants(new ArrayList<ReportParticipantRepresentation>());
		for (Integer i : pptMap.keySet()){
			ReportParticipantRepresentation repPptRep = new ReportParticipantRepresentation(pptMap.get(i));
			pojo.getParticipants().add(repPptRep);
		}
		pojo.setReportTypeList(new ArrayList<ReportRepresentation>());
		for (ReportRequest request : rptList){
			ReportRepresentation repRep = new ReportRepresentation(request);
			if (!request.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_DELETED)){
				pojo.getReportTypeList().add(repRep);
			}else{
				log.trace("Status is deleted.  Omitting");
			}
		}
		
		
		return pojo;
	}
}
