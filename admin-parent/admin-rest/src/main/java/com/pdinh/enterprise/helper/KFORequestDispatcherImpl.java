package com.pdinh.enterprise.helper;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pdinh.data.jaxb.reporting.KFOnlineReportRepresentation;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.QueueReportResultObj;
import com.pdinh.presentation.domain.ResultObj;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Stateless
@LocalBean
public class KFORequestDispatcherImpl implements KFORequestDispatcherLocal {
	
	private static final Logger log = LoggerFactory.getLogger(KFORequestDispatcherImpl.class);
	
	@EJB
	private ReportRequestDao reportRequestDao;
	
	@EJB
	private ReportingSingleton reportingSingleton;

	@EJB
	private KFOServiceHelper helper;
	
	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;

	//Create a new Transaction for each call to this method so that status updates
	//and request key value will be committed immediately, rather than at the end
	//of the larger, outer transaction which may contain many reports.
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void queueReportRequest(ReportRequest requestEntity) {
		
		ResultObj dispatchResult = this.dispatchMessage(requestEntity);
		log.debug("Result from dispatchMessage: {}", dispatchResult.getSuccess());
		
		if (dispatchResult.getSuccess()){
			
			requestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_QUEUED));
			
			requestEntity.setReportRequestKey(((QueueReportResultObj)dispatchResult).getReportRequestKey());
			
			reportRequestDao.update(requestEntity);
		
			
		
		}else{
			//TODO:  alert the worker service to check system health
			log.debug("Error Code: {}", ((ErrorObj)dispatchResult).getErrorCode());
			if (((ErrorObj)dispatchResult).getErrorCode().equals(PalmsErrorCode.FAILED_TO_QUEUE.getCode())){
				log.debug("Queuing failed Status: {}", ReportRequestStatus.STATUS_QUEUEING_FAILED);
				requestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_QUEUEING_FAILED));
				requestEntity.setErrorCode(PalmsErrorCode.FAILED_TO_QUEUE.getCode());
			}else{
				requestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
				requestEntity.setErrorCode(((ErrorObj)dispatchResult).getErrorCode());
			}
			requestEntity.setErrorMessage(((ErrorObj)dispatchResult).getErrorCode() + ": " 
			+ ((ErrorObj)dispatchResult).getErrorMsg());
			log.debug("Updating Request entity with error status: {}", ((ErrorObj)dispatchResult).getErrorCode() + ": " 
			+ ((ErrorObj)dispatchResult).getErrorMsg());
			reportRequestDao.update(requestEntity);
	
		}
		
	}

	@Override
	public ResultObj dispatchMessage(ReportRequest rptReq) {
		Map<String, String> jsonResultMap = new HashMap<String, String>();
		
		String urlTarget = properties.getProperty("KFReportAPIBaseUrl");
		urlTarget = urlTarget + "/QueueReport";
		log.debug("Got urlTarget for Queue Report service: {}", urlTarget);
		Client client = helper.getKFOJerseyClient();
		WebResource resource = client.resource(urlTarget);
		//HttpURLConnection conn = helper.getSecureConnection(urlTarget);
		KFOnlineReportRepresentation kfrptrep = new KFOnlineReportRepresentation();
		if (rptReq.getReportType().getContentType().equals("application/pdf")){
			kfrptrep.setFileExtention("pdf");
		}else if (rptReq.getReportType().getContentType().equals("application/excel")){
			kfrptrep.setFileExtention("xlsx");
		}else{
			kfrptrep.setFileExtention("xlsx");
		}
		
		kfrptrep.setMetaData(helper.makeMetadataString(rptReq));
		
		switch (rptReq.getReportGenerateRequest().getPriority()){
			case ReportGenerateRequest.REQUEST_PRIORITY_HIGH:
				kfrptrep.setPriority("HIGH");
				break;
			case ReportGenerateRequest.REQUEST_PRIORITY_LOW:
				kfrptrep.setPriority("LOW");
				break;
			default:
				kfrptrep.setPriority("LOW");
				break;
		}
		
		
        String json = "";
        ObjectMapper mapper = helper.getObjectMapper();
        try {
			
			json = mapper.writeValueAsString(kfrptrep);
			log.debug("Sending this json: {}", json);
		} catch (Exception e) {
			log.error("Caught exception: {}", e.getMessage());
			return new ErrorObj(PalmsErrorCode.FAILED_TO_QUEUE.getCode(), PalmsErrorCode.FAILED_TO_QUEUE.getMessage());
			
		} 
	   try {  
	        /*
	        PrintWriter pw = new PrintWriter(conn.getOutputStream());
			pw.write(json);
			pw.close();
			*/
		   ClientResponse response = resource.type(MediaType.APPLICATION_JSON)
					//.header("MetaData", dumbjson)
                   .post(ClientResponse.class, json);
			
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			try{
				//jsonResultMap = mapper.readValue(conn.getInputStream(), HashMap.class);
				jsonResultMap = mapper.readValue(response.getEntityInputStream(), HashMap.class);
			}catch(JsonParseException jpe){
				log.error("Caught json parse exception: {}", jpe.getMessage());
				jpe.printStackTrace();
			}catch(JsonMappingException jme){
				log.error("Caught json mapping exception: {}", jme);
				jme.printStackTrace();
			}
			
			log.debug("Got jsonResultMap from input stream:  Size: {}", jsonResultMap.size());
			if (jsonResultMap.containsKey("ReportRequestKey")){
				log.debug("Returning Successful result with key: {}", jsonResultMap.get("ReportRequestKey"));
				return new QueueReportResultObj(jsonResultMap.get("ReportRequestKey"));
			}else{
				log.debug("Returning Error Result");
				return new ErrorObj(jsonResultMap.get("ErrorCode"), jsonResultMap.get("ErrorMessage"));
			}
		} catch(IOException ioe){
			log.error("Caught IO Exception: {}", ioe.getMessage());
			ioe.printStackTrace();
			return new ErrorObj(PalmsErrorCode.FAILED_TO_QUEUE.getCode(), PalmsErrorCode.FAILED_TO_QUEUE.getMessage());
		} catch(Exception e){
			log.error("Caught exception: {}", e.getMessage());
			e.printStackTrace();
			return new ErrorObj(PalmsErrorCode.FAILED_TO_QUEUE.getCode(), PalmsErrorCode.FAILED_TO_QUEUE.getMessage());
		}finally{
			//conn = null;
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateStatusBatch(List<ReportRequest> requests,
			String statusCode) {
		for (ReportRequest rr : requests){
			rr.setReportRequestStatus(reportingSingleton.getReportRequestStatus(statusCode));
			rr.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(rr);
		}
		
	}
	

}
