package com.pdinh.enterprise.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.pdinh.data.ms.dao.UserDocumentTypeDao;
import com.pdinh.enterprise.rest.representation.RepresentationFactory;
import com.pdinh.enterprise.rest.representation.UserDocumentTypeRepresentation;
import com.pdinh.enterprise.rest.representation.listwrapper.UserDocumentTypeRepresentationList;
import com.pdinh.persistence.ms.entity.UserDocumentType;

@Stateless
@Path("/userDocumentTypes")
public class UserDocumentTypeResource {
	
	@EJB
	private UserDocumentTypeDao userDocumentTypeDao;
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public UserDocumentTypeRepresentationList getUserDocumentTypesAsXml(@Context UriInfo uriInfo) {		
		List<UserDocumentType> userDocumentTypes = userDocumentTypeDao.findAll();
		List<UserDocumentTypeRepresentation> userDocumentTypeRepresentations = new ArrayList<UserDocumentTypeRepresentation>();
		
		RepresentationFactory rf = new RepresentationFactory();
		rf.setBaseUri(uriInfo.getBaseUri().toASCIIString());
		
		for(UserDocumentType udt : userDocumentTypes) {
			userDocumentTypeRepresentations.add(rf.createUserDocumentTypeRepresentation(udt));
		}
		
		UserDocumentTypeRepresentationList list = new UserDocumentTypeRepresentationList();
		list.setList(userDocumentTypeRepresentations);
		
		return list;
	}
		
}
