package com.pdinh.enterprise.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.CreateUserServiceLocal;
import com.pdinh.data.EnrollmentServiceLocal;
import com.pdinh.data.EnrollmentServiceLocalImpl;
import com.pdinh.data.ProjectMembershipServiceLocal;
import com.pdinh.data.UsernameServiceLocal;
import com.pdinh.data.jaxb.admin.CreateUserRequest;
import com.pdinh.data.jaxb.admin.CreateUserResponse;
import com.pdinh.data.jaxb.lrm.AddUserResponse;
import com.pdinh.data.jaxb.lrm.AddUserServiceLocal;
import com.pdinh.data.jaxb.lrm.LrmObjectExistResponse;
import com.pdinh.data.jaxb.lrm.LrmObjectServiceLocal;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.ms.dao.UserDocumentDao;
import com.pdinh.data.ms.dao.UserDocumentTypeDao;
import com.pdinh.enterprise.rest.representation.RepresentationFactory;
import com.pdinh.enterprise.rest.representation.UserDocumentRepresentation;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.FileAttachment;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.persistence.ms.entity.UserDocument;
import com.pdinh.persistence.ms.entity.UserDocumentType;
import com.pdinh.presentation.domain.AddUserResultObj;
import com.pdinh.presentation.domain.EmailEventObj;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Stateless
@Path("/users")
public class UserResource {
	private static final Logger log = LoggerFactory.getLogger(UserResource.class);
	@EJB 
	private UserDao userDao;
	
	@EJB
	private ProjectDao projectDao;
	
	@EJB
	private PositionDao positionDao;
	
	@EJB
	private CreateUserServiceLocal createUserService;
	
	@EJB
	private LrmObjectServiceLocal lrmObjectService;
	
	@EJB
	private AddUserServiceLocal addUserService;
	
	@EJB
	private UsernameServiceLocal usernameService;
	
	@EJB 
	private UserDocumentTypeDao userDocumentTypeDao;
	
	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;
	
	@EJB
	private UserDocumentDao userDocumentDao;
	
	@EJB
	private ScheduledEmailDefDao emailDefDao;
	
	@EJB
	private MailTemplatesServiceLocalImpl mailTemplatesService;
	
	@EJB
	private ProjectMembershipServiceLocal projectMembershipService;
	
	@EJB
	private EnrollmentServiceLocalImpl enrollmentService;
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Path("{usersId}/userDocuments")
	public Response createUserDocument(@PathParam("usersId") int usersId, 
		@FormDataParam("file") InputStream is, @FormDataParam("file") FormDataContentDisposition fileInfo,
		@FormDataParam("representation") UserDocument userDocument, @Context UriInfo uriInfo) {
		
		User user = userDao.findById(usersId);
		
		if(user == null) {
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
				.entity("Unable to find user with usersId: " + usersId).build());
		}
		
		userDocument.setUser(user);
		
		if(userDocument.getUserDocumentType() != null) {
			UserDocumentType userDocType = userDocument.getUserDocumentType();
			//if id only specified we need to pull the real type from the DB
			if(userDocType.getUserDocumentTypeId() > 0 && (userDocType.getName() == null || userDocType.getName().equals(""))) {
				userDocument.setUserDocumentType(userDocumentTypeDao.findById(userDocType.getUserDocumentTypeId()));
			}
		}
		
		FileAttachment attachment = new FileAttachment();
		attachment.setDateUploaded(new Timestamp(System.currentTimeMillis()));
		attachment.setName(Utils.getFileNameFromPath(fileInfo.getFileName()));
		attachment.setExtAdminId("");
		attachment.setPath(UUID.randomUUID().toString());
		
		try {
			fileAttachmentService.write(attachment, is);
		}catch (IOException e) {
			throw new WebApplicationException(e,Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity("Unable to write file " + fileInfo.getName() + ", " + e.getMessage()).build());
		}
		
		userDocument.setFileAttachment(attachment);
		user.getUserDocuments().add(userDocument);
		
		//would rather use cascade but create associates userDocument with persistence context so 
		//the generated IDs are available
		userDocumentDao.create(userDocument);							 
		
		String selfUri = uriInfo.getAbsolutePath() + "/" + userDocument.getDocumentId();
						
		RepresentationFactory rf = new RepresentationFactory();
		rf.setBaseUri(uriInfo.getBaseUri().toASCIIString());
		
		UserDocumentRepresentation rep = rf.createUserDocumentRepresentation(userDocument);
		
		return Response.created(URI.create(selfUri)).entity(rep).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Path("validateNewUserRequest")
	public CreateUserResponse validateNewUserRequest(CreateUserRequest cruRequest) {
		CreateUserResponse responseXml = new CreateUserResponse();
		
		Project project = projectDao.findBatchById(cruRequest.getProjectId());
		if (project == null){
			responseXml.setSuccess(false);
			responseXml.setMessage("Invalid Project Id: " + cruRequest.getProjectId());
		
			return responseXml;
		}else{
			log.debug("Got Project: {}", project.getName());
		}
		Company company = project.getCompany();
		
		User user = new User();
		user.setLangPref(cruRequest.getLanguagePreference());
		user.setFirstname(cruRequest.getFirstname());
		user.setLastname(cruRequest.getLastname());
		user.setEmail(cruRequest.getEmail());
		if (!(cruRequest.getOptional1() == null || cruRequest.getOptional1().isEmpty())){
			user.setOptional1(cruRequest.getOptional1());
		}
		user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(), user.getLastname()));
		user.setType(1);
		user.setMserverid((short) 1);
		user.setCompany(company);
		user.setPassword(createUserService.getNewPassword());
		AddUserResultObj result = (AddUserResultObj) createUserService.validateUser(user);

		if (!result.getSuccess()) {
			//Return Error
			responseXml.setSuccess(false);
			String message = "";
			if (result.getErrors().size() == 1){
				ErrorObj err = result.getErrors().get(0);
				if (err.getErrorCode().equalsIgnoreCase("IMEC007")){
					responseXml.setSuccess(true);
					responseXml.setMessage("User account exists.");
				}
			}
			for (ErrorObj error : result.getErrors()){
				message = message + error.getErrorMsg() + "\n";
				log.debug("Error Code: {}", error.getErrorCode());
				responseXml.setErrorCode(error.getErrorCode());
			}
			
			responseXml.setMessage(message);
		}else{
			responseXml.setSuccess(true);
			responseXml.setMessage("User validation succeeded.");
			
		}
		return responseXml;
	}
	/**
	 * Creates a user in the given project, or just adds the user to the project if a user with the 
	 * same email already exists in the company
	 * @param cruRequest
	 * @param uriInfo
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Path("createNewUser")
	public CreateUserResponse createNewUser(CreateUserRequest cruRequest, @Context UriInfo uriInfo) {
		CreateUserResponse responseXml = new CreateUserResponse();
		
		Project project = projectDao.findBatchById(cruRequest.getProjectId());
		if (project == null){
			responseXml.setSuccess(false);
			responseXml.setMessage("Invalid Project Id: " + cruRequest.getProjectId());
		
			return responseXml;
		}else{
			log.debug("Got Project: {}", project.getName());
		}
		Company company = project.getCompany();
		
		User user = new User();
		user.setLangPref(cruRequest.getLanguagePreference());
		user.setFirstname(cruRequest.getFirstname());
		user.setLastname(cruRequest.getLastname());
		user.setEmail(cruRequest.getEmail());
		if (!(cruRequest.getOptional1() == null || cruRequest.getOptional1().isEmpty())){
			user.setOptional1(cruRequest.getOptional1());
		}
		user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(), user.getLastname()));
		user.setType(1);
		user.setMserverid((short) 1);
		user.setCompany(company);
		user.setPassword(createUserService.getNewPassword());
		AddUserResultObj result = (AddUserResultObj) createUserService.validateUser(user);

		if (!result.getSuccess()) {
			//Return Error
			responseXml.setSuccess(false);
			String message = "";
			if (result.getErrors().size() == 1){
				ErrorObj err = result.getErrors().get(0);
				if (err.getErrorCode().equalsIgnoreCase("IMEC007")){
					List<User> existingUserList = userDao.findByEmailAndCompany(user.getEmail(), company.getCompanyId());
					
					if (!(existingUserList == null) && existingUserList.size() == 1){
						User existingUser = existingUserList.get(0);
						ProjectUser puser = projectMembershipService.addUserToProjectWithEnrollment(existingUser, project);
						//enrollUserInCourses(project.getProjectCourses(), existingUser);
						if (!(puser == null)){
							log.debug("User {} added to project {}", existingUser.getUsername(), project.getName());
						}else{
							//user already in project
							log.debug("User {} already in  project {}", existingUser.getUsername(), project.getName());
						}
						responseXml.setSuccess(true);
						responseXml.setUsersId(existingUser.getUsersId());
						responseXml.setUsername(existingUser.getUsername());
						responseXml.setLoginStatus(existingUser.getLoginStatus());
					}
				}else{
					responseXml.setMessage(err.getErrorMsg());
					responseXml.setErrorCode(err.getErrorCode());
				}
			}else{
				for (ErrorObj error : result.getErrors()){
					message = message + error.getErrorMsg() + "\n";
					responseXml.setErrorCode(error.getErrorCode());
				}
				responseXml.setMessage(message);
			}
		}else{
			user = createUserService.createNewUser(user, company, project, cruRequest.getCHQlanguage(), cruRequest.getFEXlanguage());
			
			responseXml.setUsersId(user.getUsersId());
			responseXml.setUsername(user.getUsername());
			responseXml.setSuccess(true);
			
			List<User> singleton = new ArrayList<User>();
			singleton.add(user);
			
			LrmObjectExistResponse lrmRsp = lrmObjectService.projectExist(String.valueOf(project.getProjectId()));

			if (lrmRsp.isExist()) {
				AddUserResponse response = addUserService.addUsersToLrm(singleton, project.getProjectId(), 0);
				if (response.getPostResult().equalsIgnoreCase("FAILED")) {
					String msg = "User was added to the project, but the attempt to update the user record in PDA failed.";
					responseXml.setSuccess(true);
					responseXml.setMessage(msg);
				}
			}
			if (cruRequest.getEmailId() > 0){
				this.sendWelcomeEmail(cruRequest.getEmailId(), user);
			}
		}
		/*
		Response response = Response
				.ok()
				.entity(responseXml)
				.type(MediaType.APPLICATION_XML)
				.build();
				*/
		return responseXml;
	}
	/*
	private void enrollUserInCourses(List<ProjectCourse> pcList, User user){
		// Loop through courses in project and add the position entries
		Iterator<ProjectCourse> pcIterator = pcList.iterator();
		while (pcIterator.hasNext()) {
			ProjectCourse projectCourse = pcIterator.next();
			// JJB: Should only enroll if not already enrolled
			if (projectCourse.getSelected()) {
				user = enrollmentService.enrollUserInCourse(user.getCompany(), projectCourse, user);
				enrollmentService.activateDeactivateInstrument(user.getCompany(), projectCourse,
						user);
			} else {
				activateUnactivateInstrument(projectCourse, user.getPositions());
			}
		}
	}
	
	private void activateUnactivateInstrument(ProjectCourse projectCourse, List<Position> positions){
		
		log.debug("In activateUnactivateInstrument");
		// JJB: Making assumption then when an existing user is added to a
		// project with assessment day instruments
		// that those assessment day instrument become inactive
		for (Position position : positions) {
			if (position.getCourse().getAbbv().equals(projectCourse.getCourse().getAbbv())
					&& projectCourse.getSelected()) {
				if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 1 && !position.getActivated()) {
					log.debug("Activate deactivated instrument {}", position.getCourse().getAbbv());
					position.setActivated(true);
					positionDao.update(position);
				} else if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 2 && position.getActivated()) {
					log.debug("Deactivate activated instrument {}", position.getCourse().getAbbv());
					position.setActivated(false);
					positionDao.update(position);
				}
			}
		}
		
	}
	*/
	private void sendWelcomeEmail(int emailDefId, User user){
		ScheduledEmailDef def = emailDefDao.findById(emailDefId);
		
		List<ScheduledEmailText> texts = def.getScheduledEmailTexts();
		int id = 0;

		for (ScheduledEmailText text : texts){
			if (text.getLanguage().getCode().equalsIgnoreCase(user.getLangPref())){
				id = text.getId();
			}
		}
		EmailEventObj emailEvent = new EmailEventObj();
		//public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName) {
		ParticipantObj pobj = new ParticipantObj(user.getUsersId(), user.getFirstname(), user.getLastname(), user.getEmail(), user.getUsername());
		pobj.setPassword(user.getPassword());
		emailEvent.setParticipant(pobj);
		emailEvent.setCompany(user.getCompany());
		emailEvent.setRecipientEmailAddress(user.getEmail());
		emailEvent.setRecipientFirstLastName(user.getFirstname() + " " + user.getLastname());
		
		//emailEvent.setFromAddress("NoReply@KornFerry.com");
		//emailEvent.setSenderEmail("NoReply@KornFerry.com");
		
		//mailTemplatesService.sendTemplateInternal(id, emailEvent);
		mailTemplatesService.sendTemplate(id, emailEvent);
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public FileAttachmentServiceLocal getFileAttachmentService() {
		return fileAttachmentService;
	}

	public void setFileAttachmentService(
			FileAttachmentServiceLocal fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	public UserDocumentTypeDao getUserDocumentTypeDao() {
		return userDocumentTypeDao;
	}

	public void setUserDocumentTypeDao(UserDocumentTypeDao userDocumentTypeDao) {
		this.userDocumentTypeDao = userDocumentTypeDao;
	}

	public UserDocumentDao getUserDocumentDao() {
		return userDocumentDao;
	}

	public void setUserDocumentDao(UserDocumentDao userDocumentDao) {
		this.userDocumentDao = userDocumentDao;
	}
}
