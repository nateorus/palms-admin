package com.pdinh.enterprise.rest.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Links used to refer to other resources
 */

@XmlRootElement(name="link")
@XmlAccessorType(XmlAccessType.FIELD)
public class Link {

	@XmlAttribute
	private String rel;
	
	@XmlAttribute
	private String href;
	
	@XmlAttribute
	private String type;
	
	public Link() {		
	}
	
	public Link(String rel, String href) {
		this.rel = rel;
		this.href = href;
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	
	
}
