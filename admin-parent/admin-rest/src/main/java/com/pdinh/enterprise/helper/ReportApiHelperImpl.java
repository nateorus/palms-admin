package com.pdinh.enterprise.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ReportGenerateRequestDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectTypeReportType;
import com.pdinh.persistence.ms.entity.ReportGenerateRequest;
import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.persistence.ms.entity.User;
@Stateless
@LocalBean
public class ReportApiHelperImpl implements ReportApiHelperLocal {
	private static final Logger log = LoggerFactory.getLogger(ReportApiHelperImpl.class);
	
	@EJB
	private UserDao userDao;

	@EJB
	private LanguageSingleton languageSingleton;


	@EJB
	private ProjectDao projectDao;
	
	@EJB
	private ReportingSingleton reportingSingleton;
	
	@EJB
	private ReportGenerateRequestDao reportGenerateRequestDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ReportGenerateRequest createReportGenerateRequestEntity(
			GenerateReportsRequest rptRequest) throws PalmsException {
		log.debug("In setupReportGenerateRequestEntity()");
		ReportGenerateRequest rptGenReq = new ReportGenerateRequest();
		rptGenReq.setRequestTime(new Timestamp(new Date().getTime()));
		if (rptRequest.getPrincipal() == null || rptRequest.getPrincipal().isEmpty()){
			throw new PalmsException("No Request principal was supplied.", PalmsErrorCode.BAD_REQUEST.getCode(), "Principal is required to request reports.");
		}
		rptGenReq.setSubjectId(rptRequest.getSubjectId());
		rptGenReq.setPrincipal(rptRequest.getPrincipal());
		rptGenReq.setPriority(rptRequest.getPriority());
		String requestName = rptRequest.getRequestName();
		if (!(requestName == null)){
			rptGenReq.setRequestName(requestName);
		}
		
		List<ReportParticipant> rptPptList = new ArrayList<ReportParticipant>();
		for (ReportParticipantRepresentation rpr : rptRequest.getParticipants()){
			ReportParticipant ppt = new ReportParticipant();
			ppt.setReportGenerateRequest(rptGenReq);
			if (!(rpr.getReportLang() == null) && !rpr.getReportLang().isEmpty()){
				Language pptLang = languageSingleton.getLanguageByCode(rpr.getReportLang());
				
				ppt.setLanguage(pptLang);
			}
			User user = userDao.findById(rpr.getUsersId());
			if (user == null){
				throw new PalmsException("Invalid Participant ID specified.", PalmsErrorCode.BAD_REQUEST.getCode());
			}
			ppt.setUser(user);
			Project project = projectDao.findById(rpr.getProjectId());
			if (project == null){
				throw new PalmsException("Invalid Project ID specified.", PalmsErrorCode.BAD_REQUEST.getCode());
			}
			ppt.setProject(project);
			if (!(rpr.getCourseVersionMap() == null || rpr.getCourseVersionMap().isEmpty())){
				ppt.setCourseVersionMap(rpr.getCourseVersionMap());
			}
			rptPptList.add(ppt);
		}
		List<ReportRequest> rptRequestList = new ArrayList<ReportRequest>();
		for (ReportRepresentation rpt : rptRequest.getReportTypeList()){
			log.debug("Report type: {}", rpt.getCode());
			log.debug("Report title: {}", rpt.getTitle());
			log.debug("Report target level: {}", rpt.getTargetLevel());
			log.debug("Report name: {}", rpt.getReportName());
			log.debug("Report lang: {}", rpt.getReportLang());
			TargetLevelType tltype = null;
			if (!((rpt.getTargetLevel() == null) || rpt.getTargetLevel().isEmpty())){
				tltype = reportingSingleton.getTargetLevelTypeByCode(rpt.getTargetLevel());
				if (tltype == null){
					throw new PalmsException("Invalid Target Level specified.", PalmsErrorCode.BAD_REQUEST.getCode());
				}
			}


			ReportType rptType = reportingSingleton.getReportType(rpt.getCode());
			if (rptType == null){
				log.error("Report type is null.  Throwing palms exception");
				throw new PalmsException("Invalid Report Type specified.", PalmsErrorCode.BAD_REQUEST.getCode());
			}
			if (rptType.isGroupReport()){
				ReportRequest rptRequestEntity = new ReportRequest();
				rptRequestEntity.setReportParticipants(this.getValidParticipants(rptPptList, rpt));
				if (rpt.getReportLang() == null){
					rptRequestEntity.setLanguage(languageSingleton.getLanguageByCode("en"));
				}else{
					Language rptLang = languageSingleton.getLanguageByCode(rpt.getReportLang());
					if (rptLang == null){
						throw new PalmsException("Invalid Report Language specified.", PalmsErrorCode.BAD_REQUEST.getCode());
					}
					rptRequestEntity.setLanguage(rptLang);
				}
				rptRequestEntity.setReportGenerateRequest(rptGenReq);
				if (!(rpt.getReportIdNameOption() == null || rpt.getReportIdNameOption().isEmpty())){
					rptRequestEntity.setReportIdNameOption(rpt.getReportIdNameOption());
				}
				if (!(rpt.getTitle() == null || rpt.getTitle().isEmpty())){
					rptRequestEntity.setReportTitle(rpt.getTitle());
				}
				if (!(rpt.getContentType() == null || rpt.getContentType().isEmpty())){
					rptRequestEntity.setContentType(rpt.getContentType());
				}
				
				rptRequestEntity.setFilesize(rpt.getFilesize());
				rptRequestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_REQUESTED));
				rptRequestEntity.setStatusUpdateTs(new Timestamp(new Date().getTime()));
				rptRequestEntity.setReportType(rptType);
				if (!(tltype == null)){
					rptRequestEntity.setTargetLevelType(tltype);
				}
				rptRequestEntity.setCreateType(rpt.getCreateType());
				rptRequestList.add(rptRequestEntity);

			}else{
				for (ReportParticipant rptPpt : rptPptList){
					if (!this.isValidReportForProjectType(rpt, rptPpt.getProject())){
						log.trace("Report type not valid for this participant({}) and project({}).  Skipping", 
								rptPpt.getUser().getUsersId(), rptPpt.getProject().getProjectId());
						continue;
					}
					ReportRequest rptRequestEntity = new ReportRequest();
					List<ReportParticipant> singleUserList = new ArrayList<ReportParticipant>();
					singleUserList.add(rptPpt);
					rptRequestEntity.setReportParticipants(singleUserList);
					//Use participant language if available, if not then report lang.  english as last resort
					if (rptPpt.getLanguage() == null){
						if (rpt.getReportLang() == null){
							rptRequestEntity.setLanguage(languageSingleton.getLanguageByCode("en"));
						}else{
							rptRequestEntity.setLanguage(languageSingleton.getLanguageByCode(rpt.getReportLang()));
						}
					}else{
						rptRequestEntity.setLanguage(rptPpt.getLanguage());
					}
					
					rptRequestEntity.setReportGenerateRequest(rptGenReq);
					if (!(rpt.getReportIdNameOption() == null || rpt.getReportIdNameOption().isEmpty())){
						rptRequestEntity.setReportIdNameOption(rpt.getReportIdNameOption());
					}
					rptRequestEntity.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_REQUESTED));
					rptRequestEntity.setStatusUpdateTs(new Timestamp(new Date().getTime()));
					rptRequestEntity.setReportType(rptType);
					if (!(tltype == null)){
						rptRequestEntity.setTargetLevelType(tltype);
					}
					if (!(rpt.getTitle() == null || rpt.getTitle().isEmpty())){
						rptRequestEntity.setReportTitle(rpt.getTitle());
					}
					if (!(rpt.getContentType() == null || rpt.getContentType().isEmpty())){
						rptRequestEntity.setContentType(rpt.getContentType());
					}
					rptRequestEntity.setFilesize(rpt.getFilesize());
					rptRequestEntity.setCreateType(rpt.getCreateType());
					rptRequestList.add(rptRequestEntity);
				}
			}

		}
		//if mismatch with project type/report type mappings, this could occur.
		if (rptPptList.size() < 1 || rptRequestList.size() < 1){
			throw new PalmsException(PalmsErrorCode.BAD_REQUEST.getMessage(), PalmsErrorCode.BAD_REQUEST.getCode(), "No valid reports found in request.");
		}
		rptGenReq.setReportParticipants(rptPptList);
		rptGenReq.setReportRequests(rptRequestList);
		return reportGenerateRequestDao.create(rptGenReq);
	}
	
	private boolean isValidReportForProjectType(ReportRepresentation rep, Project project){
		//TODO:  update Report Type entity with Other report type code constants for other group and other ind
		if (rep.getCode().startsWith("OTHER")){
			//Other is valid for any project type
			return true;
		}
		for (ProjectTypeReportType ptype : project.getProjectType().getProjectTypeReportTypes()){
			if (ptype.getReportType().getCode().equalsIgnoreCase(rep.getCode())){
				return true;
			}
		}
		return false;
	}
	
	private List<ReportParticipant> getValidParticipants(List<ReportParticipant> ppts, ReportRepresentation rep){
		List<ReportParticipant> returnList = new ArrayList<ReportParticipant>();
		for (ReportParticipant ppt : ppts){
			if (this.isValidReportForProjectType(rep, ppt.getProject())){
				log.trace("Group Report type is valid for this participant({}) and project({}).  Including.", 
						ppt.getUser().getUsersId(), ppt.getProject().getProjectId());
				returnList.add(ppt);
			}
		}
		return returnList;
	}

}
