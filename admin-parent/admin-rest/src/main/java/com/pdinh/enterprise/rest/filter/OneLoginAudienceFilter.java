package com.pdinh.enterprise.rest.filter;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.onelogin.AccountSettings;
import com.onelogin.AppSettings;
import com.onelogin.saml.AuthRequest;

/**
 * Servlet Filter implementation class OneLoginAudienceFilter
 */

public class OneLoginAudienceFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(OneLoginAudienceFilter.class);
    /**
     * Default constructor. 
     */
    public OneLoginAudienceFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		log.debug("In OneLoginAudienceFilter with new Request");
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()){
			chain.doFilter(request, response);
			return;
		}
		//subject.logout();
		/*
		Map<String, String[]> params = request.getParameterMap();
		for(Map.Entry<String, String[]> map : params.entrySet()){
			subject.getSession().setAttribute(map.getKey(), map.getValue());
		}*/
		//subject.getSession().setAttribute(key, value);
		// the appSettings object contain application specific settings used by the SAML library
		  AppSettings appSettings = new AppSettings();
		  
		// set the URL of the consume.jsp (or similar) file for this app. The SAML Response will be posted to this URL
		  
		  String url = request.getServerName() + ":" + request.getServerPort() + req.getContextPath();

		  String protocol = "http://";
		  if (request.isSecure()){
			  protocol = "https://";
		  }
		  appSettings.setAssertionConsumerServiceUrl(protocol + url + "/sso/consumer");
		 
		  log.debug("Set ConsumerServiceUrl: {}", appSettings.getAssertionConsumerServiceUrl());
		  
		  appSettings.setIssuer("Korn/Ferry");
		  log.debug("Set Issuer: {}", appSettings.getIssuer());
		  
		  // the accSettings object contains settings specific to the users account. 
		  // At this point, your application must have identified the users origin
		  AccountSettings accSettings = new AccountSettings();
		  
		  // The URL at the Identity Provider where to the authentication request should be sent
		  accSettings.setIdpSsoTargetUrl("https://app.onelogin.com/trust/saml2/http-post/sso/350444");
		  
		  // Generate an AuthRequest and send it to the identity provider
		  AuthRequest authReq = new AuthRequest(appSettings, accSettings);
		  
		  String reqString = null;
		try {
			reqString = accSettings.getIdp_sso_target_url()+"?SAMLRequest=" + URLEncoder.encode(authReq.getRequest(AuthRequest.base64),"UTF-8");
			//log.debug("Request String: {}", reqString);
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			log.error("Got Error building requestString: {}", e.getMessage());
			e.printStackTrace();
		}
		  
		  resp.sendRedirect(reqString);

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
