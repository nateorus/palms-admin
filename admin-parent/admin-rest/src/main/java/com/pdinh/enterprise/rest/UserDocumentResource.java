package com.pdinh.enterprise.rest;

import java.io.IOException;
import java.net.URI;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.pdinh.data.ms.dao.UserDocumentDao;
import com.pdinh.enterprise.rest.representation.RepresentationFactory;
import com.pdinh.enterprise.rest.representation.UserDocumentRepresentation;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.ms.entity.UserDocument;

@Stateless
@Path("/userDocuments")
public class UserDocumentResource {
			
	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;
	
	@EJB
	private UserDocumentDao userDocumentDao;
	
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Path("{documentId}")
	public Response getUserDocument(@PathParam("documentId") int documentId, @Context UriInfo uriInfo) {
				
		UserDocument userDocument = userDocumentDao.findById(documentId);
		if(userDocument == null) {
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
				.entity("Unable to find userDocument with documentId: " + documentId).build());
		}	
				
		String selfUri = uriInfo.getAbsolutePath().toString();
		
		RepresentationFactory rf = new RepresentationFactory();
		rf.setBaseUri(uriInfo.getBaseUri().toASCIIString());
		
		UserDocumentRepresentation rep = rf.createUserDocumentRepresentation(userDocument);
		
		return Response.ok(URI.create(selfUri)).entity(rep).build();
	}
	
	@DELETE		
	@Path("{documentId}")
	public Response deleteUserDocument(@PathParam("documentId") int documentId,
		@Context UriInfo uriInfo) {
		UserDocument userDocument = userDocumentDao.findById(documentId);
		
		if(userDocument == null) {
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
				.entity("Unable to find userDocument with userDocumentId: " + documentId).build());
		}	
							
		try {
			fileAttachmentService.delete(userDocument.getFileAttachment());
			userDocumentDao.delete(userDocument);
		} catch (IOException e) {		
			throw new WebApplicationException(e,Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity("Unable delete to file " + userDocument.getFileAttachment().getName() + ", " + e.getMessage()).build());
		}			
				
		return Response.noContent().build();		
	}

	public FileAttachmentServiceLocal getFileAttachmentService() {
		return fileAttachmentService;
	}

	public void setFileAttachmentService(
			FileAttachmentServiceLocal fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	public UserDocumentDao getUserDocumentDao() {
		return userDocumentDao;
	}

	public void setUserDocumentDao(UserDocumentDao userDocumentDao) {
		this.userDocumentDao = userDocumentDao;
	}	
	
	
}
