package com.pdinh.enterprise.helper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.rest.security.APIAuthorizationServiceLocal;
import com.pdinh.rest.security.KFOAuthorizationClientFilter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HttpURLConnectionFactory;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
@LocalBean
@Stateless
public class KFOServiceHelper {
	private static final Logger log = LoggerFactory.getLogger(KFOServiceHelper.class);
	@EJB
	private APIAuthorizationServiceLocal authorizationService;
	
	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;
	
	private Client client;
	
	public ObjectMapper getObjectMapper(){
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        //PropertyNamingStrategy.PascalCaseStrategy strategy = new PropertyNamingStrategy.PascalCaseStrategy();
        mapper.setPropertyNamingStrategy(new PropertyNamingStrategy.PascalCaseStrategy());
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        mapper.setDateFormat(outputFormat);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        
        return mapper;
	}
	
	public String makeMetadataString(ReportRequest rptReq){
		String metadata = "";
		metadata = metadata + "reportRequestId." + rptReq.getReportRequestId();
		metadata = metadata + "|reportTypeCode." + rptReq.getReportType().getCode();
		metadata = metadata + "|reportTypeName." + rptReq.getReportType().getName();
		if (!(rptReq.getTargetLevelType() == null)){
			metadata = metadata + "|targetLevelCode." + rptReq.getTargetLevelType().getCode();
		}
		if (!(rptReq.getReportTitle() == null || rptReq.getReportTitle().isEmpty())){
			metadata = metadata + "|reportTitle." + rptReq.getReportTitle();
		}
		metadata = metadata + "|reportLang." + rptReq.getLanguage().getCode();
		if (!(rptReq.getReportIdNameOption() == null || rptReq.getReportIdNameOption().isEmpty())){
			metadata = metadata + "|reportIdNameOption." + rptReq.getReportIdNameOption();
		}
		metadata = metadata + "|requestedBy." + rptReq.getReportGenerateRequest().getPrincipal();
		metadata = metadata + "|subjectId." + rptReq.getReportGenerateRequest().getSubjectId();
		//Probably not needed here... the server knows what time it is.  avoid formatting issues
	//	metadata = metadata + "|requestTime." + rptReq.getReportGenerateRequest().getRequestTime();
		metadata = metadata + "|contentType." + rptReq.getReportType().getContentType();
		
		List<Integer> projectIds = new ArrayList<Integer>();
		List<String> engagementIds = new ArrayList<String>();
		if (!rptReq.getReportType().isGroupReport()){
			for (ReportParticipant rp : rptReq.getReportParticipants()){
				metadata = metadata + "|userId." + rp.getUser().getUsersId();
				if (!projectIds.contains(rp.getProject().getProjectId())){
					projectIds.add(rp.getProject().getProjectId());
					engagementIds.add(rp.getProject().getProjectCode());
				}
			}
			for (int projectId : projectIds){
				metadata = metadata + "|projectId." + projectId;
				
			}
			for (String eId : engagementIds){
				metadata = metadata + "|engagementId." + eId;
			}
		}
		
		return metadata;
	}
	
	public HttpURLConnection getSecureConnection(String urlTarget){
		HttpURLConnection conn = null;
		try{
			URL currUrl = new URL(urlTarget);
	
			conn = (HttpURLConnection) currUrl.openConnection();
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			conn.setRequestProperty("ACCEPT", "application/json");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			
			authorizationService.authorizeKFReportsAPIRequest(conn, properties);
			
			conn.setDoOutput(true);
			return conn;
		}catch(Exception e){
			return null;
		}
	}
	/**
	 * This method returns a single Jersey client to be used for all rest calls to KFO Report api.
	 * You can set http.proxyHost and http.proxyPort properties in your jndi if you want to send
	 * these requests through a proxy for troubleshooting.
	 */
	public Client getKFOJerseyClient() {
		if (client == null){
			log.debug("Initializing Jersey Client");
			DefaultClientConfig config = new DefaultClientConfig();
			config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
			//Client client = Client.create();
			Client client = new Client(new URLConnectionClientHandler(
			        new HttpURLConnectionFactory() {
			    Proxy p = null;
			    @Override
			    public HttpURLConnection getHttpURLConnection(URL url)
			            throws IOException {
			        if (p == null) {
			            if (properties.containsKey("http.proxyHost")){
			            	log.debug("Initializing Proxy for Jersey Client.  Host: {}, Port: {}",
			            			properties.getProperty("http.proxyHost"),
			            			properties.getProperty("http.proxyPort"));
			                p = new Proxy(Proxy.Type.HTTP,
		                        new InetSocketAddress(
		                        properties.getProperty("http.proxyHost"),
		                        Integer.valueOf(properties.getProperty("http.proxyPort"))));
			            } else {
			            	p = Proxy.NO_PROXY;
			            }
			            
			        }
			        return (HttpURLConnection) url.openConnection(p);
			    }
			}), config);
			client.addFilter(new KFOAuthorizationClientFilter(properties));
			this.setClient(client);
		}
		return client;
	}
	
	public String getFileExtention(String contentType){
		if (contentType == null){
			return "";
		}
		if (contentType.equalsIgnoreCase("application/pdf")){
			return "pdf";
		}else if (contentType.equalsIgnoreCase("application/x-excel")){
			return "xls";
		}else if (contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
			return "xlsx";
		}else if (contentType.equalsIgnoreCase("application/msword")){
			return "doc";
		}else if (contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.wordprocessingml.document")){
			return "docx";
		}else if (contentType.equalsIgnoreCase("application/vnd.ms-excel")){
			return "xls";
		}else if (contentType.equalsIgnoreCase("application/vnd.ms-powerpoint")){
			return "ppt";
		}else if (contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation")){
			return "pptx";
		}else if (contentType.equalsIgnoreCase("application/vnd.ms-excel.sheet.macroEnabled.12")){
			return "xlsm";
		}else {
			return "txt";
		}
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
}
