package com.pdinh.enterprise.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.sql.Timestamp;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.pdinh.data.ms.dao.FileAttachmentDao;
import com.pdinh.enterprise.rest.representation.FileAttachmentRepresentation;
import com.pdinh.enterprise.rest.representation.RepresentationFactory;
import com.pdinh.file.FileAttachmentServiceLocal;
import com.pdinh.persistence.ms.entity.FileAttachment;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Stateless
@Path("/fileAttachment")
public class FileAttachmentResource {

	@EJB
	private FileAttachmentServiceLocal fileAttachmentService;

	@EJB
	private FileAttachmentDao fileAttachmentDao;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Path("/upload")
	public Response createFileAttachment(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileInfo, @FormDataParam("userName") String userName,
			@Context UriInfo uriInfo) {

		FileAttachment attachment = new FileAttachment();
		attachment.setDateUploaded(new Timestamp(System.currentTimeMillis()));
		attachment.setName(Utils.getFileNameFromPath(fileInfo.getFileName()));
		attachment.setExtAdminId(userName);
		attachment.setPath(UUID.randomUUID().toString());

		try {
			fileAttachmentService.write(attachment, is);
		} catch (IOException e) {
			throw new WebApplicationException(e, Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to write file " + fileInfo.getName() + ", " + e.getMessage()).build());
		}

		fileAttachmentDao.create(attachment);

		String selfUri = uriInfo.getAbsolutePath() + "/" + attachment.getId();

		RepresentationFactory rf = new RepresentationFactory();
		rf.setBaseUri(uriInfo.getBaseUri().toASCIIString());

		FileAttachmentRepresentation rep = rf.createFileAttachmentRepresentation(attachment);

		return Response.created(URI.create(selfUri)).entity(rep).build();
	}

	public FileAttachmentServiceLocal getFileAttachmentService() {
		return fileAttachmentService;
	}

	public void setFileAttachmentService(FileAttachmentServiceLocal fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	public FileAttachmentDao getFileAttachmentDao() {
		return fileAttachmentDao;
	}

	public void setFileAttachmentDao(FileAttachmentDao fileAttachmentDao) {
		this.fileAttachmentDao = fileAttachmentDao;
	}
}
