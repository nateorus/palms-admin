package com.pdinh.enterprise.helper;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pdinh.data.jaxb.reporting.KFOnlineReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.reporting.GeneratedReportData;
import com.pdinh.rest.security.KFOAuthorizationClientFilter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

@Stateless
@LocalBean
public class KFOnlineReportApiHelperImpl implements
		KFOnlineReportApiHelperLocal {
	
	private static final Logger log = LoggerFactory.getLogger(KFOnlineReportApiHelperImpl.class);

	@EJB
	private ReportRequestDao reportRequestDao;
	
	@EJB
	private ReportingSingleton reportingSingleton;
	
	@EJB
	private KFORequestDispatcherLocal requestDispatcher;
	
	@EJB
	private KFOServiceHelper serviceHelper;

	
	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;

	@SuppressWarnings("unchecked")
	@Override
	public void updateStatusKFOnline(String reportRequestKey, String status,
			boolean retry) {
		Map<String, String> jsonResultMap = new HashMap<String, String>();
		//HttpURLConnection conn = null;
		Client client = serviceHelper.getKFOJerseyClient();

		try {
			String urlTarget = properties.getProperty("KFReportAPIBaseUrl");
			urlTarget = urlTarget + "/StatusUpdate";
			log.debug("Got urlTarget for Status Update service: {}", urlTarget);
			
			WebResource resource = client.resource(urlTarget);
			//conn = serviceHelper.getSecureConnection(urlTarget);
	
			KFOnlineReportRepresentation kfrptrep = new KFOnlineReportRepresentation();
			kfrptrep.setReportRequestKey(reportRequestKey);
			kfrptrep.setStatus(status);
			kfrptrep.setRetryCount(String.valueOf(retry));
			
			ObjectMapper mapper = serviceHelper.getObjectMapper();

	        String json = "";
	        try {
				
				json = mapper.writeValueAsString(kfrptrep);
				
				log.debug("Sending this json: {}", json);
			} catch (Exception e) {
				log.error("Caught Exception generating JSON string for status update: {}", e.getMessage());
				
			} 
	        
	        ClientResponse response = resource.type(MediaType.APPLICATION_JSON)
					//.header("MetaData", dumbjson)
                    .post(ClientResponse.class, json); 
			
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			//jsonResultMap = mapper.readValue(conn.getInputStream(), HashMap.class);
			jsonResultMap = mapper.readValue(response.getEntityInputStream(), HashMap.class);
			
			log.debug("Got jsonResultMap from input stream:  Size: {}", jsonResultMap.size());
			if (jsonResultMap.containsKey("Result")){
				log.debug("Got this result from UpdateStatus: {}", jsonResultMap.get("Result"));
				
			}else{
				log.debug("Unrecognized Response");
				for (String key : jsonResultMap.keySet()){
					log.debug("key: {}, value: {}", key, jsonResultMap.get(key));
				}
				
			}
		}catch(Exception e){
			log.error("Failed to update status.  Caught exception: {}", e.getMessage());
			
		}finally{
			//conn = null;
		}

	}
	
	private String makeMultiKeyString(List<ReportRepresentation> list){
		String keys = "";
		
		for (int index = 0; index < list.size(); index++){
			keys = keys + list.get(index).getReportRequestKey() + "|";
		}
		log.debug("Keys string: {}", keys);
		keys = keys.substring(0, keys.length() - 1);
		log.debug("keys string: {}", keys);
		return keys;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void uploadFile(String reportRequestKey, File file, String fileExtension) throws PalmsException {
		Map<String, String> jsonResultMap = new HashMap<String, String>();
		//HttpURLConnection conn = null;
		
		//Client client = Client.create();
		Client client = serviceHelper.getKFOJerseyClient();
		//TODO:  add kfo security properties to admin-rest.. remove from config?
		//client.addFilter(new KFOAuthorizationClientFilter(properties));
		
		try {
			String urlTarget = properties.getProperty("KFReportAPIBaseUrl");
			urlTarget = urlTarget + "/StoreinS3";
			log.debug("Got urlTarget for Queue Report service: {}", urlTarget);
			
			KFOnlineReportRepresentation kfrptrep = new KFOnlineReportRepresentation();
			kfrptrep.setReportRequestKey(reportRequestKey);

			ObjectMapper mapper = serviceHelper.getObjectMapper();

	       // String json = "";
	        String dumbjson = "{ \"ReportRequestKey\" : \"" + reportRequestKey + "\",\"FileExtension\" : \"" + fileExtension + "\", \"FileName\" : \"" + file.getName() + "\"}";
	        log.debug("Sending this json: {}", dumbjson);
	        

	        WebResource resource = client.resource(urlTarget);
	        log.debug("File length: {}", file.length());
	        
	        //Create the MultiPart Form
			FormDataMultiPart data = new FormDataMultiPart();
			
			//Add a field named fileName to hold the name of the file
			data.field("fileName", file.getName());
			data.field("MetaData", dumbjson);
			
			//Add the file data
		    FileDataBodyPart fdp = new FileDataBodyPart("content",
		           file,
		            MediaType.APPLICATION_OCTET_STREAM_TYPE);
		    
		    data.bodyPart(fdp);
		    
			log.debug("File name: {}", file.getName());
			
			
			ClientResponse response = resource.type(MediaType.MULTIPART_FORM_DATA)
					//.header("MetaData", dumbjson)
                    .post(ClientResponse.class, data); 

			log.debug("Response Type: {}, Response status: {}", response.getType(), response.getStatus());
			String result = response.getEntity(String.class);
			log.debug("Response Entity String: {}", result);
			//trim any data outside curly braces before sending through object mapper
			result = result.substring(result.indexOf("{"), result.lastIndexOf("}") + 1);
			log.debug("Trimmed Entity String: {}", result);
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			jsonResultMap = mapper.readValue(result, HashMap.class);
			
			log.debug("Got jsonResultMap from input stream:  Size: {}", jsonResultMap.size());
			if (jsonResultMap.containsKey("Result")){
				log.debug("Got this result from UpdateStatus: {}", jsonResultMap.get("Result"));
				
			}else{
				log.debug("Error Response");
				String detail = "";
				for (String key : jsonResultMap.keySet()){
					log.debug("key: {}, value: {}", key, jsonResultMap.get(key));
					if (key.equalsIgnoreCase("ErrorMessage")){
						detail = jsonResultMap.get(key);
					}
				}
				throw new Exception(detail);
				
			}
		}catch(Exception e){
			log.error("Failed to upload file.  Caught exception: {}", e.getMessage());
			throw new PalmsException(PalmsErrorCode.UPLOAD_FAILED.getMessage(), PalmsErrorCode.UPLOAD_FAILED.getCode(), e.getMessage());
			
		}finally{
			//conn = null;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public GeneratedReportData downloadFile(List<ReportRepresentation> reportReps)
			throws PalmsException {
		String urlTarget = properties.getProperty("KFReportAPIBaseUrl");
		urlTarget = urlTarget + "/ViewRpt";
		GeneratedReportData grd = new GeneratedReportData();
		
		//Client client = new Client();
		Client client = serviceHelper.getKFOJerseyClient();
		client.addFilter(new KFOAuthorizationClientFilter(properties));
		WebResource res = client.resource(urlTarget);
		ClientResponse resp = null;
		String keyString = this.makeMultiKeyString(reportReps);
		
		//String dumbjson = "{ \"ReportRequestKey\" : \"" + reportRep.getReportRequestKey() + "\" }";
		String dumbjson = "";
		if (reportReps.size() > 1){
			dumbjson = "{ \"ReportRequestKey\" : \"" + keyString + "\",\"Compress\" : \"true\" }";
		}else{
			dumbjson = "{ \"ReportRequestKey\" : \"" + keyString + "\",\"Compress\" : \"false\" }";
		}
		log.debug("Sending this json: {}", dumbjson);
		
	    try{
		    resp = res.type("application/json")
		    .entity(dumbjson)
		    .post(ClientResponse.class);
	    }catch(UniformInterfaceException e){
	    	log.error("Caught Exception from response: {}", e.getMessage());
	    	throw new PalmsException(PalmsErrorCode.BAD_REQUEST.getCode(), PalmsErrorCode.BAD_REQUEST.getMessage(), e.getMessage());
	    }
	    log.debug("Got Response Status: {}", resp.getStatus());
	    log.trace("Got Response type: {}", resp.getType().getType());
	    log.debug("Got Response SubType: {}", resp.getType().getSubtype());
	    
	    MultivaluedMap<String, String> headers = resp.getHeaders();
	    for (String key : headers.keySet()){
	    	if (key.equalsIgnoreCase("Content-Length")){
	    		grd.setContentLength(Integer.valueOf(headers.get(key).get(0)));
	    	}
	    	if (key.equalsIgnoreCase("Content-Disposition")){
	    		grd.setHdrStr(headers.get(key).get(0));
	    	}
	    	
	    	log.trace("Got header {} = {}", key, headers.get(key));
	    }
	    //TODO:  update when KFOnline returns appropriate error status
	    if (resp.getStatus() != 200 || resp.getType().getSubtype().contentEquals("json")){
	    	String resptext = resp.getEntity(String.class);
	    	log.debug("Got Response text: {}", resptext);
	    	//remove any bs outside curly braces
	    	resptext = resptext.substring(resptext.indexOf("{"), resptext.lastIndexOf("}") + 1);
	    	log.debug("Trimmed Response Text: {}", resptext);
	    	ObjectMapper mapper = serviceHelper.getObjectMapper();
	    	Map<String, String> jsonResultMap = new HashMap<String, String>();
	    	mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			try{
				jsonResultMap = mapper.readValue(resptext, HashMap.class);
			}catch(JsonParseException jpe){
				log.error("Caught json parse exception: {}", jpe.getMessage());
				jpe.printStackTrace();
			}catch(JsonMappingException jme){
				log.error("Caught json mapping exception: {}", jme);
				jme.printStackTrace();
			}catch(IOException ioe){
				log.error("Caught IOException reading response text: {}", ioe.getMessage());
			}
			
			log.debug("Got jsonResultMap from input stream:  Size: {}", jsonResultMap.size());
			String errorMessage = "Error Downloading from Repository";
			if (jsonResultMap.containsKey("ErrorMessage")){
				
				errorMessage = (jsonResultMap.get("ErrorMessage"));
			}
	    	
	    	throw new PalmsException(PalmsErrorCode.BAD_REQUEST.getCode(), PalmsErrorCode.BAD_REQUEST.getMessage(), errorMessage);
	    }
	    grd.setInStream(resp.getEntityInputStream());
	    
		return grd;
	}

	@Override
	public String kfoHealthCheck() {
		final String failedMessage = "failed";
		String urlTarget = properties.getProperty("KFReportAPIBaseUrl");
		urlTarget = urlTarget + "/isOnline";
		log.debug("Got urlTarget for KFO Health Check service: {}", urlTarget);
		Client client = serviceHelper.getKFOJerseyClient();
		WebResource resource = client.resource(urlTarget);
		
		String sResponse = "";
		ClientResponse response;
		try {  
	        
			response = resource.type(MediaType.APPLICATION_JSON)
					.header("Content-Length", "0")
                    .post(ClientResponse.class, ""); 
		}catch(Exception e){
			log.error("Caught Exception writing to output stream: {}", e.getMessage());
			return failedMessage;
		}
		
	    try {  
		   
	    	sResponse = IOUtils.toString(response.getEntityInputStream());
	    	
			log.debug("Got Response: {}",sResponse);

		} catch(IOException ioe){
			log.error("Caught IO Exception: {}", ioe.getMessage());
			ioe.printStackTrace();
			return failedMessage;
			
		} catch(Exception e){
			log.error("Caught exception: {}", e.getMessage());
			e.printStackTrace();
			return failedMessage;
			
		}finally{
			//conn = null;
		}
	   return sResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String uploadFile(ReportRequest request, File file, String fileName) throws PalmsException {
		Map<String, String> jsonResultMap = new HashMap<String, String>();
		
		Client client = serviceHelper.getKFOJerseyClient();
		
		try {
			String urlTarget = properties.getProperty("KFReportAPIBaseUrl");
			urlTarget = urlTarget + "/Upload";
			log.debug("Got urlTarget for Queue Report service: {}", urlTarget);
			
			ObjectMapper mapper = serviceHelper.getObjectMapper();
			String metadata = serviceHelper.makeMetadataString(request);
			String fileExtension = serviceHelper.getFileExtention(request.getContentType());
			
	        String dumbjson = "{ \"MetaData\" : \"" + metadata + "\",\"FileExtension\" : \"" + fileExtension + "\", \"FileName\" : \"" + fileName + "\" }";
	        log.debug("Sending this json: {}", dumbjson);

	        WebResource resource = client.resource(urlTarget);
	        
	        log.debug("File length: {}", file.length());

			FormDataMultiPart data = new FormDataMultiPart();
			
			data.field("fileName", fileName);
			data.field("MetaData", dumbjson);
			log.debug("Sending File with fileName: {}", fileName);
		    FileDataBodyPart fdp = new FileDataBodyPart("content",
		           file,
		            MediaType.APPLICATION_OCTET_STREAM_TYPE);
		    
		    data.bodyPart(fdp);
			log.debug("File name: {}", file.getName());
			
			ClientResponse response = resource.type(MediaType.MULTIPART_FORM_DATA)
					//.header("MetaData", dumbjson)
                    .post(ClientResponse.class, data); 

			log.debug("Response Type: {}, Response status: {}", response.getType(), response.getStatus());
			String result = response.getEntity(String.class);
			log.debug("Response Entity String: {}", result);
			result = result.substring(result.indexOf("{"), result.lastIndexOf("}") + 1);
			log.debug("Trimmed Entity String: {}", result);
			
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			
			jsonResultMap = mapper.readValue(result, HashMap.class);
			
			log.debug("Got jsonResultMap from input stream:  Size: {}", jsonResultMap.size());
			if (jsonResultMap.containsKey("ReportRequestKey")){
				log.debug("Got this result from UpdateStatus: {}", jsonResultMap.get("ReportRequestKey"));
				return jsonResultMap.get("ReportRequestKey");
				
			}else{
				log.debug("Error Response");
				String detail = "";
				for (String key : jsonResultMap.keySet()){
					log.debug("key: {}, value: {}", key, jsonResultMap.get(key));
					if (key.equalsIgnoreCase("ErrorMessage")){
						detail = jsonResultMap.get(key);
					}
				}
				throw new Exception(detail);
				
			}
		}catch(Exception e){
			log.error("Failed to upload file.  Caught exception: {}", e.getMessage());
			throw new PalmsException(PalmsErrorCode.UPLOAD_FAILED.getMessage(), PalmsErrorCode.UPLOAD_FAILED.getCode(), e.getMessage());
			
		}finally{
			//conn = null;
		}
	}
	
	

	@Asynchronous
	@Override
	public void dispatchMessages(List<ReportRequest> rptReqs) {
		
		for (ReportRequest requestEntity : rptReqs){

			requestDispatcher.queueReportRequest(requestEntity);
			if (reportingSingleton.getRequestQueueMap().remove(requestEntity.getReportRequestId()) == null){
				log.debug("Could not find request id in queue map. {}", requestEntity.getReportRequestId());
			}else{
				log.debug("Removed id from queue map: {}", requestEntity.getReportRequestId());
			}
		}
	}



}
