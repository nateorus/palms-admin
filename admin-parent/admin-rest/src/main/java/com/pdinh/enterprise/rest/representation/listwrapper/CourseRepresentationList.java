package com.pdinh.enterprise.rest.representation.listwrapper;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.enterprise.rest.representation.CourseRepresentation;

@XmlRootElement(name="courses")
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseRepresentationList {
	
	@XmlElement(name="course")
	private List<CourseRepresentation> list;

	public List<CourseRepresentation> getList() {
		return list;
	}

	public void setList(List<CourseRepresentation> list) {
		this.list = list;
	}
}
