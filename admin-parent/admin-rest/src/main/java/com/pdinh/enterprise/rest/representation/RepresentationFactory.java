package com.pdinh.enterprise.rest.representation;

import java.util.ArrayList;

import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.FileAttachment;
import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.UserDocument;
import com.pdinh.persistence.ms.entity.UserDocumentType;

public class RepresentationFactory {

	private String baseUri = "";

	public FileAttachmentRepresentation createFileAttachmentRepresentation(FileAttachment fileAttachment) {
		FileAttachmentRepresentation far = new FileAttachmentRepresentation();
		far.setId(fileAttachment.getId());
		far.setName(fileAttachment.getName());
		return far;
	}

	public static ReportRepresentation createReportRepresentation(ReportRequest rptReq) {
		ReportRepresentation rptRep = new ReportRepresentation();
		rptRep.setRequestId(rptReq.getReportRequestId());
		rptRep.setParentRequestId(rptReq.getReportGenerateRequest().getReportGenerateRequestId());
		rptRep.setReportRequestKey(rptReq.getReportRequestKey());
		rptRep.setFilesize(rptReq.getFilesize());
		rptRep.setProcessStartTime(rptReq.getProcessStartTime());
		rptRep.setCreateType(rptReq.getCreateType());
		rptRep.setStatus(rptReq.getReportRequestStatus().getCode());
		rptRep.setCode(rptReq.getReportType().getCode());
		rptRep.setReportIdNameOption(rptReq.getReportIdNameOption());
		rptRep.setReportLang(rptReq.getLanguage().getCode());
		rptRep.setReportName(rptReq.getReportType().getName());
		rptRep.setStatusTime(rptReq.getStatusUpdateTs());
		rptRep.setErrorMessage(rptReq.getErrorMessage());
		rptRep.setErrorCode(rptReq.getErrorCode());
		rptRep.setContentType(rptReq.getContentType());
		rptRep.setTitle("todo:  add title to entity");
		if (!(rptReq.getTargetLevelType() == null)) {
			rptRep.setTargetLevel(rptReq.getTargetLevelType().getCode());
		}
		rptRep.setRequestedBy(rptReq.getReportGenerateRequest().getPrincipal());
		rptRep.setSubjectId(rptReq.getReportGenerateRequest().getSubjectId());
		rptRep.setRequestTime(rptReq.getReportGenerateRequest().getRequestTime());
		rptRep.setParticipants(new ArrayList<ReportParticipantRepresentation>());
		for (ReportParticipant ppt : rptReq.getReportParticipants()) {
			ReportParticipantRepresentation rptPptRep = new ReportParticipantRepresentation();
			rptPptRep.setProjectId(ppt.getProject().getProjectId());
			rptPptRep.setProjectName(ppt.getProject().getName());
			if (!(ppt.getLanguage() == null)) {
				rptPptRep.setReportLang(ppt.getLanguage().getCode());
			}
			rptPptRep.setUsersId(ppt.getUser().getUsersId());
			rptPptRep.setFirstName(ppt.getUser().getFirstname());
			rptPptRep.setLastName(ppt.getUser().getLastname());
			rptPptRep.setEmail(ppt.getUser().getEmail());
			rptRep.getParticipants().add(rptPptRep);
		}
		return rptRep;
	}

	public UserDocumentRepresentation createUserDocumentRepresentation(UserDocument userDocument) {
		UserDocumentRepresentation udr = new UserDocumentRepresentation();

		Link selfLink = new Link("self", baseUri + UserDocumentRepresentation.RESOURCE_NAME + "/"
				+ userDocument.getDocumentId());
		udr.getLinks().add(selfLink);

		Link userLink = new Link("user", baseUri + UserRepresentation.RESOURCE_NAME + "/"
				+ userDocument.getUser().getUsersId());
		udr.getLinks().add(userLink);

		udr.setDocumentId(userDocument.getDocumentId());
		udr.setDescription(userDocument.getDescription());
		udr.setIncludeInIntegerationGrid(userDocument.getIncludeInIntegrationGrid());
		udr.setUserDocumentType(userDocument.getUserDocumentType());
		udr.setFileAttachmentRepresentation(createFileAttachmentRepresentation(userDocument.getFileAttachment()));
		return udr;
	}

	public UserDocumentTypeRepresentation createUserDocumentTypeRepresentation(UserDocumentType userDocumentType) {
		UserDocumentTypeRepresentation udtr = new UserDocumentTypeRepresentation();

		Link selfLink = new Link("self", baseUri + UserDocumentTypeRepresentation.RESOURCE_NAME + "/"
				+ userDocumentType.getUserDocumentTypeId());
		udtr.getLinks().add(selfLink);

		udtr.setUserDocumentTypeId(userDocumentType.getUserDocumentTypeId());
		udtr.setName(userDocumentType.getName());
		return udtr;
	}

	public CourseRepresentation createCourseRepresentation(Course course) {
		CourseRepresentation cr = new CourseRepresentation();

		Link selfLink = new Link("self", baseUri + CourseRepresentation.RESOURCE_NAME + "/" + course.getCourse());
		cr.getLinks().add(selfLink);

		cr.setCourse(course.getCourse());
		cr.setTitle(course.getTitle());
		cr.setAbbv(course.getAbbv());
		cr.setType(course.getType());
		cr.setHasTestData(course.getHasTestData());
		return cr;
	}

	public String getBaseUri() {
		return baseUri;
	}

	public void setBaseUri(String baseUri) {
		this.baseUri = baseUri;
	}

}
