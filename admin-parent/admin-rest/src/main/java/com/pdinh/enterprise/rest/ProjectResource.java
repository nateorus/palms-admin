package com.pdinh.enterprise.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.enterprise.rest.representation.CourseRepresentation;
import com.pdinh.enterprise.rest.representation.RepresentationFactory;
import com.pdinh.enterprise.rest.representation.listwrapper.CourseRepresentationList;
import com.pdinh.persistence.ms.entity.ProjectCourse;

@Stateless
@Path("/projects")
public class ProjectResource {

	@EJB
	private ProjectCourseDao projectCourseDao;

	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Path("{projectId}/courses")
	public CourseRepresentationList getCoursesByProjectId(@PathParam("projectId") int projectId, @Context UriInfo uriInfo) {
		
		List<ProjectCourse> projectCourses = projectCourseDao.findProjectCourseByProjectId(projectId);
				
		List<CourseRepresentation> courseRepresentations = new ArrayList<CourseRepresentation>();
						
		RepresentationFactory rf = new RepresentationFactory();
		rf.setBaseUri(uriInfo.getBaseUri().toASCIIString());
		
		for(ProjectCourse projectCourse : projectCourses) {
			if(projectCourse.getSelected()) {
				courseRepresentations.add(rf.createCourseRepresentation(projectCourse.getCourse()));
			}
		}
		
		CourseRepresentationList crl = new CourseRepresentationList();
		crl.setList(courseRepresentations);
		return crl;
	}	
		
	public ProjectCourseDao getProjectCourseDao() {
		return projectCourseDao;
	}

	public void setProjectCourseDao(ProjectCourseDao projectCourseDao) {
		this.projectCourseDao = projectCourseDao;
	}
	
}
