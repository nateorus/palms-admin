package com.pdinh.enterprise.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.TokenServiceLocal;
import com.pdinh.data.jaxb.admin.CreateTokenResponse;
import com.pdinh.data.jaxb.admin.SelfRegConfigRepresentation;
import com.pdinh.data.jaxb.admin.SelfRegListItemRepresentation;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.data.ms.dao.SelfRegConfigDao;
import com.pdinh.data.ms.dao.SelfRegListItemDao;
import com.pdinh.data.ms.dao.SelfRegListItemTypeDao;
import com.pdinh.enterprise.helper.ReportApiHelperLocal;
import com.pdinh.exception.PalmsException;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.ContentToken;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.persistence.ms.entity.SelfRegConfig;
import com.pdinh.persistence.ms.entity.SelfRegListItem;
import com.pdinh.persistence.ms.entity.SelfRegListItemType;
import com.pdinh.presentation.domain.EmailEventObj;

@Stateless
@Path("/selfReg")
public class SelfRegResource {

	private static final Logger log = LoggerFactory.getLogger(SelfRegResource.class);
	private static final Logger auditLog = LoggerFactory.getLogger("AUDIT-ADMIN");

	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;

	@EJB
	private SelfRegConfigDao selfRegConfigDao;

	@EJB
	private SelfRegListItemDao selfRegListItemDao;

	@EJB
	private SelfRegListItemTypeDao selfRegListItemTypeDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private ScheduledEmailDefDao emailDefDao;

	@EJB
	private ReportApiHelperLocal reportApiHelper;

	@EJB
	private TokenServiceLocal tokenService;

	@EJB
	private MailTemplatesServiceLocalImpl mailTemplatesService;

	private List<SelfRegListItemType> listItemTypes;

	private ScheduledEmailDef defaultEmailDef;

	private void initDefaultEmailDef() {
		this.setDefaultEmailDef(emailDefDao.findById(Integer.valueOf(properties.getProperty(
				"defaultRegistrationEmailId", "367"))));
	}

	private void initListItemTypes() {
		this.setListItemTypes(selfRegListItemTypeDao.findAll());
	}

	private SelfRegListItemType getSelfRegItemType(String typeCode) {
		if (this.listItemTypes == null) {
			initListItemTypes();
		}
		for (SelfRegListItemType t : this.listItemTypes) {
			if (t.getItemTypeCode().equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

	private ScheduledEmailDef getDefaultWelcomeEmailTemplate() {
		if (this.getDefaultEmailDef() == null) {
			initDefaultEmailDef();
		}
		return defaultEmailDef;

	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/getSelfRegConfig/{guid}")
	public SelfRegConfigRepresentation getSelfRegConfig(@PathParam(value = "guid") String guid) {
		log.debug("Searching for config by guid: {}", guid);
		SelfRegConfig config = selfRegConfigDao.findConfigByGuid(guid);
		// selfRegConfigDao.refresh(config);
		if (config == null) {
			throw new WebApplicationException();
		}
		String selfRegUrl = properties.getProperty("assessmentPortalHost", "http://mspreflexdev1:8080");
		selfRegUrl = selfRegUrl + "/portalweb/selfreg/" + guid;
		return new SelfRegConfigRepresentation(config, selfRegUrl);
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/getCreateSelfRegConfigByProjectId/{projectId}/{principal}")
	public SelfRegConfigRepresentation getCreateSelfRegConfigByProjectId(@PathParam(value = "projectId") int projectId,
			@PathParam(value = "principal") String principal) {
		List<SelfRegConfig> configs = selfRegConfigDao.findConfigByProjectId(projectId);
		SelfRegConfig config = null;
		if (configs == null || configs.size() < 1) {
			log.debug("No self registration configs found for project {}", projectId);
			Project project = projectDao.findById(projectId);
			config = new SelfRegConfig();
			config.setEnabled(false);
			config.setGuid(UUID.randomUUID().toString().toUpperCase());
			config.setProject(project);
			config.setRegistrationCount(0);
			config.setRegistrationLimit(-1);
			config.setScheduledEmailDef(this.getDefaultWelcomeEmailTemplate());
			config = selfRegConfigDao.create(config);
			auditLog.info("{} performed {} operation in company {}({}), Project: {}({}).", 
					new Object[]{principal, EntityAuditLogger.OP_CREATE_SELF_REG_CONFIG, 
						config.getProject().getCompany().getCoName(), config.getProject().getCompany().getCompanyId(),
						config.getProject().getProjectId(), config.getProject().getName()});
		} else {
			log.debug("Found {} self registration configs", configs.size());
			config = configs.get(0);
			selfRegConfigDao.refresh(config);
		}
		if (config == null) {
			log.error("Problem Creating Self Registration Configuration for project {}", projectId);
			throw new WebApplicationException();
		}

		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/getSelfRegConfigByProjectId/{projectId}")
	public SelfRegConfigRepresentation getSelfRegConfigByProjectId(@PathParam(value = "projectId") int projectId) {
		List<SelfRegConfig> configs = selfRegConfigDao.findConfigByProjectId(projectId);
		SelfRegConfig config = null;

		if (configs == null || configs.size() < 1) {
			log.debug("No self registration configs found for project {}", projectId);

			throw new WebApplicationException();

		} else {
			log.debug("Found {} self registration configs", configs.size());
			config = configs.get(0);
			selfRegConfigDao.refresh(config);
		}

		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/resetConfigUrl/{selfRegConfigId}")
	public SelfRegConfigRepresentation resetConfigUrl(@PathParam(value = "selfRegConfigId") int selfRegConfigId) {
		SelfRegConfig config = selfRegConfigDao.findById(selfRegConfigId);

		if (config == null) {
			throw new WebApplicationException();
		} else {

			config.setGuid(UUID.randomUUID().toString().toUpperCase());
			selfRegConfigDao.update(config);

		}
		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/makeLoginToken/{userId}/{companyId}")
	public CreateTokenResponse makeLoginToken(@PathParam(value = "userId") int userId,
			@PathParam(value = "companyId") int companyId) {
		CreateTokenResponse response = new CreateTokenResponse();
		ContentToken token = null;
		try {
			token = tokenService.generateToken(userId, companyId, "assessment");

		} catch (Exception e) {
			response.setSuccess(false);
			return response;
		}

		if (token == null) {
			response.setSuccess(false);
			return response;
		}

		response.setContentId("assessment");
		response.setId(token.getId());
		response.setSuccess(true);
		response.setTkn(token.getToken());

		// response.setValidMinutes(TokenServiceLocal.VERIFY_TOKEN_VALID_MINUTES);
		// this.sendVerificationEmail(token, email, null);
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/makeVerifyToken/{email}/{companyId}")
	public CreateTokenResponse makeVerifyToken(@PathParam(value = "email") String email,
			@PathParam(value = "companyId") int companyId) {
		CreateTokenResponse response = new CreateTokenResponse();
		ContentToken token = null;
		try {
			token = tokenService.generateToken(0, companyId, email);

		} catch (Exception e) {
			response.setSuccess(false);
			return response;
		}

		if (token == null) {
			response.setSuccess(false);
			return response;
		}

		response.setContentId(email);
		response.setId(token.getId());
		response.setSuccess(true);
		response.setTkn(token.getToken());

		response.setValidMinutes(TokenServiceLocal.VERIFY_TOKEN_VALID_MINUTES);
		this.sendVerificationEmail(token, email, null);
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/verifyUserToken/{tokenId}/{userInput}")
	public Response verifyUserToken(@PathParam(value = "tokenId") int tokenId,
			@PathParam(value = "userInput") String userInput) {

		if (tokenService.verifyToken(tokenId, Integer.valueOf(userInput.trim()))) {
			return Response.ok("<success/>").build();
		} else {
			return Response.serverError().build();
		}

	}

	private boolean sendVerificationEmail(ContentToken token, String email, String langCode) {
		List<ScheduledEmailDef> systemDefs = emailDefDao.findAllSystemTemplates();
		ScheduledEmailDef sendTokenDef = null;

		for (ScheduledEmailDef def : systemDefs) {
			if (def.getConstant().equals("TOKEN_SEND")) {
				sendTokenDef = def;
				break;
			}
		}
		if (sendTokenDef == null) {
			return false;
		} else {
			List<ScheduledEmailText> texts = sendTokenDef.getScheduledEmailTexts();
			int id = 0;
			if (langCode == null) {
				langCode = "en";
			}
			for (ScheduledEmailText text : texts) {
				if (text.getLanguage().getCode().equalsIgnoreCase(langCode)) {
					id = text.getId();
				}
			}
			EmailEventObj emailEvent = new EmailEventObj();
			emailEvent.setFromAddress("NoReply@KornFerry.com");
			emailEvent.setSenderEmail("NoReply@KornFerry.com");
			emailEvent.setRecipientEmailAddress(email);
			emailEvent.setTokenString(String.valueOf(token.getVerifyToken()));
			mailTemplatesService.sendTemplateInternal(id, emailEvent);
		}

		return false;
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/updateProjectSelfRegConfig/{selfRegConfigId}/{regLimit}/{enabled}/{principal}")
	public SelfRegConfigRepresentation updateProjectSelfRegConfig(
			@PathParam(value = "selfRegConfigId") int selfRegConfigId, @PathParam("regLimit") int regLimit,
			@PathParam("enabled") boolean enabled,
			@PathParam("principal") String principal) {
		SelfRegConfig config = selfRegConfigDao.findById(selfRegConfigId);

		if (config == null) {
			log.error("Invalid Self Reg Config Id: {}", selfRegConfigId);
			throw new WebApplicationException();
		}
		log.debug("Found Self Reg Config.  Updating enabled: {}, regLimit: {}", enabled, regLimit);

		config.setEnabled(enabled);
		config.setRegistrationLimit(regLimit);
		selfRegConfigDao.update(config);
		
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Enabled is {}, Reg Limit is {}.", 
				new Object[]{principal, EntityAuditLogger.OP_UPDATE_SELF_REG_CONFIG, 
					config.getProject().getCompany().getCoName(), config.getProject().getCompany().getCompanyId(),
					config.getProject().getProjectId(), config.getProject().getName(),
					enabled, regLimit});

		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/addAllowedDomains/{selfRegConfigId}/{principal}/{allowedDomain : .+}")
	public SelfRegConfigRepresentation addAllowedDomains(@PathParam(value = "selfRegConfigId") int selfRegConfigId,
			@PathParam("allowedDomain") List<PathSegment> allowedDomains,
			@PathParam("principal") String principal) {
		SelfRegConfig config = selfRegConfigDao.findById(selfRegConfigId);

		if (config == null) {
			log.error("Configuration Not found with Id: {}", selfRegConfigId);
			throw new WebApplicationException();
		}
		String addedDomains = "";

		for (PathSegment domain : allowedDomains) {
			SelfRegListItem item = new SelfRegListItem();
			item.setItemText(domain.getPath());
			item.setItemType(this.getSelfRegItemType(SelfRegListItemType.ALLOWED_DOMAIN_TYPE_CODE));
			item.setSelfRegConfig(config);
			item.setActive(true);
			item = selfRegListItemDao.create(item);
			config.getAllListItems().add(item);
			addedDomains = addedDomains + ", " + item.getItemText();
		}
		
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Added Domains: {}.", 
				new Object[]{principal, EntityAuditLogger.OP_UPDATE_SELF_REG_CONFIG, 
					config.getProject().getCompany().getCoName(), config.getProject().getCompany().getCompanyId(),
					config.getProject().getProjectId(), config.getProject().getName(),
					addedDomains});

		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/removeAllowedDomains/{selfRegConfigId}/{principal}/{allowedDomainId : .+}")
	public SelfRegConfigRepresentation removeAllowedDomains(@PathParam(value = "selfRegConfigId") int selfRegConfigId,
			@PathParam("allowedDomainId") List<PathSegment> allowedDomainIds,
			@PathParam("principal") String principal) {
		SelfRegConfig config = selfRegConfigDao.findById(selfRegConfigId);

		if (config == null) {
			log.error("Configuration Not found with Id: {}", selfRegConfigId);
			throw new WebApplicationException();
		}
		log.debug("Removing allowed domains from list with original: {}", config.getAllListItems().size());
		String removedDomains = "";
		for (PathSegment domain : allowedDomainIds) {
			
			for (SelfRegListItem item : config.getAllListItems()) {
				if (Integer.valueOf(domain.getPath()).compareTo(Integer.valueOf(item.getSelfRegListItemId())) == 0) {
					/*
					if (config.getAllListItems().remove(item)){
						log.debug("Successfully removed item: {}", item.getSelfRegListItemId());
					}else{
						log.debug("Could not remove item {} from list", item.getSelfRegListItemId());
					}
					selfRegListItemDao.delete(item);
					*/
					item.setActive(false);
					removedDomains = removedDomains + ", " + item.getItemText();
					break;
				}
			}
		}

		selfRegConfigDao.update(config);
		auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Removed Domains: {}.", 
				new Object[]{principal, EntityAuditLogger.OP_UPDATE_SELF_REG_CONFIG, 
					config.getProject().getCompany().getCoName(), config.getProject().getCompany().getCompanyId(),
					config.getProject().getProjectId(), config.getProject().getName(),
					removedDomains});
		log.debug("updated and refreshed config.  New size of list items: {}", config.getAllListItems().size());
		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/addRegisteredUser/{selfRegConfigId}/{userId}/{principal}")
	public SelfRegConfigRepresentation addRegisteredUser(@PathParam(value = "selfRegConfigId") int selfRegConfigId,
			@PathParam("userId") int userId,
			@PathParam("principal") String principal) {
		SelfRegConfig config = selfRegConfigDao.findById(selfRegConfigId);

		if (config == null) {
			throw new WebApplicationException();
		}
		boolean isDup = false;
		for (SelfRegListItem item : config.getAllListItems()){
			if (item.getItemType().getItemTypeCode().equals(SelfRegListItemType.REGISTERED_USER_ID_TYPE_CODE)){
				if (item.getItemText().equalsIgnoreCase(String.valueOf(userId))){
					isDup = true;
					log.info("User {} is attempting to register twice for project {}", userId, config.getProject().getProjectId());
					break;
				}
			}
		}
		if (!isDup){
			SelfRegListItem userItem = new SelfRegListItem();
			userItem.setItemText(String.valueOf(userId));
			userItem.setItemType(this.getSelfRegItemType(SelfRegListItemType.REGISTERED_USER_ID_TYPE_CODE));
			userItem.setSelfRegConfig(config);
			userItem.setActive(true);
			selfRegListItemDao.create(userItem);
			config.getAllListItems().add(userItem);
			// Enforce limit on client side
			if (config.getRegistrationCount() < 0) {
				config.setRegistrationCount(0);
			}
			config.setRegistrationCount(config.getRegistrationCount() + 1);
			config.setLastRegistration(new Timestamp(new Date().getTime()));
			selfRegConfigDao.update(config);
			// selfRegConfigDao.refresh(config);
			auditLog.info("{} performed {} operation in company {}({}), Project: {}({}). Registration Count is: {}. Limit is: {}", 
					new Object[]{principal, EntityAuditLogger.OP_SELF_REGISTER_PROJECT, 
						config.getProject().getCompany().getCoName(), config.getProject().getCompany().getCompanyId(),
						config.getProject().getProjectId(), config.getProject().getName(),
						config.getRegistrationCount(), config.getRegistrationLimit()});	
		}
		return new SelfRegConfigRepresentation(config, makeSelfRegUrl(config));
	}

	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Path("/updateSelfRegConfig")
	public Response updateSelfRegConfig(SelfRegConfigRepresentation config) {

		try {
			SelfRegConfig configEnt = this.updateConfig(config);
		} catch (PalmsException p) {
			throw new WebApplicationException();
		}

		return Response.ok().entity("<result>SUCCESS</result>").build();

	}

	private String makeSelfRegUrl(SelfRegConfig config) {
		String selfRegUrl = properties.getProperty("assessmentPortalHost", "http://mspreflexdev1:8080");
		selfRegUrl = selfRegUrl + "/portalweb/faces/registration.xhtml?c=" + config.getGuid();
		if (!selfRegUrl.startsWith("http")) {
			selfRegUrl = "http://" + selfRegUrl;
		}
		return selfRegUrl;
	}

	private SelfRegConfig updateConfig(SelfRegConfigRepresentation configRep) throws PalmsException {
		SelfRegConfig confEnt = selfRegConfigDao.findById(configRep.getSelfRegConfigId());
		if (confEnt == null) {
			throw new PalmsException("Invalid Self Registration Configuration: " + configRep.getSelfRegConfigId());
		}
		log.debug("Updating allowed domains");
		confEnt = this.updateItems(configRep.getAllowedEmailDomains(), confEnt,
				SelfRegListItemType.ALLOWED_DOMAIN_TYPE_CODE);
		confEnt = this.updateItems(configRep.getRegisteredUsers(), confEnt,
				SelfRegListItemType.REGISTERED_USER_ID_TYPE_CODE);
		confEnt.setEnabled(configRep.isEnabled());
		confEnt.setGuid(configRep.getGuid());
		if (!(configRep.getLastRegistrationDate() == null)) {
			confEnt.setLastRegistration(new Timestamp(configRep.getLastRegistrationDate().getTime()));
		}
		confEnt.setRegistrationCount(configRep.getRegistrationCount());
		confEnt.setRegistrationLimit(configRep.getRegistrationLimit());
		selfRegConfigDao.update(confEnt);
		selfRegConfigDao.refresh(confEnt);
		return confEnt;

	}

	private SelfRegConfig updateItems(List<SelfRegListItemRepresentation> updates, SelfRegConfig config, String typeCode) {
		for (SelfRegListItemRepresentation listItem : updates) {
			// SelfRegListItem item = null;
			log.debug("listItem id: {}", listItem.getId());
			if (listItem.getId() > 0) {
				log.debug("The config has {} list items", config.getAllListItems().size());
				for (SelfRegListItem item : config.getAllListItems()) {
					log.debug("item id: {}", item.getSelfRegListItemId());
					log.debug("Incoming item Id: {}", listItem.getId());
					if (listItem.getId() == item.getSelfRegListItemId()) {
						log.debug("Found matching list item: {}, value: {}", item.getSelfRegListItemId(),
								listItem.getValue());
						item.setItemText(listItem.getValue());
						if (!(listItem.getLang() == null)) {
							item.setLangCode(listItem.getLang());
						}
						break;
					}
				}

			} else {
				SelfRegListItem item = new SelfRegListItem();
				item.setItemText(listItem.getValue());
				item.setItemType(this.getSelfRegItemType(typeCode));
				item.setLangCode(listItem.getLang());
				item.setSelfRegConfig(config);
				// selfRegListItemDao.create(item);
				log.debug("Adding new list item: {}", listItem.getValue());
				config.getAllListItems().add(item);
			}
		}
		// find ones that were removed
		List<SelfRegListItem> removeItems = new ArrayList<SelfRegListItem>();
		for (SelfRegListItem li : config.getAllListItems()) {
			if (li.getItemType().getItemTypeCode().equals(typeCode)) {
				boolean found = false;
				for (SelfRegListItemRepresentation rep : updates) {
					if (rep.getId() == li.getSelfRegListItemId()) {
						found = true;
						break;
					}
				}
				if (!found) {
					// delete
					log.debug("Adding Item {} for removal", li.getItemText());
					removeItems.add(li);
				}
			}
		}
		for (SelfRegListItem removeItem : removeItems) {
			removeItem.setSelfRegConfig(null);

			if (config.getAllListItems().remove(removeItem)) {
				log.debug("Successfully removed item from list: {}", removeItem.getItemText());
				log.debug("List now has {} items", config.getAllListItems().size());
			} else {
				log.debug("Item was not found in the list: {}", removeItem.getItemText());
			}
			selfRegListItemDao.delete(removeItem);
		}

		return config;
	}

	public List<SelfRegListItemType> getListItemTypes() {
		return listItemTypes;
	}

	public void setListItemTypes(List<SelfRegListItemType> listItemTypes) {
		this.listItemTypes = listItemTypes;
	}

	public ScheduledEmailDef getDefaultEmailDef() {
		return defaultEmailDef;
	}

	public void setDefaultEmailDef(ScheduledEmailDef defaultEmailDef) {
		this.defaultEmailDef = defaultEmailDef;
	}
}
