package com.pdinh.enterprise.rest.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="user")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class UserRepresentation extends Representation {

	public static final String RESOURCE_NAME = "users";

	public UserRepresentation() {		
	}
	
}
