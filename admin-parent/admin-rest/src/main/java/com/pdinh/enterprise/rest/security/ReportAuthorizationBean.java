package com.pdinh.enterprise.rest.security;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.dao.RoleContextDao;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.data.jaxb.reporting.GenerateReportsRequest;
import com.pdinh.data.jaxb.reporting.ReportParticipantRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.ProjectUser;

@LocalBean
@Stateless
public class ReportAuthorizationBean {
	
	private static final Logger log = LoggerFactory.getLogger(ReportAuthorizationBean.class);

	@EJB
	private RoleContextDao roleContextDao;
	
	@EJB
	private EntityServiceLocal entityService;
	
	@EJB
	private GlobalRolesBean rolesBean;

	@EJB
	private ProjectUserDao projectUserDao;
	
	public GenerateReportsRequest authorizeReports(GenerateReportsRequest repReq, int subjectId){
		List<Integer> distinctProjectIds = new ArrayList<Integer>();
		//first get a list of distinct project ids in this request
		for (ReportParticipantRepresentation ppt : repReq.getParticipants()){
			if (!distinctProjectIds.contains(ppt.getProjectId())){
				distinctProjectIds.add(ppt.getProjectId());
			}
		}
		//now get all the project user entities associated with this request
		List<ProjectUser> pusers = new ArrayList<ProjectUser>();
		for (Integer pid : distinctProjectIds){
			List<Integer> userIds = new ArrayList<Integer>();
			for (ReportParticipantRepresentation ppt : repReq.getParticipants()){
				if (ppt.getProjectId() == pid){
					userIds.add(ppt.getUsersId());
				}
			}
			pusers.addAll(projectUserDao.findProjectUsersFiltered(pid, userIds));
		}
		//now get the RoleContext for this subject
		
		RoleContext context = this.getDefaultRoleContext(subjectId);
		
		List<Integer> entityRuleIds = new ArrayList<Integer>();
		entityRuleIds.add(rolesBean.getEntityRule(EntityRule.ALLOW_CHILDREN).getEntity_rule_id());
		List<Integer> authorizedProjectIds = entityService.getEntityIds(context.getRole_context_id(), 
				EntityType.PROJECT_TYPE, entityRuleIds, true);
		if (authorizedProjectIds.containsAll(distinctProjectIds)){
			//no need to authorize further
			log.debug("All projects in request are authorized by Allow Children rule");
			return repReq;
		}

		List<ProjectUser> disallowedPUsers = this.getUnauthorizedProjectUsers(pusers, context);

		if (disallowedPUsers.isEmpty()){
			//no need to go further.  return the original
			log.debug("All ProjectUser entities in request are authorized");
			return repReq;
		}
		
		//new remove any reports that contain disallowed users
		List<ReportRepresentation> filteredReports = this.getAuthorizedReports(repReq.getReportTypeList(), disallowedPUsers);
		
		repReq.setReportTypeList(filteredReports);
		return repReq;
		
	}
	
	public List<ReportRepresentation> authorizeReports(List<ReportRepresentation> reports, int subjectId){
		List<Integer> distinctProjectIds = new ArrayList<Integer>();
		List<ProjectUser> pusers = new ArrayList<ProjectUser>();
		List<Integer> userIds = new ArrayList<Integer>();
		RoleContext context = this.getDefaultRoleContext(subjectId);
		//first get a list of distinct project ids in this request
		for (ReportRepresentation repRep : reports){
			
			for (ReportParticipantRepresentation ppt : repRep.getParticipants()){
				if (!distinctProjectIds.contains(ppt.getProjectId())){
					distinctProjectIds.add(ppt.getProjectId());
				}
				userIds.add(ppt.getUsersId());
			}
			
		}

		//now get all the project user entities associated with this request
				
		for (Integer pid : distinctProjectIds){
			
			pusers.addAll(projectUserDao.findProjectUsersFiltered(pid, userIds));
		}
		
		List<ProjectUser> unauthorized = this.getUnauthorizedProjectUsers(pusers, context);
		
		reports = this.getAuthorizedReports(reports, unauthorized);
		
		return reports;
		
	}
	
	public boolean authorizeUserGroupReports(int subjectId, int userId){
		//get all projectUser entities
		List<ProjectUser> pusers = projectUserDao.findProjectUsersByUserId(userId);
		
		RoleContext defaultCtx = this.getDefaultRoleContext(subjectId);
		
		List<Integer> entityRuleIds = new ArrayList<Integer>();
		entityRuleIds.add(rolesBean.getEntityRule(EntityRule.ALLOW_CHILDREN).getEntity_rule_id());
		List<Integer> authorizedProjectIds = entityService.getEntityIds(defaultCtx.getRole_context_id(), EntityType.PROJECT_TYPE, entityRuleIds, true);
		for (ProjectUser puser : pusers){
			if (!authorizedProjectIds.contains(puser.getProject().getProjectId())){
				log.debug("User does not have allow children rule for all projects for which the user is a member.");
				return false;
			}
		}
		return true;
	}
	
	private RoleContext getDefaultRoleContext(int subjectId){
		List<RoleContext> contexts = roleContextDao.findRoleContextsBySubject(subjectId);
		
		for (RoleContext ctx : contexts){
			if (ctx.isIsdefault()){
				return ctx;
			}
		}
		return null;
	}
	
	private List<ProjectUser> getUnauthorizedProjectUsers(List<ProjectUser> pusers, RoleContext context){
		//now get the project user ids authorized by this context
		List<Integer> authorizedPUsers = entityService.getAuthorizedEntities(context, EntityType.PROJECT_USER_TYPE, true);
		//now remove the project users that are not authorized
		List<ProjectUser> disallowedPUsers = new ArrayList<ProjectUser>();
		for (ProjectUser puser : pusers){
			if (!authorizedPUsers.contains(puser.getRowId())){
				disallowedPUsers.add(puser);
			}
		}
		return disallowedPUsers;
	}
	
	private List<ReportRepresentation> getAuthorizedReports(List<ReportRepresentation> reports, List<ProjectUser> disallowedPUsers){
		List<ReportRepresentation> filteredReports = new ArrayList<ReportRepresentation>();
		for (ReportRepresentation rptRep : reports){
			boolean disallowed = false;
			for (ReportParticipantRepresentation ppt : rptRep.getParticipants()){
				
				for (ProjectUser puser : disallowedPUsers){
					if (puser.getUser().getUsersId() == ppt.getUsersId() && puser.getProject().getProjectId() == ppt.getProjectId()){
						disallowed = true;
						log.debug("Report {} is disallowed, because it contains unauthorized project_user entity {}", rptRep.getCode(), puser.getRowId());
						break;
					}
				}
				
			}
			if (!disallowed){
				filteredReports.add(rptRep);
			}
		}
		return filteredReports;
	}
}
