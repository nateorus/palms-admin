package com.pdinh.enterprise.rest.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="course")
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseRepresentation extends Representation {
	public static final String RESOURCE_NAME = "courses";
		
	@XmlAttribute
	private int course;
	
	@XmlElement
	private String title;
	
	@XmlElement
	private String abbv;	
	
	@XmlElement
	private int type;	
	
	@XmlElement
	private Boolean hasTestData;
	
	public CourseRepresentation() {
	}

	public int getCourse() {
		return course;
	}

	public void setCourse(int course) {
		this.course = course;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAbbv() {
		return abbv;
	}

	public void setAbbv(String abbv) {
		this.abbv = abbv;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Boolean getHasTestData() {
		return hasTestData;
	}

	public void setHasTestData(Boolean hasTestData) {
		this.hasTestData = hasTestData;
	}

	
}
