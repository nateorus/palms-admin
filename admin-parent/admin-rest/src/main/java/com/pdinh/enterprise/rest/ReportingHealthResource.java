package com.pdinh.enterprise.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.reporting.SystemHealthCheckResponse;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.enterprise.helper.KFOnlineReportApiHelperLocal;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.presentation.helper.URLContextConstants;
import com.pdinh.rest.security.AuthorizationClientFilter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Stateless
@Path("/health")
public class ReportingHealthResource {
	
	private static final Logger log = LoggerFactory.getLogger(ReportingHealthResource.class);

	@EJB
	private KFOnlineReportApiHelperLocal kfOnlineReportApiHelper;
	
	@EJB
	private ReportingSingleton reportingSingleton;
	
	@EJB
	private ReportRequestDao reportRequestDao;
	
	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;
	

	
	/**
	 * This method calls the KFO service endpoint “/isOnline” and returns the 
	 * text response received from that service.  The purpose of this method is to test 
	 * connectivity and troubleshoot configuration or security issues affecting communication 
	 * with the KFO services.  If there are no issues it will return the string "success".
	 * @return
	 */
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/queueTest")
	public String queueTest(){
		return kfOnlineReportApiHelper.kfoHealthCheck();
	}
	/**
	 * This method returns an xml response with status information about various 
	 * components of the Reporting sub-system.  This is used by the Report Poller 
	 * component to automatically shut down report polling and generation in the 
	 * event that part of the system goes down.  It is also used by the Palms Admin 
	 * UI to display the system status, and other configuration and runtime properties.
	 * @return
	 */
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("/ping")
	public SystemHealthCheckResponse ping(){
		String reportUrl = properties.getProperty("reportServerHost", "http://mspreflexdev1:8080");
		Date now = new Date();
		if (!reportUrl.startsWith("http")){
			reportUrl = "http://" + reportUrl;
		}
		reportUrl = reportUrl + URLContextConstants.REPORT_REST_CONTEXT + "/healthcheck/ping";
		log.debug("Got Report test url: {}",reportUrl);
		Client client = Client.create();
		WebResource resource = client.resource(reportUrl);
		SystemHealthCheckResponse health = new SystemHealthCheckResponse();
		if (!(reportingSingleton.getAdminServerStatus() == ReportingSingleton.SERVER_STATUS_OK)){
			reportingSingleton.setAdminServerStatus(ReportingSingleton.SERVER_STATUS_OK);
			reportingSingleton.setAdminServerStatusChangeTime(now);
		}
		health.setAdmin(ReportingSingleton.SERVER_STATUS_OK);
		health.setAdminServerStatusChangeTime(reportingSingleton.getAdminServerStatusChangeTime());
		try{
			String response = resource.accept(MediaType.TEXT_PLAIN).get(String.class);
			if (response.trim().equalsIgnoreCase(ReportingSingleton.SERVER_STATUS_OK)){
				log.debug("Returning successful result");
				//return Response.ok().build();
				health.setReportServer(ReportingSingleton.SERVER_STATUS_OK);
			}else{
				log.debug("Returning error result...  report server is down.");
				//return Response.serverError().build();
				health.setReportServer(ReportingSingleton.SERVER_STATUS_DOWN);
			}
		}catch(UniformInterfaceException e){
			log.error("Caught uniform resource exception: {}", e.getMessage());
			//return Response.serverError().build();
			health.setReportServer(ReportingSingleton.SERVER_STATUS_DOWN);
		}
		if (!(reportingSingleton.getReportServerStatus() == health.getReportServer())){
			reportingSingleton.setReportServerStatus(health.getReportServer());
			reportingSingleton.setReportServerStatusChangeTime(now);
		}
		health.setReportServerStatusChangeTime(reportingSingleton.getReportServerStatusChangeTime());
		//Do Oxcart check
		String oxcartUrl = properties.getProperty("oxcartHost", "http://mspreflexdev1:8080");
		if (!oxcartUrl.startsWith("http")){
			oxcartUrl = "http://" + oxcartUrl;
		}
		oxcartUrl = oxcartUrl + URLContextConstants.OXCART_WEB_CONTEXT + "/healthcheck.jsp";
		
		WebResource oxcart = client.resource(oxcartUrl);
		try{
			String response = oxcart.accept(MediaType.TEXT_PLAIN).get(String.class);
			log.debug("Got response from oxcart: {}", response);
			if (response.trim().startsWith("SUCCESS")){
				health.setOxcart(ReportingSingleton.SERVER_STATUS_OK);
			}else{
				log.debug("Setting Oxcart Down");
				health.setOxcart(ReportingSingleton.SERVER_STATUS_DOWN);
			}
		}catch(UniformInterfaceException e){
			log.error("Exception checking oxcart health: {}", e.getMessage());
			health.setOxcart(ReportingSingleton.SERVER_STATUS_DOWN);
		}
		
		if (!(reportingSingleton.getOxcartServerStatus() == health.getOxcart())){
			reportingSingleton.setOxcartServerStatus(health.getOxcart());
			reportingSingleton.setOxcartServerStatusChangeTime(now);
		}
		health.setOxcartServerStatusChangeTime(reportingSingleton.getOxcartServerStatusChangeTime());
		
		//Do tincan admin check
		String tincanAdminUrl = properties.getProperty("tincanAdminHost", "http://mspreflexdev1:8080");
		if (!tincanAdminUrl.startsWith("http")){
			tincanAdminUrl = "http://" + tincanAdminUrl;
		}
		tincanAdminUrl = tincanAdminUrl + URLContextConstants.TINCAN_ADMIN_REST_CONTEXT + "/healthcheck/ping";
		WebResource tincanadmin = client.resource(tincanAdminUrl);
		try{
			String response = tincanadmin.accept(MediaType.TEXT_PLAIN).get(String.class);
			log.debug("Got response from tincan admin: {}", response);
			if(response.trim().equalsIgnoreCase(ReportingSingleton.SERVER_STATUS_OK)){
				health.setTincan(ReportingSingleton.SERVER_STATUS_OK);
			}else{
				log.debug("Setting tincan admin DOWN");
				health.setTincan(ReportingSingleton.SERVER_STATUS_DOWN);
			}
		} catch(UniformInterfaceException e){
			log.error("Exception checking tincan admin health: {}", e.getMessage());
			health.setTincan(ReportingSingleton.SERVER_STATUS_DOWN);
		}
		
		if (!(reportingSingleton.getTincanServerStatus() == health.getTincan())){
			reportingSingleton.setTincanServerStatus(health.getTincan());
			reportingSingleton.setTincanServerStatusChangeTime(now);
		}
		health.setTincanServerStatusChangeTime(reportingSingleton.getTincanServerStatusChangeTime());
		
		//Do KFO Health Check
		String kfoStatus = kfOnlineReportApiHelper.kfoHealthCheck();
		if (kfoStatus.trim().equalsIgnoreCase("Success")){
			health.setKfoReportApi("OK");
		}else{
			health.setKfoReportApi("DOWN");
		}
		
		if (!(reportingSingleton.getKfoReportApiStatus() == health.getKfoReportApi())){
			reportingSingleton.setKfoReportApiStatus(health.getKfoReportApi());;
			reportingSingleton.setKfoReportApiStatusChangeTime(now);
		}
		health.setKfoReportApiStatusChangeTime(reportingSingleton.getKfoReportApiStatusChangeTime());
		
		return health;
	}
	
	/**
	 * This method looks up Report Requests with status QUEUEING_FAILED, REQUESTED, or PROCESSING in batches of 100.  
	 * For requests with status REQUESTED, it discards items whose age is not older than 20 minutes.  
	 * For requests with status PROCESSING, it sets error status for those items older than 2 hours.
	 * The remaining items are dispatched to the Queuing service to re-attempt the queuing operation.  
	 * This method is called by the Report Poller application in order to process requests that may 
	 * have failed to queue due to an outage.    The method will be called repeatedly until the 
	 * backlog of failed items is cleared, at which point this method will return “completed”.
	 * @return
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/requeueFailed")
	public Response requeueFailed(){
		//Do requeueing asynchronously to avoid timeout with large batches
		List<String> statuses = new ArrayList<String>();
		statuses.add(ReportRequestStatus.STATUS_QUEUEING_FAILED);
		statuses.add(ReportRequestStatus.STATUS_REQUESTED);
		statuses.add(ReportRequestStatus.STATUS_PROCESSING);
		List<ReportRequest> requests = reportRequestDao.getRequestsByStatus(statuses, 100, 0);
		List<ReportRequest> requestsModified = new ArrayList<ReportRequest>();
		List<ReportRequest> requestsProcessing = new ArrayList<ReportRequest>();
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MINUTE, -20);
		Calendar cal2 = new GregorianCalendar();
		cal2.add(Calendar.HOUR, -2);
		
		if (!(requests == null)){
			log.debug("Got {} requests for requeuing", requests.size());
			//dispatcher.updateStatusBatch(requests, ReportRequestStatus.STATUS_REQUESTED);
			for (ReportRequest rr : requests){
				if (rr.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_REQUESTED)){
					if (reportingSingleton.getRequestQueueMap().get(rr.getReportRequestId()) == null){
						//Check if Requested Report is older than 20 minutes
						if (rr.getStatusUpdateTs().before(cal.getTime())){
							requestsModified.add(rr);
							reportingSingleton.getRequestQueueMap().put(rr.getReportRequestId(), new Timestamp(new Date().getTime()));
						}
					}
				} else if (rr.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_PROCESSING)) {
					if (rr.getStatusUpdateTs().before(cal2.getTime())){
						//this request is older than 2 hours.  Update to error status
						requestsProcessing.add(rr);
					}
				}else{
					//Queueing Failed
					requestsModified.add(rr);
					reportingSingleton.getRequestQueueMap().put(rr.getReportRequestId(), new Timestamp(new Date().getTime()));
				}
			}
		}else{
			log.debug("Got zero results from queuing failed status");
		}
		if (requestsProcessing.size() > 0){
			log.debug("Found {} orphan reports in Processing status", requestsProcessing.size());
			this.handleOrphanReports(requestsProcessing);
		}
		if (requestsModified.size() > 0){
			log.debug("Dispatching {} failed requests to the queue.", requestsModified.size());
			kfOnlineReportApiHelper.dispatchMessages(requestsModified);
		}
		if (requests.size() < 100){
			return Response.ok("completed").build();
		}else{
			return Response.ok("continue").build();
		}
	}
	
	private void handleOrphanReports(List<ReportRequest> orphans){
		for (ReportRequest request : orphans){
			request.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			request.setErrorCode(PalmsErrorCode.REPORT_GEN_TIMEOUT.getCode());
			request.setErrorMessage(PalmsErrorCode.REPORT_GEN_TIMEOUT.getMessage());
			
			request.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(request);
			kfOnlineReportApiHelper.updateStatusKFOnline(request.getReportRequestKey(), "Error", false);
		}
		
		
	}

}
