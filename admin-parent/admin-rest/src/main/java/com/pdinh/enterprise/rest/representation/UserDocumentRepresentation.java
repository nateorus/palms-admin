package com.pdinh.enterprise.rest.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.persistence.ms.entity.UserDocumentType;

@XmlRootElement(name="userDocument")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDocumentRepresentation extends Representation {
	
	public static final String RESOURCE_NAME = "userDocuments";
		
	@XmlAttribute
	private int documentId;
	
	@XmlElement
	private String description;
	
	@XmlElement
	private boolean includeInIntegerationGrid;
	
	@XmlElement
	private UserDocumentType userDocumentType;
	
	
	@XmlElement(name="fileAttachment")
	private FileAttachmentRepresentation fileAttachmentRepresentation;
	
	public UserDocumentRepresentation() {		
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isIncludeInIntegerationGrid() {
		return includeInIntegerationGrid;
	}

	public void setIncludeInIntegerationGrid(boolean includeInIntegerationGrid) {
		this.includeInIntegerationGrid = includeInIntegerationGrid;
	}

	public UserDocumentType getUserDocumentType() {
		return userDocumentType;
	}

	public void setUserDocumentType(UserDocumentType userDocumentType) {
		this.userDocumentType = userDocumentType;
	}

	public FileAttachmentRepresentation getFileAttachmentRepresentation() {
		return fileAttachmentRepresentation;
	}

	public void setFileAttachmentRepresentation(
			FileAttachmentRepresentation fileAttachmentRepresentation) {
		this.fileAttachmentRepresentation = fileAttachmentRepresentation;
	}
	
}
