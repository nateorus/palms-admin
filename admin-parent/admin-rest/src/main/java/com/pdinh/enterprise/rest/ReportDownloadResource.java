package com.pdinh.enterprise.rest;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.IOUtils;
import com.pdinh.data.jaxb.JaxbUtil;
import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.data.jaxb.reporting.ReportRepresentationList;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.enterprise.helper.KFOnlineReportApiHelperLocal;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.audit.EntityAuditLogger;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.reporting.GeneratedReportData;
import com.pdinh.rest.security.APIAuthorizationServiceLocal;

@Stateless
@Path("/download")
public class ReportDownloadResource {
	private static final Logger log = LoggerFactory.getLogger(ReportDownloadResource.class);
	private static final Logger auditLog = LoggerFactory.getLogger("AUDIT-ADMIN");
	
	@Resource(name = "application/admin-rest/config_properties")
	private Properties properties;

	
	@EJB
	private ReportRequestDao reportRequestDao;
	
	@EJB
	private APIAuthorizationServiceLocal authorizationService;
	
	@EJB
	private KFOnlineReportApiHelperLocal kfReportApiHelper;
	
	@EJB
	private ReportingSingleton reportingSingleton;
	
	/**
	 * This method returns one or more reports from the report repository.  
	 * If there are multiple reports requested, a compressed zip file will 
	 * be returned containing all the requested reports.  Otherwise, the 
	 * report will be returned uncompressed.
	 * @param reportList
	 * @return a report stream
	 */
	
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/downloadReports")
	public Response downloadReports(ReportRepresentationList reportList){
		//Map<String, String> reportList = new HashMap<String, String>();
		List<ReportRepresentation> reportResources = reportList.getReports();
		if (reportResources == null){
			log.debug("ReportResources Is NULL");
		}else{
			log.debug("Got {} report resources for download", reportResources.size());
		}
		String contentType = "text/html";
		//InputStream reportStream = null;
		if (reportResources == null || reportResources.isEmpty()){
			ErrorRepresentation e = new ErrorRepresentation();
			e.setErrorCode(PalmsErrorCode.BAD_REQUEST.getCode());
			e.setErrorMessage(PalmsErrorCode.BAD_REQUEST.getMessage());
			e.setErrorDetail("Null or empty list of resources requested");
			return e.buildErrorResponse(new ArrayList<MediaType>());
		}
		
		GeneratedReportData grd = new GeneratedReportData();
		if (reportResources.size() == 1){
			ReportRepresentation resource = reportResources.get(0);
			contentType = resource.getContentType();
			
		}else if (reportResources.size() > 1){
			contentType="application/zip";
			
		}
		try {
			//reportStream = kfReportApiHelper.downloadFile(resource);
			grd = kfReportApiHelper.downloadFile(reportResources);
			log.debug("Content-Disposition: {}", grd.getHdrStr());
			
		} catch (PalmsException p) {
			
			ErrorRepresentation e = new ErrorRepresentation();
			e.setErrorCode(p.getErrorCode());
			e.setErrorMessage(p.getMessage());
			e.setErrorDetail(p.getErrorDetail());
			return e.buildErrorResponse(new ArrayList<MediaType>()); 
		}
		
		/* -- this was not necessary as it turns out.  return the input stream from the response from kfo
		final InputStream istream = grd.getInSream();
		StreamingOutput stream = new StreamingOutput(){
			@Override
			public void write(OutputStream os) throws IOException, WebApplicationException {
				os = new BufferedOutputStream(os, 4096);
				byte[] buffer = new byte[4096];
				int numbytes = 0;
				int counter = 0;
				while ((numbytes = istream.read(buffer)) != -1){

					os.write(buffer, 0, numbytes);
					counter++;
				}
				log.debug("Wrote {} times to output stream", counter);
				os.flush();
			}
		};
		*/
		auditLog.info("{} performed {} operation on project ({}){} with {} reports", new Object[]{reportList.getRequestedBy(), EntityAuditLogger.OP_DOWNLOAD_REPORT, 
				reportResources.get(0).getParticipants().get(0).getProjectId(), 
				reportResources.get(0).getParticipants().get(0).getProjectName(), 
				reportResources.size()});
		
		return Response
				.ok(grd.getInSream())
				.type(contentType)
				.header("Content-Disposition", grd.getHdrStr())
				.header("Content-Transfer-Encoding", "binary")
				.build();
		
		
	}
	/**
	 * This method triggers the report generation process for a previously created ReportRequest entity.  
	 * It interacts with the KFO services to provide status updates and expects the ReportRequest to 
	 * be in a particular state and ready for processing.  This is called internally by the Report Poller 
	 * application, and should not be used for any other purpose.   
	 * @param hh
	 * @param requestId
	 * @return One of:  DELETE, if the item should be permanently removed from the queue.  
	 * RETRY, if the item should be left in the queue.  
	 * UNHEALTHY, if the error returned by the reporting sub-system indicates that a 
	 * reporting resource was unavailable.  
	 * SUCCESS, if the report was generated and stored in the repository successfully
	 */
	
	@POST
	@Produces("text/plain")
	@Path("/processQueuedReport/{requestId}")
	public Response processQueuedReport(@Context HttpHeaders hh, @PathParam("requestId") int requestId){
		log.debug("In processQueuedReport with request id {}", requestId);
		ReportRequest request = reportRequestDao.getReportRequestBatchChildren(requestId);
		if (request == null){
			
			//Delete message
			return Response.ok().entity("DELETE").build();
		}
		log.debug("Got request with ID {}", request.getReportRequestId());
		if (request.getReportRequestKey() == null || request.getReportRequestKey().equals("null")){
			request.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			request.setErrorMessage("Report Request Key not found.  This request will be retried.  Do not resubmit.");
			request.setErrorCode(PalmsErrorCode.REQUEST_NOT_FOUND.getCode());
			request.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(request);
			
			//Leave the message in the queue for retry.  The queueing operation may not have committed
			//the update to the ReportRequest with the key yet.  shouldn't happen but did
			return Response.ok().entity("RETRY").build();
		}
		log.debug("Got request with key {}", request.getReportRequestKey());
		if (request.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_CANCELLED)){
			//return success.  message will be deleted. no further action required.
			log.debug("Request has been cancelled.  No report will be generated.");
			kfReportApiHelper.updateStatusKFOnline(request.getReportRequestKey(), "Cancelled", false);
			return Response.ok().entity("DELETE").build();
		}
		if (!(request.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_QUEUED) || 
				request.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_ERROR) || 
				request.getReportRequestStatus().getCode().equals(ReportRequestStatus.STATUS_PROCESSING))){
			log.error("Unexpected Request Status: {}.  This Request will not be processed.", request.getReportRequestStatus().getCode());
			return Response.ok().entity("DELETE").build();
		}else{
			log.debug("Request Status is: {}", request.getReportRequestStatus().getCode());
		}
		kfReportApiHelper.updateStatusKFOnline(request.getReportRequestKey(), "In Progress", false);
		String xml = "<ReportStreamRequest><reportRequestId>" + String.valueOf(requestId)
				+ "</reportRequestId><reportRequestKey>" + request.getReportRequestKey() + "</reportRequestKey></ReportStreamRequest>";
		HttpURLConnection conn = null;
		String fileExtension = "";
		if (!(request.getContentType() == null || request.getContentType().isEmpty())){
			fileExtension = this.getFileExtention(request.getContentType());
		}else{
			fileExtension = this.getFileExtention(request.getReportType().getContentType());
		}
		/* -- This causes status to be incorrect when there is an error in generating.. 
		 * -- Doing asynchronous, new transaction didn't help.  Maybe need separate rest call, or don't need this
		request.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_PROCESSING));
		request.setStatusUpdateTs(new Timestamp(new Date().getTime()));
		reportRequestDao.update(request);
		*/
		int maxGenTime = 120000;
		try {
			String urlTarget = properties.getProperty("myHostUrl") + "/generate/generateReportStream";
			log.debug("Got urlTarget: {}", urlTarget);
			URL currUrl = new URL(urlTarget);
			
			conn = (HttpURLConnection) currUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/xml;charset=UTF-8");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setDoOutput(true);
			
			//Report Generation Timeout settings
			
			try{
				if (request.getReportType().isGroupReport()){
					maxGenTime = Integer.valueOf(properties.getProperty("reportGenerationTimeoutGroupMillis", "240000"));
				}else{
					maxGenTime = Integer.valueOf(properties.getProperty("reportGenerationTimeoutIndividualMillis", "120000"));
				}
			}catch (NumberFormatException nfe){
				log.error("Bad property detected.  Check values of reportGenerationTimeoutGroupMillis, and reportGenerationTimeoutIndividualMillis.  Must be numeric value.");
				//bad property.. using 120000
			}
			log.trace("Using Timeout: {}", maxGenTime);
			conn.setReadTimeout(maxGenTime);
			
			authorizationService.authorizePALMSAPIRequest(conn);
			
			PrintWriter pw = new PrintWriter(conn.getOutputStream());
			pw.write(xml);
			pw.close();
			//new BufferedInputStream(conn.getInputStream(), 4096);
			
			log.debug("Got Input Stream");
			log.debug("Content type: {}", conn.getContentType());
			log.debug("Set Content Length {}", conn.getContentLength());
			log.debug("Set Content Disposition: {}", conn.getHeaderField("Content-Disposition"));
			
			String disposition = conn.getHeaderField("Content-Disposition");
			
			String fileName = request.getReportRequestKey();
			if (!(disposition == null)){
				String[] split = disposition.split("=");
				// If the file name has double-quotes (") around it,
				// suppress them
				fileName = split[1].replace('"', ' ').trim();
				fileName = fileName.replace('/', '_');
				fileName = fileName.replace('\\', '_');
			}else{
				
				fileName = fileName + fileExtension;
			}
			File tempDir = null;
			try {
				log.debug("System.getProperty(\"java.io.tmpdir\") = " + System.getProperty("java.io.tmpdir"));
				tempDir = File.createTempFile("rpt", "", new File(System.getProperty("java.io.tmpdir")));
			} catch (IOException e2) {
				log.error("EXCEPTION caught setting tmp directory: {}", e2.getMessage());
			}
			tempDir.delete();
			tempDir.mkdir();
			tempDir.deleteOnExit();
			
			FileOutputStream out = new FileOutputStream(tempDir + "/" + fileName);
			byte[] bytes = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = conn.getInputStream().read(bytes)) > 0) {
				out.write(bytes, 0, bytesRead);
			}
			out.flush();
			out.close();
			File currentFile = new File(tempDir + "/" + fileName);
			try{
				log.debug("Uploading file: {}, Size: {} bytes", currentFile.getName(), currentFile.length());
				
				kfReportApiHelper.uploadFile(request.getReportRequestKey(), currentFile, fileExtension);
				log.debug("Upload Completed");
				request.setFilesize(currentFile.length());
				request.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_STORED));
				request.setStatusUpdateTs(new Timestamp(new Date().getTime()));
				
				reportRequestDao.update(request);
			}catch(PalmsException p){
				kfReportApiHelper.updateStatusKFOnline(request.getReportRequestKey(), "Error", false);
				request.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
				request.setErrorMessage(p.getMessage());
				request.setErrorCode(p.getErrorCode());
				request.setStatusUpdateTs(new Timestamp(new Date().getTime()));
				reportRequestDao.update(request);
				log.error("Caught palms exception while uploading request {}.  Got this message from KFOnline: {}", request.getReportRequestKey(), p.getErrorDetail());
				//queue item will be retried
				return Response.ok("RETRY").build();
			}
			
			
			log.debug("Update Request entity status");
			
		}catch(SocketTimeoutException s){
			log.error("Exceeded maximum allowed time for generating report: {}", request.getReportRequestKey());
			request.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			request.setErrorMessage("The maximum allowed time for generating this report: " + maxGenTime/1000 + " seconds, was exceeded.");
			request.setErrorCode(PalmsErrorCode.REPORT_GEN_TIMEOUT.getCode());
			request.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(request);
			kfReportApiHelper.updateStatusKFOnline(request.getReportRequestKey(), "Error", false);
			//queue item will NOT be retried
			return Response.ok("DELETE").build();
			
		}catch(Exception e){
			//The Generate service will handle updating the request entity to error status in this case.
			//Just send back failure to polling app
			log.error("Got Error generating report: {}", e.getMessage());
			
			String sResponse = "";
			
		   try {
			   sResponse = IOUtils.toString(conn.getErrorStream());
			   
			} catch (IOException e1) {
				//error reading error stream...  delete from queue
				log.error("Error Reading Error Stream from response.  {}", e1.getMessage());
				return Response.ok("delete").build();
			}
		      
			log.debug("Got Response: {}",sResponse);
			ErrorRepresentation error = (ErrorRepresentation) JaxbUtil.unmarshal(sResponse.trim(), ErrorRepresentation.class);
			if (error == null){
				return Response.ok("DELETE").build();
			}else{
				if (error.getErrorCode().equalsIgnoreCase(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getCode())){
					
					return Response.ok("UNHEALTHY").build();
				}else{
					//Problem with report data or settings.. delete from queue
					return Response.ok("DELETE").build();
				}
			}
			
			
		}
		return Response.ok().entity("success").build();
			
	}
	
	
	private String getFileExtention(String contentType){
		if (contentType == null){
			return "";
		}
		if (contentType.equalsIgnoreCase("application/pdf")){
			return "pdf";
		}else if (contentType.equalsIgnoreCase("application/x-excel")){
			return "xls";
		}else if (contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
			return "xlsx";
		}else if (contentType.equalsIgnoreCase("application/msword")){
			return "doc";
		}else if (contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.wordprocessingml.document")){
			return "docx";
		}else if (contentType.equalsIgnoreCase("application/vnd.ms-excel")){
			return "xls";
		}else if (contentType.equalsIgnoreCase("application/vnd.ms-powerpoint")){
			return "ppt";
		}else if (contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation")){
			return "pptx";
		}else if (contentType.equalsIgnoreCase("application/vnd.ms-excel.sheet.macroEnabled.12")){
			return "xlsm";
		}else {
			return "txt";
		}
	}
	
	
}
