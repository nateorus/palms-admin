package com.pdinh.enterprise.helper;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Local;

import com.pdinh.data.jaxb.reporting.ReportRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.presentation.domain.ResultObj;
import com.pdinh.reporting.GeneratedReportData;

@Local
public interface KFOnlineReportApiHelperLocal {
	
	@Asynchronous
	public void updateStatusKFOnline(String reportRequestKey, String status, boolean retry);
	/*
	public ResultObj dispatchMessage(ReportRequest rptReq);
	*/
	public void uploadFile(String reportRequestKey, File file, String fileExtension) throws PalmsException;
	
	public String uploadFile(ReportRequest request, File file, String fileName) throws PalmsException;
	
	public GeneratedReportData downloadFile(List<ReportRepresentation> reportRep) throws PalmsException;
	
	public String kfoHealthCheck();
	
	public void dispatchMessages(List<ReportRequest> rptReqs);

}
