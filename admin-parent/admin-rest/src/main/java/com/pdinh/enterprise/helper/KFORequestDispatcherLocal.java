package com.pdinh.enterprise.helper;

import java.util.List;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.presentation.domain.ResultObj;

@Local
public interface KFORequestDispatcherLocal {

	public void queueReportRequest(ReportRequest rptReq);
	
	public ResultObj dispatchMessage(ReportRequest rptReq);
	
	public void updateStatusBatch(List<ReportRequest> requests, String statusCode);
}
