package com.pdinh.enterprise.rest;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.pdinh.data.ValidationImportServiceLocal;

@Stateless
@PermitAll
@Path("/bulkupload")
public class BulkUploadResource {

	public static final String SUCCESS = "SUCCESS";

	@EJB
	private ValidationImportServiceLocal validationImportService;

	@POST
	@Produces("text/plain")
	public String processRequest(@FormParam("jobid") String jobId, @FormParam("jobStatus") String jobStatus, @FormParam("callbackurl") String callbackUrl) {
		System.out.println("Received REST call with parameters: jobid '" + jobId + "', jobStatus '" + jobStatus + "', callbackurl '" + callbackUrl + "'");
		validationImportService.processFile(Integer.valueOf(jobId), jobStatus, callbackUrl);
		return SUCCESS;
	}
}
