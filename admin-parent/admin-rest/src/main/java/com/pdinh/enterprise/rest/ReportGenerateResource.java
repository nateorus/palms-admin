package com.pdinh.enterprise.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.data.jaxb.reporting.GenerateReportRequest;
import com.pdinh.data.ms.dao.CoachingPlanDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.ReportRequestDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.enterprise.helper.KFOnlineReportApiHelperLocal;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.CoachingPlan;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.ReportSourceType;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.CoachingPlanObj;
import com.pdinh.presentation.domain.CourseStatusObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ProjectObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.presentation.domain.TargetLevelTypeObj;
import com.pdinh.reporting.GeneratedReportData;
import com.pdinh.reporting.ReportingServiceLocal;

@Stateless
@Path("/generate")
public class ReportGenerateResource{
	
	@EJB
	private ReportRequestDao reportRequestDao;
	/*
	@EJB
	private ReportRequestStatusDao reportRequestStatusDao;
	*/
	@EJB
	private ReportingSingleton reportingSingleton;
	
	@EJB
	private ReportingServiceLocal reportingService;
	
	@EJB
	private ProjectUserDao projectUserDao;
	
	@EJB
	private UserDao userDao;
	
	@EJB
	private CoachingPlanDao coachingPlanDao;
	
	@EJB
	private KFOnlineReportApiHelperLocal kfOnlineReportApiHelper;
	/*
	@EJB
	private TargetLevelTypeDao targetLevelTypeDao;
	*/
	private static final Logger log = LoggerFactory.getLogger(ReportGenerateResource.class);
	
	
	/**
	 * This service generates a single report and returns Streaming output if successful.
	 * @param genRptReq
	 * @param request
	 * @return
	 */
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/generateReportStream")
	public Response generateReportStream(GenerateReportRequest genRptReq, @Context UriInfo uriInfo, @Context Request request, @Context HttpHeaders hh){
		
		int requestId = genRptReq.getReportRequestId();
		ReportRequest rptReq = null;
		if (requestId > 0){
			rptReq = reportRequestDao.getReportRequestBatchChildren(genRptReq.getReportRequestId());
		}else{
			if (!(genRptReq.getReportRequestKey() == null)){
				rptReq = reportRequestDao.getReportRequestByKey(genRptReq.getReportRequestKey());
			}
		}
		
		if (rptReq == null){
			log.error("Report Request Not found: {}", genRptReq.getReportRequestId());
			kfOnlineReportApiHelper.updateStatusKFOnline(genRptReq.getReportRequestKey(), "Error", false);
			ErrorRepresentation error = new ErrorRepresentation();
			error.setErrorCode(PalmsErrorCode.REQUEST_NOT_FOUND.getCode());
			error.setErrorMessage(PalmsErrorCode.REQUEST_NOT_FOUND.getMessage());
			error.setErrorDetail("The request to generate a report could not be fulfilled because the requested id does not exist.");
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}


		List<Map<String, String>> rptUrlMap = new ArrayList<Map<String, String>>();
		try {
			rptUrlMap = this.generateReportRequestUrlMaps(rptReq);
		}catch (PalmsException p){
			log.error("Caught PalmsException {} while generating urls for report request {}", p.getErrorCode(), rptReq.getReportRequestId());
			if (p.getErrorCode().equals(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getCode())){
				kfOnlineReportApiHelper.updateStatusKFOnline(genRptReq.getReportRequestKey(), "Error", true);
				//TODO:  Toggle off polling here
			}else{
				kfOnlineReportApiHelper.updateStatusKFOnline(genRptReq.getReportRequestKey(), "Error", false);
			}
			ErrorRepresentation error = new ErrorRepresentation(p);
			rptReq.setErrorMessage(error.getErrorMessage());
			rptReq.setErrorCode(error.getErrorCode());
			rptReq.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			rptReq.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(rptReq);
			log.debug("Updated report request to status ERROR.");
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}

		if (rptUrlMap == null || rptUrlMap.size() != 1){
			log.error("Failed to Generate Report Url Map for rpt {}", rptReq.getReportRequestId());
			kfOnlineReportApiHelper.updateStatusKFOnline(genRptReq.getReportRequestKey(), "Error", false);
			ErrorRepresentation error = new ErrorRepresentation();
			error.setErrorCode(PalmsErrorCode.CANNOT_GEN_URL.getCode());
			error.setErrorMessage(PalmsErrorCode.CANNOT_GEN_URL.getMessage());
			error.setErrorDetail("An error occurred while preparing the report request.  Insufficient data exists to generate this report.");
			rptReq.setErrorMessage(error.getErrorDetail());
			rptReq.setErrorCode(error.getErrorCode());
			rptReq.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			rptReq.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(rptReq);
			log.debug("Updated report request to status ERROR.");
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}
		//Read input stream directly from source server, write to output stream using a buffer
		//to reduce memory footprint
		//InputStream reportstream = null;
		GeneratedReportData grd = null;
		try {
			boolean isHybrid = false;
			if (rptReq.getReportType().getReportSourceType().getReportSourceTypeId() == (ReportSourceType.HYBRID)){
				isHybrid = true;
			}
			grd = reportingService.getReportStream(rptUrlMap.get(0), isHybrid);

		} catch (PalmsException e) {
			ErrorRepresentation error = new ErrorRepresentation(e);
			kfOnlineReportApiHelper.updateStatusKFOnline(genRptReq.getReportRequestKey(), "Error", false);
			rptReq.setErrorMessage(error.getErrorMessage());
			rptReq.setErrorCode(error.getErrorCode());
			rptReq.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_ERROR));
			rptReq.setStatusUpdateTs(new Timestamp(new Date().getTime()));
			reportRequestDao.update(rptReq);
			log.debug("Updated report request to status ERROR, with message: {}", error.getErrorMessage());
			return error.buildErrorResponse(hh.getAcceptableMediaTypes());
		}
		final InputStream istream = grd.getInSream();
		StreamingOutput stream = new StreamingOutput(){
			@Override
			public void write(OutputStream os) throws IOException, WebApplicationException {

				byte[] buffer = new byte[4096];
				int numbytes = 0;
				while ((numbytes = istream.read(buffer)) != -1){

					os.write(buffer, 0, numbytes);
				}

				os.flush();
			}
		};
		//TODO:  NW:  not sure we need this
		CacheControl cc = new CacheControl();
		cc.setNoTransform(true);
		cc.setMustRevalidate(false);
		cc.setNoCache(true);
		//What are the preconditions?  can we use this for validation?
		Response.ResponseBuilder responseBuilder = request.evaluatePreconditions();

		if (responseBuilder != null){
			//Is this an error condition?
			return responseBuilder.cacheControl(cc).build();
		}
		log.debug("About to update the request entity...");

		//This causes a problem when an error occurs downstream, not sure what status will
		//prevail after the transaction ends...Need to rethink status updates.
		/*
		rptReq.setReportRequestStatus(reportingSingleton.getReportRequestStatus(ReportRequestStatus.STATUS_GENERATED));
		rptReq.setStatusUpdateTs(new Timestamp(new Date().getTime()));
		if (!(genRptReq.getReportRequestKey() == null)){
			rptReq.setReportRequestKey(genRptReq.getReportRequestKey());
		}
		reportRequestDao.update(rptReq);
		*/
		log.debug("About to stream the response...Disposition: {}", grd.getHdrStr());
		
		return Response
				.ok(stream)
				//.cacheControl(cc)
				.type(rptReq.getReportType().getContentType())
				.header("Content-Disposition", grd.getHdrStr())
				.build();

	}
	
	private List<Map<String, String>> generateReportRequestUrlMaps(ReportRequest reportRequest) throws PalmsException {
		//Map of userids to additional report parameters like language and course version
		//to be added below
		Map<Integer, String[]> userIds = new HashMap<Integer, String[]>();
		//map list of userids to each project id
		Map<Integer, List<Integer>> projectUserMap = new HashMap<Integer,List<Integer>>();
		//Handle users in Multiple projects
		for (ReportParticipant rptPpt : reportRequest.getReportParticipants()){
			//userIds.add(rptPpt.getUser().getUsersId());
			String lang = "";
			String version = "";
			if (rptPpt.getLanguage() == null){
				if (reportRequest.getLanguage() == null){
					//userIds.put(rptPpt.getUser().getUsersId(), "en");
					lang = "en";
				}else{
					//userIds.put(rptPpt.getUser().getUsersId(), reportRequest.getLanguage().getCode());
					lang = reportRequest.getLanguage().getCode();
				}
			}else{
				log.debug("Mapping user id {} to lang code {}", rptPpt.getUser().getUsersId(), rptPpt.getLanguage().getCode());
				//userIds.put(rptPpt.getUser().getUsersId(), rptPpt.getLanguage().getCode());
				lang = rptPpt.getLanguage().getCode();
			}
			if (!(rptPpt.getCourseVersionMap() == null || rptPpt.getCourseVersionMap().isEmpty())){
				version = rptPpt.getCourseVersionMap();
			}
			userIds.put(rptPpt.getUser().getUsersId(), new String[]{lang,version});
			
			if (projectUserMap.containsKey(rptPpt.getProject().getProjectId())){
				projectUserMap.get(rptPpt.getProject().getProjectId()).add(rptPpt.getUser().getUsersId());
			}else{
				List<Integer> newList = new ArrayList<Integer>();
				newList.add(rptPpt.getUser().getUsersId());
				projectUserMap.put(rptPpt.getProject().getProjectId(), newList);
			}
		}
		
		ReportType rptType = reportRequest.getReportType();

		if (userIds.size() > 1 && !rptType.isGroupReport()){
			log.error("Multiple users supplied for non-group report.  User count: {}, Report type: {}", userIds.size(), rptType.getCode());

			throw new PalmsException("Multiple Users supplied for non-group report", PalmsErrorCode.BAD_REQUEST.getCode());

		}
		String projectId = "NA";
		Project project = null;
		if (projectUserMap.size() == 1){
			project = reportRequest.getReportParticipants().get(0).getProject();
			projectId = String.valueOf(project.getProjectId());
		}
		
		String targetLevel = "";
		if (!(reportRequest.getTargetLevelType() == null)){
			targetLevel = reportRequest.getTargetLevelType().getCode();
		}
		String reportIdNameOption = "";
		if (!(reportRequest.getReportIdNameOption() == null)){
			reportIdNameOption = reportRequest.getReportIdNameOption();
		}
		String lang = reportRequest.getLanguage().getCode();
		String reportTitle;
		if (reportRequest.getReportTitle() == null || reportRequest.getReportTitle().isEmpty()){
			reportTitle = rptType.getName();
		}else{
			reportTitle = reportRequest.getReportTitle();
		}

		List<ParticipantObj> participants = new ArrayList<ParticipantObj>();
		List<ProjectUser> pusers = new ArrayList<ProjectUser>();
		//in case there are users from multiple projects
		for (Integer pid : projectUserMap.keySet()){
			pusers.addAll(projectUserDao.findProjectUsersFiltered(pid, projectUserMap.get(pid)));

		}
		
		for (ProjectUser puser : pusers){
			log.trace("Setting up ppt with lang: {}", userIds.get(puser.getUser().getUsersId()));
			participants.add(this.setupParticipantObject(puser, targetLevel, userIds.get(puser.getUser().getUsersId())[0], 
					userIds.get(puser.getUser().getUsersId())[1]));
		}
		//Always only one Report type in reportTypeObjList here
		
		List<ReportTypeObj> reportTypeObjList = this.getReportTypeObjsList(rptType,
				project, lang, targetLevel, reportTitle, reportIdNameOption);
		
		try{
			return reportingService.generateReportRequestUrlMaps(participants, reportTypeObjList,
					projectId);
		}catch (PalmsException p){
			throw p;
		}

	}
	
	private List<ReportTypeObj> getReportTypeObjsList(ReportType reportType,
			Project project, String lang, String targetLevel, String title, String reportIdNameOption) {
		//Subject subject = SecurityUtils.getSubject();
		List<ReportTypeObj> reportTypeObjList = new ArrayList<ReportTypeObj>();
		
		ReportTypeObj reportTypeObj = new ReportTypeObj(reportType.getName(), reportType.getCode(), false,
				reportType.getReportLanguageVisible().booleanValue(), reportType.getParticipantLanguageVisible()
						.booleanValue());

		// fill in target levels
		if (reportType.getTargetLevelGroup() != null) {
			// Put in the project default entry for all EXCEPT those project
			// types that don't have a project default
			// TODO Data drive the presence/absence of the project default
			// selection
			//Project project = sessionBean.getProject();
			if (!(project == null)
					&& !project.getProjectType().getProjectTypeCode().equals(ProjectType.ASSESSMENT_TESTING)) {
				// Put in a project default.
				TargetLevelTypeObj tlto = new TargetLevelTypeObj(0, "Project Default", "");
				reportTypeObj.getTargetLevelTypes().add(tlto);
			}
			boolean tlOverride = false;
			if (!(targetLevel == null || targetLevel.isEmpty())){
				tlOverride = true;
			}
			for (TargetLevelGroupType targetLevelGroupType : reportType.getTargetLevelGroup()
					.getTargetLevelGroupTypes()) {
				TargetLevelType targetLevelType = targetLevelGroupType.getTargetLevelType();
				TargetLevelTypeObj tlto = new TargetLevelTypeObj(targetLevelType.getTargetLevelTypeId(),
						targetLevelType.getName(), targetLevelType.getCode());
				reportTypeObj.getTargetLevelTypes().add(tlto);
				//Only use target level override if the value is in the target level group
				if (tlOverride && targetLevel.equals(targetLevelType.getCode())){
					reportTypeObj.setTargetLevelOverride(targetLevel);
				}
			}
		}
		if (!reportIdNameOption.isEmpty()){
			reportTypeObj.setRptIdNameOpt(reportIdNameOption);
		}

		reportTypeObj.setRptTitle(title);

		reportTypeObj.setReportSourceTypeId(reportType.getReportSourceType().getReportSourceTypeId());

		
		reportTypeObj.setLanguageCode(lang);
		
		reportTypeObjList.add(reportTypeObj);


		return reportTypeObjList;
	}

	private ParticipantObj setupParticipantObject(ProjectUser puser, String targetLevel, String langCode, 
			String courseVersionMap) {

		Project project = puser.getProject();
		User user = puser.getUser();
		ProjectObj projectObj = new ProjectObj(project.getProjectId(), project.getName(), project.getProjectCode(),
				project.getProjectType().getName(), project.getProjectType().getProjectTypeId(),
				targetLevel, project.getExtAdminId(), project.getUsersCount(),
				project.getDateCreated(), project.getDueDate());
		Integer userId = user.getUsersId();

		String firstName = user.getFirstname();
		String lastName = user.getLastname();
		String email = user.getEmail();
		String username = user.getUsername();
		boolean coachFlag = user.getCoachFlag();
		Timestamp dateAdded = puser.getDateAdded();
		ParticipantObj participant = new ParticipantObj(userId, firstName, lastName, email, username, dateAdded,
				coachFlag);
		participant.setCourseVersions(new LinkedHashMap<String, String>());
		log.debug("Got course version map: {}", courseVersionMap);
		if (!courseVersionMap.isEmpty()){
			//TODO: this sucks.  fix me
			List<String> versions = new ArrayList<String>();
			if (courseVersionMap.contains("|")){
				versions = Arrays.asList(courseVersionMap.split("[|]"));
			}else{
				versions.add(courseVersionMap);
			}
			
			
			for (String version : versions){
				log.debug("Got version: {}", version);
				if (version.contains(":")){
					String[] map = version.split("[:]");
					participant.getCourseVersions().put(map[0], map[1]);
				}
			}
		}

		participant.setPassword(user.getPassword());
		participant.setOptional1(user.getOptional1());
		participant.setOptional2(user.getOptional2());
		participant.setOptional3(user.getOptional3());
		participant.setSupervisorName(user.getSupervisorName());
		participant.setSupervisorEmail(user.getSupervisorEmail());
		if (!(langCode == null || langCode.isEmpty())){
			
			participant.setReportLanguageId(langCode);
		}
		participant.setLanguageId(user.getLangPref());
		log.trace("Set user Report lang: {}", participant.getReportLanguageId());
		participant.setUserId(userId);
		participant.setRowKey(puser.getRowId());

		// NW: Now Consultants are added to view when you select the assignments
		// view.
		participant.setConsultants(new ArrayList<ExternalSubjectDataObj>());

		participant.setProject(projectObj);

		participant.setInitialEmailDate(userDao.resolveInitialEmail(user, project));
		participant.setReminderEmailDate(userDao.resolveReminderEmail(user, project));

		if (!project.getProjectType().getProjectTypeCode().equals(ProjectType.COACH)) {
			// JJB: Would be better if HashMap
			// Also should build from project_courses

			Map<String, CourseStatusObj> courseStatuses = new HashMap<String, CourseStatusObj>();
			for (ProjectCourse projectCourse : project.getProjectCourses()) {
				String courseAbbv = projectCourse.getCourse().getAbbv();
				courseStatuses.put(courseAbbv, getCourseStatusForInstrument(user.getPositions(), courseAbbv));
			}

			// create the ve_both course type
			CourseStatusObj cso = new CourseStatusObj("ve_both");
			// if there is a noDemo in here, selected (not 999), and not done
			// (not 3), allow viaEDGE to overwrite it (but only it if is done).
			if (courseStatuses.get("vedge_nodemo") != null && courseStatuses.get("vedge_nodemo").getStatus() != 999) {
				cso.setStatus(courseStatuses.get("vedge_nodemo").getStatus());
				// if there is a completed regular one, it can overide the
				// nodemo one for display
				if (courseStatuses.get("vedge_nodemo").getStatus() != 3) {
					CourseStatusObj possible = getCourseStatusForInstrument(user.getPositions(), "vedge");
					int stat = possible.getStatus();
					// Override status if vedge is Started or Complete
					if (possible.getStatus() == 2 || possible.getStatus() == 3) {
						cso.setStatus(stat);
					}
				}
			} else if (courseStatuses.get("vedge") != null) {
				// ve_both = regular ve
				cso.setStatus(courseStatuses.get("vedge").getStatus());
			} else {
				cso.setStatus(999);
			}
			courseStatuses.put("ve_both", cso);

			participant.setCourseStatuses(courseStatuses);
		} else {

			CoachingPlan cp = coachingPlanDao.findByProjectAndParticipant(project.getProjectId(), userId);
			CoachingPlanObj cpo = new CoachingPlanObj();
			if (cp != null) {
				cpo.setCoachFirstname(cp.getCoach().getFirstname());
				cpo.setCoachLastname(cp.getCoach().getLastname());
				cpo.setStartDate(cp.getStartDate());
				cpo.setEndDate(cp.getEndDate());
				cpo.setTimeElapsed(this.getDateDiffPercent(cp.getStartDate(), cp.getEndDate(), new Date()));
				log.debug("Coaching Time Elapsed: {} %", cpo.getTimeElapsed());
				cpo.setCoachingPlanStatusTypeId(cp.getCoachingPlanStatusType().getCoachingPlanStatusTypeId());
				//cpo.setCompletionStatusLabel(statusLabelsCoaching[cpo.getCoachingPlanStatusTypeId() + 1]);
				cpo.setTypeText(cp.getTypeText());
				cpo.setParticipantId(cp.getParticipant().getUsersId());
				try {
					cpo.setTargetLevelTypeId(cp.getTargetLevelType().getTargetLevelTypeId());

					cpo.setTargetLevelTypeLabel(reportingSingleton.getTargetLevelTypeById(cpo.getTargetLevelTypeId()).getCode());
				} catch (Exception e1) {
					cpo.setTargetLevelTypeId(-1);
					cpo.setTargetLevelTypeLabel("");
				}
			}
			participant.setCoachingPlanObj(cpo);

		}
		return participant;
	}
	
	private CourseStatusObj getCourseStatusForInstrument(List<Position> positions, String courseAbbv) {
		// System.out.println("IN ProjectManagementControllerBean.getCourseStatusForInstrument");
		Iterator<Position> iteratorPos = positions.iterator();
		while (iteratorPos.hasNext()) {
			Position position = iteratorPos.next();

			if (position.getCourse().getAbbv().equals(courseAbbv) && !position.getActivated()) {
				// System.out.println(position.getCourse().getAbbv() +
				// " not activated.");
				return new CourseStatusObj(position.getCourse().getAbbv(), 0);
			} else if (position.getCourse().getAbbv().equals(courseAbbv)) {
				// System.out.println(position.getUser().getEmail() + ";" +
				// position.getUser().getUsersId() + ";" +
				// position.getCourse().getAbbv() + ";" +
				// position.getCompletionStatus());
				return new CourseStatusObj(position.getCourse().getAbbv(), position.getCompletionStatus() + 1);
			}
		}

		return new CourseStatusObj(courseAbbv, 999);
	}
	

	@SuppressWarnings("deprecation")
	private static int getDateDiffPercent(Date startDate, Date endDate, Date currentDate) {
		Float pct = (float) 0.0;
		int pctInt;
		int elapsed = 0;
		int length = 0;

		GregorianCalendar g1, g2, g3, gc1, gc2, gc3, gce1;
		g1 = new GregorianCalendar(startDate.getYear(), startDate.getMonth(), startDate.getDate());
		g2 = new GregorianCalendar(endDate.getYear(), endDate.getMonth(), endDate.getDate());
		g3 = new GregorianCalendar(currentDate.getYear(), currentDate.getMonth(), currentDate.getDate());
		if (g2.after(g1)) {
			gc2 = (GregorianCalendar) g2.clone();
			gc1 = (GregorianCalendar) g1.clone();
		} else {
			gc2 = (GregorianCalendar) g1.clone();
			gc1 = (GregorianCalendar) g2.clone();
		}
		gc1.clear(Calendar.MILLISECOND);
		gc1.clear(Calendar.SECOND);
		gc1.clear(Calendar.MINUTE);
		gc1.clear(Calendar.HOUR_OF_DAY);
		gc2.clear(Calendar.MILLISECOND);
		gc2.clear(Calendar.SECOND);
		gc2.clear(Calendar.MINUTE);
		gc2.clear(Calendar.HOUR_OF_DAY);
		while (gc1.before(gc2)) {
			gc1.add(Calendar.DATE, 1);
			length++;
		}
		if (g3.after(g1)) {
			gc3 = (GregorianCalendar) g3.clone();
			gce1 = (GregorianCalendar) g1.clone();
		} else {
			gc3 = (GregorianCalendar) g1.clone();
			gce1 = (GregorianCalendar) g3.clone();
		}
		gc3.clear(Calendar.MILLISECOND);
		gc3.clear(Calendar.SECOND);
		gc3.clear(Calendar.MINUTE);
		gc3.clear(Calendar.HOUR_OF_DAY);
		while (gce1.before(gc3)) {
			gce1.add(Calendar.DATE, 1);
			elapsed++;
		}
		pct = (elapsed * 100.0f / length);
		pctInt = pct.intValue();
		if (pctInt > 100) {
			pctInt = 100;
		}
		if (pctInt < 0) {
			pctInt = 0;
		}

		return pctInt;
	}
}
