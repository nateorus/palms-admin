package com.pdinh.enterprise.rest.representation.listwrapper;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.enterprise.rest.representation.UserDocumentTypeRepresentation;

@XmlRootElement(name="userDocumentTypes")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDocumentTypeRepresentationList {

	@XmlElement(name="userDocumentType")
	private List<UserDocumentTypeRepresentation> list;

	public List<UserDocumentTypeRepresentation> getList() {
		return list;
	}

	public void setList(List<UserDocumentTypeRepresentation> list) {
		this.list = list;
	}
	
}
