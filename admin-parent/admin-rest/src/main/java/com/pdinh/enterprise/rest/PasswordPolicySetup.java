package com.pdinh.enterprise.rest;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.PasswordServiceLocalImpl;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;
import com.pdinh.persistence.ms.entity.User;

@Stateless
@PermitAll
@Path("/passwordpolicy")
public class PasswordPolicySetup {
	private static final Logger log = LoggerFactory.getLogger(PasswordPolicySetup.class);
	@EJB
	private PasswordServiceLocalImpl pwService;
	
	@EJB
	private CompanyDao companyDao;
	
	@EJB
	private UserDao userDao;
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	@Path("{companyId}/setupDefaultPolicies")
	public String setupDefaultPoliciesGet(@PathParam("companyId") int companyId){
		return setupDefaultPolicies(companyId);
	}
	
	@POST
	@Produces({MediaType.APPLICATION_XML})
	@Path("{companyId}/setupDefaultPolicies")
	public String setupDefaultPolicies(@PathParam("companyId") int companyId){
		
		Company company = companyDao.findById(companyId);
		
		if (company == null){
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to find Company with ID: " + companyId).build());
		}
		if (company.getPasswordPolicies().size() == 0){
		
			pwService.setupCompanyPolicies(company);
		}else{
			log.debug("Company {} already has password policies", company.getCoName());
			return "<result>false</result>";
		}
		
		return "<result>true</result>";
		
	}
	
	@POST
	@Produces({MediaType.APPLICATION_XML})
	@Path("{usersId}/setupDefaultUserPolicy")
	public String setupDefaultUserPolicy(@PathParam("usersId") int usersId){
		log.debug("Looking up user with id: {}", usersId);
		User user = userDao.findById(usersId);
		
		if (user == null){
			log.error("User is Null");
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to find User with ID: " + usersId).build());
		}
		if (user.getPasswordPolicyStatus() == null){
		
			PasswordPolicyUserStatus status = pwService.setupUserPolicyStatus(user);
			
			user.setPasswordPolicyStatus(status);
			userDao.update(user);
			log.debug("finished setting up password policy for user: {}", user.getUsername());
		}else{
			log.debug("User {} already has password policies", user.getUsername());
			return "<result>false</result>";
		}
		
		return "<result>true</result>";
		
	}

}
