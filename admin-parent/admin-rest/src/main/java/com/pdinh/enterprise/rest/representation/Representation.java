package com.pdinh.enterprise.rest.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * Base class for all representations sent back to REST clients, each 
 * object sent back can have several references to related resources, this approach
 * conforms to the Hypermedia as the Engine of Application State (HATEOAS) standard and allows 
 * clients to discover the next available actions (useful for workflows) and related 
 * resources so the entire object graph doesn't have to be serialized.
 */
public abstract class Representation {

	protected List<Link> links = new ArrayList<Link>();

	@XmlElement(name="link")
	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	
}
