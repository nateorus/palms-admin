package com.pdinh.enterprise.rest.filter;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.rest.security.APIAuthorizationServiceLocal;
import com.pdinh.rest.security.AwsSignature;

public class AuthorizationFilter implements Filter {
	
	//private String secretKey = "";
	//private List<String> partnerKeys = new ArrayList<String>();
	private Map<String, String> partnerKeyMap = new HashMap<String, String>();
	
	
	@Resource(mappedName = "application/admin-rest/config_properties")
	private Properties properties;

	
	private static final Logger log = LoggerFactory.getLogger(AuthorizationFilter.class);

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	//Authorize inbound requests to admin rest api

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		boolean headersInvalid = false;
		boolean debug = false;
		HttpServletRequest request = (HttpServletRequest) arg0;
		Subject subject = SecurityUtils.getSubject();
		if (!(request.getUserPrincipal() == null) && subject.isAuthenticated()){
			log.debug("Authenticated User.. bypass security key digest stuff");
			arg2.doFilter(arg0, arg1);
			return;
			//debug = true;
		}
		
		log.trace("In AuthorizationFilter do filter");
		
		
		HttpServletResponse response = (HttpServletResponse) arg1;
		String partnerKey = request.getHeader("PartnerKey");
		
		String token = request.getHeader("Token");
		String tokenType = request.getHeader("Token_Type");
		String nonce = request.getHeader("Nonce");
		String tstamp = request.getHeader("TimeStamp");
		String apiSignature = request.getHeader("API_Signature");
		String version = request.getHeader("Version");
		if (partnerKeyMap.containsKey(tokenType + version)){
			if (partnerKeyMap.get(tokenType + version).equals(partnerKey)){
				Date date = new Date(Long.valueOf(tstamp));
				Date now = new Date();
				int diffInMilliSecs = (int)((now.getTime() - date.getTime()));
				log.trace("Diff in millisecs: {}", diffInMilliSecs);
				if ((diffInMilliSecs / 1000) > 20){
					log.error("Headers invalid.  request is older than 5 seconds.");
					headersInvalid=true;
				}
			}else{
				log.error("Headers invalid.  partnerKey does not exist for {}{}", tokenType, version);
				headersInvalid = true;
			}
		}else{
			log.error("Headers invalid.  Map does not contain key: {}", tokenType + version);
			log.trace("Map size: {}", partnerKeyMap.size());
			for (String s : partnerKeyMap.keySet()){
				log.trace("Key: {}, value: {}", s, partnerKeyMap.get(s));
			}
			headersInvalid = true;
		}
		URI uri = null;
		if (headersInvalid){
			log.debug("Authorization failed.  Headers are invalid");
			response.setStatus(401);
		}else{
			try {
			
				uri = new URI(request.getRequestURL().toString());
				//uri = new URI(request.getProtocol(), request.getServerName() + request.getServerPort(), request.getContextPath(), "");
				
				log.trace("Got URI for host: {}, path: {}, scheme: {}", uri.getHost(), uri.getPath(), uri.getScheme());
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				log.error("Exception generating URI: {}", e1.getMessage());
				response.setStatus(500);
				return;
			}
			String generatedSignature = "";
			try {
				generatedSignature = AwsSignature.generateSignature(partnerKey, properties.getProperty("PALMSAPISecretKey"), tokenType, token, 
						uri, request.getMethod(), nonce, tstamp, version);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error("Exception generating signature: {}", e.getMessage());
				response.setStatus(401);
				return;
			}
			log.trace("Got generated signature: {}", generatedSignature);
			log.trace("Comparing to apiSignature header value: {}", apiSignature);
			if (generatedSignature.equals(apiSignature)){
				log.debug("Authorization succeeded");
				arg2.doFilter(arg0,arg1);
				return;
			}else{
				log.debug("Authorization failed");
				response.setStatus(401);
				return;
			}
				
			
		}
		//return;
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		//String delimitedPartnerStrings = arg0.getInitParameter("partnerKeys");
		String delimitedPartnerStrings = properties.getProperty("partnerKeys");
		
		if (!(delimitedPartnerStrings == null)){
			String[] partnerStringArray = delimitedPartnerStrings.split(",");
			log.debug("Partner String Array: {}", partnerStringArray);
			for (String s : partnerStringArray){
				String[] tokenKey = s.split("\\|");
				
				this.partnerKeyMap.put(tokenKey[0] + tokenKey[1], tokenKey[2]);
				log.debug("Added {} key with value: {}", tokenKey[0], partnerKeyMap.get(tokenKey[0] ));
				
			}
			
		}
		
	}
	

}
