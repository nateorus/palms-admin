package com.pdinh.enterprise.realm;

import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDINHCacheManager extends EhCacheManager {
	private static final Logger log = LoggerFactory.getLogger(PDINHCacheManager.class);
	public PDINHCacheManager(){
		
		log.debug("Cache config file:  {}", this.getCacheManagerConfigFile());
		setCacheManager(net.sf.ehcache.CacheManager.create());
	}
	

}
