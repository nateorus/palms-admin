package com.pdinh.enterprise.realm;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.activedirectory.ActiveDirectoryRealm;
import org.apache.shiro.realm.ldap.JndiLdapContextFactory;
import org.apache.shiro.realm.ldap.LdapContextFactory;
import org.apache.shiro.realm.ldap.LdapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDINHActiveDirectoryRealm extends ActiveDirectoryRealm {
	private static final Logger log = LoggerFactory.getLogger(PDINHActiveDirectoryRealm.class);
	public PDINHActiveDirectoryRealm(){
		super();
		
		/*
		JndiLdapContextFactory ldapContextFactory = new JndiLdapContextFactory();
		ldapContextFactory.setUrl("ldap://mspad1.ntdom1.personneldecisions.com:389");
		ldapContextFactory.setSystemPassword("ldapquery");
		ldapContextFactory.setSystemUsername("ldapRO");
		
		this.setLdapContextFactory(ldapContextFactory);
		*/
	}
	@Override
	protected AuthenticationInfo queryForAuthenticationInfo(AuthenticationToken token, LdapContextFactory ldapContextFactory) throws NamingException {

        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        upToken.setRememberMe(false);
        log.debug("About to get LDAP context for user: " + upToken.getUsername() + " Password: " + String.valueOf(upToken.getPassword()));
        
        // Binds using the username and password provided by the user.
        LdapContext ctx = null;
        try {
            //ctx = ldapContextFactory.(upToken.getUsername(), String.valueOf(upToken.getPassword()));
            ctx = ldapContextFactory.getLdapContext((Object)upToken.getUsername(), String.valueOf(upToken.getPassword()));
            log.debug("Name returned: " + ctx.getNameInNamespace());
        }catch (Exception e){
        	System.out.println(e.getMessage());
        }
        finally {
        	
            LdapUtils.closeContext(ctx);
        }

        return buildAuthenticationInfo(upToken.getUsername(), upToken.getPassword());
    }

}
