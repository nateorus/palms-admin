/**
 * 
 */
package com.pdinh.enterprise;

import java.util.Hashtable;

/**
 * @author nwondra
 *
 */
public interface PalmsLDAPInterface {
	
	public boolean authenticate(String username, String password);
	
	public int createUser(Hashtable<String, String> userdata);
	
	public int createCompany(Hashtable<String, String> clientdata);
	
	public boolean modifyAttribute(String attrname, String attrvalue, Object obj);
	
	public boolean addUserRole(String uniqueUserId, String rolename);
	
	public Hashtable<String, String> getUserData(String uniqueUserId);
	
	public Hashtable<String, String> getClientrData(String uniqueClientId);

}
