package com.pdinh.enterprise;
import java.util.Hashtable;
/*
import java.util.ArrayList;

import java.util.List;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.pdinh.ldap.persistence.entity.LdapGroup;
import com.pdinh.ldap.persistence.entity.LdapUser;
*/
public class PalmsOpenDSClientUtil implements PalmsLDAPInterface {

	@Override
	public boolean authenticate(String username, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int createUser(Hashtable<String, String> userdata) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int createCompany(Hashtable<String, String> clientdata) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean modifyAttribute(String attrname, String attrvalue, Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addUserRole(String uniqueUserId, String rolename) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Hashtable<String, String> getUserData(String uniqueUserId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, String> getClientrData(String uniqueClientId) {
		// TODO Auto-generated method stub
		return null;
	}
	/*
	private static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String providerUrlRoot = "ldap://localhost:389/";
	private static String providerUrlDirContext = "dc=example,dc=com";
	private static String securityAuthMethod = "simple";
	private static String securityPrinciple = "cn=Directory Manager";
	private static String securityPrinciplePw = "area86";
	
	public static String emailAttribute = "mail";
	public static String mobilePhoneAttribute = "mobile";
	public static String descriptionAttribute = "description";
	public static String homePhoneAttribute = "homePhone";
	public static String entryDNAttribute = "entryDN";
	public static String givenNameAttribute = "givenName";
	public static String commonNameAttribute = "cn";
	public static String objectClassAttribute = "objectClass";
	public static String addressAttribute = "postalAddress";
	public static String postalCodeAttribute = "postalCode";
	public static String stateAttribute = "st";
	public static String surnameAttribute = "sn";
	public static String groupMembershipAttribute = "isMemberOf";
	public static String useridAttribute = "uid";
	public static String passwordAttribute = "userPassword";
	
	public PalmsOpenDSClientUtil(){
		
	}

	@Override
	public boolean authenticate(String username, String password) {
		DirContext usercontext = null;
    	Hashtable<String, String> env = new Hashtable<String, String>();
    	env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
    	env.put(Context.PROVIDER_URL, providerUrlRoot + username + "," + providerUrlDirContext);
    	env.put(Context.SECURITY_AUTHENTICATION, securityAuthMethod);
    	env.put(Context.SECURITY_PRINCIPAL, username + "," + providerUrlDirContext);
    	env.put(Context.SECURITY_CREDENTIALS, password);
    	try{
    		usercontext = new InitialDirContext(env);
    		usercontext.close();
    		return true;
    	}catch(AuthenticationException a){
    		a.printStackTrace();
    	}catch(NamingException e){
    		e.printStackTrace();
    	}finally{
    		
    	}
    	
		return false;
	}

	@Override
	public int createUser(Hashtable<String, String> userdata) {
		// TODO Auto-generated method stub
		return 0;
	}
	public boolean createUser(LdapUser user, String parentOrg){
		DirContext admctx = getAdminContext();
		try {
			DirContext orgctx = (DirContext)admctx.lookup(parentOrg);
			System.out.println("Got Orgctx: " + orgctx.getNameInNamespace());
			BasicAttributes userAttrs = new BasicAttributes(commonNameAttribute, user.getAttributeValue("cn"));
			BasicAttribute oclass = new BasicAttribute(objectClassAttribute, "inetOrgPerson");
			BasicAttribute givenName = new BasicAttribute(givenNameAttribute, user.getAttributeValue("givenName"));
			BasicAttribute surName = new BasicAttribute(surnameAttribute, user.getAttributeValue("cn"));
			BasicAttribute email = new BasicAttribute(emailAttribute, user.getAttributeValue("mail"));
			BasicAttribute description = new BasicAttribute(descriptionAttribute, user.getAttributeValue("description"));
			BasicAttribute uid = new BasicAttribute(useridAttribute, user.getAttributeValue("uid"));
			BasicAttribute pword = new BasicAttribute(passwordAttribute, "area86");
			userAttrs.put(oclass);
			userAttrs.put(givenName);
			userAttrs.put(surName);
			userAttrs.put(email);
			userAttrs.put(description);
			userAttrs.put(uid);
			userAttrs.put(pword);
			orgctx.createSubcontext(commonNameAttribute + "=" + user.getAttributeValue("cn"), userAttrs);
			System.out.println("Successfully Created new user context");
			orgctx.close();
			admctx.close();
		} catch (NamingException e) {
			System.out.println("Exception Occurred while creating user: " + e.getExplanation());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public int createCompany(Hashtable<String, String> clientdata) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean modifyAttribute(String attrname, String attrvalue, Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addUserRole(String uniqueId, String rolename) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @param args
	 */
	/*
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public List<LdapAttribute> getAttrList(String name){
		String[] nameValue = new String[2];
		List<LdapAttribute> allattrs = new ArrayList<LdapAttribute>();
		LdapAttribute ldapattr;
		
		DirContext adminContext = getAdminContext();
		try {
			DirContext userContext = (DirContext)adminContext.lookup(name);
			Attributes attrs = userContext.getAttributes("");
			NamingEnumeration attrenum = attrs.getAll();
			while (attrenum.hasMore()){
				Attribute attr = (Attribute) attrenum.next();
				ldapattr = new LdapAttribute(attr.getID());
				System.out.println("=============================New Attribute===========================");
				System.out.println("Attribute Name: " + attr.getID());
				
				
				DirContext attrDef = attr.getAttributeDefinition();
				Attributes defattrs = attrDef.getAttributes("");
				NamingEnumeration defenum = defattrs.getAll();
				while (defenum.hasMore()){
					Attribute defattr = (Attribute) defenum.next();
					System.out.println("Attribute Definition attribute: " + defattr.getID());
					if (defattr.getID().equals("SYNTAX")){
						try{
							DirContext attrSyntaxDef = attr.getAttributeSyntaxDefinition();
							Attributes defsyntaxattrs = attrSyntaxDef.getAttributes("");
							NamingEnumeration defsyntaxenum = defsyntaxattrs.getAll();
							while (defsyntaxenum.hasMore()){
								Attribute defsyntaxattr = (Attribute) defsyntaxenum.next();
								System.out.println("Attribute Syntax Definition attribute: " + defsyntaxattr.getID());
								NamingEnumeration defsyntaxattrvals = defsyntaxattr.getAll();
								while (defsyntaxattrvals.hasMore()){
									System.out.println(defsyntaxattr.getID() + "value:" + defsyntaxattrvals.next());
								}
							}
							attrSyntaxDef.close();
						}catch(NameNotFoundException n){
							System.out.println("could not find syntax definition");
						}
						
						
						
						
					}
					NamingEnumeration defattrvals = defattr.getAll();
					while (defattrvals.hasMore()){
						String defattrval = defattrvals.next().toString();
						System.out.println(defattr.getID() + " value:" + defattrval);
						if (defattr.getID().equals("NAME")){
							ldapattr.setDefName(defattrval);
						}else if (defattr.getID().equals("EQUALITY")){
							ldapattr.setDefEquality(defattrval);
						}else if (defattr.getID().equals("SUBSTR")){
							ldapattr.setDefSubstr(defattrval);
						}else if (defattr.getID().equals("X-ORIGIN")){
							ldapattr.setDefxOrigin(defattrval);
						}else if (defattr.getID().equals("NUMERICOID")){
							ldapattr.setDefNumberidOid(defattrval);
						}
							
					}
				}
				
				
				NamingEnumeration attrvals = attr.getAll();
				System.out.println("==========================Attribute Values=========================");
				while (attrvals.hasMore()){
					String attrval = attrvals.next().toString();
					LdapAttribute l = new LdapAttribute(ldapattr.getAttrName());
					l.setAttrVal(attrval);
					l.setDefEquality(ldapattr.getDefEquality());
					l.setDefName(ldapattr.getDefName());
					l.setDefNumberidOid(ldapattr.getDefNumberidOid());
					l.setDefSubstr(ldapattr.getDefSubstr());
					l.setDefxOrigin(ldapattr.getDefxOrigin());
					
					allattrs.add(l);
					System.out.println("Attribute Value: " + attrval);
				}
				
				attrDef.close();
			}
			userContext.close();
			adminContext.close();
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return allattrs;
	}
	
	public DirContext getAdminContext(){


        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://localhost:389/dc=example,dc=com");
        //env.put("com.sun.jndi.ldap.connect.pool", "true");
        //env.put("com.sun.jndi.ldap.connect.timeout", "5000");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "cn=Directory Manager");
        env.put(Context.SECURITY_CREDENTIALS, "area86");
        
        DirContext ctx = null;
        try{
	        ctx = new InitialDirContext(env);
	        
	        Attributes attrs = ctx.getAttributes("");
	        NamingEnumeration<? extends Attribute> e = attrs.getAll();
	        while(e.hasMore()) {
	            System.out.println(e.next());
	        }
	        return ctx;
        }catch (NamingException ne){
        	ne.printStackTrace();
        	System.out.println("Problem connecting to directory: " + ne.getExplanation());
        	return ctx;
        }finally{
        	
        }


    }
	
	public List<LdapGroup> getUserGroups(String userName){
		List<LdapGroup> groups = new ArrayList<LdapGroup>();
		LdapGroup agroup;
		DirContext admctx = getAdminContext();
		
		try{
			String filter = "(&(objectClass=groupOfUniqueNames)(uniqueMember=" + userName + "*))";
			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			System.out.println("Searching for User Groups: " + userName);
			NamingEnumeration results = admctx.search("", filter, sc);
			while (results.hasMore()){
				SearchResult result = (SearchResult)results.next();
				System.out.println("Found User Group: " + result.getName());
				agroup = (LdapGroup)admctx.lookup(result.getName());
				
				groups.add(agroup);
			}
			
		}catch (NamingException n){
			System.out.println("Problem getting groups for user: " + userName);
		}
		return groups;
	}
	public List<String> getAdminPrivsForGroup(String groupname){
		List<String> orgNames = new ArrayList<String>();
		String parent = groupname.substring(groupname.indexOf(",") + 1);
		System.out.println("Parent Org: " + parent);
		DirContext ctx = getAdminContext();
		orgNames = getChildOrgs(parent, ctx);
		for (String o : orgNames){
			System.out.println("Org: " + o);
		}
		try {
			ctx.close();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orgNames;
	}
	/*
	 * This method returns a list of LDAP names of all Organizations and SubOrganizations that are children
	 * of the Context name passed in.
	 */
	/*
	public List<String> getChildOrgs(String name, DirContext admnctx){
		List<String> orgNames = new ArrayList<String>();
		//admnctx = getAdminContext();
		try {
			NamingEnumeration list = admnctx.list(name);
			while(list.hasMore()){
				NameClassPair next = (NameClassPair)list.next();
				//This seems like a lame way to identify the object classes i'm looking for
				//But getting objectClass from attributes would be more cumbersome.  Maybe there is more elegant way
				if (next.getName().startsWith("ou=") || next.getName().startsWith("o=")){
					orgNames.add(next.getName() + "," + name);
					List <String> suborgs = new ArrayList<String>();
					suborgs = getChildOrgs(next.getName() + "," + name, admnctx);
					orgNames.addAll(suborgs);
				}
				
			}
			//admnctx.close();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orgNames;
	}

	@Override
	public Hashtable<String, String> getUserData(String uniqueUserId) {
		Hashtable<String, String> userdata = new Hashtable<String, String>();

        DirContext ctx = null;
        try{
	        ctx = getAdminContext();
	        
	        String filter = "(uid=" + uniqueUserId + ")";  
			SearchControls sc = new SearchControls();  
            sc.setSearchScope(SearchControls.SUBTREE_SCOPE); 
			NamingEnumeration results = ctx.search("",filter,sc);
			
			
	        while(results.hasMore()) {
	        	SearchResult result = (SearchResult)results.next();
	        	NamingEnumeration n;
	        	int i;
	        	System.out.println("Object Name: " + result.getName());
	        	Attributes userattrs = ctx.getAttributes(result.getName());
	        	Attribute attr = userattrs.get(emailAttribute);
	        	if (!(attr == null)){
	        		n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("email" + i,val);
		        		i++;
		        	}
	        	}
	        	
	        	attr = userattrs.get(mobilePhoneAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("mobilePhone" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(descriptionAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("description" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(homePhoneAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("homePhone" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(entryDNAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("entryDN" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(givenNameAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("givenName" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(commonNameAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("cn" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(objectClassAttribute);
	        	System.out.println("Getting Object Classes");
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("objectClass" + i,val);
		        		System.out.println("Object Class found: " + val);
		        		i++;
		        	}
	        	}else {
	        		System.out.println("object class is null");
	        	}
	        	attr = userattrs.get(addressAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("address" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(postalCodeAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("postalCode" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(stateAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("state" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(surnameAttribute);
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("surname" + i,val);
		        		i++;
		        	}
	        	}
	        	attr = userattrs.get(groupMembershipAttribute);
	        	System.out.println("Getting Group Membership");
	        	if (!(attr == null)){
		        	n = attr.getAll();
		        	i = 1;
		        	while (n.hasMore()){
		        		String val = (String)n.next();
		        		userdata.put("groupname" + i,val);
		        		System.out.println("GROUP MEMBERSHIP: " + val);
		        		i++;
		        	}
	        	}else{
	        		System.out.println("Group membership is null");
	        	}
	        	//what if search returns multiple results?
	        	ctx.close();
	        	return userdata;
	        }
        }catch (NamingException ne){
        	ne.printStackTrace();
        	System.out.println("Problem connecting to directory: " + ne.getExplanation());
        	
        }finally{
        	
        }
		return null;
	}

	@Override
	public Hashtable<String, String> getClientrData(String uniqueClientId) {
		// TODO Auto-generated method stub
		return null;
	}
*/
}
