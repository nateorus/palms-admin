package com.pdinh.enterprise.managedbean.form;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.dao.RoleContextDao;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.enterprise.managedbean.SessionBean;
import com.pdinh.enterprise.realm.PDINHAuthenticationToken;
/*
import com.pdinh.ldap.persistence.dao.LdapUserDao;
import com.pdinh.ldap.persistence.entity.LdapUser;
*/
import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;

/**
 * Session Bean implementation class LoginBean
 */
@ManagedBean(name="LoginBean")
@ViewScoped
public class LoginBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LoginBean.class);
	private static final Logger auditLog = LoggerFactory.getLogger("AUDIT");
	private String username;
	private String password;
	private String newPassword;
	private String confirmPassword;
	
	private String userTime;
	
	
	//private Subject subject;
	//private LdapUserDao userDao = new LdapUserDao();
	private SecurityManager securityManager;
	
	@EJB
	private PalmsSubjectDao palmsSubjectDao;
	
	@EJB
	private PalmsRealmDao palmsRealmDao;
	
	@EJB
	private RoleContextDao roleContextDao;
	
	@EJB
	private PermissionServiceLocal permissionService;
	
	@EJB
	private EntityServiceLocal entityService;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	
    public LoginBean() {
        // nothing here
    }
    public String loginShiro(){
    	//UsernamePasswordToken token = new UsernamePasswordToken(this.username, this.password);
    	PDINHAuthenticationToken token = new PDINHAuthenticationToken(this.username, this.password);
    	
    	log.debug("Got User Time string: {}", this.userTime);
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss aa");
    	Date localTime = new Date();
    	try {
			localTime = formatter.parse(this.userTime);
			
			log.debug("Parsed local time: {}", localTime);
		} catch (ParseException e1) {
			log.debug("Caught ParseException parsing local time: {}", this.userTime);
			//e1.printStackTrace();
		}
    	token.setLocalTime(localTime);
    	FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getFlash().setKeepMessages(true);
		Subject subject = SecurityUtils.getSubject();
    	try{
    		subject.logout();
    		subject.login(token);
    		if (subject.isAuthenticated()){
    			log.debug("Subject {} is authenticated", username);
	    		auditLog.info("Subject logged in: {}, local time: {}",username, userTime);
	    		Session session = subject.getSession();
	    		session.setAttribute("username", this.username);
	    		
	    		//This triggers Authorization Info retrieval via doGetAuthorizationInfo method 
	    		//in PDINHJndiLdapRealm implementation.  To ensure Subject instance is created for
	    		//new users, always call isPermitted before retrieving subject and role context.
	    		
	    		if (subject.isPermitted("loginAuthorization")){
	    			log.debug("Subject is Permitted to Login");
	    		}else{
	    			log.debug("Subject is Not permitted to Login");
	    			facesContext.addMessage(null, new FacesMessage("Logon Failed", "Logon Permission not granted"));
	    			return "login?faces-redirect=true";
	    		}
    		}else{
    			log.debug("Subject {} is not authenticated", username);
    		}
    	}catch (UnknownAccountException uae){
    		//uae.printStackTrace();
    		log.error("Unknown Account Exception: {}", uae.getMessage());
    		facesContext.addMessage(null, new FacesMessage("Logon Failed", "Unknown Account"));
    		return "login_failed?faces-redirect=true";
    	}catch (IncorrectCredentialsException ice){
    		//ice.printStackTrace();
    		log.error("Incorrect Credentials Exception: {}", ice.getMessage());
    		facesContext.addMessage(null, new FacesMessage("Logon Failed", "Incorrect Credentials"));
    		return "login_failed?faces-redirect=true";
    	}catch (AuthenticationException e){
    		log.error("Caught Authentication exception. Message: {} Cause: {}", e.getMessage(), e.getCause());
    		if (e.getMessage().contains("expired".subSequence(0, 6))){
    			facesContext.addMessage(null, new FacesMessage("Password Expired", "Please update your password to continue"));
    			return "password_expired?faces-redirect=true";
    		}else{
    			
    	    	facesContext.addMessage(null, new FacesMessage("Logon Failed", "Please try again"));
    			return "login_failed?faces-redirect=true";
    		}
    	}
    		
		Set<?> realms = subject.getPrincipals().getRealmNames();
		String realm = "";
		if (realms.size() != 1){
			if (realms.isEmpty()){
				log.error("No Realms found...Subject Must belong to a Realm: {}", subject.getPrincipal());
				facesContext.addMessage(null, new FacesMessage("Logon Failed", "No Realm found"));
				return "login_failed?faces-redirect=true";
			}else{
				log.error("Multiple realms detected...Subject Cannot belong to multiple realms: {}", subject.getPrincipal());
				facesContext.addMessage(null, new FacesMessage("Logon Failed", "Multiple Realm found"));
				return "login_failed?faces-redirect=true";
			}
		}else{
    		Iterator<?> riter = realms.iterator();
    		
    		while (riter.hasNext()){
    			realm = (String)riter.next();
    			log.debug("Realm: " + realm);
    		}
		}
		setSessionPermissions(realm);
		

    	
    	token.clear();
    	
    	return "dashboard?faces-redirect=true";
    	
    }
    
    public void setSessionPermissions(String realmName){
    	sessionBean.setSessionPermissions(realmName);
    	/*
    	Subject subject = SecurityUtils.getSubject();
		//PalmsRealm prealm = palmsRealmDao.findRealmByName(realmName);
		PalmsSubject psub = palmsSubjectDao.findSubjectByPrincipalAndRealmName(subject.getPrincipal().toString(), realmName);
		
		RoleContext sessionRoleContext = new RoleContext();
		if (psub == null){
			//Subject should have been created by login process (PDINHJndiLdapRealm) if not found so this is a failure
			log.error("No Palms Subject Found. Authorization Failed for {} and realm {}", username, realmName);
			throw new AuthenticationException();
			
		}else{
			log.debug("Subject found with ID: {} and ext_id: {}", psub.getSubject_id(), psub.getExt_id());
			sessionRoleContext = entityService.getDefaultContext(psub);
			log.debug("Finished Getting Default Context");
			sessionBean.setRoleContext(sessionRoleContext);
		}
		/*
    	if (entityService.isTypeAuthorized(sessionRoleContext.getRole_context_id(), EntityType.REALM_TYPE)){
    		sessionBean.setAllowAllRealms(true);
    	}else{
    		List<Integer> authorizedRealmIds = new ArrayList<Integer>();
	    	List<Integer> allowRules = new ArrayList<Integer>();
	    	allowRules.add(3);//AllowChildren
	    	allowRules.add(5);//AllowAll
	    	allowRules.add(1);//Allow
	    	authorizedRealmIds = entityService.getEntityIds(sessionRoleContext.getRole_context_id(), EntityType.REALM_TYPE, allowRules);
	    	
	    	sessionBean.setAuthorizedRealmIds(authorizedRealmIds);
    	}
    	*/
    	/*
		sessionBean.setLoggedIn(true);
		sessionBean.setAllowAllRealms(true);
		sessionBean.setUid(subject.getPrincipal().toString());

		sessionBean.setSubjectId(psub.getSubject_id());
		sessionBean.setRenderPermissionGroupTab(subject.isPermitted("viewPermissionGroups"));
		sessionBean.setRenderRoleTab(subject.isPermitted("viewRoles"));
		sessionBean.setRenderRoleTemplateTab(subject.isPermitted("viewRoleTemplates"));
		sessionBean.setRenderRealmTab(subject.isPermitted("viewRealms"));
		sessionBean.setRenderSubjectTab(subject.isPermitted("viewSubjects"));
		sessionBean.setRenderProjectTab(subject.isPermitted("manageProject"));
		// TODO Add permission for manage entities
		sessionBean.setRenderEntitiesTab(true);
		
		*/
	}
    
    

    public String logout(){
    	
    	
    	auditLog.info("Subject logged out: {}", SecurityUtils.getSubject().getPrincipal());
		
		Subject subject = SecurityUtils.getSubject();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			request.logout();
			subject.logout();
			
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			return "login?faces-redirect=true";
		} catch (ServletException e) {
			e.printStackTrace();
		}

		return null;
    }
    /*
    public String resetPassword(){
    	Subject subject = SecurityUtils.getSubject();
    	if (!newPassword.equals(confirmPassword)){
        	FacesContext facesContext = FacesContext.getCurrentInstance();
        	facesContext.addMessage(null, new FacesMessage("Passwords do not match", "Please try again"));
        	return "password_expired.xhtml?faces-redirect=true";
    	}
    	subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		Object username = session.getAttribute("username");
		log.debug("Got username from shiro session: {}", username);
		LdapUserDao dao = new LdapUserDao();
		dao.setHost("nwondra2");
		dao.setDomain("dc=example,dc=com");
		//LdapUser user = dao.findUserByUid(username.toString());
		Attributes attrs = new BasicAttributes();
		Attribute newPasswordAttr = new BasicAttribute("userPassword", newPassword);
		Attribute usernameAttr = new BasicAttribute("uid", username.toString());
		attrs.put(newPasswordAttr);
		attrs.put(usernameAttr);
		dao.modifyUserAttributes(attrs);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
    	facesContext.addMessage(null, new FacesMessage("Password updated successfully", "Please log in"));
    	return "login.xhtml?faces-redirect=true";
    }
    */
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	public SecurityManager getSecurityManager() {
		return securityManager;
	}
	public void setSecurityManager(SecurityManager securityManager) {
		this.securityManager = securityManager;
	}
	public PalmsSubjectDao getPalmsSubjectDao() {
		return palmsSubjectDao;
	}
	public void setPalmsSubjectDao(PalmsSubjectDao palmsSubjectDao) {
		this.palmsSubjectDao = palmsSubjectDao;
	}
	public PalmsRealmDao getPalmsRealmDao() {
		return palmsRealmDao;
	}
	public void setPalmsRealmDao(PalmsRealmDao palmsRealmDao) {
		this.palmsRealmDao = palmsRealmDao;
	}
	public RoleContextDao getRoleContextDao() {
		return roleContextDao;
	}
	public void setRoleContextDao(RoleContextDao roleContextDao) {
		this.roleContextDao = roleContextDao;
	}
	public PermissionServiceLocal getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(PermissionServiceLocal permissionService) {
		this.permissionService = permissionService;
	}
/*	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}*/
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getUserTime() {
		return userTime;
	}
	public void setUserTime(String userTime) {
		this.userTime = userTime;
	}

}
