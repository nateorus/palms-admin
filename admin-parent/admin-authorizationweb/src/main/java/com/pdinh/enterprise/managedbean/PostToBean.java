package com.pdinh.enterprise.managedbean;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.pdinh.auth.persistence.entity.PalmsSession;

@ManagedBean
@ViewScoped
public class PostToBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@Resource(mappedName = "application/config_properties")
	private Properties properties;

	private String path;
	private String sysName;
	private String returnTo;

	public void init() {
		System.out.println("PostToBean.init()!");
	}

	public void preparePost() {
		String hostName = "";
		ExternalContext econtext = FacesContext.getCurrentInstance().getExternalContext();
		String protocol = "http://";
		if (econtext.isSecure()) {
			protocol = "https://";
		}
		RequestContext context = RequestContext.getCurrentInstance();
		PalmsSession ps = sessionBean.getPalmsSession();
		context.addCallbackParam("sessionToken", ps.getSessionToken());
		context.addCallbackParam("subjectId", sessionBean.getRoleContext().getSubject().getSubject_id());
		
		if (getReturnTo() != null) {
			context.addCallbackParam("returnTo", getReturnTo());
		}

		if (getSysName().equalsIgnoreCase("lrm")) {
			hostName = properties.getProperty("lrmHost");
		} else if (getSysName().equalsIgnoreCase("auth")) {
			hostName = properties.getProperty("authorizationHost");
		} else {
			hostName = econtext.getRequestServerName() + ":" + econtext.getRequestServerPort();
		}

		context.addCallbackParam("url", protocol + hostName + getPath());
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public String getReturnTo() {
		return returnTo;
	}

	public void setReturnTo(String returnTo) {
		this.returnTo = returnTo;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

}
