package com.pdinh.enterprise;

import java.io.Serializable;


public class LdapAttribute implements Serializable{
	private static final long serialVersionUID = 1L;
	private String attrName;
	private String attrVal = "";
	private String defName = "";
	private String defEquality = "";
	private String defSubstr = "";
	private String defxOrigin = "";
	private String defNumberidOid = "";
	private boolean valueModified = false;
	
	
	public LdapAttribute(String name){
		attrName = name;
	}
	
	public LdapAttribute(String name, String value){
		attrName = name;
		attrVal = value;
	}
	
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrVal() {
		return attrVal;
	}
	public void setAttrVal(String attrVal) {
		this.attrVal = attrVal;
	}
	public String getDefName() {
		return defName;
	}
	public void setDefName(String defName) {
		this.defName = defName;
	}
	public String getDefEquality() {
		return defEquality;
	}
	public void setDefEquality(String defEquality) {
		this.defEquality = defEquality;
	}
	public String getDefSubstr() {
		return defSubstr;
	}
	public void setDefSubstr(String defSubstr) {
		this.defSubstr = defSubstr;
	}
	public String getDefxOrigin() {
		return defxOrigin;
	}
	public void setDefxOrigin(String defxOrigin) {
		this.defxOrigin = defxOrigin;
	}
	public String getDefNumberidOid() {
		return defNumberidOid;
	}
	public void setDefNumberidOid(String defNumberidOid) {
		this.defNumberidOid = defNumberidOid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public boolean isValueModified() {
		return valueModified;
	}
	public void setValueModified(boolean valueModified) {
		this.valueModified = valueModified;
	}
}
