package com.pdinh.enterprise.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationListener;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LdapAuthenticationListener implements AuthenticationListener {
	private static final Logger log = LoggerFactory.getLogger(LdapAuthenticationListener.class);
	@Override
	public void onFailure(AuthenticationToken arg0, AuthenticationException arg1) {
		log.debug("In Listener onFailure() with AuthenticationToken {}, and Exception {}", arg0.getPrincipal(), arg1.getMessage());

	}

	@Override
	public void onLogout(PrincipalCollection arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSuccess(AuthenticationToken arg0, AuthenticationInfo arg1) {
		// TODO Auto-generated method stub

	}

}
