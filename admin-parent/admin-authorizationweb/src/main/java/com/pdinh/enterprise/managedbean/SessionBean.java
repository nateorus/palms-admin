package com.pdinh.enterprise.managedbean;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.dao.PalmsSessionDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.persistence.entity.PalmsSession;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.persistence.audit.EntityAuditLogger;


/**
 * Session Bean implementation class SessionBean
 */
@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SessionBean.class);
	//private List<String> roles;
	//private List<String> permissionsNames;
	//private LdapUser user;
	private boolean loggedIn = false;
	private String uid;

	private RoleContext roleContext;
	
	private boolean renderPermissionGroupTab = false;
	private boolean renderRoleTab = false;
	private boolean renderRoleTemplateTab = false;
	private boolean renderRealmTab = false;
	private boolean renderSubjectTab = false;
	private boolean renderProjectTab = false;
	private boolean renderEntitiesTab = false;
	
	private List<Integer> authorizedRealmIds;
	private boolean allowAllRealms = false;
	private int subjectId;
	
	@EJB
	private PalmsSubjectDao palmsSubjectDao;
	
	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private PalmsSessionDao palmsSessionDao;
    /**
     * Default constructor. 
     */
    public SessionBean() {
        // TODO Auto-generated constructor stub
    }
    
    @PostConstruct 
    public void init(){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("In SessionBean init()");
    	if (subject.isAuthenticated()){
    		log.debug("Subject is authenticated: {}", subject.getPrincipal());
    		Set<?> realms = subject.getPrincipals().getRealmNames();
    		String realm = "";
    		if (realms.size() != 1){
    			//TODO: Redirect to login page.. this shouldn't happen
    			if (realms.isEmpty()){
    				log.error("No Realms found...Subject Must belong to a Realm: {}", subject.getPrincipal());
    				
    			}else{
    				log.error("Multiple realms detected...Subject Cannot belong to multiple realms: {}", subject.getPrincipal());
    				
    			}
    		}else{
    			
        		Iterator<?> riter = realms.iterator();
        		
        		while (riter.hasNext()){
        			realm = (String)riter.next();
        			log.debug("Realm: " + realm);
        		}
    		}
    		setSessionPermissions(realm);
    	}
    }
    public void setSessionPermissions(String realmName){
    	log.debug("In setSessionPermissions");
    	Subject subject = SecurityUtils.getSubject();
		//PalmsRealm prealm = palmsRealmDao.findRealmByName(realmName);
		PalmsSubject psub = palmsSubjectDao.findSubjectByPrincipalAndRealmName(subject.getPrincipal().toString(), realmName);
		
		RoleContext sessionRoleContext = new RoleContext();
		if (psub == null){
			//Subject should have been created by login process (PDINHJndiLdapRealm) if not found so this is a failure
			log.error("No Palms Subject Found. Authorization Failed for {} and realm {}", subject.getPrincipal(), realmName);
			throw new AuthenticationException();
			
		}else{
			log.debug("Subject found with ID: {} and ext_id: {}", psub.getSubject_id(), psub.getExt_id());
			sessionRoleContext = entityService.getDefaultContext(psub);
			log.debug("Finished Getting Default Context");
			this.setRoleContext(sessionRoleContext);
		}
		
		this.setLoggedIn(true);
		log.debug("Settig allowAllRealms = true");
		this.setAllowAllRealms(true);
		this.setUid(subject.getPrincipal().toString());

		this.setSubjectId(psub.getSubject_id());
		this.setRenderPermissionGroupTab(subject.isPermitted("viewPermissionGroups"));
		this.setRenderRoleTab(subject.isPermitted("viewRoles"));
		this.setRenderRoleTemplateTab(subject.isPermitted("viewRoleTemplates"));
		this.setRenderRealmTab(subject.isPermitted("viewRealms"));
		this.setRenderSubjectTab(subject.isPermitted("viewSubjects"));
		this.setRenderProjectTab(subject.isPermitted("manageProject"));
		// TODO Add permission for manage entities
		this.setRenderEntitiesTab(true);
		
	}
    

	public PalmsSession getPalmsSession() {
		Subject subject = SecurityUtils.getSubject();
		return palmsSessionDao.findSessionById(subject.getSession().getId().toString());
	}

	public String handleClickPalmsAdminLink() {
		Subject subject = SecurityUtils.getSubject();
		EntityAuditLogger.auditLog.info("{} performed {} operation.", new Object[] { subject.getPrincipal(),
				EntityAuditLogger.OP_ACCESS_AUTHORIZATION_SYSTEM });
		return "postTo.xhtml?faces-redirect=true&path=/adminweb/faces/clientList.xhtml&sys=admin";
	}


/*	public LdapUser getUser() {
		return user;
	}
	public void setUser(LdapUser user) {
		this.user = user;
	}*/
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}

	public RoleContext getRoleContext() {
		return roleContext;
	}
	public void setRoleContext(RoleContext roleContext) {
		this.roleContext = roleContext;
	}
	public boolean isRenderPermissionGroupTab() {
		return renderPermissionGroupTab;
	}
	public void setRenderPermissionGroupTab(boolean renderPermissionGroupTab) {
		this.renderPermissionGroupTab = renderPermissionGroupTab;
	}
	public boolean isRenderRoleTab() {
		return renderRoleTab;
	}
	public void setRenderRoleTab(boolean renderRoleTab) {
		this.renderRoleTab = renderRoleTab;
	}
	public boolean isRenderRoleTemplateTab() {
		return renderRoleTemplateTab;
	}
	public void setRenderRoleTemplateTab(boolean renderRoleTemplateTab) {
		this.renderRoleTemplateTab = renderRoleTemplateTab;
	}
	public boolean isRenderRealmTab() {
		return renderRealmTab;
	}
	public void setRenderRealmTab(boolean renderRealmTab) {
		this.renderRealmTab = renderRealmTab;
	}
	public boolean isRenderSubjectTab() {
		return renderSubjectTab;
	}
	public void setRenderSubjectTab(boolean renderSubjectTab) {
		this.renderSubjectTab = renderSubjectTab;
	}


	public List<Integer> getAuthorizedRealmIds() {
		return authorizedRealmIds;
	}


	public void setAuthorizedRealmIds(List<Integer> authorizedRealmIds) {
		this.authorizedRealmIds = authorizedRealmIds;
	}


	public boolean isAllowAllRealms() {
		return allowAllRealms;
	}


	public void setAllowAllRealms(boolean allowAllRealms) {
		this.allowAllRealms = allowAllRealms;
	}


	public int getSubjectId() {
		return subjectId;
	}


	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}


	public boolean isRenderProjectTab() {
		return renderProjectTab;
	}


	public void setRenderProjectTab(boolean renderProjectTab) {
		this.renderProjectTab = renderProjectTab;
	}


	public boolean isRenderEntitiesTab() {
		return renderEntitiesTab;
	}


	public void setRenderEntitiesTab(boolean renderEntitiesTab) {
		this.renderEntitiesTab = renderEntitiesTab;
	}



}
