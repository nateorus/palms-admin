package com.pdinh.authorization.presentation.util;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PermissionGroupDao;

@ManagedBean(name = "permissionGroupNameValidator")
@ViewScoped
@FacesValidator(value = "permissionGroupNameValidator")
public class PermissionGroupNameValidator implements Validator, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PermissionGroupNameValidator.class);

	@EJB
	private PermissionGroupDao permissionGroupDao;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		// TODO Auto-generated method stub
		String pgName = String.valueOf(arg2);
		String errMsg = "";
		log.debug("Validating Permission Group Name: {}", pgName);
		log.debug("Validating for UI Component: {}", arg1.getClientId());

		//Role role = roleDao.findRoleByName(roleName);
		boolean valid = true;
		if (arg2 == null){
			valid = false;
			errMsg = "Invalid Permission Group Name.  Name cannot be null.";

		} else if (pgName.length() > 50){
			valid = false;
			errMsg = "Invalid Permission Group Name.  Permission Group name > 50 chars.";
		} else if (pgName.contains(" ")){
			valid = false;
			errMsg = "Invalid Permission Group Name.  Permission Group names cannot contain spaces.";
		} else if (pgName.equalsIgnoreCase("")){
			valid = false;
			errMsg = "Invalid Permission Group Name.  Permission Group name cannot be empty.";
		}
		if (arg1.getClientId().endsWith("grpNameInputforNew")){
			if (!(permissionGroupDao.findPermissionGroupByName(pgName) == null)){
				valid = false;
				errMsg = "Invalid Permission Group Name.  A Permission Group with that name already exists.";
			}
		}
		if (!valid){
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, errMsg, 
					"The Permission Group name provided is null, already in use, contains invalid chars or is too long");
			throw new ValidatorException(message);
			
		}
				
		
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public PermissionGroupDao getPermissionGroupDao() {
		return permissionGroupDao;
	}

	public void setPermissionGroupDao(PermissionGroupDao permissionGroupDao) {
		this.permissionGroupDao = permissionGroupDao;
	}

}
