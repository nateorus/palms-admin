package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.SessionBean;


import com.pdinh.authorization.presentation.util.PermissionGroupConverter;
import com.pdinh.authorization.presentation.util.RoleTypeConverter;

import com.pdinh.authorization.presentation.view.ManageRolesViewBean;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.PermissionGroupDao;
import com.pdinh.auth.data.dao.RoleDao;
import com.pdinh.auth.data.dao.RoleTypeDao;

import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.PermissionGroup;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.auth.persistence.entity.RoleType;

/**
 * Session Bean implementation class ManageRolesControllerBean
 */
@ManagedBean(name = "manageRolesControllerBean")
@ViewScoped
public class ManageRolesControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(ManageRolesControllerBean.class);
	
	//private Subject subject;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(name = "manageRolesViewBean", value = "#{manageRolesViewBean}")
	private ManageRolesViewBean manageRolesViewBean;
	
//	@ManagedProperty(name = "roleNameValidator", value = "#{roleNameValidator}")
//	private RoleNameValidator roleNameValidator;
	
	@ManagedProperty(name = "permissionGroupConverter", value="#{permissionGroupConverter}")
	private PermissionGroupConverter permissionGroupConverter;
	
	@ManagedProperty(name = "roleTypeConverter", value="#{roleTypeConverter}")
	private RoleTypeConverter roleTypeConverter;
	
	
	@EJB
	private RoleDao roleDao;
	
	@EJB
	private PermissionGroupDao permissionGroupDao;
	
	@EJB
	private RoleTypeDao roleTypeDao;
	
	@EJB
	private PermissionServiceLocal permissionService;
	
	@EJB
	private EntityServiceLocal entityService;
	
    public ManageRolesControllerBean() {
        // TODO Auto-generated constructor stub
    }
    public void initView(){
    	//Use this for checking permissions
    	Subject subject = SecurityUtils.getSubject();

    	List<Integer> authRoleIds = entityService.getAuthorizedEntities(sessionBean.getRoleContext(), "rl", true);
    	if (authRoleIds.size() == 0){
    		manageRolesViewBean.setRoles(roleDao.findAll());
    	} else {
    		manageRolesViewBean.setRoles(roleDao.findAllAuthorizedRoles(authRoleIds));
    	}
    	manageRolesViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
    	
    	manageRolesViewBean.setRoleTypes(roleTypeDao.findAll(true));
    	
    	if (manageRolesViewBean.getCreateRoleLists() == null){
    		manageRolesViewBean.setCreateRoleLists(new DualListModel<PermissionGroup>(manageRolesViewBean.getPermissionGroups(), new ArrayList<PermissionGroup>()));
    	}
    	if (subject.isPermitted("createRoles")){
    		manageRolesViewBean.setCreateDisabled(false);
    	}
    	
    }

    public void handleRoleSelect(SelectEvent event){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("Handle Selecting a Role here");
    	if (subject.isPermitted("deleteRoles")){
    		manageRolesViewBean.setDeleteDisabled(false);
    	}
    	if (subject.isPermitted("editRoles")){
    		manageRolesViewBean.setEditDisabled(false);
    	}

    	//PermissionGroup pgroup = dashboardViewBean.getSelectedGroup();
    	//log.debug("selected group: {}", pgroup.getName());
    }


    public void handleCreateRole(){
    	log.debug("Handle Creating a new Role here");
    	Role aNewRole = new Role();
    	List <PermissionGroup> rList = manageRolesViewBean.getCreateRoleLists().getTarget();
    	Iterator iter = rList.iterator();
    	if (rList.isEmpty()){
    		log.debug("the target List was empty... no permission groups added");
    	}
    	while (iter.hasNext()){
    		PermissionGroup temp = (PermissionGroup)iter.next();
    		log.debug("Permission Group Object Name: {}", temp.getName());
    	}
    	aNewRole.setPermissionGroups(rList);
    	manageRolesViewBean.setNewRole(aNewRole);
    }
    public List<PermissionGroup> removePermissionGroup(List<PermissionGroup> list, Integer permissionGroupId){
    	for (PermissionGroup pg: list){
    		if (permissionGroupId == pg.getPermission_group_id()){
    			list.remove(pg);
    			return list;
    		}
    	}
    	return list;
    }
    public void handleEditRole(){
    	Role sRole = manageRolesViewBean.getSelectedRole();
    	manageRolesViewBean.setNewRole(sRole);
    	List<PermissionGroup> availableGroups = manageRolesViewBean.getPermissionGroups();
    	
    	
		for (PermissionGroup includedGroup:sRole.getPermissionGroups()){
			availableGroups = removePermissionGroup(availableGroups, includedGroup.getPermission_group_id());
		}
    	
    	
    	manageRolesViewBean.setCreateRoleLists(new DualListModel<PermissionGroup>(availableGroups, sRole.getPermissionGroups()));
    	
    	manageRolesViewBean.setRenderRoleNameInputEdit(true);
    	manageRolesViewBean.setRenderRoleNameInputNew(false);
    	manageRolesViewBean.setRenderEditButton(true);
    	manageRolesViewBean.setRenderSaveButton(false);
    	List<RoleType> types = manageRolesViewBean.getRoleTypes();
    	for (RoleType r: types){
    		if (r.getRole_type_id() == sRole.getRole_type_id()){
    			manageRolesViewBean.setsSelectedType(r.getName());
    		}
    	}
    	
    }
    public void handleCreateInit(){
    	handleClearSelectedPermissionGroups();
    	
    	manageRolesViewBean.setRenderRoleNameInputEdit(false);
    	manageRolesViewBean.setRenderRoleNameInputNew(true);
    	manageRolesViewBean.setRenderEditButton(false);
    	manageRolesViewBean.setRenderSaveButton(true);
    }
    public void handleNewRoleSetup(){
    	log.debug("Handle Setting new Role with user input here");
    	Role role = manageRolesViewBean.getNewRole();
    	role.setCreated_by(sessionBean.getUid());
    	List <RoleType> roleTypeList = manageRolesViewBean.getRoleTypes();
    	Iterator iter = roleTypeList.iterator();
    	while (iter.hasNext()){
    		RoleType thisRole = (RoleType)iter.next();
    		if (thisRole.getName().equalsIgnoreCase(manageRolesViewBean.getsSelectedType())){
    			log.debug("Found matching role name: {}", thisRole.getName());
    			role.setRole_type_id(thisRole.getRole_type_id());
    			break;
    		}
    	}
    	List <PermissionGroup> rList = manageRolesViewBean.getCreateRoleLists().getTarget();
    	
    	if (rList.isEmpty()){
    		log.debug("the target List was empty... no permission groups added");
    	}else{
    		log.debug("the target List contains {} items", rList.size());
    		role.setPermissionGroups(rList);
    	}
    }
    public void handleSubmitNewRole(){
    	log.debug("Handle Persistence of a new Role here");
    	
    	Role role = manageRolesViewBean.getNewRole();
    	
    	log.debug("Name: {}", role.getName());
    	log.debug("Label: {}", role.getLabel());
    	log.debug("Description: {}", role.getDescription());
    	log.debug("ID: {}", role.getRole_id());
    	log.debug("Role type id: {}", role.getRole_type_id());
    	
    	roleDao.create(role);
    	manageRolesViewBean.setRoles(roleDao.findAll(true));
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Created Role", role.getName()));
    	
    	
    }
    public void handleSubmitEditRole(){
    	Role role = manageRolesViewBean.getNewRole();
    	roleDao.update(role);
    	roleDao.evictAll();
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Updated Role", role.getName()));
    }
    public void handleClearSelectedPermissionGroups(){
    	manageRolesViewBean.setCreateRoleLists(new DualListModel<PermissionGroup>(manageRolesViewBean.getPermissionGroups(), new ArrayList<PermissionGroup>()));
    }
    public void handleDeleteRole(){
    	Role role = manageRolesViewBean.getSelectedRole();
    	if (permissionService.canDeleteRole(role)){
    		roleDao.delete(role);
        	manageRolesViewBean.setRoles(roleDao.findAll(true));
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage("Role Deleted", role.getName()));
    	}else{
    		FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage("Cannot Delete a Role that is used in a Template", role.getName()));
    	}
    	
    }
    public String handleFlowProcessEvent(FlowEvent event){
    	log.debug("Current wizard Step: {}", event.getOldStep());
    	log.debug("Next step: {}", event.getNewStep());
    
    	if (event.getOldStep().equalsIgnoreCase("selectPGroupsTab")){
    		//handleCreateRole();
    	}
    	if (event.getOldStep().equalsIgnoreCase("roleDetailsTab")){
    		handleNewRoleSetup();
    	}
    	return event.getNewStep();
    }

	public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	public ManageRolesViewBean getManageRolesViewBean() {
		return manageRolesViewBean;
	}
	public void setManageRolesViewBean(ManageRolesViewBean manageRolesViewBean) {
		this.manageRolesViewBean = manageRolesViewBean;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	public PermissionGroupDao getPermissionGroupDao() {
		return permissionGroupDao;
	}
	public void setPermissionGroupDao(PermissionGroupDao permissionGroupDao) {
		this.permissionGroupDao = permissionGroupDao;
	}
	public PermissionGroupConverter getPermissionGroupConverter() {
		return permissionGroupConverter;
	}
	public void setPermissionGroupConverter(PermissionGroupConverter permissionGroupConverter) {
		this.permissionGroupConverter = permissionGroupConverter;
	}
	public RoleTypeDao getRoleTypeDao() {
		return roleTypeDao;
	}
	public void setRoleTypeDao(RoleTypeDao roleTypeDao) {
		this.roleTypeDao = roleTypeDao;
	}
	public RoleTypeConverter getRoleTypeConverter() {
		return roleTypeConverter;
	}
	public void setRoleTypeConverter(RoleTypeConverter roleTypeConverter) {
		this.roleTypeConverter = roleTypeConverter;
	}
	public PermissionServiceLocal getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(PermissionServiceLocal permissionService) {
		this.permissionService = permissionService;
	}
	public EntityServiceLocal getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityServiceLocal entityService) {
		this.entityService = entityService;
	}
	/*
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
*/


}
