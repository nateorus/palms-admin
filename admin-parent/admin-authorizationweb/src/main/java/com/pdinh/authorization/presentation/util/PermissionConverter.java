package com.pdinh.authorization.presentation.util;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.PermissionDao;
import com.pdinh.auth.persistence.entity.Permission;

@ManagedBean(name = "permissionConverter")
@ViewScoped
@FacesConverter(value = "permissionConverter")
public class PermissionConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PermissionConverter.class);
	
	@EJB
	private PermissionDao permissionDao;
	
	public PermissionConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		if (arg2 != null){
			int pid = Integer.valueOf(arg2);
			log.trace("In getAsObject method of permissionConverter.  Argument: {}", pid);
			//log.debug("PermissionDao {}", permissionDao.toString());
//			List <Permission> plist = permissionDao.findAll(true);
//			Iterator iter = plist.iterator();
//			while (iter.hasNext()){
//				Permission p = (Permission)iter.next();
//				log.debug("Iterating over all permissions {}", p.getName());
//				if (p.getPermission_id() == pid){
//					return p;
//				}
//			}
			Permission p = permissionDao.findById(pid);
			log.trace("Returned Permission Object: {}", p.getName());
			return p;

		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		if (arg2 != null){
			String pid = "" + ((Permission) arg2).getPermission_id();
			log.trace("In getAsString method of permissionConverter.  Argument: {}", pid);
			return pid;
		}
		return null;
	}

	public PermissionDao getPermissionDao() {
		return permissionDao;
	}

	public void setPermissionDao(PermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}

}
