package com.pdinh.authorization.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class AuditLogServlet
 */
@WebServlet("/AuditLogServlet")
public class AuditLogServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger auditLog = LoggerFactory.getLogger("AUDIT-ADMIN");
	private static final Logger log = LoggerFactory.getLogger(AuditLogServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuditLogServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String user = request.getParameter("user");
		String action = request.getParameter("action");
		String entityType = request.getParameter("entityType");
		String entityId = request.getParameter("entityId");
		String message = request.getParameter("message");
		log.debug("{} performed {} action on {} {}. {}", new Object[] { user, action, entityType, entityId, message });

		auditLog.info("{} performed {} action on {} {}. {}",
				new Object[] { user, action, entityType, entityId, message });
	}

}
