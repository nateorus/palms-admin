package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.List;




import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.model.DualListModel;
import org.primefaces.model.MenuModel;
import org.primefaces.model.TreeNode;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.AuditEvent;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RealmFilter;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.authorization.presentation.obj.CompanyEntityDataObj;
import com.pdinh.authorization.presentation.obj.EntityTreeNode;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.User;

/**
 * Session Bean implementation class DashboardViewBean
 */
@ManagedBean(name = "manageSubjectsViewBean")
@ViewScoped
public class ManageSubjectsViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String uid;

	private List<PalmsRealm> realms;
	private List<PalmsSubject> subjects;
	private List<String> realmNames;
	private List<RoleContext> roleContexts;
	private List<ExternalSubjectDataObj> realmUsers;
	private List<EntityRule> entityRules;
	private List<CompanyEntityDataObj> allowedCompanies;
	private List<CompanyEntityDataObj> selectedAllowedCompanies;
	private List<AuditEvent> auditEvents;
	private List<AuditEvent> filteredEvents;
	//private List<EntityTreeNode> selectedNodes;
	private TreeNode[] selectedNodes;
	private List<TreeNode> allSelectedNodes;
	private List<TreeNode> unSelectedNodes;
	
	
	
	private String selectedRealmName;
	private String selectedContextId;
	private String selectedEntityRuleId;
	private PalmsSubject selectedSubject;
	private RoleContext selectedContext;
	private RoleContext newContext = new RoleContext();
	private int newContextDefault;
	private ExternalSubjectDataObj selectedRealmUser;
	
	private EntityType companyType;
	private EntityType projectType;
	private EntityType userType;
	

	private AuditEvent selectedAuditEvent;
	private DualListModel<Role> contextRoleLists;
	
	private boolean editRolesDisabled = true;
	private boolean getSubjectsDisabled = true;
	private boolean addContextDisabled = true;
	private boolean deleteContextDisabled = true;
	private boolean importSubjectsDisabled = true;
	private boolean renderCountryColumn = true;
	private boolean renderCompanyNameColumn = false;
	private boolean renderLocationColumn = true;
	private boolean renderExternalIdColumn = false;
	private boolean renderTelephoneColumn = true;
	private boolean entitiesButtonDisabled = true;
	private boolean navEntitiesButtonDisabled = true;
	private boolean initRealmMetadataDisabled = true;
	private boolean updateRealmMetadataDisabled = true;
	private boolean refreshMetadataButtonDisabled = true;
	
	/* -- Stuff for Linked Account Dialog -- */
	private boolean linkedAccountButtonDisabled = true;
	private boolean linkRadioDisabled = false;
	private boolean linkRadioSubmitDisabled = true;
	private boolean refreshAccountButtonDisabled = true;
	private boolean lookupByEmailButtonDisabled = true;
	
	private User existingLinkedUser;
	private String usersIdInput = "(enter numeric users id)";
	private String selectedLinkOption;
	
	/* -- End stuff for linked Account Dialog -- */
	
	private TreeNode rootNode;
	private TreeNode rootNodeView;
	private TreeNode selectedNode;
	private boolean grantAll = false;
	private boolean pickCompaniesDisabled = false;
	private String confirmGrantAllMessage = "Are you sure you want to grant full access to ALL entities?";
	private boolean renderConfirmButton = true;
	private boolean renderConfirm2Button = false;

	private DualListModel<Company> companyLists;
	private MenuModel menuModel;
	private boolean contextMenuDisabled = true;
	
	private boolean selectSearchMethodRendered = false;
	private List<RealmFilter> queries;
	private String selectedSearchAttr;
	private boolean selectCompanyRendered = false;
	private List<Company> companies;
	private String selectedCompany;
	private String emailInput;
	private boolean emailInputRendered = false;
	
	private Object[] filterParameters;
	
	public EntityType getCompanyType() {
		return companyType;
	}
	public void setCompanyType(EntityType companyType) {
		this.companyType = companyType;
	}
	public EntityType getProjectType() {
		return projectType;
	}
	public void setProjectType(EntityType projectType) {
		this.projectType = projectType;
	}
	public EntityType getUserType() {
		return userType;
	}
	public void setUserType(EntityType userType) {
		this.userType = userType;
	}

	
	
    /**
     * Default constructor. 
     */
    public ManageSubjectsViewBean() {
        // TODO Auto-generated constructor stub
    }
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<PalmsRealm> getRealms() {
		return realms;
	}
	public void setRealms(List<PalmsRealm> realms) {
		this.realms = realms;
	}
	public List<PalmsSubject> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<PalmsSubject> subjects) {
		this.subjects = subjects;
	}
	public List<String> getRealmNames() {
		return realmNames;
	}
	public void setRealmNames(List<String> realmNames) {
		this.realmNames = realmNames;
	}
	public String getSelectedRealmName() {
		return selectedRealmName;
	}
	public void setSelectedRealmName(String selectedRealmName) {
		this.selectedRealmName = selectedRealmName;
	}

	public PalmsSubject getSelectedSubject() {
		return selectedSubject;
	}
	public void setSelectedSubject(PalmsSubject selectedSubject) {
		this.selectedSubject = selectedSubject;
	}

	public boolean isGetSubjectsDisabled() {
		return getSubjectsDisabled;
	}
	public void setGetSubjectsDisabled(boolean getSubjectsDisabled) {
		this.getSubjectsDisabled = getSubjectsDisabled;
	}
	public boolean isEditRolesDisabled() {
		return editRolesDisabled;
	}
	public void setEditRolesDisabled(boolean editRolesDisabled) {
		this.editRolesDisabled = editRolesDisabled;
	}
	public boolean isAddContextDisabled() {
		return addContextDisabled;
	}
	public void setAddContextDisabled(boolean addContextDisabled) {
		this.addContextDisabled = addContextDisabled;
	}
	public List<RoleContext> getRoleContexts() {
		return roleContexts;
	}
	public void setRoleContexts(List<RoleContext> roleContexts) {
		this.roleContexts = roleContexts;
	}
	public RoleContext getSelectedContext() {
		return selectedContext;
	}
	public void setSelectedContext(RoleContext selectedContext) {
		this.selectedContext = selectedContext;
	}
	public DualListModel<Role> getContextRoleLists() {
		return contextRoleLists;
	}
	public void setContextRoleLists(DualListModel<Role> contextRoleLists) {
		this.contextRoleLists = contextRoleLists;
	}
	public String getSelectedContextId() {
		return selectedContextId;
	}
	public void setSelectedContextId(String selectedContextId) {
		this.selectedContextId = selectedContextId;
	}
	public RoleContext getNewContext() {
		return newContext;
	}
	public void setNewContext(RoleContext newContext) {
		this.newContext = newContext;
	}
	public int getNewContextDefault() {
		return newContextDefault;
	}
	public void setNewContextDefault(int newContextDefault) {
		this.newContextDefault = newContextDefault;
	}
	public boolean isDeleteContextDisabled() {
		return deleteContextDisabled;
	}
	public void setDeleteContextDisabled(boolean deleteContextDisabled) {
		this.deleteContextDisabled = deleteContextDisabled;
	}
	public boolean isImportSubjectsDisabled() {
		return importSubjectsDisabled;
	}
	public void setImportSubjectsDisabled(boolean importSubjectsDisabled) {
		this.importSubjectsDisabled = importSubjectsDisabled;
	}
	public List<ExternalSubjectDataObj> getRealmUsers() {
		return realmUsers;
	}
	public void setRealmUsers(List<ExternalSubjectDataObj> realmUsers) {
		this.realmUsers = realmUsers;
	}
	public ExternalSubjectDataObj getSelectedRealmUser() {
		return selectedRealmUser;
	}
	public void setSelectedRealmUser(ExternalSubjectDataObj selectedRealmUser) {
		this.selectedRealmUser = selectedRealmUser;
	}
	public boolean isRenderCountryColumn() {
		return renderCountryColumn;
	}
	public void setRenderCountryColumn(boolean renderCountryColumn) {
		this.renderCountryColumn = renderCountryColumn;
	}
	public boolean isRenderCompanyNameColumn() {
		return renderCompanyNameColumn;
	}
	public void setRenderCompanyNameColumn(boolean renderCompanyNameColumn) {
		this.renderCompanyNameColumn = renderCompanyNameColumn;
	}
	public boolean isRenderLocationColumn() {
		return renderLocationColumn;
	}
	public void setRenderLocationColumn(boolean renderLocationColumn) {
		this.renderLocationColumn = renderLocationColumn;
	}
	public boolean isRenderExternalIdColumn() {
		return renderExternalIdColumn;
	}
	public void setRenderExternalIdColumn(boolean renderExternalIdColumn) {
		this.renderExternalIdColumn = renderExternalIdColumn;
	}

	public boolean isEntitiesButtonDisabled() {
		return entitiesButtonDisabled;
	}
	public void setEntitiesButtonDisabled(boolean entitiesButtonDisabled) {
		this.entitiesButtonDisabled = entitiesButtonDisabled;
	}
	public TreeNode getRootNode() {
		return rootNode;
	}
	public void setRootNode(TreeNode rootNode) {
		this.rootNode = rootNode;
	}
	public TreeNode[] getSelectedNodes() {
		return selectedNodes;
	}
	public void setSelectedNodes(TreeNode[] selectedNodes) {
		this.selectedNodes = selectedNodes;
	}
	public List<TreeNode> getUnSelectedNodes() {
		return unSelectedNodes;
	}
	public void setUnSelectedNodes(List<TreeNode> unSelectedNodes) {
		this.unSelectedNodes = unSelectedNodes;
	}
	public DualListModel<Company> getCompanyLists() {
		return companyLists;
	}
	public void setCompanyLists(DualListModel<Company> companyLists) {
		this.companyLists = companyLists;
	}
	public TreeNode getRootNodeView() {
		return rootNodeView;
	}
	public void setRootNodeView(TreeNode rootNodeView) {
		this.rootNodeView = rootNodeView;
	}
	public TreeNode getSelectedNode() {
		return selectedNode;
	}
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	public String getSelectedEntityRuleId() {
		return selectedEntityRuleId;
	}
	public void setSelectedEntityRuleId(String selectedEntityRuleId) {
		this.selectedEntityRuleId = selectedEntityRuleId;
	}
	public List<EntityRule> getEntityRules() {
		return entityRules;
	}
	public void setEntityRules(List<EntityRule> entityRules) {
		this.entityRules = entityRules;
	}
	public List<CompanyEntityDataObj> getAllowedCompanies() {
		return allowedCompanies;
	}
	public void setAllowedCompanies(List<CompanyEntityDataObj> allowedCompanies) {
		this.allowedCompanies = allowedCompanies;
	}
	public List<CompanyEntityDataObj> getSelectedAllowedCompanies() {
		return selectedAllowedCompanies;
	}
	public void setSelectedAllowedCompanies(List<CompanyEntityDataObj> selectedAllowedCompanies) {
		this.selectedAllowedCompanies = selectedAllowedCompanies;
	}
	
	public boolean isGrantAll() {
		return grantAll;
	}
	public void setGrantAll(boolean grantAll) {
		this.grantAll = grantAll;
	}
	public boolean isPickCompaniesDisabled() {
		return pickCompaniesDisabled;
	}
	public void setPickCompaniesDisabled(boolean pickCompaniesDisabled) {
		this.pickCompaniesDisabled = pickCompaniesDisabled;
	}
	public String getConfirmGrantAllMessage() {
		return confirmGrantAllMessage;
	}
	public void setConfirmGrantAllMessage(String confirmGrantAllMessage) {
		this.confirmGrantAllMessage = confirmGrantAllMessage;
	}
	public boolean isRenderConfirmButton() {
		return renderConfirmButton;
	}
	public void setRenderConfirmButton(boolean renderConfirmButton) {
		this.renderConfirmButton = renderConfirmButton;
	}
	public boolean isRenderConfirm2Button() {
		return renderConfirm2Button;
	}
	public void setRenderConfirm2Button(boolean renderConfirm2Button) {
		this.renderConfirm2Button = renderConfirm2Button;
	}
	public List<TreeNode> getAllSelectedNodes() {
		return allSelectedNodes;
	}
	public void setAllSelectedNodes(List<TreeNode> allSelectedNodes) {
		this.allSelectedNodes = allSelectedNodes;
	}
	public MenuModel getMenuModel() {
		return menuModel;
	}
	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}
	public boolean isContextMenuDisabled() {
		return contextMenuDisabled;
	}
	public void setContextMenuDisabled(boolean contextMenuDisabled) {
		this.contextMenuDisabled = contextMenuDisabled;
	}
	public List<AuditEvent> getAuditEvents() {
		return auditEvents;
	}
	public void setAuditEvents(List<AuditEvent> auditEvents) {
		this.auditEvents = auditEvents;
	}
	public AuditEvent getSelectedAuditEvent() {
		return selectedAuditEvent;
	}
	public void setSelectedAuditEvent(AuditEvent selectedAuditEvent) {
		this.selectedAuditEvent = selectedAuditEvent;
	}
	public List<AuditEvent> getFilteredEvents() {
		return filteredEvents;
	}
	public void setFilteredEvents(List<AuditEvent> filteredEvents) {
		this.filteredEvents = filteredEvents;
	}
	public boolean isLinkedAccountButtonDisabled() {
		return linkedAccountButtonDisabled;
	}
	public void setLinkedAccountButtonDisabled(boolean linkedAccountButtonDisabled) {
		this.linkedAccountButtonDisabled = linkedAccountButtonDisabled;
	}
	public String getUsersIdInput() {
		return usersIdInput;
	}
	public void setUsersIdInput(String usersIdInput) {
		this.usersIdInput = usersIdInput;
	}


	public String getSelectedLinkOption() {
		return selectedLinkOption;
	}
	public void setSelectedLinkOption(String selectedLinkOption) {
		this.selectedLinkOption = selectedLinkOption;
	}
	public User getExistingLinkedUser() {
		return existingLinkedUser;
	}
	public void setExistingLinkedUser(User existingLinkedUser) {
		this.existingLinkedUser = existingLinkedUser;
	}
	public boolean isLinkRadioDisabled() {
		return linkRadioDisabled;
	}
	public void setLinkRadioDisabled(boolean linkRadioDisabled) {
		this.linkRadioDisabled = linkRadioDisabled;
	}
	public boolean isLinkRadioSubmitDisabled() {
		return linkRadioSubmitDisabled;
	}
	public void setLinkRadioSubmitDisabled(boolean linkRadioSubmitDisabled) {
		this.linkRadioSubmitDisabled = linkRadioSubmitDisabled;
	}
	public boolean isRefreshAccountButtonDisabled() {
		return refreshAccountButtonDisabled;
	}
	public void setRefreshAccountButtonDisabled(boolean refreshAccountButtonDisabled) {
		this.refreshAccountButtonDisabled = refreshAccountButtonDisabled;
	}
	public boolean isLookupByEmailButtonDisabled() {
		return lookupByEmailButtonDisabled;
	}
	public void setLookupByEmailButtonDisabled(boolean lookupByEmailButtonDisabled) {
		this.lookupByEmailButtonDisabled = lookupByEmailButtonDisabled;
	}
	public boolean isNavEntitiesButtonDisabled() {
		return navEntitiesButtonDisabled;
	}
	public void setNavEntitiesButtonDisabled(boolean navEntitiesButtonDisabled) {
		this.navEntitiesButtonDisabled = navEntitiesButtonDisabled;
	}
	public boolean isRenderTelephoneColumn() {
		return renderTelephoneColumn;
	}
	public void setRenderTelephoneColumn(boolean renderTelephoneColumn) {
		this.renderTelephoneColumn = renderTelephoneColumn;
	}
	public boolean isSelectSearchMethodRendered() {
		return selectSearchMethodRendered;
	}
	public void setSelectSearchMethodRendered(boolean selectSearchMethodRendered) {
		this.selectSearchMethodRendered = selectSearchMethodRendered;
	}
	public List<RealmFilter> getQueries() {
		return queries;
	}
	public void setQueries(List<RealmFilter> queries) {
		this.queries = queries;
	}
	public String getSelectedSearchAttr() {
		return selectedSearchAttr;
	}
	public void setSelectedSearchAttr(String selectedSearchAttr) {
		this.selectedSearchAttr = selectedSearchAttr;
	}
	public boolean isSelectCompanyRendered() {
		return selectCompanyRendered;
	}
	public void setSelectCompanyRendered(boolean selectCompanyRendered) {
		this.selectCompanyRendered = selectCompanyRendered;
	}
	public List<Company> getCompanies() {
		return companies;
	}
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}
	public String getSelectedCompany() {
		return selectedCompany;
	}
	public void setSelectedCompany(String selectedCompany) {
		this.selectedCompany = selectedCompany;
	}

	public Object[] getFilterParameters() {
		return filterParameters;
	}
	public void setFilterParameters(Object[] filterParameters) {
		this.filterParameters = filterParameters;
	}
	public String getEmailInput() {
		return emailInput;
	}
	public void setEmailInput(String emailInput) {
		this.emailInput = emailInput;
	}
	public boolean isEmailInputRendered() {
		return emailInputRendered;
	}
	public void setEmailInputRendered(boolean emailInputRendered) {
		this.emailInputRendered = emailInputRendered;
	}
	public boolean isInitRealmMetadataDisabled() {
		return initRealmMetadataDisabled;
	}
	public void setInitRealmMetadataDisabled(boolean initRealmMetadataDisabled) {
		this.initRealmMetadataDisabled = initRealmMetadataDisabled;
	}
	public boolean isUpdateRealmMetadataDisabled() {
		return updateRealmMetadataDisabled;
	}
	public void setUpdateRealmMetadataDisabled(boolean updateRealmMetadataDisabled) {
		this.updateRealmMetadataDisabled = updateRealmMetadataDisabled;
	}
	public boolean isRefreshMetadataButtonDisabled() {
		return refreshMetadataButtonDisabled;
	}
	public void setRefreshMetadataButtonDisabled(
			boolean refreshMetadataButtonDisabled) {
		this.refreshMetadataButtonDisabled = refreshMetadataButtonDisabled;
	}



	


}
