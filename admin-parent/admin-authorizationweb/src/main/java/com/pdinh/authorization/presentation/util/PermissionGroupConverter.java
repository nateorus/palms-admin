package com.pdinh.authorization.presentation.util;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.PermissionGroupDao;
import com.pdinh.auth.persistence.entity.PermissionGroup;

@ManagedBean(name = "permissionGroupConverter")
@ViewScoped
@FacesConverter(value = "permissionGroupConverter")
public class PermissionGroupConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PermissionGroupConverter.class);
	
	@EJB
	private PermissionGroupDao permissionGroupDao;
	
	public PermissionGroupConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		if ((arg2 != null) && (!(arg2.equals(String.valueOf(0)))) && (!(arg2.equals(""))) ){
			int pid = Integer.valueOf(arg2);
			log.trace("In getAsObject method of permissionGroupConverter.  Argument: {}", pid);
			//log.debug("PermissionDao {}", permissionDao.toString());
//			List <Permission> plist = permissionDao.findAll(true);
//			Iterator iter = plist.iterator();
//			while (iter.hasNext()){
//				Permission p = (Permission)iter.next();
//				log.debug("Iterating over all permissions {}", p.getName());
//				if (p.getPermission_id() == pid){
//					return p;
//				}
//			}
			PermissionGroup p = permissionGroupDao.findById(pid);
			log.debug("Returned PermissionGroup Object: {}", p.getName());
			return p;

		}else{
			log.debug("Returning Null from PermissionGroupConverter getAsObjectMethod");
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		if ((!(arg2.equals(null))) && arg2 instanceof PermissionGroup){
			String pid = "" + ((PermissionGroup) arg2).getPermission_group_id();
			log.trace("In getAsString method of permissionGroupConverter.  Argument: {}", pid);
			return pid;
		} else {
			log.debug("Returning Null from PermissionGroupConverter getAsString class");
			return null;
		}
		
	}

	public PermissionGroupDao getPermissionGroupDao() {
		return permissionGroupDao;
	}

	public void setPermissionGroupDao(PermissionGroupDao permissionGroupDao) {
		this.permissionGroupDao = permissionGroupDao;
	}

}
