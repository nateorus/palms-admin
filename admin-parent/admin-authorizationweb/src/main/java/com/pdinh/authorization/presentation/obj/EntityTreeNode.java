package com.pdinh.authorization.presentation.obj;

import java.io.Serializable;

public class EntityTreeNode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String entityName;
	private String entityType;
	private int entityId;
	private String sId;
	private int entityRuleId;
	private String entityRuleName;
	
	public EntityTreeNode(String entityName, String entityType, String sId, int entityId){
		setEntityName(entityName);
		setEntityType(entityType);
		setsId(sId);
		setEntityId(entityId);
	}
	public EntityTreeNode(String entityName, String entityType, String sId, int entityId, int entityRuleId, String entityRuleName){
		setEntityName(entityName);
		setEntityType(entityType);
		setsId(sId);
		setEntityId(entityId);
		setEntityRuleId(entityRuleId);
		setEntityRuleName(entityRuleName);
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public int getEntityRuleId() {
		return entityRuleId;
	}

	public void setEntityRuleId(int entityRuleId) {
		this.entityRuleId = entityRuleId;
	}
	public String getEntityRuleName() {
		return entityRuleName;
	}
	public void setEntityRuleName(String entityRuleName) {
		this.entityRuleName = entityRuleName;
	}
}
