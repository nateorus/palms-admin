package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import org.apache.shiro.SecurityUtils;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.dao.EntityRuleDao;
import com.pdinh.auth.data.dao.EntityTypeDao;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.RoleDao;
import com.pdinh.auth.data.dao.RoleTemplateDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.auth.persistence.entity.RoleTemplate;
import com.pdinh.authorization.presentation.util.RoleConverter;
import com.pdinh.authorization.presentation.view.ProjectTeamsViewBean;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.enterprise.managedbean.SessionBean;
import com.pdinh.persistence.ms.entity.Project;

@ManagedBean(name = "projectTeamsControllerBean")
@ViewScoped
public class ProjectTeamsControllerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ProjectTeamsControllerBean.class);
	
	@ManagedProperty(name = "projectTeamsViewBean", value = "#{projectTeamsViewBean}")
	private ProjectTeamsViewBean projectTeamsViewBean;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@EJB
	private PalmsRealmDao realmDao;
	
	@EJB
	private CompanyDao companyDao;
	
	@EJB
	private ProjectDao projectDao;
	
	@EJB
	private EntityRuleDao entityRuleDao;
	@EJB
	private EntityTypeDao entityTypeDao;
	
	@EJB
	private ExternalSubjectServiceLocal subjectService;
	
	@EJB 
	private EntityServiceLocal entityService;
	
	@EJB
	private RoleTemplateDao roleTemplateDao;
	
	@EJB
	private RoleDao roleDao;
	
	@ManagedProperty(name = "roleConverter", value="#{roleConverter}")
	private RoleConverter roleConverter;
	
	public void initView(){
		//do some initialization
		if (sessionBean.isAllowAllRealms()){
    		projectTeamsViewBean.setRealms(realmDao.findAll());
    	}else{
    		projectTeamsViewBean.setRealms(realmDao.findAllAuthorizedRealms(sessionBean.getAuthorizedRealmIds()));
    	}
		projectTeamsViewBean.setCompanies(companyDao.findAll());
	}
	
	public void handleRealmSelect(ValueChangeEvent e){
		log.debug("Handle Selecting a Realm here");
		projectTeamsViewBean.setSelectCompanyDisabled(false);
		projectTeamsViewBean.setSearchSubjectsRoleDisabled(false);
		log.debug("Selected Realm ID {}", projectTeamsViewBean.getSelectedRealmName());
		RoleTemplate template = realmDao.findById(Integer.valueOf((String)e.getNewValue())).getRoleTemplate();
		projectTeamsViewBean.setRoleTemplate(template);
	}
	
	public void handleCompanySelect(){
		log.debug("Handle selecting a company here");
		projectTeamsViewBean.setGetProjectsDisabled(false);
	}
	
	public void handleGetProjects(){
		log.debug("Handle getting projects here");
		List<Project> projects = projectDao.findAllByCompanyId(Integer.valueOf(projectTeamsViewBean.getSelectedCompanyId()));
		projectTeamsViewBean.setProjects(projects);
	}
	
	public void handleSubjectSelect(){
		log.debug("Handle Selecting subject here");
		projectTeamsViewBean.setSetTeamDisabled(false);
	}
	
	public void handleSetProjectTeam(){
		log.debug("Handle Setting the project team here");
		handleGetSubjects();
	}
	
	public void handleGetSubjects(){
    	log.debug("Handle Getting Subjects from selected Realm here");
    	
    	int realm_id = Integer.valueOf(projectTeamsViewBean.getSelectedRealmName());
    	log.debug("Realm ID Selected: {}", realm_id);
    	PalmsRealm theRealm = realmDao.findById(realm_id); 
    	
    	List<ExternalSubjectDataObj> userobjs = new ArrayList<ExternalSubjectDataObj>();
    	List<ExternalSubjectDataObj> teammbrs = new ArrayList<ExternalSubjectDataObj>();
    	userobjs = subjectService.getExternalSubjects(theRealm, "", false);
    	
    	teammbrs = subjectService.getSubjectsWithEntity(theRealm, projectTeamsViewBean.getSelectedProject().getProjectId(), EntityType.PROJECT_TYPE);
    	
    	projectTeamsViewBean.setRealmUsers(userobjs);
    	projectTeamsViewBean.setCurrentTeamMembers(teammbrs);
    	//manageSubjectsViewBean.setSubjects(subjectDao.findSubjectsByRealmId(realm_id));
    	log.debug("Returned {} Subjects from Realm", userobjs.size());
    }
	
	public void handleMemberSelect(){
		projectTeamsViewBean.setRemoveMemberDisabled(false);
	}
	public void handleRemoveMember(){
		ExternalSubjectDataObj user = projectTeamsViewBean.getSelectedCurrentUser();
		Project project = projectTeamsViewBean.getSelectedProject();
		String currentUser = SecurityUtils.getSubject().getPrincipal().toString();
		RoleContext context = subjectService.getDefaultRoleContext(user.getUsername(), Integer.valueOf(projectTeamsViewBean.getSelectedRealmName()), user.getEntryDN(), user.getExternal_id());
		context = entityService.revokeEntityAccess(context, project.getProjectId(), EntityType.PROJECT_TYPE, currentUser);
		List<ExternalSubjectDataObj> currentMembers = projectTeamsViewBean.getCurrentTeamMembers();
		for (ExternalSubjectDataObj o: currentMembers){
			if (o.getUsername().equalsIgnoreCase(user.getUsername())){
				currentMembers.remove(o);
				break;
			}
		}
		projectTeamsViewBean.setCurrentTeamMembers(currentMembers);
	}
	
	    
    public String handleFlowProcessEvent(FlowEvent event){
    	log.debug("Current wizard Step: {}", event.getOldStep());
    	log.debug("Next step: {}", event.getNewStep());
    	if (event.getOldStep().equalsIgnoreCase("currentTeamMembers")){
    		List<ExternalSubjectDataObj> currentUsers = projectTeamsViewBean.getCurrentTeamMembers();
    		List<ExternalSubjectDataObj> availableUsers = projectTeamsViewBean.getRealmUsers();
    		for (ExternalSubjectDataObj c:currentUsers){
    			for (ExternalSubjectDataObj a:availableUsers){
    				if (c.getUsername().equalsIgnoreCase(a.getUsername())){
    					availableUsers.remove(a);
    					break;
    				}
    			}
    		}
    		projectTeamsViewBean.setRealmUsers(availableUsers);
    	}
    	if (event.getNewStep().equalsIgnoreCase("selectTeamRoles")){
    		
    		RoleTemplate template = projectTeamsViewBean.getRoleTemplate();
    		List <Role> sourceRoles = template.getAllowedRoles();
    		List <Role> targetRoles = new ArrayList<Role>();
    		projectTeamsViewBean.setContextRoleLists(new DualListModel<Role>(sourceRoles, targetRoles));
    		projectTeamsViewBean.getSelectedUsers().addAll(projectTeamsViewBean.getCurrentTeamMembers());
    	}
    	if (event.getOldStep().equalsIgnoreCase("selectRolesTab")){
    		//manageSubjectsViewBean.getSelectedContext().setRoles(manageSubjectsViewBean.getContextRoleLists().getTarget());
    	}
    	return event.getNewStep();
    }
	    
	public void handleSelectTeamMember(){
		projectTeamsViewBean.setPickRolesDisabled(false);
		ExternalSubjectDataObj teamMember = projectTeamsViewBean.getSelectedUser();
		PalmsRealm realm = realmDao.findById(Integer.valueOf(projectTeamsViewBean.getSelectedRealmName()));
		Project project = projectTeamsViewBean.getSelectedProject();
		EntityRule rule = entityRuleDao.findEntityRuleByName(EntityRule.ALLOW);
		EntityType type = entityTypeDao.findEntityTypeByCode(EntityType.PROJECT_TYPE);
		RoleContext context = subjectService.getDefaultRoleContext(teamMember.getUsername(), realm.getRealm_id(), teamMember.getEntryDN(), teamMember.getExternal_id());
		AuthorizedEntity entity = entityService.getAuthorizedEntity(context.getRole_context_id(), type.getEntity_type_id(), project.getProjectId());
		if (entity == null){
			entity = new AuthorizedEntity();
			entity.setEntity_id(project.getProjectId());
			entity.setEntity_type_rule(EntityRule.ALLOW);
			entity.setEntityRule(rule);
			entity.setEntityType(type);
			entity.setRoleContext(context);
			entity.setSubject_id(context.getSubject().getSubject_id());
			projectTeamsViewBean.setSelectedUserProjectEntity(entity);
			projectTeamsViewBean.setContextRoleLists(new DualListModel(realm.getRoleTemplate().getAllowedRoles(), new ArrayList<Role>()));
		} else {
			projectTeamsViewBean.setSelectedUserProjectEntity(entity);
			projectTeamsViewBean.setContextRoleLists(new DualListModel(realm.getRoleTemplate().getAllowedRoles(), entity.getRoles()));
		}
	}
	
	public void handleTransferRole(TransferEvent event){
		String currentUsername = SecurityUtils.getSubject().getPrincipal().toString();
		if (event.isAdd()){
			ExternalSubjectDataObj teamMember = projectTeamsViewBean.getSelectedUser();
			
			AuthorizedEntity entity = projectTeamsViewBean.getSelectedUserProjectEntity();
			if (entity.getRoles() == null){
				entity.setRoles(new ArrayList<Role>());
			}
			List<Role> roles = (List<Role>)event.getItems();
			for (Role r:roles){
				log.debug("Event Role: {}", r.getName());
			}
			entity.getRoles().addAll((List<Role>)event.getItems());
			RoleContext context = subjectService.getDefaultRoleContext(teamMember.getUsername(), Integer.valueOf(projectTeamsViewBean.getSelectedRealmName()), teamMember.getEntryDN(), teamMember.getExternal_id());
			entityService.grantEntityAccess(context.getRole_context_id(), entity, currentUsername);
			
		}else if (event.isRemove()){
			ExternalSubjectDataObj teamMember = projectTeamsViewBean.getSelectedUser();
			RoleContext context = subjectService.getDefaultRoleContext(teamMember.getUsername(), Integer.valueOf(projectTeamsViewBean.getSelectedRealmName()), teamMember.getEntryDN(), teamMember.getExternal_id());
			AuthorizedEntity entity = projectTeamsViewBean.getSelectedUserProjectEntity();
			List<Role> roles = (List<Role>)event.getItems();
			//Remove all doesn't work.. have to do this crap.  fix me
			List<Integer> removeIndexes = new ArrayList<Integer>();
			for (Role r: roles){
				List<Role> entityRoles = entity.getRoles();
				
				for (int i=0;i<entityRoles.size();i++){
					Role role = entityRoles.get(i);
					if (role.getRole_id() == r.getRole_id()){
						removeIndexes.add(i);
					}
				}
				
			}
			for (Integer i:removeIndexes){
				log.debug("Removing Role: {}", entity.getRoles().get(i).getName());
				entity.getRoles().remove(entity.getRoles().get(i));
				
			}
			
			if (entity.getRoles().isEmpty()){
				//no more roles.  remove access to entity
				context = entityService.revokeEntityAccess(context, entity.getEntity_id(), entity.getEntityType().getEntity_type_code(), currentUsername);
			}else{
				//still some roles left.  update the existing entity with the new role collection.
				context = entityService.grantEntityAccess(context.getRole_context_id(), entity, currentUsername);
			}
		}
		
	}
	
	public void handleSearchSubjectsRole(){
		PalmsRealm realm = realmDao.findById(Integer.valueOf(projectTeamsViewBean.getSelectedRealmName()));
		Role role = roleDao.findRoleByName("ScorecardAdmin");
		
		subjectService.getSubjectsWithRole(realm, role, false);
	}

	public ProjectTeamsViewBean getProjectTeamsViewBean() {
		return projectTeamsViewBean;
	}

	public void setProjectTeamsViewBean(ProjectTeamsViewBean projectTeamsViewBean) {
		this.projectTeamsViewBean = projectTeamsViewBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public RoleTemplateDao getRoleTemplateDao() {
		return roleTemplateDao;
	}

	public void setRoleTemplateDao(RoleTemplateDao roleTemplateDao) {
		this.roleTemplateDao = roleTemplateDao;
	}

	public RoleConverter getRoleConverter() {
		return roleConverter;
	}

	public void setRoleConverter(RoleConverter roleConverter) {
		this.roleConverter = roleConverter;
	}

	public EntityServiceLocal getEntityService() {
		return entityService;
	}

	public void setEntityService(EntityServiceLocal entityService) {
		this.entityService = entityService;
	}

}
