package com.pdinh.authorization.presentation.filter;

import java.io.IOException;
import java.security.cert.CertificateException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.onelogin.AccountSettings;
import com.onelogin.saml.Response;
import com.pdinh.enterprise.realm.KfyOneLoginSamlToken;

/**
 * Servlet Filter implementation class PortalOneLoginFilter
 */

public class OneLoginConsumerFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(OneLoginConsumerFilter.class);
    /**
     * Default constructor. 
     */
    public OneLoginConsumerFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse resp = (HttpServletResponse) response;
		
		String certificateS ="MIIEHjCCAwagAwIBAgIBATANBgkqhkiG9w0BAQUFADBnMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEVMBMGA1UEBwwMU2FudGEgTW9uaWNhMREwDwYDVQQKDAhPbmVMb2dpbjEZMBcGA1UEAwwQYXBwLm9uZWxvZ2luLmNvbTAeFw0xMjEyMTMxOTU3MThaFw0xNzEyMTMxOTU3MThaMGcxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMRUwEwYDVQQHDAxTYW50YSBNb25pY2ExETAPBgNVBAoMCE9uZUxvZ2luMRkwFwYDVQQDDBBhcHAub25lbG9naW4uY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzPNdmgmo56M+oOfDrSUnbp1o89KZivSD1dgvFjoDF5FS4aSwNL4HZflj+/alSCJpH7gCkHHmg6i3UUqg9wttAsBoYtjjdAH3ED87r7VsVkg7FcetdyGE+cmtoNalILe9iEPLRTCHw1bOzh/Vl5cZTXtTd/vayROWArQbj3F59PFs/2tsE5ML0pvd2ndZVRnobpd+wv3mWXcpqGtudF0X1qdtYohUKKxWC+JXHowOc/8eioK6vCy9lLhizlKIzkLY0v0ipKtoZvqnGs/U1emchPNvNAgEbEilpWUcJi2rZ7PuDvxwHy3Hr8rxL0LmLKEOAr488Q334gornsp5PMXL8wIDAQABo4HUMIHRMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFFLXS+yH9zxWG19mVTtPrMLc7TWzMIGRBgNVHSMEgYkwgYaAFFLXS+yH9zxWG19mVTtPrMLc7TWzoWukaTBnMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEVMBMGA1UEBwwMU2FudGEgTW9uaWNhMREwDwYDVQQKDAhPbmVMb2dpbjEZMBcGA1UEAwwQYXBwLm9uZWxvZ2luLmNvbYIBATAOBgNVHQ8BAf8EBAMCBPAwDQYJKoZIhvcNAQEFBQADggEBACw4PZURRaJKSaSFeUUSt+lWxA1IAUqWKsGI/aeinQnz+3pDJZiewut5lPYd+6pd0kVy45jduGkiVEmdNg2uuMoLuttal4WZKT6RoukEIg5J70nixC8Nj9Ip6Z22KkvlwB9H/lwFfMLI5oOGJjWMK1ugFIxourHUXkPBAhZ5qwD+0wHNclamezsMq0KqopGsFAbeOv591mbukePj4Z6W5bYI+A/g+heywalh5+yCNkwEl+1Pfn+HZR3o/lg1x1kQa4FqLCDhUhIcTmmHdc4pesxIZvCbntGKHfawe+leQYXkC0XiJ/2GLTvDG2JCussxb5y/HaZdRzvJPlxrEvoGKj4=";

		  // user account specific settings. Import the certificate here
		  AccountSettings accountSettings = new AccountSettings();
		  accountSettings.setCertificate(certificateS);
	
		  Response samlResponse = null;
		try {
			samlResponse = new Response(accountSettings);
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  try {
			  //log.debug("Got SAML Response: {}", request.getParameter("SAMLResponse"));
			samlResponse.loadXmlFromBase64(request.getParameter("SAMLResponse"));
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 Subject subject = SecurityUtils.getSubject();
		 Session session = subject.getSession();
		 
		 log.debug("Shiro Session Id: {}", subject.getSession().getId());
		 for (Object obj : subject.getSession().getAttributeKeys()){
			 log.debug("Found session attribute with Key: {} and value {}", obj, session.getAttribute(obj));
		 }

		 try{
			 //subject.logout(); -- do this in audience filter
			 subject.login(new KfyOneLoginSamlToken(samlResponse));
		 }catch (AuthenticationException ae){
			 
			 log.debug("Authentication failed due to: {}", ae.getMessage());
			 resp.sendRedirect("/authorization/faces/login.xhtml");
		 }

		 if (subject.isAuthenticated()){
			 log.debug("SSO Succeeded");
			 resp.sendRedirect("/authorization/faces/dashboard.xhtml");
		 }else{
			 resp.sendRedirect("/authorization/faces/login.xhtml");
		 }
		//chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
