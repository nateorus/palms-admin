package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.faces.bean.ManagedBean;

import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleGroup;
import com.pdinh.auth.persistence.entity.RoleTemplate;

/**
 * Session Bean implementation class DashboardViewBean
 */
@ManagedBean(name = "manageTemplatesViewBean")
@ViewScoped
public class ManageTemplatesViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String uid;

	private List<Role> roles;
	private List<RoleTemplate> roleTemplates;
	private List<RoleGroup> roleGroups = new ArrayList<RoleGroup>();
	private List<EntityType> entityTypes = new ArrayList<EntityType>();

	private RoleTemplate newRoleTemplate = new RoleTemplate();
	private RoleGroup newRoleGroup = new RoleGroup();
	private RoleTemplate selectedRoleTemplate = new RoleTemplate();
	private DualListModel<Role> createRoleTemplateLists;
	private DualListModel<Role> createDefaultRoleTemplateLists;
	private DualListModel<Role> createRoleGroupLists;
	private DualListModel<EntityType> entityTypeLists;
	private boolean editDisabled = true;
	private boolean deleteDisabled = true;
	private boolean renderNameInputNew = true;
	private boolean renderNameInputEdit = false;
	private boolean renderSaveNewButton = true;
	private boolean renderSaveEditButton = false;
	private boolean createButtonDisabled = true;
	private boolean roleGroupsButtonDisabled = true;
	private boolean deleteGroupButtonRendered = false;
	
	private String selectedRoleGroupId = "";
	private String roleGroupDescription = "";
    /**
     * Default constructor. 
     */
    public ManageTemplatesViewBean() {
        // TODO Auto-generated constructor stub
    }
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


	
	public boolean isEditDisabled() {
		return editDisabled;
	}
	public void setEditDisabled(boolean editDisabled) {
		this.editDisabled = editDisabled;
	}
	public boolean isDeleteDisabled() {
		return deleteDisabled;
	}
	public void setDeleteDisabled(boolean deleteDisabled) {
		this.deleteDisabled = deleteDisabled;
	}
	public List<RoleTemplate> getRoleTemplates() {
		return roleTemplates;
	}
	public void setRoleTemplates(List<RoleTemplate> roleTemplates) {
		this.roleTemplates = roleTemplates;
	}
	public RoleTemplate getNewRoleTemplate() {
		return newRoleTemplate;
	}
	public void setNewRoleTemplate(RoleTemplate newRoleTemplate) {
		this.newRoleTemplate = newRoleTemplate;
	}
	public RoleTemplate getSelectedRoleTemplate() {
		return selectedRoleTemplate;
	}
	public void setSelectedRoleTemplate(RoleTemplate selectedRoleTemplate) {
		this.selectedRoleTemplate = selectedRoleTemplate;
	}
	public DualListModel<Role> getCreateRoleTemplateLists() {
		return createRoleTemplateLists;
	}
	public void setCreateRoleTemplateLists(DualListModel<Role> createRoleTemplateLists) {
		this.createRoleTemplateLists = createRoleTemplateLists;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public DualListModel<Role> getCreateDefaultRoleTemplateLists() {
		return createDefaultRoleTemplateLists;
	}
	public void setCreateDefaultRoleTemplateLists(DualListModel<Role> createDefaultRoleTemplateLists) {
		this.createDefaultRoleTemplateLists = createDefaultRoleTemplateLists;
	}
	public boolean isRenderNameInputNew() {
		return renderNameInputNew;
	}
	public void setRenderNameInputNew(boolean renderNameInputNew) {
		this.renderNameInputNew = renderNameInputNew;
	}
	public boolean isRenderNameInputEdit() {
		return renderNameInputEdit;
	}
	public void setRenderNameInputEdit(boolean renderNameInputEdit) {
		this.renderNameInputEdit = renderNameInputEdit;
	}
	public boolean isRenderSaveNewButton() {
		return renderSaveNewButton;
	}
	public void setRenderSaveNewButton(boolean renderSaveNewButton) {
		this.renderSaveNewButton = renderSaveNewButton;
	}
	public boolean isRenderSaveEditButton() {
		return renderSaveEditButton;
	}
	public void setRenderSaveEditButton(boolean renderSaveEditButton) {
		this.renderSaveEditButton = renderSaveEditButton;
	}
	public boolean isCreateButtonDisabled() {
		return createButtonDisabled;
	}
	public void setCreateButtonDisabled(boolean createButtonDisabled) {
		this.createButtonDisabled = createButtonDisabled;
	}
	public List<RoleGroup> getRoleGroups() {
		return roleGroups;
	}
	public void setRoleGroups(List<RoleGroup> roleGroups) {
		this.roleGroups = roleGroups;
	}
	public boolean isRoleGroupsButtonDisabled() {
		return roleGroupsButtonDisabled;
	}
	public void setRoleGroupsButtonDisabled(boolean roleGroupsButtonDisabled) {
		this.roleGroupsButtonDisabled = roleGroupsButtonDisabled;
	}
	public String getSelectedRoleGroupId() {
		return selectedRoleGroupId;
	}
	public void setSelectedRoleGroupId(String selectedRoleGroupId) {
		this.selectedRoleGroupId = selectedRoleGroupId;
	}
	public DualListModel<Role> getCreateRoleGroupLists() {
		return createRoleGroupLists;
	}
	public void setCreateRoleGroupLists(DualListModel<Role> createRoleGroupLists) {
		this.createRoleGroupLists = createRoleGroupLists;
	}
	public RoleGroup getNewRoleGroup() {
		return newRoleGroup;
	}
	public void setNewRoleGroup(RoleGroup newRoleGroup) {
		this.newRoleGroup = newRoleGroup;
	}
	public String getRoleGroupDescription() {
		return roleGroupDescription;
	}
	public void setRoleGroupDescription(String roleGroupDescription) {
		this.roleGroupDescription = roleGroupDescription;
	}
	public List<EntityType> getEntityTypes() {
		return entityTypes;
	}
	public void setEntityTypes(List<EntityType> entityTypes) {
		this.entityTypes = entityTypes;
	}
	public DualListModel<EntityType> getEntityTypeLists() {
		return entityTypeLists;
	}
	public void setEntityTypeLists(DualListModel<EntityType> entityTypeLists) {
		this.entityTypeLists = entityTypeLists;
	}
	public boolean isDeleteGroupButtonRendered() {
		return deleteGroupButtonRendered;
	}
	public void setDeleteGroupButtonRendered(boolean deleteGroupButtonRendered) {
		this.deleteGroupButtonRendered = deleteGroupButtonRendered;
	}

}
