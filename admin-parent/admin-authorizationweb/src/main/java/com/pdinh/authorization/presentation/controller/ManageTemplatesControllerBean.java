package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.SessionBean;

import com.pdinh.authorization.presentation.util.EntityTypeConverter;
import com.pdinh.authorization.presentation.util.RoleConverter;
import com.pdinh.authorization.presentation.view.ManageTemplatesViewBean;

import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.EntityTypeDao;
import com.pdinh.auth.data.dao.RoleDao;
import com.pdinh.auth.data.dao.RoleGroupDao;
import com.pdinh.auth.data.dao.RoleTemplateDao;
import com.pdinh.auth.data.dao.RoleTypeDao;

import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleGroup;
import com.pdinh.auth.persistence.entity.RoleTemplate;

/**
 * Session Bean implementation class ManageRolesControllerBean
 */
@ManagedBean(name = "manageTemplatesControllerBean")
@ViewScoped
public class ManageTemplatesControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(ManageRolesControllerBean.class);
	
	//private Subject subject;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(name = "manageTemplatesViewBean", value = "#{manageTemplatesViewBean}")
	private ManageTemplatesViewBean manageTemplatesViewBean;
	
//	@ManagedProperty(name = "roleNameValidator", value = "#{roleNameValidator}")
//	private RoleNameValidator roleNameValidator;
	
	@ManagedProperty(name = "roleConverter", value="#{roleConverter}")
	private RoleConverter roleConverter;
	
	@ManagedProperty(name = "entityTypeConverter", value = "#{entityTypeConverter}")
	private EntityTypeConverter entityTypeConverter;
	
	@EJB
	private RoleDao roleDao;
	
	@EJB
	private RoleTemplateDao roleTemplateDao;
	
	@EJB
	private RoleTypeDao roleTypeDao;
	
	@EJB
	private PermissionServiceLocal permissionService;
	
	@EJB
	private RoleGroupDao roleGroupDao;
	
	@EJB
	private EntityTypeDao entityTypeDao;
	
    public ManageTemplatesControllerBean() {
        // TODO Auto-generated constructor stub
    }
    public void initView(){
    	//Use this for checking permissions
    	Subject subject = SecurityUtils.getSubject();
    	
    	manageTemplatesViewBean.setRoleTemplates(roleTemplateDao.findAll(true));
    	manageTemplatesViewBean.setRoles(roleDao.findAll(true));
    	manageTemplatesViewBean.setEntityTypes(entityTypeDao.findAll());
    	
    	if (manageTemplatesViewBean.getCreateDefaultRoleTemplateLists() == null){
    		handleClearSelectedRoles();
    	}
    	if (subject.isPermitted("createRoleTemplates")){
    		manageTemplatesViewBean.setCreateButtonDisabled(false);
    	}
    	    	
    	
    }

    public void handleRoleTemplateSelect(SelectEvent event){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("Handle Selecting a Role here");
    	if (subject.isPermitted("deleteRoleTemplates")){
    		manageTemplatesViewBean.setDeleteDisabled(false);
    	}
    	if (subject.isPermitted("editRoleTemplates")){
    		manageTemplatesViewBean.setEditDisabled(false);
    	}
    	manageTemplatesViewBean.setRoleGroupsButtonDisabled(false);
    	
    	
    	
    	//PermissionGroup pgroup = dashboardViewBean.getSelectedGroup();
    	//log.debug("selected group: {}", pgroup.getName());
    }
    
    public void handleRoleGroupsInit(){
    	manageTemplatesViewBean.setRoleGroups(manageTemplatesViewBean.getSelectedRoleTemplate().getRoleGroups());
    }



    public void handleSetNewRoleTemplateAllowedRoles(){
    	manageTemplatesViewBean.getNewRoleTemplate().setAllowedRoles(manageTemplatesViewBean.getCreateRoleTemplateLists().getTarget());
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Allowed Roles Saved"));
    }
    public void handleSetNewRoleTemplateDefaultRoles(){
    	manageTemplatesViewBean.getNewRoleTemplate().setDefaultRoles(manageTemplatesViewBean.getCreateDefaultRoleTemplateLists().getTarget());
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Default Roles Saved"));
    }
    public void handleSetDefaultRoles(){
    	log.debug("Handle Setting the default role collection here");
    }
    public void handleNewRoleTemplateSetup(){
    	log.debug("Handle Setting new RoleTemplate with user input here");
    	RoleTemplate roleTemplate = manageTemplatesViewBean.getNewRoleTemplate();
    	roleTemplate.setCreated_by(sessionBean.getUid());
    	
    	List <Role> rList = manageTemplatesViewBean.getCreateRoleTemplateLists().getTarget();
    	List <Role> defaultRoleList = manageTemplatesViewBean.getCreateDefaultRoleTemplateLists().getTarget();
    	
    	if (rList.isEmpty()){
    		log.debug("the target List was empty... no permission groups added");
    	}else{
    		log.debug("the target List contains {} items", rList.size());
    		roleTemplate.setAllowedRoles(rList);
    		roleTemplate.setDefaultRoles(defaultRoleList);
    		//All default roles must be in the allowed roles
    		for (Role r : defaultRoleList){
    			if (!(containsRole(r.getName(), rList))){
    				roleTemplate.getAllowedRoles().add(r);
    			}
    		}
    		
    	}
    }
    private boolean containsRole(String roleName, List<Role> roles){
    	for (Role r : roles){
    		if (r.getName().equals(roleName)){
    			return true;
    		}
    	}
    	return false;
    }
    public void handleSubmitNewRoleTemplate(){
    	log.debug("Handle Persistence of a new Role Template here");
    	
    	RoleTemplate roleTemplate = manageTemplatesViewBean.getNewRoleTemplate();
    	
    	log.debug("Name: {}", roleTemplate.getRole_template_name());
     	log.debug("Description: {}", roleTemplate.getRole_template_description());
    	log.debug("ID: {}", roleTemplate.getRole_template_id());
    	
    	roleTemplateDao.create(roleTemplate);
    	manageTemplatesViewBean.setRoleTemplates(roleTemplateDao.findAll(true));
    	//handleClearSelectedRoles();
    	//reset 
    	//manageTemplatesViewBean.setNewRoleTemplate(new RoleTemplate());
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("New Role Template Created", roleTemplate.getRole_template_name()));
    	
    	
    }
    public void handleClearSelectedRoles(){
    	manageTemplatesViewBean.setCreateRoleTemplateLists(new DualListModel<Role>(manageTemplatesViewBean.getRoles(), new ArrayList<Role>()));
    	manageTemplatesViewBean.setCreateDefaultRoleTemplateLists(new DualListModel<Role>(manageTemplatesViewBean.getNewRoleTemplate().getAllowedRoles(), new ArrayList<Role>()));
    	
    }
    public void handleDeleteRoleTemplate(){
    	RoleTemplate roleTemplate = manageTemplatesViewBean.getSelectedRoleTemplate();
    	log.debug("Attempting to Delete Role Template with ID: {}", roleTemplate.getRole_template_id());
    	if (permissionService.canDeleteRoleTemplate(roleTemplate.getRole_template_id())){
    		roleTemplateDao.delete(roleTemplate);
    		manageTemplatesViewBean.setRoleTemplates(roleTemplateDao.findAll(true));
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage("Role Template Deleted", roleTemplate.getRole_template_name()));
    	}else{
    		FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage("Failed to Delete Role Template", roleTemplate.getRole_template_name() + " is used by a Realm"));
    	}
    	
    	
    }
    public String handleFlowProcessEvent(FlowEvent event){
    	log.debug("Current wizard Step: {}", event.getOldStep());
    	log.debug("Next step: {}", event.getNewStep());

    	if (event.getOldStep().equalsIgnoreCase("roleTemplateDetailsTab")){
    		handleNewRoleTemplateSetup();
    	}
    	if (event.getOldStep().equalsIgnoreCase("selectRolesTab") && event.getNewStep().equalsIgnoreCase("selectDefaultRolesTab")){
    		if (manageTemplatesViewBean.getCreateRoleTemplateLists().getTarget().size() == 0){
    			FacesContext context = FacesContext.getCurrentInstance();
    	    	context.addMessage(null, new FacesMessage("Allowed Roles not selected or saved", "Must select at least 1 allowed role"));
    	    	return event.getOldStep();
    		} else {
    			List<Role> allowed = manageTemplatesViewBean.getCreateRoleTemplateLists().getTarget();
    			List<Role> defaultRoles = manageTemplatesViewBean.getNewRoleTemplate().getDefaultRoles();
    			if (!(defaultRoles == null)){
	    			for (Role r: defaultRoles){
	    	    		allowed = removeRole(allowed, r.getRole_id());
	    	    	}
    			}
    			manageTemplatesViewBean.getCreateDefaultRoleTemplateLists().setSource(allowed);
    			return event.getNewStep();
    		}
    			
    	}
    	return event.getNewStep();
    }
    public String handleFlowProcessEventRoleGroups(FlowEvent event){
    	log.debug("Current wizard Step: {}", event.getOldStep());
    	if (event.getOldStep().equalsIgnoreCase("selectTab")){
    		DualListModel <Role> newRoleGroupLists = new DualListModel<Role>();
    		DualListModel <EntityType> entityTypeLists = new DualListModel<EntityType>();
    		if (manageTemplatesViewBean.getSelectedRoleGroupId() == null || manageTemplatesViewBean.getSelectedRoleGroupId().isEmpty()){
    			log.debug("Selected Role Group ID is empty or null.  Creating new");
    			
    			newRoleGroupLists.setSource(manageTemplatesViewBean.getSelectedRoleTemplate().getAllowedRoles());
    			newRoleGroupLists.setTarget(new ArrayList<Role>());
    			entityTypeLists.setSource(manageTemplatesViewBean.getEntityTypes());
        		entityTypeLists.setTarget(new ArrayList<EntityType>());
    			manageTemplatesViewBean.setNewRoleGroup(new RoleGroup());
    		}else{
    			log.debug("Selected Role Group Id: {}", manageTemplatesViewBean.getSelectedRoleGroupId());
    			
    			List<Role> availableRoles = manageTemplatesViewBean.getSelectedRoleTemplate().getAllowedRoles();
    			RoleGroup selectedGroup = roleGroupDao.findById(Integer.valueOf(manageTemplatesViewBean.getSelectedRoleGroupId()));
    			for (Role role: selectedGroup.getRoles()){
    				availableRoles = removeRole(availableRoles, role.getRole_id());
    			}
    			newRoleGroupLists.setSource(availableRoles);
    			newRoleGroupLists.setTarget(selectedGroup.getRoles());
    			
    			List<EntityType> availableTypes = manageTemplatesViewBean.getEntityTypes();
    			for (EntityType type: selectedGroup.getEntityTypes()){
    				availableTypes = removeType(availableTypes, type.getEntity_type_id());
    			}
    			entityTypeLists.setSource(availableTypes);
    			entityTypeLists.setTarget(selectedGroup.getEntityTypes());
    			manageTemplatesViewBean.setNewRoleGroup(selectedGroup);
    			
    		}
    		
    		
    		manageTemplatesViewBean.setEntityTypeLists(entityTypeLists);
    		manageTemplatesViewBean.setCreateRoleGroupLists(newRoleGroupLists);
    	}
    	return event.getNewStep();
    }
    private List<Role> removeRole(List<Role> roles, Integer roleId){
    	for (Role r: roles){
    		if (r.getRole_id() == roleId){
    			roles.remove(r);
    			return roles;
    		}
    	}
    	return roles;
    }
    private List<EntityType> removeType(List<EntityType> types, Integer typeId){
    	for (EntityType t: types){
    		if (t.getEntity_type_id() == typeId){
    			types.remove(t);
    			return types;
    		}
    	}
    	return types;
    }
    
    public void handleEditRoleTemplate(){
    	log.debug("handle editing role template here");
    	RoleTemplate selectedTemplate = manageTemplatesViewBean.getSelectedRoleTemplate();
    	manageTemplatesViewBean.setNewRoleTemplate(selectedTemplate);
    	List<Role> availableRoles = manageTemplatesViewBean.getRoles();
    	for (Role r: selectedTemplate.getAllowedRoles()){
    		availableRoles = removeRole(availableRoles, r.getRole_id());
    	}
    	manageTemplatesViewBean.setCreateRoleTemplateLists(new DualListModel<Role>(availableRoles, selectedTemplate.getAllowedRoles()));
    	List<Role> allowedRoles = selectedTemplate.getAllowedRoles();
    	
    	manageTemplatesViewBean.setCreateDefaultRoleTemplateLists(new DualListModel<Role>(allowedRoles, selectedTemplate.getDefaultRoles()));
    	manageTemplatesViewBean.setRenderNameInputEdit(true);
    	manageTemplatesViewBean.setRenderNameInputNew(false);
    	manageTemplatesViewBean.setRenderSaveNewButton(false);
    	manageTemplatesViewBean.setRenderSaveEditButton(true);
    }
    
    public void handleCreateRoleTemplate(){
    	manageTemplatesViewBean.setCreateRoleTemplateLists(new DualListModel<Role>(manageTemplatesViewBean.getRoles(), new ArrayList<Role>()));
    	manageTemplatesViewBean.setCreateDefaultRoleTemplateLists(new DualListModel<Role>(manageTemplatesViewBean.getNewRoleTemplate().getAllowedRoles(), new ArrayList<Role>()));
    	manageTemplatesViewBean.setRenderNameInputEdit(false);
    	manageTemplatesViewBean.setRenderNameInputNew(true);
    	manageTemplatesViewBean.setNewRoleTemplate(new RoleTemplate());
    	manageTemplatesViewBean.setRenderSaveNewButton(true);
    	manageTemplatesViewBean.setRenderSaveEditButton(false);
    }
    public void handleCommitEditRoleTemplate(){
    	RoleTemplate roleTemplate = manageTemplatesViewBean.getNewRoleTemplate();
    	roleTemplateDao.update(roleTemplate);
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Saved Edits to Role Template", roleTemplate.getRole_template_name()));
    }
    public void handleSubmitNewRoleGroup(){
    	RoleGroup group = manageTemplatesViewBean.getNewRoleGroup();
    	group.setRoles(manageTemplatesViewBean.getCreateRoleGroupLists().getTarget());
    	group.setEntityTypes(manageTemplatesViewBean.getEntityTypeLists().getTarget());
    	if (group.getRoleGroupId() < 1){
    		log.debug("No ID, so use create: {}", group.getRoleGroupId());
	    	group.setRoleTemplate(manageTemplatesViewBean.getSelectedRoleTemplate());
	    	roleGroupDao.create(group);
    	}else{
    		log.debug("Found ID {}, use update", group.getRoleGroupId());
    		roleGroupDao.update(group);
    	}
    }
    public void handleSelectRoleGroup(ValueChangeEvent e){
    	log.debug("New Selected Role Group ID: {}", e.getNewValue());
    	if (e.getNewValue() == null || ((String)e.getNewValue()).equalsIgnoreCase("")){
    		log.debug("Selected Create New option...");
    		manageTemplatesViewBean.setDeleteGroupButtonRendered(false);
    		manageTemplatesViewBean.setRoleGroupDescription("Click Next to create new Role Group");
    	}else{
    		RoleGroup roleGroup = roleGroupDao.findById(Integer.valueOf((String)e.getNewValue()));
    		manageTemplatesViewBean.setRoleGroupDescription(roleGroup.getRoleGroupLabel() + " (" + roleGroup.getRoleGroupName() + ") " + roleGroup.getRoleGroupDescription());
    		manageTemplatesViewBean.setDeleteGroupButtonRendered(true);
    	}
    }
    public void deleteRoleGroup(){
    	log.debug("In deleteRoleGroup()");
    	RoleGroup group = roleGroupDao.findById(Integer.valueOf(manageTemplatesViewBean.getSelectedRoleGroupId()));
    	String groupName = group.getRoleGroupName();
    	roleGroupDao.delete(group);
    	handleRoleGroupsInit();
    	log.debug("finished deleting roleGroup");
    	manageTemplatesViewBean.setSelectedRoleGroupId("");
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Deleted Role Group", groupName));
    }
    public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public RoleTypeDao getRoleTypeDao() {
		return roleTypeDao;
	}
	public void setRoleTypeDao(RoleTypeDao roleTypeDao) {
		this.roleTypeDao = roleTypeDao;
	}
	public ManageTemplatesViewBean getManageTemplatesViewBean() {
		return manageTemplatesViewBean;
	}
	public void setManageTemplatesViewBean(ManageTemplatesViewBean manageTemplatesViewBean) {
		this.manageTemplatesViewBean = manageTemplatesViewBean;
	}
	public RoleConverter getRoleConverter() {
		return roleConverter;
	}
	public void setRoleConverter(RoleConverter roleConverter) {
		this.roleConverter = roleConverter;
	}
	public RoleTemplateDao getRoleTemplateDao() {
		return roleTemplateDao;
	}
	public void setRoleTemplateDao(RoleTemplateDao roleTemplateDao) {
		this.roleTemplateDao = roleTemplateDao;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public PermissionServiceLocal getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(PermissionServiceLocal permissionService) {
		this.permissionService = permissionService;
	}
	public EntityTypeConverter getEntityTypeConverter() {
		return entityTypeConverter;
	}
	public void setEntityTypeConverter(EntityTypeConverter entityTypeConverter) {
		this.entityTypeConverter = entityTypeConverter;
	}



}
