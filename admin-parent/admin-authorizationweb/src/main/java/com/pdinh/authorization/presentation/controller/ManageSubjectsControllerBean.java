package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.MenuModel;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.AuditEventDao;
import com.pdinh.auth.data.dao.AuthorizedEntityDao;
import com.pdinh.auth.data.dao.EntityRuleDao;
import com.pdinh.auth.data.dao.EntityTypeDao;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.dao.RealmFilterDao;
import com.pdinh.auth.data.dao.RoleContextDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.data.singleton.ExternalSubjectCache;
import com.pdinh.auth.persistence.entity.AuditEvent;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.RealmFilter;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.auth.persistence.entity.RoleTemplate;
import com.pdinh.authorization.presentation.obj.CompanyEntityDataObj;
import com.pdinh.authorization.presentation.obj.EntityTreeNode;
import com.pdinh.authorization.presentation.util.CompanyConverter;
import com.pdinh.authorization.presentation.util.RoleConverter;
import com.pdinh.authorization.presentation.view.ManageSubjectsViewBean;
import com.pdinh.data.CreateUserServiceLocal;
import com.pdinh.data.UsernameServiceLocal;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.enterprise.managedbean.SessionBean;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;

/**
 * Session Bean implementation class ManageRolesControllerBean
 */
@ManagedBean(name = "manageSubjectsControllerBean")
@ViewScoped
public class ManageSubjectsControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ManageSubjectsControllerBean.class);

	// private Subject subject;

	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;

	@ManagedProperty(name = "manageSubjectsViewBean", value = "#{manageSubjectsViewBean}")
	private ManageSubjectsViewBean manageSubjectsViewBean;

	@ManagedProperty(name = "roleConverter", value = "#{roleConverter}")
	private RoleConverter roleConverter;

	@ManagedProperty(name = "companyConverter", value = "#{companyConverter}")
	private CompanyConverter companyConverter;

	@EJB
	private PalmsSubjectDao subjectDao;

	@EJB
	private RoleContextDao roleContextDao;

	@EJB
	private PalmsRealmDao realmDao;

	@EJB
	private ExternalSubjectServiceLocal subjectService;

	@EJB
	private PermissionServiceLocal permissionService;

	@EJB
	private EntityServiceLocal entityService;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private UserDao userDao;

	@EJB
	private EntityTypeDao entityTypeDao;

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private EntityRuleDao entityRuleDao;

	@EJB
	private AuthorizedEntityDao entityDao;

	@EJB
	private AuditEventDao auditEventDao;

	@EJB
	private RealmFilterDao realmFilterDao;

	@EJB
	private CreateUserServiceLocal createUserService;

	@EJB
	private UsernameServiceLocal usernameService;

	@EJB
	private ExternalSubjectCache subjectCache;

	public ManageSubjectsControllerBean() {
		// TODO Auto-generated constructor stub
	}

	public void initView() {
		// Use this for checking permissions
		// Subject subject = SecurityUtils.getSubject();

		// prevent repeatedly initializing stuff
		if (manageSubjectsViewBean.getRootNode() == null) {
			if (sessionBean.isAllowAllRealms()) {
				manageSubjectsViewBean.setRealms(realmDao.findAll());
			} else {
				manageSubjectsViewBean.setRealms(realmDao.findAllAuthorizedRealms(sessionBean.getAuthorizedRealmIds()));
			}

			List<EntityRule> supportedRules = new ArrayList<EntityRule>();
			supportedRules.add(entityRuleDao.findEntityRuleByName("Allow"));
			supportedRules.add(entityRuleDao.findEntityRuleByName("AllowChildren"));
			manageSubjectsViewBean.setEntityRules(supportedRules);
			manageSubjectsViewBean.setRootNode(new DefaultTreeNode("root", null));
			manageSubjectsViewBean.setCompanyType(entityTypeDao.findEntityTypeByCode("co"));
			manageSubjectsViewBean.setProjectType(entityTypeDao.findEntityTypeByCode("prj"));
			manageSubjectsViewBean.setUserType(entityTypeDao.findEntityTypeByCode("usr"));
			manageSubjectsViewBean.setCompanyLists(new DualListModel<Company>(new ArrayList<Company>(),
					new ArrayList<Company>()));

		}

	}

	public void initCompanyLists() {
		List<Company> coList = companyDao.findAll();

		// List<Integer> authCos =
		// entityService.getAuthorizedEntities(manageSubjectsViewBean.getSelectedContext(),
		// "co");
		log.debug("About to search for entities of type {} in context id {}", EntityType.COMPANY_TYPE,
				manageSubjectsViewBean.getSelectedContext().getRole_context_id());
		List<AuthorizedEntity> authCoEntities = entityService.getAuthorizedEntities(EntityType.COMPANY_TYPE,
				manageSubjectsViewBean.getSelectedContext().getRole_context_id());
		log.debug("Got {} Authorized entities", authCoEntities.size());
		List<Company> targetList;
		manageSubjectsViewBean.setGrantAll(false);
		manageSubjectsViewBean.setConfirmGrantAllMessage("Are you sure you want to grant full access to ALL entities?");
		manageSubjectsViewBean.setRenderConfirm2Button(false);
		manageSubjectsViewBean.setRenderConfirmButton(true);
		manageSubjectsViewBean.setPickCompaniesDisabled(false);
		if (authCoEntities == null || authCoEntities.size() == 0) {
			targetList = new ArrayList<Company>();
		} else {
			List<Integer> authCos = new ArrayList<Integer>();
			List<CompanyEntityDataObj> coDataObjs = new ArrayList<CompanyEntityDataObj>();
			for (AuthorizedEntity ae : authCoEntities) {
				if (ae.getEntityRule().getEntity_rule_name().equalsIgnoreCase(EntityRule.ALLOW_ALL)) {
					log.debug("Found Entity with ID 0. Id {}, EntityId {}, entity rule {}",
							new Object[] { ae.getAuthorized_entity_id(), ae.getEntity_id(),
									ae.getEntityRule().getEntity_rule_name() });
					manageSubjectsViewBean.setGrantAll(true);
					manageSubjectsViewBean.setPickCompaniesDisabled(true);
					manageSubjectsViewBean
							.setConfirmGrantAllMessage("This action will disable All Company access.  Access may then be granted on a per company basis.  Do you wish to continue?");
					manageSubjectsViewBean.setRenderConfirm2Button(true);
					manageSubjectsViewBean.setRenderConfirmButton(false);
					authCos.add(0);
					break;
				} else {
					authCos.add(ae.getEntity_id());
					CompanyEntityDataObj obj = new CompanyEntityDataObj();
					obj.setCompanyId(ae.getEntity_id());
					if (!(ae.getEntityRule() == null)) {
						obj.setEntityRuleId(ae.getEntityRule().getEntity_rule_id());
						obj.setEntityRuleName(ae.getEntityRule().getEntity_rule_name());
					}
					coDataObjs.add(obj);
				}
			}
			targetList = companyDao.findAllCompaniesFiltered(authCos);
			log.debug("Size of Company List before remove: {}", coList.size());
			for (Company c : targetList) {
				for (Company co : coList) {
					if (co.getCompanyId() == c.getCompanyId()) {
						coList.remove(co);
						break;
					}
				}
				for (CompanyEntityDataObj dobj : coDataObjs) {
					if (dobj.getCompanyId() == c.getCompanyId()) {
						dobj.setConame(c.getCoName());
						break;
					}
				}
			}

			log.debug("Size of Company List after remove: {}", coList.size());
			manageSubjectsViewBean.setAllowedCompanies(coDataObjs);
		}

		manageSubjectsViewBean.setCompanyLists(new DualListModel<Company>(coList, targetList));
	}

	public void handleRealmSelect(ValueChangeEvent e) {
		log.debug("Handle Selecting a Realm here");

		int realm_id = Integer.valueOf((String) e.getNewValue());
		log.debug("Realm ID Selected: {}", realm_id);
		PalmsRealm theRealm = realmDao.findById(realm_id);
		if (theRealm.getRealmType().getRealm_type_name().equalsIgnoreCase("JDBC")) {
			List<RealmFilter> searchFilters = new ArrayList<RealmFilter>();
			for (RealmFilter filter : theRealm.getFilters()) {
				if (filter.getRealmFilterType().getCode().equalsIgnoreCase("SEARCH")) {
					searchFilters.add(filter);
				}
			}
			manageSubjectsViewBean.setQueries(searchFilters);
			manageSubjectsViewBean.setSelectSearchMethodRendered(true);
			manageSubjectsViewBean.setGetSubjectsDisabled(true);
			manageSubjectsViewBean.setImportSubjectsDisabled(true);
			manageSubjectsViewBean.setInitRealmMetadataDisabled(true);
			manageSubjectsViewBean.setUpdateRealmMetadataDisabled(true);
		} else {
			manageSubjectsViewBean.setGetSubjectsDisabled(false);
			manageSubjectsViewBean.setImportSubjectsDisabled(false);
			manageSubjectsViewBean.setSelectSearchMethodRendered(false);
			manageSubjectsViewBean.setSelectCompanyRendered(false);
			manageSubjectsViewBean.setInitRealmMetadataDisabled(false);
			manageSubjectsViewBean.setUpdateRealmMetadataDisabled(false);
		}
	}

	public void handleGetSubjects() {
		log.debug("Handle Getting Subjects from selected Realm here");

		int realm_id = Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName());

		log.debug("Realm ID Selected: {}", realm_id);
		PalmsRealm theRealm = realmDao.findById(realm_id);

		List<ExternalSubjectDataObj> userobjs = new ArrayList<ExternalSubjectDataObj>();
		if (theRealm.getRealmType().getRealm_type_name().equalsIgnoreCase("Active Directory")) {
			manageSubjectsViewBean.setRenderCompanyNameColumn(false);
			manageSubjectsViewBean.setRenderCountryColumn(true);
			manageSubjectsViewBean.setRenderExternalIdColumn(false);
			manageSubjectsViewBean.setRenderLocationColumn(true);
			List<String> realmNames = new ArrayList<String>();
			realmNames.add(theRealm.getRealm_name());
			userobjs = subjectCache.getAllSubjects(realmNames);
			// userobjs = subjectService.getExternalSubjects(theRealm,"",
			// false);

		} else if (theRealm.getRealmType().getRealm_type_name().equalsIgnoreCase("JDBC")) {
			manageSubjectsViewBean.setRenderCompanyNameColumn(true);
			manageSubjectsViewBean.setRenderCountryColumn(false);
			manageSubjectsViewBean.setRenderExternalIdColumn(true);
			manageSubjectsViewBean.setRenderLocationColumn(false);
			RealmFilter realmFilter = realmFilterDao.findById(Integer.valueOf(manageSubjectsViewBean
					.getSelectedSearchAttr()));
			if (realmFilter.getCode().equalsIgnoreCase("PARTEMAIL")) {
				log.debug("Searching by email: {}", manageSubjectsViewBean.getEmailInput());
				String emailInput = manageSubjectsViewBean.getEmailInput();
				
				if (emailInput.matches("^[A-Za-z0-9\u00C0-\u1FFF\u2C00-\uD7FF:`/./-/_@]{4,}")){
					if (emailInput.equalsIgnoreCase(".com")
							|| emailInput.equalsIgnoreCase(".org")) {
						FacesContext facesContext = FacesContext.getCurrentInstance();
						facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Are you trying to crash the server you moron?  Try again", "Complete"));
						return;
					}
				}else{
					FacesContext facesContext = FacesContext.getCurrentInstance();
					facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email Search string "
							+ emailInput
							+ " is invalid search string", "Complete"));
					return;
				}
				
				manageSubjectsViewBean.setFilterParameters(new Object[] { emailInput });
			}
			userobjs = subjectService.getExternalSubjects(realmFilter, manageSubjectsViewBean.getFilterParameters(),
					false);

		} else if (theRealm.getRealmType().getRealm_type_name().equalsIgnoreCase("LDAP")) {
			manageSubjectsViewBean.setRenderCompanyNameColumn(true);
			manageSubjectsViewBean.setRenderCountryColumn(false);
			manageSubjectsViewBean.setRenderExternalIdColumn(false);
			manageSubjectsViewBean.setRenderLocationColumn(true);
			List<String> realmNames = new ArrayList<String>();
			realmNames.add(theRealm.getRealm_name());
			userobjs = subjectCache.getAllSubjects(realmNames);
			// userobjs = subjectService.getExternalSubjects(theRealm,"",
			// false);

		}

		manageSubjectsViewBean.setRealmUsers(userobjs);
		// manageSubjectsViewBean.setSubjects(subjectDao.findSubjectsByRealmId(realm_id));
		log.debug("Returned {} Subjects from Realm", userobjs.size());
	}

	public void handleQuerySelect(ValueChangeEvent e) {
		log.debug("In handle query select");
		RealmFilter filter = realmFilterDao.findById(Integer.valueOf((String) e.getNewValue()));
		if (filter.getCode().equalsIgnoreCase("FINDBYCO")) {
			List<Company> companies = new ArrayList<Company>();
			companies = companyDao.findAllActiveAssessment();
			manageSubjectsViewBean.setCompanies(companies);
			manageSubjectsViewBean.setSelectCompanyRendered(true);
			manageSubjectsViewBean.setEmailInputRendered(false);

		} else if (filter.getCode().equalsIgnoreCase("ALLADMIN")) {
			manageSubjectsViewBean.setGetSubjectsDisabled(false);
			manageSubjectsViewBean.setSelectCompanyRendered(false);
			manageSubjectsViewBean.setEmailInputRendered(false);
			manageSubjectsViewBean.setFilterParameters(new Object[] {});
		} else if (filter.getCode().equalsIgnoreCase("PARTEMAIL")) {
			manageSubjectsViewBean.setEmailInputRendered(true);
			manageSubjectsViewBean.setSelectCompanyRendered(false);
			manageSubjectsViewBean.setGetSubjectsDisabled(false);
		}
		log.debug("Finished with Query Select");
	}

	public void handleCompanySelect(ValueChangeEvent e) {
		log.debug("handle selecting a company here");
		manageSubjectsViewBean.setFilterParameters(new Object[] { e.getNewValue() });

		manageSubjectsViewBean.setGetSubjectsDisabled(false);

	}

	public void generateContextMenu() {
		ExternalSubjectDataObj userObj = manageSubjectsViewBean.getSelectedRealmUser();
		List<PalmsRealm> realmList = manageSubjectsViewBean.getRealms();

		PalmsRealm selectedRealm = realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));

		PalmsSubject selectedSubject = subjectDao.findSubjectByPrincipalAndRealm(userObj.getUsername(),
				selectedRealm.getRealm_id());
		if (selectedSubject == null) {

			selectedSubject = subjectService.createSubjectDefaultContext(userObj.getUsername(),
					selectedRealm.getRealm_id(), userObj.getEntryDN(), userObj.getExternal_id());
		}
		manageSubjectsViewBean.setSelectedSubject(selectedSubject);
		List<RoleContext> contextList = selectedSubject.getContextList();
		List<AuthorizedEntity> authorizedRealms = new ArrayList<AuthorizedEntity>();
		for (RoleContext context : contextList) {
			if (context.isIsdefault()) {
				authorizedRealms = entityService.getAuthorizedEntities(EntityType.REALM_TYPE,
						context.getRole_context_id());
				break;
			}
		}

		FacesContext facesContext = FacesContext.getCurrentInstance();
		Class<?>[] parameterTypes = {};
		MenuModel model = new DefaultMenuModel();
		Submenu entityMenu = new Submenu();
		entityMenu.setLabel("Entities");
		MenuItem viewItem = new MenuItem();
		viewItem.setValue("View");
		viewItem.setId("viewEntitiesMenuItem");
		viewItem.setOncomplete("viewEntitiesDlg.show()");
		viewItem.setUpdate("viewEntitiesPanel");
		viewItem.setActionExpression(facesContext
				.getApplication()
				.getExpressionFactory()
				.createMethodExpression(facesContext.getELContext(),
						"#{manageSubjectsControllerBean.handleViewEntitiesInit}", null, parameterTypes));
		MenuItem removeItem = new MenuItem();
		removeItem.setValue("Remove All");
		removeItem.setUrl("#");
		removeItem.setOnclick("viewEntitiesDlg.show()");
		removeItem.setIcon("ui-icon-close");
		entityMenu.getChildren().add(viewItem);
		entityMenu.getChildren().add(removeItem);

		Submenu realmMenu = new Submenu();
		realmMenu.setLabel("Realms");
		Submenu grantRealmAccessMenu = new Submenu();
		grantRealmAccessMenu.setLabel("Make Realm Admin");
		MenuItem revokeItem = new MenuItem();
		revokeItem.setValue("Revoke Realm Access");
		revokeItem.setActionExpression(facesContext
				.getApplication()
				.getExpressionFactory()
				.createMethodExpression(facesContext.getELContext(),
						"#{manageSubjectsControllerBean.handleRevokeRealmAccess}", null, parameterTypes));
		revokeItem.setUpdate("growl");
		Class<?>[] paramTypes = { String.class };
		boolean hasUberRealmAccess = false;
		for (AuthorizedEntity realmEntity : authorizedRealms) {
			if (realmEntity.getEntityRule().getEntity_rule_name().equalsIgnoreCase("AllowAll")) {
				hasUberRealmAccess = true;
			}
		}
		for (PalmsRealm realm : realmList) {
			MenuItem realmItem = new MenuItem();
			realmItem.setValue(realm.getRealm_name());
			for (AuthorizedEntity entity : authorizedRealms) {
				if (entity.getEntity_id() == realm.getRealm_id() || hasUberRealmAccess) {
					realmItem.setIcon("ui-icon-check");
					realmItem.setDisabled(true);
					break;
				}
			}
			realmItem.setUpdate("growl");
			realmItem.setActionExpression(facesContext
					.getApplication()
					.getExpressionFactory()
					.createMethodExpression(facesContext.getELContext(),
							"#{manageSubjectsControllerBean.handleGrantRealmAdmin(" + realm.getRealm_id() + ")}", null,
							paramTypes));
			grantRealmAccessMenu.getChildren().add(realmItem);
		}
		MenuItem addAllItem = new MenuItem();
		addAllItem.setValue("Select All Realms");
		addAllItem.setActionExpression(facesContext
				.getApplication()
				.getExpressionFactory()
				.createMethodExpression(facesContext.getELContext(),
						"#{manageSubjectsControllerBean.handleGrantAllRealmAdmin}", null, parameterTypes));

		if (hasUberRealmAccess) {
			addAllItem.setDisabled(true);
		}
		grantRealmAccessMenu.getChildren().add(addAllItem);
		realmMenu.getChildren().add(grantRealmAccessMenu);
		realmMenu.getChildren().add(revokeItem);
		// Audit Log Menu
		Submenu auditMenu = new Submenu();
		auditMenu.setLabel("Audit");
		MenuItem showAuditLogItem = new MenuItem();
		showAuditLogItem.setValue("Show Audit Log");
		showAuditLogItem.setActionExpression(facesContext
				.getApplication()
				.getExpressionFactory()
				.createMethodExpression(facesContext.getELContext(),
						"#{manageSubjectsControllerBean.handleShowAuditLog}", null, parameterTypes));
		showAuditLogItem.setOncomplete("auditLogDlg.show()");
		showAuditLogItem.setUpdate("auditLogTable");// update audit log table
		auditMenu.getChildren().add(showAuditLogItem);

		model.addSubmenu(entityMenu);
		model.addSubmenu(realmMenu);
		model.addSubmenu(auditMenu);
		manageSubjectsViewBean.setMenuModel(model);

	}

	public void handleShowAuditLog() {
		ExternalSubjectDataObj selectedUser = manageSubjectsViewBean.getSelectedRealmUser();
		List<String> loggerNames = new ArrayList<String>();
		loggerNames.add("AUDIT-AUTHORIZATION");
		List<AuditEvent> events = auditEventDao.findByArg0AndLoggerName(selectedUser.getUsername(), loggerNames);
		log.debug("Got {} audit events for {}", events.size(), selectedUser.getUsername());
		manageSubjectsViewBean.setAuditEvents(events);
	}

	// not used currently
	public String convertTime(long time) {
		Date date = new Date(time);
		Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		return format.format(date).toString();
	}

	public void handleGrantAllRealmAdmin() {
		log.debug("handle granting AllowAll rule for Realms");
		ExternalSubjectDataObj userObj = manageSubjectsViewBean.getSelectedRealmUser();
		PalmsRealm selectedRealm = realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));
		PalmsSubject selectedSubject = subjectDao.findSubjectByPrincipalAndRealm(userObj.getUsername(),
				selectedRealm.getRealm_id());
		if (selectedSubject == null) {

			selectedSubject = subjectService.createSubjectDefaultContext(userObj.getUsername(),
					selectedRealm.getRealm_id(), userObj.getEntryDN(), userObj.getExternal_id());
		}
		RoleContext context = roleContextDao.findDefaultRoleContextBySubjectAndRealm(selectedSubject.getPrincipal(),
				selectedRealm.getRealm_id());

		context = entityService.grantAllEntityAccess(context, EntityType.REALM_TYPE, null, SecurityUtils.getSubject()
				.getPrincipal().toString());

		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("Subject " + selectedSubject.getPrincipal()
				+ " granted Admin rights over All realms", "Complete"));

	}

	public void handleGrantRealmAdmin(String RealmId) {
		log.debug("Handle Granting access to a realm here");
		ExternalSubjectDataObj userObj = manageSubjectsViewBean.getSelectedRealmUser();
		List<PalmsRealm> realmList = manageSubjectsViewBean.getRealms();

		PalmsRealm selectedRealm = realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));
		PalmsRealm grantedRealm = realmDao.findById(Integer.valueOf(RealmId));
		PalmsSubject selectedSubject = subjectDao.findSubjectByPrincipalAndRealm(userObj.getUsername(),
				selectedRealm.getRealm_id());
		if (selectedSubject == null) {

			selectedSubject = subjectService.createSubjectDefaultContext(userObj.getUsername(),
					selectedRealm.getRealm_id(), userObj.getEntryDN(), userObj.getExternal_id());
		}
		RoleContext context = roleContextDao.findDefaultRoleContextBySubjectAndRealm(selectedSubject.getPrincipal(),
				selectedRealm.getRealm_id());
		AuthorizedEntity authorizedRealm = new AuthorizedEntity();
		authorizedRealm.setEntity_id(Integer.valueOf(RealmId));
		EntityType realmType = entityTypeDao.findEntityTypeByCode(EntityType.REALM_TYPE);
		authorizedRealm.setEntityType(realmType);
		EntityRule realmRule = entityRuleDao.findEntityRuleByName(EntityRule.ALLOW);
		authorizedRealm.setEntityRule(realmRule);
		authorizedRealm.setSubject_id(selectedSubject.getSubject_id());
		authorizedRealm.setRoleContext(context);

		context = entityService.grantEntityAccess(context.getRole_context_id(), authorizedRealm, SecurityUtils
				.getSubject().getPrincipal().toString());
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("Subject " + selectedSubject.getPrincipal()
				+ " granted Admin rights over " + grantedRealm.getRealm_label(), "Complete"));
	}

	public void handleEditSubjects() {
		log.debug("Handle Manage Subjects Here");
	}

	public void handleGrantAllAccess() {
		RoleContext context = manageSubjectsViewBean.getSelectedContext();

		log.debug("Handling Grant all Access");

		context = entityService.grantAllEntityAccess(context, EntityType.COMPANY_TYPE, null, SecurityUtils.getSubject()
				.getPrincipal().toString());
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("AllowAll Company Access Granted", "Complete"));

	}

	public void handleRevokeRealmAccess() {
		ExternalSubjectDataObj userObj = manageSubjectsViewBean.getSelectedRealmUser();
		PalmsRealm selectedRealm = realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));
		PalmsSubject selectedSubject = subjectDao.findSubjectByPrincipalAndRealm(userObj.getUsername(),
				selectedRealm.getRealm_id());
		if (selectedSubject == null) {

			selectedSubject = subjectService.createSubjectDefaultContext(userObj.getUsername(),
					selectedRealm.getRealm_id(), userObj.getEntryDN(), userObj.getExternal_id());
		}
		RoleContext context = roleContextDao.findDefaultRoleContextBySubjectAndRealm(selectedSubject.getPrincipal(),
				selectedRealm.getRealm_id());

		List<AuthorizedEntity> realmEntities = entityService.getAuthorizedEntities(EntityType.REALM_TYPE,
				context.getRole_context_id());
		log.debug("Size of Realm Entity List to remove: {}", realmEntities.size());
		for (AuthorizedEntity realmEntity : realmEntities) {
			entityDao.delete(realmEntity);

		}
		roleContextDao.update(context);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null,
				new FacesMessage("Realm Admin Rights revoked for user: " + selectedSubject.getPrincipal(), "Complete"));
	}

	public void handleRevokeAllAccess() {
		RoleContext context = manageSubjectsViewBean.getSelectedContext();
		log.debug("user has chosen to revoke access to all entities");
		manageSubjectsViewBean.setPickCompaniesDisabled(false);
		manageSubjectsViewBean.setConfirmGrantAllMessage("Are you sure you want to grant full access to ALL entities?");
		manageSubjectsViewBean.setRenderConfirm2Button(false);
		manageSubjectsViewBean.setRenderConfirmButton(true);
		context = entityService.revokeEntityAccess(context, 0, EntityType.COMPANY_TYPE, SecurityUtils.getSubject()
				.getPrincipal().toString());
		// AuthorizedEntity uberCompanyEntity =
		// entityService.getAuthorizedEntity(context, EntityType.COMPANY_TYPE,
		// 0);
		// context.setEntities(null);
		// entityDao.delete(uberCompanyEntity);
		// roleContextDao.update(context);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("AllowAll Company Access Revoked", "Complete"));
	}

	public void handleCheckGrantEvent() {
		log.debug("In handleCheckGrantEvent()");
	}

	public void handleSubjectSelect() {
		log.debug("Handle Selecting a subject here");
		Subject subject = SecurityUtils.getSubject();
		// List<String> permissions = sessionBean.getPermissionsNames();
		if (subject.isPermitted("editSubjects")) {
			manageSubjectsViewBean.setEditRolesDisabled(false);
		}
		if (subject.isPermitted("editRoleContexts")) {
			manageSubjectsViewBean.setAddContextDisabled(false);
		}
		if (subject.isPermitted("deleteRoleContexts")) {
			manageSubjectsViewBean.setDeleteContextDisabled(false);
		}
		if (subject.isPermitted("manageEntities")) {
			manageSubjectsViewBean.setEntitiesButtonDisabled(false);
			manageSubjectsViewBean.setNavEntitiesButtonDisabled(false);
		}
		/* -- hook this up to a permission
		if (subject.isPermitted("createLinkedAccounts")){
			manageSubjectsViewBean.setLinkedAccountButtonDisabled(false);
		}
		*/
		manageSubjectsViewBean.setRefreshMetadataButtonDisabled(false);
		manageSubjectsViewBean.setLinkedAccountButtonDisabled(false);
		generateContextMenu();
		manageSubjectsViewBean.setContextMenuDisabled(false);

	}

	public void handleLinkedAccountDialogInit() {
		log.debug("Handling linked account dialog init");
		handleEditRoles();
		manageSubjectsViewBean.setSelectedLinkOption(null);
		manageSubjectsViewBean.setLinkRadioSubmitDisabled(true);
		int realm_id = Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName());
		PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(manageSubjectsViewBean.getSelectedRealmUser()
				.getUsername(), realm_id);
		if (!(subj.getLinked_id() == 0)) {
			User user = userDao.findById(subj.getLinked_id());
			/*
			ExternalSubjectDataObj existUser = new ExternalSubjectDataObj();
			existUser.setEmail(user.getEmail());
			existUser.setFirstname(user.getFirstname());
			existUser.setLastname(user.getLastname());
			existUser.setExternal_id(user.getUsersId());
			existUser.setUsername(user.getUsername());
			*/
			manageSubjectsViewBean.setExistingLinkedUser(user);
			manageSubjectsViewBean.setLinkRadioDisabled(true);
			manageSubjectsViewBean.setRefreshAccountButtonDisabled(false);
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage(null,
					new FacesMessage("Subject " + subj.getPrincipal() + " is linked to  " + user.getEmail() + "("
							+ user.getUsersId() + ")", "You may refresh or Unlink"));
		} else {
			manageSubjectsViewBean.setLinkRadioDisabled(false);
			manageSubjectsViewBean.setRefreshAccountButtonDisabled(true);
		}
	}

	public void handleSelectLinkOption() {
		if (manageSubjectsViewBean.getSelectedLinkOption().equalsIgnoreCase("1")) {
			manageSubjectsViewBean.setLinkRadioSubmitDisabled(false);
		} else if (manageSubjectsViewBean.getSelectedLinkOption().equalsIgnoreCase("2")) {
			manageSubjectsViewBean.setLookupByEmailButtonDisabled(false);

		}
	}

	public void handleRefreshAccount() {
		// ExternalSubjectDataObj existUser =
		// manageSubjectsViewBean.getExistingLinkedUser();
		User user = manageSubjectsViewBean.getExistingLinkedUser();
		ExternalSubjectDataObj selectedUser = manageSubjectsViewBean.getSelectedRealmUser();
		user.setFirstname(selectedUser.getFirstname());
		user.setLastname(selectedUser.getLastname());
		user.setEmail(selectedUser.getEmail());
		user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(), user.getLastname()));
		user.setExternallyManaged(true);
		userDao.update(user);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("User data for linked account " + user.getUsersId()
				+ " has been refreshed", user.getFirstname() + " " + user.getLastname() + " " + user.getEmail()));

	}

	public void lookupByEmail() {
		ExternalSubjectDataObj selectedUser = manageSubjectsViewBean.getSelectedRealmUser();
		PalmsRealm realm = realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));
		List<User> user = userDao.findByEmailAndCompany(selectedUser.getEmail(), realm.getLinked_company_id());

		if (user.isEmpty()) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage(null, new FacesMessage("No user found with email " + selectedUser.getEmail()
					+ " in company " + realm.getLinked_company_id()));

		} else if (user.size() > 1) {
			String userids = "";
			for (User u : user) {
				userids = userids + " " + u.getUsersId();
			}
			FacesContext facesContext = FacesContext.getCurrentInstance();

			facesContext.addMessage(null, new FacesMessage("Multiple users found: " + userids));

		} else {
			manageSubjectsViewBean.setUsersIdInput(String.valueOf(user.get(0).getUsersId()));
			manageSubjectsViewBean.setLinkRadioSubmitDisabled(false);
			FacesContext facesContext = FacesContext.getCurrentInstance();

			facesContext.addMessage(null, new FacesMessage("Found User: " + user.get(0).getUsersId()));
		}
	}

	public void handleIdUpdate() {
		manageSubjectsViewBean.setLinkRadioSubmitDisabled(false);
	}

	public void handleSubmitLinkSubject() {
		log.debug("In handleSubmitLinkSubject");
		String selectedLinkOption = manageSubjectsViewBean.getSelectedLinkOption();
		PalmsRealm realm = realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));
		log.debug("Selected Link Option {}", selectedLinkOption);
		if (selectedLinkOption.equalsIgnoreCase("1")) {

			int company_id = realm.getLinked_company_id();
			Company company = companyDao.findById(company_id);
			ExternalSubjectDataObj user = manageSubjectsViewBean.getSelectedRealmUser();
			User palms_user = new User();
			palms_user.setFirstname(user.getFirstname());
			palms_user.setLastname(user.getLastname());
			palms_user.setEmail(user.getEmail());
			palms_user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(),
					user.getLastname()));
			palms_user.setExternallyManaged(true);
			palms_user.setMserverid((short) 1);
			palms_user.setType(1);

			if (userDao.findByEmailAndCompany(user.getEmail(), company_id).isEmpty()) {
				palms_user = createUserService.createNewUser(palms_user, company, null, null, null);
				log.debug("Created New User with id: {}", palms_user.getUsersId());
				PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(manageSubjectsViewBean
						.getSelectedRealmUser().getUsername(), realm.getRealm_id());
				subj.setLinked_id(palms_user.getUsersId());
				subjectDao.update(subj);
				log.debug("Updated Link to subject id {}", subj.getSubject_id());
				FacesContext facesContext = FacesContext.getCurrentInstance();
				facesContext.addMessage(null,
						new FacesMessage("Created externally managed User " + palms_user.getUsersId() + " in company "
								+ company_id, "Updates to this record are limited"));
			} else {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				facesContext.addMessage(null, new FacesMessage("A user with email " + user.getEmail()
						+ " already exists in company " + company_id, "Link to this user or change the user email"));
			}
		} else if (selectedLinkOption.equalsIgnoreCase("2")) {
			log.debug("Lookup existing user here");
			int UserId = Integer.valueOf(manageSubjectsViewBean.getUsersIdInput());
			User user = userDao.findById(UserId);
			ExternalSubjectDataObj realmUser = manageSubjectsViewBean.getSelectedRealmUser();
			user.setFirstname(realmUser.getFirstname());
			user.setLastname(realmUser.getLastname());
			user.setEmail(realmUser.getEmail());
			user.setUsername(usernameService.generateUsername(user.getEmail(), user.getFirstname(), user.getLastname()));
			user.setPassword(null);
			user.setHashedpassword(createUserService.generateHashedPassword(createUserService.getNewPassword(), user
					.getRowguid().substring(0, 10).toUpperCase()));
			user.setExternallyManaged(true);
			userDao.update(user);
			PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(manageSubjectsViewBean.getSelectedRealmUser()
					.getUsername(), realm.getRealm_id());
			subj.setLinked_id(user.getUsersId());
			subj = subjectDao.update(subj);
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage(null, new FacesMessage("Created Link to existing user " + user.getEmail() + "("
					+ user.getUsersId() + ")", "User is now externally managed"));
		}

	}

	public String handleNavEntities() {
		int subjectId = manageSubjectsViewBean.getSelectedSubject().getSubject_id();
		int realmId = Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName());
		return "manageEntities.xhtml?faces-redirect=true&subjId=" + subjectId + "&realmId=" + realmId;
	}

	public void handleEditRoles() {
		log.debug("Handle Edit Roles here");
		int realm_id = Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName());
		// PalmsRealm realm =
		// realmDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName()));
		log.debug("Got Palms Realm ID: {}", realm_id);
		RoleContext defaultContext;
		log.debug("About to get Subject {}", manageSubjectsViewBean.getSelectedRealmUser().getUsername());
		PalmsSubject subj = subjectDao.findSubjectByPrincipalAndRealm(manageSubjectsViewBean.getSelectedRealmUser()
				.getUsername(), realm_id);
		log.debug("Got Subject {}", subj);
		if (subj == null) {
			subj = new PalmsSubject();
			ExternalSubjectDataObj obj = manageSubjectsViewBean.getSelectedRealmUser();

			subj = subjectService.createSubjectDefaultContext(obj.getUsername(), realm_id, obj.getEntryDN(),
					obj.getExternal_id());

			defaultContext = subj.getContextList().get(0);

			manageSubjectsViewBean.setRoleContexts(subj.getContextList());
			manageSubjectsViewBean.setSelectedSubject(subj);
		} else {
			log.debug("About to get RoleContexts for subject");
			List<RoleContext> contexts = subj.getContextList();
			log.debug("Got {} Role Conexts", contexts.size());
			// in case subject was created without a role context
			if (contexts == null || contexts.isEmpty()) {
				defaultContext = permissionService.createDefaultRoleContext(subj);
				contexts.add(defaultContext);
			}
			manageSubjectsViewBean.setRoleContexts(contexts);
			manageSubjectsViewBean.setSelectedSubject(subj);
		}
		log.debug("finished with handleEditRoles()");

	}

	public String handleFlowProcessEvent(FlowEvent event) {
		log.debug("Current wizard Step: {}", event.getOldStep());
		log.debug("Next step: {}", event.getNewStep());
		if (event.getNewStep().equalsIgnoreCase("selectRolesTab")) {
			int context_id = Integer.valueOf(manageSubjectsViewBean.getSelectedContextId());
			RoleContext selectedContext = roleContextDao.findById(context_id);
			manageSubjectsViewBean.setSelectedContext(selectedContext);
			RoleTemplate template = manageSubjectsViewBean.getSelectedSubject().getRealm().getRoleTemplate();
			List<Role> sourceRoles = template.getAllowedRoles();
			List<Role> targetRoles = manageSubjectsViewBean.getSelectedContext().getRoles();
			for (Role r : targetRoles) {
				sourceRoles = removeRole(sourceRoles, r.getRole_id());
			}
			manageSubjectsViewBean.setContextRoleLists(new DualListModel<Role>(sourceRoles, targetRoles));
		}
		if (event.getOldStep().equalsIgnoreCase("selectRolesTab")) {
			manageSubjectsViewBean.getSelectedContext().setRoles(
					manageSubjectsViewBean.getContextRoleLists().getTarget());
		}
		return event.getNewStep();
	}

	public List<Role> removeRole(List<Role> list, Integer roleId) {
		for (Role r : list) {
			if (r.getRole_id() == roleId) {
				list.remove(r);
				return list;
			}
		}
		return list;
	}

	public String handleFlowAddContext(FlowEvent event) {

		if (event.getNewStep().equalsIgnoreCase("selectRolesTab2")) {

			RoleTemplate template = manageSubjectsViewBean.getSelectedSubject().getRealm().getRoleTemplate();
			List<Role> sourceRoles = template.getAllowedRoles();
			List<Role> targetRoles = new ArrayList<Role>();
			manageSubjectsViewBean.setContextRoleLists(new DualListModel<Role>(sourceRoles, targetRoles));
		}
		if (event.getOldStep().equalsIgnoreCase("selectRolesTab2")) {
			manageSubjectsViewBean.getNewContext().setRoles(manageSubjectsViewBean.getContextRoleLists().getTarget());
		}

		return event.getNewStep();
	}

	public String handleFlowEntityWizard(FlowEvent event) {
		if (event.getOldStep().equalsIgnoreCase("rulesTab") && event.getNewStep().equalsIgnoreCase("selectEntitiesTab")) {
			List<CompanyEntityDataObj> colist = manageSubjectsViewBean.getAllowedCompanies();
			for (CompanyEntityDataObj obj : colist) {
				if (obj.getEntityRuleName() == null || obj.getEntityRuleName().equalsIgnoreCase("")) {
					FacesContext facesContext = FacesContext.getCurrentInstance();
					facesContext.addMessage(null,
							new FacesMessage("You Must set an entity rule for each company", obj.getConame()
									+ " has no rule"));
					return event.getOldStep();
				}
			}
			handleEntitiesInit();
		}
		if (event.getOldStep().equalsIgnoreCase("selectCompanyTab") && event.getNewStep().equalsIgnoreCase("rulesTab")) {
			handleRulesInit();
		}
		return event.getNewStep();
	}

	public void handleRulesInit() {
		log.debug("List of Allowed companies contains {} companies", manageSubjectsViewBean.getAllowedCompanies()
				.size());
		manageSubjectsViewBean.setSelectedAllowedCompanies(new ArrayList<CompanyEntityDataObj>());
		List<Company> selectedCos = manageSubjectsViewBean.getCompanyLists().getTarget();
		List<CompanyEntityDataObj> allowedCos = new ArrayList<CompanyEntityDataObj>();
		for (Company co : selectedCos) {
			CompanyEntityDataObj allowedCo;
			AuthorizedEntity entity = entityService.getAuthorizedEntity(manageSubjectsViewBean.getSelectedContext()
					.getRole_context_id(), manageSubjectsViewBean.getCompanyType().getEntity_type_id(), co
					.getCompanyId());
			if (!(entity == null)) {

				if (entity.getEntityRule() == null) {
					allowedCo = new CompanyEntityDataObj();
					allowedCo.setCompanyId(co.getCompanyId());
					allowedCo.setConame(co.getCoName());
				} else {
					allowedCo = new CompanyEntityDataObj(co.getCoName(), entity.getEntityRule().getEntity_rule_name(),
							co.getCompanyId(), entity.getEntityRule().getEntity_rule_id());
				}
			} else {
				allowedCo = new CompanyEntityDataObj();
				allowedCo.setCompanyId(co.getCompanyId());
				allowedCo.setConame(co.getCoName());
			}
			allowedCos.add(allowedCo);
		}
		manageSubjectsViewBean.setAllowedCompanies(allowedCos);
	}

	public void handleRoleTransfer(TransferEvent event) {
		RoleContext context = manageSubjectsViewBean.getSelectedContext();
		Subject subject = SecurityUtils.getSubject();
		if (event.isAdd()) {
			List<Role> roles = (List<Role>) event.getItems();
			for (Role r : roles) {
				context = permissionService.grantRole(context, r, subject.getPrincipal().toString());
			}
			// manageSubjectsViewBean.setSelectedContext(context);
		} else if (event.isRemove()) {
			List<Role> roles = (List<Role>) event.getItems();
			for (Role r : roles) {

				context = permissionService.removeRole(context, r, subject.getPrincipal().toString());
			}
		}
	}

	public void handleTransfer(TransferEvent event) {
		List<Company> companies = (List<Company>) event.getItems();
		log.debug("Handling transfer event for items: {}", event.getItems());
		String message = "";
		RoleContext context = manageSubjectsViewBean.getSelectedContext();
		// List<Role> roles = context.getRoles();
		if (event.isAdd()) {
			List<CompanyEntityDataObj> newObjs = new ArrayList<CompanyEntityDataObj>();
			for (Company c : companies) {
				CompanyEntityDataObj obj = new CompanyEntityDataObj();
				obj.setCompanyId(c.getCompanyId());
				obj.setConame(c.getCoName());
				newObjs.add(obj);
				AuthorizedEntity newCoEntity = new AuthorizedEntity();
				newCoEntity.setEntity_id(c.getCompanyId());
				newCoEntity.setSubject_id(context.getSubject().getSubject_id());
				newCoEntity.setEntityType(manageSubjectsViewBean.getCompanyType());
				newCoEntity.setRoleContext(context);
				// newCoEntity.setRoles(roles);
				context.getEntities().add(newCoEntity);
				// context =
				// entityService.grantEntityAccess(context.getRole_context_id(),
				// newCoEntity, subject.getPrincipal().toString());
			}
			// manageSubjectsViewBean.setSelectedContext(context);
			if (manageSubjectsViewBean.getAllowedCompanies() == null) {
				manageSubjectsViewBean.setAllowedCompanies(newObjs);
			} else {
				manageSubjectsViewBean.getAllowedCompanies().addAll(newObjs);
			}
			message = "Added New Authorized Companies";
		} else if (event.isRemove()) {
			List<CompanyEntityDataObj> removeThese = new ArrayList<CompanyEntityDataObj>();
			List<CompanyEntityDataObj> allowedCos = manageSubjectsViewBean.getAllowedCompanies();

			for (Company c : companies) {

				for (CompanyEntityDataObj co : allowedCos) {
					if (c.getCompanyId() == co.getCompanyId()) {
						removeThese.add(co);
					}
				}
			}
			for (CompanyEntityDataObj removeCo : removeThese) {
				entityService.revokeEntityAccess(context, removeCo.getCompanyId(), EntityType.COMPANY_TYPE,
						SecurityUtils.getSubject().getPrincipal().toString());
				log.debug("Removed Authorized Entity: {}", removeCo.getConame());

			}

			allowedCos.removeAll(removeThese);
			manageSubjectsViewBean.setAllowedCompanies(allowedCos);
			message = "Removed Authorized Companies";
		}
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage(message, "complete"));
	}

	public void handleContextSelect(AjaxBehaviorEvent event) {
		log.debug("Some info about the Event:  {}", event.getBehavior().getClass());
		log.debug("New Selected Context: {}", manageSubjectsViewBean.getSelectedContextId());
		RoleContext context = roleContextDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedContextId()));
		manageSubjectsViewBean.setSelectedContext(context);
		initCompanyLists();
	}

	public void handleContextSelectForEditRolesDlg(AjaxBehaviorEvent event) {
		RoleContext context = roleContextDao.findById(Integer.valueOf(manageSubjectsViewBean.getSelectedContextId()));
		manageSubjectsViewBean.setSelectedContext(context);
	}

	public void handleEntityRuleSelect() {
		log.debug("handle selecting an entity rule here");
	}

	public void handleCoSelect() {
		log.debug("handle selecting a co here");
	}

	public void handleSetRules() {
		List<CompanyEntityDataObj> selectedCos = manageSubjectsViewBean.getSelectedAllowedCompanies();
		EntityRule selectedRule = entityRuleDao.findById(Integer.valueOf(manageSubjectsViewBean
				.getSelectedEntityRuleId()));
		List<CompanyEntityDataObj> allowedCos = manageSubjectsViewBean.getAllowedCompanies();
		RoleContext context = manageSubjectsViewBean.getSelectedContext();
		RoleContext newContext = new RoleContext();
		for (CompanyEntityDataObj obj : selectedCos) {
			obj.setEntityRuleId(selectedRule.getEntity_rule_id());
			obj.setEntityRuleName(selectedRule.getEntity_rule_name());
			if (allowedCos.contains(obj)) {
				log.debug("it's all good");
			}
			AuthorizedEntity entity = entityService.getAuthorizedEntity(context, "co", obj.getCompanyId());
			entity.setEntityRule(selectedRule);
			newContext = entityService.grantEntityAccess(context.getRole_context_id(), entity, SecurityUtils
					.getSubject().getPrincipal().toString());
		}
		if (!(newContext == null)) {
			manageSubjectsViewBean.setSelectedContext(newContext);
		}

	}

	public void handleSaveRoleContext() {
		log.debug("about to update role context");
		// roleContextDao.update(manageSubjectsViewBean.getSelectedContext());
		log.debug("finished update to role context");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Saved Roles for Context", "complete"));
	}

	public void handleCreateRoleContext() {
		RoleContext newContext = manageSubjectsViewBean.getNewContext();
		if (newContext.isIsdefault()) {
			List<RoleContext> clist = manageSubjectsViewBean.getSelectedSubject().getContextList();

			Iterator<RoleContext> iter = clist.iterator();
			while (iter.hasNext()) {
				RoleContext thisContext = iter.next();
				if (thisContext.isIsdefault()) {
					thisContext.setIsdefault(false);
					roleContextDao.update(thisContext);
					break;
				}
			}
		}
		newContext.setSubject(manageSubjectsViewBean.getSelectedSubject());
		roleContextDao.create(newContext);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Created new Role Context", newContext.getRole_context_name()));
	}

	public void handleAddRoleContext() {
		manageSubjectsViewBean.setNewContext(new RoleContext());
	}

	public void handleInitRealmMetadata() {
		int realmId = Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName());
		List<String> realmNames = new ArrayList<String>();
		for (PalmsRealm realm : manageSubjectsViewBean.getRealms()) {
			if (realmId == realm.getRealm_id()) {
				realmNames.add(realm.getRealm_name());
				break;
			}
		}
		subjectCache.reInitSubjectMetadata(realmNames);
		log.debug("finished initializing realm metadata");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Finished Initializing realm metadata"));
	}

	public void handleUpdateMetadata() {
		int realmId = Integer.valueOf(manageSubjectsViewBean.getSelectedRealmName());
		List<String> realmNames = new ArrayList<String>();
		for (PalmsRealm realm : manageSubjectsViewBean.getRealms()) {
			if (realmId == realm.getRealm_id()) {
				realmNames.add(realm.getRealm_name());
				break;
			}
		}
		int newSubjects = subjectCache.addNewSubjectMetadata(realmNames);
		log.debug("finished Updating realm metadata");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Finished Updating realm metadata", "added " + newSubjects
				+ " new users"));
	}

	public void handleRefreshMetadata() {
		if (manageSubjectsViewBean != null) {
			ExternalSubjectDataObj obj = manageSubjectsViewBean.getSelectedRealmUser();
			if (subjectCache != null && obj != null) {
				subjectCache.refreshSubjectMetadata(obj.getUsername(), obj.getRealm_id());
				handleGetSubjects();
			}
		}
	}

	public void handleDeleteRoleContextInit() {
		log.debug("Handle Delete Roles here");
		manageSubjectsViewBean.setRoleContexts(roleContextDao.findRoleContextsBySubject(manageSubjectsViewBean
				.getSelectedSubject().getSubject_id()));
	}

	public void handleEntityWizardDialogInit() {
		handleEditRoles();
		List<RoleContext> contexts = manageSubjectsViewBean.getRoleContexts();
		for (RoleContext rc : contexts) {
			if (rc.isIsdefault()) {
				manageSubjectsViewBean.setSelectedContext(rc);
				manageSubjectsViewBean.setSelectedContextId(String.valueOf(rc.getRole_context_id()));
				break;
			}
		}
		initCompanyLists();

	}

	public void handleViewEntitiesInit() {
		log.debug("In handleViewEntitiesInit().  before handleEditRoles()");
		handleEditRoles();
		log.debug("In handleViewEntitiesInit().  after handleEditRoles()");

		// Set the default RoleContext selected by default
		List<RoleContext> contexts = manageSubjectsViewBean.getRoleContexts();
		for (RoleContext rc : contexts) {
			if (rc.isIsdefault()) {
				manageSubjectsViewBean.setSelectedContext(rc);
				manageSubjectsViewBean.setSelectedContextId(String.valueOf(rc.getRole_context_id()));
			}
		}
		log.debug("About to get Authorized Companies");
		List<Integer> authCos = entityService.getAuthorizedEntities(manageSubjectsViewBean.getSelectedContext(),
				EntityType.COMPANY_TYPE, false);
		List<Company> companies = companyDao.findAllCompaniesFiltered(authCos);
		TreeNode root = new DefaultTreeNode("root", null);
		TreeNode companyContainer = new DefaultTreeNode("container", new EntityTreeNode("Companies", "container", "-",
				-1), root);
		companyContainer.setSelectable(false);
		companyContainer.setExpanded(true);
		for (Company c : companies) {
			TreeNode company = new DefaultTreeNode("company", new EntityTreeNode(c.getCoName(), "company",
					String.valueOf(c.getCompanyId()), c.getCompanyId()), companyContainer);

			TreeNode projects = new DefaultTreeNode("container", new EntityTreeNode("Projects", "container", "-", -1),
					company);
			TreeNode placeholderProject = new DefaultTreeNode(new EntityTreeNode("---", "bogus", "-", -1), projects);
			placeholderProject.setSelectable(false);
			projects.setSelectable(false);
			log.trace("Adding company to Tree: {}", c.getCoName());
		}
		manageSubjectsViewBean.setRootNodeView(root);
		log.debug("Size of root child list: {}", root.getChildCount());
		log.debug("Size of Child list of company container: {}", companyContainer.getChildCount());

	}

	public void handleEntitiesInit() {
		// handleEditRoles();
		log.debug("In handleEntitiesInit()");
		manageSubjectsViewBean.setUnSelectedNodes(new ArrayList<TreeNode>());
		manageSubjectsViewBean.setAllSelectedNodes(new ArrayList<TreeNode>());

		log.debug("About to get Authorized Companies");
		// List<Integer> authCos =
		// entityService.getAuthorizedEntities(manageSubjectsViewBean.getSelectedContext(),
		// "co");

		// List<Company> companies = companyDao.findAllCompaniesOrdered();
		List<CompanyEntityDataObj> companies = manageSubjectsViewBean.getAllowedCompanies();
		log.debug("Got {} companies", companies.size());

		TreeNode root = new DefaultTreeNode("root", null);
		TreeNode companyContainer = new DefaultTreeNode("container", new EntityTreeNode("Companies", "container", "-",
				-1), root);
		companyContainer.setSelectable(false);
		for (CompanyEntityDataObj c : companies) {
			TreeNode company;
			company = new DefaultTreeNode("company", new EntityTreeNode(c.getConame(), "company", String.valueOf(c
					.getCompanyId()), c.getCompanyId(), c.getEntityRuleId(), c.getEntityRuleName()), companyContainer);
			company.setSelectable(false);

			if (c.getEntityRuleName().equalsIgnoreCase("Allow")) {
				TreeNode projects = new DefaultTreeNode("container", new EntityTreeNode("Projects", "container", "-",
						-1), company);
				TreeNode placeholderProject = new DefaultTreeNode(new EntityTreeNode("---", "bogus", "-", -1), projects);
				placeholderProject.setSelectable(false);
				projects.setSelectable(false);
			}

			log.trace("Adding company to Tree: {}", c.getConame());
		}

		manageSubjectsViewBean.setRootNode(root);
		log.debug("Size of root child list: {}", root.getChildCount());
		log.debug("Size of Child list of company container: {}", companyContainer.getChildCount());

	}

	public void handleNodeExpandForView(NodeExpandEvent event) {
		log.debug("handle expanding tree node here");
		EntityTreeNode node = (EntityTreeNode) event.getTreeNode().getData();
		if (node.getEntityType().equalsIgnoreCase("container") && node.getEntityName().equalsIgnoreCase("Projects")) {
			if (event.getTreeNode().getChildCount() == 1) {
				List<Integer> authPrjs = entityService.getAuthorizedEntities(
						manageSubjectsViewBean.getSelectedContext(), EntityType.PROJECT_TYPE, false);
				log.debug("Authorized Projects: {}", authPrjs);
				EntityTreeNode parentNode = (EntityTreeNode) event.getTreeNode().getParent().getData();
				List<Project> projects = projectDao.findAuthorizedByCompanyId(parentNode.getEntityId(), authPrjs);
				for (Project p : projects) {
					AuthorizedEntity ap = entityService.getAuthorizedEntity(manageSubjectsViewBean.getSelectedContext()
							.getRole_context_id(), manageSubjectsViewBean.getProjectType().getEntity_type_id(), p
							.getProjectId());
					TreeNode project = new DefaultTreeNode("project", new EntityTreeNode(p.getName(), "project",
							String.valueOf(p.getProjectId()), p.getProjectId(), ap.getEntityRule().getEntity_rule_id(),
							ap.getEntityRule().getEntity_rule_name()), event.getTreeNode());

					TreeNode placeholderUser = new DefaultTreeNode(new EntityTreeNode("---", "bogus", "-", -1), project);
					placeholderUser.setSelectable(false);
					event.getTreeNode().setExpanded(true);
					log.trace("Adding project to Tree: {}", p.getName());
				}
			}

		} else if (node.getEntityType().equalsIgnoreCase("project")) {
			// check to see if the node has already been expanded
			if (event.getTreeNode().getChildCount() == 1) {
				List<Integer> authUsers = entityService.getAuthorizedEntities(
						manageSubjectsViewBean.getSelectedContext(), EntityType.USER_TYPE, false);

				List<ProjectUser> projectUsers = projectUserDao.findProjectUsersFiltered(node.getEntityId(), authUsers);
				for (ProjectUser pu : projectUsers) {
					AuthorizedEntity au = entityService.getAuthorizedEntity(manageSubjectsViewBean.getSelectedContext()
							.getRole_context_id(), manageSubjectsViewBean.getUserType().getEntity_type_id(), pu
							.getUser().getUsersId());
					User u = pu.getUser();
					TreeNode user = new DefaultTreeNode("user", new EntityTreeNode(u.getFirstname() + " "
							+ u.getLastname() + " (" + u.getEmail() + ")", "user", String.valueOf(u.getUsersId()),
							u.getUsersId(), au.getEntityRule().getEntity_rule_id(), au.getEntityRule()
									.getEntity_rule_name()), event.getTreeNode());

					event.getTreeNode().setExpanded(true);
					log.trace("Adding user to Tree: {}", u.getUserCoursePrefs());
				}
			}
		}
	}

	public void handleNodeExpand(NodeExpandEvent event) {
		log.debug("handle expanding tree node here");
		EntityTreeNode node = (EntityTreeNode) event.getTreeNode().getData();

		if (node.getEntityType().equalsIgnoreCase("container") && node.getEntityName().equalsIgnoreCase("Projects")) {
			// check to see if the node has already been expanded so we don't
			// get duplicates in the tree
			if (event.getTreeNode().getChildCount() == 1) {
				List<Integer> authPrjs = entityService.getAuthorizedEntities(
						manageSubjectsViewBean.getSelectedContext(), EntityType.PROJECT_TYPE, false);
				log.debug("Authorized Projects: {}", authPrjs);
				EntityTreeNode parentNode = (EntityTreeNode) event.getTreeNode().getParent().getData();
				Company c = companyDao.findById(parentNode.getEntityId());
				List<Project> projects = c.getProjects();
				for (Project p : projects) {

					TreeNode project;
					AuthorizedEntity ap = new AuthorizedEntity();
					if (authPrjs.contains(p.getProjectId())) {
						ap = entityService.getAuthorizedEntity(manageSubjectsViewBean.getSelectedContext()
								.getRole_context_id(), manageSubjectsViewBean.getProjectType().getEntity_type_id(), p
								.getProjectId());
						project = new DefaultTreeNode("project", new EntityTreeNode(p.getName(), "project",
								String.valueOf(p.getProjectId()), p.getProjectId(), ap.getEntityRule()
										.getEntity_rule_id(), ap.getEntityRule().getEntity_rule_name()),
								event.getTreeNode());
						log.debug("Project {} is authorized.  Setting project selected", p.getName());
						project.setSelected(true);
					} else {
						project = new DefaultTreeNode("project", new EntityTreeNode(p.getName(), "project",
								String.valueOf(p.getProjectId()), p.getProjectId()), event.getTreeNode());
					}
					TreeNode placeholderUser;
					if (project.isSelected() && !(ap.getEntityRule().getEntity_rule_name() == null)) {
						if (ap.getEntityRule().getEntity_rule_name().equalsIgnoreCase("AllowChildren")) {
							log.debug("found project with allowChildren rule.  don't make expandable");
						} else {
							placeholderUser = new DefaultTreeNode(new EntityTreeNode("---", "bogus", "-", -1), project);
							placeholderUser.setSelectable(false);
						}
					} else {
						placeholderUser = new DefaultTreeNode(new EntityTreeNode("---", "bogus", "-", -1), project);
						placeholderUser.setSelectable(false);
					}

					event.getTreeNode().setExpanded(true);
					log.trace("Adding project to Tree: {}", p.getName());
				}
			}
		} else if (node.getEntityType().equalsIgnoreCase("project")) {
			// check to see if the node has already been expanded
			if (event.getTreeNode().getChildCount() == 1) {
				List<Integer> authUsers = entityService.getAuthorizedEntities(
						manageSubjectsViewBean.getSelectedContext(), EntityType.USER_TYPE, false);
				Project p = projectDao.findById(node.getEntityId());
				List<ProjectUser> projectUsers = p.getProjectUsers();
				for (ProjectUser pu : projectUsers) {
					User u = pu.getUser();
					TreeNode user;
					if (authUsers.contains(u.getUsersId())) {
						AuthorizedEntity au = entityService.getAuthorizedEntity(manageSubjectsViewBean
								.getSelectedContext().getRole_context_id(), manageSubjectsViewBean.getUserType()
								.getEntity_type_id(), u.getUsersId());
						user = new DefaultTreeNode("user", new EntityTreeNode(u.getFirstname() + " " + u.getLastname()
								+ " (" + u.getEmail() + ")", "user", String.valueOf(u.getUsersId()), u.getUsersId(), au
								.getEntityRule().getEntity_rule_id(), au.getEntityRule().getEntity_rule_name()),
								event.getTreeNode());
						log.debug("user {} is authorized.  Setting user selected", u.getUsername());
						user.setSelected(true);
					} else {
						user = new DefaultTreeNode("user", new EntityTreeNode(u.getFirstname() + " " + u.getLastname()
								+ " (" + u.getEmail() + ")", "user", String.valueOf(u.getUsersId()), u.getUsersId()),
								event.getTreeNode());
					}
					event.getTreeNode().setExpanded(true);
					log.trace("Adding user to Tree: {}", u.getUserCoursePrefs());
				}
			}
		}
	}

	public void handleNodeSelect(NodeSelectEvent event) {
		log.debug("handle selecting tree node here");
		// If selected node was previously unselected...
		if (manageSubjectsViewBean.getUnSelectedNodes().contains(event.getTreeNode())) {
			manageSubjectsViewBean.getUnSelectedNodes().remove(event.getTreeNode());
		}
		if (!manageSubjectsViewBean.getAllSelectedNodes().contains(event.getTreeNode())) {
			manageSubjectsViewBean.getAllSelectedNodes().add(event.getTreeNode());
		}
		EntityTreeNode node = (EntityTreeNode) event.getTreeNode().getData();
		if (node.getEntityType().equalsIgnoreCase("user")) {
			log.debug("Handling User Selection");
			// event.getTreeNode().getParent().setSelected(true);
		} else if (node.getEntityType().equalsIgnoreCase("project")) {
			// event.getTreeNode().getParent().getParent().setSelected(true);

			event.getTreeNode().setExpanded(false);

		} else if (node.getEntityType().equalsIgnoreCase("company")) {
			event.getTreeNode().setExpanded(false);
		}

	}

	public void handleNodeCollapse(NodeCollapseEvent event) {
		log.debug("Handling Node Collapse: {}", event.getClass());
		EntityTreeNode node = (EntityTreeNode) event.getTreeNode().getData();
		log.debug("Collapsed Node type: {}, id: {}", node.getEntityType(), node.getEntityId());
		event.getTreeNode().setExpanded(false);
	}

	public void handleNodeUnselect(NodeUnselectEvent event) {
		log.debug("Handling Node Unselect: {}", event.getClass());
		EntityTreeNode node = (EntityTreeNode) event.getTreeNode().getData();
		log.debug("Unselected Node type: {}, id: {}", node.getEntityType(), node.getEntityId());
		if (!(manageSubjectsViewBean.getUnSelectedNodes().contains(event.getTreeNode()))) {
			manageSubjectsViewBean.getUnSelectedNodes().add(event.getTreeNode());
		}
		if (manageSubjectsViewBean.getAllSelectedNodes().contains(event.getTreeNode())) {
			manageSubjectsViewBean.getAllSelectedNodes().remove(event.getTreeNode());
		}

		if (event.getTreeNode().getChildCount() > 0) {
			List<TreeNode> childNodes = event.getTreeNode().getChildren();
			for (TreeNode n : childNodes) {
				if (n.isSelected()) {
					n.setSelected(false);
					/*if (!(manageSubjectsViewBean.getUnSelectedNodes().contains(n))){
						manageSubjectsViewBean.getUnSelectedNodes().add(n);
					}*/
				}
			}
		}
		event.getTreeNode().setSelected(false);

	}

	public void handleSaveSelectedAuthorizedEntities() {
		// TreeNode[] selectedNodes = manageSubjectsViewBean.getSelectedNodes();
		Subject subject = SecurityUtils.getSubject();
		List<TreeNode> allSelectedNodes = manageSubjectsViewBean.getAllSelectedNodes();
		TreeNode root = manageSubjectsViewBean.getRootNode();
		List<TreeNode> unSelectedNodes = manageSubjectsViewBean.getUnSelectedNodes();
		RoleContext context = manageSubjectsViewBean.getSelectedContext();
		// Do Removal of Unselected entities
		for (TreeNode node : unSelectedNodes) {
			EntityTreeNode entityDataObj = (EntityTreeNode) node.getData();
			if (entityDataObj.getEntityType().equalsIgnoreCase("company")) {
				// doing this in Transfer event of Company Picklist
				/*if (entityService.isAuthorized(context, "co", entityDataObj.getEntityId())){
					context.getEntities().remove(entityService.getAuthorizedEntity(context, "co", entityDataObj.getEntityId()));
				}*/
			}
			if (entityDataObj.getEntityType().equalsIgnoreCase("project")) {
				if (entityService.isAuthorized(context, EntityType.PROJECT_TYPE, entityDataObj.getEntityId(), true)) {
					// context.getEntities().remove(entityService.getAuthorizedEntity(context.getRole_context_id(),
					// manageSubjectsViewBean.getProjectType().getEntity_type_id(),
					// entityDataObj.getEntityId()));
					entityService.revokeEntityAccess(context, entityDataObj.getEntityId(), EntityType.PROJECT_TYPE,
							subject.getPrincipal().toString());
				}
			}
			if (entityDataObj.getEntityType().equalsIgnoreCase("user")) {
				log.debug("found unselected user: {}", entityDataObj.getEntityName());
				if (entityService.isAuthorized(context, EntityType.USER_TYPE, entityDataObj.getEntityId(), true)) {
					log.debug("user is authorized, remove from context");
					// context.getEntities().remove(entityService.getAuthorizedEntity(context.getRole_context_id(),
					// manageSubjectsViewBean.getUserType().getEntity_type_id(),
					// entityDataObj.getEntityId()));
					// this goes to audit log
					entityService.revokeEntityAccess(context, entityDataObj.getEntityId(), EntityType.PROJECT_TYPE,
							subject.getPrincipal().toString());
					log.debug("finished removing authorizedEntity");
				}
			}
		}
		//
		for (TreeNode node : allSelectedNodes) {
			EntityTreeNode entityDataObj = (EntityTreeNode) node.getData();
			log.debug("Selected Node: {}, Type: {}", entityDataObj.getEntityName(), entityDataObj.getEntityType());

			if (entityDataObj.getEntityType().equalsIgnoreCase("project")) {
				// check if it's already authorized
				AuthorizedEntity project = new AuthorizedEntity();

				AuthorizedEntity company = new AuthorizedEntity();
				EntityTreeNode parentCo = (EntityTreeNode) node.getParent().getParent().getData();
				company = entityService.getAuthorizedEntity(context, EntityType.COMPANY_TYPE, parentCo.getEntityId());

				project.setEntity_id(entityDataObj.getEntityId());
				project.setEntityType(manageSubjectsViewBean.getProjectType());
				project.setRoleContext(context);
				project.setSubject_id(manageSubjectsViewBean.getSelectedSubject().getSubject_id());
				project.setEntityRule(entityRuleDao.findEntityRuleByName("AllowChildren"));
				// project.setRoles(context.getRoles());
				project.setParentEntity(company);

				context = entityService.grantEntityAccess(context.getRole_context_id(), project, subject.getPrincipal()
						.toString());

			} else if (entityDataObj.getEntityType().equalsIgnoreCase("user")) {
				// AuthorizedEntity project = new AuthorizedEntity();
				AuthorizedEntity company = new AuthorizedEntity();
				AuthorizedEntity user = new AuthorizedEntity();
				EntityTreeNode parentProj = (EntityTreeNode) node.getParent().getData();
				EntityTreeNode parentCo = (EntityTreeNode) node.getParent().getParent().getParent().getData();
				company = entityService.getAuthorizedEntity(context, "co", parentCo.getEntityId());
				AuthorizedEntity project = entityService.getAuthorizedEntity(context, EntityType.PROJECT_TYPE,
						parentProj.getEntityId());
				if (project == null) {
					project = new AuthorizedEntity();
					project.setEntityRule(entityRuleDao.findEntityRuleByName("Allow"));
					project.setEntity_id(parentProj.getEntityId());
					project.setEntityType(manageSubjectsViewBean.getProjectType());
					project.setRoleContext(context);
					project.setSubject_id(manageSubjectsViewBean.getSelectedSubject().getSubject_id());
					project.setEntityRule(entityRuleDao.findEntityRuleByName("Allow"));
					// project.setRoles(context.getRoles());
					project.setParentEntity(company);
				}
				user.setEntity_id(entityDataObj.getEntityId());
				user.setEntityType(manageSubjectsViewBean.getUserType());
				user.setRoleContext(context);
				user.setSubject_id(manageSubjectsViewBean.getSelectedSubject().getSubject_id());
				user.setEntityRule(entityRuleDao.findEntityRuleByName("Allow"));
				// user.setRoles(context.getRoles());
				user.setParentEntity(project);
				context = entityService.grantEntityAccess(context.getRole_context_id(), user, subject.getPrincipal()
						.toString());

			}

		}
		// test 2
		// context = roleContextDao.update(context);
		// roleContextDao.evictClass(RoleContext.class);
		// manageSubjectsViewBean.setSelectedContext(context);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("Saved Authorized Entities for Context", "complete"));

	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public PalmsRealmDao getRealmDao() {
		return realmDao;
	}

	public void setRealmDao(PalmsRealmDao realmDao) {
		this.realmDao = realmDao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ManageSubjectsViewBean getManageSubjectsViewBean() {
		return manageSubjectsViewBean;
	}

	public void setManageSubjectsViewBean(ManageSubjectsViewBean manageSubjectsViewBean) {
		this.manageSubjectsViewBean = manageSubjectsViewBean;
	}

	public PalmsSubjectDao getSubjectDao() {
		return subjectDao;
	}

	public void setSubjectDao(PalmsSubjectDao subjectDao) {
		this.subjectDao = subjectDao;
	}

	public RoleContextDao getRoleContextDao() {
		return roleContextDao;
	}

	public void setRoleContextDao(RoleContextDao roleContextDao) {
		this.roleContextDao = roleContextDao;
	}

	public RoleConverter getRoleConverter() {
		return roleConverter;
	}

	public void setRoleConverter(RoleConverter roleConverter) {
		this.roleConverter = roleConverter;
	}

	public ExternalSubjectServiceLocal getSubjectService() {
		return subjectService;
	}

	public void setSubjectService(ExternalSubjectServiceLocal subjectService) {
		this.subjectService = subjectService;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public CompanyConverter getCompanyConverter() {
		return companyConverter;
	}

	public void setCompanyConverter(CompanyConverter companyConverter) {
		this.companyConverter = companyConverter;
	}

	public ProjectUserDao getProjectUserDao() {
		return projectUserDao;
	}

	public void setProjectUserDao(ProjectUserDao projectUserDao) {
		this.projectUserDao = projectUserDao;
	}

	public EntityRuleDao getEntityRuleDao() {
		return entityRuleDao;
	}

	public void setEntityRuleDao(EntityRuleDao entityRuleDao) {
		this.entityRuleDao = entityRuleDao;
	}

	public AuthorizedEntityDao getEntityDao() {
		return entityDao;
	}

	public void setEntityDao(AuthorizedEntityDao entityDao) {
		this.entityDao = entityDao;
	}

}
