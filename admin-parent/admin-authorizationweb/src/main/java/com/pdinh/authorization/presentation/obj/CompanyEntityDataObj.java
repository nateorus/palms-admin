package com.pdinh.authorization.presentation.obj;

import java.io.Serializable;

public class CompanyEntityDataObj implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String coname;
	private int companyId;
	private int entityRuleId;
	private String entityRuleName;
	
	public CompanyEntityDataObj(){
		
	}
	
	public CompanyEntityDataObj(String coname, String entityRuleName, Integer companyId, Integer entityRuleId){
		this.coname = coname;
		this.entityRuleName = entityRuleName;
		this.companyId = companyId;
		this.entityRuleId = entityRuleId;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getEntityRuleId() {
		return entityRuleId;
	}
	public void setEntityRuleId(int entityRuleId) {
		this.entityRuleId = entityRuleId;
	}
	public String getEntityRuleName() {
		return entityRuleName;
	}
	public void setEntityRuleName(String entityRuleName) {
		this.entityRuleName = entityRuleName;
	}

}
