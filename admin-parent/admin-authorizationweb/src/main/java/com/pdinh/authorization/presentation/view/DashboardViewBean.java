package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.pdinh.auth.persistence.entity.Permission;
import com.pdinh.auth.persistence.entity.PermissionGroup;
import com.pdinh.auth.persistence.entity.Role;

/**
 * Session Bean implementation class DashboardViewBean
 */
@ManagedBean(name = "dashboardViewBean")
@ViewScoped
public class DashboardViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String uid;
	private String cn;
	private String firstname;
	private String email;
	private List<String> objectClasses;
	private List<String> pdinhRoles;
	private List<String> groupDNs;
	private String oldPassword;
	private String newPassword;
	private String newPasswordConfirm;
	private List<Permission> permissions;
	private List<PermissionGroup> permissionGroups;
	private PermissionGroup newGroup = new PermissionGroup();
	private PermissionGroup selectedGroup = new PermissionGroup();
	private DualListModel<Permission> createPermissionsGroupLists;
	private List<String> userPermissions; 
	private Permission selectedPermission;
	private List<Permission> filteredPermissions;
	private boolean contextMenuDisabled = false;
	private List<Role> rolesWPermission;
	private List<PermissionGroup> permGrpsWPermission;
	private Role selectedRole;
	private PermissionGroup selectedPermissionGroup;
	
    /**
     * Default constructor. 
     */
    public DashboardViewBean() {
        // TODO Auto-generated constructor stub
    }
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<String> getPdinhRoles() {
		return pdinhRoles;
	}
	public void setPdinhRoles(List<String> pdinhRoles) {
		this.pdinhRoles = pdinhRoles;
	}
	public List<String> getObjectClasses() {
		return objectClasses;
	}
	public void setObjectClasses(List<String> objectClasses) {
		this.objectClasses = objectClasses;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}
	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}
	public List<String> getGroupDNs() {
		return groupDNs;
	}
	public void setGroupDNs(List<String> groupDNs) {
		this.groupDNs = groupDNs;
	}
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	public List<PermissionGroup> getPermissionGroups() {
		return permissionGroups;
	}
	public void setPermissionGroups(List<PermissionGroup> permissionGroups) {
		this.permissionGroups = permissionGroups;
	}
	public DualListModel<Permission> getCreatePermissionsGroupLists() {
		return createPermissionsGroupLists;
	}
	public void setCreatePermissionsGroupLists(DualListModel<Permission> createPermissionsGroupLists) {
		this.createPermissionsGroupLists = createPermissionsGroupLists;
	}
	public PermissionGroup getNewGroup() {
		return newGroup;
	}
	public void setNewGroup(PermissionGroup newGroup) {
		this.newGroup = newGroup;
	}
	public PermissionGroup getSelectedGroup() {
		return selectedGroup;
	}
	public void setSelectedGroup(PermissionGroup selectedGroup) {
		this.selectedGroup = selectedGroup;
	}
	public List<String> getUserPermissions() {
		return userPermissions;
	}
	public void setUserPermissions(List<String> userPermissions) {
		this.userPermissions = userPermissions;
	}
	public Permission getSelectedPermission() {
		return selectedPermission;
	}
	public void setSelectedPermission(Permission selectedPermission) {
		this.selectedPermission = selectedPermission;
	}
	public List<Permission> getFilteredPermissions() {
		return filteredPermissions;
	}
	public void setFilteredPermissions(List<Permission> filteredPermissions) {
		this.filteredPermissions = filteredPermissions;
	}
	public boolean isContextMenuDisabled() {
		return contextMenuDisabled;
	}
	public void setContextMenuDisabled(boolean contextMenuDisabled) {
		this.contextMenuDisabled = contextMenuDisabled;
	}
	public List<Role> getRolesWPermission() {
		return rolesWPermission;
	}
	public void setRolesWPermission(List<Role> rolesWPermission) {
		this.rolesWPermission = rolesWPermission;
	}
	public List<PermissionGroup> getPermGrpsWPermission() {
		return permGrpsWPermission;
	}
	public void setPermGrpsWPermission(List<PermissionGroup> permGrpsWPermission) {
		this.permGrpsWPermission = permGrpsWPermission;
	}
	public Role getSelectedRole() {
		return selectedRole;
	}
	public void setSelectedRole(Role selectedRole) {
		this.selectedRole = selectedRole;
	}
	public PermissionGroup getSelectedPermissionGroup() {
		return selectedPermissionGroup;
	}
	public void setSelectedPermissionGroup(PermissionGroup selectedPermissionGroup) {
		this.selectedPermissionGroup = selectedPermissionGroup;
	}


}
