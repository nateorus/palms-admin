package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.SessionBean;

import com.pdinh.authorization.presentation.util.PermissionConverter;
import com.pdinh.authorization.presentation.view.PermissionGroupsViewBean;

import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.PermissionDao;
import com.pdinh.auth.data.dao.PermissionGroupDao;

import com.pdinh.auth.persistence.entity.Permission;
import com.pdinh.auth.persistence.entity.PermissionGroup;

/**
 * Session Bean implementation class DashboardControllerBean
 */
@ManagedBean(name = "permissionGroupsControllerBean")
@ViewScoped
public class PermissionGroupsControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(PermissionGroupsControllerBean.class);
	
	//private Subject subject;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(name = "permissionGroupsViewBean", value = "#{permissionGroupsViewBean}")
	private PermissionGroupsViewBean permissionGroupsViewBean;
	
	@ManagedProperty(name = "permissionConverter", value="#{permissionConverter}")
	private PermissionConverter permissionConverter;

	
	@EJB
	private PermissionDao permissionDao;
	
	@EJB
	private PermissionGroupDao permissionGroupDao;
	
	@EJB
	private PermissionServiceLocal permissionService;
	
    public PermissionGroupsControllerBean() {
        // TODO Auto-generated constructor stub
    }
    public void initView(){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("In initView()");
    	
    	//only initialize these once
    	if (permissionGroupsViewBean.getCreatePermissionsGroupLists() == null){
    		permissionGroupsViewBean.setPermissions(permissionDao.findAll(true));
        	permissionGroupsViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
    		handleClearSelectedPermissions();
    		
        	//permissionGroupsViewBean.setUserPermissions(permissionService.listPermissionNamesInContext(sessionBean.getRoleContext()));
    	}
    	if (subject.isPermitted("createPermissionGroups")){
    		permissionGroupsViewBean.setCreateButtonDisabled(false);
    	}
    	
    	
    }

    
    public void handlePermissionGroupSelect(SelectEvent event){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("Handle Selecting a permission Group here");
    	//PermissionGroup pgroup = permissionGroupsViewBean.getSelectedGroup();
    	//log.debug("selected group: {}", pgroup.getName());
    	if (subject.isPermitted("editPermissionGroups")){
    		permissionGroupsViewBean.setEditButtonDisabled(false);
    	}
    	if (subject.isPermitted("deletePermissionGroups")){
    		permissionGroupsViewBean.setDeleteButtonDisabled(false);
    	}
    }


    public void handleInitWizardCreate(){
    	handleClearSelectedPermissions();
    	PermissionGroup aNewGroup = new PermissionGroup();
    	permissionGroupsViewBean.setNewGroup(aNewGroup);
    	permissionGroupsViewBean.setRenderEditGroupButton(false);
    	permissionGroupsViewBean.setRenderSaveGroupButton(true);
    	permissionGroupsViewBean.setRenderGrpNameInputForNew(true);
    	permissionGroupsViewBean.setRenderGrpNameInputForEdit(false);
    }
    public void handleInitWizardEdit(){
    	List<Permission> plist = permissionGroupsViewBean.getSelectedGroup().getPermissions();
    	if (plist.isEmpty()){
    		log.debug("In handleInitWizardEdit:  Permission list is empty");
    	}
    	List<Permission> availablePermissions = permissionGroupsViewBean.getPermissions();
    	for (Permission p: plist){
    		availablePermissions = removePermission(availablePermissions, p.getPermission_id());
    	}
    	permissionGroupsViewBean.setCreatePermissionsGroupLists(new DualListModel<Permission>(availablePermissions, plist));
    	log.debug("Init Wizard for Edit.  Set Lists from Group: {}", permissionGroupsViewBean.getSelectedGroup().getName());
    	permissionGroupsViewBean.setNewGroup(permissionGroupsViewBean.getSelectedGroup());
    	permissionGroupsViewBean.setRenderEditGroupButton(true);
    	permissionGroupsViewBean.setRenderSaveGroupButton(false);
    	permissionGroupsViewBean.setRenderGrpNameInputForNew(false);
    	permissionGroupsViewBean.setRenderGrpNameInputForEdit(true);
    }
    public List<Permission> removePermission(List<Permission> list, Integer permissionId){
    	for (Permission p:list){
    		if (p.getPermission_id() == permissionId){
    			list.remove(p);
    			return list;
    		}
    	}
    	return list;
    }
    public void handleSavePermissionList(){
    	log.debug("Handle Saving Permission List here");
    	//PermissionGroup aNewGroup = new PermissionGroup();
    	List <Permission> pList = permissionGroupsViewBean.getCreatePermissionsGroupLists().getTarget();
    	permissionGroupsViewBean.getNewGroup().setPermissions(pList);
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Saved Permission List", "complete"));

    }
    public void handleSubmitNewGroup(){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("Handle Persistence of a new Permission Group here");
    	permissionGroupsViewBean.getNewGroup().setCreated_by(sessionBean.getUid());
    	PermissionGroup pgrp = permissionGroupsViewBean.getNewGroup();
    	log.debug("Name: {}", pgrp.getName());
    	log.debug("Label: {}", pgrp.getLabel());
    	log.debug("Description: {}", pgrp.getDescription());
    	log.debug("ID: {}", pgrp.getPermission_group_id());
    	log.debug("Custom: {}", pgrp.getCustom());
    	permissionGroupDao.createAudit(pgrp, subject.getPrincipal().toString());
    	permissionGroupsViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
    	//handleClearSelectedPermissions();
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Created New Group", pgrp.getLabel()));
    	
    	
    }
    public void handleSubmitEditGroup(){
    	Subject subject = SecurityUtils.getSubject();
    	log.debug("Handle Persistence of edited Permission Group here");
    	PermissionGroup pgrp = permissionGroupsViewBean.getNewGroup();
    	pgrp.setCreated_by(sessionBean.getUid());
    	permissionGroupDao.updateAudit(pgrp, subject.getPrincipal().toString());
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Edited Group", pgrp.getLabel()));
    }
    public void handleClearSelectedPermissions(){
    	permissionGroupsViewBean.setCreatePermissionsGroupLists(new DualListModel<Permission>(permissionGroupsViewBean.getPermissions(), new ArrayList<Permission>()));
    }
    public void handleDeletePermissionGroup(){
    	Subject subject = SecurityUtils.getSubject();
    	PermissionGroup group = permissionGroupsViewBean.getSelectedGroup();
    	if (permissionService.canDeletePermissionGroup(group)){
    		permissionGroupDao.deleteAudit(group, subject.getPrincipal().toString());
        	permissionGroupsViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage("Deleted Permission Group", group.getLabel()));
    	}else{
    		FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage("Delete Permission Group Failed", "The group is contained in a Role"));
    	}
    	
    }
    public String handleFlowProcessEvent(FlowEvent event){
    	log.debug("Current wizard Step: {}", event.getOldStep());
    	log.debug("Next step: {}", event.getNewStep());
   
    	if (event.getOldStep().equalsIgnoreCase("selectPermissionsTab")){
    		List <Permission> pList = permissionGroupsViewBean.getCreatePermissionsGroupLists().getTarget();
    		if (pList.isEmpty()){
    			FacesContext context = FacesContext.getCurrentInstance();
            	context.addMessage(null, new FacesMessage("No Permissions Selected", "You must save some permissions"));
            	return event.getOldStep();
    		} else {
    			
    	    	permissionGroupsViewBean.getNewGroup().setPermissions(pList);
    			return event.getNewStep();
    		}
    	}

    	return event.getNewStep();
    }

	public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	public PermissionGroupsViewBean getPermissionGroupsViewBean() {
		return permissionGroupsViewBean;
	}
	public void setPermissionGroupsViewBean(PermissionGroupsViewBean permissionGroupsViewBean) {
		this.permissionGroupsViewBean = permissionGroupsViewBean;
	}

	public PermissionDao getPermissionDao() {
		return permissionDao;
	}
	public void setPermissionDao(PermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}
	public PermissionGroupDao getPermissionGroupDao() {
		return permissionGroupDao;
	}
	public void setPermissionGroupDao(PermissionGroupDao permissionGroupDao) {
		this.permissionGroupDao = permissionGroupDao;
	}
	public PermissionConverter getPermissionConverter() {
		return permissionConverter;
	}
	public void setPermissionConverter(PermissionConverter permissionConverter) {
		this.permissionConverter = permissionConverter;
	}
	public PermissionServiceLocal getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(PermissionServiceLocal permissionService) {
		this.permissionService = permissionService;
	}



}
