package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.SessionBean;
import com.pdinh.authorization.presentation.util.PermissionConverter;
import com.pdinh.authorization.presentation.view.DashboardViewBean;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.PermissionDao;
import com.pdinh.auth.data.dao.PermissionGroupDao;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.persistence.entity.Permission;
import com.pdinh.auth.persistence.entity.PermissionGroup;
import com.pdinh.auth.persistence.entity.Role;

/**
 * Session Bean implementation class DashboardControllerBean
 */
@ManagedBean(name = "dashboardControllerBean")
@ViewScoped
public class DashboardControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(DashboardControllerBean.class);
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(name = "dashboardViewBean", value = "#{dashboardViewBean}")
	private DashboardViewBean dashboardViewBean;
	
	@ManagedProperty(name = "permissionConverter", value="#{permissionConverter}")
	private PermissionConverter permissionConverter;
	//private LdapUserDao userDao;
	
	@EJB
	private PermissionDao permissionDao;
	
	@EJB
	private PermissionGroupDao permissionGroupDao;
	
	@EJB
	private PermissionServiceLocal permissionService;
	
	@EJB
	private GlobalRolesBean rolesBean;
	
    public DashboardControllerBean() {
        // TODO Auto-generated constructor stub
    }
    public void initView(){
    	Subject subject = SecurityUtils.getSubject();
    	/*
    	if (sessionBean.getUser() != null){
	    	dashboardViewBean.setCn(sessionBean.getUser().getAttributeValue("cn"));
	    	dashboardViewBean.setEmail(sessionBean.getUser().getAttributeValue("mail"));
	    	dashboardViewBean.setFirstname(sessionBean.getUser().getAttributeValue("givenName"));
	    	dashboardViewBean.setPdinhRoles(sessionBean.getUser().getManyAttributeValues("pdinhRoles"));
	    	dashboardViewBean.setUid(sessionBean.getUser().getAttributeValue("uid"));
	    	dashboardViewBean.setObjectClasses(sessionBean.getUser().getManyAttributeValues("objectClass"));
	    	
	    	userDao = new LdapUserDao();
	    	
	    	dashboardViewBean.setGroupDNs(userDao.findUserGroupDNs(sessionBean.getUser().getAttributeValue("uid")));
    	}else{
    		dashboardViewBean.setCn("none");
    		dashboardViewBean.setEmail("none");
    		dashboardViewBean.setFirstname("none");
    		dashboardViewBean.setUid(subject.getPrincipal().toString());
    	}
    	*/
    	dashboardViewBean.setUid(subject.getPrincipal().toString());
    	dashboardViewBean.setPermissions(permissionDao.findAll(true));
    	//dashboardViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
    	
    	//dashboardViewBean.setCreatePermissionsGroupLists(new DualListModel<Permission>(dashboardViewBean.getPermissions(), new ArrayList<Permission>()));
    	dashboardViewBean.setUserPermissions(permissionService.listPermissionNamesInContext(sessionBean.getRoleContext()));
    	
    }

    public void handlePermissionSelect(SelectEvent event){
    	log.debug("Handle selecting a permission here");
    	Permission perm = dashboardViewBean.getSelectedPermission();
    	if (perm == null){
    		log.debug("Perm is NULL");
    	}else{
    		log.debug("Selected Permission: {}", perm.getName());
    	}
    }
    
    public void getRolesForPermissions(){
    	Permission selectedPermission = dashboardViewBean.getSelectedPermission();
    	if (selectedPermission == null){
    		log.debug("selectedPermission is NULL");
    	}else{
    		log.debug("Selected Permission: {}", selectedPermission.getName());
    	}
    	List<Role> rolesWPermission = rolesBean.getRolesWithPermission(selectedPermission.getName());
    	log.debug("Found {} roles with permission {}", rolesWPermission.size(), selectedPermission.getName());
    	dashboardViewBean.setRolesWPermission(rolesWPermission);
    }
    
    public void getPermissionGroupsForPermissions(){
    	Permission selectedPermission = dashboardViewBean.getSelectedPermission();
    	List<PermissionGroup> allPGrp = permissionGroupDao.findPermissionGroupWithPermission(selectedPermission.getName());
    	
    	log.debug("Found {} permission groups with permission {}", allPGrp.size(), selectedPermission.getName());
    	dashboardViewBean.setPermGrpsWPermission(allPGrp);
    }


    public void handleUpdatePassword(){
    	log.debug("Handling password change: ?", dashboardViewBean.getNewPasswordConfirm());
    }
    public void handleCreatePermissionGroup(){
    	log.debug("Handle Creating a new Permission Group here");
    	PermissionGroup aNewGroup = new PermissionGroup();
    	List <Permission> pList = dashboardViewBean.getCreatePermissionsGroupLists().getTarget();
    	Iterator iter = pList.iterator();
    	while (iter.hasNext()){
    		Permission temp = (Permission)iter.next();
    		log.debug("Permission Object Name: {}", temp.getName());
    	}
    	aNewGroup.setPermissions(pList);
    	dashboardViewBean.setNewGroup(aNewGroup);
    }
    public void handleSubmitNewGroup(){
    	log.debug("Handle Persistence of a new Permission Group here");
    	dashboardViewBean.getNewGroup().setCreated_by(sessionBean.getUid());
    	PermissionGroup pgrp = dashboardViewBean.getNewGroup();
    	log.debug("Name: {}", pgrp.getName());
    	log.debug("Label: {}", pgrp.getLabel());
    	log.debug("Description: {}", pgrp.getDescription());
    	log.debug("ID: {}", pgrp.getPermission_group_id());
    	log.debug("Custom: {}", pgrp.getCustom());
    	permissionGroupDao.create(dashboardViewBean.getNewGroup());
    	dashboardViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
    	handleClearSelectedPermissions();
    	
    	
    }
    public void handleClearSelectedPermissions(){
    	dashboardViewBean.setCreatePermissionsGroupLists(new DualListModel<Permission>(dashboardViewBean.getPermissions(), new ArrayList<Permission>()));
    }
    public void handleDeletePermissionGroup(){
    	PermissionGroup group = dashboardViewBean.getSelectedGroup();
    	permissionGroupDao.delete(group);
    	dashboardViewBean.setPermissionGroups(permissionGroupDao.findAll(true));
    }
	public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	public DashboardViewBean getDashboardViewBean() {
		return dashboardViewBean;
	}
	public void setDashboardViewBean(DashboardViewBean dashboardViewBean) {
		this.dashboardViewBean = dashboardViewBean;
	}
	/*
	public LdapUserDao getUserDao() {
		return userDao;
	}
	public void setUserDao(LdapUserDao userDao) {
		this.userDao = userDao;
	}
	*/
	public PermissionDao getPermissionDao() {
		return permissionDao;
	}
	public void setPermissionDao(PermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}
	public PermissionGroupDao getPermissionGroupDao() {
		return permissionGroupDao;
	}
	public void setPermissionGroupDao(PermissionGroupDao permissionGroupDao) {
		this.permissionGroupDao = permissionGroupDao;
	}
	public PermissionConverter getPermissionConverter() {
		return permissionConverter;
	}
	public void setPermissionConverter(PermissionConverter permissionConverter) {
		this.permissionConverter = permissionConverter;
	}
	public PermissionServiceLocal getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(PermissionServiceLocal permissionService) {
		this.permissionService = permissionService;
	}


}
