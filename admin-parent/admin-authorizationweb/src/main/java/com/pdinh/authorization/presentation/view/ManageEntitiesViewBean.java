package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;

@ManagedBean(name = "manageEntitiesViewBean")
@ViewScoped
public class ManageEntitiesViewBean implements Serializable{
	private static final Logger log = LoggerFactory.getLogger(ManageEntitiesViewBean.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Timestamp now;
	
	private int subjectId;
	
	private int realmId;
	
	private PalmsSubject subject;
	
	private PalmsRealm realm;
	
	private RoleContext context;
	
	private List<AuthorizedEntity> entities;
	
	private AuthorizedEntity selectedEntity;
	
	private boolean editEntityButtonDisabled = true;
	
	private boolean deleteEntityButtonDisabled = true;
	
	private List<EntityType> types;
	
	private List<EntityRule> rules;
	
	private String selectedRuleId = "";
	
	private boolean selectedTimeBoundValueEdit = false;
	
	private boolean selectedTimeBoundValueCo = false;
	
	private boolean selectedTimeBoundValuePrj = false;
	
	private Date startTime;
	
	private Date endTime;
	
	private boolean startTimeDisabled = true;
	
	private boolean endTimeDisabled = true;
	
	DualListModel<Role> entityRoleLists;
	
	List<Role> sourceRoles;
	
	private boolean addCompanyButtonDisabled = false;
	private List<Company> allCompanies;
	private List<Company> permittedCompanies;
	private List<Company> nonPermittedCompanies;
	private String selectedCoId;
	private String selectedCoNameAddCoDlg = "";

	private List<Project> availableProjects;
	private List<Project> permittedProjects;
	private String selectedPrjId;
	private String selectedPrjNameAddPrjDlg = "";
	
	private String username;
	private String selectedCompanyEntityRuleId = "";
	private String selectedCompanyId = "";
	
	private String selectedCompanyIdPrjDlg = "";
	private String selectedProjectEntityRuleId = "";
	
	
	private List<ProjectUser> availableProjectUsers;
	private String selectedPrjUserId = "";
	private String selectedPrjIdPrjUserDlg = "";
	private String selectedCompanyIdPrjUserDlg = "";
	private boolean selectedTimeBoundValuePrjUser = false;
	private String selectedPrjNameAddPrjUserDlg = "";
	private String selectedProjectUserEntityRuleId = "";
	private String selectedProjectUserName = "";
	
	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public int getRealmId() {
		return realmId;
	}

	public void setRealmId(int realmId) {
		this.realmId = realmId;
	}

	public PalmsSubject getSubject() {
		return subject;
	}

	public void setSubject(PalmsSubject subject) {
		this.subject = subject;
	}

	public PalmsRealm getRealm() {
		return realm;
	}

	public void setRealm(PalmsRealm realm) {
		this.realm = realm;
	}

	public RoleContext getContext() {
		return context;
	}

	public void setContext(RoleContext context) {
		this.context = context;
	}

	public List<AuthorizedEntity> getEntities() {
		return entities;
	}

	public void setEntities(List<AuthorizedEntity> entities) {
		this.entities = entities;
	}

	public AuthorizedEntity getSelectedEntity() {
		
		return selectedEntity;
	}

	public void setSelectedEntity(AuthorizedEntity selectedEntity) {
		this.selectedEntity = selectedEntity;
	}

	public boolean isEditEntityButtonDisabled() {
		return editEntityButtonDisabled;
	}

	public void setEditEntityButtonDisabled(boolean editEntityButtonDisabled) {
		this.editEntityButtonDisabled = editEntityButtonDisabled;
	}

	public List<EntityType> getTypes() {
		return types;
	}

	public void setTypes(List<EntityType> types) {
		this.types = types;
	}

	public List<EntityRule> getRules() {
		return rules;
	}

	public void setRules(List<EntityRule> rules) {
		this.rules = rules;
	}

	public String getSelectedRuleId() {
		log.debug("Getting Selected Rule Id {}", selectedRuleId);
		return selectedRuleId;
	}

	public void setSelectedRuleId(String selectedRuleId) {
		log.debug("Setting Selected Rule Id {}", selectedRuleId);
		this.selectedRuleId = selectedRuleId;
	}


	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public boolean isStartTimeDisabled() {
		return startTimeDisabled;
	}

	public void setStartTimeDisabled(boolean startTimeDisabled) {
		this.startTimeDisabled = startTimeDisabled;
	}

	public boolean isEndTimeDisabled() {
		return endTimeDisabled;
	}

	public void setEndTimeDisabled(boolean endTimeDisabled) {
		this.endTimeDisabled = endTimeDisabled;
	}

	public DualListModel<Role> getEntityRoleLists() {
		return entityRoleLists;
	}

	public void setEntityRoleLists(DualListModel<Role> entityRoleLists) {
		this.entityRoleLists = entityRoleLists;
	}

	public List<Role> getSourceRoles() {
		return sourceRoles;
	}

	public void setSourceRoles(List<Role> sourceRoles) {
		this.sourceRoles = sourceRoles;
	}

	public boolean isAddCompanyButtonDisabled() {
		return addCompanyButtonDisabled;
	}

	public void setAddCompanyButtonDisabled(boolean addCompanyButtonDisabled) {
		this.addCompanyButtonDisabled = addCompanyButtonDisabled;
	}

	public List<Company> getAllCompanies() {
		return allCompanies;
	}

	public void setAllCompanies(List<Company> allCompanies) {
		this.allCompanies = allCompanies;
	}

	public List<Company> getPermittedCompanies() {
		return permittedCompanies;
	}

	public void setPermittedCompanies(List<Company> permittedCompanies) {
		this.permittedCompanies = permittedCompanies;
	}

	public List<Company> getNonPermittedCompanies() {
		return nonPermittedCompanies;
	}

	public void setNonPermittedCompanies(List<Company> nonPermittedCompanies) {
		this.nonPermittedCompanies = nonPermittedCompanies;
	}


	public String getSelectedCoId() {
		return selectedCoId;
	}

	public void setSelectedCoId(String selectedCoId) {
		this.selectedCoId = selectedCoId;
	}

	public List<Project> getAvailableProjects() {
		return availableProjects;
	}

	public void setAvailableProjects(List<Project> availableProjects) {
		this.availableProjects = availableProjects;
	}

	public String getSelectedPrjId() {
		return selectedPrjId;
	}

	public void setSelectedPrjId(String selectedPrjId) {
		this.selectedPrjId = selectedPrjId;
	}

	public List<Project> getPermittedProjects() {
		return permittedProjects;
	}

	public void setPermittedProjects(List<Project> permittedProjects) {
		this.permittedProjects = permittedProjects;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSelectedCompanyEntityRuleId() {
		return selectedCompanyEntityRuleId;
	}

	public void setSelectedCompanyEntityRuleId(
			String selectedCompanyEntityRuleId) {
		this.selectedCompanyEntityRuleId = selectedCompanyEntityRuleId;
	}

	public String getSelectedCompanyId() {
		return selectedCompanyId;
	}

	public void setSelectedCompanyId(String selectedCompanyId) {
		this.selectedCompanyId = selectedCompanyId;
	}

	public String getSelectedCompanyIdPrjDlg() {
		return selectedCompanyIdPrjDlg;
	}

	public void setSelectedCompanyIdPrjDlg(String selectedCompanyIdPrjDlg) {
		this.selectedCompanyIdPrjDlg = selectedCompanyIdPrjDlg;
	}

	public String getSelectedProjectEntityRuleId() {
		return selectedProjectEntityRuleId;
	}

	public void setSelectedProjectEntityRuleId(
			String selectedProjectEntityRuleId) {
		this.selectedProjectEntityRuleId = selectedProjectEntityRuleId;
	}

	public boolean isDeleteEntityButtonDisabled() {
		return deleteEntityButtonDisabled;
	}

	public void setDeleteEntityButtonDisabled(boolean deleteEntityButtonDisabled) {
		this.deleteEntityButtonDisabled = deleteEntityButtonDisabled;
	}

	public boolean isSelectedTimeBoundValueEdit() {
		return selectedTimeBoundValueEdit;
	}

	public void setSelectedTimeBoundValueEdit(boolean selectedTimeBoundValueEdit) {
		this.selectedTimeBoundValueEdit = selectedTimeBoundValueEdit;
	}

	public boolean isSelectedTimeBoundValueCo() {
		return selectedTimeBoundValueCo;
	}

	public void setSelectedTimeBoundValueCo(boolean selectedTimeBoundValueCo) {
		this.selectedTimeBoundValueCo = selectedTimeBoundValueCo;
	}

	public boolean isSelectedTimeBoundValuePrj() {
		return selectedTimeBoundValuePrj;
	}

	public void setSelectedTimeBoundValuePrj(boolean selectedTimeBoundValuePrj) {
		this.selectedTimeBoundValuePrj = selectedTimeBoundValuePrj;
	}

	public String getSelectedCoNameAddCoDlg() {
		return selectedCoNameAddCoDlg;
	}

	public void setSelectedCoNameAddCoDlg(String selectedCoNameAddCoDlg) {
		this.selectedCoNameAddCoDlg = selectedCoNameAddCoDlg;
	}

	public String getSelectedPrjNameAddPrjDlg() {
		return selectedPrjNameAddPrjDlg;
	}

	public void setSelectedPrjNameAddPrjDlg(String selectedPrjNameAddPrjDlg) {
		this.selectedPrjNameAddPrjDlg = selectedPrjNameAddPrjDlg;
	}

	public Timestamp getNow() {
		return now;
	}

	public void setNow(Timestamp now) {
		this.now = now;
	}

	public List<ProjectUser> getAvailableProjectUsers() {
		return availableProjectUsers;
	}

	public void setAvailableProjectUsers(List<ProjectUser> availableProjectUsers) {
		this.availableProjectUsers = availableProjectUsers;
	}

	public String getSelectedPrjUserId() {
		return selectedPrjUserId;
	}

	public void setSelectedPrjUserId(String selectedPrjUserId) {
		this.selectedPrjUserId = selectedPrjUserId;
	}

	public String getSelectedPrjIdPrjUserDlg() {
		return selectedPrjIdPrjUserDlg;
	}

	public void setSelectedPrjIdPrjUserDlg(String selectedPrjIdPrjUserDlg) {
		this.selectedPrjIdPrjUserDlg = selectedPrjIdPrjUserDlg;
	}

	public String getSelectedCompanyIdPrjUserDlg() {
		return selectedCompanyIdPrjUserDlg;
	}

	public void setSelectedCompanyIdPrjUserDlg(
			String selectedCompanyIdPrjUserDlg) {
		this.selectedCompanyIdPrjUserDlg = selectedCompanyIdPrjUserDlg;
	}

	public boolean isSelectedTimeBoundValuePrjUser() {
		return selectedTimeBoundValuePrjUser;
	}

	public void setSelectedTimeBoundValuePrjUser(
			boolean selectedTimeBoundValuePrjUser) {
		this.selectedTimeBoundValuePrjUser = selectedTimeBoundValuePrjUser;
	}

	public String getSelectedPrjNameAddPrjUserDlg() {
		return selectedPrjNameAddPrjUserDlg;
	}

	public void setSelectedPrjNameAddPrjUserDlg(
			String selectedPrjNameAddPrjUserDlg) {
		this.selectedPrjNameAddPrjUserDlg = selectedPrjNameAddPrjUserDlg;
	}

	public String getSelectedProjectUserEntityRuleId() {
		return selectedProjectUserEntityRuleId;
	}

	public void setSelectedProjectUserEntityRuleId(
			String selectedProjectUserEntityRuleId) {
		this.selectedProjectUserEntityRuleId = selectedProjectUserEntityRuleId;
	}


	public String getSelectedProjectUserName() {
		return selectedProjectUserName;
	}

	public void setSelectedProjectUserName(String selectedProjectUserName) {
		this.selectedProjectUserName = selectedProjectUserName;
	}


}
