package com.pdinh.authorization.presentation.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.SessionBean;
import com.pdinh.enterprise.realm.PDINHSessionToken;


public class FacesAjaxAwareUserFilter extends UserFilter {
	private static final String FACES_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

	private static final Logger log = LoggerFactory.getLogger(FacesAjaxAwareUserFilter.class);
    @Override
    protected void redirectToLogin(ServletRequest req, ServletResponse res) throws IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        log.trace("In FacesAjaxAwareUserFilter.  redirecting to login");
        if ("partial/ajax".equals(request.getHeader("Faces-Request"))) {
        	log.trace("Detected faces request.  Doing faces redirect");
            res.setContentType("text/xml");
            res.setCharacterEncoding("UTF-8");
            res.getWriter().printf(FACES_REDIRECT_XML, request.getContextPath() + getLoginUrl());
        }
        else {
        	log.trace("Detected regular request.  Doing normal redirect");
            super.redirectToLogin(req, res);
        }
    }
    
    @Override
    protected boolean onAccessDenied(ServletRequest req, ServletResponse res){
    	
    	if (req.getParameter("sessionToken") != null && req.getParameter("subjectId") != null){
    		log.debug("Go Session Token login request: {}", req.getParameter("sessionToken"));
    		PDINHSessionToken token = new PDINHSessionToken(req.getParameter("sessionToken"), 
    				Integer.valueOf(req.getParameter("subjectId")));
    		Subject subject = SecurityUtils.getSubject();
    		try{
    			
    			subject.logout();//this prevents failure of PostConstruct method in SessionBean
    			subject.login(token);
    			saveRequest(req);
        		return true;
    		}catch(AuthenticationException ae){
    			log.error("Failed to log in with Session Token for user session: {}, subject: {}", req.getParameter("palmsSessionId"), req.getParameter("subjectId"));
    			ae.printStackTrace();
    			try {
    				return super.onAccessDenied(req, res);
    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    				return false;
    			}
    		}
    		
    	}
    	//request doesn't have palmsSessionId param.  handle as normal user filter
    	try {
			return super.onAccessDenied(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    }
    
    @Override
	protected boolean isAccessAllowed(ServletRequest req, ServletResponse res, Object mappedValue) {
		if (null == req.getParameter("subjectId")) {
			// TODO: This leaves a security hole for hackers to get into an
			// existing session by failing to pass a subjectId value.
			return super.isAccessAllowed(req, res, mappedValue);
		}

		Subject subject = SecurityUtils.getSubject();
		Integer subjectId = Integer.valueOf(req.getParameter("subjectId"));
		SessionBean sessionBean = (SessionBean) ((HttpServletRequest) req).getSession().getAttribute("sessionBean");

		if (!subject.isAuthenticated() || sessionBean == null) {
			return false;
		}

		return super.isAccessAllowed(req, res, mappedValue) && (subjectId.equals(sessionBean.getSubjectId()));
	}
}
