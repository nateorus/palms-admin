package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleTemplate;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;

@ManagedBean(name = "projectTeamsViewBean")
@ViewScoped
public class ProjectTeamsViewBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List<PalmsRealm> realms = new ArrayList<PalmsRealm>();
	List<Company> companies = new ArrayList<Company>();
	List<Project> projects = new ArrayList<Project>();
	private List<ExternalSubjectDataObj> realmUsers = new ArrayList<ExternalSubjectDataObj>();
	private List<ExternalSubjectDataObj> selectedUsers = new ArrayList<ExternalSubjectDataObj>();
	private ExternalSubjectDataObj selectedCurrentUser;
	private List<ExternalSubjectDataObj> currentTeamMembers = new ArrayList<ExternalSubjectDataObj>();
	private ExternalSubjectDataObj selectedUser;
	private String selectedRealmName;
	private String selectedCompanyId;
	private boolean selectCompanyDisabled = true;
	private boolean getProjectsDisabled = true;
	private boolean setTeamDisabled = true;
	private boolean searchSubjectsRoleDisabled = true;
	private boolean removeMemberDisabled = true;
	private boolean pickRolesDisabled = true;
	private Project selectedProject;
	private RoleTemplate roleTemplate;
	List<AuthorizedEntity> teamEntities = new ArrayList<AuthorizedEntity>();
	AuthorizedEntity selectedUserProjectEntity;
	
	private DualListModel<Role> contextRoleLists;

	public List<PalmsRealm> getRealms() {
		return realms;
	}

	public void setRealms(List<PalmsRealm> realms) {
		this.realms = realms;
	}

	public String getSelectedRealmName() {
		return selectedRealmName;
	}

	public void setSelectedRealmName(String selectedRealmName) {
		this.selectedRealmName = selectedRealmName;
	}


	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public boolean isSelectCompanyDisabled() {
		return selectCompanyDisabled;
	}

	public void setSelectCompanyDisabled(boolean selectCompanyDisabled) {
		this.selectCompanyDisabled = selectCompanyDisabled;
	}

	public boolean isGetProjectsDisabled() {
		return getProjectsDisabled;
	}

	public void setGetProjectsDisabled(boolean getProjectsDisabled) {
		this.getProjectsDisabled = getProjectsDisabled;
	}

	public String getSelectedCompanyId() {
		return selectedCompanyId;
	}

	public void setSelectedCompanyId(String selectedCompanyId) {
		this.selectedCompanyId = selectedCompanyId;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Project getSelectedProject() {
		return selectedProject;
	}

	public void setSelectedProject(Project selectedProject) {
		this.selectedProject = selectedProject;
	}

	public boolean isSetTeamDisabled() {
		return setTeamDisabled;
	}

	public void setSetTeamDisabled(boolean setTeamDisabled) {
		this.setTeamDisabled = setTeamDisabled;
	}

	public List<ExternalSubjectDataObj> getRealmUsers() {
		return realmUsers;
	}

	public void setRealmUsers(List<ExternalSubjectDataObj> realmUsers) {
		this.realmUsers = realmUsers;
	}

	public List<ExternalSubjectDataObj> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<ExternalSubjectDataObj> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public ExternalSubjectDataObj getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(ExternalSubjectDataObj selectedUser) {
		this.selectedUser = selectedUser;
	}

	public DualListModel<Role> getContextRoleLists() {
		return contextRoleLists;
	}

	public void setContextRoleLists(DualListModel<Role> contextRoleLists) {
		this.contextRoleLists = contextRoleLists;
	}

	public RoleTemplate getRoleTemplate() {
		return roleTemplate;
	}

	public void setRoleTemplate(RoleTemplate roleTemplate) {
		this.roleTemplate = roleTemplate;
	}

	public List<AuthorizedEntity> getTeamEntities() {
		return teamEntities;
	}

	public void setTeamEntities(List<AuthorizedEntity> teamEntities) {
		this.teamEntities = teamEntities;
	}

	public AuthorizedEntity getSelectedUserProjectEntity() {
		return selectedUserProjectEntity;
	}

	public void setSelectedUserProjectEntity(
			AuthorizedEntity selectedUserProjectEntity) {
		this.selectedUserProjectEntity = selectedUserProjectEntity;
	}

	public boolean isSearchSubjectsRoleDisabled() {
		return searchSubjectsRoleDisabled;
	}

	public void setSearchSubjectsRoleDisabled(boolean searchSubjectsRoleDisabled) {
		this.searchSubjectsRoleDisabled = searchSubjectsRoleDisabled;
	}

	public List<ExternalSubjectDataObj> getCurrentTeamMembers() {
		return currentTeamMembers;
	}

	public void setCurrentTeamMembers(List<ExternalSubjectDataObj> currentTeamMembers) {
		this.currentTeamMembers = currentTeamMembers;
	}


	public boolean isRemoveMemberDisabled() {
		return removeMemberDisabled;
	}

	public void setRemoveMemberDisabled(boolean removeMemberDisabled) {
		this.removeMemberDisabled = removeMemberDisabled;
	}

	public ExternalSubjectDataObj getSelectedCurrentUser() {
		return selectedCurrentUser;
	}

	public void setSelectedCurrentUser(ExternalSubjectDataObj selectedCurrentUser) {
		this.selectedCurrentUser = selectedCurrentUser;
	}

	public boolean isPickRolesDisabled() {
		return pickRolesDisabled;
	}

	public void setPickRolesDisabled(boolean pickRolesDisabled) {
		this.pickRolesDisabled = pickRolesDisabled;
	}
}
