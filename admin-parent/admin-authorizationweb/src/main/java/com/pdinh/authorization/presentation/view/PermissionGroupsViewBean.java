package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.pdinh.auth.persistence.entity.Permission;
import com.pdinh.auth.persistence.entity.PermissionGroup;

/**
 * Session Bean implementation class DashboardViewBean
 */
@ManagedBean(name = "permissionGroupsViewBean")
@ViewScoped
public class PermissionGroupsViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String uid;
	
	private List<Permission> permissions;
	private List<PermissionGroup> permissionGroups;
	private List<PermissionGroup> filteredPermissionGroups;
	private PermissionGroup newGroup = new PermissionGroup();
	private PermissionGroup selectedGroup = new PermissionGroup();
	private DualListModel<Permission> createPermissionsGroupLists;
	private List<String> userPermissions; 
	private boolean renderSaveGroupButton = true;
	private boolean renderEditGroupButton = false;
	private boolean renderGrpNameInputForNew = true;
	private boolean renderGrpNameInputForEdit = false;
	private boolean editButtonDisabled = true;
	private boolean deleteButtonDisabled = true;
	private boolean createButtonDisabled = true;
	
    /**
     * Default constructor. 
     */
    public PermissionGroupsViewBean() {
        // TODO Auto-generated constructor stub
    }
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	public List<PermissionGroup> getPermissionGroups() {
		return permissionGroups;
	}
	public void setPermissionGroups(List<PermissionGroup> permissionGroups) {
		this.permissionGroups = permissionGroups;
	}
	public DualListModel<Permission> getCreatePermissionsGroupLists() {
		return createPermissionsGroupLists;
	}
	public void setCreatePermissionsGroupLists(DualListModel<Permission> createPermissionsGroupLists) {
		this.createPermissionsGroupLists = createPermissionsGroupLists;
	}
	public PermissionGroup getNewGroup() {
		return newGroup;
	}
	public void setNewGroup(PermissionGroup newGroup) {
		this.newGroup = newGroup;
	}
	public PermissionGroup getSelectedGroup() {
		return selectedGroup;
	}
	public void setSelectedGroup(PermissionGroup selectedGroup) {
		this.selectedGroup = selectedGroup;
	}
	public List<String> getUserPermissions() {
		return userPermissions;
	}
	public void setUserPermissions(List<String> userPermissions) {
		this.userPermissions = userPermissions;
	}
	public boolean isRenderSaveGroupButton() {
		return renderSaveGroupButton;
	}
	public void setRenderSaveGroupButton(boolean renderSaveGroupButton) {
		this.renderSaveGroupButton = renderSaveGroupButton;
	}
	public boolean isRenderEditGroupButton() {
		return renderEditGroupButton;
	}
	public void setRenderEditGroupButton(boolean renderEditGroupButton) {
		this.renderEditGroupButton = renderEditGroupButton;
	}
	public boolean isRenderGrpNameInputForNew() {
		return renderGrpNameInputForNew;
	}
	public void setRenderGrpNameInputForNew(boolean renderGrpNameInputForNew) {
		this.renderGrpNameInputForNew = renderGrpNameInputForNew;
	}
	public boolean isRenderGrpNameInputForEdit() {
		return renderGrpNameInputForEdit;
	}
	public void setRenderGrpNameInputForEdit(boolean renderGrpNameInputForEdit) {
		this.renderGrpNameInputForEdit = renderGrpNameInputForEdit;
	}

	public boolean isEditButtonDisabled() {
		return editButtonDisabled;
	}
	public void setEditButtonDisabled(boolean editButtonDisabled) {
		this.editButtonDisabled = editButtonDisabled;
	}
	public boolean isDeleteButtonDisabled() {
		return deleteButtonDisabled;
	}
	public void setDeleteButtonDisabled(boolean deleteButtonDisabled) {
		this.deleteButtonDisabled = deleteButtonDisabled;
	}
	public boolean isCreateButtonDisabled() {
		return createButtonDisabled;
	}
	public void setCreateButtonDisabled(boolean createButtonDisabled) {
		this.createButtonDisabled = createButtonDisabled;
	}
	public List<PermissionGroup> getFilteredPermissionGroups() {
		return filteredPermissionGroups;
	}
	public void setFilteredPermissionGroups(List<PermissionGroup> filteredPermissionGroups) {
		this.filteredPermissionGroups = filteredPermissionGroups;
	}


}
