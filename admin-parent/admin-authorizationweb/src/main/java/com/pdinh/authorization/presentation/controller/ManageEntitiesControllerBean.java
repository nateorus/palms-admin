package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.dao.AuthorizedEntityDao;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.dao.RoleTemplateDao;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.data.obj.RolesBean;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.auth.persistence.entity.RoleTemplate;
import com.pdinh.authorization.presentation.util.RoleConverter;
import com.pdinh.authorization.presentation.view.ManageEntitiesViewBean;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;

@ManagedBean(name = "manageEntitiesControllerBean")
@ViewScoped
public class ManageEntitiesControllerBean implements Serializable{

	/**
	 * 
	 */
	private static final Logger log = LoggerFactory.getLogger(ManageEntitiesControllerBean.class);
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(name = "manageEntitiesViewBean", value = "#{manageEntitiesViewBean}")
	private ManageEntitiesViewBean manageEntitiesViewBean;
	
	@ManagedProperty(name = "roleConverter", value="#{roleConverter}")
	private RoleConverter roleConverter;
	
	@EJB
	private PalmsSubjectDao subjectDao;
	
	@EJB
	private PalmsRealmDao realmDao;
	
	@EJB
	private AuthorizedEntityDao entityDao;
	
	@EJB
	private EntityServiceLocal entityService;
	
	@EJB
	private RoleTemplateDao templateDao;
	
	@EJB
	private GlobalRolesBean rolesBean;
	
	@EJB
	private CompanyDao companyDao;
	
	@EJB
	private ProjectDao projectDao;
	
	@EJB
	private ProjectUserDao projectUserDao;
	
	public void initView(){
		if (manageEntitiesViewBean.getContext() == null){
			Subject s_subject = SecurityUtils.getSubject();
			manageEntitiesViewBean.setUsername(s_subject.getPrincipal().toString());
			log.debug("Subject Id: {}", manageEntitiesViewBean.getSubjectId());
			log.debug("Realm Id: {}", manageEntitiesViewBean.getRealmId());
			PalmsSubject subject = subjectDao.findById(manageEntitiesViewBean.getSubjectId());
			manageEntitiesViewBean.setSubject(subject);
			PalmsRealm realm = realmDao.findById(manageEntitiesViewBean.getRealmId());
			manageEntitiesViewBean.setRealm(realm);
			manageEntitiesViewBean.setSourceRoles(realm.getRoleTemplate().getAllowedRoles());
			RoleContext context = entityService.getDefaultContext(subject);
			manageEntitiesViewBean.setContext(context);
			manageEntitiesViewBean.setEntities(entityDao.findAllByContext(context.getRole_context_id()));
			manageEntitiesViewBean.setTypes(rolesBean.getEntityTypes());
			manageEntitiesViewBean.setRules(rolesBean.getEntityRules());
			manageEntitiesViewBean.setAllCompanies(companyDao.findAll());
			manageEntitiesViewBean.setNow(new Timestamp(new java.util.Date().getTime()));
		}
	}
	
	public void handleEntitySelect(){
		log.debug("In handleEntitySelect");
		manageEntitiesViewBean.setEditEntityButtonDisabled(false);
		manageEntitiesViewBean.setDeleteEntityButtonDisabled(false);
		AuthorizedEntity entity = manageEntitiesViewBean.getSelectedEntity();
		log.debug("Selected Entity {}", entity.getAuthorized_entity_id());
		manageEntitiesViewBean.setSelectedRuleId(String.valueOf(entity.getEntityRule().getEntity_rule_id()));
		manageEntitiesViewBean.setSelectedTimeBoundValueEdit(entity.isTimeBound());
		manageEntitiesViewBean.setStartTime(entity.getAccessStartDate());
		manageEntitiesViewBean.setEndTime(entity.getAccessEndDate());
		if (entity.isTimeBound()){
			
			manageEntitiesViewBean.setStartTimeDisabled(false);
			manageEntitiesViewBean.setEndTimeDisabled(false);
		}
	}
	
	public void handleEditEntity(){
		log.debug("In handle Edit Entity");
		RoleTemplate template = templateDao.findById(manageEntitiesViewBean.getRealm().getRoleTemplate().getRole_template_id());
		List<Role> sourceRoles = template.getAllowedRoles();
		
		List<Role> targetRoles = manageEntitiesViewBean.getSelectedEntity().getRoles();
		for (Role r: targetRoles){
			for (Role sr: sourceRoles){
				if (r.getRole_id() == sr.getRole_id()){
					sourceRoles.remove(sr);
					log.debug("Removed Role from source list: {}", sr.getName());
					
					break;
				}
			}
		}
		
		manageEntitiesViewBean.setEntityRoleLists(new DualListModel<Role>(sourceRoles,targetRoles));
		
	}
	
	public void handleDeleteEntity(){
		RoleContext context = entityService.revokeEntityAccess(manageEntitiesViewBean.getContext(),
				manageEntitiesViewBean.getSelectedEntity().getEntity_id(),
				manageEntitiesViewBean.getSelectedEntity().getEntityType().getEntity_type_code(),
				manageEntitiesViewBean.getUsername());
		manageEntitiesViewBean.setContext(context);
		manageEntitiesViewBean.setEntities(context.getEntities());
	}
	
	public void handleTimeSelectEdit(){
		log.debug("Selected Value: {}", manageEntitiesViewBean.isSelectedTimeBoundValueEdit());
		if (manageEntitiesViewBean.isSelectedTimeBoundValueEdit()){
			manageEntitiesViewBean.setStartTimeDisabled(false);
			manageEntitiesViewBean.setEndTimeDisabled(false);
		}else{
			manageEntitiesViewBean.setStartTimeDisabled(true);
			manageEntitiesViewBean.setEndTimeDisabled(true);
		}
	}
	
	public void handleTimeSelectCo(){
		log.debug("Selected Value: {}", manageEntitiesViewBean.isSelectedTimeBoundValueCo());
		if (manageEntitiesViewBean.isSelectedTimeBoundValueCo()){
			manageEntitiesViewBean.setStartTimeDisabled(false);
			manageEntitiesViewBean.setEndTimeDisabled(false);
		}else{
			manageEntitiesViewBean.setStartTimeDisabled(true);
			manageEntitiesViewBean.setEndTimeDisabled(true);
		}
	}
	
	public void handleTimeSelectPrj(){
		log.debug("Selected Value: {}", manageEntitiesViewBean.isSelectedTimeBoundValuePrj());
		if (manageEntitiesViewBean.isSelectedTimeBoundValuePrj()){
			manageEntitiesViewBean.setStartTimeDisabled(false);
			manageEntitiesViewBean.setEndTimeDisabled(false);
		}else{
			manageEntitiesViewBean.setStartTimeDisabled(true);
			manageEntitiesViewBean.setEndTimeDisabled(true);
		}
	}
	
	public void handleTimeSelectPrjUser(){
		log.debug("Selected Value: {}", manageEntitiesViewBean.isSelectedTimeBoundValuePrjUser());
		if (manageEntitiesViewBean.isSelectedTimeBoundValuePrjUser()){
			manageEntitiesViewBean.setStartTimeDisabled(false);
			manageEntitiesViewBean.setEndTimeDisabled(false);
		}else{
			manageEntitiesViewBean.setStartTimeDisabled(true);
			manageEntitiesViewBean.setEndTimeDisabled(true);
		}
	}
	
	public String handleFlowEntityWizard(FlowEvent event){
		log.debug("Handling flow event.  Old Step {}, new step {}", event.getOldStep(), event.getNewStep());
		if (event.getOldStep().equalsIgnoreCase("entityDetailTab")){
			if (manageEntitiesViewBean.isSelectedTimeBoundValueEdit()){
				if (manageEntitiesViewBean.getStartTime() == null || manageEntitiesViewBean.getEndTime() == null){
					FacesContext facesContext = FacesContext.getCurrentInstance();
			    	facesContext.addMessage(null, new FacesMessage("If Entity access is Time bound, you must provide start and end times"));
			    	return event.getOldStep();
				}
			}
		}
		if (event.getNewStep().equalsIgnoreCase("entityRolesTab")){
			FacesContext facesContext = FacesContext.getCurrentInstance();
	    	facesContext.addMessage(null, new FacesMessage("Roles Are Optional.  Select no roles if you want the user to have their default roles for this entity." +
	    			"Roles Selected here will override the user's default roles"));
		}
		
		return event.getNewStep();
	}
	
	public void handleRoleTransfer(TransferEvent event){
		
	}
	
	public void handleSaveEntity(){
		AuthorizedEntity entity = manageEntitiesViewBean.getSelectedEntity();
		List<EntityRule> rules = manageEntitiesViewBean.getRules();
		for (EntityRule r: rules){
			if (r.getEntity_rule_id() == Integer.valueOf(manageEntitiesViewBean.getSelectedRuleId())){
				entity.setEntityRule(r);
				break;
			}
		}
		log.debug("Setting Time Bound Value {}", manageEntitiesViewBean.isSelectedTimeBoundValueEdit());
		entity.setTimeBound(manageEntitiesViewBean.isSelectedTimeBoundValueEdit());
		if (entity.isTimeBound()){
			log.debug("Entity is Time Bound");
			entity.setAccessStartDate(new Timestamp(manageEntitiesViewBean.getStartTime().getTime()));
			entity.setAccessEndDate(new Timestamp(manageEntitiesViewBean.getEndTime().getTime()));
		}
		entity.setRoles(manageEntitiesViewBean.getEntityRoleLists().getTarget());
		RoleContext context = entityService.grantEntityAccess(manageEntitiesViewBean.getContext().getRole_context_id(), entity, manageEntitiesViewBean.getUsername());
		manageEntitiesViewBean.setContext(context);
		manageEntitiesViewBean.setEntities(context.getEntities());
		//entityDao.evictClass(AuthorizedEntity.class);
	}
	public void handleCreateCoEntity(){
		//Subject subject = SecurityUtils.getSubject();
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntityType(rolesBean.getEntityType(EntityType.COMPANY_TYPE));
		List<EntityRule> rules = manageEntitiesViewBean.getRules();
		log.debug("Selectd Rule Id {}", manageEntitiesViewBean.getSelectedCompanyEntityRuleId());
		for (EntityRule r: rules){
			if (r.getEntity_rule_id() == Integer.valueOf(manageEntitiesViewBean.getSelectedCompanyEntityRuleId())){
				entity.setEntityRule(r);
				break;
			}
		}
		entity.setTimeBound(manageEntitiesViewBean.isSelectedTimeBoundValueCo());
		if (entity.isTimeBound()){
			
			entity.setAccessStartDate(new Timestamp(manageEntitiesViewBean.getStartTime().getTime()));
			entity.setAccessEndDate(new Timestamp(manageEntitiesViewBean.getEndTime().getTime()));
		}
		entity.setRoleContext(manageEntitiesViewBean.getContext());
		entity.setSubject_id(manageEntitiesViewBean.getSubjectId());
		entity.setEntity_id(Integer.valueOf(manageEntitiesViewBean.getSelectedCompanyId()));
		entity.setDescription(manageEntitiesViewBean.getSelectedCoNameAddCoDlg());
		RoleContext newContext = entityService.grantEntityAccess(manageEntitiesViewBean.getContext().getRole_context_id(), entity,manageEntitiesViewBean.getUsername());
		manageEntitiesViewBean.setEntities(newContext.getEntities());
		manageEntitiesViewBean.setContext(newContext);
		
	}
	public void handleCreatePrjEntity(){
		Subject subject = SecurityUtils.getSubject();
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntityType(rolesBean.getEntityType(EntityType.PROJECT_TYPE));
		List<EntityRule> rules = manageEntitiesViewBean.getRules();
		for (EntityRule r: rules){
			log.debug("SelectedProjectEntityRuleId: {}", manageEntitiesViewBean.getSelectedProjectEntityRuleId());
			if (r.getEntity_rule_id() == Integer.valueOf(manageEntitiesViewBean.getSelectedProjectEntityRuleId())){
				entity.setEntityRule(r);
				break;
			}
		}
		entity.setTimeBound(manageEntitiesViewBean.isSelectedTimeBoundValuePrj());
		if (entity.isTimeBound()){
			
			entity.setAccessStartDate(new Timestamp(manageEntitiesViewBean.getStartTime().getTime()));
			entity.setAccessEndDate(new Timestamp(manageEntitiesViewBean.getEndTime().getTime()));
		}
		entity.setRoleContext(manageEntitiesViewBean.getContext());
		entity.setSubject_id(manageEntitiesViewBean.getSubjectId());
		entity.setEntity_id(Integer.valueOf(manageEntitiesViewBean.getSelectedPrjId()));
		AuthorizedEntity parent = entityDao.findByContextTypeAndId(manageEntitiesViewBean.getContext().getRole_context_id(), 
				rolesBean.getEntityType(EntityType.COMPANY_TYPE).getEntity_type_id(), 
				Integer.valueOf(manageEntitiesViewBean.getSelectedCompanyIdPrjDlg()));
		entity.setParentEntity(parent);
		entity.setDescription(manageEntitiesViewBean.getSelectedPrjNameAddPrjDlg());
		//entity = entityDao.create(entity);
		RoleContext newContext = entityService.grantEntityAccess(manageEntitiesViewBean.getContext().getRole_context_id(), entity, subject.getPrincipal().toString());
		manageEntitiesViewBean.setEntities(newContext.getEntities());
		manageEntitiesViewBean.setContext(newContext);
		
		
	}
	
	public void handleCreatePrjUserEntity(){
		Subject subject = SecurityUtils.getSubject();
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntityType(rolesBean.getEntityType(EntityType.PROJECT_USER_TYPE));
		List<EntityRule> rules = manageEntitiesViewBean.getRules();
		for (EntityRule r: rules){
			if (r.getEntity_rule_id() == Integer.valueOf(manageEntitiesViewBean.getSelectedProjectUserEntityRuleId())){
				entity.setEntityRule(r);
				break;
			}
		}
		entity.setTimeBound(manageEntitiesViewBean.isSelectedTimeBoundValuePrj());
		if (entity.isTimeBound()){
			
			entity.setAccessStartDate(new Timestamp(manageEntitiesViewBean.getStartTime().getTime()));
			entity.setAccessEndDate(new Timestamp(manageEntitiesViewBean.getEndTime().getTime()));
		}
		entity.setRoleContext(manageEntitiesViewBean.getContext());
		entity.setSubject_id(manageEntitiesViewBean.getSubjectId());
		entity.setEntity_id(Integer.valueOf(manageEntitiesViewBean.getSelectedPrjUserId()));
		AuthorizedEntity parent = entityDao.findByContextTypeAndId(manageEntitiesViewBean.getContext().getRole_context_id(), 
				rolesBean.getEntityType(EntityType.PROJECT_TYPE).getEntity_type_id(), 
				Integer.valueOf(manageEntitiesViewBean.getSelectedPrjIdPrjUserDlg()));
		entity.setParentEntity(parent);
		entity.setDescription(manageEntitiesViewBean.getSelectedProjectUserName());
		//entity = entityDao.create(entity);
		RoleContext newContext = entityService.grantEntityAccess(manageEntitiesViewBean.getContext().getRole_context_id(), entity, subject.getPrincipal().toString());
		manageEntitiesViewBean.setEntities(newContext.getEntities());
		manageEntitiesViewBean.setContext(newContext);
	}
	public void handleCompanyInit(){
		log.debug("In addCompanyInit()");
		List<Company> notPermitted = new ArrayList<Company>();
		List<Company> allCompanies = manageEntitiesViewBean.getAllCompanies();
		List<Integer> authorizedCos = entityService.getAuthorizedEntities(manageEntitiesViewBean.getContext(), EntityType.COMPANY_TYPE, false);
		List<Company> permittedCos = companyDao.findAllCompaniesFiltered(authorizedCos);
		
		for (Company c: allCompanies){
			boolean permitted = false;
			for (Company co: permittedCos){
				if (co.getCompanyId() == c.getCompanyId()){
					permitted = true;
				}
			}
			if (!(permitted)){
				notPermitted.add(c);
				log.debug("Added not permitted company: {}", c.getCoName());
			}
		}
		manageEntitiesViewBean.setNonPermittedCompanies(notPermitted);
		manageEntitiesViewBean.setEndTimeDisabled(true);
		manageEntitiesViewBean.setStartTimeDisabled(true);
		manageEntitiesViewBean.setSelectedRuleId("");
		manageEntitiesViewBean.setSelectedTimeBoundValueCo(false);
		
	}
	
	public void handleCoSelect(){
		log.debug("In handleCoSelect()");
		Company co = companyDao.findById(Integer.valueOf(manageEntitiesViewBean.getSelectedCompanyId()));
		manageEntitiesViewBean.setSelectedCoNameAddCoDlg(co.getCoName());
	}
	public void handlePrjSelect(){
		log.debug("In handlePrjSelect()");
		Project p = projectDao.findById(Integer.valueOf(manageEntitiesViewBean.getSelectedPrjId()));
		manageEntitiesViewBean.setSelectedPrjNameAddPrjDlg(p.getName());
	}
	public void handleCoSelectforPrj(){
		log.debug("In handleCoSelectforPrj()");
		int coid = Integer.valueOf(manageEntitiesViewBean.getSelectedCompanyIdPrjDlg());
		List<Project> projects = projectDao.findAllByCompanyId(coid);
		manageEntitiesViewBean.setAvailableProjects(projects);
	}
	public void handleCoSelectforPrjUser(){
		log.debug("In handleCoSelectforPrjUser()");
		int coid = Integer.valueOf(manageEntitiesViewBean.getSelectedCompanyIdPrjUserDlg());
		List<Project> projects = projectDao.findAllByCompanyId(coid);
		manageEntitiesViewBean.setAvailableProjects(projects);
	}
	
	public void handlePrjSelectforPrjUser(){
		log.debug("In handlePrjSelectforPrjUser()");
		Project p = projectDao.findById(Integer.valueOf(manageEntitiesViewBean.getSelectedPrjIdPrjUserDlg()));
		manageEntitiesViewBean.setSelectedPrjNameAddPrjUserDlg(p.getName());
		manageEntitiesViewBean.setAvailableProjectUsers(p.getProjectUsers());
	}
	
	public void handlePrjUserSelect(){
		log.debug("In handlePrjUserSelect... not sure if i need to do anything here...");
		ProjectUser pu = projectUserDao.findById(Integer.valueOf(manageEntitiesViewBean.getSelectedPrjUserId()));
		manageEntitiesViewBean.setSelectedProjectUserName(pu.getUser().getLastname() + ", " + pu.getUser().getFirstname() + "(" + pu.getProject().getName() + ")");
		
	}
	
	public void addProjectInit(){
		List<Integer> authorizedCos = entityService.getAuthorizedEntities(manageEntitiesViewBean.getContext(), EntityType.COMPANY_TYPE, false);
		manageEntitiesViewBean.setPermittedCompanies(companyDao.findAllCompaniesFiltered(authorizedCos));
		manageEntitiesViewBean.setEndTimeDisabled(true);
		manageEntitiesViewBean.setStartTimeDisabled(true);
		manageEntitiesViewBean.setSelectedRuleId("");
		manageEntitiesViewBean.setSelectedTimeBoundValuePrj(false);
		AuthorizedEntity selected = manageEntitiesViewBean.getSelectedEntity();
		if (selected.getEntityType().getEntity_type_code().equalsIgnoreCase(EntityType.COMPANY_TYPE)){
			manageEntitiesViewBean.setSelectedCompanyIdPrjDlg(String.valueOf(selected.getEntity_id()));
			List<Project> projects = projectDao.findAllByCompanyId(selected.getEntity_id());
			manageEntitiesViewBean.setAvailableProjects(projects);
		}
	}
	
	public void addProjectUserInit(){
		List<Integer> authorizedCos = entityService.getAuthorizedEntities(manageEntitiesViewBean.getContext(), EntityType.COMPANY_TYPE, false);
		manageEntitiesViewBean.setPermittedCompanies(companyDao.findAllCompaniesFiltered(authorizedCos));
		
		manageEntitiesViewBean.setEndTimeDisabled(true);
		manageEntitiesViewBean.setStartTimeDisabled(true);
		manageEntitiesViewBean.setSelectedRuleId("");
		manageEntitiesViewBean.setSelectedTimeBoundValuePrjUser(false);
		AuthorizedEntity selected = manageEntitiesViewBean.getSelectedEntity();
		if (selected.getEntityType().getEntity_type_code().equalsIgnoreCase(EntityType.COMPANY_TYPE)){
			manageEntitiesViewBean.setSelectedCompanyIdPrjUserDlg(String.valueOf(selected.getEntity_id()));
			List<Project> projects = projectDao.findAllByCompanyId(selected.getEntity_id());
			manageEntitiesViewBean.setAvailableProjects(projects);
		}
	}
	
	public void handleRuleSelect(){
		log.debug("In handleRuleSelect");
		log.debug("New Value: {}", manageEntitiesViewBean.getSelectedRuleId());
		
	}

	public ManageEntitiesViewBean getManageEntitiesViewBean() {
		return manageEntitiesViewBean;
	}

	public void setManageEntitiesViewBean(ManageEntitiesViewBean manageEntitiesViewBean) {
		this.manageEntitiesViewBean = manageEntitiesViewBean;
	}

	public RoleConverter getRoleConverter() {
		return roleConverter;
	}

	public void setRoleConverter(RoleConverter roleConverter) {
		this.roleConverter = roleConverter;
	}

}
