package com.pdinh.authorization.presentation.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestrictPageFilter implements Filter {
	FilterConfig fc;
	private static final Logger log = LoggerFactory.getLogger(RestrictPageFilter.class);
	private SecurityManager securityManager;
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String pageRequested = req.getRequestURL().toString();
		Subject subject = SecurityUtils.getSubject();
		
		
		if (!subject.isAuthenticated() && !pageRequested.contains("/faces/login.xhtml") 
				&& !pageRequested.contains("/faces/login_failed.xhtml")
				&& !pageRequested.contains("/faces/login_timeout.xhtml")) {
			
			resp.sendRedirect("login_failed.xhtml");
		} else {
			chain.doFilter(request,response);
		}
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		fc = filterConfig;
	}

	public SecurityManager getSecurityManager() {
		return securityManager;
	}

	public void setSecurityManager(SecurityManager securityManager) {
		this.securityManager = securityManager;
	}
}
