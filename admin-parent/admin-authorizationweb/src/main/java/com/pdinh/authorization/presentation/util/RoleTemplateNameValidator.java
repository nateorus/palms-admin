package com.pdinh.authorization.presentation.util;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.RoleTemplateDao;

@ManagedBean(name = "roleTemplateNameValidator")
@ViewScoped
@FacesValidator(value = "roleTemplateNameValidator")
public class RoleTemplateNameValidator implements Validator, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RoleTemplateNameValidator.class);

	@EJB
	private RoleTemplateDao roleTemplateDao;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		// TODO Auto-generated method stub
		String roleTemplateName = String.valueOf(arg2);
		String errMsg = "";
		log.debug("Validating Role Template Name: {}", roleTemplateName);

		//Role role = roleDao.findRoleByName(roleName);
		boolean valid = true;
		if (arg2 == null){
			valid = false;
			errMsg = "Invalid Role Template Name.  Name cannot be null.";
		} else if (roleTemplateName.length() > 50){
			valid = false;
			errMsg = "Invalid Role Template Name.  Role Template name > 100 chars.";
		} else if (roleTemplateName.contains(" ")){
			valid = false;
			errMsg = "Invalid Role Template Name.  Role Template names cannot contain spaces.";
		} else if (roleTemplateName.equalsIgnoreCase("")){
			valid = false;
			errMsg = "Invalid Role Template Name.  Role Template name cannot be empty.";
		}
		if (arg1.getClientId().endsWith("roleTemplateNameInputNew")){
			if (!(roleTemplateDao.findRoleTemplateByName(roleTemplateName) == null)){
				valid = false;
				errMsg = "Invalid Role Template Name.  A Role Template with that name already exists.";
			}
		}
		if (!valid){
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, errMsg, 
					"The Role Template name provided is null, already in use, contains invalid chars or is too long");
			throw new ValidatorException(message);
			
		}
				
		
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RoleTemplateDao getRoleTemplateDao() {
		return roleTemplateDao;
	}

	public void setRoleTemplateDao(RoleTemplateDao roleTemplateDao) {
		this.roleTemplateDao = roleTemplateDao;
	}

}
