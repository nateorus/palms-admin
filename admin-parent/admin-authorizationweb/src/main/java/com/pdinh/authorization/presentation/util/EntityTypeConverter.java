package com.pdinh.authorization.presentation.util;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.EntityTypeDao;
import com.pdinh.auth.persistence.entity.EntityType;

@ManagedBean(name = "entityTypeConverter")
@ViewScoped
@FacesConverter(value = "entityTypeConverter")
public class EntityTypeConverter implements Converter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EntityTypeConverter.class);
	
	@EJB
	private EntityTypeDao entityTypeDao;
	
	public EntityTypeConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if ((arg2 != null) && (!(arg2.equals(String.valueOf(0)))) && (!(arg2.equals(""))) ){
			int pid = Integer.valueOf(arg2);
			log.trace("In getAsObject method of permissionGroupConverter.  Argument: {}", pid);
			
			EntityType type = entityTypeDao.findById(pid);
			log.trace("Returned EntityType Object: {}", type.getEntity_type_name());
			return type;

		}else{
			log.debug("Returning Null from EntityTypeConverter getAsObjectMethod");
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if ((!(arg2.equals(null))) && arg2 instanceof EntityType){
			String pid = "" + ((EntityType) arg2).getEntity_type_id();
			log.trace("In getAsString method of EntityTypeConverter.  Argument: {}", pid);
			return pid;
		} else {
			log.debug("Returning Null from EntityTypeConverter getAsString class");
			return null;
		}
	}

}
