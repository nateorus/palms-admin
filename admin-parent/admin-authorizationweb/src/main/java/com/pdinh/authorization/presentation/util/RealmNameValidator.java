package com.pdinh.authorization.presentation.util;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.PalmsRealmDao;

@ManagedBean(name = "realmNameValidator")
@ViewScoped
@FacesValidator(value = "realmNameValidator")
public class RealmNameValidator implements Validator, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RealmNameValidator.class);

	@EJB
	private PalmsRealmDao realmDao;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		// TODO Auto-generated method stub
		String realmName = String.valueOf(arg2);
		String errMsg = "";
		log.debug("Validating Realm Name: {}", realmName);
		log.debug("Validating for UI Component: {}", arg1.getClientId());

		//Role role = roleDao.findRoleByName(roleName);
		boolean valid = true;
		if (arg2 == null){
			valid = false;
			errMsg = "Invalid Realm Name.  Name cannot be null.";
		} else if (realmName.length() > 50){
			valid = false;
			errMsg = "Invalid Realm Name.  Realm name > 100 chars.";
		} else if (realmName.contains(" ")){
			valid = false;
			errMsg = "Invalid Realm Name.  Realm names cannot contain spaces.";
		} else if (realmName.equalsIgnoreCase("")){
			valid = false;
			errMsg = "Invalid Realm Name.  Realm name cannot be empty.";
		}
		if (arg1.getClientId().equalsIgnoreCase("realmNameInputForNew")){
			if (!(realmDao.findRealmByName(realmName) == null)){
				valid = false;
				errMsg = "Invalid Realm Name.  A Realm with that name already exists.";
			}
		}
		if (!valid){
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, errMsg, 
					"The Realm name provided is null, already in use, contains invalid chars or is too long");
			throw new ValidatorException(message);
			
		}
				
		
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PalmsRealmDao getRealmDao() {
		return realmDao;
	}

	public void setRealmDao(PalmsRealmDao realmDao) {
		this.realmDao = realmDao;
	}

}
