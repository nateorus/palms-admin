package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.List;


import javax.faces.bean.ManagedBean;

import javax.faces.bean.ViewScoped;

import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.RealmType;
import com.pdinh.auth.persistence.entity.RoleTemplate;

/**
 * Session Bean implementation class DashboardViewBean
 */
@ManagedBean(name = "manageRealmsViewBean")
@ViewScoped
public class ManageRealmsViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String uid;

	private List<PalmsRealm> realms;
	private List<RoleTemplate> roleTemplates;
	private List<RealmType> realmTypes;
	private RealmType selectedType = new RealmType();
	private String sSelectedType = "";
	
	private RoleTemplate selectedTemplate;
	private String sSelectedTemplate = "";

	private PalmsRealm newRealm = new PalmsRealm();
	private PalmsRealm selectedRealm = new PalmsRealm();

	private boolean editDisabled = true;
	private boolean deleteDisabled = true;
	
	private boolean submitNewButtonRendered = true;
	private boolean submitEditButtonRendered = false;
	
	private boolean renderInputForNew = true;
	private boolean renderInputForEdit = false;
	
	private boolean importSubjectsDisabled = true;
	private boolean createButtonDisabled = true;
	
	private String realmDlgHeader = "Create New Realm";
    /**
     * Default constructor. 
     */
    public ManageRealmsViewBean() {
        // TODO Auto-generated constructor stub
    }
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public boolean isEditDisabled() {
		return editDisabled;
	}
	public void setEditDisabled(boolean editDisabled) {
		this.editDisabled = editDisabled;
	}
	public boolean isDeleteDisabled() {
		return deleteDisabled;
	}
	public void setDeleteDisabled(boolean deleteDisabled) {
		this.deleteDisabled = deleteDisabled;
	}
	public List<RoleTemplate> getRoleTemplates() {
		return roleTemplates;
	}
	public void setRoleTemplates(List<RoleTemplate> roleTemplates) {
		this.roleTemplates = roleTemplates;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<PalmsRealm> getRealms() {
		return realms;
	}
	public void setRealms(List<PalmsRealm> realms) {
		this.realms = realms;
	}
	public List<RealmType> getRealmTypes() {
		return realmTypes;
	}
	public void setRealmTypes(List<RealmType> realmTypes) {
		this.realmTypes = realmTypes;
	}
	public RealmType getSelectedType() {
		return selectedType;
	}
	public void setSelectedType(RealmType selectedType) {
		this.selectedType = selectedType;
	}
	public String getsSelectedType() {
		return sSelectedType;
	}
	public void setsSelectedType(String sSelectedType) {
		this.sSelectedType = sSelectedType;
	}
	public PalmsRealm getNewRealm() {
		return newRealm;
	}
	public void setNewRealm(PalmsRealm newRealm) {
		this.newRealm = newRealm;
	}
	public PalmsRealm getSelectedRealm() {
		return selectedRealm;
	}
	public void setSelectedRealm(PalmsRealm selectedRealm) {
		this.selectedRealm = selectedRealm;
	}
	public RoleTemplate getSelectedTemplate() {
		return selectedTemplate;
	}
	public void setSelectedTemplate(RoleTemplate selectedTemplate) {
		this.selectedTemplate = selectedTemplate;
	}
	public String getsSelectedTemplate() {
		return sSelectedTemplate;
	}
	public void setsSelectedTemplate(String sSelectedTemplate) {
		this.sSelectedTemplate = sSelectedTemplate;
	}
	public boolean isSubmitNewButtonRendered() {
		return submitNewButtonRendered;
	}
	public void setSubmitNewButtonRendered(boolean submitNewButtonRendered) {
		this.submitNewButtonRendered = submitNewButtonRendered;
	}
	public boolean isSubmitEditButtonRendered() {
		return submitEditButtonRendered;
	}
	public void setSubmitEditButtonRendered(boolean submitEditButtonRendered) {
		this.submitEditButtonRendered = submitEditButtonRendered;
	}
	public boolean isRenderInputForNew() {
		return renderInputForNew;
	}
	public void setRenderInputForNew(boolean renderInputForNew) {
		this.renderInputForNew = renderInputForNew;
	}
	public boolean isRenderInputForEdit() {
		return renderInputForEdit;
	}
	public void setRenderInputForEdit(boolean renderInputForEdit) {
		this.renderInputForEdit = renderInputForEdit;
	}
	public boolean isImportSubjectsDisabled() {
		return importSubjectsDisabled;
	}
	public void setImportSubjectsDisabled(boolean importSubjectsDisabled) {
		this.importSubjectsDisabled = importSubjectsDisabled;
	}
	public String getRealmDlgHeader() {
		return realmDlgHeader;
	}
	public void setRealmDlgHeader(String realmDlgHeader) {
		this.realmDlgHeader = realmDlgHeader;
	}
	public boolean isCreateButtonDisabled() {
		return createButtonDisabled;
	}
	public void setCreateButtonDisabled(boolean createButtonDisabled) {
		this.createButtonDisabled = createButtonDisabled;
	}


}
