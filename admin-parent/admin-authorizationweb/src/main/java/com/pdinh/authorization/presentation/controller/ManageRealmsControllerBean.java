package com.pdinh.authorization.presentation.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.enterprise.managedbean.SessionBean;

import com.pdinh.authorization.presentation.view.ManageRealmsViewBean;
import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.dao.AuthorizedEntityDao;
import com.pdinh.auth.data.dao.EntityRuleDao;
import com.pdinh.auth.data.dao.EntityTypeDao;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.dao.RealmTypeDao;
import com.pdinh.auth.data.dao.RoleTemplateDao;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.RealmType;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.auth.persistence.entity.RoleTemplate;

/**
 * Session Bean implementation class ManageRolesControllerBean
 */
@ManagedBean(name = "manageRealmsControllerBean")
@ViewScoped
public class ManageRealmsControllerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(ManageRealmsControllerBean.class);
	
	//private Subject subject;
	
	@ManagedProperty(name = "sessionBean", value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(name = "manageRealmsViewBean", value = "#{manageRealmsViewBean}")
	private ManageRealmsViewBean manageRealmsViewBean;
	
	@EJB
	private RoleTemplateDao roleTemplateDao;
	
	@EJB
	private RealmTypeDao realmTypeDao;
	
	@EJB
	private PalmsRealmDao realmDao;
	
	@EJB
	private EntityTypeDao entityTypeDao;
	
	@EJB
	private EntityRuleDao entityRuleDao;
	
	@EJB
	private EntityServiceLocal entityService;
	
	@EJB
	private AuthorizedEntityDao entityDao;
	
	@EJB 
	private PalmsSubjectDao subjectDao;
	
    public ManageRealmsControllerBean() {
        // TODO Auto-generated constructor stub
    }
    public void initView(){
    	//Use this for checking permissions
    	Subject subject = SecurityUtils.getSubject();
    	if (sessionBean.isAllowAllRealms()){
    		manageRealmsViewBean.setRealms(realmDao.findAll());
    	}else{
    		manageRealmsViewBean.setRealms(realmDao.findAllAuthorizedRealms(sessionBean.getAuthorizedRealmIds()));
    	}
    	manageRealmsViewBean.setRoleTemplates(roleTemplateDao.findAll(true));
    	manageRealmsViewBean.setRealmTypes(realmTypeDao.findAll(true));
    	if (subject.isPermitted("createRealms")){
    		manageRealmsViewBean.setCreateButtonDisabled(false);
    	}
    	    	
    }

    public void handleRealmSelect(SelectEvent event){
    	log.debug("Handle Selecting a Realm here");
    	Subject subject = SecurityUtils.getSubject();
    	if (subject.isPermitted("deleteRealms")){
    		manageRealmsViewBean.setDeleteDisabled(false);
    	}
    	if (subject.isPermitted("editRealms")){
    		manageRealmsViewBean.setEditDisabled(false);
    	}
    	
    	//manageRealmsViewBean.setImportSubjectsDisabled(false);
    	
    	//PermissionGroup pgroup = dashboardViewBean.getSelectedGroup();
    	//log.debug("selected group: {}", pgroup.getName());
    }


    public void handleCreateRealm(){
    	log.debug("Handle Creating a new Realm here");
    	PalmsRealm aNewRealm = new PalmsRealm();

    	manageRealmsViewBean.setNewRealm(aNewRealm);
    }
    
    public void handleNewRealmSetup(){
    	log.debug("Handle Setting new Realm with user input here");
    	PalmsRealm realm = manageRealmsViewBean.getNewRealm();
    	
    	List <RealmType> realmTypeList = manageRealmsViewBean.getRealmTypes();
    	Iterator iter = realmTypeList.iterator();
    	while (iter.hasNext()){
    		RealmType thisRealm = (RealmType)iter.next();
    		if (thisRealm.getRealm_type_name().equalsIgnoreCase(manageRealmsViewBean.getsSelectedType())){
    			log.debug("Found matching role name: {}", thisRealm.getRealm_type_name());
    			realm.setRealmType(thisRealm);
    			break;
    		}
    	}
    	
    	realm.setRoleTemplate(roleTemplateDao.findRoleTemplateByName(manageRealmsViewBean.getsSelectedTemplate()));

    }
    public void handleSubmitNewRealm(){
    	log.debug("Handle Persistence of a new Realm here");
    	
    	PalmsRealm realm = manageRealmsViewBean.getNewRealm();
    	String templateName = manageRealmsViewBean.getsSelectedTemplate();
    	String typeName = manageRealmsViewBean.getsSelectedType();
    	Iterator roleTemplateIter = manageRealmsViewBean.getRoleTemplates().iterator();
    	while (roleTemplateIter.hasNext()){
    		RoleTemplate thisTemplate = (RoleTemplate)roleTemplateIter.next();
    		if (thisTemplate.getRole_template_name().equalsIgnoreCase(templateName)){
    			realm.setRoleTemplate(thisTemplate);
    			break;
    		}
    	}
    	Iterator realmTypeIter = manageRealmsViewBean.getRealmTypes().iterator();
    	while (realmTypeIter.hasNext()){
    		RealmType thisType = (RealmType)realmTypeIter.next();
    		if (thisType.getRealm_type_name().equalsIgnoreCase(typeName)){
    			realm.setRealmType(thisType);
    			break;
    		}
    	}
    	
    	log.debug("Name: {}", realm.getRealm_name());
    	log.debug("Label: {}", realm.getRealm_label());
    	log.debug("Description: {}", realm.getRealm_description());
    	log.debug("ID: {}", realm.getRealm_id());
    	log.debug("RoleTemplate Id: {}", realm.getRoleTemplate().getRole_template_id());
    	
    	realmDao.create(realm);
    	manageRealmsViewBean.setRealms(realmDao.findAll(true));
    	handleClearSelectedRealms();
    	grantRealmAccessToCreator(realm);
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Created New Realm", realm.getRealm_label()));
    	
    	
    }
    public void grantRealmAccessToCreator(PalmsRealm realm){
    	Subject subject = SecurityUtils.getSubject();
    	if (entityService.isTypeAuthorized(sessionBean.getRoleContext().getRole_context_id(), EntityType.REALM_TYPE)){
    		log.debug("User is already granted access to all Realms");
    	}else{
    		RoleContext context = sessionBean.getRoleContext();
	    	AuthorizedEntity realmEntity = new AuthorizedEntity();
	    	realmEntity.setEntity_id(realm.getRealm_id());
	    	realmEntity.setEntityType(entityTypeDao.findEntityTypeByCode(EntityType.REALM_TYPE));
	    	realmEntity.setRoleContext(context);
	    	realmEntity.setEntityRule(entityRuleDao.findEntityRuleByName(EntityRule.ALLOW_CHILDREN));
	    	realmEntity.setSubject_id(sessionBean.getSubjectId());
	    	entityService.grantEntityAccess(context.getRole_context_id(), realmEntity, subject.getPrincipal().toString());
	    	List<Integer> entityRuleIds = new ArrayList<Integer>();
	    	entityRuleIds.add(3);//allowChildren
	    	entityRuleIds.add(1);//allow
	    	
	    	sessionBean.setAuthorizedRealmIds(entityService.getEntityIds(context.getRole_context_id(), EntityType.REALM_TYPE, entityRuleIds, true));
    	}
    }
    public void handleSubmitEditRealm(){
    	log.debug("Handle Persistence of a new Realm here");
    	
    	PalmsRealm realm = manageRealmsViewBean.getNewRealm();
    	String templateName = manageRealmsViewBean.getsSelectedTemplate();
    	String typeName = manageRealmsViewBean.getsSelectedType();
    	Iterator roleTemplateIter = manageRealmsViewBean.getRoleTemplates().iterator();
    	while (roleTemplateIter.hasNext()){
    		RoleTemplate thisTemplate = (RoleTemplate)roleTemplateIter.next();
    		if (thisTemplate.getRole_template_name().equalsIgnoreCase(templateName)){
    			realm.setRoleTemplate(thisTemplate);
    			break;
    		}
    	}
    	Iterator realmTypeIter = manageRealmsViewBean.getRealmTypes().iterator();
    	while (realmTypeIter.hasNext()){
    		RealmType thisType = (RealmType)realmTypeIter.next();
    		if (thisType.getRealm_type_name().equalsIgnoreCase(typeName)){
    			realm.setRealmType(thisType);
    			break;
    		}
    	}
    	
    	log.debug("Name: {}", realm.getRealm_name());
    	log.debug("Label: {}", realm.getRealm_label());
    	log.debug("Description: {}", realm.getRealm_description());
    	log.debug("ID: {}", realm.getRealm_id());
    	log.debug("RoleTemplate Id: {}", realm.getRoleTemplate().getRole_template_id());
    	
    	realmDao.update(realm);
    	manageRealmsViewBean.setRealms(realmDao.findAll(true));
    	handleClearSelectedRealms();
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Updated Realm", realm.getRealm_label()));
    	
    	
    }
    public void handleEditRealm(){
    	manageRealmsViewBean.setNewRealm(manageRealmsViewBean.getSelectedRealm());
    	if (manageRealmsViewBean.getSelectedRealm().getRoleTemplate() == null){
    		manageRealmsViewBean.setsSelectedTemplate("");
    	}else{
    		manageRealmsViewBean.setsSelectedTemplate(manageRealmsViewBean.getSelectedRealm().getRoleTemplate().getRole_template_name());
    	}
    	
    	manageRealmsViewBean.setsSelectedType(manageRealmsViewBean.getSelectedRealm().getRealmType().getRealm_type_name());
    	manageRealmsViewBean.setSubmitEditButtonRendered(true);
    	manageRealmsViewBean.setSubmitNewButtonRendered(false);
    	manageRealmsViewBean.setRenderInputForEdit(true);
    	manageRealmsViewBean.setRenderInputForNew(false);
    	manageRealmsViewBean.setRealmDlgHeader("Edit Realm");
    }
    public void handleCreateCommand(){
    	log.debug("In handleCreateCommand()");
    	manageRealmsViewBean.setNewRealm(new PalmsRealm());
    	manageRealmsViewBean.setsSelectedTemplate("");
    	manageRealmsViewBean.setsSelectedType("");
    	manageRealmsViewBean.setSubmitEditButtonRendered(false);
    	manageRealmsViewBean.setSubmitNewButtonRendered(true);
    	manageRealmsViewBean.setRenderInputForEdit(false);
    	manageRealmsViewBean.setRenderInputForNew(true);
    	manageRealmsViewBean.setRealmDlgHeader("Create New Realm");
    	log.debug("Finished handleCreateCommand()");
    }
    public void handleClearSelectedRealms(){
    	manageRealmsViewBean.setNewRealm(new PalmsRealm());
    }
    public void handleDeleteRealm(){
    	PalmsRealm realm = manageRealmsViewBean.getSelectedRealm();
    	int deleteCount = entityService.deleteEntity(EntityType.REALM_TYPE, realm.getRealm_id());
    	int subjectCount = subjectDao.deleteSubjectsByRealmId(realm.getRealm_id());
    	
    	realmDao.delete(realm);
    	manageRealmsViewBean.setRealms(realmDao.findAll(true));
    	
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.addMessage(null, new FacesMessage("Deleted Realm, and " + deleteCount + " Authorized Entities, " + subjectCount + " subjects.", realm.getRealm_label()));
    }
    public void handleImportSubjects(){
    	log.debug("Handle Importing Subjects from Realm here");
    }
    
    public String handleFlowProcessEvent(FlowEvent event){
    	return event.getNewStep();
    }
    

	public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	public ManageRealmsViewBean getManageRealmsViewBean() {
		return manageRealmsViewBean;
	}
	public void setManageRealmsViewBean(ManageRealmsViewBean manageRealmsViewBean) {
		this.manageRealmsViewBean = manageRealmsViewBean;
	}
	public RoleTemplateDao getRoleTemplateDao() {
		return roleTemplateDao;
	}
	public void setRoleTemplateDao(RoleTemplateDao roleTemplateDao) {
		this.roleTemplateDao = roleTemplateDao;
	}
	public RealmTypeDao getRealmTypeDao() {
		return realmTypeDao;
	}
	public void setRealmTypeDao(RealmTypeDao realmTypeDao) {
		this.realmTypeDao = realmTypeDao;
	}
	public PalmsRealmDao getRealmDao() {
		return realmDao;
	}
	public void setRealmDao(PalmsRealmDao realmDao) {
		this.realmDao = realmDao;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	



}
