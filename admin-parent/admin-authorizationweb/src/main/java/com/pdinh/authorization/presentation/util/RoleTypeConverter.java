package com.pdinh.authorization.presentation.util;

import java.io.Serializable;


import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.RoleTypeDao;

import com.pdinh.auth.persistence.entity.RoleType;

@ManagedBean(name = "roleTypeConverter")
@ViewScoped
@FacesConverter(value = "roleTypeConverter")
public class RoleTypeConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RoleTypeConverter.class);
	
	@EJB
	private RoleTypeDao roleTypeDao;
	
	public RoleTypeConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		if ((arg2 != null) && (!(arg2.equals(String.valueOf(0)))) && (!(arg2.equals("")))){
			int pid = Integer.valueOf(arg2);
			log.debug("In getAsObject method of RoleTypeConverter.  Argument: {}", pid);

			RoleType rt = roleTypeDao.findById(pid);
			log.debug("Returned RoleType Object: {}", rt.getName());
			return rt;

		} else {
			log.debug("Returning Null from roleTypeConverter getAsObject Method");
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		if ((!(arg2.equals(null))) && arg2 instanceof RoleType){
			String pid = "" + ((RoleType) arg2).getRole_type_id();
			log.debug("In getAsString method of roleTypeConverter.  Argument: {}", pid);
			return pid;
		}else{
			log.debug("Returning null from roleTypeConverter getAsString method");
			return null;
		}
		
	}



	public RoleTypeDao getRoleTypeDao() {
		return roleTypeDao;
	}

	public void setRoleTypeDao(RoleTypeDao roleTypeDao) {
		this.roleTypeDao = roleTypeDao;
	}

}
