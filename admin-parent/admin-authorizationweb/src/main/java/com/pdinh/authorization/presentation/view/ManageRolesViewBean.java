package com.pdinh.authorization.presentation.view;

import java.io.Serializable;
import java.util.List;


import javax.faces.bean.ManagedBean;

import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.pdinh.auth.persistence.entity.PermissionGroup;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleType;

/**
 * Session Bean implementation class DashboardViewBean
 */
@ManagedBean(name = "manageRolesViewBean")
@ViewScoped
public class ManageRolesViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String uid;

	private List<Role> roles;
	private List<PermissionGroup> permissionGroups;
	private List<RoleType> roleTypes;
	private RoleType selectedType = new RoleType();
	private String sSelectedType = "";
	private Role newRole = new Role();
	private Role selectedRole = new Role();
	private DualListModel<PermissionGroup> createRoleLists;
	private boolean editDisabled = true;
	private boolean deleteDisabled = true;
	
	private boolean renderRoleNameInputNew = true;
	private boolean renderRoleNameInputEdit = false;
	private boolean renderSaveButton = true;
	private boolean renderEditButton = false;
	private boolean createDisabled = true;
    /**
     * Default constructor. 
     */
    public ManageRolesViewBean() {
        // TODO Auto-generated constructor stub
    }
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
	public List<PermissionGroup> getPermissionGroups() {
		return permissionGroups;
	}
	public void setPermissionGroups(List<PermissionGroup> permissionGroups) {
		this.permissionGroups = permissionGroups;
	}
	public DualListModel<PermissionGroup> getCreateRoleLists() {
		return createRoleLists;
	}
	public void setCreateRoleLists(DualListModel<PermissionGroup> createRoleLists) {
		this.createRoleLists = createRoleLists;
	}
	public Role getNewRole() {
		return newRole;
	}
	public void setNewRole(Role newRole) {
		this.newRole = newRole;
	}
	public Role getSelectedRole() {
		return selectedRole;
	}
	public void setSelectedRole(Role selectedRole) {
		this.selectedRole = selectedRole;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public List<RoleType> getRoleTypes() {
		return roleTypes;
	}
	public void setRoleTypes(List<RoleType> roleTypes) {
		this.roleTypes = roleTypes;
	}
	public RoleType getSelectedType() {
		return selectedType;
	}
	public void setSelectedType(RoleType selectedType) {
		this.selectedType = selectedType;
	}
	public String getsSelectedType() {
		return sSelectedType;
	}
	public void setsSelectedType(String sSelectedType) {
		this.sSelectedType = sSelectedType;
	}
	
	public boolean isEditDisabled() {
		return editDisabled;
	}
	public void setEditDisabled(boolean editDisabled) {
		this.editDisabled = editDisabled;
	}
	public boolean isDeleteDisabled() {
		return deleteDisabled;
	}
	public void setDeleteDisabled(boolean deleteDisabled) {
		this.deleteDisabled = deleteDisabled;
	}

	public boolean isRenderRoleNameInputNew() {
		return renderRoleNameInputNew;
	}
	public void setRenderRoleNameInputNew(boolean renderRoleNameInputNew) {
		this.renderRoleNameInputNew = renderRoleNameInputNew;
	}
	public boolean isRenderRoleNameInputEdit() {
		return renderRoleNameInputEdit;
	}
	public void setRenderRoleNameInputEdit(boolean renderRoleNameInputEdit) {
		this.renderRoleNameInputEdit = renderRoleNameInputEdit;
	}
	public boolean isRenderEditButton() {
		return renderEditButton;
	}
	public void setRenderEditButton(boolean renderEditButton) {
		this.renderEditButton = renderEditButton;
	}
	public boolean isRenderSaveButton() {
		return renderSaveButton;
	}
	public void setRenderSaveButton(boolean renderSaveButton) {
		this.renderSaveButton = renderSaveButton;
	}
	public boolean isCreateDisabled() {
		return createDisabled;
	}
	public void setCreateDisabled(boolean createDisabled) {
		this.createDisabled = createDisabled;
	}

}
