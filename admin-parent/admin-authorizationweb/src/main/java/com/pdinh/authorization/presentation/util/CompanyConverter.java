package com.pdinh.authorization.presentation.util;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.RoleDao;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.persistence.ms.entity.Company;

@ManagedBean(name = "companyConverter")
@ViewScoped
@FacesConverter(value = "companyConverter")
public class CompanyConverter implements Converter, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CompanyConverter.class);
	
	@EJB
	private CompanyDao companyDao;
	
	public CompanyConverter(){
		
	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		if ((arg2 != null) && (!(arg2.equals(String.valueOf(0)))) && (!(arg2.equals(""))) ){
			int pid = Integer.valueOf(arg2);
			log.trace("In getAsObject method of permissionGroupConverter.  Argument: {}", pid);
			
			Company c = companyDao.findById(pid);
			log.trace("Returned Company Object: {}", c.getCoName());
			return c;

		}else{
			log.warn("Returning Null from PermissionGroupConverter getAsObjectMethod");
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		if ((!(arg2.equals(null))) && arg2 instanceof Company){
			String pid = "" + ((Company) arg2).getCompanyId();
			log.trace("In getAsString method of CompanyConverter.  Argument: {}", pid);
			return pid;
		} else {
			log.warn("Returning Null from CompanyConverter getAsString class");
			return null;
		}
		
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
