package com.pdinh.authorization.presentation.util;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.dao.RoleDao;

@ManagedBean(name = "roleNameValidator")
@ViewScoped
@FacesValidator(value = "roleNameValidator")
public class RoleNameValidator implements Validator, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RoleNameValidator.class);

	@EJB
	private RoleDao roleDao;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		// TODO Auto-generated method stub
		String roleName = String.valueOf(arg2);
		String errMsg = "";
		log.debug("Role Name: {}", roleName);
		log.debug("Role Dao Null? {}", roleDao);
		//Role role = roleDao.findRoleByName(roleName);
		boolean valid = true;
		if (arg2 == null){
			valid = false;
			errMsg = "Invalid Role Name.  Name cannot be null.";
		} else if (roleName.length() > 100){
			valid = false;
			errMsg = "Invalid Role Name.  Role name > 100 chars.";
		} else if (roleName.contains(" ")){
			valid = false;
			errMsg = "Invalid Role Name.  Role names cannot contain spaces.";
		} else if (roleName.equalsIgnoreCase("")){
			valid = false;
			errMsg = "Invalid Role Name.  Role name cannot be empty.";
		}
		if (arg1.getClientId().equalsIgnoreCase("roleNameInputNew")){
			if (!(roleDao.findRoleByName(roleName) == null)){
				valid = false;
				errMsg = "Invalid Role Name.  A Role with that name already exists.";
			}
		}
		if (!valid){
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, errMsg, 
					"The role name provided is null, already in use, contains invalid chars or is too long");
			throw new ValidatorException(message);
			
		}
				
		
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
