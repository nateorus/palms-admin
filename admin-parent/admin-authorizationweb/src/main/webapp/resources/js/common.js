function postToReportingServlet(xhr, status, args) {  
    var form = document.createElement("form");
    form.target = "_blank";
    form.method = "POST";
    form.action = args.reportingUrl;
    
    var input = document.createElement("textarea");
    input.name = "xml";
    input.value = unescapeRequestContextCallbackParam(args.reportXml);
    form.appendChild(input);        
    
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);    
}  

function postToExternalPage(xhr, status, args){
	
	var form = document.createElement("form");
	form.method = "POST";
	form.action = args.url;
	
	var tokenInput = document.createElement("input");
	tokenInput.name = "sessionToken";
	tokenInput.value = args.sessionToken;
	form.appendChild(tokenInput);
	
	var subjectIdInput = document.createElement("input");
	subjectIdInput.name = "subjectId";
	subjectIdInput.value = args.subjectId;
	form.appendChild(subjectIdInput);

	if(args.projectId != undefined) {
		var projectIdInput = document.createElement("input");
		projectIdInput.name = "projectId";
		projectIdInput.value = args.projectId;
		form.appendChild(projectIdInput);
	}
	
	if(args.returnTo != undefined) {
		var returnToInput = document.createElement("input");
		returnToInput.
		name = "returnTo";
		returnToInput.value = args.returnTo;
		form.appendChild(returnToInput);	
	}
	
	document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);  
}

function unescapeRequestContextCallbackParam(str) {
	str = str.replace("&lt;","<");
	str = str.replace("&gt;",">");
	return str;
}

function getDate(){
	var d = new Date();
	
	document.getElementById('loginForm:userTime').setAttribute('value', d.toDateString() + ' ' + d.toLocaleTimeString());

}