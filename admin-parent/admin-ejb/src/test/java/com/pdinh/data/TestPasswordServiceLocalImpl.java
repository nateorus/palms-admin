package com.pdinh.data;

import java.util.List;

import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.ValidatePasswordResultObj;

public class TestPasswordServiceLocalImpl {
	public static void main(String[] args){
		String password = "ARnateA012nn!!!!55555";
		PasswordPolicy policy = new PasswordPolicy();
		policy.setMaximumLength(20);
		policy.setMinimumLength(8);
		policy.setRequireLowercase(true);
		policy.setRequireUppercase(true);
		policy.setRequireNumeric(true);
		policy.setRequireSpecial(true);
		//custom requirement: password must contain the string "nate"
		policy.setValidationCustomRegex(".*(nate).*");
		
		PasswordServiceLocalImpl impl = new PasswordServiceLocalImpl();
		ValidatePasswordResultObj result = impl.validatePassword(password, policy);
	
		System.out.println(result.getStatus());
		if (!result.getSuccess()){
			
			List<ErrorObj> errs = result.getErrors();
			
			for (ErrorObj err: errs){
				System.out.println("Error Code: " + err.getErrorCode() + " Error Message : " + err.getErrorMsg());
			}
		}
		
	}
}
