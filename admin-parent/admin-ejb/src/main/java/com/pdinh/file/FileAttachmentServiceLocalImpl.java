package com.pdinh.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.persistence.ms.entity.FileAttachment;


/**
 * Service for reading / writing file attachments on a file system.
 * 
 */
@Stateless
@LocalBean
public class FileAttachmentServiceLocalImpl implements FileAttachmentServiceLocal {

	@Resource(mappedName="application/config_properties")
	private Properties properties;
	
	@Override
	public void write(FileAttachment fileAttachment, InputStream is) throws IOException {				
		File root = new File(getFilesRootPath());
		//create the new dir		
		
		File newDir = new File(root,fileAttachment.getPath());
		newDir.mkdirs();
		
		File newFile = new File(newDir,fileAttachment.getName());
		writeFileFromStream(newFile,is);
	}

	@Override
	public void delete(FileAttachment fileAttachment) throws IOException {
		delete(fileAttachment.getPath() + "/" + fileAttachment.getName());		
	}

	@Override
	public InputStream getAsStream(FileAttachment fileAttachment) throws IOException {		
		return new FileInputStream(getFile(fileAttachment));		
	}
	
	@Override
	public InputStream getAsStream(String path) throws IOException{		
		File root = new File(getFilesRootPath());
		return new FileInputStream(new File(root,path));		
	}

	private String getFilesRootPath() {
		return properties.getProperty("filesRoot");
	}
	
	private File getFile(FileAttachment fileAttachment) {
		return new File(getFilesRootPath() + "/" + fileAttachment.getPath() + "/" + fileAttachment.getName());
	}

	@Override
	public void write(String path, InputStream is) throws IOException {
		File root = new File(getFilesRootPath());
		//create the new dir
		String pathForwardSlashes = path.replace("\\","/");
		String pathDirOnly = pathForwardSlashes.substring(0,pathForwardSlashes.lastIndexOf("/"));
		String name = pathForwardSlashes.substring(pathForwardSlashes.lastIndexOf("/"),pathForwardSlashes.length());
		
		File newDir = new File(root,pathDirOnly);
		newDir.mkdirs();
		
		File newFile = new File(newDir,name);
		
		writeFileFromStream(newFile,is);		
	}

	@Override
	public void delete(String path) throws IOException {
		File file = new File(getFilesRootPath());
		File fileToDelete = new File(file,path);
		System.out.println("Deleting file: " + fileToDelete.getAbsolutePath());
		File parentFile = fileToDelete.getParentFile();
		fileToDelete.delete();		
		//delete parent directory too
		System.out.println("Deleting directory: " + parentFile.getAbsolutePath());
		parentFile.delete();
	}
	
	private void writeFileFromStream(File file, InputStream is) throws IOException {
		FileOutputStream fos = null;
		try {
			file.createNewFile();
			int bytesRead = 0;
			byte[] bytes = new byte[1024];
			fos = new FileOutputStream(file);
			
			while((bytesRead = is.read(bytes)) > 0) {
				fos.write(bytes,0,bytesRead);				
			}			
		} catch (IOException e) {
			throw new IOException(e);
		} finally {
			//messy, but need to make sure each one is 
			//called at least once
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

}
