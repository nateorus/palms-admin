package com.pdinh.file;

import java.io.IOException;
import java.io.InputStream;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.FileAttachment;

@Local
public interface FileAttachmentServiceLocal {

	void write(FileAttachment fileAttachment, InputStream is) throws IOException;	
	void write(String path, InputStream is) throws IOException;
	void delete(FileAttachment fileAttachment) throws IOException;
	void delete(String path) throws IOException;
	InputStream getAsStream(FileAttachment fileAttachment) throws IOException;
	InputStream getAsStream(String path) throws IOException;	
	
}
