package com.pdinh.manage.emailtemplates;

import java.util.List;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;

@Local
public interface EmailTemplatesService {
	public EmailTemplateObj scheduledEmailDef2EmailTemplateObj(ScheduledEmailDef template);

	public void createNewTemplate(EmailTemplateObj templateObj, String level, Company company, Project project);

	public void updateTemplate(EmailTemplateObj templateObj, String level);

	public void cloneTemplate(EmailTemplateObj template, String title, String level, Company company, Project project);

	public void deleteTemplate(EmailTemplateObj template);

	public List<ScheduledEmailDef> getEmailTemplatesByLevel(String level, Company company, Project project);

	public List<ListItemObj> getTemplateLanguges(EmailTemplateObj template);

	public EmailTextObj findEmailTextByLangCode(EmailTemplateObj template, String code);

	public List<ListItemObj> getTemplateProducts(int companyId);

	public List<ListItemObj> getEmailSenderKeys();

	public List<ListItemObj> getTemplateStyles();

	public List<String> getEmailMergeFields();

	public List<String> getEmailRuleNamesByTemplate(EmailTemplateObj template);

	public List<ListItemObj> getUnusedLanguages(EmailTemplateObj template);

	public Boolean isTemplateExits(Integer templateId);

}
