package com.pdinh.manage.emailtemplates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailFromFieldKeyCombinationDao;
import com.pdinh.data.ms.dao.EmailRuleDao;
import com.pdinh.data.ms.dao.ProductDao;
import com.pdinh.data.ms.dao.ScheduledEmailCompanyDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.data.ms.dao.ScheduledEmailProjectDao;
import com.pdinh.data.ms.dao.ScheduledEmailStyleDao;
import com.pdinh.data.ms.dao.ScheduledEmailTextDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.EmailFromFieldKeyCombination;
import com.pdinh.persistence.ms.entity.EmailRule;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.Product;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailCompany;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailProject;
import com.pdinh.persistence.ms.entity.ScheduledEmailStyle;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.presentation.domain.EmailTemplateObj;
import com.pdinh.presentation.domain.EmailTextObj;
import com.pdinh.presentation.domain.ListItemObj;

/**
 * Session Bean implementation class MailTemplatesServiceLocalImpl
 */
@Stateless
@LocalBean
public class EmailTemplatesServiceImpl implements EmailTemplatesService {

	private static final Logger log = LoggerFactory.getLogger(EmailTemplatesServiceImpl.class);

	@EJB
	ScheduledEmailDefDao scheduledEmailDefDao;

	@EJB
	ScheduledEmailCompanyDao scheduledEmailCompanyDao;

	@EJB
	ScheduledEmailProjectDao scheduledEmailProjectDao;

	@EJB
	ScheduledEmailStyleDao scheduledEmailStyleDao;

	@EJB
	ScheduledEmailTextDao scheduledEmailTextDao;

	@EJB
	EmailFromFieldKeyCombinationDao emailFromFieldKeyCombinationDao;

	@EJB
	ProductDao productDao;

	// @EJB
	// LanguageDao languageDao;

	@EJB
	EmailRuleDao emaiRuleDao;

	@EJB
	private LanguageSingleton languageSingleton;
	

	@Override
	public EmailTemplateObj scheduledEmailDef2EmailTemplateObj(ScheduledEmailDef template) {
		List<EmailTextObj> templateTextObjs = new ArrayList<EmailTextObj>();
		
		//Not elegant, but fewer db hits.. clean this up if you are bored
		String typeConstantLabel = "";
		if (!(template.getConstant() == null)){
			if (template.getConstant().equals(ScheduledEmailStyle.CUSTOM_CONSTANT)){
				typeConstantLabel = ScheduledEmailStyle.CUSTOM_LABEL_EN;
			}else if (template.getConstant().equals(ScheduledEmailStyle.FIRST_PASSWORD_CONSTANT)){
				typeConstantLabel = ScheduledEmailStyle.FIRST_PASSWORD_LABEL_EN;
			}else if (template.getConstant().equals(ScheduledEmailStyle.PARTICIPANT_REMINDER_CONSTANT)){
				typeConstantLabel = ScheduledEmailStyle.PARTICIPANT_REMINDER_LABEL_EN;
			}
		}
		EmailTemplateObj templateObj = new EmailTemplateObj(template.getId(), template.getTitle(),
				template.getConstant(), template.getEmailType(), template.getIsTemplate(), template.getLevel(),
				template.getFromKeyCode(), template.getCustomFrom(), template.getCustomReplyTo(),
				typeConstantLabel,
				template.getEmailTypeProdLabel());
				//getProductNameByCode(template.getEmailType()));
		//List<ScheduledEmailText> texts = scheduledEmailTextDao.findByDefBatchLang(template.getId());
		for (ScheduledEmailText templateText : template.getScheduledEmailTexts()) {
			/*
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> scheduledEmailDef2EmailTemplateObj templateText = {}", templateText);
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> scheduledEmailDef2EmailTemplateObj templateText.getId = {}", templateText.getId());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> scheduledEmailDef2EmailTemplateObj templateText.getSubject = {}", templateText.getSubject());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> scheduledEmailDef2EmailTemplateObj templateText.getText = {}", templateText.getText());
			*/
			templateTextObjs.add(new EmailTextObj(templateText.getId(), templateText.getSubject(), templateText
					.getText(), templateText.getLanguage().getCode()));
		}
		templateObj.setEmailText(templateTextObjs);
		templateObj.setActive(template.isActive());
		return templateObj;
	}

	private String getProductNameByCode(String code) {
		Product product = productDao.findProductByCode(code);
		if (product != null) {
			return product.getName();
		}
		return "";
	}

	private ScheduledEmailDef setEmailTemplateFields(ScheduledEmailDef target, EmailTemplateObj source,
			String overrideLevel, boolean update) {

		target.setTitle(source.getTitle());
		target.setActive(source.isActive());
		if (overrideLevel == null) {
			target.setLevel(source.getLevel());
			target.setIsTemplate(source.getIsTemplate());
		} else {
			target.setLevel(overrideLevel);
			if (overrideLevel.equals(ScheduledEmailDef.GLOBAL) || overrideLevel.equals(ScheduledEmailDef.SYS)) {
				target.setIsTemplate(1);
			} else {
				target.setIsTemplate(0);
			}

		}

		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields target.level = {}", target.getLevel());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields target.isTemplate = {}", target.getIsTemplate());

		target.setConstant(source.getStyle());
		target.setEmailType(source.getType());
		// target.setConstantStyleLabel(source.getConstantStyleLabel()); // mb:
		// removing because we are removing this column in db
		// target.setEmailTypeProdLabel(source.getEmailTypeProdLabel());

		target.setFromKeyCode(source.getFromKeyCode());

		if (target.getFromKeyCode().equals("CUSTOM")) {
			target.setCustomFrom(source.getCustomFrom());
			target.setCustomReplyTo(source.getCustomReplyTo());
		} else {
			target.setCustomFrom(null);
			target.setCustomReplyTo(null);
		}

		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields source = {}", source);
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getTitle = {}", source.getTitle());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields isActive = {}", source.isActive());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getIsTemplate = {}", source.getIsTemplate());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getLevel = {}", source.getLevel());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getType = {}", source.getType());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getStyle = {}", source.getStyle());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getType = {}", source.getType());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getConstantStyleLabel = {}",
				source.getConstantStyleLabel());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getEmailTypeProdLabel = {}",
				source.getEmailTypeProdLabel());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getFromKeyCode = {}", source.getFromKeyCode());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getCustomFrom = {}", source.getCustomFrom());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getCustomReplyTo = {}", source.getCustomReplyTo());
		log.debug(">>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getEmailText = {}", source.getEmailText());

		// add case
		for (EmailTextObj textObj : source.getEmailText()) {
			ScheduledEmailText templateText = null;
			boolean found = false;
			for (ScheduledEmailText text : target.getScheduledEmailTexts()) {
				if (textObj.getLangCode().equals(text.getLanguage().getCode())) {
					templateText = text;
					found = true;
					break;
				}
			}

			if (!found) {
				templateText = new ScheduledEmailText();
			}

			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields textObj = {}", textObj);
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getId = {}",
					(textObj != null) ? textObj.getId() : "XXXXX");
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getSubject = {}",
					textObj.getSubject());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getEmailText = {}",
					textObj.getEmailText());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getLangCode = {}",
					textObj.getLangCode());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

			templateText.setSubject(textObj.getSubject());
			templateText.setText(textObj.getEmailText());
			// templateText.setLanguage(languageDao.findByCode(textObj.getLangCode()));
			templateText.setLanguage(languageSingleton.getLanguageByCode(textObj.getLangCode()));
			templateText.setScheduledEmailDef(target);

			if (!found) {
				target.getScheduledEmailTexts().add(templateText);
			}

			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields text = {}", templateText);
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getId = {}", templateText.getId());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getSubject = {}", templateText.getSubject());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getEmailText = {}", templateText.getText());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>> setEmailTemplateFields getLangCode = {}", templateText.getLanguage()
					.getCode());
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}

		// creating delete list
		List<ScheduledEmailText> deleteList = new ArrayList<ScheduledEmailText>();

		for (ScheduledEmailText text : target.getScheduledEmailTexts()) {
			boolean found = false;
			for (EmailTextObj textObj : source.getEmailText()) {
				if (text.getLanguage().getCode().equals(textObj.getLangCode())) {
					found = true;
					break;
				}
			}
			if (!found) {
				deleteList.add(text);
			}
		}

		// deleting
		for (ScheduledEmailText item : deleteList) {
			target.getScheduledEmailTexts().remove(item);
		}

		return target;
	}

	@Override
	public void createNewTemplate(EmailTemplateObj templateObj, String level, Company company, Project project) {
		ScheduledEmailDef template = new ScheduledEmailDef();

		template = setEmailTemplateFields(template, templateObj, level, false);
		
		ScheduledEmailCompany sec = new ScheduledEmailCompany();
		sec.setCompany(company);
		sec.setScheduledEmailDef(template);
		List<ScheduledEmailCompany> secs = new ArrayList<ScheduledEmailCompany>();
		secs.add(sec);
		template.setScheduledEmailCompanies(secs);

		template = scheduledEmailDefDao.create(template);

		

		// if there's a project, create the project
		if (template.getLevel().equals(ScheduledEmailDef.PROJ)) {
			ScheduledEmailProject sep = new ScheduledEmailProject();
			sep.setProject(project);
			sep.setCompany(company);
			sep.setScheduledEmailDef(template);
			scheduledEmailProjectDao.create(sep);
		}

	}

	@Override
	public void updateTemplate(EmailTemplateObj templateObj, String level) {
		ScheduledEmailDef template = scheduledEmailDefDao.findById(templateObj.getId());
		template = setEmailTemplateFields(template, templateObj, level, true);
		scheduledEmailDefDao.update(template);
	}

	@Override
	public void cloneTemplate(EmailTemplateObj template, String title, String level, Company company, Project project) {
		ScheduledEmailDef emailDefSource = scheduledEmailDefDao.findById(template.getId());
		ScheduledEmailDef emailDefClone = new ScheduledEmailDef();

		emailDefClone.setTitle(title);
		emailDefClone.setEmailType(emailDefSource.getEmailType());
		emailDefClone.setConstant(emailDefSource.getConstant());
		// emailDefClone.setConstantStyleLabel(emailDefSource.getConstantStyleLabel());
		// removing because we are removing this column from
		// scheduledEmailDef
		// emailDefClone.setEmailTypeProdLabel(emailDefSource.getEmailTypeProdLabel());
		emailDefClone.setLevel(level);
		if (emailDefSource.getFromKeyCode() != null) {
			emailDefClone.setFromKeyCode(emailDefSource.getFromKeyCode());
		} else {
			emailDefClone.setFromKeyCode(emailFromFieldKeyCombinationDao.findKeyCombinationByKeyCode(
					EmailFromFieldKeyCombination.GSC).getKeyCode());
		}
		emailDefClone.setCustomFrom(emailDefSource.getCustomFrom());
		emailDefClone.setCustomReplyTo(emailDefSource.getCustomReplyTo());
		emailDefClone.setActive(true);
		emailDefClone.setIsTemplate(0);

		emailDefClone.setScheduledEmailTexts(new ArrayList<ScheduledEmailText>());

		for (ScheduledEmailText sourceText : emailDefSource.getScheduledEmailTexts()) {
			ScheduledEmailText cloneText = new ScheduledEmailText();
			cloneText.setLanguage(sourceText.getLanguage());
			cloneText.setSubject(sourceText.getSubject());
			cloneText.setText(sourceText.getText());
			cloneText.setScheduledEmailDef(emailDefClone);
			emailDefClone.getScheduledEmailTexts().add(cloneText);
		}
		
		// update company
		if (company != null) {
			ScheduledEmailCompany sec = new ScheduledEmailCompany();
			sec.setCompany(company);
			sec.setScheduledEmailDef(emailDefClone);
			//scheduledEmailCompanyDao.create(sec);
			company.getScheduledEmailCompanies().add(sec);
			List<ScheduledEmailCompany> secs = new ArrayList<ScheduledEmailCompany>();
			secs.add(sec);
			emailDefClone.setScheduledEmailCompanies(secs);
		}

		scheduledEmailDefDao.create(emailDefClone);

		

		// update project
		if (level.equals(ScheduledEmailDef.PROJ)) {
			if (project != null) {
				ScheduledEmailProject sep = new ScheduledEmailProject();
				sep.setProject(project);
				sep.setCompany(company);
				sep.setScheduledEmailDef(emailDefClone);
				scheduledEmailProjectDao.create(sep);
				project.getScheduledEmailProjects().add(sep);
			}
		}

	}

	@Override
	public void deleteTemplate(EmailTemplateObj template) {
		ScheduledEmailDef sed = scheduledEmailDefDao.findById(template.getId());
		
		scheduledEmailDefDao.delete(sed);
	}


	@Override
	public List<ScheduledEmailDef> getEmailTemplatesByLevel(String level, Company company, Project project) {
		List<ScheduledEmailDef> templates = new ArrayList<ScheduledEmailDef>();

		if (level.equals(ScheduledEmailDef.PROJ)) {
			templates = scheduledEmailDefDao.findAllProjectTemplates(company.getCompanyId(), project.getProjectId());
		} else if (level.equals(ScheduledEmailDef.CLIENT)) {
			templates = scheduledEmailDefDao.findAllClientTemplates(company.getCompanyId());
		} else if (level.equals(ScheduledEmailDef.GLOBAL)) {
			templates = scheduledEmailDefDao.findAllGlobalTemplates();
		} else if (level.equals(ScheduledEmailDef.SYS)) {
			templates = scheduledEmailDefDao.findAllSystemTemplates();
		}

		/*
		for(ScheduledEmailDef template : templates) {
			for(ScheduledEmailText templateText : template.getScheduledEmailTexts()) {
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> getEmailTemplatesByLevel templateText = {}", templateText);
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> getEmailTemplatesByLevel templateText.getId = {}", templateText.getId());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> getEmailTemplatesByLevel templateText.getSubject = {}", templateText.getSubject());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>> getEmailTemplatesByLevel templateText.getText = {}", templateText.getText());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			}
		}
		*/

		return templates;
	}

	/* 
	 * Searches Email Template and returns EmailTextObj base on the language code 
	 */
	@Override
	public EmailTextObj findEmailTextByLangCode(EmailTemplateObj template, String code) {
		for (EmailTextObj text : template.getEmailText()) {
			if (text.getLangCode().equalsIgnoreCase(code)) {
				return text;
			}
		}
		return null;
	}

	@Override
	public List<ListItemObj> getTemplateLanguges(EmailTemplateObj template) {
		List<ListItemObj> langs = new ArrayList<ListItemObj>();
		if (template != null) {
			for (EmailTextObj text : template.getEmailText()) {
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>> getTemplateLanguges EmailTextObj= {}, {}", text.getLangCode(),
						text.getSubject());
				Language lang = languageSingleton.getLanguageByCode(text.getLangCode());
				log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>> getTemplateLanguges lang = {}", lang);
				if (lang == null) {
					log.debug("Language Not found: {}", text.getLangCode());
				} else {
					langs.add(new ListItemObj(lang.getName(), lang.getCode()));
				}
			}
		} else {
			Language lang = languageSingleton.getLanguageByCode("en");
			langs.add(new ListItemObj(lang.getName(), lang.getCode()));
		}

		return langs;
	}

	@Override
	public List<ListItemObj> getTemplateProducts(int companyId) {
		List<Product> products = productDao.findAssessmentProductsByCompanyId(companyId);
		List<ListItemObj> templateProducts = new ArrayList<ListItemObj>();

		for (Product product : products) {
			templateProducts.add(new ListItemObj(product.getName(), product.getCode()));
		}

		return templateProducts;
	}

	@Override
	public List<ListItemObj> getEmailSenderKeys() {
		// set up the email combination lists (doesn't matter if new or add
		// translation,etc.)
		List<EmailFromFieldKeyCombination> keyList = emailFromFieldKeyCombinationDao.findAll();
		List<ListItemObj> emailCombList = new ArrayList<ListItemObj>();

		for (EmailFromFieldKeyCombination key : keyList) {
			ListItemObj item = new ListItemObj();
			item.setId(key.getKeyCode());
			item.setName(key.getSenderName());
			emailCombList.add(item);
		}

		return emailCombList;
	}

	@Override
	public List<ListItemObj> getTemplateStyles() {
		// set up the template constants (styles) drop down list // called Style
		// in UI
		HashMap<String, String> constants = scheduledEmailStyleDao.getTemplateConstants();
		List<ListItemObj> styleList = new ArrayList<ListItemObj>();
		Iterator<String> stringIt = constants.keySet().iterator();
		while (stringIt.hasNext()) {
			ListItemObj lio = new ListItemObj();
			String key = stringIt.next().toString();
			lio.setId(key);
			lio.setName(constants.get(key));
			styleList.add(lio);
		}

		return styleList;

	}

	@Override
	public List<String> getEmailMergeFields() {

		List<String> fields = new ArrayList<String>();

		fields.add("[partFirstName]");
		fields.add("[partLastName]");
		fields.add("[partEmailAddress]");
		fields.add("[partId]");
		fields.add("[partPassword]");
		fields.add("[partOption1]");
		fields.add("[partOption2]");
		fields.add("[partOption3]");
		fields.add("[partManagerName]");
		fields.add("[partManagerEmail]");
		fields.add("[projectName]");
		fields.add("[clientName]");
		fields.add("[partUrl]");
		// additional KFAP statics for PALMS 1.5
		fields.add("[recipientFirstLastName]");
		fields.add("[recipientLastfirstName]");
		fields.add("[projectDueDate]");
		fields.add("[projectDaysRemaining]");
		// fields.add("[assesseeNameKFAP]");
		// fields.add("[reviewerNameKFAP]");
		// fields.add("[passwdResetUrlAsmtPortal]");
		// fields.add("[passwdResetUrlClientPortal]");
		// fields.add("[participantsWithinEvent]");
		fields.add("[recipientFirstName]");
		fields.add("[siteUrlAP]");
		fields.add("[siteUrlCP]");
		// fields.add("[assesseeName]");

		return fields;

	}

	@Override
	public List<String> getEmailRuleNamesByTemplate(EmailTemplateObj template) {

		List<EmailRule> emailRules = emaiRuleDao.findEmailRulesByEmailTemplateId(template.getId());
		List<String> emailRuleNames = new ArrayList<String>();

		if (!emailRules.isEmpty()) {
			for (EmailRule emailRule : emailRules) {
				emailRuleNames.add(emailRule.getName());
			}
		}
		return emailRuleNames;
	}

	@Override
	public List<ListItemObj> getUnusedLanguages(EmailTemplateObj template) {

		List<Language> languages = languageSingleton.getLanguageEntities();
		List<ListItemObj> unusedLanguages = new ArrayList<ListItemObj>();
		List<ListItemObj> templateLanguages = getTemplateLanguges(template);

		for (Language language : languages) {
			boolean found = false;
			for (ListItemObj templateLanguage : templateLanguages) {
				if (templateLanguage.getId().equals(language.getCode())) {
					found = true;
					break;
				}
			}

			if (!found) {
				unusedLanguages.add(new ListItemObj(language.getName(), language.getCode()));
			}
		}

		return unusedLanguages;
	}

	@Override
	public Boolean isTemplateExits(Integer templateId) {
		if (templateId != null) {
			if (scheduledEmailDefDao.findById(templateId) != null) {
				return true;
			}
		}
		return false;
	}

}
