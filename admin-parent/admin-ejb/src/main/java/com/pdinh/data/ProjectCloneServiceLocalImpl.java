package com.pdinh.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.persistence.FieldEntityType;
import com.kf.uffda.service.UffdaService;
import com.pdinh.data.ms.dao.EmailRuleCriteriaTypeDao;
import com.pdinh.data.ms.dao.EmailRuleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleTypeDao;
import com.pdinh.data.ms.dao.ScheduledEmailDefDao;
import com.pdinh.data.ms.dao.ScheduledEmailTextDao;
import com.pdinh.manage.emailtemplates.EmailTemplatesService;

@Stateless
@LocalBean
public class ProjectCloneServiceLocalImpl implements ProjectCloneServiceLocal {

	@EJB
	EmailRuleDao emailRuleDao;

	@EJB
	EmailRuleCriteriaTypeDao emailRuleCriteriaTypeDao;

	@EJB
	EmailRuleScheduleTypeDao emailRuleScheduleTypeDao;

	@EJB
	EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	ScheduledEmailDefDao scheduledEmailDefDao;

	@EJB
	ScheduledEmailTextDao scheduledEmailTextDao;

	@EJB
	EmailRuleScheduleDao emailRuleScheduleDao;

	@EJB
	EmailTemplatesService emailTemplatesService;

	@EJB
	UffdaService uffdaService;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cloneProjectCustomFields(int userId, int companyId, int existingProjectId, int newProjectId) {
		List<FieldDto> customFields = new ArrayList<FieldDto>();

		// Get the external Custom Fields
		uffdaService.getListOfCustomFields(companyId, existingProjectId, customFields,
				FieldEntityType.PALMS_PROJECT_USERS, false, false, false, false);

		// Get the internal Custom Fields
		uffdaService.getListOfCustomFields(companyId, existingProjectId, customFields,
				FieldEntityType.PALMS_PROJECT_USERS, true, false, false, false);
		Collections.sort(customFields);

		// Make the custom fields look like they changed
		for (FieldDto field : customFields) {
			field.setChanged(true);
		}

		// Add the custom fields to the project
		uffdaService.saveCustomFieldsToFieldMap(userId, newProjectId, FieldEntityType.PALMS_PROJECT_USERS,
				customFields, new ArrayList<FieldDto>());
	}
}
