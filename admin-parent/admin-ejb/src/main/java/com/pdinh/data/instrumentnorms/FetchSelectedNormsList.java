package com.pdinh.data.instrumentnorms;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FetchSelectedNormsList")
@XmlAccessorType(XmlAccessType.FIELD)
public class FetchSelectedNormsList {
	@XmlAttribute(name = "project_id")
	private Integer projectId;

	@XmlElementWrapper(name = "participantList")
	@XmlElement(name = "participant")
	private List<Participant> participants = new ArrayList<Participant>();

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer project) {
		this.projectId = project;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

}
