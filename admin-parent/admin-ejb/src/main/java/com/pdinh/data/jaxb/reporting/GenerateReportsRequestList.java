package com.pdinh.data.jaxb.reporting;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "generateReportsRequests")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenerateReportsRequestList {
	
	@XmlElement(name = "totalRecordCount")
	long totalRecordCount;
	
	@XmlElement(name = "reportsRequest")
	List<GenerateReportsRequest> reportsRequests;

	public List<GenerateReportsRequest> getReportsRequests() {
		return reportsRequests;
	}

	public void setReportsRequests(List<GenerateReportsRequest> reportsRequests) {
		this.reportsRequests = reportsRequests;
	}

	public long getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

}
