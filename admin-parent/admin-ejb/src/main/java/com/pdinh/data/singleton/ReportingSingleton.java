package com.pdinh.data.singleton;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.CourseVersionDao;
import com.pdinh.data.ms.dao.ReportRequestStatusDao;
import com.pdinh.data.ms.dao.ReportSourceTypeDao;
import com.pdinh.data.ms.dao.ReportTypeDao;
import com.pdinh.data.ms.dao.TargetLevelGroupDao;
import com.pdinh.data.ms.dao.TargetLevelGroupTypeDao;
import com.pdinh.data.ms.dao.TargetLevelTypeDao;
import com.pdinh.persistence.ms.entity.CourseVersion;
import com.pdinh.persistence.ms.entity.ReportRequestStatus;
import com.pdinh.persistence.ms.entity.ReportSourceType;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelGroup;
import com.pdinh.persistence.ms.entity.TargetLevelGroupType;
import com.pdinh.persistence.ms.entity.TargetLevelType;

@Singleton(mappedName = "reportingSingleton")
public class ReportingSingleton {

	private static final Logger log = LoggerFactory.getLogger(ReportingSingleton.class);

	public static final String SERVER_STATUS_OK = "OK";
	public static final String SERVER_STATUS_DOWN = "DOWN";
	public static final String SERVER_STATUS_UNKNOWN = "UNKNOWN";

	@EJB
	private ReportRequestStatusDao reportRequestStatusDao;

	@EJB
	private TargetLevelTypeDao targetLevelTypeDao;

	@EJB
	private ReportTypeDao reportTypeDao;

	@EJB
	private TargetLevelGroupDao targetLevelGroupDao;

	@EJB
	private TargetLevelGroupTypeDao targetLevelGroupTypeDao;

	@EJB
	private ReportSourceTypeDao reportSourceTypeDao;

	@EJB
	private CourseVersionDao courseVersionDao;

	private List<ReportSourceType> reportSourceTypes;

	private List<ReportRequestStatus> reportRequestStatuses;

	private List<TargetLevelType> targetLevels;

	private List<ReportType> reportTypes;

	private List<TargetLevelGroup> targetLevelGroups;

	private List<TargetLevelGroupType> targetLevelGroupTypes;

	private List<CourseVersion> courseVersions;

	private String reportServerStatus = ReportingSingleton.SERVER_STATUS_UNKNOWN;
	private Date reportServerStatusChangeTime;

	private String oxcartServerStatus = ReportingSingleton.SERVER_STATUS_UNKNOWN;
	private Date oxcartServerStatusChangeTime;

	private String tincanServerStatus = ReportingSingleton.SERVER_STATUS_UNKNOWN;
	private Date tincanServerStatusChangeTime;

	private String adminServerStatus = ReportingSingleton.SERVER_STATUS_UNKNOWN;
	private Date adminServerStatusChangeTime;

	private String kfoReportApiStatus = ReportingSingleton.SERVER_STATUS_UNKNOWN;
	private Date kfoReportApiStatusChangeTime;

	private Map<Integer, Timestamp> requestQueueMap = new ConcurrentHashMap<Integer, Timestamp>();

	private void initCourseVersions() {
		this.courseVersions = this.courseVersionDao.findAllBatchCourse();
	}

	private void initReportSourceTypes() {
		this.reportSourceTypes = reportSourceTypeDao.findAll();
	}

	private void initReportRequestStatuses() {
		this.reportRequestStatuses = reportRequestStatusDao.findAll();
	}

	private void initTargetLevels() {
		this.targetLevels = targetLevelTypeDao.findAll();
	}

	private void initReportTypes() {
		this.reportTypes = reportTypeDao.findAll();
	}

	private void initTargetLevelGroups() {
		this.targetLevelGroups = targetLevelGroupDao.findAll();
	}

	private void initTargetLevelGroupTypes() {
		this.targetLevelGroupTypes = targetLevelGroupTypeDao.findAllGroupTypesBatch();
	}

	public List<CourseVersion> getCourseVersions(int courseId) {
		if (this.courseVersions == null) {
			this.initCourseVersions();
		}
		List<CourseVersion> versions = new ArrayList<CourseVersion>();
		for (CourseVersion version : this.getCourseVersions()) {
			if (version.getCourse().getCourse() == courseId) {
				versions.add(version);
			}
		}
		return versions;
	}

	public CourseVersion getCourseVersionByCompletionDate(Timestamp completionDate, int courseId) {
		for (CourseVersion version : this.getCourseVersions(courseId)) {
			if (completionDate != null) {
				if (completionDate.after(version.getVersionStartTs())) {
					if (version.getVersionEndTs() == null || completionDate.before(version.getVersionEndTs())) {
						return version;
					}
				}
			} else {
				if (version.isActive() && version.isDefault()) {
					return version;
				}
			}
		}
		return null;
	}

	public List<ReportSourceType> getAllReportSourceTypes() {
		if (this.reportSourceTypes == null) {
			this.initReportSourceTypes();
		}
		return this.reportSourceTypes;
	}

	public ReportType getReportType(String code) {
		if (this.reportTypes == null) {
			this.initReportTypes();
		}
		for (ReportType t : this.reportTypes) {
			if (t.getCode().equals(code)) {
				return t;
			}
		}
		return null;
	}

	public TargetLevelType getTargetLevelTypeById(int id) {
		if (this.targetLevels == null) {
			this.initTargetLevels();
		}
		for (TargetLevelType t : this.targetLevels) {
			if (t.getTargetLevelTypeId() == id) {
				return t;
			}
		}
		return null;
	}

	public List<ReportRequestStatus> getReportRequestStatuses() {
		if (this.reportRequestStatuses == null) {
			this.initReportRequestStatuses();
		}
		return this.reportRequestStatuses;
	}

	public List<String> getAllReportRequestStatuses() {
		if (this.reportRequestStatuses == null) {
			this.initReportRequestStatuses();
		}
		List<String> allStatusCodes = new ArrayList<String>();
		for (ReportRequestStatus s : this.reportRequestStatuses) {
			allStatusCodes.add(s.getCode());
		}
		return allStatusCodes;
	}

	public ReportRequestStatus getReportRequestStatus(String code) {
		if (reportRequestStatuses == null) {
			initReportRequestStatuses();
		}

		for (ReportRequestStatus status : this.reportRequestStatuses) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;

	}

	public TargetLevelType getTargetLevelTypeByCode(String code) {
		if (targetLevels == null) {
			this.targetLevels = targetLevelTypeDao.findAll();
		}
		for (TargetLevelType tlt : this.targetLevels) {
			if (tlt.getCode().equals(code)) {
				return tlt;
			}
		}
		return null;
	}

	public List<TargetLevelType> getTargetLevelTypesByGroup(int groupId) {
		if (targetLevelGroupTypes == null) {
			this.initTargetLevelGroupTypes();
		}
		List<TargetLevelType> types = new ArrayList<TargetLevelType>();
		for (TargetLevelGroupType t : targetLevelGroupTypes) {
			if (t.getTargetLevelGroup().getTargetLevelGroupId() == groupId) {
				types.add(t.getTargetLevelType());
			}
		}
		return types;
	}

	public String getReportServerStatus() {
		return reportServerStatus;
	}

	public void setReportServerStatus(String reportServerStatus) {
		this.reportServerStatus = reportServerStatus;
	}

	public Date getReportServerStatusChangeTime() {
		return reportServerStatusChangeTime;
	}

	public void setReportServerStatusChangeTime(Date reportServerStatusChangeTime) {
		this.reportServerStatusChangeTime = reportServerStatusChangeTime;
	}

	public String getOxcartServerStatus() {
		return oxcartServerStatus;
	}

	public void setOxcartServerStatus(String oxcartServerStatus) {
		this.oxcartServerStatus = oxcartServerStatus;
	}

	public Date getOxcartServerStatusChangeTime() {
		return oxcartServerStatusChangeTime;
	}

	public void setOxcartServerStatusChangeTime(Date oxcartServerStatusChangeTime) {
		this.oxcartServerStatusChangeTime = oxcartServerStatusChangeTime;
	}

	public String getTincanServerStatus() {
		return tincanServerStatus;
	}

	public void setTincanServerStatus(String tincanServerStatus) {
		this.tincanServerStatus = tincanServerStatus;
	}

	public Date getTincanServerStatusChangeTime() {
		return tincanServerStatusChangeTime;
	}

	public void setTincanServerStatusChangeTime(Date tincanServerStatusChangeTime) {
		this.tincanServerStatusChangeTime = tincanServerStatusChangeTime;
	}

	public String getAdminServerStatus() {
		return adminServerStatus;
	}

	public void setAdminServerStatus(String adminServerStatus) {
		this.adminServerStatus = adminServerStatus;
	}

	public Date getAdminServerStatusChangeTime() {
		return adminServerStatusChangeTime;
	}

	public void setAdminServerStatusChangeTime(Date adminServerStatusChangeTime) {
		this.adminServerStatusChangeTime = adminServerStatusChangeTime;
	}

	public String getKfoReportApiStatus() {
		return kfoReportApiStatus;
	}

	public void setKfoReportApiStatus(String kfoReportApiStatus) {
		this.kfoReportApiStatus = kfoReportApiStatus;
	}

	public Date getKfoReportApiStatusChangeTime() {
		return kfoReportApiStatusChangeTime;
	}

	public void setKfoReportApiStatusChangeTime(Date kfoReportApiStatusChangeTime) {
		this.kfoReportApiStatusChangeTime = kfoReportApiStatusChangeTime;
	}

	public Map<Integer, Timestamp> getRequestQueueMap() {
		return requestQueueMap;
	}

	public void setRequestQueueMap(Map<Integer, Timestamp> requestQueueMap) {
		this.requestQueueMap = requestQueueMap;
	}

	public List<CourseVersion> getCourseVersions() {
		return courseVersions;
	}

	public void setCourseVersions(List<CourseVersion> courseVersions) {
		this.courseVersions = courseVersions;
	}

}
