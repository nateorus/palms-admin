package com.pdinh.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.CachingSecurityManager;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.action.EntityServiceLocal;
import com.pdinh.auth.data.action.ExternalSubjectServiceLocal;
import com.pdinh.auth.data.action.PermissionServiceLocal;
import com.pdinh.auth.data.dao.AuthorizedEntityDao;
import com.pdinh.auth.data.dao.PalmsRealmDao;
import com.pdinh.auth.data.dao.PalmsSubjectDao;
import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.data.obj.GlobalRolesBean;
import com.pdinh.auth.data.singleton.ExternalSubjectCache;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.EntityRule;
import com.pdinh.auth.persistence.entity.EntityType;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;
@Stateless
@LocalBean
public class AuthorizationServiceLocalImpl implements AuthorizationServiceLocal {
	private static final Logger log = LoggerFactory.getLogger(AuthorizationServiceLocalImpl.class);
	//@Resource(mappedName="application/config_properties")
	//private Properties properties;
	@EJB
	private ExternalSubjectServiceLocal externalSubjectService;
	@EJB
	private EntityServiceLocal entityService;
	@EJB
	private PermissionServiceLocal permissionService;
	
	@EJB
	private GlobalRolesBean rolesBean;
	@EJB
	private UserDao userDao;
	@EJB
	private ProjectDao projectDao;
	@EJB
	private CompanyDao companyDao;
	@EJB
	private ProjectUserDao projectUserDao;
	@EJB
	private PalmsRealmDao realmDao;
	@EJB
	private PalmsSubjectDao subjectDao;
	
	@EJB
	private AuthorizedEntityDao authorizedEntityDao;
	
	@EJB
	private ExternalSubjectCache subjectCache;
	
	@Override
	public RoleContext grantProjectAccess(PalmsSubject subject, Project project,
			String accessGrantor, String roleName, String entityRule) {
		log.debug("About to get default role context");
		RoleContext context = externalSubjectService.getDefaultRoleContext(subject.getPrincipal(), subject.getRealm().getRealm_id(), subject.getEntryDN(), subject.getExt_id());
		log.debug("Returned from getting default role context");
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntity_id(project.getProjectId());
		entity.setDescription(project.getName());
		entity.setEntityRule(rolesBean.getEntityRule(entityRule));
		entity.setEntityType(rolesBean.getEntityType(EntityType.PROJECT_TYPE));
		entity.setRoleContext(context);
		List<Role> roles = new ArrayList<Role>();
		if (!(roleName == null || roleName.isEmpty())){
			Role role = rolesBean.getRole(roleName);
			
			roles.add(role);
			entity.setRoles(roles);
		}
		log.debug("Looking for parent entity");
		//AuthorizedEntity parent = entityService.getAuthorizedEntity(context, EntityType.COMPANY_TYPE, project.getCompany().getCompanyId());
		AuthorizedEntity parent = authorizedEntityDao.findByContextTypeAndId(context.getRole_context_id(), rolesBean.getEntityType(EntityType.COMPANY_TYPE).getEntity_type_id(), project.getCompany().getCompanyId());
		log.debug("Done looking for parent entity");
		if (!(parent== null)){
			entity.setParentEntity(parent);
		}else{
			parent = new AuthorizedEntity();
			parent.setEntity_id(project.getCompany().getCompanyId());
			parent.setDescription(project.getCompany().getCoName());
			parent.setEntityRule(rolesBean.getEntityRule(EntityRule.ALLOW));
			parent.setEntityType(rolesBean.getEntityType(EntityType.COMPANY_TYPE));
			parent.setRoleContext(context);
			parent.setRoles(roles);
			parent.setSubject_id(subject.getSubject_id());
			parent.setTimeBound(false);
			entity.setParentEntity(parent);
		}
		
				
		entity.setSubject_id(subject.getSubject_id());
		entity.setTimeBound(false);
		context = entityService.grantEntityAccess(context.getRole_context_id(), entity, accessGrantor);
		// Granting access to yourself -- need to clear the cache so you can see the new entity
		if (accessGrantor.equalsIgnoreCase(subject.getPrincipal())){
			log.debug("Grantor is equal to subject.  Clearing authorization cache");
			removeAuthorizationCache(accessGrantor, subject.getRealm().getRealm_name());
		}
		return context;
	}
	
	private void removeAuthorizationCache(String principal, String realmName){
		// Remove Authorization info from cache for current user so they don't have to re-login
		CachingSecurityManager sman =(CachingSecurityManager) SecurityUtils.getSecurityManager();
		EhCacheManager cacheManager = (EhCacheManager) sman.getCacheManager();
		Set<Object> keys = cacheManager.getCache(realmName + ".authorizationCache").keys();
		for (Object o : keys){
			if (o.toString().equalsIgnoreCase(principal)){
				cacheManager.getCache(realmName + ".authorizationCache").remove(o);
				log.debug("Removed Authorization cache Object from cache {}, for principal {}", realmName + ".authorizationCache", principal);
			}
		}
		
	}


	@Override
	public List<User> getAllowedUsersInCompany(RoleContext context,
			Integer companyId) {
		List<Integer> allowRules = new ArrayList<Integer>();//a list of rules to filter entities by
		
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW_CHILDREN).getEntity_rule_id());//AllowChildren
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW_ALL).getEntity_rule_id());//AllowAll
    	//Get Projects with allow_children rule
    	List<Integer> projectIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.PROJECT_TYPE, allowRules, true);
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW).getEntity_rule_id());
    	//Get users with Allow rule
    	List<Integer> projectUserIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.PROJECT_USER_TYPE, allowRules, true);
    	List<Integer> userIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.USER_TYPE, allowRules, true);
    	return userDao.findAuthorizedUsersInCompany(companyId, projectIds, projectUserIds, userIds);
		
	}
	@Override
	public List<ExternalSubjectDataObj> getAssignedUsersForProjectUser(int projectId, int usersId, List<String> realmNames){
		//List<String> realmNames = Arrays.asList(properties.getProperty("palmsRealmNames").split(","));
		List<PalmsRealm> realms = realmDao.findRealmsInListOfNames(realmNames);
		ProjectUser pu = projectUserDao.findByProjectIdAndUserId(projectId, usersId);
		List<ExternalSubjectDataObj> subjects = new ArrayList<ExternalSubjectDataObj>();
		for (PalmsRealm realm : realms){
			subjects.addAll(externalSubjectService.getSubjectsWithEntity(realm, pu.getRowId(), EntityType.PROJECT_USER_TYPE));
		}
		return subjects;
		
	}

	@Override
	public RoleContext grantProjectUserAccess(PalmsSubject subject,
			Project project, User user, String accessGrantor, String roleName,
			String entityRule) {
		RoleContext context = externalSubjectService.getDefaultRoleContext(subject.getPrincipal(), subject.getRealm().getRealm_id(), subject.getEntryDN(), subject.getExt_id());
		ProjectUser projectUser = projectUserDao.findByProjectIdAndUserId(project.getProjectId(), user.getUsersId());
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntity_id(projectUser.getRowId());
		entity.setDescription("Project: " + project.getName() + ", User: " + user.getUsername());
		entity.setEntityRule(rolesBean.getEntityRule(entityRule));
		entity.setEntityType(rolesBean.getEntityType(EntityType.PROJECT_USER_TYPE));
		entity.setRoleContext(context);
		List<Role> roles = new ArrayList<Role>();
		if (!(roleName == null || roleName.isEmpty())){
			Role role = rolesBean.getRole(roleName);
			
			roles.add(role);
			entity.setRoles(roles);
		}
		//AuthorizedEntity parent = entityService.getAuthorizedEntity(context, EntityType.PROJECT_TYPE, project.getProjectId());
		AuthorizedEntity parent = authorizedEntityDao.findByContextTypeAndId(context.getRole_context_id(), rolesBean.getEntityType(EntityType.PROJECT_TYPE).getEntity_type_id(), project.getProjectId());
		if (!(parent== null)){
			entity.setParentEntity(parent);
		}else{
			grantProjectAccess(subject, project, accessGrantor, roleName, EntityRule.ALLOW);  //Parent entity gets allow rule
			parent = authorizedEntityDao.findByContextTypeAndId(context.getRole_context_id(), rolesBean.getEntityType(EntityType.PROJECT_TYPE).getEntity_type_id(), project.getProjectId());
			/*parent = new AuthorizedEntity();
			parent.setEntity_id(project.getCompany().getCompanyId());
			parent.setDescription(project.getName());
			parent.setEntityRule(rolesBean.getEntityRule(EntityRule.ALLOW));
			parent.setEntityType(rolesBean.getEntityType(EntityType.PROJECT_TYPE));
			parent.setRoleContext(context);
			parent.setRoles(roles);
			parent.setSubject_id(subject.getSubject_id());
			parent.setTimeBound(false);*/
			entity.setParentEntity(parent);
		}
		
		entity.setSubject_id(subject.getSubject_id());
		entity.setTimeBound(false);
		context = entityService.grantEntityAccess(context.getRole_context_id(), entity, accessGrantor);
		// Granting access to yourself -- need to clear the cache so you can see the new entity
		if (accessGrantor.equalsIgnoreCase(subject.getPrincipal())){
			log.debug("Grantor is equal to subject.  Clearing authorization cache");
			removeAuthorizationCache(accessGrantor, subject.getRealm().getRealm_name());
		}
		return context;
		
	}

	@Override
	public RoleContext grantUserAccess(PalmsSubject subject, Company company,
			User user, String accessGrantor, String roleName, String entityRule) {
		RoleContext context = externalSubjectService.getDefaultRoleContext(subject.getPrincipal(), subject.getRealm().getRealm_id(), subject.getEntryDN(), subject.getExt_id());
		
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntity_id(user.getUsersId());
		entity.setDescription("User: " + user.getUsername());
		entity.setEntityRule(rolesBean.getEntityRule(entityRule));
		entity.setEntityType(rolesBean.getEntityType(EntityType.USER_TYPE));
		entity.setRoleContext(context);
		List<Role> roles = new ArrayList<Role>();
		if (!(roleName == null || roleName.isEmpty())){
			Role role = rolesBean.getRole(roleName);
			
			roles.add(role);
			entity.setRoles(roles);
		}
		AuthorizedEntity parent = entityService.getAuthorizedEntity(context, EntityType.COMPANY_TYPE, company.getCompanyId());
		if (!(parent== null)){
			entity.setParentEntity(parent);
		}else{
			parent = new AuthorizedEntity();
			parent.setEntity_id(company.getCompanyId());
			parent.setDescription(company.getCoName());
			parent.setEntityRule(rolesBean.getEntityRule(EntityRule.ALLOW));
			parent.setEntityType(rolesBean.getEntityType(EntityType.COMPANY_TYPE));
			parent.setRoleContext(context);
			parent.setRoles(roles);
			parent.setSubject_id(subject.getSubject_id());
			parent.setTimeBound(false);
			entity.setParentEntity(parent);
		}
		
		entity.setSubject_id(subject.getSubject_id());
		entity.setTimeBound(false);
		context = entityService.grantEntityAccess(context.getRole_context_id(), entity, accessGrantor);
		// Granting access to yourself -- need to clear the cache so you can see the new entity
		if (accessGrantor.equalsIgnoreCase(subject.getPrincipal())){
			log.debug("Grantor is equal to subject.  Clearing authorization cache");
			removeAuthorizationCache(accessGrantor, subject.getRealm().getRealm_name());
		}
		return context;
	}

	@Override
	public RoleContext grantCompanyAccess(String username, String realmName,
			Company company, String accessGrantor, String roleName, String entityRule) {
		
		PalmsSubject subject = subjectDao.findSubjectByPrincipalAndRealmName(username, realmName);
		RoleContext context = entityService.getDefaultContext(subject);
		
		
		AuthorizedEntity entity = new AuthorizedEntity();
		entity.setEntity_id(company.getCompanyId());
		entity.setDescription("Company: " + company.getCoName());
		entity.setEntityRule(rolesBean.getEntityRule(entityRule));
		entity.setEntityType(rolesBean.getEntityType(EntityType.COMPANY_TYPE));
		entity.setRoleContext(context);
		List<Role> roles = new ArrayList<Role>();
		if (!(roleName == null || roleName.isEmpty())){
			Role role = rolesBean.getRole(roleName);
			
			roles.add(role);
			if (entity.getRoles() == null){
				entity.setRoles(roles);
			}else{
				entity.getRoles().add(role);
			}
		}
		
		entity.setSubject_id(subject.getSubject_id());
		entity.setTimeBound(false);
		context = entityService.grantEntityAccess(context.getRole_context_id(), entity, accessGrantor);
		// Granting access to yourself -- need to clear the cache so you can see the new entity
		if (accessGrantor.equalsIgnoreCase(subject.getPrincipal())){
			log.debug("Grantor is equal to subject.  Clearing authorization cache");
			removeAuthorizationCache(accessGrantor, subject.getRealm().getRealm_name());
		}
		return context;
	}

	@Override
	public List<User> getAllowedUsersInProject(RoleContext context,
			Integer companyId, Integer projectId) {
		// TODO: Implement me	
		/*
List<Integer> allowRules = new ArrayList<Integer>();//a list of rules to filter entities by
		
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW_CHILDREN).getEntity_rule_id());//AllowChildren
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW_ALL).getEntity_rule_id());//AllowAll
    	//Get Projects with allow_children rule
    	List<Integer> projectIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.PROJECT_TYPE, allowRules, true);
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW).getEntity_rule_id());
    	//Get users with Allow rule
    	List<Integer> projectUserIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.PROJECT_USER_TYPE, allowRules, true);
    	
    	List<Integer> userIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.USER_TYPE, allowRules, true);
    	if (entityService.isAuthorized(context, EntityType.PROJECT_TYPE, projectId, false)){
    		projectIds.add(projectId)
    	}
    	return userDao.findAuthorizedUsersInCompany(companyId, projectIds, projectUserIds, userIds);
		
		*/
		return null;
	}
	
	@Override
	public List<User> getAllowedUsersNotInProject(RoleContext context,
			Integer companyId, Integer projectId) {
List<Integer> allowRules = new ArrayList<Integer>();//a list of rules to filter entities by
		
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW_CHILDREN).getEntity_rule_id());//AllowChildren
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW_ALL).getEntity_rule_id());//AllowAll
    	//Get Projects with allow_children rule
    	List<Integer> projectIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.PROJECT_TYPE, allowRules, true);
    	allowRules.add(rolesBean.getEntityRule(EntityRule.ALLOW).getEntity_rule_id());
    	//Get users with Allow rule
    	List<Integer> projectUserIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.PROJECT_USER_TYPE, allowRules, true);
    	
    	List<Integer> userIds = entityService.getEntityIds(context.getRole_context_id(), EntityType.USER_TYPE, allowRules, true);
    	
    	for (Integer i : projectIds){
    		if (i.intValue() == projectId.intValue()){
    			projectIds.remove(i);
    			log.debug("Removed Project id {} from list", i);
    			break;
    		}else{
    			log.trace("int Values are not equal: {} vs {}", i, projectId);
    		}
    	}
    	return userDao.findAuthorizedUsersInCompany(companyId, projectIds, projectUserIds, userIds);
		
		
	}
	/**
	 * Buisiness logic for retrieving authorized companies goes here
	 */

	@Override
	public List<Company> getAllowedCompanyList(RoleContext context) {
		Subject subject = SecurityUtils.getSubject();
		log.debug("flush entities");
		companyDao.evictAll();
		List<Company> companyList = new ArrayList<Company>();
		if (subject.isPermitted("ViewAllCompanies") || subject.isPermitted(EntityType.COMPANY_TYPE + ":*:0")) {
			companyList = companyDao.findAllActiveAssessment();
		} else {
			List<Integer> authorizedCoIds = entityService.getAuthorizedEntities(context,
					EntityType.COMPANY_TYPE, true);
			companyList = companyDao.findAllCompaniesFiltered(authorizedCoIds);
		}
		return companyList;
	}
	/**
	 * Buisiness logic for retrieving authorized projects in a company goes here
	 */

	@Override
	public List<Project> getAllowedProjectList(RoleContext context,
			int companyId) {
		Subject subject = SecurityUtils.getSubject();
		if (subject.isPermitted(EntityType.COMPANY_TYPE + ":AllowChildren:" + companyId)  ||
				subject.isPermitted("ViewAllProjects")){
			return projectDao.findAllByCompanyId(companyId);
			
		}else{
			List<Integer> authorizedProjIds = entityService.getAuthorizedEntities(context, EntityType.PROJECT_TYPE, true);
			return projectDao.findAuthorizedProjectsInCompany(authorizedProjIds, companyId);
		}
		
	}

	@Override
	public List<AuthorizedEntity> getEntities(List<Integer> entityIds,
			String entityType) {
		EntityType type = rolesBean.getEntityType(entityType);
		return authorizedEntityDao.findAllByEntityIdsTypeAndRealm(entityIds, type.getEntity_type_id());
		
	}

	@Override
	public ExternalSubjectDataObj getSubjectMetadata(int subjectId) {
		PalmsSubject subject = subjectDao.findById(subjectId);
		return getUserData(subject);
	}
	
	private ExternalSubjectDataObj getUserData(PalmsSubject subject) {
		if (subject == null) {
			return null;
		} else {
			PalmsRealm realm = subject.getRealm();

			ExternalSubjectDataObj user;
			if (realm.getRealmType().getRealm_type_name().equals("JDBC")) {
				user = subjectCache.get(subject.getPrincipal(), subject.getExt_id(), realm.getRealm_id());
				user.setSubject_id(subject.getSubject_id());
				return user;

			} else if (realm.getRealmType().getRealm_type_name().equalsIgnoreCase("LDAP")) {
				// Not supported
				return null;

			} else {// Active directory
				user = subjectCache.get(subject.getPrincipal(), realm.getRealm_id());
				user.setSubject_id(subject.getSubject_id());
				return user;

			}

		}
	}

}
