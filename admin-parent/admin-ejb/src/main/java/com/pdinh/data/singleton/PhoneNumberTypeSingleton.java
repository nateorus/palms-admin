package com.pdinh.data.singleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.pdinh.data.ms.dao.PhoneNumberTypeDao;
import com.pdinh.persistence.ms.entity.PhoneNumberType;

@Startup
@Singleton(mappedName = "phoneNumberTypeSingleton")
public class PhoneNumberTypeSingleton {

	@EJB
	private PhoneNumberTypeDao phoneNumberTypeDao;

	private Map<String, PhoneNumberType> phoneNumberTypes = new HashMap<String, PhoneNumberType>();

	@PostConstruct
	public void init() {
		List<PhoneNumberType> types = phoneNumberTypeDao.findAll();
		for (PhoneNumberType type : types) {
			phoneNumberTypes.put(type.getAbbv(), type);
		}
	}

	public PhoneNumberType getTypeByAbbv(String abbv) {
		return phoneNumberTypes.get(abbv);
	}

}
