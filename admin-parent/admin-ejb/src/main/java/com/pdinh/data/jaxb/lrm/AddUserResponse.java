package com.pdinh.data.jaxb.lrm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AddUserResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddUserResponse {

	private List<String> results;

	@XmlAttribute
	private String postResult = "FAILED";

	public List<String> getResults() {
		return results;
	}

	public void setResults(List<String> results) {
		this.results = results;
	}

	public String getPostResult() {
		return postResult;
	}

	public void setPostResult(String postResult) {
		this.postResult = postResult;
	}
}
