package com.pdinh.data;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.ContentToken;

@Local
public interface TokenServiceLocal {
	
	public static final int VERIFY_TOKEN_VALID_MINUTES = 10;
	
	public ContentToken generateToken(int users_id, int company_id, String contentId);
	
	public boolean verifyToken(int tokenId, int verifyToken);
}
