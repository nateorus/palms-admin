package com.pdinh.data.jaxb.reporting;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ReportListJsonSerializer extends JsonSerializer<List<ReportRepresentation>>{

	@Override
	public void serialize(List<ReportRepresentation> reports, JsonGenerator generator,
			SerializerProvider sp) throws IOException,
			JsonProcessingException {
		generator.writeArrayFieldStart("reports");
		for (ReportRepresentation report : reports){
			generator.writeNumberField("requestId", report.getRequestId());
			generator.writeStringField("code", report.getCode());
			generator.writeArrayFieldStart("participants");
				for(ReportParticipantRepresentation rpr : report.getParticipants()){
					generator.writeNumberField("userId", rpr.getUsersId());
				}
			generator.writeEndArray();
		}
		
		generator.writeEndArray();
		
	}

}
