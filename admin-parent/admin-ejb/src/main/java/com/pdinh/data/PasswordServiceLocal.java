package com.pdinh.data;

import java.util.List;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.PasswordStatusResultObj;
import com.pdinh.presentation.domain.ResultObj;

@Local
public interface PasswordServiceLocal {
	
	public ResultObj validatePassword(String password, PasswordPolicy policy);
	
	public PasswordPolicyUserStatus setupUserPolicyStatus(User user);
	
	public void setupCompanyPolicies(Company company);
	
	public PasswordPolicyUserStatus updatePasswordPolicyUserStatus(User user, boolean success);
	
	public PasswordStatusResultObj doLockoutUpdateEval(User user, boolean success);
	
	
	
}
