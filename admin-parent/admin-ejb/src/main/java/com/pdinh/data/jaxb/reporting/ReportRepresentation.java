package com.pdinh.data.jaxb.reporting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.ReportParticipant;
import com.pdinh.persistence.ms.entity.ReportRequest;

@XmlRootElement(name = "report")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportRepresentation implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ReportRepresentation.class);
	
	@XmlAttribute(name="requestId")
	private int requestId;
	
	@XmlAttribute(name="parentRequestId")
	private int parentRequestId;
	
	@XmlAttribute(name="name")
	private String reportName;
	
	@XmlAttribute(name="code")
	private String code;
	
	@XmlAttribute(name="status")
	private String status;
	
	@XmlAttribute(name="statusTime")
	private Date statusTime;
	
	@XmlAttribute(name="s3ObjectKey")
	private String s3ObjectKey;
	
	@XmlAttribute(name="sqsMessageId")
	private String sqsMessageId;
	
	@XmlAttribute(name="reportRequestKey")
	private String reportRequestKey;
	
	@XmlAttribute(name="targetLevel")
	private String targetLevel;
	
	@XmlAttribute(name="reportLang")
	private String reportLang;
	
	@XmlAttribute(name="reportIdNameOption")
	private String reportIdNameOption;
	
	@XmlAttribute(name="title")
	private String title;
	
	@XmlAttribute(name="requestedBy")
	private String requestedBy;
	
	@XmlAttribute(name="subjectId")
	private int subjectId;
	
	@XmlAttribute(name="requestTime")
	private Date requestTime;
	
	@XmlAttribute(name="errorMessage")
	private String errorMessage;
	
	@XmlAttribute(name="errorCode")
	private String errorCode;
	
	@XmlAttribute(name="contentType")
	private String contentType;

	@XmlAttribute(name = "manuallyUploaded")
	private boolean manuallyUploaded;
	
	@XmlAttribute(name = "filesize")
	private long filesize;
	
	@XmlAttribute(name = "processStartTime")
	private Date processStartTime;
	
	@XmlAttribute(name = "createType")
	private int createType;

	@XmlElementWrapper(name="participants")
	@XmlElement(name = "participants")
	private List<ReportParticipantRepresentation> participants;
	
	public ReportRepresentation(){
		
	}
	
	public ReportRepresentation(ReportRequest request){
		
		this.setRequestId(request.getReportRequestId());
		this.setParentRequestId(request.getReportGenerateRequest().getReportGenerateRequestId());
		this.setReportRequestKey(request.getReportRequestKey());
		this.setFilesize(request.getFilesize());
		this.setProcessStartTime(request.getProcessStartTime());
		this.setCreateType(request.getCreateType());
		this.setStatus(request.getReportRequestStatus().getCode());
		this.setCode(request.getReportType().getCode());
		this.setReportIdNameOption(request.getReportIdNameOption());
		this.setReportLang(request.getLanguage().getCode());
		this.setReportName(request.getReportType().getName());
		this.setStatusTime(request.getStatusUpdateTs());
		this.setErrorMessage(request.getErrorMessage());
		this.setErrorCode(request.getErrorCode());
		if (request.getContentType() == null || request.getContentType().isEmpty()){
			this.setContentType(request.getReportType().getContentType());
		}else{
			this.setContentType(request.getContentType());
		}
		this.setTitle(request.getReportTitle());
		if (!(request.getTargetLevelType() == null)){
			this.setTargetLevel(request.getTargetLevelType().getCode());
		}
		this.setRequestedBy(request.getReportGenerateRequest().getPrincipal());
		this.setSubjectId(request.getReportGenerateRequest().getSubjectId());
		this.setRequestTime(request.getReportGenerateRequest().getRequestTime());
		this.setParticipants(new ArrayList<ReportParticipantRepresentation>());
		log.trace("Setting up participants for report request {}", request.getReportRequestId());
		for (ReportParticipant ppt : request.getReportParticipants()){
			ReportParticipantRepresentation rptPptRep = new ReportParticipantRepresentation();
			rptPptRep.setProjectId(ppt.getProject().getProjectId());
			rptPptRep.setProjectName(ppt.getProject().getName());
			if (!(ppt.getLanguage() == null)){
				rptPptRep.setReportLang(ppt.getLanguage().getCode());
			}
			if (!(ppt.getCourseVersionMap() == null || ppt.getCourseVersionMap().isEmpty())){
				rptPptRep.setCourseVersionMap(ppt.getCourseVersionMap());
			}
			rptPptRep.setUsersId(ppt.getUser().getUsersId());
			rptPptRep.setFirstName(ppt.getUser().getFirstname());
			rptPptRep.setLastName(ppt.getUser().getLastname());
			rptPptRep.setEmail(ppt.getUser().getEmail());
			this.getParticipants().add(rptPptRep);
		}
		log.trace("Finished setting up participants");
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getReportLang() {
		return reportLang;
	}

	public void setReportLang(String reportLang) {
		this.reportLang = reportLang;
	}

	public String getReportIdNameOption() {
		return reportIdNameOption;
	}

	public void setReportIdNameOption(String reportIdNameOption) {
		this.reportIdNameOption = reportIdNameOption;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ReportParticipantRepresentation> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ReportParticipantRepresentation> participants) {
		this.participants = participants;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getS3ObjectKey() {
		return s3ObjectKey;
	}

	public void setS3ObjectKey(String s3ObjectKey) {
		this.s3ObjectKey = s3ObjectKey;
	}

	public String getSqsMessageId() {
		return sqsMessageId;
	}

	public void setSqsMessageId(String sqsMessageId) {
		this.sqsMessageId = sqsMessageId;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public Date getStatusTime() {
		return statusTime;
	}

	public void setStatusTime(Date statusTime) {
		this.statusTime = statusTime;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getReportRequestKey() {
		return reportRequestKey;
	}

	public void setReportRequestKey(String reportRequestKey) {
		this.reportRequestKey = reportRequestKey;
	}

	public int getParentRequestId() {
		return parentRequestId;
	}

	public void setParentRequestId(int parentRequestId) {
		this.parentRequestId = parentRequestId;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isManuallyUploaded() {
		return manuallyUploaded;
	}

	public void setManuallyUploaded(boolean manuallyUploaded) {
		this.manuallyUploaded = manuallyUploaded;
	}

	public long getFilesize() {
		return filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public Date getProcessStartTime() {
		return processStartTime;
	}

	public void setProcessStartTime(Date processStartTime) {
		this.processStartTime = processStartTime;
	}

	public int getCreateType() {
		return createType;
	}

	public void setCreateType(int createType) {
		this.createType = createType;
	}

}
