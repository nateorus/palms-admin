package com.pdinh.data;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.kf.dochandler.excel.ExcelRow;
import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.vo.CustomFieldVo;
import com.pdinh.persistence.dto.BulkUploadUser;
import com.pdinh.persistence.ms.entity.Language;

/**
 * The purpose of this class is to facilitate comparison between a user which
 * already exists in the PALMS system and a row of user data loaded from an
 * import file.
 * 
 * @author dbailey
 * 
 */
public class ImportUser {

	public static final String FIRST_NAME = "First Name";
	public static final String LAST_NAME = "Last Name";
	public static final String EMAIL_ADDRESS = "Email Address";
	public static final String BUSINESS_PHONE = "Business Phone";
	public static final String SUPERVISOR_NAME = "Supervisor Name";
	public static final String SUPERVISOR_EMAIL = "Supervisor Email";
	public static final String LANGUAGE = "Language";
	public static final String OPTIONAL_1 = "Optional 1";
	public static final String OPTIONAL_2 = "Optional 2";
	public static final String OPTIONAL_3 = "Optional 3";
	public static final String LOGON_ID = "Logon ID";
	public static final String PASSWORD = "Password";
	public static final String UPDATE_LOGON_FIELDS = "Update Logon Fields";
	private static final Set<String> CORE_FIELD_KEYS = new HashSet<String>();

	private int userId = -1;
	private int projectUserId = -1;
	private int excelRowId = -1;
	private boolean testData;
	private String testDataDetail;
	private boolean canImport = true;

	private final Map<String, String> coreFields = new HashMap<String, String>();
	private final Map<String, String> stringValuedCustomFields = new HashMap<String, String>();
	private final Map<String, Long> integerValuedCustomFields = new HashMap<String, Long>();
	private final Map<String, Date> dateValuedCustomFields = new HashMap<String, Date>();
	private final Map<String, String> listValuedCustomFields = new HashMap<String, String>();

	private Set<String> coreFieldMismatches;
	private Set<String> customFieldMismatches;
	private final Set<String> newCustomFields = new HashSet<String>();

	static {
		CORE_FIELD_KEYS.add(FIRST_NAME);
		CORE_FIELD_KEYS.add(LAST_NAME);
		CORE_FIELD_KEYS.add(EMAIL_ADDRESS);
		CORE_FIELD_KEYS.add(BUSINESS_PHONE);
		CORE_FIELD_KEYS.add(SUPERVISOR_NAME);
		CORE_FIELD_KEYS.add(SUPERVISOR_EMAIL);
		CORE_FIELD_KEYS.add(LANGUAGE);
		CORE_FIELD_KEYS.add(OPTIONAL_1);
		CORE_FIELD_KEYS.add(OPTIONAL_2);
		CORE_FIELD_KEYS.add(OPTIONAL_3);
		CORE_FIELD_KEYS.add(LOGON_ID);
		CORE_FIELD_KEYS.add(PASSWORD);
		CORE_FIELD_KEYS.add(UPDATE_LOGON_FIELDS);
	}

	public ImportUser() {
		// default constructor
	}

	public String getFirstName() {
		return coreFields.get(FIRST_NAME);
	}

	public String getLastName() {
		return coreFields.get(LAST_NAME);
	}

	public String getEmailAddress() {
		return coreFields.get(EMAIL_ADDRESS);
	}

	public void setEmailAddress(String emailAddress) {
		coreFields.put(EMAIL_ADDRESS, emailAddress);
	}

	public String getBusinessPhone() {
		return coreFields.get(BUSINESS_PHONE);
	}

	public String getSupervisorName() {
		return coreFields.get(SUPERVISOR_NAME);
	}

	public String getSupervisorEmail() {
		return coreFields.get(SUPERVISOR_EMAIL);
	}

	public String getLanguageCode() {
		return coreFields.get(LANGUAGE);
	}

	public String getOptional1() {
		return coreFields.get(OPTIONAL_1);
	}

	public String getOptional2() {
		return coreFields.get(OPTIONAL_2);
	}

	public String getOptional3() {
		return coreFields.get(OPTIONAL_3);
	}

	public String getLogonID() {
		return coreFields.get(LOGON_ID);
	}

	public String getPassword() {
		return coreFields.get(PASSWORD);
	}

	public String getUploadLogonFields() {
		return coreFields.get(UPDATE_LOGON_FIELDS);
	}

	public void setUploadLogonFields(String value) {
		coreFields.put(UPDATE_LOGON_FIELDS, value);
	}

	public int getProjectUserId() {
		return projectUserId;
	}

	public void setProjectUserId(int projectUserId) {
		this.projectUserId = projectUserId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getExcelRowId() {
		return excelRowId;
	}

	public void setExcelRowId(int rowId) {
		this.excelRowId = rowId;
	}

	public String getCoreFieldValue(String key) {
		return coreFields.get(key);
	}

	public String getCustomFieldStringValue(String code) {
		return stringValuedCustomFields.get(code);
	}

	public Long getCustomFieldIntValue(String code) {
		return integerValuedCustomFields.get(code);
	}

	public Date getCustomFieldDateValue(String code) {
		return dateValuedCustomFields.get(code);
	}

	public String getCustomFieldListValue(String code) {
		return listValuedCustomFields.get(code);
	}

	public Object getCustomFieldValue(String code, CustomFieldDto dto) {
		if (dto.isCharValue()) {
			return stringValuedCustomFields.get(code);

		} else if (dto.isIntValue()) {
			return integerValuedCustomFields.get(code);

		} else if (dto.isDateValue()) {
			return dateValuedCustomFields.get(code);

		} else if (dto.isListValue()) {
			return listValuedCustomFields.get(code);
		}

		return null;
	}

	/**
	 * Set the values for this <code>ImportUser</code>, based on the contents of
	 * an <code>ExcelRow</code>.
	 * 
	 * @param row
	 */
	public void initialize(ExcelRow row, Map<String, Language> languageMap, Map<String, CustomFieldDto> customFieldMap) {
		if (row.hasSevereErrors()) {
			canImport = false;
		}

		excelRowId = row.getOrdinal();
		for (String key : CORE_FIELD_KEYS) {
			if (key.equalsIgnoreCase(LANGUAGE)) {
				Language language = languageMap.get(row.getStringValue(LANGUAGE));
				if (language != null) {
					coreFields.put(key, languageMap.get(row.getStringValue(LANGUAGE)).getCode());
				}
			} else {
				coreFields.put(key, row.getStringValue(key));
			}
		}

		for (Map.Entry<String, CustomFieldDto> entry : customFieldMap.entrySet()) {
			String code = entry.getKey();
			if (entry.getValue().isCharValue()) {
				stringValuedCustomFields.put(code, row.getStringValue(code));

			} else if (entry.getValue().isIntValue()) {
				integerValuedCustomFields.put(code, row.getIntValue(code));

			} else if (entry.getValue().isDateValue()) {
				dateValuedCustomFields.put(code, row.getDateValue(code));

			} else if (entry.getValue().isListValue()) {
				listValuedCustomFields.put(code, row.getStringValue(code));
			}
		}
	}

	public void initialize(BulkUploadUser user) {
		userId = user.getUserId();
		coreFields.put(FIRST_NAME, user.getFirstName());
		coreFields.put(LAST_NAME, user.getLastName());
		coreFields.put(EMAIL_ADDRESS, user.getEmailAddress());
		coreFields.put(BUSINESS_PHONE, user.getBusinessPhone());
		coreFields.put(SUPERVISOR_NAME, user.getSupervisorName());
		coreFields.put(SUPERVISOR_EMAIL, user.getSupervisorEmail());
		coreFields.put(LANGUAGE, user.getLanguageCode());
		coreFields.put(OPTIONAL_1, user.getOptional1());
		coreFields.put(OPTIONAL_2, user.getOptional2());
		coreFields.put(OPTIONAL_3, user.getOptional3());
		coreFields.put(LOGON_ID, user.getLogonId());
		coreFields.put(PASSWORD, user.getPassword());
		coreFields.put(UPDATE_LOGON_FIELDS, "No");
	}

	public void addCustomField(CustomFieldVo customField) {
		if (customField.isCharValue()) {
			stringValuedCustomFields.put(customField.getCode(), customField.getValueAsStr());

		} else if (customField.isIntValue()) {
			integerValuedCustomFields.put(customField.getCode(), customField.getValueAsInt());

		} else if (customField.isDateValue()) {
			dateValuedCustomFields.put(customField.getCode(), (Date) customField.getValue());

		} else if (customField.isListValue()) {
			listValuedCustomFields.put(customField.getCode(), customField.getSummaryValue());
		}

		if (!customField.isValueInDatabase()) {
			newCustomFields.add(customField.getCode());
		}
	}

	/**
	 * Detect which core fields in this object differ from the corresponding
	 * fields in the passed 'other' object
	 * 
	 * @param other
	 *            An <code>ImportUser</code> to compare to this one
	 * @param nonNullOnly
	 *            If <strong>true</strong>, only considers fields which are
	 *            non-<strong>NULL</strong> in this object
	 * @return A <code>Set</code> of string identifiers for the core fields
	 *         where this <code>ImportUser</code> differs from the one which is
	 *         passed.
	 */
	public Set<String> getCoreFieldMismatches(ImportUser other, boolean nonNullOnly) {
		coreFieldMismatches = new HashSet<String>();

		for (Map.Entry<String, String> entry : coreFields.entrySet()) {
			if (entry.getKey().equals(EMAIL_ADDRESS)) {
				// We don't update email address.
				continue;
			}
			if ((!nonNullOnly || entry.getValue() != null)
					&& !StringUtils.equals(entry.getValue(), other.getCoreFieldValue(entry.getKey()))) {
				coreFieldMismatches.add(entry.getKey());
			}
		}

		return coreFieldMismatches;
	}

	/**
	 * Detect which custom fields in this object differ from the corresponding
	 * fields in the passed 'other' object
	 * 
	 * @param other
	 *            An <code>ImportUser</code> to compare to this one
	 * @param nonNullOnly
	 *            If <strong>true</strong>, only considers fields which are
	 *            non-<strong>NULL</strong> in this object
	 * @return A <code>Set</code> of string identifiers for the custom fields
	 *         where this <code>ImportUser</code> differs from the one which is
	 *         passed.
	 */
	public Set<String> getCustomFieldMismatches(ImportUser other, boolean nonNullOnly) {
		customFieldMismatches = new HashSet<String>();

		for (Map.Entry<String, String> entry : stringValuedCustomFields.entrySet()) {
			if ((!nonNullOnly || entry.getValue() != null)
					&& !StringUtils.equals(entry.getValue(), other.getCustomFieldStringValue(entry.getKey()))) {
				customFieldMismatches.add(entry.getKey());
			}
		}

		for (Map.Entry<String, Long> entry : integerValuedCustomFields.entrySet()) {
			if ((!nonNullOnly || entry.getValue() != null)
					&& !ObjectUtils.equals(entry.getValue(), other.getCustomFieldIntValue(entry.getKey()))) {
				customFieldMismatches.add(entry.getKey());
			}
		}

		for (Map.Entry<String, Date> entry : dateValuedCustomFields.entrySet()) {
			if ((!nonNullOnly || entry.getValue() != null)
					&& !ObjectUtils.equals(entry.getValue(), other.getCustomFieldDateValue(entry.getKey()))) {
				customFieldMismatches.add(entry.getKey());
			}
		}

		for (Map.Entry<String, String> entry : listValuedCustomFields.entrySet()) {
			if ((!nonNullOnly || entry.getValue() != null)
					&& !StringUtils.equals(entry.getValue(), other.getCustomFieldListValue(entry.getKey()))) {
				customFieldMismatches.add(entry.getKey());
			}
		}

		return customFieldMismatches;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User ID: " + userId + ", ");
		builder.append(FIRST_NAME + ": " + coreFields.get(FIRST_NAME) + ", ");
		builder.append(LAST_NAME + ": " + coreFields.get(LAST_NAME) + ", ");
		builder.append(EMAIL_ADDRESS + ": " + coreFields.get(EMAIL_ADDRESS) + ", ");
		builder.append(BUSINESS_PHONE + ": " + coreFields.get(BUSINESS_PHONE) + ", ");
		builder.append(SUPERVISOR_NAME + ": " + coreFields.get(SUPERVISOR_NAME) + ", ");
		builder.append(SUPERVISOR_EMAIL + ": " + coreFields.get(SUPERVISOR_EMAIL) + ", ");
		builder.append(LANGUAGE + ": " + coreFields.get(LANGUAGE) + ", ");
		builder.append(OPTIONAL_1 + ": " + coreFields.get(OPTIONAL_1) + ", ");
		builder.append(OPTIONAL_2 + ": " + coreFields.get(OPTIONAL_2) + ", ");
		builder.append(OPTIONAL_3 + ": " + coreFields.get(OPTIONAL_3) + ", ");
		builder.append(LOGON_ID + ": " + coreFields.get(OPTIONAL_3) + ", ");
		builder.append(PASSWORD + ": " + coreFields.get(PASSWORD) + ", ");
		builder.append(UPDATE_LOGON_FIELDS + ": " + coreFields.get(UPDATE_LOGON_FIELDS) + ", ");

		for (Map.Entry<String, String> entry : stringValuedCustomFields.entrySet()) {
			builder.append(entry.getKey() + ": " + entry.getValue() + ", ");
		}

		for (Map.Entry<String, Long> entry : integerValuedCustomFields.entrySet()) {
			builder.append(entry.getKey() + ": " + entry.getValue() + ", ");
		}

		for (Map.Entry<String, Date> entry : dateValuedCustomFields.entrySet()) {
			builder.append(entry.getKey() + ": " + entry.getValue() + ", ");
		}

		for (Map.Entry<String, String> entry : listValuedCustomFields.entrySet()) {
			builder.append(entry.getKey() + ": " + entry.getValue() + ", ");
		}

		return builder.toString();
	}

	public boolean hasTestData() {
		return testData;
	}

	public void setTestData(boolean testData) {
		this.testData = testData;
	}

	public boolean canImport() {
		return canImport;
	}

	public Set<String> getCoreFieldMismatches() {
		return coreFieldMismatches;
	}

	public Set<String> getCustomFieldMismatches() {
		return customFieldMismatches;
	}

	@Override
	public int hashCode() {
		if (StringUtils.isBlank(getEmailAddress())) {
			return 0;
		}

		return getEmailAddress().toLowerCase().hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof ImportUser) || other == null) {
			return false;
		}

		return hashCode() == other.hashCode();
	}

	public String getTestDataDetail() {
		return testDataDetail;
	}

	public void setTestDataDetail(String testDataDetail) {
		this.testDataDetail = testDataDetail;
	}

	public Set<String> getNewCustomFields() {
		return newCustomFields;
	}
}
