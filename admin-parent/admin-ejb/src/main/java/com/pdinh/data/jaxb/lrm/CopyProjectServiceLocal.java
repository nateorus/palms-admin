package com.pdinh.data.jaxb.lrm;

import javax.ejb.Local;

@Local
public interface CopyProjectServiceLocal {

	public CopyLrmProjectResponse copyLrmProject(int subjectId, int oldProjectId, int newProjectId, String name);
	
	public boolean copyAbyDSetup(int oldProjectId, int newProjectId);
}
