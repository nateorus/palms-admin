package com.pdinh.data;

import javax.ejb.Local;

@Local
public interface AsynchronousAuditLoggerLocal {

	public void doAudit(String message, Object[] params);
}
