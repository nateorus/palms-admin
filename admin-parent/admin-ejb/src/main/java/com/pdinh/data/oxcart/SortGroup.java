package com.pdinh.data.oxcart;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sortGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class SortGroup {

	@XmlAttribute(name = "id")
	protected String id;

	@XmlAttribute(name = "displayName")
	protected String displayName;

	@XmlElementWrapper(name = "sortItems")
	@XmlElement(name = "sortItem")
	private List<SortItem> sortItems = new ArrayList<SortItem>();

	public String getId() {
		return id;
	}

	public void setId(String value) {
		this.id = value;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String value) {
		this.displayName = value;
	}

	public List<SortItem> getSorts() {
		if (sortItems == null) {
			sortItems = new ArrayList<SortItem>();
		}
		return this.sortItems;
	}

	public void setSorts(List<SortItem> value) {
		this.sortItems = value;
		return;
	}

}
