package com.pdinh.data.jaxb.lrm;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AddUserRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddUserRequest {

	@XmlAttribute
	private String projectId;

	@XmlAttribute
	private String userId;

	private List<NewProjectUser> newUsers;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<NewProjectUser> getNewUsers() {
		return newUsers;
	}

	public void setNewUsers(List<NewProjectUser> newUsers) {
		this.newUsers = newUsers;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
