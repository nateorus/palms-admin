package com.pdinh.data.instrumentnorms;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "course")
@XmlAccessorType(XmlAccessType.FIELD)
public class Course {
	@XmlAttribute
	private String abbv;
	@XmlElement(name = "normGroup")
	private List<NormGroup> normGroups = new ArrayList<NormGroup>();
	@XmlAttribute
	private String shortName = "";

	/*
	 * setter/getter
	 */
	public String getAbbv() {
		return abbv;
	}

	public void setAbbv(String abbv) {
		this.abbv = abbv;
	}

	public List<NormGroup> getNormGroups() {
		return normGroups;
	}

	public void setNormGroups(List<NormGroup> normGroups) {
		this.normGroups = normGroups;
	}

	@Override
	public String toString() {
		return "Course [abbv=" + abbv + ", normGroups=" + normGroups + "]";
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
