package com.pdinh.data;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.kf.dochandler.excel.DocumentValidation;
import com.kf.dochandler.excel.ExcelRow;
import com.kf.uffda.dto.FieldDto;

@Local
public interface ValidationImportServiceLocal {

	void processFile(int jobId, String jobStatus, String callbackUrl);

	void validateFile(InputStream stream, String extension, List<FieldDto> standardFields, List<FieldDto> customFields,
			Map<String, List<ExcelRow>> rows, DocumentValidation validation);
}
