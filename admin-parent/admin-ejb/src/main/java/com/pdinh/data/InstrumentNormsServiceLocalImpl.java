package com.pdinh.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.instrumentnorms.Course;
import com.pdinh.data.instrumentnorms.CourseNormList;
import com.pdinh.data.instrumentnorms.FetchSelectedNormsList;
import com.pdinh.data.instrumentnorms.NormGroup;
import com.pdinh.data.instrumentnorms.Participant;
import com.pdinh.data.instrumentnorms.ProjectNormsDefaults;
import com.pdinh.data.instrumentnorms.SelectedNorms;
import com.pdinh.data.instrumentnorms.SetParticipantNormsList;
import com.pdinh.data.jaxb.JaxbUtil;
import com.pdinh.reporting.PortalServletServiceLocal;

/**
 * Local Bean implementation class InstrumentNormsServiceLocalImpl
 */
@Stateless
@LocalBean
public class InstrumentNormsServiceLocalImpl implements InstrumentNormsServiceLocal {

	private static final Logger log = LoggerFactory.getLogger(InstrumentNormsServiceLocalImpl.class);
	//
	// Beans and Resources
	//
	@Resource(name = "custom/abyd_properties")
	private Properties abydProperties;
	//
	// Static data
	//
	static private String xmlPrefix = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	@EJB
	PortalServletServiceLocal portalServletServiceLocal;
	public PortalServletServiceLocal getPortalServletServiceLocal() {
		return portalServletServiceLocal;
	}

	public void setPortalServletServiceLocal(
			PortalServletServiceLocal portalServletServiceLocal) {
		this.portalServletServiceLocal = portalServletServiceLocal;
	}

	//
	// Constructors
	//
	/**
	 * Default constructor.
	 */
	public InstrumentNormsServiceLocalImpl() {
		// TODO Auto-generated constructor stub
	}

	//
	// Instance Methods
	//

	/**
	 * getCourseNormList - Get the list of all of the valid norms for the
	 * courses in a project
	 * 
	 * @param project_id
	 *            - The project for which to get the norms
	 * @return A CourseNormList object
	 */
	@Override
	public CourseNormList getCourseNormList(int project_id) {
		String inpXml = xmlPrefix + "<CourseNormsRequest><project id=\"" + project_id + "\"/></CourseNormsRequest>";

		String respXml = portalServletServiceLocal.portalServletRequest(inpXml);

		// We now have the string... Dump it
		if (respXml == null || respXml.length() < 1) {
			respXml = "No XML returned";
			return new CourseNormList();
		}
		log.debug("portal Servlet returned: {}", respXml);
		return (CourseNormList) JaxbUtil.unmarshal(respXml, CourseNormList.class);
	}

	/**
	 * setProjectNormDefaults - set the default norms for a project
	 * 
	 * @param projectNormsDefaults
	 *            - A ProjectNormsDefaults object
	 */
	@Override
	public void setProjectNormDefaults(ProjectNormsDefaults projectNormsDefaults) {
		String inpXml = "";
		projectNormsDefaults.setParticipants(null);
		inpXml += JaxbUtil.marshalToXmlString(projectNormsDefaults, ProjectNormsDefaults.class);

		String respXml = portalServletServiceLocal.portalServletRequest(inpXml);

		// We now have the string... Dump it
		if (respXml == null || respXml.length() < 1)
			respXml = "No XML returned";
		log.debug("portal servlet returned: {}",respXml);

		return;
	}

	/**
	 * Participant p = new Participant();
	 * 
	 * setParticipantNorms - set the norms for one or more participant
	 * 
	 * @param setParticipantNormsList
	 *            - A SetParticipantNormsList object containing the participants
	 *            and data to update
	 */
	@Override
	public void setParticipantNorms(SetParticipantNormsList setParticipantNormsList) {
		String inpXml = "";
		inpXml += JaxbUtil.marshalToXmlString(setParticipantNormsList, SetParticipantNormsList.class);

		String respXml = portalServletServiceLocal.portalServletRequest(inpXml);

		// We now have the string... Dump it
		if (respXml == null || respXml.length() < 1)
			respXml = "No XML returned";
		log.debug("Portal servlet returned: {}", respXml);
	}

	@Override
	public SelectedNorms fetchSelectedNormsList(FetchSelectedNormsList fetchSelectedNormsList) {
		String inpXml = "";
		inpXml += JaxbUtil.marshalToXmlString(fetchSelectedNormsList, FetchSelectedNormsList.class);
		String respXml = portalServletServiceLocal.portalServletRequest(inpXml);

		// We now have the string... Dump it
		if (respXml == null || respXml.length() < 1)
			respXml = "No XML returned";
		log.debug("Portal Servlet returned: {}",respXml);
		SelectedNorms sn = (SelectedNorms) JaxbUtil.unmarshal(respXml, SelectedNorms.class);
		if (sn == null) {
			return sn;
		}
		// set norms to selected as true at project level
		if (sn.getProjectNormsDefaults().getCourses() != null && sn.getProjectNormsDefaults().getCourses().size() > 0) {
			for (Course c : sn.getProjectNormsDefaults().getCourses()) {
				for (NormGroup ng : c.getNormGroups()) {
					ng.setSelected(true);
				}
			}
		}
		// set norms to selected as true at participant override level
		if (sn.getParticipants() != null && sn.getParticipants().size() > 0) {
			for (Participant ppt : sn.getParticipants()) {
				for (Course c : ppt.getCourses()) {
					for (NormGroup ng : c.getNormGroups()) {
						ng.setSelected(true);
					}
				}
			}
		}
		return sn;
	}
}
