package com.pdinh.data.jaxb.reporting;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "participant")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportParticipantRepresentation implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlAttribute(name = "id")
	private int usersId;

	@XmlAttribute(name = "projectId")
	private int projectId;

	@XmlAttribute(name = "projectName")
	private String projectName;

	@XmlAttribute(name = "reportLang")
	private String reportLang;

	@XmlAttribute(name = "firstName")
	private String firstName;

	@XmlAttribute(name = "lastName")
	private String lastName;

	@XmlAttribute(name = "email")
	private String email;
	
	@XmlAttribute(name = "courseVersionMap")
	private String courseVersionMap;

	public ReportParticipantRepresentation() {

	}

	public ReportParticipantRepresentation(com.pdinh.persistence.ms.entity.ReportParticipant participant) {
		this.setUsersId(participant.getUser().getUsersId());
		this.setProjectId(participant.getProject().getProjectId());
		this.setFirstName(participant.getUser().getFirstname());
		this.setLastName(participant.getUser().getLastname());
		this.setProjectName(participant.getProject().getName());
		if (!(participant.getCourseVersionMap() == null || participant.getCourseVersionMap().isEmpty())){
			this.setCourseVersionMap(participant.getCourseVersionMap());
		}
		if (!(participant.getLanguage() == null)) {
			this.setReportLang(participant.getLanguage().getCode());
		}
		this.setEmail(participant.getUser().getEmail());
	}

	public int getUsersId() {
		return usersId;
	}

	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getReportLang() {
		return reportLang;
	}

	public void setReportLang(String reportLang) {
		this.reportLang = reportLang;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCourseVersionMap() {
		return courseVersionMap;
	}

	public void setCourseVersionMap(String courseVersionMap) {
		this.courseVersionMap = courseVersionMap;
	}
}
