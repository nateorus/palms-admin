package com.pdinh.data.jaxb.lrm;

import javax.ejb.Local;

@Local
public interface LrmObjectServiceLocal {

	public LrmObjectExistResponse projectExist(String projectId);

	public LrmObjectExistResponse assessmentExist(String projectId, String pptId);

}
