package com.pdinh.data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ConsentUserDao;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.PasswordPolicyUserStatusDao;
import com.pdinh.data.ms.dao.PhoneNumberDao;
import com.pdinh.data.ms.dao.PhoneNumberTypeDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.PhoneNumber;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.Roster;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.AddUserResultObj;
import com.pdinh.presentation.domain.ResultObj;
import com.pdinh.presentation.helper.ParticipantValidator;

/**
 * Session Bean implementation class CreateUserServiceLocalImpl
 */
@Stateless
@LocalBean
public class CreateUserServiceLocalImpl implements CreateUserServiceLocal {
	private static final Logger log = LoggerFactory.getLogger(CreateUserServiceLocal.class);
	@EJB
	UserDao userDao;

	@EJB
	CourseDao courseDao;

	@EJB
	ProjectUserDao projectUserDao;

	@EJB
	ProjectDao projectDao;

	@EJB
	PhoneNumberDao phoneNumberDao;

	@EJB
	PhoneNumberTypeDao phoneNumberTypeDao;

	@EJB
	EnrollmentServiceLocalImpl enrollmentService;

	@EJB
	ConsentUserDao consentUserDao;

	@EJB
	PasswordPolicyUserStatusDao pwdPolicyUserStatusDao;

	@EJB
	PasswordServiceLocalImpl passwordService;

	@EJB
	ProjectSetupSingleton setupSingleton;

	private static String lCaseChars = "abcdefghjkmnpqrstuvwxyz";
	private static String uCaseChars = "ABCDEFGHJKMNPQRSTUVWXYZ";
	private static String numbers = "23456789";

	private static String chqAbbv = "chq";
	private static String fexAbbv = "finex";

	/**
	 * Default constructor.
	 */
	public CreateUserServiceLocalImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public User createNewUser(User user, Company company, Project project, String CHQlanguage, String FEXlanguage) {
		user.setCompany(company);
		user.setActive((short) 1);

		user.setLearningType((short) 1);
		user.setMentorGender((short) 1);

		user.setPositions(new ArrayList<Position>());
		user.setRosters(new ArrayList<Roster>());
		// default user type to 1
		if (!((user.getType() > 0) && (user.getType() < 5))) {
			log.debug("user type is invalid, setting to 1");
			user.setType(1);
		}
		// default media server
		if (!(user.getMserverid() > 0)) {
			log.debug("media server is < 1, setting to 1");
			user.setMserverid((short) 1);
		}

		if (project != null) {
			List<ProjectCourse> projectCourses = project.getProjectCourses();
			if (projectCourses != null) {
				for (ProjectCourse projectCourse : projectCourses) {
					if (projectCourse.getSelected()) {
						user = enrollmentService.enrollUserInCourse(company, projectCourse, user);
						enrollmentService.activateDeactivateInstrument(company, projectCourse, user);
					}
				}
			}
		} else {
			if (company.getPermitallprog() == 1) {
				log.debug("Project is null, but company allows all users to take all courses. Enrolling in all courses.");
				user = enrollmentService.enrollUserInAllCourses(user, company);
			} else {
				log.debug("Project is Null, company enrollment is controlled, No Enrollment");
			}
		}
		log.debug("About to create user.  password: {}", user.getPassword());
		user = userDao.create(user);
		log.debug("Created User.  password: {}, Rowguid: {}", user.getPassword(), user.getRowguid());

		// synchronize persistence context with database
		// userDao.getEntityManager().refresh(user);

		// user = userDao.findById(user.getUsersId());

		// NW: Must set password after user is created in order to use
		// rowguid as salt. Uses first 10 characters of rowguid as salt.
		user.setHashedpassword(generateHashedPassword(user.getPassword(), user.getRowguid().substring(0, 10)
				.toUpperCase()));
		log.debug("finished setting hashedpassword: {}, password: {}", user.getHashedpassword(), user.getPassword());
		// PasswordPolicyUserStatus pwStatus =
		// passwordService.setupUserPolicyStatus(user);

		// user.setPasswordPolicyStatus(pwStatus);

		/* End Password Policy */
		log.debug("Performing user update");
		userDao.update(user);
		log.debug("Finished User update.  password: {}, hashedpassword {}, rowguid {}", user.getPassword(),
				user.getHashedpassword(), user.getRowguid());
		// passwordService.setupUserPolicyStatus(user);

		if (!(CHQlanguage == null)) {
			Course chq = setupSingleton.findCourseByAbbv(chqAbbv);
			userDao.updateCourseLanguagePref(user, chq, CHQlanguage);
		}
		if (!(FEXlanguage == null)) {
			Course fex = setupSingleton.findCourseByAbbv(fexAbbv);
			userDao.updateCourseLanguagePref(user, fex, FEXlanguage);
		}
		if (!(project == null)) {
			String principal = "serviceUser";

			try {
				Subject subject = SecurityUtils.getSubject();
				principal = subject.getPrincipal().toString();
			} catch (UnavailableSecurityManagerException exp) {
				log.error("No subject available, this must be service user");
			} catch (NullPointerException np){
				log.error("Could not find subject.  Set service user");
			}

			ProjectUser projectUser = new ProjectUser();
			projectUser.setProject(project);
			projectUser.setUser(user);
			projectUser.setAddedBy(principal);
			project.getProjectUsers().add(projectUser);

			projectUserDao.create(projectUser);

		}
		// userDao.update(user);
		log.info("User {}({}) created successfully", user.getUsername(), user.getUsersId());

		return user;
	}

	@Override
	public ResultObj validateUser(User user) {
		ParticipantValidator pv = new ParticipantValidator(userDao);
		AddUserResultObj result = null;
		result = pv.validateUserObj(user);
		return result;
	}

	@Override
	public byte[] generateHashedPassword(String password, String salt) {
		log.debug("In GenerateHashedPassword, create user service.  password: {}, salt: {}", password, salt);
		if (password == null || password.isEmpty()) {
			log.debug("Password is Null.  Generating random");
			password = getNewPassword();
			log.debug("Generated Random Password: {}", password);
		}

		byte[] hashbyte = (password + salt).getBytes();
		log.debug("salted password byte array: {}", hashbyte);
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update(hashbyte);
			log.debug("successfully hashed password.  Returning digested byte array");
			return digest.digest();

		} catch (NoSuchAlgorithmException e) {
			log.error("Error hashing password: {}", e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String getNewPassword() {
		Random generator = new Random();
		String pw = "";
		int index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		index = generator.nextInt(numbers.length());
		pw += numbers.charAt(index);

		index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		index = generator.nextInt(uCaseChars.length());
		pw += uCaseChars.charAt(index);

		index = generator.nextInt(numbers.length());
		pw += numbers.charAt(index);

		index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		index = generator.nextInt(uCaseChars.length());
		pw += uCaseChars.charAt(index);

		index = generator.nextInt(lCaseChars.length());
		pw += lCaseChars.charAt(index);

		return pw;
	}

	@Override
	public boolean addPhoneNumber(Integer usersId, String number, String typeAbbv, boolean isPrimary,
			boolean replacePrimary, String note) {
		if (number == null || number.isEmpty()) {
			return false;
		}
		List<PhoneNumber> numbers = phoneNumberDao.findByUsersIdAndType(usersId, typeAbbv);
		boolean match = false;
		PhoneNumber matchingNumber = new PhoneNumber();
		for (PhoneNumber pnum : numbers) {
			if (number.equalsIgnoreCase(pnum.getNumber())) {
				match = true;
				matchingNumber = pnum;
				break;
			}
		}
		if (match) {
			if (isPrimary) {

				if (matchingNumber.isPrimary()) {
					log.debug("The provided number already matches an existing number that is the primary number.");
					return false;
				} else {
					log.debug("Delete primary number and set the matching number as primary.  ");
					for (PhoneNumber num : numbers) {
						if (num.isPrimary()) {
							if (replacePrimary) {
								phoneNumberDao.delete(num);
							} else {
								num.setPrimary(false);
								phoneNumberDao.update(num);
							}
							break;
						}
					}
					matchingNumber.setPrimary(true);
					phoneNumberDao.update(matchingNumber);
					return true;
				}

			} else {
				if (matchingNumber.isPrimary()) {
					// set to not primary.. we can't do that b/c there must be a
					// primary.. need different method
				} else {
					// nothing to do here..number exists and is not primary
				}
			}
		} else {
			// no matching number found. create new
			User user = userDao.findById(usersId);
			PhoneNumber newNumber = new PhoneNumber();
			newNumber.setUser(user);
			newNumber.setPhoneNumberType(phoneNumberTypeDao.getByAbbv(typeAbbv));
			newNumber.setNote(note);
			newNumber.setNumber(number);
			newNumber.setPrimary(isPrimary);

			if (isPrimary) {

				for (PhoneNumber num : numbers) {
					if (num.isPrimary()) {
						if (replacePrimary) {
							phoneNumberDao.delete(num);
						} else {
							num.setPrimary(false);
							phoneNumberDao.update(num);
						}
						break;
					}
				}
			}

			phoneNumberDao.create(newNumber);
			user.getPhoneNumbers().add(newNumber);
			return true;
		}
		return false;
	}

}
