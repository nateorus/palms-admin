package com.pdinh.data.jaxb.lrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LrmObjectExistResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class LrmObjectExistResponse {

	@XmlAttribute
	private boolean exist;

	@XmlAttribute
	private String requestResult;

	@XmlAttribute
	private String message;

	@XmlAttribute
	private Integer lrmProjectId;

	public boolean isExist() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}

	public String getRequestResult() {
		return requestResult;
	}

	public void setRequestResult(String requestResult) {
		this.requestResult = requestResult;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getLrmProjectId() {
		return lrmProjectId;
	}

	public void setLrmProjectId(Integer lrmProjectId) {
		this.lrmProjectId = lrmProjectId;
	}

}
