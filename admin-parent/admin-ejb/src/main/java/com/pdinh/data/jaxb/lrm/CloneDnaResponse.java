package com.pdinh.data.jaxb.lrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CloneResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CloneDnaResponse {
	
	@XmlAttribute
	private String okStatus;

	public String getOkStatus() {
		return okStatus;
	}

	public void setOkStatus(String okStatus) {
		this.okStatus = okStatus;
	}
	
	

}
