package com.pdinh.data.instrumentnorms;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "selectedNorms")
@XmlAccessorType(XmlAccessType.FIELD)
public class SelectedNorms {
	@XmlElement(name = "projectNorms")
	private ProjectNormsDefaults projectNormsDefaults;

	@XmlElementWrapper(name = "participantNorms")
	@XmlElement(name = "participant")
	private List<Participant> participants = new ArrayList<Participant>();

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	public ProjectNormsDefaults getProjectNormsDefaults() {
		return projectNormsDefaults;
	}

	public void setProjectNormsDefaults(ProjectNormsDefaults projectNormsDefaults) {
		this.projectNormsDefaults = projectNormsDefaults;
	}

}
