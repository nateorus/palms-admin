package com.pdinh.data.jaxb.lrm;

import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.JaxbUtil;
import com.pdinh.reporting.PortalServletServiceLocal;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
@Stateless
@LocalBean
public class CopyProjectServiceLocalImpl implements CopyProjectServiceLocal {
	@Resource(name = "application/config_properties")
	private Properties properties;
	
	@EJB
	private PortalServletServiceLocal portalService;
	
	private static final Logger log = LoggerFactory.getLogger(CopyProjectServiceLocalImpl.class);
	
	@Override
	public CopyLrmProjectResponse copyLrmProject(int subjectId,
			int oldProjectId, int newProjectId, String name) {
		log.debug("In copyLrmProject() : {}, {}", newProjectId, name);
		CopyLrmProjectRequest request = new CopyLrmProjectRequest();
		request.setCreateSubjectId(String.valueOf(subjectId));
		request.setNewProjectId(String.valueOf(newProjectId));
		request.setOldProjectId(String.valueOf(oldProjectId));
		request.setNewProjectName(name);
		String serviceUrl = properties.getProperty("lrmHost");
		serviceUrl = "http://" + serviceUrl + "/lrmrest/jaxrs/CopyProject/" + oldProjectId;
		try {
			 
			Client client = Client.create();
	 
			WebResource webResource = client
			   .resource(serviceUrl);
	 
			
			
			com.sun.jersey.api.client.WebResource.Builder builder = webResource.type("application/xml");
			
			CopyLrmProjectResponse response = builder.post(CopyLrmProjectResponse.class, request);
			  
			/*
			if (response.getRequestResult().equals("FAILED")) {
				throw new RuntimeException("Failed : HTTP error code : "
				     + response.getRequestResult());
			}
			*/
	 
			log.debug("Output from Server .... \n");
			String output = response.getNewLrmProjectId();
			log.debug("New Project Id: {}", output);
			return response;
	 
		  } catch (Exception e) {
	 
			e.printStackTrace();
			CopyLrmProjectResponse resp = new CopyLrmProjectResponse();
			resp.setRequestResult("FAILED");
			resp.setNewLrmProjectId(String.valueOf(-1));
			return resp;
	 
		  }

		
	}
	

	@Override
	public boolean copyAbyDSetup(int oldProjectId, int newProjectId) {
		CloneDnaRequest request = new CloneDnaRequest();
		request.setSource(String.valueOf(oldProjectId));
		request.setDestination(String.valueOf(newProjectId));
		String xmlString = JaxbUtil.marshalToXmlString(request, CloneDnaRequest.class);
		log.debug("Sending this xml to portalService: {}", xmlString);
		String xmlReturned = portalService.portalServletRequest(xmlString);
		
		CloneDnaResponse response = (CloneDnaResponse) JaxbUtil.unmarshal(xmlReturned, CloneDnaResponse.class);
		log.debug("Got xml response: {}", xmlReturned);
		if (response == null || response.getOkStatus().equalsIgnoreCase("false")){
			return false;
		}else{
			return true;
		}
	}

}
