package com.pdinh.data.jaxb.admin;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "selfRegConfigListItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class SelfRegListItemRepresentation implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name = "id")
	private int id;
	
	@XmlAttribute(name = "value")
	private String value;
	
	@XmlAttribute(name = "type")
	private String type;
	
	@XmlAttribute(name = "lang")
	private String lang;
	
	

	public SelfRegListItemRepresentation(int id, String value, String type, String lang){
		this.id = id;
		this.value = value;
		this.type = type;
		this.lang = lang;
	}

	public SelfRegListItemRepresentation(){
		
	}
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
