package com.pdinh.data.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.LanguageDao;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.presentation.domain.CourseLanguageObj;
import com.pdinh.presentation.domain.LanguageObj;

@Startup
@Singleton(mappedName="languageSingleton")
public class LanguageSingleton {
	private static final Logger log = LoggerFactory.getLogger(LanguageSingleton.class);
	@EJB
	private LanguageDao languageDao;
	
	@EJB
	private CourseDao courseDao;
	
	private List<LanguageObj> languages;
	
	private List<LanguageObj> assessmentLanguages;
	
	private List<CourseLanguageObj> courseLangs;
	
	private List<Language> languageEntities;
	

	@PostConstruct
    public void init() {
    	List<LanguageObj> languages = new ArrayList<LanguageObj>();
    	List<LanguageObj> assessmentLanguages = new ArrayList<LanguageObj>();
		
		List<Language> langs = languageDao.findAll();
		log.debug("GOT {} Languages", langs.size());
		setLanguageEntities(langs);
		for (Language lang : langs) {
			log.trace("loading {} into language singleton ", lang.getTransName());
			LanguageObj dto = new LanguageObj(lang.getCode(), lang.getId(), lang.getName(), lang.getTransName());
			dto.setTransNameUnicode(lang.getTransNameUnicode());
			languages.add(dto);
			if (lang.getActiveAssessment() == (short) 1) {
				assessmentLanguages.add(dto);
			}
		}
		setLanguages(languages);
		setAssessmentLanguages(assessmentLanguages);
		//List<Object[]> rows = courseDao.findAllCourseAbbvIdArray();
		log.debug("Batch loading course languages");
		List<Course> courses = courseDao.findAllBatchLanguage();
		//List<Course> courses = courseDao.findAll();
		List<CourseLanguageObj> clo = new ArrayList<CourseLanguageObj>();
		for (Course c: courses){
			//List<Language> clangs = languageDao.findByCourseAbbv(c[0].toString());
			List<LanguageObj> lo = new ArrayList<LanguageObj>();
			for (Language l: langs){
				if (c.getLanguages().contains(l)){
					lo.add(new LanguageObj(l.getCode(), l.getId(), l.getName(), l.getTransName()));
				}
			}
			clo.add(new CourseLanguageObj(c.getAbbv(), c.getCourse(), lo));
			log.trace("Loaded {} Languages for course {}", lo.size(), c.getAbbv());
		}
		setCourseLangs(clo);
		log.debug("Loaded {} course language objects into Language Singleton", clo.size());
    }
	
	public HashMap<String, String> getDistinctLanguageList(List<String> courseAbbvs){
		HashMap<String, String> distinctLangs = new HashMap<String, String>();
		for (String abbv: courseAbbvs){
			List<LanguageObj> clos = getCourseLangs(abbv);
			for (LanguageObj lo: clos){
				distinctLangs.put(lo.getCode(), lo.getName());
			}
		}
		return distinctLangs;
	}
	
	public List<LanguageObj> getCourseLangs(String courseAbbv){
		for (CourseLanguageObj clo: this.courseLangs){
			if (clo.getCourseAbbv().equalsIgnoreCase(courseAbbv)){
				return clo.getCourseLangs();
			}
		}
		return new ArrayList<LanguageObj>();
	}
	 public LanguageObj getLangByCode(String code){
		 for (LanguageObj o:this.languages){
			 if (o.getCode().equalsIgnoreCase(code)){
				 return o;
			 }
		 }
		 return null;
	 }
	 public Language getLanguageByCode(String code){
		 for (Language l: this.languageEntities){
			 if (l.getCode().equalsIgnoreCase(code)){
				 return l;
			 }
		 }
		 return null;
	 }

	public List<LanguageObj> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageObj> languages) {
		this.languages = languages;
	}
	
	 public List<LanguageObj> getAssessmentLanguages() {
		return assessmentLanguages;
	}

	public void setAssessmentLanguages(List<LanguageObj> assessmentLanguages) {
		this.assessmentLanguages = assessmentLanguages;
	}

	public List<CourseLanguageObj> getCourseLangs() {
		return courseLangs;
	}
	public void setCourseLangs(List<CourseLanguageObj> coursesLangs) {
		this.courseLangs = coursesLangs;
	}

	public List<Language> getLanguageEntities() {
		return languageEntities;
	}

	public void setLanguageEntities(List<Language> languageEntities) {
		this.languageEntities = languageEntities;
	}

}
