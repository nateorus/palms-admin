package com.pdinh.data.jaxb.lrm;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.User;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

@Stateless
@LocalBean
public class AddUserServiceLocalImpl implements AddUserServiceLocal {

	@Resource(name = "application/config_properties")
	private Properties properties;

	private static final Logger log = LoggerFactory.getLogger(AddUserServiceLocalImpl.class);

	@Override
	public AddUserResponse addUsersToLrm(List<User> users, int projectId, int userId) {
		log.debug("In addUserToLrm() : User {} adding {} users to project {}", userId, users.size(), projectId);

		AddUserRequest request = new AddUserRequest();
		request.setProjectId(String.valueOf(projectId));
		request.setUserId(String.valueOf(userId));
		System.out.println("In PALMS, project ID is: " + request.getProjectId());

		List<NewProjectUser> newUsers = new ArrayList<NewProjectUser>();
		for (User user : users) {
			NewProjectUser newUser = new NewProjectUser();
			newUser.setUserId(String.valueOf(user.getUsersId()));
			newUser.setEmail(user.getEmail());
			newUsers.add(newUser);
			System.out.println("In PALMS, added user with ID: " + user.getUsersId() + ", Email: " + user.getEmail());
		}
		request.setNewUsers(newUsers);

		String serviceUrl = properties.getProperty("lrmHost");
		serviceUrl = "http://" + serviceUrl + "/lrmrest/jaxrs/AddUser";
		try {

			Client client = Client.create();
			WebResource webResource = client.resource(serviceUrl);
			com.sun.jersey.api.client.WebResource.Builder builder = webResource.type("application/xml");

			AddUserResponse response = builder.post(AddUserResponse.class, request);

			log.debug("Add User Result: {}", response.getPostResult());
			return response;

		  } catch (Exception e) {

			e.printStackTrace();
			AddUserResponse resp = new AddUserResponse();
			resp.setPostResult("FAILED");
			return resp;

		  }
	}

}
