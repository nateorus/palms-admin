package com.pdinh.data;

import javax.ejb.Local;

@Local
public interface VoucherIdServiceLocal {

	String getVoucherIdByCompanyId(int companyId);

	String getVoucherIdByUserId(int userId);
}
