package com.pdinh.data.singleton;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.presentation.domain.ProjectTemplateRuleObj;

@Startup
@Singleton(mappedName = "projectTemplateSingleton")
public class ProjectTemplateSingleton {

	private static final Logger log = LoggerFactory.getLogger(ProjectTemplateSingleton.class);

	private static HashMap<Integer, ProjectTemplateRuleObj> templateRules = new HashMap<Integer, ProjectTemplateRuleObj>();

	@PostConstruct
	public void init() {
		ProjectTemplateSingleton.initializeRules();
	}

	public static Integer generateHashCode(String section, String component, String projectType, String configType,
			String assmtLevel, String purpose, String deliveryMode) {
		Integer aHashCode = (section + "|" + component + "|" + projectType + "|" + configType + "|" + assmtLevel + "|"
				+ purpose + "|" + deliveryMode).hashCode();
		return aHashCode;
	}

	private static ArrayList<String> getStandardDurations() {
		ArrayList<String> durations = new ArrayList<String>();
		durations.add("60");
		durations.add("90");
		durations.add("120");
		durations.add("150");
		durations.add("180");
		durations.add("0");
		return durations;
	}

	private static ArrayList<String> getMllvaInterviewDurations() {
		ArrayList<String> durations = new ArrayList<String>();
		durations.add("60");
		durations.add("90");
		durations.add("120");
		durations.add("150");
		durations.add("0");
		return durations;
	}

	public static HashMap<Integer, ProjectTemplateRuleObj> initializeRules() {
		// enabled, visible, selected, enabledWithActiveProject, required,
		// defaultValue, ArrayList<String> list

		// enabled, visible, selected, enabledWithActiveProject, required,
		// defaultValue, ArrayList<String> list
		// FC01 to FC12 are for ABYD projects
		// FC13 to FC20 are for GL_GPS projects

		// FC01 Fixed Configuration
		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "PEER", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC01", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC02 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules
				.put(generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC02", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC03 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "finex", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, true, false, true, "",
						new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC03", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC04 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "PEER", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "finex", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, true, false, true, "",
						new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC04", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC05 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "finex", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, true, false, true, "",
						new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC05", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC06 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC06", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC07 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules
				.put(generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC07", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC08 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC08", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC09 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC09", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC10 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "VEND", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC10", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC11 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "CUST", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC11", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC12 FIXED CONFIGURATION
		templateRules.put(
				generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "CUST", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC12", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC13 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "COACHEVENT", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "120", getStandardDurations()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC13", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC14 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "COACHEVENT", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "120", getStandardDurations()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC14", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC15 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "COACHEVENT", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "120", getStandardDurations()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC15", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC16 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "COACHEVENT", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "120", getStandardDurations()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC16", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC17 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "INTERVIEW", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "90", getMllvaInterviewDurations()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC17", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC18 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "INTERVIEW", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "90", getMllvaInterviewDurations()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC18", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC19 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "INTERVIEW", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "90", getMllvaInterviewDurations()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC19", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// FC20 FIXED CONFIGURATION for GL_GPS
		templateRules.put(generateHashCode("Simulations", "INTERVIEW", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "90", getMllvaInterviewDurations()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC20", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		/*
		 *****************************************************
		 *  Fixed configurations FCOO - Tailored or Custom   *
		 *****************************************************
		 */
		// FC00 FIXED CONFIGURATION (Tailored or Custom) for ABYD
		templateRules
				.put(generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "IB", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "FLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, true, "90",
						getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, true, "90",
						getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "FLL",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, true, "90",
						getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "IC",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, true, "90",
						getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "SEA",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, true, "120",
						getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "CEO",
						"*", "*"), new ProjectTemplateRuleObj(true, true, true, false, true, "120",
						getStandardDurations()));

		templateRules
				.put(generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "SEA",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "DRM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "FLL",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "SEA", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "BOSS", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "FLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "PEER", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "PEER", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "SEA", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "TEAM", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "FLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "CUST", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "VEND", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "BUL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "BUSREVIEW", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "SEA",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, true, "",
						new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "FAEX", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "CEO", "*",
						"*"), new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "FNTV", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "CEO", "*",
						"*"), new ProjectTemplateRuleObj(true, true, true, false, false, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Simulations", "EP", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "CEO", "*",
						"*"), new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "SVPMTG", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "CEO", "*",
						"*"), new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "CHMTG", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "CEO", "*",
						"*"), new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Simulations", "CGPMTG", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "demo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*",
						"*"), new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "wg", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "finex", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "MLL", "*",
						"*"), new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "vedge_nodemo", ProjectType.ASSESSMENT_BY_DESIGN_ONLINE, "FC00", "*",
						"*", "*"), new ProjectTemplateRuleObj(true, true, false, false, false, "",
						new ArrayList<String>()));

		// FC00 FIXED CONFIGURATION (Tailored or Custom) for GL_GPS
		templateRules.put(
				generateHashCode("Simulations", "INTERVIEW", ProjectType.GLGPS, "FC00", "*", "SELECTION", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "90", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "COACHEVENT", ProjectType.GLGPS, "FC00", "*", "READINESS", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "120", getStandardDurations()));

		templateRules.put(
				generateHashCode("Simulations", "COACHEVENT", ProjectType.GLGPS, "FC00", "*", "DEVELOPMENT", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "120", getStandardDurations()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvapm", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvadr", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvabm", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lvaci", ProjectType.GLGPS, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		// FC00 FIXED CONFIGURATION (Tailored or Custom) for RED_ASMT
		templateRules.put(
				generateHashCode("Instruments", "demo", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "chq", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "gpil", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "cs2", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "wg", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules
				.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*",
						"*"), new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "livsim", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "bre", ProjectType.READINESS_ASSMT_BUL, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, true, "", new ArrayList<String>()));

		// FC00 FIXED CONFIGURATION (Tailored or Custom) for Ericsson
		// This was a slavish copy of AbyD. Since it will becaome MLL only, the
		// level distinctions have been removed.
		templateRules.put(generateHashCode("Simulations", "IB", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "INTERVIEW", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, true, "90", getStandardDurations()));

		templateRules.put(generateHashCode("Simulations", "DRM", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "BOSS", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "PEER", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "TEAM", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Simulations", "CGPMTG", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, true, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpil", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs2", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "wg", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "finex", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "iblva", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.ERICSSON, "FC00", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		/*
		 * ******************************************************************************************
		 *                       FIXED INSTRUMENTS FOR OTHER PROJECT TYPES
		 * ******************************************************************************************
		 */

		// TLT
		templateRules.put(
				generateHashCode("Instruments", "cs",
						ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "gpi",
						ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei",
						ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		// TLT with Congs
		templateRules.put(
				generateHashCode("Instruments", "cs", ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES,
						"*", "*", "*", "*"), new ProjectTemplateRuleObj(false, true, true, false, false, "",
						new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "gpi",
						ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "lei",
						ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(
				generateHashCode("Instruments", "rv", ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES,
						"*", "*", "*", "*"), new ProjectTemplateRuleObj(false, true, true, false, false, "",
						new ArrayList<String>()));

		// -----------------------------------------------------------------------------
		// TODO Per PA-421, logic that uses this information for setup has
		// been (logically) deprecated and the project is no longer available
		// as a selection in setup. The information has been left as it may be
		// used in change logic.
		// -----------------------------------------------------------------------------
		// TLT with Learning Agility
		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.TLT_LAG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.TLT_LAG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.TLT_LAG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lagil", ProjectType.TLT_LAG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		// TLT with Learning Agility and Cogns
		templateRules.put(generateHashCode("Instruments", "cs", ProjectType.TLT_LAG_COG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpi", ProjectType.TLT_LAG_COG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.TLT_LAG_COG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.TLT_LAG_COG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lagil", ProjectType.TLT_LAG_COG, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));
		// ---------------------- End deprecated content ---------------------
		// -----------------------------------------------------------------------

		// TLT DEMO with Cogs
		templateRules.put(generateHashCode("Instruments", "csd", ProjectType.TLTDEMO, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpid", ProjectType.TLTDEMO, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "leid", ProjectType.TLTDEMO, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rvd", ProjectType.TLTDEMO, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		// Assessment Testing
		templateRules.put(generateHashCode("Instruments", "demo", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "gpil", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "lei", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "cs2", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "wg", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "finex", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "finex", ProjectType.ASSESSMENT_TESTING, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		// Coaching Project
		templateRules.put(generateHashCode("Instruments", "coeng", ProjectType.COACH, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		// Shell Electronic Inbox (LoT)
		templateRules.put(
				generateHashCode("Instruments", "steib", ProjectType.SHELL_ELECTRONIC_INBOX_LOT, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		// Shell Electronic Inbox (LoC)
		templateRules.put(
				generateHashCode("Instruments", "sceib", ProjectType.SHELL_ELECTRONIC_INBOX_LOC, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		// viaEDGE
		templateRules.put(generateHashCode("Instruments", "vedge", ProjectType.VIAEDGE, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "vedge_nodemo", ProjectType.VIAEDGE, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, false, false, false, "", new ArrayList<String>()));

		// KFAP
		templateRules.put(generateHashCode("Instruments", "kfp", ProjectType.KF_ASSMT_POTENTIAL, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(false, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "rv2", ProjectType.KF_ASSMT_POTENTIAL, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "piq", ProjectType.KF_ASSMT_POTENTIAL, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, true, false, false, "", new ArrayList<String>()));

		templateRules.put(generateHashCode("Instruments", "chq", ProjectType.KF_ASSMT_POTENTIAL, "*", "*", "*", "*"),
				new ProjectTemplateRuleObj(true, true, false, false, false, "", new ArrayList<String>()));

		return templateRules;
	}

	public ProjectTemplateRuleObj getTemplateRule(String section, String component, String projectType,
			String configType, String assmtLevel, String purpose, String deliveryMode) {

		log.debug("getTemplateRule(): {}, {}, {}, {}, {}, {}, {}", section, component, projectType, configType,
				assmtLevel, purpose, deliveryMode);

		// Check various permutations of the project parameters with specific
		// values or "*" First check most for most common parameter combination
		// with highest probability of finding IF no
		// RULE IS FOUND RETURN A RULE WHERE VISIBLE AND ENABLE IS FALSE
		Integer aHashCodeKey = generateHashCode(section, component, projectType, configType, assmtLevel, purpose,
				deliveryMode);
		ProjectTemplateRuleObj aRule = templateRules.get(aHashCodeKey);
		if (aRule == null) {
			aHashCodeKey = generateHashCode(section, component, projectType, configType, "*", "*", "*");
			aRule = templateRules.get(aHashCodeKey);
			if (aRule == null) {
				aHashCodeKey = generateHashCode(section, component, projectType, configType, "*", purpose, "*");
				aRule = templateRules.get(aHashCodeKey);
				if (aRule == null) {
					aHashCodeKey = generateHashCode(section, component, projectType, configType, assmtLevel, "*", "*");
					aRule = templateRules.get(aHashCodeKey);
					if (aRule == null) {
						aHashCodeKey = generateHashCode(section, component, projectType, configType, "*", "*",
								deliveryMode);
						aRule = templateRules.get(aHashCodeKey);
						if (aRule == null) {
							aHashCodeKey = generateHashCode(section, component, projectType, configType, assmtLevel,
									purpose, "*");
							aRule = templateRules.get(aHashCodeKey);
							if (aRule == null) {
								aHashCodeKey = generateHashCode(section, component, projectType, configType, "*",
										purpose, deliveryMode);
								aRule = templateRules.get(aHashCodeKey);
								if (aRule == null) {
									aRule = new ProjectTemplateRuleObj(false, false, false, false, false, "",
											new ArrayList<String>());
								}
							}
						}
					}
				}
			}
		}
		return aRule;
	}

	/*
	public ProjectTemplateRuleObj getTemplateRule(String section, String component, String projectType,
			String configType, String assmtLevel, String purpose, String deliveryMode) {
		// IF A RULE IS NOT FOUND RETURN A RULE WHERE VISIBLE AND ENABLE IS
		// FALSE
		Integer aHashCodeKey = generateHashCode(section, component, projectType, configType, assmtLevel, purpose,
				deliveryMode);
		ProjectTemplateRuleObj aRule = templateRules.get(aHashCodeKey);
		if (aRule == null) {
			return new ProjectTemplateRuleObj(false, false, false, false, false, "", new ArrayList<String>());
		} else {
			return aRule;
		}
	}
	*/
}
