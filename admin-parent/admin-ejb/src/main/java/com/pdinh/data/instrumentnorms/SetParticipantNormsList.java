package com.pdinh.data.instrumentnorms;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SetParticipantNorms")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetParticipantNormsList {
	@XmlAttribute(name = "project_id")
	private Integer project;

	@XmlElement(name = "participant")
	private List<Participant> participants = new ArrayList<Participant>();

	public Integer getProject() {
		return project;
	}

	public void setProject(Integer project) {
		this.project = project;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

}
