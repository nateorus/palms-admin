package com.pdinh.data.singleton.helper;

import java.util.Comparator;

import org.apache.commons.lang3.builder.CompareToBuilder;

import com.pdinh.persistence.ms.entity.ProjectType;

public class ProjectTypeSorter implements Comparator<ProjectType> {

	@Override
	public int compare(ProjectType o1, ProjectType o2) {
		return new CompareToBuilder().append(o1.getName().toLowerCase(), o2.getName().toLowerCase()).toComparison();
	}

}
