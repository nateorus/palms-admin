package com.pdinh.data.jaxb.reporting;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reportList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportRepresentationList implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "totalRecordCount")
	long totalRecordCount;
	
	@XmlElement(name = "requestedBy")
	String requestedBy;
	
	@XmlElement(name = "report")
	List<ReportRepresentation> reports;

	public List<ReportRepresentation> getReports() {
		return reports;
	}

	public void setReports(List<ReportRepresentation> reports) {
		this.reports = reports;
	}

	public long getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}



}
