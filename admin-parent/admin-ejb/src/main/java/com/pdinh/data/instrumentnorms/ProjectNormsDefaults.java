package com.pdinh.data.instrumentnorms;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SetProjectNormDefaults")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectNormsDefaults {
	@XmlAttribute(name = "project_id")
	private Integer projectId;

	@XmlElement(name = "course")
	private List<Course> courses = new ArrayList<Course>();

	@XmlElementWrapper(name = "ParticipantList")
	@XmlElement(name = "Participant")
	private List<Participant> participants = new ArrayList<Participant>();

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer project) {
		this.projectId = project;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

}
