package com.pdinh.data.singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.pdinh.data.ms.dao.ConfigurationGroupDao;
import com.pdinh.data.ms.dao.ConfigurationTypeDao;
import com.pdinh.data.ms.dao.CourseDao;
import com.pdinh.data.ms.dao.CourseGroupTemplateDao;
import com.pdinh.data.ms.dao.OfficeLocationDao;
import com.pdinh.data.ms.dao.ProjectReportDao;
import com.pdinh.data.ms.dao.ProjectTypeDao;
import com.pdinh.data.singleton.helper.ProjectTypeSorter;
import com.pdinh.persistence.ms.entity.ConfigurationGroup;
import com.pdinh.persistence.ms.entity.ConfigurationType;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseGroupTemplate;
import com.pdinh.persistence.ms.entity.OfficeLocation;
import com.pdinh.persistence.ms.entity.ProjectReport;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.presentation.domain.ProjectReportObj;

@Startup
@Singleton(mappedName = "projectTypeSingleton")
public class ProjectSetupSingleton {

	@EJB
	private ProjectTypeDao projectTypeDao;

	@EJB
	private ConfigurationTypeDao configurationTypeDao;

	@EJB
	private ConfigurationGroupDao configurationGroupDao;

	@EJB
	private CourseGroupTemplateDao courseGroupTemplateDao;

	@EJB
	private CourseDao courseDao;

	@EJB
	private OfficeLocationDao officeLocationDao;

	@EJB
	private ProjectReportDao projectReportDao;

	private static final int TGT_LVL_FLL = 1;
	private static final int TGT_LVL_MLL = 2;
	private static final int TGT_LVL_BUL = 6;

	private List<ProjectType> projectTypes = new ArrayList<ProjectType>();
	private List<ConfigurationType> configurationTypes = new ArrayList<ConfigurationType>();
	private List<ConfigurationType> glgpsMLLConfigTypes = new ArrayList<ConfigurationType>();
	private List<ConfigurationType> glgpsOtherConfigTypes = new ArrayList<ConfigurationType>();
	private List<CourseGroupTemplate> courseGroupTemplates = new ArrayList<CourseGroupTemplate>();
	private List<ConfigurationType> abydMLLConfigTypes = new ArrayList<ConfigurationType>();
	private List<ConfigurationType> abydBULConfigTypes = new ArrayList<ConfigurationType>();
	private List<ConfigurationType> abydOtherConfigTypes = new ArrayList<ConfigurationType>();
	private List<ConfigurationType> custOnlyConfigTypes = new ArrayList<ConfigurationType>();
	private List<Course> courses = new ArrayList<Course>();
	private List<OfficeLocation> officeLocations = new ArrayList<OfficeLocation>();
	private List<ProjectReport> projectReportTemplates = new ArrayList<ProjectReport>();

	@PostConstruct
	public void init() {
		projectTypes = new ArrayList<ProjectType>();
		projectTypes = getProjectTypeDao().findAll();
		Collections.sort(projectTypes, new ProjectTypeSorter());
		configurationTypes = new ArrayList<ConfigurationType>();
		configurationTypes = getConfigurationTypeDao().findAll();
		courseGroupTemplates = new ArrayList<CourseGroupTemplate>();
		courseGroupTemplates = getCourseGroupTemplateDao().findAllCourseGroupTemplatesOrdered();
		glgpsMLLConfigTypes = new ArrayList<ConfigurationType>();
		glgpsOtherConfigTypes = new ArrayList<ConfigurationType>();
		abydMLLConfigTypes = new ArrayList<ConfigurationType>();
		abydBULConfigTypes = new ArrayList<ConfigurationType>();
		abydOtherConfigTypes = new ArrayList<ConfigurationType>();
		custOnlyConfigTypes = new ArrayList<ConfigurationType>();
		setCourses(new ArrayList<Course>());
		setCourses(courseDao.findAll());
		setOfficeLocations(new ArrayList<OfficeLocation>());
		setOfficeLocations(officeLocationDao.findAllOfficeLocationsOrdered());
		setProjectReportTemplates(projectReportDao.getAllProjectReportTemplates());

		/* BEWARE: Utter, complete hackery BS ahead. Should be table mapped. */
		// for (ConfigurationType ct : configurationTypes) {
		// if (ct.getConfigurationTypeId() <= 5) {
		// abydMLLConfigTypes.add(ct);
		// }
		// if (ct.getConfigurationTypeId() > 5 && ct.getConfigurationTypeId() <=
		// 12) {
		// abydBULConfigTypes.add(ct);
		// }
		// if (ct.getConfigurationTypeId() > 12) {
		// glgpsMLLConfigTypes.add(ct);
		// }
		// if (ct.getCode().equalsIgnoreCase("FC00")) {
		// abydMLLConfigTypes.add(ct);
		// abydBULConfigTypes.add(ct);
		// abydOtherConfigTypes.add(ct);
		// glgpsOtherConfigTypes.add(ct);
		// custOnlyConfigTypes.add(ct);
		// }
		// }

		// De-hacking a little...
		// ...assumes that custOnly pertains to custom/tailored only. If not,
		// then we need to refactor for Ericsson and Readiness assessment
		List<ConfigurationGroup> cgList = getConfigurationGroupDao().findAll();
		for (ConfigurationGroup cg : cgList) {
			if (cg.getConfigurationGroupCode().equals(ConfigurationGroup.ABYD_MLL)) {
				abydMLLConfigTypes.addAll(cg.getConfigurationTypes());
			}
			if (cg.getConfigurationGroupCode().equals(ConfigurationGroup.ABYD_BUL)) {
				abydBULConfigTypes.addAll(cg.getConfigurationTypes());
			}
			if (cg.getConfigurationGroupCode().equals(ConfigurationGroup.GLGPS_MLL)) {
				glgpsMLLConfigTypes.addAll(cg.getConfigurationTypes());
			}
		} // End of config group loop

		// Add the FC00 to requisite lists... needs to be at bottom of list,
		// hence processing here.
		for (ConfigurationGroup cg : cgList) {
			if (cg.getConfigurationGroupCode().equals(ConfigurationGroup.UNASSIGNED)) {
				// Find FC00 and save it for later (want ait at the bottom of
				// lists)
				for (ConfigurationType ct : cg.getConfigurationTypes()) {
					if (ct.getCode().equalsIgnoreCase("FC00")) {
						abydMLLConfigTypes.add(ct);
						abydBULConfigTypes.add(ct);
						abydOtherConfigTypes.add(ct);
						glgpsMLLConfigTypes.add(ct);
						glgpsOtherConfigTypes.add(ct);
						custOnlyConfigTypes.add(ct);
						break;
					}
				}
				// We found UNASSIGNED... no more searching need be done
				break;
			} // End of UNASSIGNED check
		}
	}

	public List<ProjectReportObj> getProjectReportTemplatesForProjectType(int projectTypeId) {
		List<ProjectReportObj> templates = new ArrayList<ProjectReportObj>();
		for (ProjectReport pr : this.projectReportTemplates) {
			if (pr.getProjectTypeId() == projectTypeId) {
				templates.add(new ProjectReportObj(pr));
			}
		}
		return templates;
	}

	public List<ProjectType> findAllByProductCode(String code) {
		try {
			List<ProjectType> pts = new ArrayList<ProjectType>();
			for (ProjectType pt : getProjectTypes()) {
				if (code.equalsIgnoreCase(pt.getProductTypeCode())) {
					pts.add(pt);
				}
			}
			return pts;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<ProjectType>();
		}
	}

	public Boolean isConfigPanelVisible(int id) {
		ProjectType pt = findProjectTypeById(id);
		return pt.getTargetLevelVisible() || pt.getConfigurationTypeVisible() || pt.getScoringMethodVisible();
	}

	public Boolean isPmSchedulerSetupVisible(int id) {
		ProjectType pt = findProjectTypeById(id);
		return pt.getConfigurationTypeVisible();
	}

	public ProjectType findProjectTypeById(int id) {
		for (ProjectType p : getProjectTypes()) {
			if (id == p.getProjectTypeId()) {
				return p;
			}
		}
		return null;
	}

	public ProjectType findProjectTypeByCode(String code) {
		for (ProjectType p : getProjectTypes()) {
			if (code.equalsIgnoreCase(p.getProjectTypeCode())) {
				return p;
			}
		}
		return null;
	}

	public List<ConfigurationType> getConfigurationTypesForProjectType(ProjectType projectType, int targetLevelTypeId) {
		// System.out.println("getConfigurationTypesForProjectType " +
		// projectType.getProjectTypeId() + ", " + targetLevelTypeId);
		List<ConfigurationType> cts = new ArrayList<ConfigurationType>();
		if (projectType.getProjectTypeCode().equals(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE)) {
			if (targetLevelTypeId == TargetLevelType.MLL) {
				cts = getAbydMLLConfigTypes();
			} else if (targetLevelTypeId == TargetLevelType.BUL) {
				cts = getAbydBULConfigTypes();
			} else {
				cts = getAbydOtherConfigTypes();
			}
		} else if (projectType.getProjectTypeCode().equals(ProjectType.GLGPS)) {
			if (targetLevelTypeId == TargetLevelType.MLL) {
				cts = getGlgpsMLLConfigTypes();
			} else {
				cts = getGlgpsOtherConfigTypes();
			}
		} else if (projectType.getProjectTypeCode().equals(ProjectType.READINESS_ASSMT_BUL)) {
			cts = getCustOnlyConfigTypes();
		} else if (projectType.getProjectTypeCode().equals(ProjectType.ERICSSON)) {
			cts = getCustOnlyConfigTypes();
		}

		return cts;
	}

	public ConfigurationType findConfigurationTypeById(int id) {
		for (ConfigurationType ct : getConfigurationTypes()) {
			if (ct.getConfigurationTypeId() == id) {
				return ct;
			}
		}
		return new ConfigurationType(-1);
	}

	public ConfigurationType findConfigurationTypeByCode(String code) {
		for (ConfigurationType ct : getConfigurationTypes()) {
			if (ct.getCode().equalsIgnoreCase(code)) {
				return ct;
			}
		}
		return null;
	}

	public Iterator<CourseGroupTemplate> findCourseGroupByProjectTypeFlowType(int projectTypeId, int flowTypeId) {
		List<CourseGroupTemplate> ctgs = new ArrayList<CourseGroupTemplate>();
		for (CourseGroupTemplate ctg : getCourseGroupTemplates()) {
			if (ctg.getProjectType().getProjectTypeId() == projectTypeId
					&& ctg.getCourseFlowType().getCourseFlowTypeId() == flowTypeId) {
				ctgs.add(ctg);
			}
		}
		return ctgs.iterator();
	}

	public int getDefaultTargetLevelTypeId(int projectTypeId) {
		// System.out.println("getDefaultTargetLevelId " + projectTypeId);
		/*
		switch (projectTypeId) {
		case this.findProjectTypeByCode(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE).getProjectTypeId():
			return TGT_LVL_MLL;
		case this.findProjectTypeByCode(ProjectType.GLGPS).getProjectTypeId():
			return TGT_LVL_MLL;
		case this.findProjectTypeByCode(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES).getProjectTypeId():
			return TGT_LVL_FLL;
		case this.findProjectTypeByCode(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES).getProjectTypeId():
			return TGT_LVL_FLL;
		case this.findProjectTypeByCode(ProjectType.TLT_LAG).getProjectTypeId():
			return TGT_LVL_FLL;
		case this.findProjectTypeByCode(ProjectType.TLT_LAG_COG).getProjectTypeId():
			return TGT_LVL_FLL;
		case this.findProjectTypeByCode(ProjectType.TLTDEMO).getProjectTypeId():
			return TGT_LVL_FLL;
		case this.findProjectTypeByCode(ProjectType.READINESS_ASSMT_BUL).getProjectTypeId():
			return TGT_LVL_BUL;
		default:
			return -1;
		}
		*/
		if (this.findProjectTypeByCode(ProjectType.ASSESSMENT_BY_DESIGN_ONLINE).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_MLL;
		} else if (this.findProjectTypeByCode(ProjectType.GLGPS).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_MLL;
		} else if (this.findProjectTypeByCode(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITH_COGNITIVES)
				.getProjectTypeId() == projectTypeId) {
			return TGT_LVL_FLL;
		} else if (this.findProjectTypeByCode(ProjectType.TALENTVIEW_OF_LEADERSHIP_TRANSITIONS_WITHOUT_COGNITIVES)
				.getProjectTypeId() == projectTypeId) {
			return TGT_LVL_FLL;
			// ----------------------------------------------------------------
			// Per PA-421, TLT w/ Learning Agility has been deprecated.
		} else if (this.findProjectTypeByCode(ProjectType.TLT_LAG).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_FLL;
		} else if (this.findProjectTypeByCode(ProjectType.TLT_LAG_COG).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_FLL;
			// -------------- End of deprecated items -------------------------
		} else if (this.findProjectTypeByCode(ProjectType.TLTDEMO).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_FLL;
		} else if (this.findProjectTypeByCode(ProjectType.READINESS_ASSMT_BUL).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_BUL;
		} else if (this.findProjectTypeByCode(ProjectType.ERICSSON).getProjectTypeId() == projectTypeId) {
			return TGT_LVL_MLL;
		} else {
			return -1;
		}
	}

	public Course findCourseByAbbv(String abbv) {
		for (Course c : getCourses()) {
			if (c.getAbbv().equalsIgnoreCase(abbv)) {
				return c;
			}
		}
		return null;
	}

	public Course findCourseById(int id) {
		for (Course c : getCourses()) {
			if (c.getCourse() == id) {
				return c;
			}
		}
		return null;
	}

	public ProjectTypeDao getProjectTypeDao() {
		return projectTypeDao;
	}

	public void setProjectTypeDao(ProjectTypeDao projectTypeDao) {
		this.projectTypeDao = projectTypeDao;
	}

	public ConfigurationTypeDao getConfigurationTypeDao() {
		return configurationTypeDao;
	}

	public void setConfigurationTypeDao(ConfigurationTypeDao configurationTypeDao) {
		this.configurationTypeDao = configurationTypeDao;
	}

	public ConfigurationGroupDao getConfigurationGroupDao() {
		return configurationGroupDao;
	}

	public void setConfigurationGroupDao(ConfigurationGroupDao configurationGroupDao) {
		this.configurationGroupDao = configurationGroupDao;
	}

	public List<ProjectType> getProjectTypes() {
		return projectTypes;
	}

	public void setProjectTypes(List<ProjectType> projectTypes) {
		this.projectTypes = projectTypes;
	}

	public List<ConfigurationType> getConfigurationTypes() {
		return configurationTypes;
	}

	public void setConfigurationTypes(List<ConfigurationType> configurationTypes) {
		this.configurationTypes = configurationTypes;
	}

	public CourseGroupTemplateDao getCourseGroupTemplateDao() {
		return courseGroupTemplateDao;
	}

	public void setCourseGroupTemplateDao(CourseGroupTemplateDao courseGroupTemplateDao) {
		this.courseGroupTemplateDao = courseGroupTemplateDao;
	}

	public List<CourseGroupTemplate> getCourseGroupTemplates() {
		return courseGroupTemplates;
	}

	public void setCourseGroupTemplates(List<CourseGroupTemplate> courseGroupTemplates) {
		this.courseGroupTemplates = courseGroupTemplates;
	}

	public List<ConfigurationType> getAbydMLLConfigTypes() {
		return abydMLLConfigTypes;
	}

	public void setAbydMLLConfigTypes(List<ConfigurationType> abydMLLConfigTypes) {
		this.abydMLLConfigTypes = abydMLLConfigTypes;
	}

	public List<ConfigurationType> getAbydBULConfigTypes() {
		return abydBULConfigTypes;
	}

	public void setAbydBULConfigTypes(List<ConfigurationType> abydBULConfigTypes) {
		this.abydBULConfigTypes = abydBULConfigTypes;
	}

	public List<ConfigurationType> getAbydOtherConfigTypes() {
		return abydOtherConfigTypes;
	}

	public void setAbydOtherConfigTypes(List<ConfigurationType> abydOtherConfigTypes) {
		this.abydOtherConfigTypes = abydOtherConfigTypes;
	}

	public List<ConfigurationType> getGlgpsMLLConfigTypes() {
		return glgpsMLLConfigTypes;
	}

	public void setGlgpsMLLConfigTypes(List<ConfigurationType> glgpsMLLConfigTypes) {
		this.glgpsMLLConfigTypes = glgpsMLLConfigTypes;
	}

	public List<ConfigurationType> getGlgpsOtherConfigTypes() {
		return glgpsOtherConfigTypes;
	}

	public void setGlgpsOtherConfigTypes(List<ConfigurationType> glgpsOtherConfigTypes) {
		this.glgpsOtherConfigTypes = glgpsOtherConfigTypes;
	}

	public List<ConfigurationType> getCustOnlyConfigTypes() {
		return this.custOnlyConfigTypes;
	}

	public void setCustOnlyConfigTypes(List<ConfigurationType> custOnlyConfigTypes) {
		this.custOnlyConfigTypes = custOnlyConfigTypes;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public List<OfficeLocation> getOfficeLocations() {
		return officeLocations;
	}

	public void setOfficeLocations(List<OfficeLocation> officeLocations) {
		this.officeLocations = officeLocations;
	}

	public List<ProjectReport> getProjectReportTemplates() {
		return projectReportTemplates;
	}

	public void setProjectReportTemplates(List<ProjectReport> projectReportTemplates) {
		this.projectReportTemplates = projectReportTemplates;
	}

}
