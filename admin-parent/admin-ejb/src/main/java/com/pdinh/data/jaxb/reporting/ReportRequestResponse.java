package com.pdinh.data.jaxb.reporting;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reportRequestResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportRequestResponse {

	@XmlElement(name = "reportRequestId")
	public int reportRequestId;
	
	@XmlElement(name = "reportRequestKey")
	public String reportRequestKey;
	
	@XmlElement(name = "reportType")
	public String reportType;
	
	@XmlElement(name = "errorCode")
	public String errorCode;
	
	@XmlElement(name = "errorMessage")
	public String errorMessage;

	public int getReportRequestId() {
		return reportRequestId;
	}

	public void setReportRequestId(int reportRequestId) {
		this.reportRequestId = reportRequestId;
	}

	public String getReportRequestKey() {
		return reportRequestKey;
	}

	public void setReportRequestKey(String reportRequestKey) {
		this.reportRequestKey = reportRequestKey;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
}
