package com.pdinh.data;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;

@Local
public interface ProjectMembershipServiceLocal {

	public ProjectUser addUserToProject(Integer usersId, Integer projectId);
	
	public ProjectUser addUserToProject(User user, Project project);
	
	public ProjectUser addUserToProjectWithEnrollment(User user, Project project);
	
	public boolean removeUserFromProject(Integer usersId, Integer projectId);
	
	public boolean removeUserFromProject(User user, Project project);
}
