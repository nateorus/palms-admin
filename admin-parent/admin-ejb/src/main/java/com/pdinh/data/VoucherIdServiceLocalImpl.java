package com.pdinh.data;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.CompanyVoucherDao;
import com.pdinh.data.ms.dao.UserDao;

@Stateless
public class VoucherIdServiceLocalImpl implements VoucherIdServiceLocal {
	@EJB
	private UserDao userDao;
	@EJB
	private CompanyDao companyDao;
	@EJB
	private CompanyVoucherDao companyVoucherDao;

	/**
	 * Returns a String voucherId.
	 * 
	 * @param int companyId
	 */
	@Override
	public String getVoucherIdByCompanyId(int companyId) {
		String voucherId;
		try {
			voucherId = companyVoucherDao.findVoucherForCompany(companyDao.findById(companyId)).getVoucherId();
		} catch (Exception e) {
			voucherId = null;
		}
		return voucherId;
	}

	/**
	 * Returns a String voucherId.
	 * 
	 * @param int userId
	 */
	@Override
	public String getVoucherIdByUserId(int userId) {
		String voucherId;
		try {
			voucherId = companyVoucherDao.findVoucherForCompany(userDao.findById(userId).getCompany()).getVoucherId();
		} catch (Exception e) {
			voucherId = null;
		}
		return voucherId;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public CompanyVoucherDao getCompanyVoucherDao() {
		return companyVoucherDao;
	}

	public void setCompanyVoucherDao(CompanyVoucherDao companyVoucherDao) {
		this.companyVoucherDao = companyVoucherDao;
	}
}
