package com.pdinh.data.jaxb.lrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="newProjectUser")
@XmlAccessorType(XmlAccessType.FIELD)
public class NewProjectUser {

	@XmlAttribute
	private String userId;

	@XmlAttribute
	private String email;

	public String getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
