package com.pdinh.data.jaxb.reporting;

import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.exception.RCMGenerationException;
import com.pdinh.exception.ServerUnavailableException;


@XmlRootElement(name = "error")
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorRepresentation {
	
	@XmlElement(name = "errorCode")
	public String errorCode;
	
	@XmlElement(name = "errorMessage")
	public String errorMessage;
	
	@XmlElement(name = "errorDetail")
	public String errorDetail;
	
	public ErrorRepresentation(){
		
	}
	
	public ErrorRepresentation(PalmsException p){
		//ErrorRepresentation error = new ErrorRepresentation();
		if (p instanceof ServerUnavailableException){
			this.setErrorCode(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getCode());
			this.setErrorMessage(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getMessage());
			this.setErrorDetail("The url " + ((ServerUnavailableException) p).getServerName() + " was unavailable.  This resource is required to generate the url for the report content");

		} else if (p instanceof RCMGenerationException){
			this.setErrorCode(p.getErrorCode());
			this.setErrorMessage(p.getMessage());
			this.setErrorDetail("An error occurred while preparing the report request.  Insufficient data exists to generate this report.");

		}else{
			if (!(p.getErrorCode() == null)){
				this.setErrorCode(p.getErrorCode());
			}else{
				this.setErrorCode(PalmsErrorCode.UNKNOWN_ERROR.getCode());
			}
			if (!(p.getMessage() == null)){
				this.setErrorMessage(p.getMessage());
			}else{
				this.setErrorMessage(PalmsErrorCode.UNKNOWN_ERROR.getMessage());
			}
			this.setErrorDetail("");
		}
		//return error;
	}
	
	public Response buildErrorResponse(List<MediaType> mediaTypes){
		String responseType = MediaType.APPLICATION_XML;
		if (mediaTypes.contains(MediaType.APPLICATION_JSON_TYPE)){
			responseType = MediaType.APPLICATION_JSON;
		}

		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(this)
				.type(responseType)
				.build();
		return response;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

}
