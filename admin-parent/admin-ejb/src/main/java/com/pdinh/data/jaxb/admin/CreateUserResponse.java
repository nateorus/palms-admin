package com.pdinh.data.jaxb.admin;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "createUserResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateUserResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlAttribute(name = "usersId")
	private int usersId;

	@XmlAttribute(name = "success")
	private boolean success;

	@XmlAttribute(name = "username")
	private String username;

	@XmlAttribute(name = "message")
	private String message;
	
	@XmlAttribute(name = "errorCode")
	private String errorCode;
	
	@XmlAttribute(name = "loginStatus")
	private int loginStatus;

	public int getUsersId() {
		return usersId;
	}

	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

}
