package com.pdinh.data.jaxb.reporting;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reportRequestResponseList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportRequestResponseList {

	@XmlElement(name = "reportRequestId")
	int reportRequestId;
	
	@XmlElement(name = "reportRequestResponse")
	List<ReportRequestResponse> reportRequestResponseList;

	public List<ReportRequestResponse> getReportRequestResponseList() {
		return reportRequestResponseList;
	}

	public void setReportRequestResponseList(
			List<ReportRequestResponse> reportRequestResponseList) {
		this.reportRequestResponseList = reportRequestResponseList;
	}

	public int getReportRequestId() {
		return reportRequestId;
	}

	public void setReportRequestId(int reportRequestId) {
		this.reportRequestId = reportRequestId;
	}
}
