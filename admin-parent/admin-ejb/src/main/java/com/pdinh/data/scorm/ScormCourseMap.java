package com.pdinh.data.scorm;

import java.util.HashMap;
import java.util.Map;

public class ScormCourseMap {
	
	private static Map<String,String> map = new HashMap<String,String>();
	
	static {
		map.put("lagil", "ORG_LAGIL_1");
		map.put("cs", "ORG_CS_1");
	}
	
	public static String getScormCourseId(String courseAbbv) {
		return map.get(courseAbbv);
	}

}
