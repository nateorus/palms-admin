package com.pdinh.data;

import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.UserDao;

@Stateless
//@LocalBean
public class UsernameServiceLocalImpl implements UsernameServiceLocal {
	private static final Logger log = LoggerFactory.getLogger(UsernameServiceLocalImpl.class);
	@EJB
	private UserDao userDao;
	
//	private String username;
//	private final static String UNIQUE_USERNAME_SQL = "SELECT count(*) as count FROM platform.dbo.users WHERE username = ?";
//	private final static String UNIQUE_USERNAME_BYID_SQL = "SELECT count(*) as count FROM platform.dbo.users WHERE username = ? AND users_id <> ?";
	private final static String OK_UNIQUER_CHARS =  "23456789"; //abcdefghjkmnpqrstuvwxyz";
	private final static String OK_UNIQUER_CHARS_ANON =  "23456789abcdefghjkmnpqrstuvwxyz";
	private final static String OK_FIRSTLAST_CHARS = "1234567890 abcdefghijklmnopqrstuvwxyz'._-";
	private final static String OK_USERNAME_CHARS = "1234567890abcdefghijklmnopqrstuvwxyz._-";
	private final static int MAX_UNIQUER_LEN = 8;
	private final static int MAX_USERNAME_LEN = 50;
	private final static int MIN_USERNAME_LEN = 4;
	
		
	public String generateUsername(String email, String firstname, String lastname) {
		log.debug("In UsernameServiceLocal.generateUsername {}, {}, {}", email, firstname,lastname);
		StringBuffer seed = new StringBuffer();
		StringBuffer username = new StringBuffer();
		try {
			if ((!isEmptyString(firstname) && !isEmptyString(lastname)) && 
					(isOkString(firstname.toLowerCase(), OK_FIRSTLAST_CHARS) && isOkString(lastname.toLowerCase(), OK_FIRSTLAST_CHARS))) {		
				seed.append(firstname + lastname);
			}else{
				seed.append(email.split("@")[0]);			
			}
		}catch(Exception e) {
			try {
				seed.append(email.split("@")[0]);
			}catch(Exception ea) {				
			}
		}
		if (seed.length() == 0) {
			seed.append("anon." + getAnonUniquer(MAX_UNIQUER_LEN));
		}
		try {
			String seedStr = seed.toString().toLowerCase();
			for (int e = 0; e < seedStr.length() && e < MAX_USERNAME_LEN; e++){
				if (OK_USERNAME_CHARS.indexOf(seedStr.charAt(e)) != -1) {
					username.append(seedStr.charAt(e));
				}
			}
			boolean usernameIsUnique = false;
			String uniquer = "";
			int uSeedLen = 0;
			while (!usernameIsUnique){
				if (username.toString().length() + uSeedLen < MIN_USERNAME_LEN) {
					uSeedLen += MIN_USERNAME_LEN - username.toString().length();
				}
				if (username.toString().length() + uSeedLen > MAX_USERNAME_LEN) {
					String u;
					u = username.toString().substring(0, MAX_USERNAME_LEN - MAX_UNIQUER_LEN);
					username = new StringBuffer().append(u);						
				}
				if (!usernameIsUnique) {
					usernameIsUnique = isUnique(new StringBuffer(username+uniquer));
				}
				if (!usernameIsUnique) {
					uSeedLen++;
					uniquer = getUniquer(uSeedLen);					
				}
			} 
			username.append(uniquer);
		}catch (Exception e) {
			username = new StringBuffer().append("anon." + getAnonUniquer(MAX_UNIQUER_LEN));
		}
		return username.toString();
	}
	
	private Boolean isUnique(StringBuffer username) {
		if (username.toString().length() < MIN_USERNAME_LEN) {
			return false;
		}
		int numUsers = 0;
		try {
			numUsers = userDao.isUniqueUsername(username.toString());
		}catch (Exception e){}
		
		return numUsers == 0;
	}
	
	private Boolean isOkString(String str, String okChars) {
		for (int i = 0; i < str.length(); i++){
			if (okChars.indexOf(str.charAt(i)) == -1) {
				return false;
			}
		}
		return true;
	}
	
	private String getUniquer(int x) {
		Random generator = new Random();
		StringBuffer uniquer = new StringBuffer();
		if (x > MAX_UNIQUER_LEN) {
			x = MAX_UNIQUER_LEN;
		}
		for (int u = 0; u < x; u++) {
			int index = generator.nextInt(OK_UNIQUER_CHARS.length());
			uniquer.append(OK_UNIQUER_CHARS.charAt(index));			
		}
		return uniquer.toString();
	}
	
	private String getAnonUniquer(int x) {
		Random generator = new Random();
		StringBuffer uniquer = new StringBuffer();
		if (x > MAX_UNIQUER_LEN) {
			x = MAX_UNIQUER_LEN;
		}
		for (int u = 0; u < x; u++) {
			int index = generator.nextInt(OK_UNIQUER_CHARS_ANON.length());
			uniquer.append(OK_UNIQUER_CHARS_ANON.charAt(index));			
		}
		return uniquer.toString();
	}
	
//	public void setUsername(String username) {
//		this.username = username;
//	}
//	public String getUsername() {
//		return username;
//	}
	
	private Boolean isUniqueForUser(String username, int usersId) {
		int numUsers = 0;
		try {
			numUsers = userDao.isUniqueUsernameForUser(username.toString(), usersId);
		}catch (Exception e){}
		
		return numUsers == 0;
	}
	private boolean isEmptyString(String str) {
	    if (str == null) {
	      return true;
	    }
	
	    if (str.trim().equals("")) {
	      return true;
	    }
	
	    return false;
	  }

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
