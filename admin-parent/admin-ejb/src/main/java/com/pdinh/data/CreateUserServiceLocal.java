package com.pdinh.data;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ResultObj;

@Local
public interface CreateUserServiceLocal {
	
	public User createNewUser(User user, Company company, Project project, String CHQlanugage, String FEXlanguage);
	
	public ResultObj validateUser(User user);
	
	public String getNewPassword();
	
	public byte[] generateHashedPassword(String password, String salt);
	
	public boolean addPhoneNumber(Integer usersId, String number, String typeAbbv, boolean isDefault, boolean replaceDefault, String note);

}
