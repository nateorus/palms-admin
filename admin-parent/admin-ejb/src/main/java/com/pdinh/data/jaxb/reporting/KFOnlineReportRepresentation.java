package com.pdinh.data.jaxb.reporting;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reportQueueRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class KFOnlineReportRepresentation {
	
	
	@XmlAttribute(name = "reportRequestKey")
	String reportRequestKey;
	
	@XmlAttribute(name = "fileExtension")
	String fileExtention;
	
	@XmlAttribute(name = "priority")
	String priority;
	
	@XmlAttribute(name = "metaData")
	String metaData;
	
	@XmlAttribute(name = "status")
	String status;
	
	@XmlAttribute(name = "retryCount")
	String retryCount;
	
	

	public String getFileExtention() {
		return fileExtention;
	}

	public void setFileExtention(String fileExtention) {
		this.fileExtention = fileExtention;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(String retryCount) {
		this.retryCount = retryCount;
	}

	public String getReportRequestKey() {
		return reportRequestKey;
	}

	public void setReportRequestKey(String reportRequestKey) {
		this.reportRequestKey = reportRequestKey;
	}


}
