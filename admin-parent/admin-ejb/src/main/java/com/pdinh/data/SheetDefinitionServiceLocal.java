package com.pdinh.data;

import java.util.List;

import javax.ejb.Local;

import com.kf.dochandler.excel.DocumentValidation;
import com.kf.dochandler.excel.SheetDefinition;
import com.kf.uffda.dto.FieldDto;

@Local
public interface SheetDefinitionServiceLocal {

	SheetDefinition getSheetDefinitionFor(List<FieldDto> persistentStandardFields,
			List<FieldDto> persistentCustomFields, String sheetName, List<String> columnNames, int maxRecords,
			boolean usesTemplate, DocumentValidation validation);
}
