package com.pdinh.data.jaxb.lrm;

import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Stateless
@LocalBean
public class LrmObjectServiceLocalImpl implements LrmObjectServiceLocal {
	// TODO Re-factor this to utilize the same underlying calling code;
	// externals
	// would be the same, but they just build the URL and call the common
	// fetching logic. Or better yet, they build the parameters and the called
	// logic post-pends it to the full URL (doing all the rest of the setup work
	// in a single place).
	@Resource(name = "application/config_properties")
	private Properties properties;

	private static final Logger log = LoggerFactory.getLogger(LrmObjectServiceLocalImpl.class);

	@Override
	public LrmObjectExistResponse projectExist(String projectId) {
		log.debug("In projectExist() : {}", projectId);
		ProjectExistRequest request = new ProjectExistRequest();
		request.setProjectId(String.valueOf(projectId));
		String serviceUrl = properties.getProperty("lrmHost");
		serviceUrl = "http://" + serviceUrl + "/lrmrest/jaxrs/LrmProjectExist/" + projectId;
		try {
			DefaultClientConfig config = new DefaultClientConfig();
			config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
			Client client = Client.create(config);
			WebResource webResource = client.resource(serviceUrl);
			com.sun.jersey.api.client.WebResource.Builder builder = webResource.type("application/xml");
			LrmObjectExistResponse response = builder.get(LrmObjectExistResponse.class);

			log.debug("Output from Server .... \n");
			boolean output = response.isExist();
			log.debug("Project Exists: {}", output);
			return response;

		} catch (Exception e) {
			//e.printStackTrace();
			log.error("Could Not connect to lrmrest: {}", e.getMessage());
			LrmObjectExistResponse response = new LrmObjectExistResponse();
			response.setRequestResult("FAILED");
			return response;
		}
	}

	@Override
	public LrmObjectExistResponse assessmentExist(String projectId, String pptId) {
		log.debug("In assessmentExist() : proj={}, ppt={}", projectId, pptId);
		// // ProjectExistRequest request = new ProjectExistRequest();
		// // request.setProjectId(String.valueOf(projectId));
		String serviceUrl = properties.getProperty("lrmHost");
		serviceUrl = "http://" + serviceUrl + "/lrmrest/jaxrs/LrmAssessmentExist/" + projectId + "/" + pptId;
		LrmObjectExistResponse response = null;
		try {
			DefaultClientConfig config = new DefaultClientConfig();
			config.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);
			Client client = Client.create(config);
			WebResource webResource = client.resource(serviceUrl);
			com.sun.jersey.api.client.WebResource.Builder builder = webResource.type("application/xml");
			response = builder.get(LrmObjectExistResponse.class);
			// log.debug("Output from Server .... \n");
			// boolean output = response.isExist();
			// log.debug("Project Exists: {}", output);
		} catch (Exception e) {
			//e.printStackTrace();
			log.error("Could not connect to lrmrest: {}", e.getMessage());
			response = new LrmObjectExistResponse();
			response.setRequestResult("FAILED");
			response.setMessage(e.getMessage());
		}

		return response;
	}
}
