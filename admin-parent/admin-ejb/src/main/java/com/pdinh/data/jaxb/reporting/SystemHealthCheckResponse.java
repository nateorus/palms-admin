package com.pdinh.data.jaxb.reporting;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "healthCheck")
@XmlAccessorType(XmlAccessType.FIELD)
public class SystemHealthCheckResponse {

	@XmlElement(name = "reportServer")
	String reportServer;
	
	@XmlElement(name = "reportServerStatusChangeTime")
	Date reportServerStatusChangeTime;
	
	@XmlElement(name = "oxcart")
	String oxcart;
	
	@XmlElement(name = "oxcartServerStatusChangeTime")
	Date oxcartServerStatusChangeTime;
	
	@XmlElement(name = "tincan")
	String tincan;
	
	@XmlElement(name = "tincanServerStatusChangeTime")
	Date tincanServerStatusChangeTime;
	
	@XmlElement(name = "admin")
	String admin;
	
	@XmlElement(name = "adminServerStatusChangeTime")
	Date adminServerStatusChangeTime;
	
	@XmlElement(name = "coaching")
	String coaching;
	
	@XmlElement(name = "coachingServerStatusChangeTime")
	Date coachingServerStatusChangeTime;
	
	@XmlElement(name = "kfoReportApi")
	String kfoReportApi;
	
	@XmlElement(name = "kfoReportApiStatusChangeTime")
	Date kfoReportApiStatusChangeTime;
	
	@XmlElement(name = "pollingStatus")
	String pollingStatus;
	
	@XmlElement(name = "pollingInterval")
	long pollingInterval;
	
	@XmlElement(name = "sqsStatus")
	String sqsStatus;
	
	@XmlElement(name = "countHighPriorityQueue")
	int countHighPriorityQueue;
	
	@XmlElement(name = "countLowPriorityQueue")
	int countLowPriorityQueue;
	
	
	@XmlElement(name = "reportPollingThreads")
	int reportPollingThreads;
	
	@XmlElement(name = "messageBatchSize")
	int messageBatchSize;

	public String getReportServer() {
		return reportServer;
	}

	public void setReportServer(String reportServer) {
		this.reportServer = reportServer;
	}

	public String getOxcart() {
		return oxcart;
	}

	public void setOxcart(String oxcart) {
		this.oxcart = oxcart;
	}

	public String getTincan() {
		return tincan;
	}

	public void setTincan(String tincan) {
		this.tincan = tincan;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public String getCoaching() {
		return coaching;
	}

	public void setCoaching(String coaching) {
		this.coaching = coaching;
	}

	public String getKfoReportApi() {
		return kfoReportApi;
	}

	public void setKfoReportApi(String kfoReportApi) {
		this.kfoReportApi = kfoReportApi;
	}

	public Date getReportServerStatusChangeTime() {
		return reportServerStatusChangeTime;
	}

	public void setReportServerStatusChangeTime(Date reportServerStatusChangeTime) {
		this.reportServerStatusChangeTime = reportServerStatusChangeTime;
	}

	public Date getOxcartServerStatusChangeTime() {
		return oxcartServerStatusChangeTime;
	}

	public void setOxcartServerStatusChangeTime(Date oxcartServerStatusChangeTime) {
		this.oxcartServerStatusChangeTime = oxcartServerStatusChangeTime;
	}

	public Date getTincanServerStatusChangeTime() {
		return tincanServerStatusChangeTime;
	}

	public void setTincanServerStatusChangeTime(Date tincanServerStatusChangeTime) {
		this.tincanServerStatusChangeTime = tincanServerStatusChangeTime;
	}

	public Date getAdminServerStatusChangeTime() {
		return adminServerStatusChangeTime;
	}

	public void setAdminServerStatusChangeTime(Date adminServerStatusChangeTime) {
		this.adminServerStatusChangeTime = adminServerStatusChangeTime;
	}

	public Date getCoachingServerStatusChangeTime() {
		return coachingServerStatusChangeTime;
	}

	public void setCoachingServerStatusChangeTime(
			Date coachingServerStatusChangeTime) {
		this.coachingServerStatusChangeTime = coachingServerStatusChangeTime;
	}

	public Date getKfoReportApiStatusChangeTime() {
		return kfoReportApiStatusChangeTime;
	}

	public void setKfoReportApiStatusChangeTime(Date kfoReportApiStatusChangeTime) {
		this.kfoReportApiStatusChangeTime = kfoReportApiStatusChangeTime;
	}

	public String getPollingStatus() {
		return pollingStatus;
	}

	public void setPollingStatus(String pollingStatus) {
		this.pollingStatus = pollingStatus;
	}

	public long getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(long pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public String getSqsStatus() {
		return sqsStatus;
	}

	public void setSqsStatus(String sqsStatus) {
		this.sqsStatus = sqsStatus;
	}

	public int getCountHighPriorityQueue() {
		return countHighPriorityQueue;
	}

	public void setCountHighPriorityQueue(int countHighPriorityQueue) {
		this.countHighPriorityQueue = countHighPriorityQueue;
	}

	public int getCountLowPriorityQueue() {
		return countLowPriorityQueue;
	}

	public void setCountLowPriorityQueue(int countLowPriorityQueue) {
		this.countLowPriorityQueue = countLowPriorityQueue;
	}

	public int getReportPollingThreads() {
		return reportPollingThreads;
	}

	public void setReportPollingThreads(int reportPollingThreads) {
		this.reportPollingThreads = reportPollingThreads;
	}

	public int getMessageBatchSize() {
		return messageBatchSize;
	}

	public void setMessageBatchSize(int messageBatchSize) {
		this.messageBatchSize = messageBatchSize;
	}
}
