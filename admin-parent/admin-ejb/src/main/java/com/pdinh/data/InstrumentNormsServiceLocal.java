package com.pdinh.data;

import javax.ejb.Local;

import com.pdinh.data.instrumentnorms.CourseNormList;
import com.pdinh.data.instrumentnorms.FetchSelectedNormsList;
import com.pdinh.data.instrumentnorms.ProjectNormsDefaults;
import com.pdinh.data.instrumentnorms.SelectedNorms;
import com.pdinh.data.instrumentnorms.SetParticipantNormsList;

@Local
public interface InstrumentNormsServiceLocal {
	public CourseNormList getCourseNormList(int project_id);

	public void setProjectNormDefaults(ProjectNormsDefaults projectNormsDefaults);

	public void setParticipantNorms(SetParticipantNormsList spnl);

	public SelectedNorms fetchSelectedNormsList(FetchSelectedNormsList fetchSelectedNormsList);

}
