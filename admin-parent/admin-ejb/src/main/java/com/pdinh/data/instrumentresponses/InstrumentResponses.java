package com.pdinh.data.instrumentresponses;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="instrumentResponses")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstrumentResponses {

	@XmlElementWrapper(name="participants")	
	@XmlElement(name="participant")
	List<Participant> participants = new ArrayList<Participant>();

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	
}
