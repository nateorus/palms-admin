package com.pdinh.data;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.RosterDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.CourseAssociation;
import com.pdinh.persistence.ms.entity.CourseRoster;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.Roster;
import com.pdinh.persistence.ms.entity.User;

/**
 * Session Bean implementation class EnrollmentServiceLocalImpl
 */
@Stateless
@LocalBean
public class EnrollmentServiceLocalImpl implements EnrollmentServiceLocal {

	private static final Logger log = LoggerFactory.getLogger(EnrollmentServiceLocalImpl.class);
	@EJB
	RosterDao rosterDao;
	@EJB
	PositionDao positionDao;
	@EJB
	UserDao userDao;

	/**
	 * Default constructor.
	 */
	public EnrollmentServiceLocalImpl() {
	}

	public User enrollUserInCourse(Company company, ProjectCourse projectCourse, User user, Boolean recurseAssociations) {
		user = enrollUserInCoursePrivate(company, projectCourse.getCourse(), projectCourse.getCourseFlowType()
				.getCourseFlowTypeId(), user);
		if (recurseAssociations) {
			// Check if there are associated courses...if so enroll them in
			// those as well
			for (CourseAssociation courseAssociation : projectCourse.getCourse().getCourseAssociations()) {
				log.debug("Enroll them in {} as well.", courseAssociation.getAssociatedCourse().getAbbv());
				// JJB: Making the assumption that a course association is
				// similar to the original course...meaning same courseFlowType
				user = enrollUserInCoursePrivate(company, courseAssociation.getAssociatedCourse(), projectCourse
						.getCourseFlowType().getCourseFlowTypeId(), user);
			}
		}

		return user;
	}

	public User enrollUserInCourse(Company company, ProjectCourse projectCourse, User user) {
		return enrollUserInCourse(company, projectCourse, user, true);
	}
	/**
	 * This should be used for users that were already persistent, not for during the new user create.
	 * @param project
	 * @param user
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public User enrollUserInProjectContent(Project project, User user){
		List<ProjectCourse> pcourses = project.getProjectCourses();
		for (ProjectCourse pcourse : pcourses){
			if (pcourse.getSelected()){
				// 1. enroll user in course
				log.debug("Enrolling user in course: {}", pcourse.getCourse().getAbbv());
				user = this.enrollExistingUserInCourse(user.getCompany(), pcourse.getCourse(), pcourse.getCourseFlowType().getCourseFlowTypeId(), user);
				
			}else{
				// Do more activate/deactivate stuff
				log.debug("Project course {} is not selected in this project... do nothing?", pcourse.getCourse().getAbbv());
			}
		}

		return user;
	}

	public void activateDeactivateInstrument(Company company, ProjectCourse projectCourse, User user,
			Boolean recurseAssociations) {
		activateDeactivateInstrumentPrivate(company, projectCourse.getCourse(), projectCourse.getCourseFlowType()
				.getCourseFlowTypeId(), user);
		if (recurseAssociations) {
			// Check if there are associated courses...if so enroll them in
			// those as well
			for (CourseAssociation courseAssociation : projectCourse.getCourse().getCourseAssociations()) {
				log.debug("Activate/Deactivate {} as well.", courseAssociation.getAssociatedCourse().getAbbv());
				// JJB: Making the assumption that a course association is
				// similar to the original course...meaning same courseFlowType
				activateDeactivateInstrumentPrivate(company, courseAssociation.getAssociatedCourse(), projectCourse
						.getCourseFlowType().getCourseFlowTypeId(), user);
			}
		}
	}

	public void activateDeactivateInstrument(Company company, ProjectCourse projectCourse, User user) {
		activateDeactivateInstrument(company, projectCourse, user, true);
	}

	/**
	 * Called when user is not in any projects, but is in a company that allows
	 * all users to take all courses. Assumes courseFlowType is 1, since courses
	 * are activated by default in this case.
	 * 
	 * @param user
	 * @param company
	 * @return
	 */
	public User enrollUserInAllCourses(User user, Company company) {
		for (CourseRoster r : company.getCourseRosters()) {
			Course course = r.getCourse();
			user = enrollUserInCoursePrivate(company, course, 1, user);
		}
		return user;
	}

	private void activateDeactivateInstrumentPrivate(Company company, Course course, Integer courseFlowTypeId, User user) {
		log.debug("In activateDeactivateInstrumentPrivate");
		// if (isEnrolled(user.getUsersId(),course.getCourse())) {
		if (rosterDao.existsByUserIdAndCourseId(user.getUsersId(), course.getCourse())) {
			if (user.getUsersId() != 0) {
				for (Position position : user.getPositions()) {
					if (position.getCourse().getCourse() == course.getCourse()) {
						log.debug("Matched course {}", course.getCourse());
						// JJB: If trying to enroll them in a prework instrument
						// and the instrument is currently not activated then
						// activate it
						if (courseFlowTypeId == 1 && !position.getActivated()) {
							log.debug("Activating: {} ", course.getCourse());
							position.setActivated(true);
							positionDao.update(position);
							positionDao.getEntityManager().flush();
							// JJB: If trying to enroll them in an assessment
							// day instrument and the instrument is currently
							// activated and has not been started then
							// deactivate it
						} else if (courseFlowTypeId == 2 && position.getCompletionStatus() == 0
								&& position.getActivated()) {
							log.debug("Deactivating: {} ", course.getCourse());
							position.setActivated(false);
							positionDao.update(position);
							positionDao.getEntityManager().flush();
						}
						break;
					}
					
				}
			}
		}
	}
	
	private User enrollUserInCoursePrivate(Company company, Course course, Integer courseFlowTypeId, User user) {
		// if (!isEnrolled(user.getUsersId(),course.getCourse())) {
		if (!rosterDao.existsByUserIdAndCourseId(user.getUsersId(), course.getCourse())) {
			Position position = new Position();
			position.setCompany(company);
			position.setCourse(course);
			position.setUser(user);
			// If it is an assessment day instrument then initially mark not
			// activated
			if (courseFlowTypeId == 2) {
				position.setActivated(false);
			} else {
				position.setActivated(true);
			}
			user.getPositions().add(position);
			// JJB: If the user already exists then we are going to create the
			// record because we won't cascade
			// May be better to pass parm in to say when to do this
			if (user.getUsersId() != 0) {
				log.debug("persisting position record {}", position.getCourse());
				positionDao.create(position);
				positionDao.getEntityManager().flush();
			} else {
				log.debug("Not persisting Position record");
			}

			Roster roster = new Roster();
			roster.setCompany(company);
			roster.setCourse(course);
			roster.setUser(user);
			user.getRosters().add(roster);
			// JJB: If the user already exists then we are going to create the
			// record because we won't cascade
			// May be better to pass parm in to say when to do this
			if (user.getUsersId() != 0) {
				rosterDao.create(roster);
				rosterDao.getEntityManager().flush();
			}
		}

		return user;
	}
	
	private User enrollExistingUserInCourse(Company company, Course course, Integer courseFlowTypeId, User user) {
		log.debug("In EnrollNoPersist");
		if (!rosterDao.existsByUserIdAndCourseId(user.getUsersId(), course.getCourse())) {
			Position position = new Position();
			position.setCompany(company);
			position.setCourse(course);
			position.setUser(user);
			// If it is an assessment day instrument then initially mark not
			// activated
			if (courseFlowTypeId == 2) {
				position.setActivated(false);
			} else {
				position.setActivated(true);
			}
			positionDao.create(position);
			//user.getPositions().add(position);
			

			Roster roster = new Roster();
			roster.setCompany(company);
			roster.setCourse(course);
			roster.setUser(user);
			rosterDao.create(roster);
			//user.getRosters().add(roster);
			
		}else{
			for (Position position : user.getPositions()) {
				if (position.getCourse().getCourse() == course.getCourse()) {
					log.debug("Matched course {}", course.getCourse());
					// JJB: If trying to enroll them in a prework instrument
					// and the instrument is currently not activated then
					// activate it
					if (courseFlowTypeId == 1 && !position.getActivated()) {
						log.debug("Activating: {} ", course.getCourse());
						position.setActivated(true);
						positionDao.update(position);
						//positionDao.getEntityManager().flush();
						// JJB: If trying to enroll them in an assessment
						// day instrument and the instrument is currently
						// activated and has not been started then
						// deactivate it
					} else if (courseFlowTypeId == 2 && position.getCompletionStatus() == 0
							&& position.getActivated()) {
						log.debug("Deactivating: {} ", course.getCourse());
						position.setActivated(false);
						positionDao.update(position);
						
					}
					break;
				}
			}
		}

		return user;
	}

	public Boolean isEnrolled(Integer userId, Integer courseId) {
		log.debug("In isEnrolled");
		if (rosterDao.findByUserIdAndCourseId(userId, courseId) != null) {
			log.debug("userID: {}, in courseId: {}, already enrolled.", userId, courseId);
			return true;
		} else {
			log.debug("userID: {}, in courseId: {} NOT enrolled", userId, courseId);
			return false;
		}
	}
}
