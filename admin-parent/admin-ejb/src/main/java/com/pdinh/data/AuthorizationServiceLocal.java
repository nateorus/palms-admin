package com.pdinh.data;

import java.util.List;

import javax.ejb.Local;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;
import com.pdinh.auth.persistence.entity.AuthorizedEntity;
import com.pdinh.auth.persistence.entity.PalmsRealm;
import com.pdinh.auth.persistence.entity.PalmsSubject;
import com.pdinh.auth.persistence.entity.Role;
import com.pdinh.auth.persistence.entity.RoleContext;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;

@Local
public interface AuthorizationServiceLocal {
	
	public RoleContext grantProjectAccess(PalmsSubject subject, Project project, String accessGrantor, String roleName, String entityRule);
	
	
	public RoleContext grantCompanyAccess(String username, String realmName, Company company, String accessGrantor, String roleName, String entityRule);
	
	public RoleContext grantProjectUserAccess(PalmsSubject subject, Project project, User user, String accessGrantor, String roleName, String entityRule);
	
	public RoleContext grantUserAccess(PalmsSubject subject, Company company, User user, String accessGrantor, String roleName, String entityRule);
	
	public List<User> getAllowedUsersInCompany(RoleContext context, Integer companyId);
	
	public List<User> getAllowedUsersInProject(RoleContext context, Integer companyId, Integer projectId);
	
	public List<User> getAllowedUsersNotInProject(RoleContext context, Integer companyId, Integer projectId);
	
	public List<Company> getAllowedCompanyList(RoleContext context);
	
	public List<Project> getAllowedProjectList(RoleContext context, int companyId);
	
	public List<ExternalSubjectDataObj> getAssignedUsersForProjectUser(int projectId, int usersId, List<String> realmNames);
	
	public List<AuthorizedEntity> getEntities(List<Integer> entityIds, String entityType);
	
	public ExternalSubjectDataObj getSubjectMetadata(int subjectId);
}
