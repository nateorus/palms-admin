package com.pdinh.data;

import javax.ejb.Local;

import com.pdinh.data.instrumentresponses.InstrumentResponses;

@Local
public interface ScormServiceLocal {
	
	public InstrumentResponses getResponses(int participantId, String instrumentId);

}
