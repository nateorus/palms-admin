package com.pdinh.data.jaxb.lrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CopyLrmProjectRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CopyLrmProjectRequest {
	
	@XmlAttribute
	private String oldProjectId;
	
	@XmlAttribute
	private String newProjectId;
	
	@XmlAttribute
	private String newProjectName;
	
	@XmlAttribute
	private String createSubjectId;

	public String getOldProjectId() {
		return oldProjectId;
	}

	public void setOldProjectId(String oldProjectId) {
		this.oldProjectId = oldProjectId;
	}

	public String getNewProjectId() {
		return newProjectId;
	}

	public void setNewProjectId(String newProjectId) {
		this.newProjectId = newProjectId;
	}

	public String getNewProjectName() {
		return newProjectName;
	}

	public void setNewProjectName(String newProjectName) {
		this.newProjectName = newProjectName;
	}

	public String getCreateSubjectId() {
		return createSubjectId;
	}

	public void setCreateSubjectId(String createSubjectId) {
		this.createSubjectId = createSubjectId;
	}

}
