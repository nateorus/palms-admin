package com.pdinh.data.jaxb.lrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CopyLrmProjectResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CopyLrmProjectResponse {
	
	@XmlAttribute
	private String newLrmProjectId;
	
	@XmlAttribute
	private String requestResult;
	
	@XmlAttribute
	private String message;

	public String getNewLrmProjectId() {
		return newLrmProjectId;
	}

	public void setNewLrmProjectId(String newLrmProjectId) {
		this.newLrmProjectId = newLrmProjectId;
	}

	public String getRequestResult() {
		return requestResult;
	}

	public void setRequestResult(String requestResult) {
		this.requestResult = requestResult;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
