package com.pdinh.data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.CompanyPasswordPolicyDao;
import com.pdinh.data.ms.dao.PasswordPolicyDao;
import com.pdinh.data.ms.dao.PasswordPolicyUserStatusDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.CompanyPasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordHistory;
import com.pdinh.persistence.ms.entity.PasswordPolicy;
import com.pdinh.persistence.ms.entity.PasswordPolicyUserStatus;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.PasswordStatusResultObj;
import com.pdinh.presentation.domain.ResultObj;
import com.pdinh.presentation.domain.ValidatePasswordResultObj;
@Stateless
@LocalBean
public class PasswordServiceLocalImpl implements PasswordServiceLocal {
	private static final Logger log = LoggerFactory.getLogger(PasswordServiceLocalImpl.class);
	private static final String LOWERCASE_REGEX = "^(?=.*[a-z]).+$";
	private static final String UPPERCASE_REGEX = "^(?=.*[A-Z]).+$";
	private static final String NUMERIC_REGEX = "^(?=.*\\d).+$";
	private static final String SPECIAL_REGEX = "^(?=.*[@#$%!&*~^()_+=-`{}|?><,.:;]).+$";
	private static final String LENGTH_ERROR_CODE = "1";
	private static final String LOWERCASE_ERROR_CODE = "2";
	private static final String UPPERCASE_ERROR_CODE = "3";
	private static final String NUMERIC_ERROR_CODE = "4";
	private static final String SPECIAL_ERROR_CODE = "5";
	private static final String CUSTOM_ERROR_CODE = "6";
	private static final String ADMIN_POLICY_CONSTANT = "ADMIN_DEFAULT";
	private static final String PARTICIPANT_POLICY_CONSTANT = "PARTICIPANT_DEFAULT";
	
	@EJB
	private PasswordPolicyUserStatusDao pwdPolicyUserStatusDao;
	
	@EJB
	private PasswordPolicyDao passwordPolicyDao;
	
	@EJB
	private CompanyPasswordPolicyDao companyPolicyDao;
	
	@EJB
	UserDao userDao;
	
	
	
	@Override
	public ValidatePasswordResultObj validatePassword(String password, PasswordPolicy policy) {
		ValidatePasswordResultObj result = new ValidatePasswordResultObj();
		result.setSuccess(true);
		List<ErrorObj> errors = new ArrayList<ErrorObj>();
		String status = "";
		int minLength = policy.getMinimumLength();
		int maxLength = policy.getMaximumLength();
		
		if (isValid(password, "(.{" + minLength + "," + maxLength + "})")){
			log.trace("Password meets length requirments");
			
		}else{
			log.trace("Password failed length requirments");
			
			errors.add(new ErrorObj(this.LENGTH_ERROR_CODE, "Failed policy password length requirment"));
			result.setSuccess(false);
		}
		
		if (policy.isRequireLowercase()){
			if (isValid(password, this.LOWERCASE_REGEX)){
				log.trace("Password meets lowercase character requirments");
				
			}else{
				log.trace("Password failed lowercase character requirments");
				
				errors.add(new ErrorObj(this.LOWERCASE_ERROR_CODE, "Failed policy lowercase character requirment"));
				result.setSuccess(false);
			}
		}
		
		if (policy.isRequireUppercase()){
			if (isValid(password, this.UPPERCASE_REGEX)){
				log.trace("Password meets uppercase character requirments");
				
			}else{
				log.trace("Password failed uppercase character requirments");
				
				errors.add(new ErrorObj(this.UPPERCASE_ERROR_CODE, "Failed policy uppercase character requirment"));
				result.setSuccess(false);
			}
		}
		
		if (policy.isRequireNumeric()){
			if (isValid(password, this.NUMERIC_REGEX)){
				log.trace("Password meets numeric character requirments");
				
			}else{
				log.trace("Password failed numeric character requirments");
				
				errors.add(new ErrorObj(this.NUMERIC_ERROR_CODE, "Failed policy numeric character requirment"));
				result.setSuccess(false);
			}
		}
		
		if (policy.isRequireSpecial()){
			if (isValid(password, this.SPECIAL_REGEX)){
				log.trace("Password meets special character requirments");
				
			}else{
				log.trace("Password failed special character requirments");
				
				errors.add(new ErrorObj(this.SPECIAL_ERROR_CODE, "Failed policy special character requirment"));
				result.setSuccess(false);
			}
		}
		
		if (!(policy.getValidationCustomRegex() == null) && !(policy.getValidationCustomRegex().length() == 0)){
			if (isValid(password, policy.getValidationCustomRegex())){
				log.trace("Password meets custom requirments");
				
			}else{
				log.trace("Password failed custom requirments");
				
				errors.add(new ErrorObj(this.CUSTOM_ERROR_CODE, "Failed policy custom requirment"));
				result.setSuccess(false);
			}
		}
		if (result.getSuccess()){
			result.setStatus("Passed All Policy Requirements");
		}else{
			result.setStatus("Password Failed Validation");
			result.setErrors(errors);
		}
		
		return result;
	}
	
	public boolean changePasswordUpdateHistory(User user, String password){
		PasswordPolicyUserStatus policystatus = user.getPasswordPolicyStatus();
		if (policystatus == null){
			policystatus = setupUserPolicyStatus(user);
		}
		List<PasswordHistory> historylist = policystatus.getPasswordHistory();
		if (historylist == null){
			historylist = new ArrayList<PasswordHistory>();
		}
		PasswordPolicy policy = policystatus.getPolicy();
		byte[] newhashedpassword = generateHashedPassword(password,  user
				.getRowguid().substring(0, 10).toUpperCase());
		for (PasswordHistory history: historylist){
			if (newhashedpassword == history.getPasswordHash()){
				log.debug("New password found in history.  return false");
				return false;
			}
		}
		log.debug("New password not found in history");
		if (policy.getMaxPasswordsInHistory() > 0){
			if (policy.getMaxPasswordsInHistory() == historylist.size()){
				log.debug("Max passwords in history reached.  Remove oldest password");
				Timestamp oldest = new Timestamp(new Date().getTime());
				int historyid = 0;
				for(PasswordHistory history: historylist){
					if (oldest.after(history.getDateCreated())){
						historyid = history.getPasswordHistoryId();
					}
				}
				for (PasswordHistory history: historylist){
					if (historyid == history.getPasswordHistoryId()){
						historylist.remove(history);
						log.debug("removed oldest password from history: {}", historyid);
						break;
					}
				}
				
			}else{
				log.debug("Password history still has room for more.");
			}
			//now find active history and set to inactive
			for (PasswordHistory history: historylist){
				if (history.isActive()){
					history.setActive(false);
					break;
				}
			}
			//now add new history
			PasswordHistory newHistory = new PasswordHistory();
			newHistory.setActive(true);
			newHistory.setDateCreated(new Timestamp(new Date().getTime()));
			newHistory.setPasswordHash(newhashedpassword);
			newHistory.setUserStatus(policystatus);
			historylist.add(newHistory);
			policystatus.setPasswordHistory(historylist);
			user.setPasswordPolicyStatus(policystatus);
			user.setHashedpassword(newhashedpassword);
			userDao.update(user);
			return true;
		}else{
			log.debug("Policy does not require password history storage");
		}
		return false;
	}
	

	
	public byte[] generateHashedPassword(String password, String salt) {
		byte[] hashbyte = (password + salt).getBytes();
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update(hashbyte);
			return digest.digest();

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	public static boolean isValid(String password, String regex){
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(password);
		
		return m.matches();
	}
	
	@Override
	public PasswordPolicyUserStatus setupUserPolicyStatus(User user) {
		log.debug("in setupUserPolicyStatus()");
		
		//Company co = user.getCompany();
		PasswordPolicyUserStatus pwStatus = new PasswordPolicyUserStatus();
		pwStatus.setAboutToExpire(false);
		pwStatus.setChangeRequired(true);
		pwStatus.setFailedLogonCount(0);
		pwStatus.setLocked(false);
		pwStatus.setNotificationSent(false);
		pwStatus.setUser(user);
		List<CompanyPasswordPolicy> policies = companyPolicyDao.findByCompanyId(user.getCompany().getCompanyId());
		log.debug("Found {} policies for company {}", policies.size(), user.getCompany().getCoName());
		for (CompanyPasswordPolicy policy:policies){
			if (policy.getUserType() == user.getType()){
				log.debug("found matching policy for user type {}", user.getType());
				pwStatus.setPolicy(policy.getPasswordPolicy());
				break;
			}
		}
		if (pwStatus.getPolicy() == null){
			log.debug("failed to find policy for user {}, type {}", user.getUsername(), user.getType());
		}
		PasswordHistory history = new PasswordHistory();
		history.setActive(true);
		history.setPasswordHash(user.getHashedpassword());
		history.setDateCreated(new Timestamp(new Date().getTime()));
		history.setUserStatus(pwStatus);
		List<PasswordHistory> historyList = new ArrayList<PasswordHistory>();
		historyList.add(history);
		pwStatus.setPasswordHistory(historyList);
		
		pwStatus = pwdPolicyUserStatusDao.create(pwStatus);
		
		return pwStatus;
	}
	
	@Override
	public void setupCompanyPolicies(Company company){
		PasswordPolicy adminPolicy = passwordPolicyDao.findTemplatePolicyByConstant(ADMIN_POLICY_CONSTANT);
		PasswordPolicy partPolicy = passwordPolicyDao.findTemplatePolicyByConstant(PARTICIPANT_POLICY_CONSTANT);
		//List<CompanyPasswordPolicy> coPolicies = new ArrayList<CompanyPasswordPolicy>();
		CompanyPasswordPolicy type1policy = new CompanyPasswordPolicy();
		type1policy.setCompany(company);
		type1policy.setUserType(1);
		type1policy.setPasswordPolicy(partPolicy);
		//coPolicies.add(type1policy);
		
		CompanyPasswordPolicy type2policy = new CompanyPasswordPolicy();
		type2policy.setCompany(company);
		type2policy.setUserType(2);
		type2policy.setPasswordPolicy(partPolicy);
		//coPolicies.add(type2policy);
		
		CompanyPasswordPolicy type3policy = new CompanyPasswordPolicy();
		type3policy.setCompany(company);
		type3policy.setUserType(3);
		type3policy.setPasswordPolicy(adminPolicy);
		//coPolicies.add(type3policy);
		
		CompanyPasswordPolicy type4policy = new CompanyPasswordPolicy();
		type4policy.setCompany(company);
		type4policy.setUserType(4);
		type4policy.setPasswordPolicy(adminPolicy);
		//coPolicies.add(type4policy);
		
		//company.setPasswordPolicies(coPolicies);
		
		companyPolicyDao.create(type1policy);
		companyPolicyDao.create(type2policy);
		companyPolicyDao.create(type3policy);
		companyPolicyDao.create(type4policy);
		
		log.debug("finished setting up default password policies for company {}", company.getCompanyId());
	}		

	@Override
	public PasswordPolicyUserStatus updatePasswordPolicyUserStatus(User user, boolean success) {
		//what to do here?
		return null;
	}
	/**
	 * Evaluates the user's password policy status record and returns true if the user is locked out
	 * and should not be granted access to the system.  This method assumes it is being called in the context of 
	 * an authentication attempt and it updates the password policy status record accordingly.
	 * 
	 * @param user The user attempting to authenticate
	 * @param success Whether or not the authentication of the username/password was successful
	 * @return true if the user is locked out (previously or as a result of this authentication attempt).
	 * false if the user is not locked out.
	 */

	@Override
	public PasswordStatusResultObj doLockoutUpdateEval(User user, boolean success) {
		PasswordStatusResultObj result = new PasswordStatusResultObj();
		PasswordPolicyUserStatus status = user.getPasswordPolicyStatus();
		PasswordPolicy policy = status.getPolicy();
		List<PasswordHistory> history = status.getPasswordHistory();
		
		Timestamp now = new Timestamp(new Date().getTime());
		Timestamp lastLoginTmstmp = status.getLogonAttemptTimestamp();
		
		if (lastLoginTmstmp == null){
			status.setLogonAttemptTimestamp(now);
		}
		long lastLogin = status.getLogonAttemptTimestamp().getTime();
		
		long millisAllowed = policy.getUnsuccessfulAttemptsPeriodMinutes() * 60 * 1000;
		Timestamp timeToBeat = new Timestamp(lastLogin + millisAllowed);
		//Successfully authenticated user/pw
		if (success){
			
			if (status.isLocked() && now.before(timeToBeat)){
				//no need to update status in this case
				result.setLocked(true);
				result.setSuccess(true);
				result.setStatus(result.STATUS_ACCOUNT_LOCKED);
				return result;
			}else {
				status.setLogonAttemptTimestamp(now);
				status.setFailedLogonCount(0);
				status.setLocked(false);
						
			}
		//Authentication failed
		} else {
			
			
			if (now.before(timeToBeat)){
				//do not set logon attempt timestamp here
				status.setFailedLogonCount(status.getFailedLogonCount() + 1);
			}else{
				status.setFailedLogonCount(1);
				status.setLogonAttemptTimestamp(now);
			}
			
			if (status.getFailedLogonCount() >= policy.getUnsuccessfulAttempts()){
				status.setLocked(true);
				result.setLocked(true);
				result.setStatus(result.STATUS_ACCOUNT_LOCKED);
			}
		}
		
		pwdPolicyUserStatusDao.update(status);
		//return status.isLocked();
		return result;
	}

	
	

}
