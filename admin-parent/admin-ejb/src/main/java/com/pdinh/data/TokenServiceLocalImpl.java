package com.pdinh.data;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ContentTokenDao;
import com.pdinh.persistence.ms.entity.ContentToken;

/**
 * Session Bean implementation class TokenServiceLocalImpl
 */
@Stateless
@LocalBean
public class TokenServiceLocalImpl implements TokenServiceLocal{
	private static final Logger log = LoggerFactory.getLogger(TokenServiceLocalImpl.class);
	@EJB
	private ContentTokenDao contentTokenDao;
	
	

    /**
     * Default constructor. 
     */
    public TokenServiceLocalImpl() {
        // TODO Auto-generated constructor stub
    }
    //For Email verification, where no userid is available, pass 0 for users_id, email as contentId
	@Override
	public ContentToken generateToken(int users_id, int company_id,
			String contentId) {
		ContentToken newToken = new ContentToken();
		newToken.setCompany_id(company_id);
		newToken.setUsers_id(users_id);
		newToken.setContent_id(contentId);
		newToken.setToken(UUID.randomUUID().toString());
		Random random = new Random();
		newToken.setVerifyToken(random.nextInt(999999));
		if (!(users_id == 0)){
			contentTokenDao.deleteAllUserContentTokens(users_id, contentId);
		}
		Timestamp tstmp = new Timestamp(new Date().getTime());
		log.debug("Token Timestamp: {}", tstmp);
		log.debug("Timestamp millis: {}", tstmp.getTime());
		newToken.setTimestamp(tstmp);
		newToken = contentTokenDao.create(newToken);

		return newToken;
	}
	
	@Override
	public boolean verifyToken(int tokenId, int verifyToken) {
		ContentToken token = contentTokenDao.findById(tokenId);
		log.debug("Token timestamp millis: {}", token.getTimestamp().getTime());
		if (token == null){
			return false;
		}else{
			log.debug("Token Id: {}", tokenId);
			log.debug("Token Timestamp: {}", token.getTimestamp());
			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(token.getTimestamp().getTime());
			cal.add(Calendar.MINUTE, VERIFY_TOKEN_VALID_MINUTES);
			
			Calendar calNow = new GregorianCalendar();
			
			if (token.getVerifyToken() == verifyToken){
				//verify token is a match
				if (cal.getTime().after(new Date())){
					//token has not expired
					return true;
				}else{
					log.debug("token expired: now: {}, token plus 10: {}", new Date(), cal.getTime());
				}
			}
		}
		return false;
	}

}
