package com.pdinh.data.jaxb.reporting;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ReportStreamRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenerateReportRequest {
	
	@XmlElement(name = "reportRequestId")
	private int reportRequestId;
	
	@XmlElement(name = "reportRequestKey", required = true)
	private String reportRequestKey;

	public int getReportRequestId() {
		return reportRequestId;
	}

	public void setReportRequestId(int reportRequestId) {
		this.reportRequestId = reportRequestId;
	}

	public String getReportRequestKey() {
		return reportRequestKey;
	}

	public void setReportRequestKey(String reportRequestKey) {
		this.reportRequestKey = reportRequestKey;
	}



}
