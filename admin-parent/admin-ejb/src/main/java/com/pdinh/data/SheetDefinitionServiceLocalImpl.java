package com.pdinh.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.kf.dochandler.excel.DateColumn;
import com.kf.dochandler.excel.DocumentValidation;
import com.kf.dochandler.excel.EmailColumn;
import com.kf.dochandler.excel.ExcelColumn;
import com.kf.dochandler.excel.IntColumn;
import com.kf.dochandler.excel.LogonIdColumn;
import com.kf.dochandler.excel.NameColumn;
import com.kf.dochandler.excel.PasswordColumn;
import com.kf.dochandler.excel.PhoneColumn;
import com.kf.dochandler.excel.SheetDefinition;
import com.kf.dochandler.excel.StringColumn;
import com.kf.dochandler.excel.exception.ExcelConstants;
import com.kf.dochandler.excel.exception.ExcelError;
import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.CustomFieldListValueDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.vo.FieldAliasVo;
import com.kf.uffda.vo.FieldTransformationVo;
import com.kf.uffda.vo.FieldValueMapSetVo;
import com.kf.uffda.vo.FieldValueMapVo;
import com.pdinh.bulkupload.GenerateTemplateServiceImpl;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.presentation.helper.ParticipantValidator;

@Stateless
@LocalBean
public class SheetDefinitionServiceLocalImpl implements SheetDefinitionServiceLocal {

	private static final Set<String> PLAIN_STRING_COLUMNS = new HashSet<String>();

	@EJB
	private LanguageSingleton languageSingleton;

	static {
		PLAIN_STRING_COLUMNS.add(ImportUser.OPTIONAL_1);
		PLAIN_STRING_COLUMNS.add(ImportUser.OPTIONAL_2);
		PLAIN_STRING_COLUMNS.add(ImportUser.OPTIONAL_3);
		PLAIN_STRING_COLUMNS.add(ImportUser.SUPERVISOR_EMAIL);
	}

	@Override
	public SheetDefinition getSheetDefinitionFor(List<FieldDto> persistentStandardFields,
			List<FieldDto> persistentCustomFields, String sheetName, List<String> columnNames, int maxRecords,
			boolean usesTemplate, DocumentValidation validation) {
		List<FieldDto> standardFields = new ArrayList<FieldDto>();
		List<FieldDto> customFields = new ArrayList<FieldDto>();
		List<FieldDto> missingFields = new ArrayList<FieldDto>();

		loadAliases(persistentStandardFields, persistentCustomFields, standardFields, customFields, missingFields,
				columnNames, usesTemplate);

		if (!missingFields.isEmpty()) {
			boolean missingRequiredField = false;
			for (FieldDto field : missingFields) {
				// We may want to consider separate errors, with separate
				// severity levels, for a missing REQUIRED column versus
				// a missing OPTIONAL column. As of now, we only have one
				// MISSING_COLUMN error in the database, so we treat them
				// all the same.
				ExcelError error = new ExcelError(ExcelConstants.SHEET_ERROR,
						field.isRequired() ? ExcelConstants.MISSING_COLUMN : ExcelConstants.MISSING_OPTIONAL_COLUMN,
						field.isRequired() ? ExcelConstants.SEVERE : ExcelConstants.WARNING);
				error.setDetail("Sheet is missing " + (field.isRequired() ? "required" : "optional") + " field "
						+ field.getSelectedAlias());
				validation.addSheetError(sheetName, error);
				if (field.isRequired()) {
					missingRequiredField = true;
				}
			}

			if (missingRequiredField) {
				return null;
			}
		}

		SheetDefinition retVal = new SheetDefinition(sheetName);
		retVal.setMaxRecords(maxRecords);
		addStandardFields(retVal, standardFields);
		addCustomFields(retVal, customFields);
		return retVal;
	}

	private void loadAliases(List<FieldDto> persistentStandardFields, List<FieldDto> persistentCustomFields,
			List<FieldDto> standardFields, List<FieldDto> customFields, List<FieldDto> missingFields,
			List<String> columnNames, boolean usesTemplate) {

		findColumnForField(persistentStandardFields, standardFields, missingFields, columnNames, usesTemplate);
		findColumnForField(persistentCustomFields, customFields, missingFields, columnNames, usesTemplate);
	}

	private void findColumnForField(List<FieldDto> originList, List<FieldDto> targetList, List<FieldDto> missingList,
			List<String> columnNames, boolean usesTemplate) {
		boolean matchFound;
		String columnName = null;

		for (FieldDto field : originList) {
			matchFound = false;
			String fieldName = field.getImportFieldName();
			String fieldCode = field.getCode();

			if ((columnName = findAndRemoveColumnName(columnNames, fieldName)) != null) {
				field.setSelectedAlias(columnName);
				matchFound = true;

			} else if ((columnName = findAndRemoveColumnName(columnNames, fieldCode)) != null) {
				field.setSelectedAlias(columnName);
				matchFound = true;

			} else {
				// Import files originating from a template should never be
				// using field aliases (right?). Only extracts.
				for (FieldAliasVo alias : field.getFieldNameAliases()) {
					if ((columnName = findAndRemoveColumnName(columnNames, alias.getAliasName())) != null) {
						field.setSelectedAlias(columnName);
						matchFound = true;
						break;
					}
				}
			}

			if (matchFound) {
				targetList.add(field);
			} else {
				field.setSelectedAlias(fieldName);
				missingList.add(field);
			}
		}
	}

	private String findAndRemoveColumnName(List<String> columnNames, String name) {
		for (String columnName : columnNames) {
			if (columnName.equalsIgnoreCase(name)) {
				columnNames.remove(columnName);
				return columnName;
			}
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public void addStandardFields(SheetDefinition sheetDefinition, List<FieldDto> standardFields) {
		ExcelColumn column = null;

		for (FieldDto field : standardFields) {

			if (field.getImportFieldName().equals(ImportUser.FIRST_NAME)) {
				NameColumn nameColumn = new NameColumn(field.getSelectedAlias(), field.getImportFieldName(), true);
				nameColumn.setInvalidSubstrings(ParticipantValidator.NAME_CHAR_BLACKLIST);
				nameColumn.setMaxLength(50);
				column = nameColumn;

				sheetDefinition.addDefiningBlankColumn(field.getImportFieldName());

			} else if (field.getImportFieldName().equals(ImportUser.LAST_NAME)) {
				NameColumn nameColumn = new NameColumn(field.getSelectedAlias(), field.getImportFieldName(), true);
				nameColumn.setInvalidSubstrings(ParticipantValidator.NAME_CHAR_BLACKLIST);
				nameColumn.setMaxLength(50);
				column = nameColumn;

				sheetDefinition.addDefiningBlankColumn(field.getImportFieldName());

			} else if (field.getImportFieldName().equals(ImportUser.SUPERVISOR_NAME)) {
				NameColumn nameColumn = new NameColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				// TODO: Does this column need to have the invalid character
				// filter applied to it?
				nameColumn.setInvalidSubstrings(ParticipantValidator.NAME_CHAR_BLACKLIST);
				nameColumn.setMaxLength(100);
				column = nameColumn;

			} else if (PLAIN_STRING_COLUMNS.contains(field.getImportFieldName())) {
				StringColumn stringColumn = new StringColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				stringColumn.setMaxLength(100);
				column = stringColumn;

			} else if (field.getImportFieldName().equals(ImportUser.BUSINESS_PHONE)) {
				PhoneColumn phoneColumn = new PhoneColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				phoneColumn.setMaxLength(50);
				column = phoneColumn;

			} else if (field.getImportFieldName().equals(ImportUser.EMAIL_ADDRESS)) {
				EmailColumn emailColumn = new EmailColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				emailColumn.setMaxLength(400);
				column = emailColumn;

				sheetDefinition.addDefiningBlankColumn(field.getImportFieldName());

			} else if (field.getImportFieldName().equals(ImportUser.LANGUAGE)) {
				column = new StringColumn(field.getSelectedAlias(), field.getImportFieldName(), field.isRequired());

				// TODO: Value-mapping for client extracts, where the list of
				// valid languages likely differs from
				// the KF internal list of valid languages.
				for (Language language : languageSingleton.getLanguageEntities()) {
					column.addValidValue(language.getName());
				}

			} else if (field.getImportFieldName().equals(ImportUser.LOGON_ID)) {
				LogonIdColumn logonIdColumn = new LogonIdColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				logonIdColumn.setMaxLength(50);
				column = logonIdColumn;

			} else if (field.getImportFieldName().equals(ImportUser.PASSWORD)) {
				PasswordColumn passwordColumn = new PasswordColumn(field.getSelectedAlias(),
						field.getImportFieldName(), field.isRequired());
				passwordColumn.setMaxLength(50);
				column = passwordColumn;

			} else if (field.getImportFieldName().equals(ImportUser.UPDATE_LOGON_FIELDS)) {
				StringColumn stringColumn = new StringColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				stringColumn.setMaxLength(10);
				column = stringColumn;
			}

			// Get the transformations
			for (FieldTransformationVo transformation : field.getFieldTransformations()) {
				column.addTransformationType(transformation.getFieldTransformationType().getCode());

				// Add the value map values
				FieldValueMapSetVo fieldValueMapSet = transformation.getFieldValueMapSet();
				if (fieldValueMapSet != null && fieldValueMapSet.hasFieldValueMapValues()) {
					for (FieldValueMapVo valueMap : fieldValueMapSet.getFieldValueMapValues()) {
						column.addValueMapValue(valueMap.getFromValue(), valueMap.getToValue());
					}
				}
			}

			// Add the column to the sheet
			sheetDefinition.addColumn(column);
		}
	}

	@SuppressWarnings("rawtypes")
	public void addCustomFields(SheetDefinition sheetDefinition, List<FieldDto> customFields) {
		ExcelColumn column = null;

		for (FieldDto field : customFields) {
			CustomFieldDto customField = (CustomFieldDto) field;

			if (customField.isCharValue()) {
				StringColumn stringColumn = new StringColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				if (customField.getMinimum() > 0) {
					stringColumn.setMinLength(customField.getMinimum());
				}
				stringColumn.setMaxLength(customField.getLength());
				column = stringColumn;

			} else if (customField.isDateValue()) {
				column = new DateColumn(field.getSelectedAlias(), field.getImportFieldName(), field.isRequired());

			} else if (customField.isIntValue()) {
				IntColumn intColumn = new IntColumn(field.getSelectedAlias(), field.getImportFieldName(),
						field.isRequired());
				intColumn.setMinValue(customField.getMinimum());
				intColumn.setMaxValue(customField.getMaximum());
				column = intColumn;

			} else if (customField.isListValue()) {
				column = new StringColumn(field.getSelectedAlias(), field.getImportFieldName(), field.isRequired());
				for (CustomFieldListValueDto valueDto : customField.getListValues()) {
					String label = valueDto.getLabel(FieldDto.LANGUAGE_ENGLISH);
					System.out.println("Adding value choice '" + label + "' to custom field column "
							+ field.getSelectedAlias());
					column.addValidValue(label);
				}
				column.setBlankValue(GenerateTemplateServiceImpl.DEFAULT_SELECTION_TEXT);
			}

			// Get the transformations
			for (FieldTransformationVo transformation : field.getFieldTransformations()) {
				column.addTransformationType(transformation.getFieldTransformationType().getCode());

				// Add the value map values
				FieldValueMapSetVo fieldValueMapSet = transformation.getFieldValueMapSet();
				if (fieldValueMapSet != null && fieldValueMapSet.hasFieldValueMapValues()) {
					for (FieldValueMapVo valueMap : fieldValueMapSet.getFieldValueMapValues()) {
						column.addValueMapValue(valueMap.getFromValue(), valueMap.getToValue());
					}
				}
			}

			// Add the column to the sheet
			sheetDefinition.addColumn(column);
		}
	}
}
