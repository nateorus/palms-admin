package com.pdinh.data.instrumentnorms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "normGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class NormGroup {
	@XmlAttribute
	private int id;
	@XmlAttribute
	private boolean isSelected;
	@XmlAttribute
	private String type;
	@XmlValue
	private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getIsSelected() {
		return isSelected();
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "NormGroup [id=" + id + ", isSelected=" + isSelected + ", type=" + type + ", value=" + value + "]";
	}

}
