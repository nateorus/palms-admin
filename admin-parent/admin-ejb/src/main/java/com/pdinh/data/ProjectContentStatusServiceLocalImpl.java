package com.pdinh.data;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectUser;

@Stateless
@LocalBean
public class ProjectContentStatusServiceLocalImpl implements
		ProjectContentStatusServiceLocal {
	
	@EJB
	private ProjectUserDao projectUserDao;

	@Override
	public ProjectUser setProjectContentStatus(ProjectUser projectUser) {
		
		List<ProjectCourse> courses = projectUser.getProject().getProjectCourses();
		List<Position> positions = projectUser.getUser().getPositions();
		int statusTally = 0;
		int enabledCourses = 0;
		for (ProjectCourse course: courses){
			//all project content
			if (course.getSelected()){
				//only selected content
				for (Position position: positions){
					if (course.getCourse().getCourse() == position.getCourse().getCourse()){
						//match project content to user position record
						enabledCourses += 1;
						statusTally = statusTally + position.getCompletionStatus();
						break;
					}
				}
			}
		}
		if (statusTally == (enabledCourses * 2)){
			//complete
			projectUser.setProjectContentCompletionStatus(2);
		}else if (statusTally == 0){
			//not started
			projectUser.setProjectContentCompletionStatus(0);
		}else {
			//in progress
			projectUser.setProjectContentCompletionStatus(1);
		}
		projectUserDao.update(projectUser);
		
		return projectUser;
	}

}
