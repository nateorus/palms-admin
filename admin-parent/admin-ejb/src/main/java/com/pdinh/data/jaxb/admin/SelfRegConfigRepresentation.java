package com.pdinh.data.jaxb.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.persistence.ms.entity.SelfRegConfig;
import com.pdinh.persistence.ms.entity.SelfRegListItem;
import com.pdinh.persistence.ms.entity.SelfRegListItemType;

@XmlRootElement(name = "selfRegConfig")
@XmlAccessorType(XmlAccessType.FIELD)
public class SelfRegConfigRepresentation implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name = "selfRegConfigId")
	private int selfRegConfigId;
	
	@XmlAttribute(name = "projectId")
	private int projectId;
	
	@XmlAttribute(name = "companyId")
	private int companyId;
	
	@XmlAttribute(name = "scheduledEmailDefId")
	private int scheduledEmailDefId;
	
	@XmlAttribute(name = "enabled")
	private boolean enabled;
	
	@XmlAttribute(name = "guid")
	private String guid;
	
	@XmlAttribute(name = "registrationLimit")
	private int registrationLimit;
	
	@XmlAttribute(name = "registrationCount")
	private int registrationCount;
	
	@XmlAttribute(name = "selfRegistrationUrl")
	private String selfRegistrationUrl;
	
	@XmlAttribute(name = "lastRegistrationDate")
	private Date lastRegistrationDate;
	
	@XmlElementWrapper(name = "allowedEmailDomains")
	@XmlElement(name = "allowedEmailDomain")
	private List<SelfRegListItemRepresentation> allowedEmailDomains;
	
	@XmlElementWrapper(name = "registeredUsers")
	@XmlElement(name = "registeredUser")
	private List<SelfRegListItemRepresentation> registeredUsers;
	
	public SelfRegConfigRepresentation(SelfRegConfig config, String url){
		this.setCompanyId(config.getProject().getCompany().getCompanyId());
		this.setProjectId(config.getProject().getProjectId());
		this.setSelfRegConfigId(config.getSelfRegConfigId());
		this.setEnabled(config.isEnabled());
		this.setGuid(config.getGuid().toString());
		if (config.getScheduledEmailDef() != null){
			this.setScheduledEmailDefId(config.getScheduledEmailDef().getId());
		}
		this.setLastRegistrationDate(config.getLastRegistration());
		this.setRegistrationCount(config.getRegistrationCount());
		this.setRegistrationLimit(config.getRegistrationLimit());
		this.setSelfRegistrationUrl(url);
		this.setAllowedEmailDomains(new ArrayList<SelfRegListItemRepresentation>());
		this.setRegisteredUsers(new ArrayList<SelfRegListItemRepresentation>());
		for (SelfRegListItem item : config.getAllListItems()){
			if (item.isActive()){
				if (item.getItemType().getItemTypeCode().equals(SelfRegListItemType.ALLOWED_DOMAIN_TYPE_CODE)){
					this.getAllowedEmailDomains().add(new SelfRegListItemRepresentation(item.getSelfRegListItemId(), 
							item.getItemText(), SelfRegListItemType.ALLOWED_DOMAIN_TYPE_CODE, item.getLangCode()));
					
				}
				if (item.getItemType().getItemTypeCode().equals(SelfRegListItemType.REGISTERED_USER_ID_TYPE_CODE)){
					this.getRegisteredUsers().add(new SelfRegListItemRepresentation(item.getSelfRegListItemId(), 
							item.getItemText(), SelfRegListItemType.REGISTERED_USER_ID_TYPE_CODE, item.getLangCode()));
				}
			}
		}
		
	}
	
	public SelfRegConfigRepresentation(){
		
	}
	
	public String getDomainsAsString(){
		String domainString = "";
		if (!(this.getAllowedEmailDomains() == null)){
			for (SelfRegListItemRepresentation item : this.allowedEmailDomains){
				domainString = domainString + item.getValue() + ", ";
			}
		}
		return domainString;
	}

	public int getSelfRegConfigId() {
		return selfRegConfigId;
	}

	public void setSelfRegConfigId(int selfRegConfigId) {
		this.selfRegConfigId = selfRegConfigId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getRegistrationLimit() {
		return registrationLimit;
	}

	public void setRegistrationLimit(int registrationLimit) {
		this.registrationLimit = registrationLimit;
	}

	public int getRegistrationCount() {
		return registrationCount;
	}

	public void setRegistrationCount(int registrationCount) {
		this.registrationCount = registrationCount;
	}

	public String getSelfRegistrationUrl() {
		return selfRegistrationUrl;
	}

	public void setSelfRegistrationUrl(String selfRegistrationUrl) {
		this.selfRegistrationUrl = selfRegistrationUrl;
	}

	public Date getLastRegistrationDate() {
		return lastRegistrationDate;
	}

	public void setLastRegistrationDate(Date lastRegistrationDate) {
		this.lastRegistrationDate = lastRegistrationDate;
	}

	public List<SelfRegListItemRepresentation> getAllowedEmailDomains() {
		return allowedEmailDomains;
	}

	public void setAllowedEmailDomains(
			List<SelfRegListItemRepresentation> allowedEmailDomains) {
		this.allowedEmailDomains = allowedEmailDomains;
	}

	public List<SelfRegListItemRepresentation> getRegisteredUsers() {
		return registeredUsers;
	}

	public void setRegisteredUsers(
			List<SelfRegListItemRepresentation> registeredUsers) {
		this.registeredUsers = registeredUsers;
	}

	public int getScheduledEmailDefId() {
		return scheduledEmailDefId;
	}

	public void setScheduledEmailDefId(int scheduledEmailDefId) {
		this.scheduledEmailDefId = scheduledEmailDefId;
	}
	
	
	

}
