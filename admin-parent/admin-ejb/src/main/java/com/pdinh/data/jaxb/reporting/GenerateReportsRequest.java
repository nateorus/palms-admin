package com.pdinh.data.jaxb.reporting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ReportsRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenerateReportsRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElementWrapper(name="participants")
	@XmlElement(name = "participant")
	private List<ReportParticipantRepresentation> participants;

	@XmlElementWrapper(name="reportTypes")
	@XmlElement(name = "reportType")
	private List<ReportRepresentation> reportTypeList;
	
	@XmlElement(name = "subject_id")
	private int subjectId;
	
	@XmlElement(name = "principal")
	private String principal;
	
	@XmlElement(name = "priority")
	private int priority;
	
	@XmlElement(name = "reportRequestCount")
	private int reportRequestCount;
	
	@XmlElement(name = "participantCount")
	private int participantCount;
	
	@XmlElement(name = "requestTime")
	private Date requestTime;
	
	@XmlElement(name = "requestId")
	private int requestId;
	
	@XmlElement(name = "status")
	private String status;
	
	@XmlElement(name = "requestName")
	private String requestName;

	public List<ReportParticipantRepresentation> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ReportParticipantRepresentation> participants) {
		this.participants = participants;
	}

	public List<ReportRepresentation> getReportTypeList() {
		return reportTypeList;
	}

	public void setReportTypeList(List<ReportRepresentation> reportTypeList) {
		this.reportTypeList = reportTypeList;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getReportRequestCount() {
		return reportRequestCount;
	}

	public void setReportRequestCount(int reportRequestCount) {
		this.reportRequestCount = reportRequestCount;
	}

	public int getParticipantCount() {
		return participantCount;
	}

	public void setParticipantCount(int participantCount) {
		this.participantCount = participantCount;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}



}
