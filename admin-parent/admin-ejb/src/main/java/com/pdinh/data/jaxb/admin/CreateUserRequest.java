package com.pdinh.data.jaxb.admin;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "createUserRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateUserRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name="projectId")
	private int projectId;
	
	@XmlAttribute(name="deliveryOfficeId")
	private int deliveryOfficeId;
	
	@XmlAttribute(name="firstname")
	private String firstname;
	
	@XmlAttribute(name="lastname")
	private String lastname;
	
	@XmlAttribute(name="email")
	private String email;
	
	@XmlAttribute(name="logonId")
	private String logonId;
	
	@XmlAttribute(name="languagePreference")
	private String languagePreference;
	
	@XmlAttribute(name="CHQlanguage")
	private String CHQlanguage;
	
	@XmlAttribute(name="FEXlanguage")
	private String FEXlanguage;
	
	@XmlAttribute(name="optional1")
	private String optional1;
	
	@XmlAttribute(name="emailId")
	private int emailId;

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getDeliveryOfficeId() {
		return deliveryOfficeId;
	}

	public void setDeliveryOfficeId(int deliveryOfficeId) {
		this.deliveryOfficeId = deliveryOfficeId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogonId() {
		return logonId;
	}

	public void setLogonId(String logonId) {
		this.logonId = logonId;
	}

	public String getLanguagePreference() {
		return languagePreference;
	}

	public void setLanguagePreference(String languagePreference) {
		this.languagePreference = languagePreference;
	}

	public String getCHQlanguage() {
		return CHQlanguage;
	}

	public void setCHQlanguage(String cHQlanguage) {
		CHQlanguage = cHQlanguage;
	}

	public String getFEXlanguage() {
		return FEXlanguage;
	}

	public void setFEXlanguage(String fEXlanguage) {
		FEXlanguage = fEXlanguage;
	}

	public String getOptional1() {
		return optional1;
	}

	public void setOptional1(String optional1) {
		this.optional1 = optional1;
	}

	public int getEmailId() {
		return emailId;
	}

	public void setEmailId(int emailId) {
		this.emailId = emailId;
	}

}
