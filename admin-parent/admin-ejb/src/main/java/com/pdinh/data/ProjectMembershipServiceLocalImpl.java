package com.pdinh.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ConsentUserDao;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.ConsentText;
import com.pdinh.persistence.ms.entity.ConsentUser;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectType;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.Roster;
import com.pdinh.persistence.ms.entity.User;

@Stateless
@LocalBean
public class ProjectMembershipServiceLocalImpl implements ProjectMembershipServiceLocal {

	private static final Logger log = LoggerFactory.getLogger(ProjectMembershipServiceLocalImpl.class);

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private ConsentUserDao consentUserDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private EnrollmentServiceLocalImpl enrollmentService;

	@Override
	public ProjectUser addUserToProject(Integer usersId, Integer projectId) {
		// TODO Auto-generated method stub
		return null;
	}
/**
 * Returns null if the user is already in the project
 * Doesn't do any enrollment.  you must do it yourself.
 */
	@Override
	public ProjectUser addUserToProject(User user, Project project) {
		String principal = "serviceUser";

		try {
			Subject subject = SecurityUtils.getSubject();
			principal = subject.getPrincipal().toString();
		} catch (UnavailableSecurityManagerException exp) {
			log.error("No subject available, this must be service user");
		} catch (NullPointerException npe){
			log.error("No subject available, this must be service user");
		}

		// Find out if user is in project already, if not, add
		List<Integer> userIdList = new ArrayList<Integer>();
		userIdList.add(user.getUsersId());
		List<ProjectUser> pUserList = projectUserDao.findProjectUsersFiltered(project.getProjectId(), userIdList);
		log.debug("got {} project users", pUserList.size());
		if (pUserList == null || pUserList.isEmpty()) {
			log.debug("User is not in project.  Add user to project");
			// user is not in project. add user to project
			// TODO: Create New service in admin ejb for adding user to a
			// project. Move this stuff there
			// also update code in projectParticipantsControllerBean to user
			// service
			ProjectUser projectUser = new ProjectUser();
			projectUser.setProject(project);
			projectUser.setUser(user);
			projectUser.setAddedBy(principal);
			project.getProjectUsers().add(projectUser);
			log.debug("added {}", user.getUsersId());

			// JJB - Seems like weirdness but now seems to be working
			// my first experiment - do it with a DAO method
			projectUserDao.create(projectUser);

			// add to roster and position
			if (user.getPositions() == null) {
				user.setPositions(new ArrayList<Position>());
			}
			if (user.getRosters() == null) {
				user.setRosters(new ArrayList<Roster>());
			}

			// Loop through courses in project and add the position entries
			/*
						Iterator<ProjectCourse> pcIterator = project.getProjectCourses().iterator();
						while (pcIterator.hasNext()) {
							ProjectCourse projectCourse = pcIterator.next();
							// JJB: Should only enroll if not already enrolled
							log.debug("Enrolling user in courses");
							if (projectCourse.getSelected()) {
								user = enrollmentService.enrollUserInCourse(project.getCompany(), projectCourse,
										user);
								enrollmentService.activateDeactivateInstrument(project.getCompany(), projectCourse,
										user);
							} else {
								activateUnactivateInstrument(projectCourse, user.getPositions());
							}
						}
						*/
			log.debug("Handling consent");
			String consentType = ConsentText.ASSESSMENT;
			if (project.getProjectType().equals(ProjectType.COACH)) {
				consentType = ConsentText.COACHING;
			}
			List<ConsentUser> consentUsers = new ArrayList<ConsentUser>();
			consentUsers = consentUserDao.findByParticipantAndType(user.getUsersId(), consentType);
			if (consentUsers == null || consentUsers.size() == 0) {
				log.debug("Adding Consent");
				ConsentUser consentUser = new ConsentUser();
				consentUser.setAccepted((short) 0);
				consentUser.setUser(user);
				consentUser.setConsentType(consentType);
				consentUserDao.create(consentUser);
			}
			return projectUser;
		}

		return null;
	}


	@Override
	public boolean removeUserFromProject(Integer usersId, Integer projectId) {
		ProjectUser puser = projectUserDao.findByProjectIdAndUserId(projectId, usersId);
		if (!(puser == null)) {
			projectUserDao.delete(puser);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeUserFromProject(User user, Project project) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public ProjectUser addUserToProjectWithEnrollment(User user, Project project) {
		String principal = "serviceUser";

		try {
			Subject subject = SecurityUtils.getSubject();
			principal = subject.getPrincipal().toString();
		} catch (UnavailableSecurityManagerException exp) {
			log.error("No subject available, this must be service user");
		} catch (NullPointerException npe){
			log.error("No subject available, this must be service user");
		}

		// Find out if user is in project already, if not, add
		List<Integer> userIdList = new ArrayList<Integer>();
		userIdList.add(user.getUsersId());
		List<ProjectUser> pUserList = projectUserDao.findProjectUsersFiltered(project.getProjectId(), userIdList);
		log.debug("got {} project users", pUserList.size());
		if (pUserList == null || pUserList.isEmpty()) {
			log.debug("User is not in project.  Add user to project");
			// user is not in project. add user to project
			// TODO: Create New service in admin ejb for adding user to a
			// project. Move this stuff there
			// also update code in projectParticipantsControllerBean to user
			// service
			ProjectUser projectUser = new ProjectUser();
			projectUser.setProject(project);
			projectUser.setUser(user);
			projectUser.setAddedBy(principal);
			project.getProjectUsers().add(projectUser);
			log.debug("added {}", user.getUsersId());

			projectUserDao.create(projectUser);

			// Loop through courses in project and add the position entries
			enrollmentService.enrollUserInProjectContent(project, user);
		
			log.debug("Handling consent");
			String consentType = ConsentText.ASSESSMENT;
			if (project.getProjectType().equals(ProjectType.COACH)) {
				consentType = ConsentText.COACHING;
			}
			List<ConsentUser> consentUsers = new ArrayList<ConsentUser>();
			consentUsers = consentUserDao.findByParticipantAndType(user.getUsersId(), consentType);
			if (consentUsers == null || consentUsers.size() == 0) {
				log.debug("Adding Consent");
				ConsentUser consentUser = new ConsentUser();
				consentUser.setAccepted((short) 0);
				consentUser.setUser(user);
				consentUser.setConsentType(consentType);
				consentUserDao.create(consentUser);
			}
			return projectUser;
		}

		return null;
	}

}
