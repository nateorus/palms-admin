package com.pdinh.data;

import javax.ejb.Local;

@Local
public interface ProjectCloneServiceLocal {

	public void cloneProjectCustomFields(int userId, int companyId, int existingProjectId, int newProjectId);
}
