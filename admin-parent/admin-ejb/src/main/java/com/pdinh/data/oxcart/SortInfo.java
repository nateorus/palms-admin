package com.pdinh.data.oxcart;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sortInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class SortInfo {

	@XmlElementWrapper(name = "sortGroups")
	@XmlElement(name = "sortGroup")
	List<SortGroup> sortGroups = new ArrayList<SortGroup>();

	public List<SortGroup> getSortGroup() {
		if (sortGroups == null) {
			sortGroups = new ArrayList<SortGroup>();
		}
		return this.sortGroups;
	}

}
