package com.pdinh.data.jaxb.admin;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "createTokenResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateTokenResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name="tkn")
	private String tkn;
	
	@XmlAttribute(name="validMinutes")
	private int validMinutes;
	
	@XmlAttribute(name="id")
	private int id;
	
	@XmlAttribute(name="contentId")
	private String contentId;
	
	@XmlAttribute(name="success")
	private boolean success;

	public String getTkn() {
		return tkn;
	}

	public void setTkn(String tkn) {
		this.tkn = tkn;
	}

	public int getValidMinutes() {
		return validMinutes;
	}

	public void setValidMinutes(int validMinutes) {
		this.validMinutes = validMinutes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
