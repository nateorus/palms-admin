package com.pdinh.data;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.ProjectUser;

@Local
public interface ProjectContentStatusServiceLocal {

	public ProjectUser setProjectContentStatus(ProjectUser projectUser);
}
