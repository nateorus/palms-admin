package com.pdinh.data;
import javax.ejb.Local;

@Local
public interface UsernameServiceLocal {
	public String generateUsername(String email, String firstname, String lastname);
	
}
