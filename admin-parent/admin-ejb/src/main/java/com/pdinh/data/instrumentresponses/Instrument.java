package com.pdinh.data.instrumentresponses;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "instrument")
@XmlAccessorType(XmlAccessType.FIELD)
public class Instrument {

	// Instrument name definitions
	// NOT A COMPLETE LIST
	public static final String INST_LAGIL = "lagil";
	public static final String INST_VEDGE = "vedge";

	@XmlAttribute
	private String id;

	@XmlElementWrapper(name = "responses")
	@XmlElement(name = "response")
	private List<Response> responses = new ArrayList<Response>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Response> getResponses() {
		return responses;
	}

	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}

}
