package com.pdinh.data;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
@Asynchronous
public class AsynchronousAuditLoggerLocalImpl implements AsynchronousAuditLoggerLocal {

	public static final Logger auditLog = LoggerFactory.getLogger("AUDIT-ADMIN");
	private static final Logger log = LoggerFactory.getLogger(AsynchronousAuditLoggerLocalImpl.class);
	
	@Override
	public void doAudit(String message, Object[] params) {
		log.debug("Current Thread: {}", Thread.currentThread().getName());
		
		auditLog.info(message, params);
		
	}

}
