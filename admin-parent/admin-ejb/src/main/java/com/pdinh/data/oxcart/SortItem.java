package com.pdinh.data.oxcart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sortItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class SortItem {

	@XmlAttribute(name = "id")
	protected String id;

	@XmlAttribute(name = "displayName")
	protected String displayName;

	public String getId() {
		return id;
	}

	public void setId(String value) {
		this.id = value;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String value) {
		this.displayName = value;
	}
}
