package com.pdinh.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.dochandler.excel.DocumentValidation;
import com.kf.dochandler.excel.ExcelRow;
import com.kf.dochandler.excel.FileReader;
import com.kf.dochandler.excel.SheetDefinition;
import com.kf.dochandler.excel.StringColumn;
import com.kf.dochandler.excel.exception.ExcelConstants;
import com.kf.dochandler.excel.exception.ExcelError;
import com.kf.dochandler.excel.hssf.HSSFFileReader;
import com.kf.dochandler.excel.xssf.XSSFFileReader;
import com.kf.fsm.service.FileService;
import com.kf.fsm.view.S3File;
import com.kf.uffda.data.dao.UploadImportBeforeAfterDao;
import com.kf.uffda.data.dao.UploadImportJobErrorDao;
import com.kf.uffda.data.dao.UploadImportJobLogDao;
import com.kf.uffda.data.dao.UploadImportRecordDao;
import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.lookup.FieldTypeMapper;
import com.kf.uffda.persistence.ErrorSeverity;
import com.kf.uffda.persistence.ErrorType;
import com.kf.uffda.persistence.FieldEntityType;
import com.kf.uffda.persistence.ImportAction;
import com.kf.uffda.persistence.ImportDataType;
import com.kf.uffda.persistence.ImportPhase;
import com.kf.uffda.persistence.JobStatus;
import com.kf.uffda.persistence.UploadImportBeforeAfter;
import com.kf.uffda.persistence.UploadImportError;
import com.kf.uffda.persistence.UploadImportJob;
import com.kf.uffda.persistence.UploadImportJobError;
import com.kf.uffda.persistence.UploadImportJobLog;
import com.kf.uffda.persistence.UploadImportRecord;
import com.kf.uffda.persistence.ValueEntityType;
import com.kf.uffda.service.BulkUploadService;
import com.kf.uffda.service.BulkUploadSingleton;
import com.kf.uffda.service.UffdaService;
import com.kf.uffda.utils.DateUtil;
import com.kf.uffda.vo.CoreFieldGroupVo;
import com.kf.uffda.vo.CustomFieldVo;
import com.kf.uffda.vo.CustomFieldsVo;
import com.pdinh.data.ms.dao.CompanyDao;
import com.pdinh.data.ms.dao.PhoneNumberDao;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.PhoneNumberTypeSingleton;
import com.pdinh.persistence.dto.BulkUploadUser;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.PhoneNumber;
import com.pdinh.persistence.ms.entity.PhoneNumberType;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.AddUserResultObj;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;

@Stateless
@LocalBean
public class ValidationImportServiceLocalImpl implements ValidationImportServiceLocal {

	@EJB
	private BulkUploadService bulkUploadService;

	@EJB
	private SheetDefinitionServiceLocal sheetDefinitionService;

	@EJB
	private UploadImportJobErrorDao uploadImportJobErrorDao;

	@EJB
	private UploadImportJobLogDao uploadImportJobLogDao;

	@EJB
	private UffdaService uffdaService;

	@EJB
	private LanguageSingleton languageSingleton;

	@EJB
	private UserDao userDao;

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private PhoneNumberDao phoneNumberDao;

	@EJB
	private BulkUploadSingleton bulkUploadSingleton;

	@EJB
	private UploadImportBeforeAfterDao uploadImportBeforeAfterDao;

	@EJB
	private UploadImportRecordDao uploadImportRecordDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private CompanyDao companyDao;

	@EJB
	private UsernameServiceLocal usernameService;

	@EJB
	private CreateUserServiceLocal createUserService;

	@EJB
	private PhoneNumberTypeSingleton phoneNumberTypeSingleton;

	@EJB
	private ProjectMembershipServiceLocal projectMembershipService;

	@EJB
	private EnrollmentServiceLocalImpl enrollmentService;

	@EJB
	private PositionDao positionDao;

	@EJB
	private FieldTypeMapper fieldTypeMapper;

	@EJB
	private FileService fileService;

	@Resource(mappedName = "application/config_properties")
	private Properties configProperties;

	private static final Logger log = LoggerFactory.getLogger(ValidationImportServiceLocalImpl.class);
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd");
	private static final Pattern CLEAN_PATTERN = Pattern.compile("([^:\\*]+?)[:\\*\\s]*$");
	private final Map<String, Language> languageMap = new HashMap<String, Language>();

	public static final String PARTICIPANTS_SHEET = "Participants";

	// Number of records to validate/import in a single transaction.
	private int chunkSize = 100;
	private int maxRecordsPerSheet = 50000;

	@PostConstruct
	public void init() {
		for (Language language : languageSingleton.getLanguageEntities()) {
			// TODO: Need to revisit how we map languages when aliases are
			// introduced.
			languageMap.put(language.getName(), language);
		}

		chunkSize = Integer.valueOf(configProperties.getProperty("persistChunkSize", "100"));
		maxRecordsPerSheet = Integer.valueOf(configProperties.getProperty("maxRecordsPerImportSheet", "1000"));
	}

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void processFile(int jobId, String jobStatus, String callbackUrl) {
		log.trace("Running ValidationImportServiceLocalImpl.processFile with parameters {}, {}", jobId, jobStatus);

		boolean isImport = jobStatus.equals(JobStatus.IMPORT_PENDING);

		// Make sure the specified job exists, with the proper status.
		UploadImportJobLog jobLog = bulkUploadService.getCurrentJobLog(jobId, jobStatus);

		// Try to update the job status to the next status in the progression
		// (either VALIDATION_IN_PROGRESS or IMPORT_IN_PROGRESS).
		UploadImportJob job = bulkUploadService.updateStatus(jobId, jobStatus);

		// If the job does not exist, or the status is not what we expect, stop.
		if (job == null || jobLog == null) {
			// This should never happen.
			// TODO: Do we want to send a callback to the import poller in this
			// case?
			log.error(
					"Received 'processFile' request for job {} with status '{}', but either the job or jobLog record is NULL.",
					jobId, jobStatus);
			return;
		}

		// Initializing local fields.
		Date startTime = new Date();
		Date endTime;
		ImportPhase importPhase;

		if (isImport) {
			importPhase = bulkUploadSingleton.getImportPhase(ImportPhase.IMPORT);
			bulkUploadService.setImportStarted(jobLog.getUploadImportJobLogId(), startTime);
		} else {
			importPhase = bulkUploadSingleton.getImportPhase(ImportPhase.VALIDATION);
			bulkUploadService.setValidationStarted(jobLog.getUploadImportJobLogId(), 0, startTime);
		}

		DocumentValidation validation = new DocumentValidation();
		Map<String, List<ExcelRow>> rows = new HashMap<String, List<ExcelRow>>();

		// Retrieve the Excel file from S3.
		S3File file = getFileForKey(jobLog.getOriginalFilename(), jobLog.getFileKeyName(), validation);
		if (file == null) {
			recordJobError(job, jobLog, validation, isImport, validation.getGlobalErrors().get(0).getDetail(),
					importPhase);
			endTime = new Date();
			if (isImport) {
				bulkUploadService.setImportComplete(jobLog.getUploadImportJobLogId(), 1, 0, 0, 0, 0, 0, startTime,
						endTime);
			} else {
				bulkUploadService.setValidationComplete(jobLog.getUploadImportJobLogId(), 0, startTime, endTime);
			}
			return;
		}

		// Save the Excel file to a local location, so we don't need to keep the
		// connection to S3 open for the entire validation process.
		// NOTE: S3 always uses lowercase strings for metadata keys, no matter
		// what case was used for the addUserMetadata call.

		InputStream stream = getFileInputStream(file, jobStatus, validation);

		try {
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (stream == null) {
			recordJobError(job, jobLog, validation, isImport, validation.getGlobalErrors().get(0).getDetail(),
					importPhase);
			endTime = new Date();
			if (isImport) {
				bulkUploadService.setImportComplete(jobLog.getUploadImportJobLogId(), 1, 0, 0, 0, 0, 0, startTime,
						endTime);
			} else {
				bulkUploadService.setValidationComplete(jobLog.getUploadImportJobLogId(), 0, startTime, endTime);
			}
			return;
		}

		// For October 2014, we only care about adding participants to projects.
		// Later we will also 'just' add users to PALMS.
		// Look up what standard and custom fields we expect to find in the
		// upload file.
		Project project = projectDao.findById(job.getProjectId());
		String fieldEntityType = FieldEntityType.PALMS_PROJECT_USERS;
		List<FieldDto> persistentStandardFields = uffdaService.getStandardFields(job.getCompanyId(),
				CoreFieldGroupVo.PALMS, job.getProjectId(), fieldEntityType, true, true, true,
				project.isIncludeClientLogonIdPassword());

		List<FieldDto> persistentCustomFields = new ArrayList<FieldDto>();
		uffdaService.getListOfCustomFields(job.getCompanyId(), job.getProjectId(), persistentCustomFields,
				fieldEntityType, false, true, true, true);

		// Run validation on the file. This does NOT include the check to see
		// whether a user in the upload file already exists
		// in PALMS.
		validateFile(stream, file.getFileExtention(), persistentStandardFields, persistentCustomFields, rows,
				validation);
		try {
			stream.close();
		} catch (IOException ioe) {
			// Really, is there anything we can do about this?
		}

		// If certain kinds of validation errors occurred, record them and stop.
		if (rows.get(PARTICIPANTS_SHEET) == null) {
			String errorDetail;
			if (validation.getGlobalErrorCount() > 0) {
				// Some global error occurred during the validation process. Use
				// that error for the detail.
				errorDetail = validation.getGlobalErrors().get(0).getDetail();
			} else {
				// One or more columns is missing from the Participants sheet.
				errorDetail = "Participants sheet is missing one or more expected columns.";
			}
			recordJobError(job, jobLog, validation, isImport, errorDetail, importPhase);
			endTime = new Date();
			if (isImport) {
				bulkUploadService.setImportComplete(jobLog.getUploadImportJobLogId(), validation.getGlobalErrorCount(),
						0, 0, 0, 0, 0, startTime, endTime);
			} else {
				bulkUploadService.setValidationComplete(jobLog.getUploadImportJobLogId(), 0, startTime, endTime);
			}
			return;
		}

		// If there are too many rows in the import document, log an error and
		// stop.
		if (!validation.getErrorsForSheet(PARTICIPANTS_SHEET).isEmpty()) {
			for (ExcelError error : validation.getErrorsForSheet(PARTICIPANTS_SHEET)) {
				if (error.getError().equals(UploadImportError.MAX_RECORDS_EXCEEDED)) {
					recordJobError(job, jobLog, validation, isImport, error.getDetail(), importPhase);
					endTime = new Date();
					if (isImport) {
						bulkUploadService.setImportComplete(jobLog.getUploadImportJobLogId(), 0, 0, 0, 0, 0, 0,
								startTime, endTime);
					} else {
						bulkUploadService
								.setValidationComplete(jobLog.getUploadImportJobLogId(), 0, startTime, endTime);
					}
					return;
				}
			}
		}

		// Need an easy way to get the custom field information corresponding to
		// data in the Excel document.
		Map<String, CustomFieldDto> customFieldMap = new HashMap<String, CustomFieldDto>();
		for (FieldDto field : persistentCustomFields) {
			customFieldMap.put(field.getImportFieldName(), (CustomFieldDto) field);
		}

		// The following collections will be populated when we call
		// 'getUsersToImport' . . .

		// Contains all rows which duplicate an email address found in another
		// row earlier in the document.
		List<ExcelRow> duplicateEmailRows = new ArrayList<ExcelRow>();

		// Collection of email addresses which appear in multiple rows. Here for
		// convenience, because it's pretty painful to dig an email address out
		// of an ExcelRow.
		Set<String> duplicateEmails = new HashSet<String>();

		// List of ImportUsers with an email column that is somehow invalid
		// (missing or not a valid email).
		List<ImportUser> invalidEmailUsers = new ArrayList<ImportUser>();

		// Convert the document rows into ImportUser objects, and track them by
		// email address --- and populate the problem cases of duplicate or
		// invalid email addresses.
		Map<String, ImportUser> usersToImport = getUsersToImport(rows.get(PARTICIPANTS_SHEET), customFieldMap,
				duplicateEmailRows, duplicateEmails, invalidEmailUsers);

		// We will ignore any rows which have duplicate email addresses, so
		// remove them from the DocumentValidation object.
		for (ExcelRow row : duplicateEmailRows) {
			validation.removeErrorsForRow(PARTICIPANTS_SHEET, row.getOrdinal());
		}

		// Following this call, ALL rows from the import document (which have a
		// reasonable email address) will be represented in the usersToImport
		// map. Users which are in PALMS but not in the project will be in the
		// existingPalmsUsers list, and users which are in PALMS AND the project
		// will be in the existingProjectUsers list.
		List<ImportUser> existingPalmsUsers = new ArrayList<ImportUser>();
		List<ImportUser> existingProjectUsers = new ArrayList<ImportUser>();
		findExistingUsers(job.getCompanyId(), job.getProjectId(), usersToImport.keySet(), existingPalmsUsers,
				existingProjectUsers);

		// Load custom field data for the existing users we found.
		loadCustomFieldData(existingPalmsUsers, existingProjectUsers, job.getCompanyId(), job.getProjectId());

		// Detect all validation errors and persist them.
		int blankRows = 0;
		for (ExcelRow row : rows.get(PARTICIPANTS_SHEET)) {
			if (row.isBlank()) {
				blankRows++;
			}
		}

		if (!isImport) {
			bulkUploadService.setValidationStarted(jobLog.getUploadImportJobLogId(), rows.get(PARTICIPANTS_SHEET)
					.size(), startTime);
		}

		handleValidation(validation, usersToImport, existingPalmsUsers, existingProjectUsers, customFieldMap, jobLog,
				isImport, duplicateEmails, blankRows, importPhase);

		if (isImport) {
			log.trace("We're ready to import data for job {} !", job.getUploadImportJobId());

			ImportCountTracker tracker = performImports(job, jobLog.getImportSubmitterId(), usersToImport,
					existingPalmsUsers, existingProjectUsers, customFieldMap, invalidEmailUsers,
					jobLog.getUploadImportJobLogId());
			log.trace("tracker = {}", tracker.toString());
			endTime = new Date();

			bulkUploadService.setImportComplete(jobLog.getUploadImportJobLogId(), tracker.totalRecordsErrored,
					tracker.totalRecordsExisting, tracker.totalRecordsAdded, tracker.totalRecordsUpdated,
					tracker.totalRecordsUpdatedAndAdded, tracker.totalRecordsImported, startTime, endTime);
		} else {
			endTime = new Date();
			bulkUploadService.setValidationComplete(jobLog.getUploadImportJobLogId(), rows.get(PARTICIPANTS_SHEET)
					.size(), startTime, endTime);
		}
		bulkUploadService.updateStatus(jobId, job.getJobStatus().getCode());

		if (callbackUrl != null) {
			makeCallback(jobId, callbackUrl);
		}
	}

	private S3File getFileForKey(String fileName, String fileKey, DocumentValidation validation) {

		S3File object = null;
		String bucketName = configProperties.getProperty("bulkUploadS3Bucket");

		try {
			object = fileService.downloadFile(fileName, fileKey, bucketName, configProperties);
		} catch (Exception exp) {
			String message = "File Storage Manager exception when attempting to retrieve file '" + fileKey
					+ "' from S3.  Error Message: " + exp.getMessage();
			ExcelError error = new ExcelError(ExcelConstants.GLOBAL_ERROR, ExcelConstants.READ_ERROR,
					ExcelConstants.SEVERE);
			error.setDetail(message);
			validation.addGlobalError(error);
			log.error(message);
		}

		return object;
	}

	private InputStream getFileInputStream(S3File s3File, String jobStatus, DocumentValidation validation) {
		File rootDir = getTempRootDir(jobStatus);
		File target = new File(rootDir, cleanUpFileKeyName(s3File.getFileKey()) + "." + s3File.getFileExtention());
		InputStream retVal = null;

		try {
			target.createNewFile();
			InputStream in = s3File.getInputStream();

			FileOutputStream out = new FileOutputStream(target);

			byte[] buffer = new byte[1024];
			int bytesRead = 0;

			while ((bytesRead = in.read(buffer)) > -1) {
				out.write(buffer, 0, bytesRead);
			}

			out.close();
			retVal = new FileInputStream(target);

		} catch (FileNotFoundException fnfe) {
			String message = "FileNotFoundException attempting to write temp file to location: "
					+ target.getAbsolutePath();
			ExcelError error = new ExcelError(ExcelConstants.GLOBAL_ERROR, ExcelConstants.READ_ERROR);
			error.setDetail(message);
			validation.addGlobalError(error);
			log.error(message);

		} catch (IOException ioe) {
			String message = "IOException attempting to write/read temp file at location: " + target.getAbsolutePath();
			ExcelError error = new ExcelError(ExcelConstants.GLOBAL_ERROR, ExcelConstants.READ_ERROR);
			error.setDetail(message);
			validation.addGlobalError(error);
			log.error(message);
		}

		return retVal;
	}

	private String cleanUpFileKeyName(String keyName) {
		return keyName.replaceAll("/", "_");
	}

	private File getTempRootDir(String jobStatus) {
		File importFilesRoot = new File(configProperties.getProperty("importFilesRoot"));
		String dateString = DATE_FORMAT.format(new Date());
		File jobStatusRoot = new File(importFilesRoot, jobStatus + File.separator + dateString);

		if (!jobStatusRoot.exists()) {
			jobStatusRoot.mkdirs();
		}
		return jobStatusRoot;
	}

	// NOTE: CAN CALL THIS METHOD FOR REAL-TIME, SYNCHRONOUS FILE VALIDATION
	// WHICH DOES NOT REQUIRE COMMUNICATION WITH AWS!!!
	// Does NOT include user validation, checking whether users exist, have
	// conflicting data, etc.
	@Override
	public void validateFile(InputStream stream, String extension, List<FieldDto> standardFields,
			List<FieldDto> customFields, Map<String, List<ExcelRow>> rows, DocumentValidation validation) {

		FileReader reader;
		if (extension.equalsIgnoreCase("xls")) {
			reader = new HSSFFileReader(stream);
		} else {
			reader = new XSSFFileReader(stream);
		}

		ExcelError error = reader.init();
		if (error != null) {
			validation.addGlobalError(error);
			log.error(error.getDetail());
			return;
		}

		// TODO: Verify that the returned templateVersion is current.
		String templateVersion = getTemplateVersion(reader);
		boolean usesTemplate = (templateVersion != null);
		log.trace("Template version is: {}", templateVersion);

		if (!reader.containsSheet(PARTICIPANTS_SHEET)) {
			error = new ExcelError(ExcelConstants.GLOBAL_ERROR, ExcelConstants.MISSING_SHEET, ExcelConstants.SEVERE);
			error.setDetail("Required sheet '" + PARTICIPANTS_SHEET + "' is missing from file.");
			validation.addGlobalError(error);
			return;
		}

		List<String> columnNames = sanitizeColumnNames(reader.getColumnNamesForSheet(PARTICIPANTS_SHEET));
		SheetDefinition sheetDefinition = sheetDefinitionService.getSheetDefinitionFor(standardFields, customFields,
				PARTICIPANTS_SHEET, columnNames, maxRecordsPerSheet, usesTemplate, validation);

		if (sheetDefinition == null) {
			// Sheet is missing some columns.
			log.trace("sheetDefinition is NULL!!");
			return;
		}

		Map<String, SheetDefinition> sheets = new HashMap<String, SheetDefinition>();
		sheets.put(sheetDefinition.getName(), sheetDefinition);

		for (Map.Entry<String, SheetDefinition> entry : sheets.entrySet()) {
			List<ExcelRow> rowsForSheet = processSheet(entry.getValue(), reader, validation);

			// Remove the first two rows, which are a dummy row (because Excel
			// is 1-based) and the header row.
			rowsForSheet.remove(0);
			rowsForSheet.remove(0);

			if (rowsForSheet != null) {
				rows.put(entry.getKey(), rowsForSheet);
			}
		}

		Set<String> knownSheetNames = new HashSet<String>();
		knownSheetNames.add("Participants");
		knownSheetNames.add("Metadata");
		knownSheetNames.add("Definitions");
		for (String unknownSheetName : reader.getUnknownSheetNames(knownSheetNames)) {
			log.trace("Found unknown sheet: {}", unknownSheetName);
			error = new ExcelError(ExcelConstants.GLOBAL_ERROR, ExcelConstants.UNKNOWN_SHEET);
			error.setDetail(unknownSheetName);
			validation.addGlobalError(error);
		}
	}

	private List<String> sanitizeColumnNames(List<String> columnNames) {
		List<String> retVal = new ArrayList<String>();

		for (String header : columnNames) {
			Matcher matcher = CLEAN_PATTERN.matcher(header);
			matcher.find();
			retVal.add(matcher.group(1));
			log.trace("Adding column name '{}'", matcher.group(1));
		}

		return retVal;
	}

	private String getTemplateVersion(FileReader reader) {
		String templateVersion = null;
		if (reader.containsSheet("Metadata")) {
			SheetDefinition metadataSheet = new SheetDefinition("Metadata");
			metadataSheet.addColumn(new StringColumn("Key", "Key", true));
			metadataSheet.addColumn(new StringColumn("Value", "Value", true));

			List<ExcelError> errors = new ArrayList<ExcelError>();
			for (ExcelRow row : reader.readRowsForSheet(metadataSheet, errors)) {
				String key = row.getStringValue("Key");
				if (key != null && key.equalsIgnoreCase("Template Version:")) {
					templateVersion = row.getStringValue("Value");
					break;
				}
			}
		}

		return templateVersion;
	}

	private List<ExcelRow> processSheet(SheetDefinition sheetDefinition, FileReader reader,
			DocumentValidation validation) {
		boolean canProcessRows = true;
		List<ExcelError> sheetErrors = new ArrayList<ExcelError>();
		List<ExcelRow> rows = reader.readRowsForSheet(sheetDefinition, sheetErrors);

		if (rows == null) {
			// Should only happen if specified sheet does not exist in the Excel
			// document.
			for (ExcelError error : sheetErrors) {
				validation.addSheetError(sheetDefinition.getName(), error);
			}
			return rows;
		}
		validation.addRowCount(sheetDefinition.getName(), rows.size());
		validation.addUnknownColumnNames(sheetDefinition.getName(), sheetDefinition.getUnknownColumnNames());

		if (sheetErrors.size() > 0) {
			canProcessRows = handleSheetErrors(sheetErrors, sheetDefinition.getName(), validation);
		}

		if (canProcessRows) {
			for (int idx = 2; idx < rows.size(); idx++) {
				ExcelRow row = rows.get(idx);
				log.trace("Running validation on row {}", row.getOrdinal());
				if (!row.isBlank() && row.hasErrors()) {
					validation.addRowErrors(sheetDefinition.getName(), row);
				}
			}
		}

		return rows;
	}

	private boolean handleSheetErrors(List<ExcelError> sheetErrors, String sheetName, DocumentValidation validation) {
		boolean canProcessRows = true;
		for (ExcelError error : sheetErrors) {
			if (error.getError().equals(ExcelConstants.MAX_RECORDS_EXCEEDED)) {
				validation.addSheetError(sheetName, error);
			} else {
				validation.addGlobalError(error);
				canProcessRows = false;
			}
		}

		return canProcessRows;
	}

	private Map<String, ImportUser> getUsersToImport(List<ExcelRow> rows, Map<String, CustomFieldDto> customFieldMap,
			List<ExcelRow> duplicateEmailRows, Set<String> duplicateEmails, List<ImportUser> invalidEmailRowIDs) {
		Map<String, ImportUser> retVal = new HashMap<String, ImportUser>();
		if (rows == null || rows.isEmpty()) {
			return retVal;
		}

		for (ExcelRow row : rows) {
			if (row.isBlank()) {
				continue;
			}

			ImportUser user = new ImportUser();
			user.initialize(row, languageMap, customFieldMap);

			if (user.getEmailAddress() != null) {
				if (retVal.containsKey(user.getEmailAddress().toLowerCase())) {
					duplicateEmailRows.add(row);
					duplicateEmails.add(user.getEmailAddress().toLowerCase());
				} else {
					retVal.put(user.getEmailAddress().toLowerCase(), user);
				}
			} else {
				// Email address was blank, or invalid format. If invalid
				// format, dig out the email address data from the ExcelError
				// object and add it to the ImportUser.
				Set<ExcelError> emailErrors = row.getErrorsForColumn(ImportUser.EMAIL_ADDRESS);

				// Should be only one
				ExcelError error = emailErrors.iterator().next();
				if (error.getError().equals(ExcelConstants.INVALID_EMAIL_ADDRESS)) {
					user.setEmailAddress(error.getDataValue());
				}

				invalidEmailRowIDs.add(user);
			}
		}

		return retVal;
	}

	private void findExistingUsers(int companyId, Integer projectId, Set<String> userEmails,
			List<ImportUser> existingPalmsUsers, List<ImportUser> existingProjectUsers) {
		Map<Integer, BulkUploadUser> userMap = new HashMap<Integer, BulkUploadUser>();

		if (userEmails.size() > 0) {
			for (BulkUploadUser bulkUploadUser : userDao.findUserDataForBulkUpload(companyId, userEmails)) {
				userMap.put(bulkUploadUser.getUserId(), bulkUploadUser);
			}
		}

		// Check if we have any existing users in the spreadsheet, if not then
		// return
		if (userMap.size() == 0) {
			return;
		}

		List<Integer> userIdList = new ArrayList<Integer>();
		userIdList.addAll(userMap.keySet());
		List<PhoneNumber> businessPhones = phoneNumberDao.findByUsersIdListAndType(userIdList,
				PhoneNumberType.BUISINESS_PHONE_ABBV);

		if (businessPhones != null && !businessPhones.isEmpty()) {
			for (PhoneNumber number : businessPhones) {
				userMap.get(number.getUser().getUsersId()).setBusinessPhone(number.getNumber());
			}
		}

		// UserMap now contains a BulkUploadUser object for every email address
		// in the import file which corresponds to a user in PALMS. For each
		// user, check if they're already assigned to the project.
		if (projectId != null) {
			for (ProjectUser projectUser : projectUserDao.findProjectUsersFiltered(projectId, userIdList)) {
				ImportUser importUser = new ImportUser();

				// Note that each user which is already assigned to the project
				// is REMOVED from the userMap, so when we're done, we're only
				// left with PALMS users who are NOT assigned to the project.
				importUser.initialize(userMap.remove(projectUser.getUser().getUsersId()));
				importUser.setProjectUserId(projectUser.getRowId());

				// Get the most recent project for this user that's not the
				// current project.
				ProjectUser recentProject = projectUser;
				for (ProjectUser userProject : projectUserDao.findProjectUsersByUserId(projectUser.getUser()
						.getUsersId())) {

					if (projectUser.getRowId() != userProject.getRowId()) {
						recentProject = userProject;
					}
				}

				List<String> instruments = new ArrayList<String>();

				// Get all the project courses for the current project.
				for (ProjectCourse projectCourse : projectUser.getProject().getProjectCourses()) {

					// Get all the positions for the most recent project.
					for (Position position : recentProject.getUser().getPositions()) {

						// Check to see if the user already has test data for
						// this course.
						if (companyId == position.getCompany().getCompanyId() && position.getCompletionStatus() > 0
								&& projectCourse.getCourse().getCourse() == position.getCourse().getCourse()) {

							// Looks like the user already has test data for a
							// different project.
							instruments.add(position.getCourse().getCourseLabel());
							importUser.setTestData(true);
						}
					}
				}

				// Check if the user already has test data.
				if (importUser.hasTestData()) {

					// Create the test data detail message.
					StringBuffer str = new StringBuffer();
					str.append("For recent Project ");
					str.append(recentProject.getProject().getName());
					str.append(", Participant added on ");
					str.append(DateUtil.getFormattedDateTime(recentProject.getDateAdded()));
					str.append(", with common instruments(s)");

					int count = 0;
					for (String instrument : instruments) {
						if (count > 0) {
							str.append(",");
						}
						str.append(" ");
						str.append(instrument);
						count++;
					}
					importUser.setTestDataDetail(str.toString());

				} else {
					importUser.setTestDataDetail("");
				}

				// Add the user to the existing project user list
				existingProjectUsers.add(importUser);
			}
		}

		for (Map.Entry<Integer, BulkUploadUser> entry : userMap.entrySet()) {
			ImportUser importUser = new ImportUser();
			importUser.initialize(entry.getValue());
			existingPalmsUsers.add(importUser);
		}
	}

	private void loadCustomFieldData(List<ImportUser> existingPalmsUsers, List<ImportUser> existingProjectUsers,
			int companyId, Integer projectId) {
		// TODO: As of October, 2014, it is only possible to assign custom field
		// values to a user within the context of a project.
		// However, once it is possible to assign custom field values to a user
		// directly, this method becomes more complicated.
		if (projectId == null) {
			return;
		}

		List<Integer> userIdList = new ArrayList<Integer>();
		for (ImportUser user : existingProjectUsers) {
			userIdList.add(new Integer(user.getUserId()));
		}

		Map<Integer, CustomFieldsVo> customFieldsMap = uffdaService.getCustomFieldsAsMap(companyId, projectId,
				FieldEntityType.PALMS_PROJECT_USERS, userIdList, ValueEntityType.PROJECT_USER_ID,
				FieldDto.LANGUAGE_ENGLISH, false, new Date(), true);

		for (ImportUser user : existingProjectUsers) {
			CustomFieldsVo customFields = customFieldsMap.get(new Integer(user.getUserId()));

			for (CustomFieldVo customField : customFields.getFields()) {
				user.addCustomField(customField);
			}
		}
	}

	private void handleValidation(DocumentValidation validation, Map<String, ImportUser> usersToImport,
			List<ImportUser> existingPalmsUsers, List<ImportUser> existingProjectUsers,
			Map<String, CustomFieldDto> customFieldMap, UploadImportJobLog jobLog, boolean isImport,
			Set<String> duplicateEmails, int blankRowCount, ImportPhase importPhase) {

		// A list to track all UploadImportError records we write to the
		// upload_import_job_error table.
		List<UploadImportError> errorList = new ArrayList<UploadImportError>();

		UploadImportError blankRowsError = bulkUploadSingleton.getUploadImportError(UploadImportError.BLANK_ROWS);
		// Save info message about number of blank rows in file.
		if (blankRowCount > 0) {
			UploadImportJobError jobError = new UploadImportJobError();
			jobError.setUploadImportJobLog(jobLog);
			jobError.setUploadImportError(blankRowsError);
			jobError.setSheetName(PARTICIPANTS_SHEET);
			jobError.setDetails("Found " + blankRowCount + " blank rows in sheet.");
			jobError.setImportPhase(importPhase);
			uploadImportJobErrorDao.create(jobError);
			errorList.add(blankRowsError);
		}

		// Multiple records with the same email address.
		errorList.addAll(persistDuplicateEmailErrors(duplicateEmails, jobLog, importPhase));

		// Errors intrinsic to the import file (not taking into account
		// duplicate users).
		errorList.addAll(persistFileValidationErrors(validation, jobLog, importPhase));

		// Information about users in the import file who exist in PALMS, but
		// not in the project.
		errorList.addAll(persistDataConflictErrors(usersToImport, existingPalmsUsers, customFieldMap, jobLog,
				importPhase));

		// Information about users in the import file who exist in PALMS AND the
		// project.
		errorList.addAll(persistDataConflictErrors(usersToImport, existingProjectUsers, customFieldMap, jobLog,
				importPhase));

		// Check if any users already have test in the system.
		errorList.addAll(persistTestDataErrors(usersToImport, existingPalmsUsers, jobLog, importPhase));
		errorList.addAll(persistTestDataErrors(usersToImport, existingProjectUsers, jobLog, importPhase));

		// Persist number of severe, warning, info-level errors, and error scope
		// (global, sheet-level, row-level)
		updateErrorCounts(jobLog, errorList, isImport);
	}

	private List<UploadImportError> persistDuplicateEmailErrors(Set<String> duplicateEmails, UploadImportJobLog jobLog,
			ImportPhase importPhase) {
		List<UploadImportError> errorList = new ArrayList<UploadImportError>();
		List<String> duplicateEmailList = new ArrayList<String>(duplicateEmails);

		int idx = 0;
		while (idx < duplicateEmailList.size()) {
			idx += performDuplicateEmailValidation(jobLog, duplicateEmailList, errorList, idx, importPhase);
		}

		return errorList;
	}

	private int performDuplicateEmailValidation(UploadImportJobLog jobLog, List<String> duplicateEmailList,
			List<UploadImportError> errorList, int startIdx, ImportPhase importPhase) {

		int rowsProcessed = 0;
		UploadImportError duplicateEmailError = bulkUploadSingleton
				.getUploadImportError(UploadImportError.DUPLICATE_EMAIL_IN_SHEET);

		for (; rowsProcessed < chunkSize && (startIdx + rowsProcessed < duplicateEmailList.size()); rowsProcessed++) {
			String emailAddress = duplicateEmailList.get(startIdx + rowsProcessed);
			UploadImportJobError jobError = new UploadImportJobError();
			jobError.setUploadImportJobLog(jobLog);
			jobError.setUploadImportError(duplicateEmailError);
			jobError.setSheetName(PARTICIPANTS_SHEET);
			jobError.setDetails("Multiple records found with email address " + emailAddress
					+ ".  All records after the first will be ignored.");
			jobError.setImportPhase(importPhase);
			uploadImportJobErrorDao.create(jobError);
			errorList.add(duplicateEmailError);
		}

		return rowsProcessed;
	}

	private List<UploadImportError> persistFileValidationErrors(DocumentValidation validation,
			UploadImportJobLog jobLog, ImportPhase importPhase) {
		List<UploadImportError> errorList = new ArrayList<UploadImportError>();

		// Global errors.
		for (ExcelError error : validation.getGlobalErrors()) {
			errorList.add(persistValidationError(jobLog, error, null, null, null, importPhase));
		}

		// Unknown sheets in file.
		for (String sheetName : validation.getUnknownSheetNames()) {
			ExcelError error = new ExcelError(ExcelConstants.GLOBAL_ERROR, ExcelConstants.UNKNOWN_SHEET,
					ExcelConstants.WARNING);
			errorList.add(persistValidationError(jobLog, error, sheetName, null, null, importPhase));
		}

		// Sheets with sheet-level errors
		for (String sheetName : validation.getNamesOfSheetsWithErrors()) {
			for (ExcelError error : validation.getErrorsForSheet(sheetName)) {
				errorList.add(persistValidationError(jobLog, error, sheetName, null, null, importPhase));
			}
		}

		// Known sheets processed.
		for (String sheetName : validation.getProcessedSheetNames()) {
			// Unknown columns in the sheet.
			for (String columnName : validation.getUnknownColumnNames(sheetName)) {
				ExcelError error = new ExcelError(ExcelConstants.SHEET_ERROR, ExcelConstants.UNKNOWN_COLUMN,
						ExcelConstants.WARNING);
				errorList.add(persistValidationError(jobLog, error, sheetName, columnName, null, importPhase));
			}

			// Row-level errors within sheet
			// Starting with index 2 because DocumentValidation still has
			// references to the dummy 0-row and the header 1-row
			int idx = 2;
			while (idx < validation.getRowCount(sheetName)) {
				idx += persistRowValidationErrors(validation, jobLog, sheetName, errorList, idx, importPhase);
			}
		}

		return errorList;
	}

	private int persistRowValidationErrors(DocumentValidation validation, UploadImportJobLog jobLog, String sheetName,
			List<UploadImportError> errorList, int startIdx, ImportPhase importPhase) {
		int rowsProcessed = 0;
		int rowId;

		for (; rowsProcessed < chunkSize && (startIdx + rowsProcessed < validation.getRowCount(sheetName)); rowsProcessed++) {
			rowId = startIdx + rowsProcessed;
			for (Map.Entry<String, Set<ExcelError>> entry : validation.getErrorsForRow(sheetName, rowId).entrySet()) {
				for (ExcelError error : entry.getValue()) {
					errorList.add(persistValidationError(jobLog, error, sheetName, entry.getKey(), rowId, importPhase));
				}
			}
		}

		return rowsProcessed;
	}

	private List<UploadImportError> persistDataConflictErrors(Map<String, ImportUser> usersToImport,
			List<ImportUser> existingUsers, Map<String, CustomFieldDto> customFieldMap, UploadImportJobLog jobLog,
			ImportPhase importPhase) {
		List<UploadImportError> errorList = new ArrayList<UploadImportError>();

		if (existingUsers.isEmpty()) {
			// Nothing to do.
			return errorList;
		}

		int idx = 0;
		while (idx < existingUsers.size()) {
			idx += performDataConflictValidation(jobLog, usersToImport, existingUsers, customFieldMap, errorList, idx,
					importPhase);
		}

		return errorList;
	}

	private int performDataConflictValidation(UploadImportJobLog jobLog, Map<String, ImportUser> usersToImport,
			List<ImportUser> existingUsers, Map<String, CustomFieldDto> customFieldMap,
			List<UploadImportError> errorList, int startIdx, ImportPhase importPhase) {

		int usersProcessed = 0;
		UploadImportError valueChangedError = bulkUploadSingleton
				.getUploadImportError(UploadImportError.FIELD_VALUE_CHANGED);

		UploadImportError newCustomFieldError = bulkUploadSingleton
				.getUploadImportError(UploadImportError.NEW_CUSTOM_FIELD);

		UploadImportError existingUserError = bulkUploadSingleton.getUploadImportError(UploadImportError.EXISTING_USER);
		boolean dataValueChange;

		for (; usersProcessed < chunkSize && (startIdx + usersProcessed < existingUsers.size()); usersProcessed++) {
			ImportUser existingUser = existingUsers.get(startIdx + usersProcessed);
			ImportUser importUser = usersToImport.get(existingUser.getEmailAddress().toLowerCase());

			dataValueChange = false;

			for (String fieldKey : importUser.getCoreFieldMismatches(existingUser, true)) {
				UploadImportJobError jobError = new UploadImportJobError();
				jobError.setUploadImportJobLog(jobLog);
				jobError.setUploadImportError(valueChangedError);
				jobError.setSheetName(PARTICIPANTS_SHEET);
				jobError.setColumnName(fieldKey);
				jobError.setRowId(importUser.getExcelRowId());
				jobError.setImportPhase(importPhase);
				uploadImportJobErrorDao.create(jobError);
				errorList.add(valueChangedError);

				UploadImportBeforeAfter ba = new UploadImportBeforeAfter();
				ba.setUploadImportJob(jobLog.getUploadImportJob());
				ba.setUploadImportJobLog(jobLog);
				ba.setUploadImportJobError(jobError);
				ba.setImportDataType(bulkUploadSingleton.getImportDataType(ImportDataType.CHARACTER));
				ba.setBeforeCharValue(existingUser.getCoreFieldValue(fieldKey));
				ba.setAfterCharValue(importUser.getCoreFieldValue(fieldKey));
				uploadImportBeforeAfterDao.create(ba);

				dataValueChange = true;
			}

			for (String fieldKey : existingUser.getNewCustomFields()) {
				UploadImportJobError jobError = new UploadImportJobError();
				jobError.setUploadImportJobLog(jobLog);
				jobError.setUploadImportError(newCustomFieldError);
				jobError.setSheetName(PARTICIPANTS_SHEET);
				jobError.setColumnName(fieldKey);
				jobError.setRowId(importUser.getExcelRowId());
				jobError.setImportPhase(importPhase);
				uploadImportJobErrorDao.create(jobError);
				errorList.add(valueChangedError);
			}

			for (String fieldKey : importUser.getCustomFieldMismatches(existingUser, true)) {
				UploadImportJobError jobError = new UploadImportJobError();
				jobError.setUploadImportJobLog(jobLog);
				jobError.setUploadImportError(valueChangedError);
				jobError.setSheetName(PARTICIPANTS_SHEET);
				jobError.setColumnName(fieldKey);
				jobError.setRowId(importUser.getExcelRowId());
				jobError.setImportPhase(importPhase);
				uploadImportJobErrorDao.create(jobError);
				errorList.add(valueChangedError);

				UploadImportBeforeAfter ba = new UploadImportBeforeAfter();
				ba.setUploadImportJob(jobLog.getUploadImportJob());
				ba.setUploadImportJobLog(jobLog);
				ba.setUploadImportJobError(jobError);

				CustomFieldDto field = customFieldMap.get(fieldKey);
				if (field.isCharValue()) {
					ba.setImportDataType(bulkUploadSingleton.getImportDataType(ImportDataType.CHARACTER));
					ba.setBeforeCharValue(existingUser.getCustomFieldStringValue(fieldKey));
					ba.setAfterCharValue(importUser.getCustomFieldStringValue(fieldKey));

				} else if (field.isIntValue()) {
					ba.setImportDataType(bulkUploadSingleton.getImportDataType(ImportDataType.INT));
					ba.setBeforeIntValue(existingUser.getCustomFieldIntValue(fieldKey));
					ba.setAfterIntValue(importUser.getCustomFieldIntValue(fieldKey));

				} else if (field.isDateValue()) {
					ba.setImportDataType(bulkUploadSingleton.getImportDataType(ImportDataType.DATE));
					ba.setBeforeDateValue(existingUser.getCustomFieldDateValue(fieldKey));
					ba.setAfterDateValue(importUser.getCustomFieldDateValue(fieldKey));

				} else if (field.isListValue()) {
					String beforeValue = existingUser.getCustomFieldListValue(fieldKey);
					int beforeIdx = field.findListValueIdByValue(beforeValue);

					String afterValue = importUser.getCustomFieldListValue(fieldKey);
					int afterIdx = field.findListValueIdByValue(afterValue);

					ba.setImportDataType(bulkUploadSingleton.getImportDataType(ImportDataType.VALUE_IN_LIST));
					ba.setBeforeCharValue(beforeValue);
					ba.setAfterCharValue(afterValue);
					ba.setBeforeInListIndex(beforeIdx);
					ba.setAfterInListIndex(afterIdx);

				}
				uploadImportBeforeAfterDao.create(ba);
				dataValueChange = true;
			}

			if (!dataValueChange) {
				// All fields in the import file match those in the database.
				// Record this fact.
				UploadImportJobError jobError = new UploadImportJobError();
				jobError.setUploadImportJobLog(jobLog);
				jobError.setUploadImportError(existingUserError);
				jobError.setSheetName("Participants");
				jobError.setRowId(importUser.getExcelRowId());
				jobError.setImportPhase(importPhase);
				uploadImportJobErrorDao.create(jobError);
				errorList.add(existingUserError);
			}
		}

		return usersProcessed;
	}

	private List<UploadImportError> persistTestDataErrors(Map<String, ImportUser> usersToImport,
			List<ImportUser> existingUsers, UploadImportJobLog jobLog, ImportPhase importPhase) {

		List<UploadImportError> errorList = new ArrayList<UploadImportError>();

		if (existingUsers.isEmpty()) {
			// Nothing to do.
			return errorList;
		}

		int idx = 0;
		while (idx < existingUsers.size()) {
			idx += performTestDataValidation(jobLog, usersToImport, existingUsers, errorList, idx, importPhase);
		}

		return errorList;
	}

	private int performTestDataValidation(UploadImportJobLog jobLog, Map<String, ImportUser> usersToImport,
			List<ImportUser> existingUsers, List<UploadImportError> errorList, int startIdx, ImportPhase importPhase) {

		int usersProcessed = 0;
		UploadImportError testDataError = bulkUploadSingleton
				.getUploadImportError(UploadImportError.USER_IN_ANOTHER_PROJECT_W_INSTRUMENT);

		for (; usersProcessed < chunkSize && (startIdx + usersProcessed < existingUsers.size()); usersProcessed++) {
			ImportUser existingUser = existingUsers.get(startIdx + usersProcessed);
			ImportUser importUser = usersToImport.get(existingUser.getEmailAddress().toLowerCase());

			if (importUser != null && existingUser.hasTestData()) {
				UploadImportJobError jobError = new UploadImportJobError();
				jobError.setUploadImportJobLog(jobLog);
				jobError.setUploadImportError(testDataError);
				jobError.setSheetName("Participants");
				jobError.setRowId(importUser.getExcelRowId());
				jobError.setDetails(existingUser.getTestDataDetail());
				jobError.setImportPhase(importPhase);
				uploadImportJobErrorDao.create(jobError);

				errorList.add(testDataError);
			}
		}

		return usersProcessed;
	}

	private UploadImportError persistValidationError(UploadImportJobLog jobLog, ExcelError error, String sheetName,
			String columnName, Integer rowId, ImportPhase importPhase) {

		UploadImportError importError = bulkUploadSingleton.getUploadImportError(error.getError());
		UploadImportJobError jobError = new UploadImportJobError();
		jobError.setUploadImportJobLog(jobLog);
		jobError.setUploadImportError(importError);
		jobError.setSheetName(sheetName);
		jobError.setColumnName(columnName);
		jobError.setRowId(rowId);

		if (rowId != null && error.getDetail() == null) {
			jobError.setDetails(error.getDataValue());
		} else {
			jobError.setDetails(error.getDetail());
		}

		jobError.setImportPhase(importPhase);
		uploadImportJobErrorDao.create(jobError);
		return importError;
	}

	private void recordJobError(UploadImportJob job, UploadImportJobLog jobLog, DocumentValidation validation,
			boolean isImport, String errorDetail, ImportPhase importPhase) {

		List<UploadImportError> errorList = persistFileValidationErrors(validation, jobLog, importPhase);
		updateErrorCounts(jobLog, errorList, isImport);
		bulkUploadService.recordJobError(job.getUploadImportJobId(), errorDetail);

		// TODO: Do we want to call back to the import poller?
	}

	private void updateErrorCounts(UploadImportJobLog jobLog, List<UploadImportError> errorList, boolean isImport) {
		int severeErrorCount = 0;
		int warningErrorCount = 0;
		int infoErrorCount = 0;

		int globalErrorCount = 0;
		int sheetErrorCount = 0;
		int rowErrorCount = 0;

		for (UploadImportError error : errorList) {
			if (error.getErrorSeverity().getCode().equals(ErrorSeverity.SEVERE)) {
				severeErrorCount++;
			} else if (error.getErrorSeverity().getCode().equals(ErrorSeverity.WARNING)) {
				warningErrorCount++;
			} else if (error.getErrorSeverity().getCode().equals(ErrorSeverity.INFO)) {
				infoErrorCount++;
			}

			if (error.getErrorType().getCode().equals(ErrorType.GLOBAL)) {
				globalErrorCount++;
			} else if (error.getErrorType().getCode().equals(ErrorType.SHEET)) {
				sheetErrorCount++;
			} else if (error.getErrorType().getCode().equals(ErrorType.ROW)) {
				rowErrorCount++;
			}
		}

		if (isImport) {
			jobLog.setImportSevereErrorCount(severeErrorCount);
			jobLog.setImportWarningErrorCount(warningErrorCount);
			jobLog.setImportInfoErrorCount(infoErrorCount);
			jobLog.setImportGlobalErrors(globalErrorCount);
			jobLog.setImportSheetErrors(sheetErrorCount);
			jobLog.setImportRowErrors(rowErrorCount);

		} else {
			jobLog.setValidationSevereErrorCount(severeErrorCount);
			jobLog.setValidationWarningErrorCount(warningErrorCount);
			jobLog.setValidationInfoErrorCount(infoErrorCount);
			jobLog.setValidationGlobalErrors(globalErrorCount);
			jobLog.setValidationSheetErrors(sheetErrorCount);
			jobLog.setValidationRowErrors(rowErrorCount);
		}

		uploadImportJobLogDao.update(jobLog);
	}

	@TransactionAttribute(TransactionAttributeType.NEVER)
	private ImportCountTracker performImports(UploadImportJob job, int userId, Map<String, ImportUser> usersToImport,
			List<ImportUser> existingPalmsUsers, List<ImportUser> existingProjectUsers,
			Map<String, CustomFieldDto> customFieldMap, List<ImportUser> invalidEmailUsers, int jobLogId) {

		Project project = null;
		Company company = null;

		if (job.getProjectId() != null) {
			project = projectDao.findById(job.getProjectId());
			company = project.getCompany();
		} else {
			company = companyDao.findById(job.getCompanyId());
		}

		ImportCountTracker tracker = new ImportCountTracker();
		int idx = 0;

		while (idx < existingProjectUsers.size()) {
			idx += updateExistingUsers(job, company, project, userId, usersToImport, existingProjectUsers,
					customFieldMap, tracker, true, idx);

			bulkUploadService.updateImportCounts(jobLogId, tracker.totalRecordsErrored, tracker.totalRecordsExisting,
					tracker.totalRecordsAdded, tracker.totalRecordsUpdated, tracker.totalRecordsUpdatedAndAdded,
					tracker.totalRecordsImported);
		}

		idx = 0;
		while (idx < existingPalmsUsers.size()) {
			idx += updateExistingUsers(job, company, project, userId, usersToImport, existingPalmsUsers,
					customFieldMap, tracker, false, idx);

			bulkUploadService.updateImportCounts(jobLogId, tracker.totalRecordsErrored, tracker.totalRecordsExisting,
					tracker.totalRecordsAdded, tracker.totalRecordsUpdated, tracker.totalRecordsUpdatedAndAdded,
					tracker.totalRecordsImported);
		}

		idx = 0;
		List<ImportUser> newUsers = new ArrayList<ImportUser>();
		newUsers.addAll(usersToImport.values());
		while (idx < newUsers.size()) {
			idx += addNewUsers(job, company, project, userId, newUsers, customFieldMap, tracker, idx);

			bulkUploadService.updateImportCounts(jobLogId, tracker.totalRecordsErrored, tracker.totalRecordsExisting,
					tracker.totalRecordsAdded, tracker.totalRecordsUpdated, tracker.totalRecordsUpdatedAndAdded,
					tracker.totalRecordsImported);
		}

		idx = 0;
		while (idx < invalidEmailUsers.size()) {
			idx += recordInvalidEmails(job, company, userId, invalidEmailUsers, idx);
		}

		return tracker;
	}

	private int updateExistingUsers(UploadImportJob job, Company company, Project project, int userId,
			Map<String, ImportUser> usersToImport, List<ImportUser> existingUsers,
			Map<String, CustomFieldDto> customFieldMap, ImportCountTracker tracker, boolean inProject, int startIdx) {

		List<ImportUser> erroredUsers = new ArrayList<ImportUser>();
		List<ImportUser> noActionUsers = new ArrayList<ImportUser>();
		List<ImportUser> palmsUpdateUsers = new ArrayList<ImportUser>();
		List<ImportUser> uffdaUpdateUsers = new ArrayList<ImportUser>();

		String noUpdatesImportAction = (inProject ? ImportAction.DUPLICATE_NO_ACTION
				: ImportAction.DUPLICATE_ADDED_TO_PROJECT);

		String updatesImportAction = (inProject ? ImportAction.DUPLICATE_UPDATED
				: ImportAction.DUPLICATE_UPDATED_AND_ADDED);

		int usersProcessed = 0;
		for (; usersProcessed < chunkSize && (startIdx + usersProcessed < existingUsers.size()); usersProcessed++) {
			ImportUser existingUser = existingUsers.get(startIdx + usersProcessed);

			// Remove this user from the usersToImport map, so we don't try to
			// import a duplicate record later on.
			ImportUser importUser = usersToImport.remove(existingUser.getEmailAddress().toLowerCase());
			importUser.setUserId(existingUser.getUserId());

			if (!importUser.canImport()) {
				// Can't import.
				tracker.incrementErrorCount();
				erroredUsers.add(importUser);

			} else if (importUser.getCoreFieldMismatches().isEmpty() && importUser.getCustomFieldMismatches().isEmpty()) {
				// Nothing to do.
				if (inProject) {
					tracker.incrementExistingCount();
				} else {
					tracker.incrementAddedCount();
				}
				noActionUsers.add(importUser);

			} else {
				if (!importUser.getCoreFieldMismatches().isEmpty()) {
					palmsUpdateUsers.add(importUser);
				}

				if (!importUser.getCustomFieldMismatches().isEmpty()) {
					uffdaUpdateUsers.add(importUser);
				}

				if (inProject) {
					tracker.incrementUpdatedCount();
				} else {
					tracker.incrementUpdatedAndAddedCount();
				}
			}
		}

		// Updates data in separate databases, so we needs separate lists and
		// separate method calls.
		updatePalmsUserData(palmsUpdateUsers);
		updateUffdaUserData(userId, job, uffdaUpdateUsers, customFieldMap);

		// Now that updates are done, merge the update lists. There may be
		// overlap, so dump both lists into a set.
		Set<ImportUser> updateUsers = new HashSet<ImportUser>();
		updateUsers.addAll(palmsUpdateUsers);
		updateUsers.addAll(uffdaUpdateUsers);

		if (project != null && !inProject) {
			addUsersToProject(project, noActionUsers, updateUsers);
		}

		addUploadImportRecords(job, userId, updateUsers, updatesImportAction);
		addUploadImportRecords(job, userId, noActionUsers, noUpdatesImportAction);
		addUploadImportRecords(job, userId, erroredUsers, ImportAction.SEVERE_ERROR);
		return usersProcessed;
	}

	private int addNewUsers(UploadImportJob job, Company company, Project project, int userId,
			List<ImportUser> newUsers, Map<String, CustomFieldDto> customFieldMap, ImportCountTracker tracker,
			int startIdx) {

		List<ImportUser> erroredUsers = new ArrayList<ImportUser>();
		List<ImportUser> addedUsers = new ArrayList<ImportUser>();

		int usersProcessed = 0;
		for (; usersProcessed < chunkSize && (startIdx + usersProcessed < newUsers.size()); usersProcessed++) {
			ImportUser user = newUsers.get(startIdx + usersProcessed);
			if (!user.canImport()) {
				// Can't import.
				tracker.incrementErrorCount();
				erroredUsers.add(user);
			} else {
				tracker.incrementImportedCount();
				addedUsers.add(user);
			}
		}

		addPalmsUserData(company, project, addedUsers);
		addUffdaUserData(userId, job, addedUsers, customFieldMap);
		addUploadImportRecords(job, userId, addedUsers, ImportAction.NEW_USER_ADDED);
		addUploadImportRecords(job, userId, erroredUsers, ImportAction.SEVERE_ERROR);

		return usersProcessed;
	}

	private int recordInvalidEmails(UploadImportJob job, Company company, int userId,
			List<ImportUser> invalidEmailUsers, int startIdx) {
		List<ImportUser> erroredUsers = new ArrayList<ImportUser>();
		int recordsProcessed = 0;
		for (; recordsProcessed < chunkSize && (startIdx + recordsProcessed < invalidEmailUsers.size()); recordsProcessed++) {
			erroredUsers.add(invalidEmailUsers.get(startIdx + recordsProcessed));
		}

		addUploadImportRecords(job, userId, erroredUsers, ImportAction.SEVERE_ERROR);

		return recordsProcessed;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void addPalmsUserData(Company company, Project project, List<ImportUser> addedUsers) {
		for (ImportUser newUser : addedUsers) {
			User user = new User();
			user.setCompany(company);
			user.setFirstname(newUser.getFirstName());
			user.setLastname(newUser.getLastName());
			user.setEmail(newUser.getEmailAddress());
			user.setSupervisorName(newUser.getSupervisorName());
			user.setSupervisorEmail(newUser.getSupervisorEmail());
			user.setLangPref(newUser.getLanguageCode());
			user.setOptional1(newUser.getOptional1());
			user.setOptional2(newUser.getOptional2());
			user.setOptional3(newUser.getOptional3());

			if (StringUtils.isNotBlank(newUser.getLogonID())) {
				user.setUsername(newUser.getLogonID());
			} else {
				user.setUsername(usernameService.generateUsername(newUser.getEmailAddress(), newUser.getFirstName(),
						newUser.getLastName()));
			}

			if (StringUtils.isNotBlank(newUser.getPassword())) {
				user.setPassword(newUser.getPassword());
			} else {
				user.setPassword(createUserService.getNewPassword());
			}

			AddUserResultObj result = (AddUserResultObj) createUserService.validateUser(user);
			if (!result.getSuccess()) {
				// TODO: Handle this.
			}

			createUserService.createNewUser(user, company, project, null, null);
			newUser.setUserId(user.getUsersId());

			if (!StringUtils.isBlank(newUser.getBusinessPhone())) {
				createNewBusinessPhoneForUser(user, newUser.getBusinessPhone());
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void updatePalmsUserData(List<ImportUser> updateUsers) {

		for (ImportUser updateUser : updateUsers) {

			String logonID = null;
			String password = null;
			String updateLogonFields = null;

			User user = userDao.findById(updateUser.getUserId());
			for (String key : updateUser.getCoreFieldMismatches()) {

				if (key.equals(ImportUser.FIRST_NAME)) {
					user.setFirstname(updateUser.getFirstName());

				} else if (key.equals(ImportUser.LAST_NAME)) {
					user.setLastname(updateUser.getLastName());

					// We don't update email address

				} else if (key.equals(ImportUser.BUSINESS_PHONE)) {
					PhoneNumber businessNumber;
					List<PhoneNumber> numbers = phoneNumberDao.findByUsersIdAndType(updateUser.getUserId(),
							PhoneNumberType.BUISINESS_PHONE_ABBV);
					if (numbers != null && !numbers.isEmpty()) {
						businessNumber = numbers.get(0);
						businessNumber.setNumber(updateUser.getBusinessPhone());
						businessNumber.setPrimary(true);
						phoneNumberDao.update(businessNumber);

					} else {
						createNewBusinessPhoneForUser(user, updateUser.getBusinessPhone());
					}

				} else if (key.equals(ImportUser.SUPERVISOR_NAME)) {
					user.setSupervisorName(updateUser.getSupervisorName());

				} else if (key.equals(ImportUser.SUPERVISOR_EMAIL)) {
					user.setSupervisorEmail(updateUser.getSupervisorEmail());

				} else if (key.equals(ImportUser.LANGUAGE)) {
					user.setLangPref(updateUser.getLanguageCode());

				} else if (key.equals(ImportUser.OPTIONAL_1)) {
					user.setOptional1(updateUser.getOptional1());

				} else if (key.equals(ImportUser.OPTIONAL_2)) {
					user.setOptional2(updateUser.getOptional2());

				} else if (key.equals(ImportUser.OPTIONAL_3)) {
					user.setOptional3(updateUser.getOptional3());

				} else if (key.equals(ImportUser.LOGON_ID)) {
					logonID = updateUser.getLogonID();

				} else if (key.equals(ImportUser.PASSWORD)) {
					password = updateUser.getPassword();

				} else if (key.equals(ImportUser.UPDATE_LOGON_FIELDS)) {
					updateLogonFields = updateUser.getUploadLogonFields();
				}
			}

			if (StringUtils.isNotBlank(updateLogonFields) && updateLogonFields.equalsIgnoreCase("yes")) {
				if (StringUtils.isNotBlank(logonID)) {
					user.setUsername(logonID);
				}
				if (StringUtils.isNotBlank(password)) {
					user.setPassword(password);
					resetPassword(user);
				}
			}

			userDao.update(user);
		}
	}

	private void resetPassword(User currentUser) {
		if (currentUser.getRowguid() != null) {

			currentUser.setHashedpassword(createUserService.generateHashedPassword(currentUser.getPassword(),
					currentUser.getRowguid().substring(0, 10).toUpperCase()));
			currentUser.setLoginStatus((short) 0);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void addUffdaUserData(int userId, UploadImportJob job, List<ImportUser> addedUsers,
			Map<String, CustomFieldDto> customFieldMap) {

		// NOTE: This assumes we always have a non-NULL project ID.
		for (ImportUser user : addedUsers) {
			CustomFieldsVo view = new CustomFieldsVo();

			for (Map.Entry<String, CustomFieldDto> entry : customFieldMap.entrySet()) {

				Object value = user.getCustomFieldValue(entry.getKey(), entry.getValue());
				if (value != null) {
					view.addField(getCustomFieldViewFor(entry.getValue(), value));
				}
			}

			uffdaService.persistCustomFields(userId, job.getProjectId(), FieldEntityType.PALMS_PROJECT_USERS,
					user.getUserId(), ValueEntityType.PROJECT_USER_ID, view, job.getCompanyId(), true);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void updateUffdaUserData(int userId, UploadImportJob job, List<ImportUser> uffdaUpdateUsers,
			Map<String, CustomFieldDto> customFieldMap) {

		// NOTE: This assumes we always have a non-NULL project ID.
		for (ImportUser user : uffdaUpdateUsers) {

			CustomFieldsVo view = new CustomFieldsVo();
			for (String key : user.getCustomFieldMismatches()) {

				CustomFieldDto dto = customFieldMap.get(key);
				Object value = user.getCustomFieldValue(key, dto);
				if (value != null) {
					view.addField(getCustomFieldViewFor(dto, value));
				}
			}

			for (CustomFieldVo customField : view.getFields()) {
				log.trace("{}: {}", customField.getCode(), customField.getValue());
			}
			uffdaService.persistCustomFields(userId, job.getProjectId(), FieldEntityType.PALMS_PROJECT_USERS,
					user.getUserId(), ValueEntityType.PROJECT_USER_ID, view, job.getCompanyId(), true);
		}
	}

	private CustomFieldVo getCustomFieldViewFor(CustomFieldDto dto, Object value) {
		CustomFieldVo fieldView = new CustomFieldVo();
		fieldView.setId(dto.getDatabaseId());
		fieldView.setFieldType(fieldTypeMapper.getViewByName(dto.getInputTypeName()));
		fieldView.setValueId(dto.getValueId());

		if (fieldView.isListValue()) {
			fieldView.setValue(dto.findListValueIdByValue(value.toString()));
		} else {
			fieldView.setValue(value);
		}

		return fieldView;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void addUsersToProject(Project project, Iterable<ImportUser> users1, Iterable<ImportUser> users2) {
		List<Integer> userIds = new ArrayList<Integer>();

		// All users in both collections exist in PALMS. Add them to the
		// project.
		for (ImportUser user : users1) {
			userIds.add(user.getUserId());
		}

		for (ImportUser user : users2) {
			userIds.add(user.getUserId());
		}

		List<User> usersToAddToProject = userDao.findUsersInList(userIds);
		List<ProjectCourse> projectCourses = project.getProjectCourses();

		for (User user : usersToAddToProject) {

			ProjectUser puser = projectMembershipService.addUserToProject(user, project);
			if (puser != null) {

				for (ProjectCourse course : projectCourses) {
					log.debug("Enrolling user " + user.getFirstname() + " " + user.getLastname() + " in courses");
					if (course.getSelected()) {
						user = enrollmentService.enrollUserInCourse(project.getCompany(), course, user);
						enrollmentService.activateDeactivateInstrument(project.getCompany(), course, user);
					} else {
						activateUnactivateInstrument(course, user.getPositions());
					}
				}
			}
		}
	}

	private void createNewBusinessPhoneForUser(User user, String phoneNumnber) {
		PhoneNumber businessNumber = new PhoneNumber();
		businessNumber.setNumber(phoneNumnber);
		businessNumber.setUser(user);
		businessNumber.setPrimary(true);
		businessNumber.setPhoneNumberType(phoneNumberTypeSingleton.getTypeByAbbv(PhoneNumberType.BUISINESS_PHONE_ABBV));
		phoneNumberDao.create(businessNumber);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void addUploadImportRecords(UploadImportJob job, int userId, Iterable<ImportUser> users, String actionCode) {
		// Create an upload_import_record for each user, with the given action
		// code.
		for (ImportUser user : users) {
			UploadImportRecord record = new UploadImportRecord();
			record.setUploadImportJobId(job.getUploadImportJobId());
			record.setRowNumber(user.getExcelRowId());
			record.setCompanyId(job.getCompanyId());
			record.setProjectId(job.getProjectId());
			record.setEmail(user.getEmailAddress());
			record.setFirstName(user.getFirstName());
			record.setLastName(user.getLastName());

			// This value will be 0 for a new user record with a severe error,
			// because no import occurred. All other ImportUsers should have a
			// valid users ID.
			record.setUsersId(user.getUserId());
			record.setCreatedById(userId);
			record.setUpdatedById(userId);
			ImportAction action = bulkUploadSingleton.getImportAction(actionCode);
			if (action != null) {
				record.setImportActionId(action.getImportActionId());
			} else {
				record.setImportActionId(0);
			}

			uploadImportRecordDao.createUploadImportRecord(userId, record);
		}
	}

	private void makeCallback(int jobId, String callbackUrl) {
		Client client = Client.create();
		WebResource resource = client.resource(callbackUrl);

		Form form = new Form();
		form.add("jobid", jobId);

		try {
			String response = resource.type(MediaType.APPLICATION_FORM_URLENCODED).accept(MediaType.TEXT_PLAIN)
					.post(String.class, form);
			log.trace("Received response '{}' from Import Poller callback.", response);

		} catch (Exception exc) {
			// TODO: Log or something
			exc.printStackTrace();
		}
	}

	// TODO: This method was copied from DeliveryManagmentControllerBean in
	// lrm-web. Comment there indicates
	// this functionality should move to admin-ejb at some point --- at which
	// time, this method can be
	// deleted and that one called instead.
	private void activateUnactivateInstrument(ProjectCourse projectCourse, List<Position> positions) {
		// JJB: Making assumption then when an existing user is added to a
		// project with assessment day instruments
		// that those assessment day instrument become inactive
		for (Position position : positions) {
			if (position.getCourse().getAbbv().equals(projectCourse.getCourse().getAbbv())
					&& projectCourse.getSelected()) {

				if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 1 && !position.getActivated()) {
					log.debug("Activate deactivated instrument {}", position.getCourse().getAbbv());
					position.setActivated(true);
					positionDao.update(position);

				} else if (projectCourse.getCourseFlowType().getCourseFlowTypeId() == 2 && position.getActivated()) {
					log.debug("Deactivate activated instrument {}", position.getCourse().getAbbv());
					position.setActivated(false);
					positionDao.update(position);
				}
			}
		}
	}

	class ImportCountTracker {
		int totalRecordsErrored = 0;
		int totalRecordsImported = 0;
		int totalRecordsUpdatedAndAdded = 0;
		int totalRecordsAdded = 0;
		int totalRecordsUpdated = 0;
		int totalRecordsExisting = 0;

		void incrementErrorCount() {
			totalRecordsErrored++;
		}

		void incrementImportedCount() {
			totalRecordsImported++;
		}

		void incrementUpdatedAndAddedCount() {
			totalRecordsUpdatedAndAdded++;
		}

		void incrementAddedCount() {
			totalRecordsAdded++;
		}

		void incrementUpdatedCount() {
			totalRecordsUpdated++;
		}

		void incrementExistingCount() {
			totalRecordsExisting++;
		}

		@Override
		public String toString() {
			return "Errored: " + totalRecordsErrored + ", imported: " + totalRecordsImported + ", updated and added: "
					+ totalRecordsUpdatedAndAdded + ", added: " + totalRecordsAdded + ", updated: "
					+ totalRecordsUpdated + ", existing: " + totalRecordsExisting;
		}
	}
}
