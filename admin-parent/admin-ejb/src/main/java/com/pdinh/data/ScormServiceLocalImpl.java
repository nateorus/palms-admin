package com.pdinh.data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.instrumentresponses.Instrument;
import com.pdinh.data.instrumentresponses.InstrumentResponses;
import com.pdinh.data.instrumentresponses.Participant;
import com.pdinh.data.instrumentresponses.Response;
import com.pdinh.data.scorm.ScormCourseMap;

@Stateless
public class ScormServiceLocalImpl implements ScormServiceLocal {

	private static final Logger logger = LoggerFactory.getLogger(ScormServiceLocalImpl.class);

	@Resource(name = "jdbc/pdinh-scorm")
	private DataSource scormDataSource;

	@Override
	public InstrumentResponses getResponses(int participantId, String instrumentId) {

		//logger.info("Getting scorm responses for " + participantId + ", " + instrumentId);

		InstrumentResponses instrumentResponses = new InstrumentResponses();
		Participant participant = new Participant();
		participant.setId(participantId);
		instrumentResponses.getParticipants().add(participant);

		String scormCourseId = ScormCourseMap.getScormCourseId(instrumentId);

		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = scormDataSource.getConnection();

			cs = conn.prepareCall("{call get_interaction_view_by_course(?, ?)}");
			cs.setString(1, participantId + "");
			cs.setString(2, scormCourseId);
			// logger.info("--- Starting  get_interaction_view query");
			cs.executeQuery();
			// logger.info("--- Ending  get_interaction_view query");
			ResultSet rs = cs.executeQuery();

			Instrument instrument = new Instrument();
			instrument.setId(instrumentId);

			while (rs.next()) {
				Response response = new Response();
				String id = rs.getString("interactionId");
				String rsp = rs.getString("learnerResponse").trim();
				// Bogus data appeared to be a fluke... removed hack
				// // TODO Find a better way to detect an invalid character
				// // Right now we look for a negative number but ther has to be
				// a
				// // better way
				// int ii = -999;
				// try {
				// byte[] b = rsp.getBytes("UTF-8");
				// ii = b[0];
				// } catch (UnsupportedEncodingException e) {
				// logger.debug("Bad encoding... char=" + rsp.charAt(0) + "...("
				// + ii + ")");
				// }
				// if (rsp == null || rsp.length() < 1 || ii < 0) {
				if (rsp == null || rsp.length() < 1) {
					continue;
				}
				response.setId(id);
				response.setValue(rsp);
				instrument.getResponses().add(response);
			}
			participant.getInstruments().add(instrument);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (cs != null) {
				try {
					cs.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return instrumentResponses;
	}

	public DataSource getScormDataSource() {
		return scormDataSource;
	}

	public void setScormDataSource(DataSource scormDataSource) {
		this.scormDataSource = scormDataSource;
	}

}
