package com.pdinh.data.jaxb.lrm;

import java.util.List;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.User;

@Local
public interface AddUserServiceLocal {

	AddUserResponse addUsersToLrm(List<User> users, int projectId, int userId);
}
