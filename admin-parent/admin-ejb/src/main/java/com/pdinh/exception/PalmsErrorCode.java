package com.pdinh.exception;

public enum PalmsErrorCode {
	CANNOT_GEN_URL ("ECRPT001", "Failed to Generate report URL map"),
	BAD_REQUEST ("ECRPT002", "Invalid Report Request"),
	REPORT_SERVER_UNAVAILABLE ("ECRPT003", "A Report resource server was unavailable to service the request."),
	RCM_GEN_FAILED ("ECRPT004", "Generation of Report Content Model failed."),
	UNKNOWN_ERROR ("ECRPT005", "An Unknown Error occurred."), 
	REQUEST_NOT_FOUND ("ECRPT006", "The report request could not be located."),
	REQUEST_CANCELLED ("ECRPT007", "The report request has been cancelled."),
	REPORT_GEN_FAILED ("ECRPT008", "The report failed to generate."),
	FAILED_TO_QUEUE ("ECRPT009", "Failed to Add report request to the Queue."),
	ACTION_DISALLOWED ("ECRPT010", "The requested operation is not allowed.  See error details."),
	UPLOAD_FAILED ("ECRPT011", "The report failed to upload to the repository."),
	REPORT_GEN_TIMEOUT ("ECRPT012", "The maximum time allowed for report generation was exceeded"),
	ADD_USER_ALREADY_EXIST ("IMEC007", "A user with the provided email already exists."),
	ADD_USER_EMAIL_INVALID ("IMEC012", "The email provided was invalid"),
	ADD_USER_EMAIL_EMPTY ("IMEC008", "No email was provided."),
	ADD_USER_NO_LOGON("IMEC009", "No logon id was provided."),
	ADD_USER_LOGON_INVALID("IMEC011", "The logon id provided did not meet the requirements."),
	ADD_USER_LOGON_ALREADY_EXISTS("IMEC010", "The logon id provided is already in use in the system.");
	
	private final String message;
	private final String code;
	
	PalmsErrorCode(String code, String message){
		this.message = message;
		this.code = code;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public String getCode(){
		return this.code;
	}
}

	
