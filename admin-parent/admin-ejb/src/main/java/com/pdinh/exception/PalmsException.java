package com.pdinh.exception;

public class PalmsException extends Exception {

	public String errorCode;
	public String errorDetail;

	public PalmsException(String message) {
		super(message);
	}
	
	public PalmsException (String message, String errorCode){
		super(message);
		this.errorCode = errorCode;
	}
	
	public PalmsException (String message, String errorCode, String detail){
		super(message);
		this.errorCode = errorCode;
		this.errorDetail = detail;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}
}
