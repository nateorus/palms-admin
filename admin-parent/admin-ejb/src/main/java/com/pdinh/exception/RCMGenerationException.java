package com.pdinh.exception;

public class RCMGenerationException extends PalmsException{

	public RCMGenerationException(String message) {
		super(message, PalmsErrorCode.RCM_GEN_FAILED.getCode());
	}

}
