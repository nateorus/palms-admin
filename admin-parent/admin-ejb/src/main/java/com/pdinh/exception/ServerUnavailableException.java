package com.pdinh.exception;

public class ServerUnavailableException extends PalmsException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String serverName;
	
	public ServerUnavailableException(String serverName, String message){
		super(message);
		this.serverName = serverName;
		
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

}
