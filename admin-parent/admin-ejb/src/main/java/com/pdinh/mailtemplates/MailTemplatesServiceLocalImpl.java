package com.pdinh.mailtemplates;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailFromFieldKeyCombinationDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailLogDao;
import com.pdinh.data.ms.dao.ScheduledEmailLogStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailTextDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.mail.MailServiceLocal;
import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.EmailFromFieldKeyCombination;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailLog;
import com.pdinh.persistence.ms.entity.ScheduledEmailLogStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.presentation.domain.EmailEventObj;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.helper.TextUtils;

/**
 * Session Bean implementation class MailTemplatesServiceLocalImpl
 */
@Stateless
@LocalBean
public class MailTemplatesServiceLocalImpl implements MailTemplatesServiceLocal {

	private static final Logger log = LoggerFactory.getLogger(MailTemplatesServiceLocalImpl.class);

	@EJB
	ScheduledEmailTextDao scheduledEmailTextDao;

	@EJB
	ScheduledEmailLogDao scheduledEmailLogDao;

	@EJB
	EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	private ScheduledEmailLogStatusDao scheduledEmailLogStatusDao;

	@EJB
	private ScheduledEmailStatusDao scheduledEmailStatusDao;

	@EJB
	UserDao userDao;

	@EJB
	MailServiceLocal mailService;

	@EJB
	EmailFromFieldKeyCombinationDao emailFromFieldKeyCombinationDao;

	@Resource(mappedName = "custom/mailMerge_properties")
	private Properties mailMergeProperties;

	@Resource(mappedName = "custom/tlt_properties")
	private Properties tltProperties;

	public static final String PART_FIRST = "[partFirstName]";
	public static final String PART_LAST = "[partLastName]";
	public static final String PART_EMAIL = "[partEmailAddress]";
	public static final String PART_UID = "[partId]";
	public static final String PART_PW = "[partPassword]";
	public static final String PART_OPT1 = "[partOption1]";
	public static final String PART_OPT2 = "[partOption2]";
	public static final String PART_OPT3 = "[partOption3]";
	public static final String MGR_NAME = "[partManagerName]";
	public static final String MGR_EMAIL = "[partManagerEmail]";
	public static final String PROJECT = "[projectName]";
	public static final String CLIENT = "[clientName]";

	public static final String PART_URL = "[partUrl]";

	// additional KFAP statics for PALMS 1.5
	public static final String RECIPIENT_FIRSTLAST_NAME = "[recipientFirstLastName]";
	public static final String RECIPIENT_LASTFIRST_NAME = "[recipientLastfirstName]";
	public static final String EVENT_DUE_DATE = "[projectDueDate]";
	public static final String EVENT_DAYS_REMG = "[projectDaysRemaining]";
	public static final String ASSESSEE_NAME_KFAP = "[assesseeNameKFAP]";
	public static final String RATER_NAME = "[reviewerNameKFAP]";
	public static final String PASSWORD_RESET_URL_AP = "[passwdResetUrlAsmtPortal]";
	public static final String PASSWORD_RESET_URL_CP = "[passwdResetUrlClientPortal]";
	public static final String PARTICIPANT_NAMES_WITHIN_EVENT = "[participantsWithinEvent]";
	// more KFAP statics from the templates
	public static final String RECIPIENT_FIRST_NAME = "[recipientFirstName]";
	public static final String SITE_URL_AP = "[siteUrlAP]";
	public static final String ASSESSEE_NAME = "[assesseeName]";
	public static final String SITE_URL_CP = "[siteUrlCP]";

	// LRM PALMS 2.0 replacement tokens

	public static final String LRM_PROJECT_NAME = "[lrmProjectName]";
	public static final String LRM_SCHEDULER_NOTE = "[lrmSchedulerNote]";
	public static final String LRM_URL = "[lrmUrl]";
	public static final String SENDER_FIRST_NAME = "[senderFirstName]";
	public static final String SENDER_LAST_NAME = "[senderLastName]";
	public static final String SENDER_EMAIL = "[senderEmail]";
	public static final String SENDER_TELEPHONE = "[senderPhone]";
	public static final String ASSESSMENT_REQUESTOR_FIRST_NAME = "[assessmentRequestorFirstName]";
	public static final String ASSESSMENT_REQUESTOR_LAST_NAME = "[assessmentRequestorLastName]";
	public static final String ASSESSMENT_REQUESTOR_EMAIL = "[assessmentRequestorEmail]";
	public static final String ASSESSMENT_REQUESTOR_PHONE = "[assessmentRequestorPhone]";

	// For Self-Registration verification email
	public static final String TOKEN_STRING = "[tokenString]";
	public static final String MASKED_TO_ADDRESS = "[maskedRecipientAddress]";

	private final HashMap<String, String> replacementVariables = new HashMap<String, String>();

	/**
	 * Default constructor.
	 */
	public MailTemplatesServiceLocalImpl() {
	}

	public void sendTemplate(Integer scheduledEmailTextId, ParticipantObj participant, Project project,
			Company company, String participantUrl) {
		EmailEventObj event = new EmailEventObj();
		event.setProject(project);
		event.setCompany(company);
		event.setParticipant(participant);
		event.setRecipientEmailAddress(participant.getEmail());
		event.setRecipientFirstLastName(participant.getFirstName() + " " + participant.getLastName());
		event.setRecipientLastFirstName(participant.getLastName() + " " + participant.getFirstName());
		event.setParticipantUrl(participantUrl);
		getMergeFieldData(event);
		sendTemplate(scheduledEmailTextId, event);

	}

	public void sendTemplate(Integer scheduledEmailTextId, ParticipantObj participant, Project project,
			Company company, String participantUrl, String eventDaysRemain, String raterName, String passwordUrlAp,
			String passwordUrlCp, List<String> participantsInEvent) {
		EmailEventObj event = new EmailEventObj();
		event.setProject(project);
		event.setCompany(company);
		event.setParticipant(participant);
		event.setRecipientEmailAddress(participant.getEmail());
		event.setRecipientFirstLastName(participant.getFirstName() + " " + participant.getLastName());
		event.setRecipientLastFirstName(participant.getLastName() + " " + participant.getFirstName());
		event.setParticipantUrl(participantUrl);
		event.setPasswordResetURLAssmtPortal(passwordUrlAp);
		event.setPasswordResetURLClientPortal(passwordUrlCp);
		event.setRaterName(raterName);
		event.setParticipantsInEvent(participantsInEvent);
		getMergeFieldData(event);

		sendTemplate(scheduledEmailTextId, event);

	}

	public EmailEventObj getTemplate(ScheduledEmailText text, EmailEventObj event) {

		String customFrom = text.getScheduledEmailDef().getCustomFrom();
		String customReplyTo = text.getScheduledEmailDef().getCustomReplyTo();
		EmailFromFieldKeyCombination fromKeyComb = emailFromFieldKeyCombinationDao.findKeyCombinationByKeyCode(text
				.getScheduledEmailDef().getFromKeyCode());

		if (fromKeyComb == null) {
			if (customFrom == null && customReplyTo == null) {
				// Getting default sender key combination.
				fromKeyComb = emailFromFieldKeyCombinationDao
						.findKeyCombinationByKeyCode(EmailFromFieldKeyCombination.GSC);
				customFrom = fromKeyComb.getSenderEmail();
				customReplyTo = fromKeyComb.getSenderEmail();
			}
		} else {
			customFrom = fromKeyComb.getSenderEmail();
			customReplyTo = fromKeyComb.getSenderEmail();
		}

		if (customFrom == null) {
			customFrom = "";
		}
		if (customReplyTo == null) {
			customReplyTo = "";
		}

		log.trace("customFrom: {}", customFrom);
		log.trace("customReplyTo: {}", customReplyTo);

		event.setClientName(event.getCompany().getCoName());

		if (!(event.getProject() == null)) {
			event.setProjectName(event.getProject().getName());
			// TODO: format this date
			if (event.getProject().getDueDate() != null) {
				String s = new SimpleDateFormat("MMM dd, yyyy").format(event.getProject().getDueDate());
				event.setEventDueDate(s);

			}
		} else {
			log.debug("Project is Null.");
		}

		log.trace("Email Text: {}", text.getText());
		String cleanText = TextUtils.getHtmlText(text.getText());
		text.setText(cleanText);

		String sendSubject = replaceText(text.getSubject(), event, false);
		String sendMessage = replaceText(text.getText(), event, false);

		// Storing copy without password
		String storeSubject = replaceText(text.getSubject(), event, true);
		String storeMessage = replaceText(text.getText(), event, true);

		log.trace("Email Text Replaced: {}", sendMessage);

		event.setSendText(sendMessage);
		event.setSendSubject(sendSubject);
		event.setStoreSubject(storeSubject);
		event.setStoreText(storeMessage);
		event.setFromAddress(customFrom);
		event.setScheduledEmailText(text);
		// email.setToAddress(event.getParticipant().getEmail());
		event.setRaterName(customReplyTo);

		return event;
	}

	public boolean sendTemplateInternal(Integer scheduledEmailTextId, EmailEventObj event) {
		ScheduledEmailText scheduledEmailText = scheduledEmailTextDao.findById(scheduledEmailTextId);
		// event.setClientName(event.getCompany().getCoName());

		if (!(event.getProject() == null)) {
			event.setProjectName(event.getProject().getName());
			// TODO: format this date
			if (event.getProject().getDueDate() != null) {
				String s = new SimpleDateFormat("MMM dd, yyyy").format(event.getProject().getDueDate());
				event.setEventDueDate(s);

			}
		} else {
			log.debug("Project is Null.");
		}

		log.trace("Email Text: {}", scheduledEmailText.getText());
		String cleanText = TextUtils.getHtmlText(scheduledEmailText.getText());
		scheduledEmailText.setText(cleanText);

		String sendSubject = replaceText(scheduledEmailText.getSubject(), event, false);
		String sendMessage = replaceText(scheduledEmailText.getText(), event, false);

		log.trace("Email Text Replaced: {}", sendMessage);

		if (mailService.sendMessage(sendSubject, event.getSenderEmail(), event.getRecipientEmailAddress(),
				event.getCcEmailAddress(), event.getBccEmailAddress(), sendMessage)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean sendTemplate(Integer scheduledEmailTextId, EmailEventObj event) {
		log.debug("looking up scheduled email text by id: {}", scheduledEmailTextId);
		ScheduledEmailText scheduledEmailText = scheduledEmailTextDao.findById(scheduledEmailTextId);
		if (scheduledEmailText == null) {
			log.error("No email text found. :(");
			return false;
		}
		event = this.getMergeFieldData(event);
		String customFrom = scheduledEmailText.getScheduledEmailDef().getCustomFrom();
		String customReplyTo = scheduledEmailText.getScheduledEmailDef().getCustomReplyTo();
		EmailFromFieldKeyCombination fromKeyComb = emailFromFieldKeyCombinationDao
				.findKeyCombinationByKeyCode(scheduledEmailText.getScheduledEmailDef().getFromKeyCode());

		if (fromKeyComb == null) {
			if (customFrom == null && customReplyTo == null) {
				// Getting default sender key combination.
				fromKeyComb = emailFromFieldKeyCombinationDao
						.findKeyCombinationByKeyCode(EmailFromFieldKeyCombination.GSC);
				customFrom = fromKeyComb.getSenderEmail();
				customReplyTo = fromKeyComb.getSenderEmail();
			}
		} else {
			customFrom = fromKeyComb.getSenderEmail();
			customReplyTo = fromKeyComb.getSenderEmail();
		}

		if (customFrom == null) {
			customFrom = "";
		}
		if (customReplyTo == null) {
			customReplyTo = "";
		}

		log.trace("customFrom: {}", customFrom);
		log.trace("customReplyTo: {}", customReplyTo);
		if (!(event.getCompany() == null)) {
			event.setClientName(event.getCompany().getCoName());
		}

		if (!(event.getProject() == null)) {
			event.setProjectName(event.getProject().getName());
			// TODO: format this date
			if (event.getProject().getDueDate() != null) {
				String s = new SimpleDateFormat("MMM dd, yyyy").format(event.getProject().getDueDate());
				event.setEventDueDate(s);

			}
		} else {
			log.debug("Project is Null.");
		}

		log.trace("Email Text: {}", scheduledEmailText.getText());
		String cleanText = TextUtils.getHtmlText(scheduledEmailText.getText());
		scheduledEmailText.setText(cleanText);

		String sendSubject = replaceText(scheduledEmailText.getSubject(), event, false);
		String sendMessage = replaceText(scheduledEmailText.getText(), event, false);

		// Storing copy without password
		String storeSubject = replaceText(scheduledEmailText.getSubject(), event, true);
		String storeMessage = replaceText(scheduledEmailText.getText(), event, true);

		log.trace("Email Text Replaced: {}", sendMessage);

		ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
				ScheduledEmailLogStatus.STATUS_SENDING_EMAIL, 1);

		ScheduledEmailLog emlLog = scheduledEmailLogDao.create(event.getCompany(), event.getProject(),
				userDao.findById(event.getParticipant().getUserId()), scheduledEmailText, status, event.getSchedule(),
				storeSubject, storeMessage);

		if (mailService.sendJMSEmailMessage(sendSubject, customFrom, customReplyTo, event.getRecipientEmailAddress(),
				sendMessage, emlLog.getEmailLogId())) {

			if (event.getCompany() != null && event.getProject() != null) {
				if (scheduledEmailText.getScheduledEmailDef().getConstant().equals(ScheduledEmailStatus.FIRST_PASSWORD)
						|| scheduledEmailText.getScheduledEmailDef().getConstant()
								.equals(ScheduledEmailStatus.PARTICIPANT_REMINDER)) {
					scheduledEmailStatusDao.createOrUpdate(event.getCompany(), event.getProject(),
							userDao.findById(event.getParticipant().getUserId()), scheduledEmailText);
				}
			} else {
				log.debug(
						"Unable to update ScheduledEmailStatus because company or project is null: company = {}, project = {}",
						event.getCompany(), event.getProject());
			}

			// Log emails
			log.debug("Successfully added email to message queue {}", event.getParticipant().getEmail());

			return true;
		} else {
			log.error("Failed to add email to message queue: {}", event.getParticipant().getEmail());
			status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR,
					1);
			emlLog.setStatus(status);
			emlLog.setErrorMessage("Failed to add email to message queue");
			scheduledEmailLogDao.update(emlLog);
			return false;
		}
	}

	public Boolean sendPreviewTemplate(String subject, String from, String replyTo, String sendTo, String message) {

		if (mailService.sendMessage(subject, from, sendTo, message)) {
			// Log emails
			log.debug("Successfully added email to message queue {}", sendTo);
			return true;
		} else {
			log.error("Failed to add email to message queue: {}", sendTo);
			return false;
		}
	}

	public EmailEventObj getMergeFieldData(EmailEventObj event) {
		event.setSiteUrlAP(mailMergeProperties.getProperty("siteUrlAP", "http://default.url"));
		event.setSiteUrlCP(mailMergeProperties.getProperty("siteUrlCP", "http://default.url"));

		String lang = "en";
		if (event.getParticipant().getLanguageId() != null) {
			lang = event.getParticipant().getLanguageId();
		} else if (event.getCompany().getLangPref() != null) {
			lang = event.getCompany().getLangPref();
		}
		String emailUrl = tltProperties.getProperty("tltParticipantExperienceUrl", "http://participant.experience.url")
				+ "?lang=" + lang;
		event.setParticipantUrl(emailUrl);

		String eventDaysRmng = ""; //
		if (!(event.getProject() == null)) {
			Timestamp dueDate = event.getProject().getDueDate();
			if (dueDate != null) {
				Calendar calDueDate = Calendar.getInstance();
				calDueDate.setTimeInMillis(dueDate.getTime());

				Date now = new Date();
				Calendar today = Calendar.getInstance();
				today.setTime(now);

				Calendar date = (Calendar) today.clone();
				int daysBetween = 0;
				while (date.before(calDueDate)) {
					date.add(Calendar.DAY_OF_MONTH, 1);
					daysBetween++;
				}
				eventDaysRmng = new Integer(daysBetween).toString();
				event.setEventDaysRemain(eventDaysRmng);
			}
		}

		return event;

	}

	public String replaceText(String text, EmailEventObj event, boolean hidePassword) {
		ParticipantObj ppt = event.getParticipant();
		if (!(ppt == null)) {
			replacementVariables.put(PART_FIRST, ppt.getFirstName());
			replacementVariables.put(PART_LAST, ppt.getLastName());
			replacementVariables.put(PART_EMAIL, ppt.getEmail());
			replacementVariables.put(PART_UID, ppt.getUserName());
			if (!hidePassword) {
				replacementVariables.put(PART_PW, ppt.getPassword());
			} else {
				replacementVariables.put(PART_PW, "********");
			}

			replacementVariables.put(PART_OPT1, ppt.getOptional1());

			replacementVariables.put(PART_OPT2, ppt.getOptional2());
			replacementVariables.put(PART_OPT3, ppt.getOptional3());
			replacementVariables.put(MGR_NAME, ppt.getSupervisorName());
			replacementVariables.put(MGR_EMAIL, ppt.getSupervisorEmail());

			replacementVariables.put(ASSESSEE_NAME_KFAP, ppt.getFirstName() + " " + ppt.getLastName());

			replacementVariables.put(RECIPIENT_FIRST_NAME, ppt.getFirstName());

			replacementVariables.put(ASSESSEE_NAME, ppt.getFirstName() + " " + ppt.getLastName());

		}
		replacementVariables.put(RATER_NAME, event.getRaterName());
		replacementVariables.put(PASSWORD_RESET_URL_AP, event.getPasswordResetURLAssmtPortal());
		replacementVariables.put(PASSWORD_RESET_URL_CP, event.getPasswordResetURLClientPortal());
		replacementVariables.put(PARTICIPANT_NAMES_WITHIN_EVENT, event.getParticipantsInEvent().toString());
		replacementVariables.put(PROJECT, event.getProjectName());
		replacementVariables.put(CLIENT, event.getClientName());
		replacementVariables.put(PART_URL, event.getParticipantUrl());
		replacementVariables.put(RECIPIENT_FIRSTLAST_NAME, event.getRecipientFirstLastName());
		replacementVariables.put(RECIPIENT_LASTFIRST_NAME, event.getRecipientLastFirstName());
		replacementVariables.put(EVENT_DUE_DATE, event.getEventDueDate());
		replacementVariables.put(EVENT_DAYS_REMG, event.getEventDaysRemain());
		replacementVariables.put(SITE_URL_AP, event.getSiteUrlAP());
		replacementVariables.put(SITE_URL_CP, event.getSiteUrlCP());
		replacementVariables.put(LRM_PROJECT_NAME, event.getLrmProjectName());
		replacementVariables.put(LRM_URL, event.getLrmUrl());
		replacementVariables.put(LRM_SCHEDULER_NOTE, event.getLrmSchedulerNote());
		replacementVariables.put(ASSESSMENT_REQUESTOR_EMAIL, event.getRequestorEmail());
		replacementVariables.put(ASSESSMENT_REQUESTOR_FIRST_NAME, event.getRequestorFirstName());
		replacementVariables.put(ASSESSMENT_REQUESTOR_LAST_NAME, event.getRequestorLastName());
		replacementVariables.put(ASSESSMENT_REQUESTOR_PHONE, event.getRequestorPhone());
		replacementVariables.put(SENDER_EMAIL, event.getSenderEmail());
		replacementVariables.put(SENDER_FIRST_NAME, event.getSenderFirstName());
		replacementVariables.put(SENDER_LAST_NAME, event.getSenderLastName());
		replacementVariables.put(SENDER_TELEPHONE, event.getSenderPhone());
		replacementVariables.put(TOKEN_STRING, event.getTokenString());
		replacementVariables.put(MASKED_TO_ADDRESS, this.maskEmailAddress(event.getRecipientEmailAddress(), "*", 2));

		for (String key : replacementVariables.keySet()) {
			if (replacementVariables.get(key) != null) {
				text = text.replace(key, replacementVariables.get(key));
			}
		}

		return text;
	}

	private String maskEmailAddress(String emailAddress, String maskCharacter, int startMask) {
		if (emailAddress != null && !emailAddress.isEmpty()) {
			String[] parts = emailAddress.split("[@]");
			if (parts.length == 2) {
				String maskText = StringUtils.repeat(maskCharacter, parts[0].length() - startMask);
				parts[0] = StringUtils.overlay(parts[0], maskText, startMask, parts[0].length());
				emailAddress = new StringBuffer(parts[0]).append("@").append(parts[1]).toString();
			}
		}
		return emailAddress;
	}
}
