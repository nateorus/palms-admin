package com.pdinh.bulkupload;

import java.io.File;
import java.io.IOException;

import javax.ejb.Local;

@Local
public interface GenerateTemplateService {

	void generateTemplate(File file, Integer projectId) throws IOException;

	boolean isDefinitionsVisible();

	void setDefinitionsVisible(boolean definitionsVisible);

	String getExcelFileFormat();

	void setExcelFileFormat(String excelFileFormat);

	void setDefaultNumberOfRows(int defaultNumberOfRows);

	void setDefaultColumnWidth(int defaultColumnWidth);

}
