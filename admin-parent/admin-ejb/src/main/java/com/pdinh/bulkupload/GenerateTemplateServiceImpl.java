package com.pdinh.bulkupload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidationHelper;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidation.ErrorStyle;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationConstraint.OperatorType;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.uffda.dto.CustomFieldDto;
import com.kf.uffda.dto.CustomFieldListValueDto;
import com.kf.uffda.dto.FieldDto;
import com.kf.uffda.service.UffdaService;
import com.pdinh.data.ms.dao.CountryDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.singleton.LanguageSingleton;
import com.pdinh.data.singleton.ProjectSetupSingleton;
import com.pdinh.persistence.ms.entity.Country;
import com.pdinh.persistence.ms.entity.Language;
import com.pdinh.persistence.ms.entity.OfficeLocation;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.presentation.domain.ProjectObj;

@Stateless
@LocalBean
public class GenerateTemplateServiceImpl implements GenerateTemplateService {

	private static final Logger log = LoggerFactory.getLogger(GenerateTemplateServiceImpl.class);

	private static final int HEADER_ROW_INDEX = 0;

	private static final int COUNTRIES_INDEX = 0;
	private static final int AVAILABLE_LANGUAGES_INDEX = 1;
	private static final int UPDATE_LOGON_FIELDS_INDEX = 2;
	// private static final int DELIVERY_OFFICE_INDEX = 2;
	private static final int YESNO_INDEX = 3;

	private static final String DEFINITION_COUNTY = "Country of Residence";
	private static final String DEFINITION_AVAILABLE_LANGUAGES = "Language";
	private static final String DEFINITION_UPDATE_LOGON_FIELDS = "Update Logon Fields";
	// private static final String DEFINITION_DELIVERY_OFFICE =
	// "Delivery Office";
	public static final String DEFAULT_SELECTION_TEXT = "--- Select One ---";
	private static final String DEFINITION_YESNO = "Yes/No";

	private LinkedHashMap<Integer, FieldDto> participantColumnNames;
	private LinkedHashMap<String, String> metadataMap;

	private int defaultNumberOfRows = 3000;
	private int defaultColumnWidth = 9000;

	private String excelFileFormat;
	private List<String> countries;
	private List<String> availableLanguages;
	private List<String> yesNo;
	private List<String> deliveryOffices;
	private ProjectObj project;
	private short headerCellStyleIndex;
	private short dateCellStyleIndex;
	private short requiredCellStyleIndex;
	private short labelCellStyleIndex;
	private short redBoldFontIndex;
	private boolean definitionsVisible;
	private boolean newExcelFormat;

	@EJB
	LanguageSingleton languageSingleton;

	@EJB
	CountryDao countryDao;

	@EJB
	ProjectDao projectDao;

	@EJB
	UffdaService uffdaService;

	@EJB
	ProjectSetupSingleton projectSetupSingleton;

	/**
	 * Default constructor.
	 */
	public GenerateTemplateServiceImpl() {
	}

	private void initialize(Integer projectId) {

		Project project = projectDao.findById(projectId);

		setProject(getProjectData(project));

		// Building Participant sheet column names list
		setParticipantColumnNames(new LinkedHashMap<Integer, FieldDto>());
		List<FieldDto> importTemplatefields = uffdaService.getPalmsImportTemplateFields(project.getCompany()
				.getCompanyId(), projectId, false, false, project.isIncludeClientLogonIdPassword());

		int index = 0;
		for (FieldDto field : importTemplatefields) {
			getParticipantColumnNames().put(index++, field);
			log.trace("Import Template fields : name = {}; required = {}", field.getName(), field.isRequired());
		}

		// Building Metadata sheet data
		setMetadataMap(new LinkedHashMap<String, String>());
		getMetadataMap().put("Key", "Value");
		getMetadataMap().put("Client Name", getProject().getClientName());
		getMetadataMap().put("Project Name", getProject().getName());
		getMetadataMap().put("Project Type", getProject().getType());
		getMetadataMap().put("Template Version", "1.0");
		getMetadataMap().put("Template Creation Date", new SimpleDateFormat("dd-MMM-yyyy").format(new Date()));

		// Dropdown's data
		setCountries(getListOfCountries());
		setAvailableLanguages(getListOfAvailableLanguages());
		setDeliveryOffices(getListOfDeliveryOffices());
		setYesNo(getListOfYesNo());

	}

	private ProjectObj getProjectData(Project project) {
		ProjectObj projectObj = null;

		if (project != null) {

			projectObj = new ProjectObj();
			projectObj.setClientName(project.getCompany().getCoName());
			projectObj.setName(project.getName());
			projectObj.setType(project.getProjectType().getName());
			projectObj.setIncludeClientLogonIdPassword(project.isIncludeClientLogonIdPassword());
			projectObj.setChqLanguageCode(projectDao.findChqLanguageCodeForProject(project.getProjectId()));
		}

		return projectObj;

	}

	@Override
	public void generateTemplate(File file, Integer projectId) throws IOException {

		Workbook workbook = null;

		initialize(projectId);

		if (getExcelFileFormat().equals("xlsx")) {
			workbook = new XSSFWorkbook();
			setNewExcelFormat(true);
		} else if (getExcelFileFormat().equals("xls")) {
			workbook = new HSSFWorkbook();
			setNewExcelFormat(false);
		}

		if (workbook != null) {

			createDateCellStyle(workbook);
			createHeaderCellStyle(workbook);
			createRequiredCellStyle(workbook);
			createLabelCellStyle(workbook);
			createRedBoldFont(workbook);

			Sheet sheetDefinitions = workbook.createSheet("Definitions");
			Sheet sheetParticipants = workbook.createSheet("Participants");

			// Hiding Definitions. Changing order of Definitions sheet to fix
			// the issue with xls.
			if (!isDefinitionsVisible()) {
				workbook.setSheetOrder("Definitions", workbook.getNumberOfSheets() - 1);
				workbook.setSheetHidden(workbook.getSheetIndex("Definitions"), Workbook.SHEET_STATE_VERY_HIDDEN);
				workbook.setSelectedTab(0);
				workbook.setActiveSheet(0);
			}

			createDefinitions(sheetDefinitions);
			createParticipant(sheetParticipants);

			Sheet sheetMetadata = workbook.createSheet("Metadata");

			createMetadata(sheetMetadata);

			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				log.trace("Sheet #{} name = {}", i, workbook.getSheetAt(i).getSheetName());
			}

			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			out.close();

			workbook = null;

			log.trace("file = {}", file.getAbsoluteFile());
		}

	}

	private void createDefinitions(Sheet targetSheet) {

		if (targetSheet == null) {
			return;
		}

		addDefinitions(targetSheet, DEFINITION_COUNTY, getCountries(), COUNTRIES_INDEX, true);
		addDefinitions(targetSheet, DEFINITION_AVAILABLE_LANGUAGES, getAvailableLanguages(), AVAILABLE_LANGUAGES_INDEX,
				false);
		addDefinitions(targetSheet, DEFINITION_YESNO, getYesNo(), YESNO_INDEX, false);
		// addDefinitions(targetSheet, DEFINITION_DELIVERY_OFFICE,
		// getDeliveryOffices(), DELIVERY_OFFICE_INDEX, true);
		// addDefinitions(targetSheet, DEFINITION_YESNO, getYesNo(),
		// YESNO_INDEX, true);

	}

	private RichTextString markAsRequired(RichTextString text, Font font) {
		int asteriskPosition = text.getString().indexOf("*");

		log.trace("asteriskPosition = {}", asteriskPosition);

		if (asteriskPosition != -1) {
			text.applyFont(asteriskPosition, asteriskPosition + 1, font);
		}

		return text;
	}

	private RichTextString convertToRichText(String text) {
		RichTextString r = null;

		if (isNewExcelFormat()) {
			r = new XSSFRichTextString(text);
		} else if (!isNewExcelFormat()) {
			r = new HSSFRichTextString(text);
		}

		return r;
	}

	private void createParticipant(Sheet targetSheet) {

		if (targetSheet == null) {
			return;
		}

		Row row = null;
		Cell cell = null;
		FieldDto field = null;
		Map<Integer, FieldDto> combinedMap = new LinkedHashMap<Integer, FieldDto>();
		List<String> listOfItems = new ArrayList<String>();
		String columnHeader = null;
		RichTextString columnRichTextHeader = null;
		int columnIndex = 0;
		Font requiredFont = null;
		CellStyle requiredCellStyle = null;

		combinedMap = getParticipantColumnNames();
		requiredFont = targetSheet.getWorkbook().getFontAt(getRedBoldFontIndex());
		requiredCellStyle = targetSheet.getWorkbook().getCellStyleAt(getRequiredCellStyleIndex());

		initialSheetSetup(targetSheet, getDefaultNumberOfRows(), combinedMap.size());

		for (int i = 0; i <= targetSheet.getLastRowNum(); i++) {
			row = targetSheet.getRow(i);
			for (Map.Entry<Integer, FieldDto> entry : combinedMap.entrySet()) {
				cell = row.getCell(columnIndex);
				field = entry.getValue();
				if (i == HEADER_ROW_INDEX) {

					columnHeader = field.getImportFieldName();

					if (field.isRequired()) {
						columnHeader = columnHeader + " *";
						cell.setCellStyle(requiredCellStyle);
					}

					columnRichTextHeader = convertToRichText(columnHeader);
					columnRichTextHeader = (field.isRequired()) ? markAsRequired(columnRichTextHeader, requiredFont)
							: columnRichTextHeader;

					cell.setCellValue(columnRichTextHeader);

					log.trace("columnHeader = {}", cell.getStringCellValue());

				} else if (i == 1) {

					if (field.isListValue()) {

						if (field instanceof CustomFieldDto) {

							listOfItems = convertFieldValues(((CustomFieldDto) field).getListValues());

							log.trace("Drop down list for {}; size = {}; default = {}", field.getCode(),
									listOfItems.toString(), getDefaultChoiceIndex(field));

							int dropdownColumnIndex = findLastColumnIndex(targetSheet.getWorkbook(), "Definitions");
							int[] position = new int[] { 1, getDefaultNumberOfRows() - 1, columnIndex, columnIndex };

							addDefinitions(targetSheet.getWorkbook().getSheet("Definitions"), field.getName(),
									listOfItems, dropdownColumnIndex, true);

							createDropDown(targetSheet, "custom_list_" + columnIndex, listOfItems, position,
									getDefaultChoiceIndex(field), field.getName());

						}

					}

				}
				columnIndex++;
			}

			columnIndex = 0;
		}

		Integer languagesColumnIndex = findColumnIndexByName(targetSheet, DEFINITION_AVAILABLE_LANGUAGES);
		Integer countryOfResidenceColumnIndex = findColumnIndexByName(targetSheet, DEFINITION_COUNTY);
		Integer updateLogonFieldsColumnIndex = findColumnIndexByName(targetSheet, DEFINITION_UPDATE_LOGON_FIELDS);
		// Integer deliveryOfficeColumnIndex =
		// findColumnIndexByName(targetSheet, DEFINITION_DELIVERY_OFFICE);

		log.trace("languagesColumnIndex = {}", languagesColumnIndex);
		log.trace("countryOfResidenceColumnIndex = {}", countryOfResidenceColumnIndex);
		// log.trace("deliveryOfficeColumnIndex = {}",
		// deliveryOfficeColumnIndex);

		if (countryOfResidenceColumnIndex != null) {
			createDropDown(targetSheet, "country_of_residence", getCountries(), new int[] { 1,
					getDefaultNumberOfRows() - 1, countryOfResidenceColumnIndex, countryOfResidenceColumnIndex }, 0,
					DEFINITION_COUNTY);
		}

		if (languagesColumnIndex != null) {
			int defaultLanguageIndex = 0;
			int index = 0;
			List<String> languages = getListOfAvailableLanguages();
			Language projectLanguage = languageSingleton.getLanguageByCode(project.getChqLanguageCode());

			if (projectLanguage != null) {
				for (String languageName : languages) {
					if (languageName.equalsIgnoreCase(projectLanguage.getName())) {
						defaultLanguageIndex = index;
						break;
					}
					index++;
				}
			}

			createDropDown(targetSheet, "available_languages", languages, new int[] { 1, getDefaultNumberOfRows() - 1,
					languagesColumnIndex, languagesColumnIndex }, defaultLanguageIndex, DEFINITION_AVAILABLE_LANGUAGES);
		}

		// Create the update logon fields dropdown.
		if (updateLogonFieldsColumnIndex != null) {
			createDropDown(targetSheet, "update_logon_fields", getYesNo(), new int[] { 1, getDefaultNumberOfRows() - 1,
					updateLogonFieldsColumnIndex, updateLogonFieldsColumnIndex }, 0, DEFINITION_YESNO);
		}

		/*
		if (deliveryOfficeColumnIndex != null) {
			createDropDown(targetSheet, "delivery_office", getDeliveryOffices(), new int[] { 1,
					getDefaultNumberOfRows() - 1, deliveryOfficeColumnIndex, deliveryOfficeColumnIndex }, 0,
					DEFINITION_DELIVERY_OFFICE);
		}
		*/
		targetSheet.setFitToPage(true);
	}

	private List<String> convertFieldValues(List<CustomFieldListValueDto> targetList) {
		List<String> finalList = new ArrayList<String>();

		for (CustomFieldListValueDto item : targetList) {
			finalList.add(item.getLabel(FieldDto.LANGUAGE_ENGLISH));
		}
		return finalList;
	}

	private int getDefaultChoiceIndex(FieldDto field) {
		int index = 0;

		if (field.isListValue()) {
			if (field instanceof CustomFieldDto) {
				for (int i = 0; i < ((CustomFieldDto) field).getListValues().size(); i++) {
					if (((CustomFieldDto) field).getDefaultChoice() == ((CustomFieldDto) field).getListValues().get(i)
							.getId()) {
						index = i + 1;
						break;
					}
				}
			}
		}
		return index;
	}

	private void createMetadata(Sheet targetSheet) {

		if (targetSheet == null) {
			return;
		}

		Row row = null;
		Cell labelCell = null;
		Cell valueCell = null;
		int rowIndex = 0;

		for (Map.Entry<String, String> entry : getMetadataMap().entrySet()) {

			row = targetSheet.createRow(rowIndex);
			labelCell = row.createCell(0);
			valueCell = row.createCell(1);

			labelCell.setCellValue(entry.getKey() + ": ");
			valueCell.setCellValue(entry.getValue());

			targetSheet.setColumnWidth(0, getDefaultColumnWidth());
			targetSheet.setColumnWidth(1, getDefaultColumnWidth());

			labelCell.setCellStyle(targetSheet.getWorkbook().getCellStyleAt(getLabelCellStyleIndex()));

			rowIndex++;

			log.trace("CreateMetadata: Key = {}; Value = {}", entry.getKey(), entry.getValue());

		}

		targetSheet.protectSheet("password");

	}

	private void addDefinitions(Sheet targetSheet, String name, List<String> items, int columnIndex, boolean isSelectOne) {

		if (targetSheet == null) {
			return;
		}

		Row row = null;
		Cell cell = null;

		// Number of items + header
		int header = 1;
		int numRows = 0;

		if (isSelectOne) {
			items.add(0, getDefaultSelectionText());
		}

		numRows = items.size() + header;

		for (int i = 0; i < numRows; i++) {
			row = (targetSheet.getRow(i) != null) ? targetSheet.getRow(i) : targetSheet.createRow(i);
			cell = (row.getCell(columnIndex) != null) ? row.getCell(columnIndex) : row.createCell(columnIndex);
			if (i == HEADER_ROW_INDEX) {
				cell.setCellValue(name);
				cell.setCellStyle(targetSheet.getWorkbook().getCellStyleAt(getHeaderCellStyleIndex()));
				targetSheet.setColumnWidth(columnIndex, getDefaultColumnWidth());
			} else {
				cell.setCellValue(items.get(i - header));
			}
		}

	}

	private void initialSheetSetup(Sheet targetSheet, int numberOfRows, int numberOfColumns) {

		if (targetSheet == null) {
			return;
		}

		Row row = null;
		Cell cell = null;
		String headerTextValidationMsg = "Modifying column header is not allowed.";

		for (int i = 0; i < numberOfRows; i++) {
			row = (targetSheet.getRow(i) != null) ? targetSheet.getRow(i) : targetSheet.createRow(i);
			for (int j = 0; j < numberOfColumns; j++) {
				cell = (row.getCell(j) != null) ? row.getCell(j) : row.createCell(j);
				if (i == HEADER_ROW_INDEX) {
					cell.setCellStyle(targetSheet.getWorkbook().getCellStyleAt(getHeaderCellStyleIndex()));
					targetSheet.setColumnWidth(j, getDefaultColumnWidth());
					makeCellReadOnly(cell, true, headerTextValidationMsg);
				}
			}
		}

		targetSheet.setDisplayZeros(false);

		// Freezing First, Last Name columns and headers row
		targetSheet.createFreezePane(2, 1);
	}

	private short findLastColumnIndex(Workbook workbook, String sheetName) {
		return workbook.getSheet(sheetName).getRow(0).getLastCellNum();
	}

	private Integer findColumnIndexByName(Sheet targetSheet, String columnName) {

		log.trace("Sheet Name: {}; # of rows = {}", targetSheet.getSheetName(), targetSheet.getLastRowNum());

		Cell cell = null;
		String cellValue = null;

		if (targetSheet != null) {

			for (int i = 0; i < targetSheet.getRow(HEADER_ROW_INDEX).getLastCellNum(); i++) {
				cell = targetSheet.getRow(HEADER_ROW_INDEX).getCell(i);
				if (cell != null) {
					cellValue = removeRequiredMark(cell.getStringCellValue());
					log.trace("Cell Value After: '{}'; Before = '{}'; Equals = '{}'", cellValue,
							cell.getStringCellValue(), columnName);

					if (StringUtils.equals(cellValue, columnName)) {
						return cell.getColumnIndex();
					}
				}
			}
		}

		return null;
	}

	private String removeRequiredMark(String source) {
		return source.replace("*", "").trim();
	}

	private void createDropDown(Sheet targetSheet, String namedRangeName, List<String> items, int[] position,
			int defaultSelectionIndex, String columnName) {

		DataValidation dataValidation = null;
		DataValidationConstraint constraint = null;
		DataValidationHelper validationHelper = null;
		CellRangeAddressList addressList = null;
		Name name = null;
		int columnIndex = 0;

		if (targetSheet.getWorkbook() instanceof XSSFWorkbook) {
			validationHelper = new XSSFDataValidationHelper((XSSFSheet) targetSheet);
		} else if (targetSheet.getWorkbook() instanceof HSSFWorkbook) {
			validationHelper = new HSSFDataValidationHelper((HSSFSheet) targetSheet);
		}

		name = targetSheet.getWorkbook().getName(namedRangeName);

		if (name == null) {
			name = targetSheet.getWorkbook().createName();
			columnIndex = findColumnIndexByName(targetSheet.getWorkbook().getSheet("Definitions"), columnName);
			name.setNameName(namedRangeName);
			name.setRefersToFormula("Definitions!$" + CellReference.convertNumToColString(columnIndex) + "$2:$"
					+ CellReference.convertNumToColString(columnIndex) + "$" + (items.size() + 1));
		}

		constraint = validationHelper.createFormulaListConstraint(namedRangeName);
		addressList = new CellRangeAddressList(position[0], position[1], position[2], position[3]);
		dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setEmptyCellAllowed(false);
		dataValidation.setErrorStyle(ErrorStyle.STOP);
		dataValidation.createErrorBox("Validation Error", "Please select a value from the list.");
		dataValidation.setShowErrorBox(true);

		targetSheet.addValidationData(dataValidation);

		Cell cell = null;

		// Setting default selection
		for (int i = 1; i <= targetSheet.getLastRowNum(); i++) {
			if (items != null) {
				cell = targetSheet.getRow(i).getCell(position[2]);
				if (cell != null) {
					cell.setCellValue(items.get(defaultSelectionIndex));
				}
			}
		}
	}

	private void makeCellReadOnly(Cell targetCell, boolean showCustomMsg, String msgText) {
		DataValidation dataValidation = null;
		DataValidationHelper validationHelper = null;
		DataValidationConstraint constraint = null;
		CellRangeAddressList addressList = null;

		if (targetCell.getSheet().getWorkbook() instanceof XSSFWorkbook) {
			validationHelper = new XSSFDataValidationHelper((XSSFSheet) targetCell.getSheet());
		} else if (targetCell.getSheet().getWorkbook() instanceof HSSFWorkbook) {
			validationHelper = new HSSFDataValidationHelper((HSSFSheet) targetCell.getSheet());
		}

		constraint = validationHelper.createTextLengthConstraint(OperatorType.BETWEEN, "1000", "5000");
		addressList = new CellRangeAddressList(targetCell.getRowIndex(), targetCell.getRowIndex(),
				targetCell.getColumnIndex(), targetCell.getColumnIndex());
		dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setEmptyCellAllowed(false);
		dataValidation.setErrorStyle(ErrorStyle.STOP);
		if (showCustomMsg) {
			dataValidation.createErrorBox("Validation Error", msgText);
			dataValidation.setShowErrorBox(true);
		}
		dataValidation.setSuppressDropDownArrow(true);

		targetCell.getSheet().addValidationData(dataValidation);
	}

	private List<String> getListOfCountries() {
		List<String> list = new ArrayList<String>();
		List<Country> countries = countryDao.findAll();

		for (Country country : countries) {
			list.add(country.getName());
		}

		Collections.sort(list);

		return list;
	}

	private List<String> getListOfAvailableLanguages() {
		List<String> list = new ArrayList<String>();
		List<Language> languages = languageSingleton.getLanguageEntities();

		for (Language language : languages) {
			list.add(language.getName());
		}

		Collections.sort(list);
		Collections.swap(list, list.indexOf("English"), 0);

		return list;
	}

	private List<String> getListOfDeliveryOffices() {
		List<String> list = new ArrayList<String>();
		List<OfficeLocation> offices = projectSetupSingleton.getOfficeLocations();

		for (OfficeLocation office : offices) {
			list.add(office.getLocation());
		}

		Collections.sort(list);

		return list;
	}

	private List<String> getListOfYesNo() {
		return new ArrayList<String>(Arrays.asList("Yes", "No"));
	}

	private void createHeaderCellStyle(Workbook workbook) {
		Font font = workbook.createFont();
		CellStyle style = workbook.createCellStyle();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		setHeaderCellStyleIndex(style.getIndex());
	}

	private void createDateCellStyle(Workbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle style = workbook.createCellStyle();
		style.setDataFormat(format.getFormat("dd-mmm-yyyy"));
		style.setAlignment(CellStyle.ALIGN_CENTER);
		setDateCellStyleIndex(style.getIndex());
	}

	private void createRequiredCellStyle(Workbook workbook) {
		Font font = workbook.createFont();
		CellStyle style = workbook.createCellStyle();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		// font.setColor(IndexedColors.WHITE.index);
		font.setColor(IndexedColors.BLACK.index);
		style.setFont(font);
		style.setAlignment(CellStyle.ALIGN_CENTER);

		if (workbook instanceof XSSFWorkbook) {
			((XSSFCellStyle) style).setFillForegroundColor(new XSSFColor(new java.awt.Color(242, 242, 242)));
		} else if (workbook instanceof HSSFWorkbook) {
			((HSSFWorkbook) workbook).getCustomPalette().setColorAtIndex(IndexedColors.DARK_BLUE.index, (byte) 242,
					(byte) 242, (byte) 242);
			((HSSFCellStyle) style).setFillForegroundColor(IndexedColors.DARK_BLUE.index);
		}
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);

		setRequiredCellStyleIndex(style.getIndex());
	}

	private void createLabelCellStyle(Workbook workbook) {
		Font font = workbook.createFont();
		CellStyle style = workbook.createCellStyle();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		setLabelCellStyleIndex(style.getIndex());
	}

	private void createRedBoldFont(Workbook workbook) {
		Font font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(IndexedColors.RED.index);
		setRedBoldFontIndex(font.getIndex());
	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public int getDefaultColumnWidth() {
		return defaultColumnWidth;
	}

	public int getDefaultNumberOfRows() {
		return defaultNumberOfRows;
	}

	@Override
	public String getExcelFileFormat() {
		return excelFileFormat;
	}

	@Override
	public void setExcelFileFormat(String excelFileFormat) {
		this.excelFileFormat = excelFileFormat;
	}

	public List<String> getCountries() {
		return countries;
	}

	public void setCountries(List<String> countries) {
		this.countries = countries;
	}

	public LinkedHashMap<Integer, FieldDto> getParticipantColumnNames() {
		return participantColumnNames;
	}

	public void setParticipantColumnNames(LinkedHashMap<Integer, FieldDto> participantColumnNames) {
		this.participantColumnNames = participantColumnNames;
	}

	public ProjectObj getProject() {
		return project;
	}

	public void setProject(ProjectObj project) {
		this.project = project;
	}

	public LinkedHashMap<String, String> getMetadataMap() {
		return metadataMap;
	}

	public void setMetadataMap(LinkedHashMap<String, String> metadataMap) {
		this.metadataMap = metadataMap;
	}

	public short getHeaderCellStyleIndex() {
		return headerCellStyleIndex;
	}

	public void setHeaderCellStyleIndex(short headerCellStyleIndex) {
		this.headerCellStyleIndex = headerCellStyleIndex;
	}

	public short getDateCellStyleIndex() {
		return dateCellStyleIndex;
	}

	public void setDateCellStyleIndex(short dateCellStyleIndex) {
		this.dateCellStyleIndex = dateCellStyleIndex;
	}

	public short getRequiredCellStyleIndex() {
		return requiredCellStyleIndex;
	}

	public void setRequiredCellStyleIndex(short requiredCellStyleIndex) {
		this.requiredCellStyleIndex = requiredCellStyleIndex;
	}

	public List<String> getYesNo() {
		return yesNo;
	}

	public void setYesNo(List<String> yesNo) {
		this.yesNo = yesNo;
	}

	public short getLabelCellStyleIndex() {
		return labelCellStyleIndex;
	}

	public void setLabelCellStyleIndex(short labelCellStyleIndex) {
		this.labelCellStyleIndex = labelCellStyleIndex;
	}

	@Override
	public boolean isDefinitionsVisible() {
		return definitionsVisible;
	}

	@Override
	public void setDefinitionsVisible(boolean definitionsVisible) {
		this.definitionsVisible = definitionsVisible;
	}

	public List<String> getAvailableLanguages() {
		return availableLanguages;
	}

	public void setAvailableLanguages(List<String> availableLanguages) {
		this.availableLanguages = availableLanguages;
	}

	public List<String> getDeliveryOffices() {
		return deliveryOffices;
	}

	public void setDeliveryOffices(List<String> deliveryOffices) {
		this.deliveryOffices = deliveryOffices;
	}

	public String getDefaultSelectionText() {
		return DEFAULT_SELECTION_TEXT;
	}

	@Override
	public void setDefaultNumberOfRows(int defaultNumberOfRows) {
		this.defaultNumberOfRows = defaultNumberOfRows;
	}

	@Override
	public void setDefaultColumnWidth(int defaultColumnWidth) {
		this.defaultColumnWidth = defaultColumnWidth;
	}

	public boolean isNewExcelFormat() {
		return newExcelFormat;
	}

	public void setNewExcelFormat(boolean newExcelFormat) {
		this.newExcelFormat = newExcelFormat;
	}

	public short getRedBoldFontIndex() {
		return redBoldFontIndex;
	}

	public void setRedBoldFontIndex(short redBoldFontIndex) {
		this.redBoldFontIndex = redBoldFontIndex;
	}

}
