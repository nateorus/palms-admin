package com.pdinh.reporting;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class PortalServletServiceImpl implements PortalServletServiceLocal {
	@Resource(name = "custom/abyd_properties")
	private Properties abydProperties;

	private static final Logger log = LoggerFactory.getLogger(PortalServletServiceImpl.class);

	@Override
	public String portalServletRequest(String inpXml) {
		String rptUrl = abydProperties.getProperty("reportingUrl");
		String respXml = "";
		BufferedReader in = null;
		try {
			// Set up a connection
			URL url = new URL(rptUrl);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			conn.setDefaultUseCaches(true);

			BufferedWriter bwOut = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
			bwOut.write("xml=" + inpXml);
			bwOut.flush();
			bwOut.close();

			// this is the xml coming back from portal servlet....
			// NOTE: GOTTA have the data typing on the reader because
			// "...streams are broken..."; they default to the local device code
			// page despite the encoding on the incoming data
			String inp;
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			while ((inp = in.readLine()) != null) {
				respXml += inp;
				// log.trace("Response = {}", inp.);
			}
			in.close();

			return respXml;
		} catch (MalformedURLException ex) {
			// Report the URL error but don't put out stuff to the RGR
			log.error("testNormListReq() - Malformed URL:  URL string= {}, msg={}", rptUrl, ex.getMessage());
			return null;
		} catch (IOException ex) {
			// Report the IOException but don't put out stuff to the RGR
			log.error("testNormListReq() - IOException:  URL string={}, msg={}", rptUrl, ex.getMessage());
			return null;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				in = null;
				e.printStackTrace();
			}
		}
	}
}
