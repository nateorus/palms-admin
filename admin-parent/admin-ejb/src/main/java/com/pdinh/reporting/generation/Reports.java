package com.pdinh.reporting.generation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Reports")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reports {
		
	@XmlElement(name="ReportRequest")
	private List<ReportRequest> reportRequests = new ArrayList<ReportRequest>();

	public Reports() {		
	}
		
	public List<ReportRequest> getReportRequests() {
		return reportRequests;
	}

	public void setReportRequests(List<ReportRequest> reportRequests) {
		this.reportRequests = reportRequests;
	}
}
