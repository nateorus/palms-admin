package com.pdinh.reporting.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.reporting.PortalServletServiceImpl;

@Stateless
@LocalBean
public class ViaEdgeScoredDataLocalImpl implements ViaEdgeScoredDataLocal {

	@Resource(name = "application/config_properties")
	private Properties configProperties;
	private static final Logger log = LoggerFactory
			.getLogger(PortalServletServiceImpl.class);

	public Map<String, String> getViaEdgeConfidences(
			List<ProjectUser> projectUsers) {
		Map<String, String> confMap = new HashMap<String, String>();
		String rptUrl = "";
		String respXml = "";
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setIgnoringElementContentWhitespace(true);
		Document xmlDoc;
		DocumentBuilder db;
		try {
			db = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			log.error(e1.getMessage());
			return null;
		}

		try {
			// setup request url, one to many participants
			if (projectUsers.size() > 1) {
				rptUrl = configProperties.getProperty("reportserverBaseUrl")
						+ "/ViaEdgeRgrServlet?type=grp&manifest=vedgeGroupManifest&pids=";
			} else {
				rptUrl = configProperties.getProperty("reportserverBaseUrl")
						+ "/ViaEdgeRgrServlet?type=indiv&manifest=vedgeIndividualManifest&pid=";
			}
			int users = projectUsers.size();
			for (int i = 0; i < users; i++) {
				rptUrl += projectUsers.get(i).getUser().getUsersId();
				if (i+1 != users) {
					rptUrl += ";";
				}
			}
			// Set up a connection
			URL url = new URL(rptUrl);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			conn.setDefaultUseCaches(true);
			// Servlet request to reportweb
			BufferedWriter bwOut = new BufferedWriter(new OutputStreamWriter(
					conn.getOutputStream()));
			bwOut.flush();
			bwOut.close();
			// Read returned stream
			String inp;
			BufferedReader in = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "UTF-8"));

			while ((inp = in.readLine()) != null) {
				respXml += inp;
				//log.trace("Response = {}", inp);
			}
			in.close();
			StringReader sr = new StringReader(respXml);
//			System.out
//					.println("respXML ########################################## = "
//							+ respXml);
			// InputSource is = new InputSource(sr);
			InputSource is = new InputSource(new ByteArrayInputStream(
					respXml.getBytes()));
			xmlDoc = db.parse(is);

		} catch (Exception e) {
			log.error("In ViaEdgeScoreDataLocalImpl.java - " + e.getMessage());
			return null;
		}

		XPath xpath = XPathFactory.newInstance().newXPath();
		// LOOP through results and create participantId,confidenceScore pairs.
		// add them to confMap
		for (ProjectUser pu : projectUsers) {
			String puRId = String.valueOf(pu.getUser().getUsersId());
			try {

				XPathExpression exprTEST = xpath
						.compile("//reportGenerationRequest/participants/participant");
				NodeList nodes = (NodeList) exprTEST.evaluate(xmlDoc,
						XPathConstants.NODESET);
				for (int i = 0; i < nodes.getLength(); i++) {
					Node var = nodes.item(i);
					NamedNodeMap nnm = var.getAttributes();
					String participantID = nnm.getNamedItem("extId").getNodeValue();
					if(puRId.equals(participantID)){
//						System.out.println("MATCHED "+puRId+" ####### " + participantID);
						if(var instanceof Element) {
						    Element docElement = (Element)var;
						    NodeList subVars = docElement.getElementsByTagName("var");//.item(0);
						    //I reversed the loop to start with the last index because the conf index should be the last one.
						    for (int j = subVars.getLength() - 1; i > -1; j--) {
						    	Node subVar = subVars.item(j);
						    	String svId = subVar.getAttributes().getNamedItem("id").getNodeValue();
						    	if(svId.equals("Confidence_index")){
						    		confMap.put(participantID, subVar.getAttributes().getNamedItem("value").getNodeValue());
						    		break;
						    	}
						    }
						}
//						System.out.println("confMap has " + String.valueOf(confMap.size()));
						break;
					}else{
						continue;
					}
					//nnm.getNamedItem("value").getNodeValue();
					
				}
			} catch (XPathExpressionException e) {
				e.printStackTrace();
				return null;
			}
		}

		return confMap;

	}
}
