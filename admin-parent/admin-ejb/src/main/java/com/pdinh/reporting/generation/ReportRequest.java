package com.pdinh.reporting.generation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.pdinh.reporting.generation.ReportParticipant;

@XmlRootElement(name = "ReportRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportRequest {

	@XmlAttribute
	private String code;

	@XmlAttribute
	private String langCode;

	@XmlAttribute
	private String hybridReport = "NO";

	@XmlAttribute
	private boolean showName;
	@XmlAttribute
	private boolean showLci;
	@XmlAttribute
	private String axisLabels;
	@XmlAttribute
	private String sortId;

	@XmlElement(name = "Override")
	@XmlElementWrapper(name = "Overrides")
	private List<Override> overrides;

	@XmlElement(name = "Participant")
	@XmlElementWrapper(name = "Participants")
	private List<ReportParticipant> reportParticipants = new ArrayList<ReportParticipant>();

	public ReportRequest() {
	}

	public ReportRequest(String code) {
		this.code = code;
	}

	public ReportRequest(String code, String langCode) {
		this.code = code;
		this.langCode = langCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getHybridReport() {
		return this.hybridReport;
	}

	public void setHybridReport(String value) {
		this.hybridReport = value;
	}

	public boolean getShowName() {
		return this.showName;
	}

	public void setShowName(boolean value) {
		this.showName = value;
	}

	public boolean getShowLci() {
		return this.showLci;
	}

	public void setShowLci(boolean value) {
		this.showLci = value;
	}

	public String getAxisLabels() {
		return this.axisLabels;
	}

	public void setAxisLabels(String value) {
		this.axisLabels = value;
	}

	public String getSortId() {
		return this.sortId;
	}

	public void setSortId(String value) {
		this.sortId = value;
	}

	public List<Override> getOverrides() {
		if (overrides == null) {
			overrides = new ArrayList<Override>();
		}
		return overrides;
	}

	public void setOverrides(List<Override> overrides) {
		this.overrides = overrides;
	}

	public List<ReportParticipant> getReportParticipants() {
		return reportParticipants;
	}

	public void setReportParticipants(List<ReportParticipant> reportParticipants) {
		this.reportParticipants = reportParticipants;
	}

}
