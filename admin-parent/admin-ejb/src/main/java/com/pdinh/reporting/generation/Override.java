package com.pdinh.reporting.generation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Override")
@XmlAccessorType(XmlAccessType.FIELD)
public class Override {

	@XmlAttribute
	private String key;
	
	@XmlAttribute
	private String value;
	
	public Override() {		
	}
	
	public Override(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
