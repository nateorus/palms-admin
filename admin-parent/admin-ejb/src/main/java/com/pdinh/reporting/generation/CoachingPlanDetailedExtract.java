package com.pdinh.reporting.generation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "coachingPlanExtractRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoachingPlanDetailedExtract {

	@XmlElement(name = "participants")
	private Participants participants = new Participants();

	public CoachingPlanDetailedExtract() {
	}

	public Participants getParticipants() {
		return participants;
	}

	public void setParticipants(Participants participants) {
		this.participants = participants;
	}

}
