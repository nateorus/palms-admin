package com.pdinh.reporting;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.pdinh.exception.PalmsException;
import com.pdinh.exception.ServerUnavailableException;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.reporting.generation.CoachingPlanDetailedExtract;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.Reports;

@Local
public interface ReportingServiceLocal {

	String generationReportXml(Reports reports);

	String generationParticipantsXml(Participants participants);

	AggregatedReportStream getReports(List<Map<String, String>> urls);
	
	GeneratedReportData getReportStream(Map<String, String> url, boolean isHybrid) throws PalmsException;

	String generationCoachingPlanDetailedExtractXml(CoachingPlanDetailedExtract coachingPlanDetailedExtract);

	List<Map<String, String>> generateReportRequestUrlMaps(List<ParticipantObj> participants, 
			List<ReportTypeObj> reportTypes, String projectId) throws PalmsException;
}
