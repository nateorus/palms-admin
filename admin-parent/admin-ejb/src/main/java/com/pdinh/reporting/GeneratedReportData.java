package com.pdinh.reporting;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class GeneratedReportData {
	private InputStream inStream = null;
	private String hdrStr = null;
	private Map<String, List<String>> headerFields = null;
	private int contentLength = 0;
	private String contentType = null;

	//
	// Constructor
	//
	public GeneratedReportData() {
		// do nothing
	}

	//
	// Instance methods
	//
	public void clear() {
		this.setHdrStr(null);
		this.setInStream(null);
		this.setHeaderFields(null);
	}

	// -----------------------
	public InputStream getInSream() {
		return this.inStream;
	}

	public void setInStream(InputStream val) {
		this.inStream = val;
	}

	// -----------------------
	public String getHdrStr() {
		return this.hdrStr;
	}

	public void setHdrStr(String val) {
		this.hdrStr = val;
	}

	public Map<String, List<String>> getHeaderFields() {
		return headerFields;
	}

	public void setHeaderFields(Map<String, List<String>> headerFields) {
		this.headerFields = headerFields;
	}

	public int getContentLength() {
		return contentLength;
	}

	public void setContentLength(int contentLength) {
		this.contentLength = contentLength;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
