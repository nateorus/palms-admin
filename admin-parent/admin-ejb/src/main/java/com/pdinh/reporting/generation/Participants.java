package com.pdinh.reporting.generation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "participants")
@XmlAccessorType(XmlAccessType.FIELD)
public class Participants {

	@XmlElement(name = "participant")
	private final List<ReportParticipant> participants = new ArrayList<ReportParticipant>();

	public List<ReportParticipant> getParticipants() {
		return participants;
	}

}
