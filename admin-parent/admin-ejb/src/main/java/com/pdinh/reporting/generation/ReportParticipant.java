package com.pdinh.reporting.generation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Participant")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportParticipant {

	@XmlAttribute
	private String participantId;

	@XmlAttribute
	private String projectId;

	@XmlAttribute
	private String projectName;

	@XmlAttribute
	private String langCode;
	
	@XmlAttribute
	private int defaultProjectTargetLevelIndex;
	
	@XmlAttribute
	private String courseVersionMap;

	// make it private so that the 3 parameter constructor is the only way to
	// make one of these (for now)
	private ReportParticipant() {
	}

	public ReportParticipant(String participantId, String projectId, String projectName) {
		this.participantId = participantId;
		this.projectId = projectId;
		this.projectName = projectName;
	}
	
	public ReportParticipant(String participantId, String projectId, String projectName, String courseVersionMap) {
		this.participantId = participantId;
		this.projectId = projectId;
		this.projectName = projectName;
		this.courseVersionMap = courseVersionMap;
	}

	public String getParticipantId() {
		return participantId;
	}

	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String value) {
		this.projectName = value;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public int getDefaultProjectTargetLevelIndex() {
		return defaultProjectTargetLevelIndex;
	}

	public void setDefaultProjectTargetLevelIndex(int defaultProjectTargetLevelIndex) {
		this.defaultProjectTargetLevelIndex = defaultProjectTargetLevelIndex;
	}

	public String getCourseVersionMap() {
		return courseVersionMap;
	}

	public void setCourseVersionMap(String courseVersionMap) {
		this.courseVersionMap = courseVersionMap;
	}
}
