package com.pdinh.reporting;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.pdfbox.util.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.exception.ServerUnavailableException;
import com.pdinh.persistence.ms.entity.ReportSourceType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportSourceTypeObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.presentation.helper.ReportServiceHelper;
import com.pdinh.reporting.generation.CoachingPlanDetailedExtract;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.Reports;

/**
 * Service for generating report xml requests that are sent to the report
 * generation server
 */

/**
 * @author dwood
 * 
 */
@Stateless
@LocalBean
public class ReportingServiceLocalImpl implements ReportingServiceLocal {

	//
	// Static variables
	//
	final static Logger logger = LoggerFactory.getLogger(ReportingServiceLocalImpl.class);
	/*
	@EJB
	private ReportSourceTypeDao reportSourceTypeDao;
	*/
	@EJB
	private ReportingSingleton reportingSingleton;

	@EJB
	ReportServiceHelper helper;

	@Resource(name = "application/config_properties")
	private Properties configProperties;

	//
	// Instance variables
	//

	//
	// Instance methods
	//
	@Override
	public String generationReportXml(Reports reports) {
		// Generate the xml
		String xml = "";
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(Reports.class);
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			marshaller.marshal(reports, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	/**
	 * getReports - Fetch reports in a stream
	 * 
	 * @param urls
	 *            - A List of Maps. Each element in the list is a Map of all of
	 *            the required elements needed to generate a report. One member
	 *            of each map is the url (keyed as "url"); the rest are request
	 *            parameters.
	 * @return An AggregatedReportStream object
	 */
	@Override
	public AggregatedReportStream getReports(List<Map<String, String>> urls) {
		AggregatedReportStream reportFileStream = new AggregatedReportStream();
		File tempDir = null;
		List<File> files = new ArrayList<File>();
		File currentFile = null;
		String gMimeType = null;
		int fileIndex = 1;
		try {
			logger.debug("System.getProperty(\"java.io.tmpdir\") = " + System.getProperty("java.io.tmpdir"));
			tempDir = File.createTempFile("rpt", "", new File(System.getProperty("java.io.tmpdir")));
		} catch (IOException e2) {
			logger.error("EXCEPTION caught setting tmp directory: {}", e2.getMessage());
		}
		tempDir.delete();
		tempDir.mkdir();

		// Shouldn't happen, but it did
		if (urls.size() < 1) {
			logger.debug("No valid reports selected error");
			String errFile = tempDir + "/noValidReportsSelectedError-" + fileIndex + ".html";
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(errFile);
				String msg = "<html><body><h2 style=\"color:red;\">No valid report type was selected</h2>";
				msg += "<p>Either no reports were selected or there is a logic error.</p>";
				msg += "</body></html>";
				byte[] bytes = msg.getBytes();
				out.write(bytes);
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}

			currentFile = new File(errFile);
			gMimeType = "text/html";
			fileIndex++;
			files.add(currentFile);
		} else {
			// Loop through the list of URL maps. Each map contains the URL and
			// parameters for a single report.
			logger.debug("Valid reports selected");
			for (Map<String, String> urlMap : urls) {
				String fileName = null;
				String fileExtension = null;
				FileOutputStream out = null;

				String hyb = urlMap.get("hybrid");
				boolean isHybrid = (hyb == null || !hyb.equals("yes")) ? false : true;
				String urlStr;
				if (isHybrid) {
					urlStr = "Hybrid";
				} else {
					urlStr = urlMap.get("url");
					if (urlStr == null)
						urlStr = "";
				}

				try {
					// generate the report using the passed url map.
					// Differentiate between "regular" (single) reports and
					// hybrid reports.
					GeneratedReportData grd = null;
					if (isHybrid) {
						try {
							logger.debug("Calling getHybridRptData()");
							grd = getHybridRptData(urlMap);
						} catch (PalmsException e) {
							// TODO: Use exception message instead of null
							// handling below?
							logger.error("Error generating Hybrid report: {}", e.getMessage());
							e.printStackTrace();
						}
					} else {
						grd = getRptData(urlMap);
					}

					if (grd == null || grd.getHdrStr() == null) {
						// error HTML page
						String errFile = tempDir + "/noContentError-" + fileIndex + ".html";
						out = new FileOutputStream(errFile);

						String msg = "<html><body><h2 style=\"color:red;\">No content was generated</h2>";
						msg += "<p>Caller=" + ((urlStr.length() < 1) ? "<undefined>" : urlStr) + "</p>";
						msg += "<p>Parameters=" + urlMap.toString() + "</p>";
						msg += "</body></html>";

						String friendlyMsg = "<html><body><h2 style=\"color:red;\">No reports generated.</h2>Possible causes:<br /><ul><li>Participant has no valid instrument results.</li><li>Norms have not been selected.</li> <li>An error was detected while gathering participant instrument information.</li> <li>A target level has not been set for the project.</li></ul> </body></html>";
						byte[] bytes = friendlyMsg.getBytes();
						out.write(bytes);
						out.flush();

						currentFile = new File(errFile);
						fileExtension = "html";
						logger.debug("No content for coaching report: {} ", msg);
					} else {
						// A "real" report
						String[] cdarr = grd.getHdrStr().split("=");
						// If the file name has double-quotes (") around it,
						// suppress them. Replace slashes with underscores to
						// avoid file path issues
						fileName = cdarr[1].replace('"', ' ').trim();
						fileName = fileName.replace('/', '_');
						fileName = fileName.replace('\\', '_');
						/*
						 Apparently, intelligible content-type is not consistently available from the report servers so we'll look at filename/extension
						 */
						logger.debug("NOW fileName = " + fileName);
						fileExtension = "";
						int i = fileName.lastIndexOf('.');
						if (i > 0) {
							fileExtension = fileName.substring(i + 1);
						}
						if (fileExtension == null || fileExtension.isEmpty()) {
							fileExtension = "html";
						}

						out = new FileOutputStream(tempDir + "/" + fileName);
						byte[] bytes = new byte[4096];
						int bytesRead = 0;
						while ((bytesRead = grd.getInSream().read(bytes)) > 0) {
							out.write(bytes, 0, bytesRead);
						}
						currentFile = new File(tempDir + "/" + fileName);
					}
					fileIndex++;
					files.add(currentFile);
					if (fileExtension.equalsIgnoreCase("zip")) {
						gMimeType = "application/zip";
					} else if (fileExtension.equalsIgnoreCase("pdf")) {
						gMimeType = "application/pdf";
					} else if (fileExtension.equalsIgnoreCase("xlsx")) {
						gMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
					} else if (fileExtension.toLowerCase().indexOf("xls") > -1 || fileExtension.equalsIgnoreCase("csv")) {
						gMimeType = "application/x-excel";
					} else {
						gMimeType = "text/html";
					}
				} catch (IOException e1) {
					logger.error("EXCEPTION caught getting content from report server {} : {}", urlStr, e1.getMessage());
				}

			} // End for urls
		} // End of else on if urls.size() < 1

		if (files.size() > 1) {
			// Multiple files returned... zip up all files in files[]
			// It's a bunch of reports, return a zip
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(bos);
			for (File r : files) {
				try {
					String fileName = r.getName();
					FileInputStream fis = new FileInputStream(r);
					zos.putNextEntry(new ZipEntry(fileName));
					byte[] bytes = new byte[4096];
					int bytesRead = 0;
					try {
						while ((bytesRead = fis.read(bytes)) > 0) {
							zos.write(bytes, 0, bytesRead);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					fis.close();
					zos.closeEntry();
				} catch (ZipException ze) {
					// duplicate name, not sure if it's the 1st, 2nd, but it
					// probably isn't the 10,000th
					logger.error("ZIP EXCEPTION caught zipping file {} in report zip: {}", r.getName(), ze.getMessage());
					Random generator = new Random();
					int randomIndex = generator.nextInt(10000) * 10000;
					String fileName = r.getName();
					fileName = fileName.replace(".", "[" + randomIndex + "].");
					ZipEntry zipEntry = new ZipEntry(fileName);
					try {
						zos.putNextEntry(zipEntry);
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						zos.closeEntry();
					} catch (IOException e) {
						e.printStackTrace();
					}

				} catch (IOException e) {
					logger.error("IO Execption caught while zipping up report files: {}", e.getMessage());
				}
			} // End for files
			String EXTRACT_DATE_FORMAT = "yyyyMMdd";
			SimpleDateFormat sdf = new SimpleDateFormat(EXTRACT_DATE_FORMAT);
			String date = sdf.format(new Date());
			reportFileStream.setFilename("reports_" + date + ".zip");
			try {
				FileInputStream fis = new FileInputStream(reportFileStream.getFilename());
				byte[] bytes = new byte[4096];
				int bytesRead = 0;
				try {
					while ((bytesRead = fis.read(bytes)) > 0) {
						bos.write(bytes, 0, bytesRead);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				logger.error("Execption caught while bundling zip {} in memory: {}", reportFileStream.getFilename(),
						e.getMessage());
			}
			reportFileStream.setByteArrayOutputStream(bos);
			reportFileStream.setMimeType("application/zip");
			try {
				bos.close();
				zos.close();
			} catch (IOException e) {
				logger.error("Execption caught while closing zip {} in memory: {}", reportFileStream.getFilename(),
						e.getMessage());
			}
		} else {
			// just return single file
			reportFileStream.setFilename(currentFile.getName());
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				FileInputStream fis = new FileInputStream(currentFile);
				byte[] bytes = new byte[4096];
				int bytesRead = 0;
				try {
					while ((bytesRead = fis.read(bytes)) > 0) {
						bos.write(bytes, 0, bytesRead);
					}
				} catch (IOException e) {
					logger.error("Execption caught while writing out report file byte array");
				}
			} catch (FileNotFoundException e) {
				logger.error("Execption caught while returning file: {}", reportFileStream.getFilename());
			}
			try {
				bos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			reportFileStream.setByteArrayOutputStream(bos);
			reportFileStream.setMimeType(gMimeType);
		}

		for (File f : files) {
			f.delete();
		}
		tempDir.delete();

		return reportFileStream;
	}

	@Override
	public GeneratedReportData getReportStream(Map<String, String> urlMap, boolean isHybrid) throws PalmsException {
		// ByteArrayOutputStream stream = new ByteArrayOutputStream(4096);

		String urlTarget = null;

		HttpURLConnection conn = null;
		GeneratedReportData grd = new GeneratedReportData();
		if (isHybrid) {
			grd = getHybridRptData(urlMap);
		} else {
			try {
				urlTarget = urlMap.get("url");
				logger.debug("Got urlTarget: {}", urlTarget);
				URL currUrl = new URL(urlTarget);

				conn = (HttpURLConnection) currUrl.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
				conn.setRequestProperty("Accept-Charset", "UTF-8");
				conn.setDoOutput(true);

				StringBuffer urlParameters = new StringBuffer();
				Iterator<Entry<String, String>> itr = urlMap.entrySet().iterator();
				boolean firstIter = true;
				while (itr.hasNext()) {
					Entry<String, String> pair = itr.next();
					if (!pair.getKey().equalsIgnoreCase("url")) {
						if (!firstIter) {
							urlParameters.append("&");
						} else {
							firstIter = false;
						}
						//logger.trace("adding url parameter: {}={}", pair.getKey(), pair.getValue());
						urlParameters.append(pair.getKey() + "=" + URLEncoder.encode(pair.getValue(), "UTF-8"));
					}
				}
				// NW: this can dump some ginormous blobs out... setting to
				// trace level -- NW:  getting rid of entirely.  
				//logger.trace("urlParameters = {}", urlParameters.toString());
				// Generate the report
				OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
				writer.write(urlParameters.toString());
				writer.flush();
			} catch (MalformedURLException e) {
				logger.debug("Exception creating the target URL (url={}): {}", urlTarget, e.getMessage());
				throw new PalmsException(e.getMessage(), PalmsErrorCode.BAD_REQUEST.getCode());

			} catch (IOException e) {
				logger.error("Exception on the connection - establishing or writing (url={}): {}", urlTarget,
						e.getMessage());
				ServerUnavailableException sue = new ServerUnavailableException(urlTarget, e.getMessage());
				sue.setErrorCode(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getCode());
				throw sue;

			}
		}

		// ...otherwise, get the generated report as a stream
		try {

			if (isHybrid) {

				return grd;
			}
			
			if (!(conn.getHeaderField("Error") == null)){
				String errorMessage = conn.getHeaderField("Error");
				if (errorMessage.length() > 250){
					errorMessage = errorMessage.substring(0, 249);
				}
				logger.error("Got Error Header in response: {}", errorMessage);
				throw new PalmsException(errorMessage,
						PalmsErrorCode.REPORT_GEN_FAILED.getCode());
			}
			grd.setInStream(conn.getInputStream());
			if (!(conn.getHeaderField("Content-Length") == null)) {
				grd.setContentLength(Integer.valueOf(conn.getHeaderField("Content-Length")));
			}
			if (!(conn.getHeaderField("content-disposition") == null)){
				grd.setHdrStr(conn.getHeaderField("content-disposition"));
			}
			if (!(conn.getHeaderField("Content-Disposition") == null)){
				grd.setHdrStr(conn.getHeaderField("Content-Disposition"));
			}

			// grd.setHeaderFields(conn.getHeaderFields());
			/*
			Map<String, List<String>> headers = conn.getHeaderFields();

			for (String key : headers.keySet()) {
				
				logger.debug("Got header: {}, with value: {}", key, headers.get(key));
			}
			*/
			return grd;
		} catch (IOException ioe) {
			logger.debug("Exception fetching report input stream: {}", ioe.getMessage());
			throw new PalmsException("Exception fetching report input stream: " + ioe.getMessage(),
					PalmsErrorCode.REPORT_GEN_FAILED.getCode());

		}

		// Successful return
		// return null;
	}

	@Override
	public List<Map<String, String>> generateReportRequestUrlMaps(List<ParticipantObj> participants,
			List<ReportTypeObj> reportTypes, String projectId) throws PalmsException {
		// List<Map<String, String>> reportSourceUrls = new
		// ArrayList<Map<String, String>>();
		/*
		List<ParticipantObj> participants = reportWizardViewBean.getParticipants();
		ReportTypeObj[] selectedReportTypes = reportWizardViewBean.getSelectedReportTypes();
		doAudit(participants, selectedReportTypes);
		List<ReportSourceType> reportSourceTypes = new ArrayList<ReportSourceType>();
		*/
		Participants ppts = ReportServiceHelper.createParticipantsObject(participants);

		// Get a list of all report source types
		List<ReportSourceType> reportSourceTypes = reportingSingleton.getAllReportSourceTypes();
		List<ReportSourceTypeObj> reportSourceTypeArrayItems = new ArrayList<ReportSourceTypeObj>();
		for (ReportSourceType rst : reportSourceTypes) {
			reportSourceTypeArrayItems.add(new ReportSourceTypeObj(rst));
		}

		// Loop through the list of types, adding the reports requested for each
		// type
		for (ReportTypeObj rto : reportTypes) {
			for (ReportSourceTypeObj rstai : reportSourceTypeArrayItems) {
				if (rto.getReportSourceTypeId() == rstai.getReportSourceType().getReportSourceTypeId()) {
					logger.debug("rto.code = {}, rstai.source.name = {}, lang code={}", rto.getCode(), rstai
							.getReportSourceType().getName(), rto.getLanguageCode());
					rstai.addReportType(rto);
				}
			}
			if (!(rto.getTargetLevelOverride() ==  null || rto.getTargetLevelOverride().isEmpty())){
				TargetLevelType tlt = reportingSingleton.getTargetLevelTypeByCode(rto.getTargetLevelOverride());
				rto.setTargetLevelOverrideIndex(tlt.getTargetLevelIndex());
			}
		}
		helper.setReportSourceUrls(new ArrayList<Map<String, String>>());
		helper.setConfigProperties(configProperties);
		// Loop through the (now augmented) source types and process the
		// requests
		for (ReportSourceTypeObj rstai : reportSourceTypeArrayItems) {
			if (rstai.getReportTypes().size() > 0) {
				switch (rstai.getReportSourceType().getReportSourceTypeId()) {
				case ReportSourceType.PALMS:
					try {
						helper.genPalmsUrls(ppts, rstai, projectId);
					} catch (PalmsException p) {
						logger.error("Caught exception {} from genPalmsUrls(). {}, Details: {}", p.getErrorCode(), p.getMessage(), p.getErrorDetail());
						throw p;
					}
					break;
				case ReportSourceType.OXCART:
					helper.genOxcartUrls(participants, rstai);
					break;
				case ReportSourceType.COACHING_EXTRACT:
					helper.genCoachingExtractUrls(participants, rstai);
					break;
				case ReportSourceType.HYBRID:
					helper.genHybridUrls(participants, rstai);
					break;
				default:
				} // End of switch (<report type>)
			}
		}

		return helper.getReportSourceUrls();

	}

	/**
	 * Generate a report using the provided urlMap parameters and pass back an
	 * InputStream.
	 * 
	 * @param urlMap
	 *            - A map of parameters used to generate a report. Includes the
	 *            URL to call as a member of the map.
	 * @return - An GeneratedReportData containing the pdf generated and the
	 *         connection header.
	 */
	private GeneratedReportData getRptData(Map<String, String> urlMap) {
		GeneratedReportData ret = new GeneratedReportData();

		String urlTarget = null;
		boolean hasError = false;
		HttpURLConnection conn = null;
		try {
			urlTarget = urlMap.get("url");
			logger.debug("Got urlTarget: {}", urlTarget);
			URL currUrl = new URL(urlTarget);
			conn = (HttpURLConnection) currUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setDoOutput(true);
			StringBuffer urlParameters = new StringBuffer();
			Iterator<Entry<String, String>> itr = urlMap.entrySet().iterator();
			boolean firstIter = true;
			while (itr.hasNext()) {
				Entry<String, String> pair = itr.next();
				if (!pair.getKey().equalsIgnoreCase("url")) {
					if (!firstIter) {
						urlParameters.append("&");
					} else {
						firstIter = false;
					}
					logger.trace("adding url parameter: {}={}", pair.getKey(), pair.getValue());
					urlParameters.append(pair.getKey() + "=" + URLEncoder.encode(pair.getValue(), "UTF-8"));
				}
			}
			// NW: this can dump some ginormous blobs out... setting to trace
			// level
			logger.trace("urlParameters = {}", urlParameters.toString());
			// Generate the report
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(urlParameters.toString());
			writer.flush();
		} catch (MalformedURLException e) {
			logger.debug("Exception creating the target URL (url={}): {}", urlTarget, e.getMessage());
			hasError = true;
		} catch (IOException e) {
			logger.debug("Exception on the connection - establishing or writing (url={}): {}", e.getMessage());
			hasError = true;
		}

		// See if there was an error...
		if (hasError) {
			ret.clear();
			return ret;
		}

		// ...otherwise, get the generated report as a stream
		try {
			ret.setInStream(conn.getInputStream());
			ret.setHdrStr(conn.getHeaderField("Content-Disposition"));
		} catch (IOException ioe) {
			logger.debug("Exception fetching report input stream: {}", ioe.getMessage());
			ret.clear();
			return ret;
		}

		// Successful return
		return ret;
	}

	/**
	 * Generate a hybrid report using the passed urlMap. This method assumes two
	 * concatenated reports. Uses the same method used for single reports
	 * (getRptData) called for each of the reports in the hybrid report and then
	 * the results are merged.
	 * 
	 * Assumes that there are exactly two reports being concatenated
	 * 
	 * @param urlMap
	 * @return A GeneratedReportData object
	 */
	private GeneratedReportData getHybridRptData(Map<String, String> hybMap) throws PalmsException {
		GeneratedReportData ret = new GeneratedReportData();
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();
		String hPpt = "N/A";
		String hType = "N/A";

		// Allocate the parameters to their respective destinations
		Iterator<Entry<String, String>> it = hybMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> ent = it.next();
			String key = ent.getKey();
			if (key.startsWith("hybrid")) {
				if (key.equals("hybridType")) {
					hType = ent.getValue();
				} else if (key.equals("hybridPpt")) {
					hPpt = ent.getValue();
				}
				continue;
			}
			if (!key.contains("_")) {
				logger.warn("Invalid hybrid key (" + key + ")... Skipped.");
				continue;
			}
			// Split off the prefix
			String[] splitKey = key.split("_");
			if (splitKey[0].equals("H1")) {
				map1.put(splitKey[1], ent.getValue());
			} else if (splitKey[0].equals("H2")) {
				map2.put(splitKey[1], ent.getValue());
			} else {
				logger.warn("Invalid hybrid parameter... key=" + key + ".  Skipped.");
				continue;
			}
		} // End of while loop

		// Process the first (the viaEdge report... No now it is the TLT report)
		logger.debug("Calling getRptData()");
		GeneratedReportData grd1 = getRptData(map1);
		if (grd1.getInSream() == null || grd1.getHdrStr() == null) {
			// error... scram
			logger.error("Error detected creating part 1 of hybrid report.  type={}, ppt={}.", hType, hPpt);
			throw new PalmsException("Error detected creating part 1 of hybrid report.",
					PalmsErrorCode.REPORT_GEN_FAILED.getCode());
			// ret.clear();
			// return ret;
		}

		// Process the second (the TLT report... No now it is the viaEdgeReport)
		logger.debug("Calling getRptData again...");
		GeneratedReportData grd2 = getRptData(map2);
		if (grd2.getInSream() == null || grd2.getHdrStr() == null) {
			// error... scram
			logger.error("Error detected creating part 2 of hybrid report.  type={}, ppt={}.", hType, hPpt);
			throw new PalmsException("Error detected creating part 2 of hybrid report.",
					PalmsErrorCode.REPORT_GEN_FAILED.getCode());
			// ret.clear();
			// return ret;
		}

		// Now merge them
		ByteArrayOutputStream merged = new ByteArrayOutputStream();
		try {
			logger.debug("Starting hybrid merge...");
			PDFMergerUtility ut = new PDFMergerUtility();
			ut.addSource(grd1.getInSream());
			ut.addSource(grd2.getInSream());
			ut.setDestinationStream(merged);
			ut.mergeDocuments();
			//
		} catch (Exception e) {
			logger.debug("Error merging hybrid report PDFs" + e);
			throw new PalmsException("Error merging hybrid report PDFs. " + e.getMessage(),
					PalmsErrorCode.REPORT_GEN_FAILED.getCode());
		}
		logger.debug("done hybrid merging");
		ret.setContentType("application/pdf");
		byte[] byteArray = merged.toByteArray();
		ret.setInStream(new ByteArrayInputStream(byteArray));

		ret.setContentLength(byteArray.length);
		ret.setHdrStr("attachment; filename=rpt_" + hPpt + "_" + hType + ".pdf");

		return ret;
	}

	@Override
	public String generationParticipantsXml(Participants participants) {
		String xml = "";
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(Participants.class);
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			marshaller.marshal(participants, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	@Override
	public String generationCoachingPlanDetailedExtractXml(CoachingPlanDetailedExtract coachingPlanDetailedExtract) {
		String xml = "";
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(CoachingPlanDetailedExtract.class);
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			marshaller.marshal(coachingPlanDetailedExtract, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	/**
	 * GeneratedReportData - Private class used to transport information from a
	 * generated report
	 */
	/*
	private class GeneratedReportData {
		private InputStream inStream = null;
		private String hdrStr = null;
		private Map<String, List<String>> headerFields = null;
		private int contentLength = 0;
		private String contentType = null;

		//
		// Constructor
		//
		public GeneratedReportData() {
			// do nothing
		}

		//
		// Instance methods
		//
		public void clear() {
			this.setHdrStr(null);
			this.setInStream(null);
			this.setHeaderFields(null);
		}

		// -----------------------
		public InputStream getInSream() {
			return this.inStream;
		}

		public void setInStream(InputStream val) {
			this.inStream = val;
		}

		// -----------------------
		public String getHdrStr() {
			return this.hdrStr;
		}

		public void setHdrStr(String val) {
			this.hdrStr = val;
		}

		public Map<String, List<String>> getHeaderFields() {
			return headerFields;
		}

		public void setHeaderFields(Map<String, List<String>> headerFields) {
			this.headerFields = headerFields;
		}

		public int getContentLength() {
			return contentLength;
		}

		public void setContentLength(int contentLength) {
			this.contentLength = contentLength;
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
	}
	*/

}
