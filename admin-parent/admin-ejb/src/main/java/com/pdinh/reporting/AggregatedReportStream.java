package com.pdinh.reporting;

import java.io.ByteArrayOutputStream;

public class AggregatedReportStream {
	private ByteArrayOutputStream byteArrayOutputStream;
	private String filename;
	private String mimeType;

	public AggregatedReportStream() {

	}

	public ByteArrayOutputStream getByteArrayOutputStream() {
		return byteArrayOutputStream;
	}

	public void setByteArrayOutputStream(ByteArrayOutputStream outputStream) {
		this.byteArrayOutputStream = outputStream;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}
