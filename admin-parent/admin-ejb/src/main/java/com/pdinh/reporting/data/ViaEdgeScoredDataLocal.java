package com.pdinh.reporting.data;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import com.pdinh.persistence.ms.entity.ProjectUser;

@Local
public interface ViaEdgeScoredDataLocal {
	public Map<String, String> getViaEdgeConfidences(List<ProjectUser> projectUsers);
}
