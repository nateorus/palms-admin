package com.pdinh.reporting.generation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rcmXml")
@XmlAccessorType(XmlAccessType.FIELD)
public class PalmsReports {
	@XmlElement(name = "participants")
	private List<ReportParticipant> participants = new ArrayList<ReportParticipant>();

	public PalmsReports() {
	}

	public List<ReportParticipant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ReportParticipant> participants) {
		this.participants = participants;
	}
}
