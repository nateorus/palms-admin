package com.pdinh.rest.security;

import java.net.URI;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.CharSet;

public class AwsSignature {
    private static final String HMAC_SHA512_ALGORITHM = "HmacSHA512";

    public static String calculate(String stringToSign, String secretKey) throws Exception {
        // get an hmac_sha1 key from the raw key bytes
        SecretKeySpec signingKey = new SecretKeySpec(secretKey.toUpperCase().getBytes("ASCII"), HMAC_SHA512_ALGORITHM);

        // get an hmac_sha1 Mac instance and initialize with the signing key
        Mac mac = Mac.getInstance(HMAC_SHA512_ALGORITHM);
        mac.init(signingKey);

        // compute the hmac on input data bytes
        byte[] rawHmac = mac.doFinal(stringToSign.getBytes("ASCII"));

        // base64-encode the hmac
        String result = new String(Base64.encodeBase64(rawHmac));
        return result;
    }
    
    public static String generateSignature(String partnerKey, String secretKey, String tokenType, 
    		String token, URI url, String httpMethod, String nonce, String timestamp, 
    		String apiSignatureVersion) throws Exception{
		
		
		String signatureBase = String.format("%1$s://%2$s%3$s&%4$s&%5$s&%6$s&%7$s&%8$s&%9$s&%10$s", 
				url.getScheme(), url.getHost(), url.getPath(), httpMethod, partnerKey, nonce, timestamp, 
				token, tokenType, apiSignatureVersion).toUpperCase();
		
		return AwsSignature.calculate(signatureBase, secretKey);
	}
}
