package com.pdinh.rest.security;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

public class KFOAuthorizationClientFilter extends ClientFilter {

	
	String secretKey;
	String partnerKey;
	String partnerKeyVersion;
	String serviceToken;
	String tokenType;
	
	private static final Logger log = LoggerFactory.getLogger(KFOAuthorizationClientFilter.class);
	
	public KFOAuthorizationClientFilter(APICredentials credentials){
		secretKey = credentials.getSecretKey();
		partnerKey = credentials.getPartnerKey();
		partnerKeyVersion = credentials.getPartnerKeyVersion();
		serviceToken = credentials.getServiceToken();
		tokenType = credentials.getTokenType();
	}
	
	public KFOAuthorizationClientFilter(Properties KFOReportAPIProperties){
		//this.setProperties(properties);
		secretKey = KFOReportAPIProperties.getProperty("KFREPORTS_APISecretKey", "b0d4b7a7-d3a6-47ac-b5f0-e461b5d6a028");
		partnerKey = KFOReportAPIProperties.getProperty("KFREPORTS_APIPartnerKey", "f65b01da-7ed3-4338-b09d-c675913073bb");
		partnerKeyVersion = KFOReportAPIProperties.getProperty("KFREPORTS_APIPartnerKeyVersion", "1.0");
		
		serviceToken = KFOReportAPIProperties.getProperty("KFREPORTS_APIToken", "Reports");
		tokenType = KFOReportAPIProperties.getProperty("KFREPORTS_APITokenType", "Palms");
	}

	@Override
	public ClientResponse handle(ClientRequest cr)
			throws ClientHandlerException {
		System.out.println("Filtering Client Request");
		ClientRequest mcr = authorizePalmsAPIRequest(cr);
		
		return getNext().handle(mcr);
		
	}
	
	private ClientRequest authorizePalmsAPIRequest(ClientRequest request) {
		
		URI uri = request.getURI();
		//log.debug("Got uri: Scheme: {}, host: {}, path: {}", uri.getScheme(), uri.getHost(), uri.getPath());
		Random random = new Random();
		Date date = new Date();

		String nonce = String.valueOf(Math.abs(random.nextInt()));

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
	
		String tstmp = format.format(date.getTime());
		
		//request.getHeaders().add("kf_partner_key", partnerKey);
		request.getHeaders().putSingle("kf_partner_key", partnerKey);
		log.trace("adding header kf_partner_key: {}", partnerKey);
		//request.getHeaders().add("kf_token", serviceToken);
		request.getHeaders().putSingle("kf_token", serviceToken);
		log.trace("adding header kf_token: {}", serviceToken);
		//request.getHeaders().add("kf_token_type", tokenType);
		request.getHeaders().putSingle("kf_token_type", tokenType);
		log.trace("adding header kf_token_type: {}", tokenType);
		//request.getHeaders().add("kf_nonce", nonce);
		request.getHeaders().putSingle("kf_nonce", nonce);
		log.trace("adding header kf_nonce: {}", nonce);
		//request.getHeaders().add("kf_timestamp", tstmp);
		request.getHeaders().putSingle("kf_timestamp", tstmp);
		log.trace("adding header kf_timestamp: {}", tstmp);
		//request.getHeaders().add("kf_version", partnerKeyVersion);
		request.getHeaders().putSingle("kf_version", partnerKeyVersion);
		log.trace("adding header kf_version: {}", partnerKeyVersion);
		
		String apiSignature = "";
		try {
			apiSignature = AwsSignature.generateSignature(partnerKey, secretKey, tokenType, 
					serviceToken, uri, request.getMethod(), nonce, tstmp, partnerKeyVersion);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		//request.getHeaders().add("kf_signature", apiSignature);
		request.getHeaders().putSingle("kf_signature", apiSignature);
		log.trace("adding header kf_signature: {}", apiSignature);
		
		return request;
		
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getPartnerKey() {
		return partnerKey;
	}

	public void setPartnerKey(String partnerKey) {
		this.partnerKey = partnerKey;
	}

	public String getPartnerKeyVersion() {
		return partnerKeyVersion;
	}

	public void setPartnerKeyVersion(String partnerKeyVersion) {
		this.partnerKeyVersion = partnerKeyVersion;
	}

	public String getServiceToken() {
		return serviceToken;
	}

	public void setServiceToken(String serviceToken) {
		this.serviceToken = serviceToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}
