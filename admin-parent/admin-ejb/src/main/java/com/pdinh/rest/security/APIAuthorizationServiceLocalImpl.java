package com.pdinh.rest.security;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Stateless
@LocalBean
public class APIAuthorizationServiceLocalImpl implements
		APIAuthorizationServiceLocal {
	
	private static final Logger log = LoggerFactory.getLogger(APIAuthorizationServiceLocalImpl.class);
	
	@Resource(mappedName = "application/config_properties")
	private Properties properties;
	

	@Override
	public void authorizeKFReportsAPIRequest(HttpURLConnection conn, Properties kfoProperties) {

		URI uri = null;
		try {
			uri = conn.getURL().toURI();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.debug("Got uri: Scheme: {}, host: {}, path: {}", uri.getScheme(), uri.getHost(), uri.getPath());
		Random random = new Random();
		Date date = new Date();
		
		
		
		String secretKey = kfoProperties.getProperty("KFREPORTS_APISecretKey", "b0d4b7a7-d3a6-47ac-b5f0-e461b5d6a028");
		
		String partnerKey = kfoProperties.getProperty("KFREPORTS_APIPartnerKey", "f65b01da-7ed3-4338-b09d-c675913073bb");
		String partnerKeyVersion = kfoProperties.getProperty("KFREPORTS_APIPartnerKeyVersion", "1.0");
		
		String serviceToken = kfoProperties.getProperty("KFREPORTS_APIToken", "Reports");
		String tokenType = kfoProperties.getProperty("KFREPORTS_APITokenType", "PALMS");
		
		String nonce = String.valueOf(Math.abs(random.nextInt()));
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SX");
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");

		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		log.trace("Date String: {}", format.format(date.getTime()));
		//String tstmp = String.valueOf(date.getTime());
		String tstmp = format.format(date.getTime());
		
		conn.setRequestProperty("kf_partner_key", partnerKey);
		log.trace("kf_partner_key header: {}", partnerKey);
		conn.setRequestProperty("kf_token", serviceToken);
		log.trace("kf_token header: {}", serviceToken);
		conn.setRequestProperty("kf_token_type", tokenType);
		log.trace("kf_token_type header: {}", tokenType);
		conn.setRequestProperty("kf_nonce", nonce);
		log.trace("kf_nonce header: {}", nonce);
		conn.setRequestProperty("kf_timestamp", tstmp);
		
		log.trace("kf_timestamp header: {}", tstmp);
		conn.setRequestProperty("kf_version", partnerKeyVersion);
		log.trace("kf_version header: {}", partnerKeyVersion);
		
		String apiSignature = "";
		try {
			apiSignature = AwsSignature.generateSignature(partnerKey, secretKey, tokenType, 
					serviceToken, uri, conn.getRequestMethod(), nonce, tstmp, partnerKeyVersion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		conn.setRequestProperty("kf_signature", apiSignature);
		log.trace("kf_signature header: {}", apiSignature);


	}

	@Override
	public void authorizePALMSAPIRequest(HttpURLConnection conn) {
		
		URI uri = null;
		try {
			uri = conn.getURL().toURI();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.debug("Got uri: Scheme: {}, host: {}, path: {}", uri.getScheme(), uri.getHost(), uri.getPath());
		Random random = new Random();
		Date date = new Date();
		
		String secretKey = properties.getProperty("PALMS_APISecretKey", "b0d4b7a7-d3a6-47ac-b5f0-e461b5d6a028");
		String partnerKey = properties.getProperty("PALMS_APIPartnerKey", "f65b01da-7ed3-4338-b09d-c675913073bb");
		String partnerKeyVersion = properties.getProperty("PALMS_APIPartnerKeyVersion", "1.0");
		
		String serviceToken = properties.getProperty("PALMS_APIToken", "Reports");
		String tokenType = properties.getProperty("PALMS_APITokenType", "PALMS");
		
		String nonce = String.valueOf(random.nextInt());
		String tstmp = String.valueOf(date.getTime());
		
		conn.setRequestProperty("PartnerKey", partnerKey);
		conn.setRequestProperty("Token", serviceToken);
		conn.setRequestProperty("Token_Type", tokenType);
		conn.setRequestProperty("Nonce", nonce);
		conn.setRequestProperty("TimeStamp", tstmp);
		conn.setRequestProperty("Version", partnerKeyVersion);
		
		String apiSignature = "";
		try {
			apiSignature = AwsSignature.generateSignature(partnerKey, secretKey, tokenType, 
					serviceToken, uri, conn.getRequestMethod(), nonce, tstmp, partnerKeyVersion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		conn.setRequestProperty("API_Signature", apiSignature);

	}

}
