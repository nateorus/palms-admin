package com.pdinh.rest.security;

import java.net.HttpURLConnection;
import java.util.Properties;

import javax.ejb.Local;

@Local
public interface APIAuthorizationServiceLocal {

	public void authorizeKFReportsAPIRequest(HttpURLConnection conn, Properties kfoProperties);
	
	public void authorizePALMSAPIRequest(HttpURLConnection conn);
}
