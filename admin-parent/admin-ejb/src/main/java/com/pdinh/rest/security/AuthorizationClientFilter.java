package com.pdinh.rest.security;

import java.net.URI;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

public class AuthorizationClientFilter extends ClientFilter {

	String secretKey;
	String partnerKey;
	String partnerKeyVersion;
	String serviceToken;
	String tokenType;
	
	private static final Logger log = LoggerFactory.getLogger(AuthorizationClientFilter.class);
	
	public AuthorizationClientFilter(Properties PALMSAPIProperties){
		secretKey = PALMSAPIProperties.getProperty("PALMS_APISecretKey", "b0d4b7a7-d3a6-47ac-b5f0-e461b5d6a028");
		partnerKey = PALMSAPIProperties.getProperty("PALMS_APIPartnerKey", "f65b01da-7ed3-4338-b09d-c675913073bb");
		partnerKeyVersion = PALMSAPIProperties.getProperty("PALMS_APIPartnerKeyVersion", "1.0");
		
		serviceToken = PALMSAPIProperties.getProperty("PALMS_APIToken", "Reports");
		tokenType = PALMSAPIProperties.getProperty("PALMS_APITokenType", "PALMS");
	}
	
	public AuthorizationClientFilter(APICredentials credentials){
		secretKey = credentials.getSecretKey();
		partnerKey = credentials.getPartnerKey();
		partnerKeyVersion = credentials.getPartnerKeyVersion();
		serviceToken = credentials.getServiceToken();
		tokenType = credentials.getTokenType();
	}

	@Override
	public ClientResponse handle(ClientRequest cr)
			throws ClientHandlerException {
		System.out.println("Filtering Client Request");
		ClientRequest mcr = authorizePalmsAPIRequest(cr);
		
		return getNext().handle(mcr);
		
	}
	
	private ClientRequest authorizePalmsAPIRequest(ClientRequest request) {
		// TODO Auto-generated method stub
		URI uri = request.getURI();
		//log.debug("Got uri: Scheme: {}, host: {}, path: {}", uri.getScheme(), uri.getHost(), uri.getPath());
		Random random = new Random();
		Date date = new Date();
		
		String nonce = String.valueOf(random.nextInt());
		String tstmp = String.valueOf(date.getTime());
		
		request.getHeaders().putSingle("PartnerKey", partnerKey);
		log.trace("Adding PartnerKey header: {}", partnerKey);
		request.getHeaders().putSingle("Token", serviceToken);
		log.trace("Adding serviceToken header: {}", serviceToken);
		request.getHeaders().putSingle("Token_Type", tokenType);
		log.trace("Adding tokenType header: {}", tokenType);
		request.getHeaders().putSingle("Nonce", nonce);
		log.trace("Adding nonce header: {}", nonce);
		request.getHeaders().putSingle("TimeStamp", tstmp);
		log.trace("Adding tstmp header: {}", tstmp);
		request.getHeaders().putSingle("Version", partnerKeyVersion);
		log.trace("Adding partnerKeyVersion header: {}", partnerKeyVersion);
		
		String apiSignature = "";
		try {
			apiSignature = AwsSignature.generateSignature(partnerKey, secretKey, tokenType, 
					serviceToken, uri, request.getMethod(), nonce, tstmp, partnerKeyVersion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getHeaders().putSingle("API_Signature", apiSignature);
		log.trace("Adding apiSignature header: {}", apiSignature);
		
		return request;
		
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getPartnerKey() {
		return partnerKey;
	}

	public void setPartnerKey(String partnerKey) {
		this.partnerKey = partnerKey;
	}

	public String getPartnerKeyVersion() {
		return partnerKeyVersion;
	}

	public void setPartnerKeyVersion(String partnerKeyVersion) {
		this.partnerKeyVersion = partnerKeyVersion;
	}

	public String getServiceToken() {
		return serviceToken;
	}

	public void setServiceToken(String serviceToken) {
		this.serviceToken = serviceToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}
