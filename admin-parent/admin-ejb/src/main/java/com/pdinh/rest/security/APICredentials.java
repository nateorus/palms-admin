package com.pdinh.rest.security;

public class APICredentials {
	String secretKey;
	String partnerKey;
	String partnerKeyVersion;
	String serviceToken;
	String tokenType;
	
	public APICredentials(String secretKey, String partnerKey, String partnerKeyVersion, 
			String serviceToken, String tokenType){
		this.secretKey = secretKey;
		this.partnerKey = partnerKey;
		this.partnerKeyVersion = partnerKeyVersion;
		this.serviceToken = serviceToken;
		this.tokenType = tokenType;
	}
	
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getPartnerKey() {
		return partnerKey;
	}
	public void setPartnerKey(String partnerKey) {
		this.partnerKey = partnerKey;
	}
	public String getPartnerKeyVersion() {
		return partnerKeyVersion;
	}
	public void setPartnerKeyVersion(String partnerKeyVersion) {
		this.partnerKeyVersion = partnerKeyVersion;
	}
	public String getServiceToken() {
		return serviceToken;
	}
	public void setServiceToken(String serviceToken) {
		this.serviceToken = serviceToken;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
}
