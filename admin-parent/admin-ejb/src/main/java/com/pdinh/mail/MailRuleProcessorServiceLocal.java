package com.pdinh.mail;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.EmailRuleToProcess;

@Local
public interface MailRuleProcessorServiceLocal {
	public void processRule(EmailRuleToProcess ruleToProcess, boolean ignoreDate);
}
