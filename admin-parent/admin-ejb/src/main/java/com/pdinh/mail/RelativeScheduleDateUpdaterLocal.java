package com.pdinh.mail;

import java.util.Date;

import javax.ejb.Local;

import com.pdinh.persistence.ms.entity.Project;

@Local
public interface RelativeScheduleDateUpdaterLocal {
	public void updateRelativeEmailScheduleDatesForProject(Date newDate, Date oldDate, int projectId);
	
	public Date getEarliestAllowedDueDate(int projectId);
}
