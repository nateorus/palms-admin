package com.pdinh.mail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ProjectContentStatusServiceLocal;
import com.pdinh.data.ms.dao.EmailRuleScheduleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.EmailRuleToProcessDao;
import com.pdinh.mailtemplates.MailTemplatesServiceLocalImpl;
import com.pdinh.persistence.ms.entity.EmailRuleCriteria;
import com.pdinh.persistence.ms.entity.EmailRuleParticipant;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;
import com.pdinh.persistence.ms.entity.EmailRuleToProcess;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.ScheduledEmailDef;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.EmailEventObj;
import com.pdinh.presentation.domain.ParticipantObj;

@Stateless
@LocalBean
public class MailRuleProcessorService implements MailRuleProcessorServiceLocal {
	private static final Logger log = LoggerFactory.getLogger(MailRuleProcessorService.class);

	@EJB
	private EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	private MailTemplatesServiceLocalImpl mailTemplatesService;

	@EJB
	private ProjectContentStatusServiceLocal projectContentStatusService;
	
	@EJB
	private MailServiceLocal mailService;

	@EJB
	private EmailRuleScheduleDao emailRuleScheduleDao;

	@EJB
	private EmailRuleToProcessDao emailRuleToProcessDao;

	@Override
	public void processRule(EmailRuleToProcess ruleToProcess, boolean ignoreDate) {
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		Calendar ruleCalendar = Calendar.getInstance();
		ruleCalendar.setTime(ruleToProcess.getSendDate());
		List <EmailEventObj> emails = new ArrayList<EmailEventObj>();

		log.debug("Current Time.  Day of Month: {}, Month: {}, Year: {}, Hour: {}, Ignore date: {}",
				new Object[] { c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH), c.get(Calendar.YEAR), c.get(Calendar.HOUR_OF_DAY), ignoreDate });
		log.debug(
				"Rule Send Time.  Day of Month: {}, Month: {}, Year: {}, Hour: {}",
				new Object[] { ruleCalendar.get(Calendar.DAY_OF_MONTH), ruleCalendar.get(Calendar.MONTH),
						ruleCalendar.get(Calendar.YEAR), ruleCalendar.get(Calendar.HOUR_OF_DAY) });
		/*
		if ((c.get(Calendar.DAY_OF_MONTH) == ruleCalendar.get(Calendar.DAY_OF_MONTH)
				&& c.get(Calendar.MONTH) == ruleCalendar.get(Calendar.MONTH) && c.get(Calendar.YEAR) == ruleCalendar
				.get(Calendar.YEAR)) || c.getTime().after(ruleCalendar.getTime()) || ignoreDate) {
		*/
		if (c.getTime().after(ruleCalendar.getTime()) || ignoreDate){
			log.debug("Rule Date is in the past, or ignore date is true");
			log.debug("Email Rule Schedule Id {}", ruleToProcess.getEmailRuleSchedule().getId());
			if (ruleToProcess.getEmailRule().getActive() && (!ruleToProcess.getEmailRuleSchedule().isComplete())) {
				// rule is active... do it
				boolean success = true;
				boolean criteriaMatched = false;
				boolean errorGettingTemplateText = false;

				ScheduledEmailDef emailDef = ruleToProcess.getEmailRule().getEmailTemplate();
				if (emailDef == null){
					EmailRuleSchedule schedule = ruleToProcess.getEmailRuleSchedule();
					schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
							.findStatusByCode(EmailRuleScheduleStatus.ERRORS_ON_COMPLETE));
					schedule.setError(true);
					schedule.setComplete(true);
					
					log.error("The Rule ({}) could not be processed because the selected Email Template does not exist.  Updating Schedule with status {}",
							ruleToProcess.getEmailRule().getName(), ruleToProcess.getEmailRuleSchedule()
									.getEmailRuleScheduleStatus().getCode());
					emailRuleScheduleDao.update(schedule);
					emailRuleToProcessDao.delete(ruleToProcess);
					return;
				}

				List<EmailRuleParticipant> participants = new ArrayList<EmailRuleParticipant>();
				List<ProjectUser> sendUsers = new ArrayList<ProjectUser>();
				if (ruleToProcess.getEmailRule().getAllParticipants()) {
					// All participants option selected
					sendUsers = ruleToProcess.getEmailRule().getProject().getProjectUsers();
					if (ruleToProcess.getEmailRule().getExcludedParticipants()) {
						List<EmailRuleParticipant> excludedParticipants = ruleToProcess.getEmailRule()
								.getEmailRuleParticipants();

						// Remove Excluded participants
						for (EmailRuleParticipant excludedParticipant : excludedParticipants) {
							for (ProjectUser projectUser : sendUsers) {
								if (excludedParticipant.getUser().getUsersId() == projectUser.getUser().getUsersId()) {
									sendUsers.remove(projectUser);
									break;
								}

							}
						}

					}
				} else {
					// Specific participants selected
					participants = ruleToProcess.getEmailRule().getEmailRuleParticipants();
					List<ProjectUser> projectParticipants = ruleToProcess.getEmailRule().getProject().getProjectUsers();
					sendUsers = new ArrayList<ProjectUser>();
					for (EmailRuleParticipant ppt : participants) {
						for (ProjectUser projectUser : projectParticipants) {
							if (ppt.getUser().getUsersId() == projectUser.getUser().getUsersId()) {
								sendUsers.add(projectUser);
								break;
							}
						}
					}
				}
				// Got the users to send to. Now check logs and send
				for (ProjectUser user : sendUsers) {
					/* -- If we need to check user's logs before sending, do it here
					List<ScheduledEmailLog> logs = user.getUser().getScheduledEmailLogs();
					
					for (ScheduledEmailLog log: logs){
						if (log.getSchedule().getId() == ruleToProcess.getEmailRuleSchedule().getId()){
							if (log.getStatus().getCode().equalsIgnoreCase(EmailRuleScheduleStatus.EMAIL_SENT)){
								
							}
						}
					}
					*/
					if (matchesCriteria(user, ruleToProcess.getEmailRule().getEmailRuleCiteria())) {
						String emailLanguageCode = "en";

						if (ruleToProcess.getEmailRule().getLanguage() != null) {
							emailLanguageCode = ruleToProcess.getEmailRule().getLanguage();
						} else {
							if (user.getUser().getLangPref() != null) {
								emailLanguageCode = user.getUser().getLangPref();
								log.debug("Looking for users language prefs");
							} else {
								if (user.getUser().getCompany().getLangPref() != null) {
									emailLanguageCode = user.getUser().getCompany().getLangPref();
									log.debug("Unable to find user's language prefs, looking for company language prefs");
								}
							}
						}

						log.debug("Trying to use {} language prefs", emailLanguageCode);

						ScheduledEmailText emailText = getEmailTextByLang(emailLanguageCode, emailDef);

						if (emailText == null) {
							emailText = getEmailTextByLang("en", emailDef);
							log.debug("Translation for language {} doesn't exists. Using default EN language",
									emailLanguageCode);
						}

						EmailEventObj event = new EmailEventObj();
						event.setCompany(user.getUser().getCompany());
						event.setProject(user.getProject());
						event.setParticipant(getParticipantObj(user));
						event.setRecipientEmailAddress(user.getUser().getEmail());
						event.setRecipientFirstLastName(user.getUser().getFirstname() + " " + user.getUser().getLastname());
						event.setRecipientLastFirstName(user.getUser().getLastname() + " " + user.getUser().getFirstname());
						event.setSchedule(ruleToProcess.getEmailRuleSchedule());
						event = mailTemplatesService.getMergeFieldData(event);
						if (!(emailText == null)){
							event = mailTemplatesService.getTemplate(emailText, event);
						}else{
							errorGettingTemplateText = true;
							event.setError(true);
							event.setErrorText("No suitable email text translation found");
						}
						
						emails.add(event);
						/*
						if (!mailTemplatesService.sendTemplate(emailText.getId(), event)) {
							success = false;
						}
						*/

						criteriaMatched = true;

					} else {
						log.debug("User does not match email rule criteria");
					}
				}
				//Unhook the caboose!
				//success = mailService.sendJMSEmailBatch(emails);
				//success = mailService.sendEmailBatch(emails);
				success = mailService.sendBatchMail(emails);

				EmailRuleSchedule schedule = ruleToProcess.getEmailRuleSchedule();

				if (!criteriaMatched) {
					schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
							.findStatusByCode(EmailRuleScheduleStatus.NO_MATCH));
				} else {
					if (!success || errorGettingTemplateText) {
						schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
								.findStatusByCode(EmailRuleScheduleStatus.ERRORS_ON_COMPLETE));
						schedule.setError(true);
					} else {
						schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
								.findStatusByCode(EmailRuleScheduleStatus.NO_ERRORS));
						schedule.setError(false);
					}
				}

				schedule.setComplete(true);
				log.debug("Finished Processing Rule {}.  Deleting job.  Updating Schedule with status {}",
						ruleToProcess.getEmailRule().getName(), ruleToProcess.getEmailRuleSchedule()
								.getEmailRuleScheduleStatus().getCode());
				emailRuleScheduleDao.update(schedule);
				emailRuleToProcessDao.delete(ruleToProcess);
				log.debug("Finished Processing Rule.  Deleting job");
			} else {
				// rule is not active.. delete this ?
				log.debug("Email Rule {} Not Active, or Complete... Deleting Job", ruleToProcess.getEmailRule().getName());
				emailRuleToProcessDao.delete(ruleToProcess);
			}
		} else {
			// not ready for processing
			log.debug("Email Rule {} is not ready for processing", ruleToProcess.getEmailRule().getName());
		}

	}

	private ScheduledEmailText getEmailTextByLang(String langCode, ScheduledEmailDef def) {

		List<ScheduledEmailText> texts = def.getScheduledEmailTexts();
		for (ScheduledEmailText text : texts) {
			if (text.getLanguage().getCode().endsWith(langCode)) {
				return text;
			}
		}
		return null;
	}

	private ParticipantObj getParticipantObj(ProjectUser projUser) {
		ParticipantObj pptObj = new ParticipantObj();
		User user = projUser.getUser();
		pptObj.setEmail(user.getEmail());
		pptObj.setFirstName(user.getFirstname());
		pptObj.setLastName(user.getLastname());
		pptObj.setUserName(user.getUsername());
		pptObj.setPassword(user.getPassword());
		pptObj.setOptional1(user.getOptional1());
		pptObj.setOptional2(user.getOptional2());
		pptObj.setOptional3(user.getOptional3());
		pptObj.setSupervisorName(user.getSupervisorName());
		pptObj.setSupervisorEmail(user.getSupervisorEmail());
		pptObj.setUserId(user.getUsersId());
		return pptObj;
	}

	private boolean matchesCriteria(ProjectUser user, List<EmailRuleCriteria> criteria) {
		if (criteria.isEmpty()) {
			return true;
		}
		user = projectContentStatusService.setProjectContentStatus(user);

		for (EmailRuleCriteria criterium : criteria) {
			if (criterium.getEmailRuleCriteriaType().getCode().equalsIgnoreCase("NOT_STARTED")) {
				if (user.getProjectContentCompletionStatus() == 0) {
					return true;
				}
			}
			if (criterium.getEmailRuleCriteriaType().getCode().equalsIgnoreCase("IN_PROGRESS")) {
				if (user.getProjectContentCompletionStatus() == 1) {
					return true;
				}
			}
			if (criterium.getEmailRuleCriteriaType().getCode().equalsIgnoreCase("COMPLETED")) {
				if (user.getProjectContentCompletionStatus() == 2) {
					return true;
				}
			}

		}
		return false;
	}

}
