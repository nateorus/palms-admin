package com.pdinh.mail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleScheduleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.ScheduledEmailLogDao;
import com.pdinh.data.ms.dao.ScheduledEmailLogStatusDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;
import com.pdinh.persistence.ms.entity.ScheduledEmailLog;
import com.pdinh.persistence.ms.entity.ScheduledEmailLogStatus;
import com.pdinh.presentation.domain.EmailEventObj;
import com.pdinh.presentation.helper.TextUtils;

/**
 * Session Bean implementation class MailServiceLocalImpl
 */
@Stateless
@LocalBean
public class MailServiceLocalImpl implements MailServiceLocal {
	@Resource(name = "mail/pdinh")
	private Session mailSession;

	static private String CONNECTION_FACTORY = "jms/PalmsConnectionFactory";
	private static final String CONNECTION_QUEUE = "jms/EmlMessageQueue";

	private static final Logger log = LoggerFactory.getLogger(MailServiceLocalImpl.class);

	@EJB
	private ScheduledEmailLogStatusDao scheduledEmailLogStatusDao;

	@EJB
	private ScheduledEmailLogDao scheduledEmailLogDao;

	@EJB
	private EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;

	@EJB
	private EmailRuleScheduleDao emailRuleScheduleDao;

	@EJB
	private UserDao userDao;

	/**
	 * Default constructor.
	 */
	public MailServiceLocalImpl() {
	}

	@Override
	public Boolean sendMessage(String subject, String toAddress, String message) {
		return sendMailMessage(subject, "", toAddress, message);
	}

	@Override
	public Boolean sendMessage(String subject, String fromAddress, String toAddress, String message) {
		return sendMailMessage(subject, fromAddress, toAddress, message);
	}

	@Override
	public Boolean sendMessage(String subject, String fromAddress, String toAddress, String ccAddress,
			String bccAddress, String message) {
		List<String> ccAddresses = null;
		List<String> bccAddresses = null;

		if (ccAddress != null) {
			ccAddresses = Arrays.asList(ccAddress);
		}
		if (bccAddress != null) {
			bccAddresses = Arrays.asList(bccAddress);
		}

		return sendMessage(subject, fromAddress, Arrays.asList(toAddress), ccAddresses, bccAddresses, message);
	}

	@Override
	public Boolean sendMessage(String subject, String fromAddress, List<String> toAddresses, List<String> ccAddresses,
			List<String> bccAddresses, String message) {
		return sendMailMessage(subject, fromAddress, toAddresses, ccAddresses, bccAddresses, message);
	}

	@Override
	public Boolean sendJMSEmailMessage(String subject, String fromAddress, String replyToAddress, String toAddress,
			String message, Integer emailLogId) {
		// TODO Auto-generated method stub
		return sendMailJms(subject, fromAddress, replyToAddress, toAddress, message, emailLogId);
	}

	@Override
	public Boolean sendJMSEmailBatch(List<EmailEventObj> emails) {
		return sendBatchMailJms(emails);
	}

	@Override
	public Boolean sendEmailBatch(List<EmailEventObj> emails) {
		return sendBatchMail(emails);
	}

	private Boolean sendMailMessage(String subject, String fromAddress, String toAddress, String message) {
		MimeMessage msg = new MimeMessage(mailSession);
		Multipart multiPart = new MimeMultipart("alternative");

		try {

			MimeBodyPart text = new MimeBodyPart();
			MimeBodyPart html = new MimeBodyPart();

			log.debug("Attempting to send message to {}", toAddress);

			msg.setSubject(subject, "utf-8");
			if (!fromAddress.equals("")) {
				msg.setFrom(new InternetAddress(fromAddress));
			}
			msg.setRecipient(RecipientType.TO, new InternetAddress(toAddress));

			text.setText(TextUtils.getPlainText(message), "utf-8");
			multiPart.addBodyPart(text);

			html.setContent(message, "text/html; charset=utf-8");
			multiPart.addBodyPart(html);

			msg.setContent(multiPart);

			Transport.send(msg);

			log.debug("Sent message to {} successfully.", toAddress);

			return true;
		} catch (MessagingException me) {
			log.error("There was an error sending the email. {}", me.getMessage());
			return false;
		} catch (Exception e) {
			log.error("There was an error sending the email. {}", e.getMessage());
			return false;
		}
	}

	private Boolean sendMailMessage(String subject, String fromAddress, List<String> toAddresses,
			List<String> ccAddresses, List<String> bccAddresses, String message) {
		MimeMessage msg = new MimeMessage(mailSession);
		Multipart multiPart = new MimeMultipart("alternative");

		try {

			MimeBodyPart text = new MimeBodyPart();
			MimeBodyPart html = new MimeBodyPart();

			String debugRecipientList = getRecipientList(toAddresses, ccAddresses, bccAddresses);
			log.debug("Attempting to send message to {}", debugRecipientList);

			msg.setSubject(subject, "utf-8");
			if (!fromAddress.equals("")) {
				msg.setFrom(new InternetAddress(fromAddress));
			}

			addRecipients(msg, RecipientType.TO, toAddresses);
			addRecipients(msg, RecipientType.CC, ccAddresses);
			addRecipients(msg, RecipientType.BCC, bccAddresses);

			text.setText(TextUtils.getPlainText(message), "utf-8");
			multiPart.addBodyPart(text);

			html.setContent(message, "text/html; charset=utf-8");
			multiPart.addBodyPart(html);

			msg.setContent(multiPart);

			Transport.send(msg);

			log.debug("Sent message to {} successfully.", debugRecipientList);

			return true;
		} catch (MessagingException me) {
			log.error("There was an error sending the email. {}", me.getMessage());
			return false;
		} catch (Exception e) {
			log.error("There was an error sending the email. {}", e.getMessage());
			return false;
		}
	}

	private String getRecipientList(List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses) {
		StringBuilder builder = new StringBuilder();
		addDebugAddresses(builder, "TO", toAddresses);
		addDebugAddresses(builder, "CC", ccAddresses);
		addDebugAddresses(builder, "BCC", bccAddresses);

		return builder.toString();
	}

	private void addDebugAddresses(StringBuilder builder, String type, List<String> addresses) {
		if (addresses == null) {
			return;
		}

		builder.append(type);
		builder.append(": ");

		for (String address : addresses) {
			builder.append(address);
			builder.append(", ");
		}
	}

	private void addRecipients(MimeMessage msg, RecipientType type, List<String> addresses) throws AddressException,
			MessagingException {
		if (addresses == null || addresses.isEmpty()) {
			return;
		}

		InternetAddress[] addressArray = new InternetAddress[addresses.size()];
		for (int idx = 0; idx < addressArray.length; idx++) {
			addressArray[idx] = new InternetAddress(addresses.get(idx));
		}

		msg.addRecipients(type, addressArray);
	}
	@Asynchronous
	private Boolean sendMailMessages(List<MimeMessage> messages) {

		ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
				ScheduledEmailLogStatus.STATUS_SUCCESSFUL, 1);
		ScheduledEmailLogStatus statusUnknownError = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
				ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);

		Transport transport;
		try {
			transport = mailSession.getTransport();
		} catch (NoSuchProviderException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
			return false;
		}
		try {
			transport.connect();
		} catch (MessagingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return false;
		}

		boolean returnVal = true;
		for (MimeMessage msg : messages) {
			ScheduledEmailLog emailLog = new ScheduledEmailLog();
			try {
				emailLog = scheduledEmailLogDao.findById(Integer.valueOf(msg.getHeader("emailLogId")[0]));

				msg.removeHeader("emailLogId");

				msg.saveChanges();
				transport.sendMessage(msg, msg.getAllRecipients());
				log.debug("Sent message to {} successfully.", msg.getRecipients(Message.RecipientType.TO).toString());

				emailLog.setStatus(status);
				emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
				scheduledEmailLogDao.update(emailLog);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				emailLog.setStatus(statusUnknownError);
				emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
				emailLog.setErrorMessage(e.getMessage());
				scheduledEmailLogDao.update(emailLog);

				// manage exception
				returnVal = false;
			} catch (NumberFormatException e1) {
				log.error("Error while attempting to Get Email Log. {}", e1.getMessage());
				log.error("Could not send message or update log");
				returnVal = false;

			}

		}
		return returnVal;
	}

	private MimeMessage createMimeMessage(String subject, String fromAddress, String replyToAddress, String toAddress,
			String htmlMessage, String plainTextMessage, Integer emailLogId) {
		MimeMessage msg = new MimeMessage(mailSession);
		Multipart multiPart = new MimeMultipart("alternative");
		// ScheduledEmailLog emailLog =
		// scheduledEmailLogDao.findById(emailLogId);

		log.debug("Creating Mime Message {} ", toAddress);

		MimeBodyPart text = new MimeBodyPart();
		MimeBodyPart html = new MimeBodyPart();

		try {
			msg.setSubject(subject, "utf-8");

			msg.addHeader("emailLogId", String.valueOf(emailLogId));

			if (!fromAddress.equals("")) {
				msg.setFrom(new InternetAddress(fromAddress));
			}

			if (!replyToAddress.equals("")) {
				msg.setReplyTo(new InternetAddress[] { new InternetAddress(replyToAddress) });
			}
			msg.setRecipient(RecipientType.TO, new InternetAddress(toAddress));

			if (plainTextMessage != null) {
				text.setText(plainTextMessage, "utf-8");
				multiPart.addBodyPart(text);
			}

			if (htmlMessage != null) {
				html.setContent(htmlMessage, "text/html; charset=utf-8");
				multiPart.addBodyPart(html);
			}

			msg.setContent(multiPart);
		} catch (MessagingException e) {
			log.error("Error creating Mime Message for {}. Message: {}", toAddress, e.getMessage());
			e.printStackTrace();
			return null;
		} 

		return msg;
	}

	private Boolean sendMailMessage(String subject, String fromAddress, String replyToAddress, String toAddress,
			String htmlMessage, String plainTextMessage, Integer emailLogId) {
		MimeMessage msg = new MimeMessage(mailSession);
		Multipart multiPart = new MimeMultipart("alternative");
		ScheduledEmailLog emailLog = scheduledEmailLogDao.findById(emailLogId);

		try {
			log.debug("Attempting to send message to " + toAddress);

			MimeBodyPart text = new MimeBodyPart();
			MimeBodyPart html = new MimeBodyPart();

			msg.setSubject(subject, "utf-8");
			if (!fromAddress.equals("")) {
				msg.setFrom(new InternetAddress(fromAddress));
			}

			if (!replyToAddress.equals("")) {
				msg.setReplyTo(new InternetAddress[] { new InternetAddress(replyToAddress) });
			}
			msg.setRecipient(RecipientType.TO, new InternetAddress(toAddress));

			if (plainTextMessage != null) {
				text.setText(plainTextMessage, "utf-8");
				multiPart.addBodyPart(text);
			}

			if (htmlMessage != null) {
				html.setContent(htmlMessage, "text/html; charset=utf-8");
				multiPart.addBodyPart(html);
			}

			msg.setContent(multiPart);

			Transport.send(msg);

			log.debug("Sent message to {} successfully.", toAddress);

			ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
					ScheduledEmailLogStatus.STATUS_SUCCESSFUL, 1);
			emailLog.setStatus(status);
			emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
			scheduledEmailLogDao.update(emailLog);

			return true;
		} catch (MessagingException me) {
			log.error("There was MessagingException sending the email. {}", me.getMessage());
			me.printStackTrace();
			ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
					ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);
			emailLog.setStatus(status);
			emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
			emailLog.setErrorMessage(me.getMessage());
			scheduledEmailLogDao.update(emailLog);

			// Updating email rule schedule status and flagging an error.
			EmailRuleSchedule schedule = emailLog.getSchedule();
			if (schedule != null) {
				schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
						.findStatusByCode(EmailRuleScheduleStatus.ERRORS_ON_COMPLETE));
				schedule.setError(true);
				emailRuleScheduleDao.update(schedule);
			}

			// manage exception
			return false;
		} catch (Exception e) {
			log.error("There was an Exception sending the email. {}", e.getMessage());
			e.printStackTrace();
			ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
					ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);
			emailLog.setStatus(status);
			emailLog.setDateCompleted(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
			emailLog.setErrorMessage(e.getMessage());
			scheduledEmailLogDao.update(emailLog);

			// Updating email rule schedule status and flagging an error.
			EmailRuleSchedule schedule = emailLog.getSchedule();
			if (schedule != null) {
				schedule.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao
						.findStatusByCode(EmailRuleScheduleStatus.ERRORS_ON_COMPLETE));
				schedule.setError(true);
				emailRuleScheduleDao.update(schedule);
			}

			return false;
		}
	}

	@Override
	public Boolean sendBatchMail(List<EmailEventObj> emails) {
		ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
				ScheduledEmailLogStatus.STATUS_SENDING_EMAIL, 1);
		List<MimeMessage> messages = new ArrayList<MimeMessage>();
		boolean createMsgSuccess = true;
		for (EmailEventObj email : emails) {

			ScheduledEmailLog emlLog;
			if (email.isError()) {
				emlLog = scheduledEmailLogDao.create(email.getCompany(), email.getProject(),
						userDao.findById(email.getParticipant().getUserId()), email.getScheduledEmailText(), status,
						email.getSchedule(), email.getStoreSubject(), email.getStoreText());
				emlLog.setErrorMessage(email.getErrorText());
				emlLog.setStatus(scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
						ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1));
				scheduledEmailLogDao.update(emlLog);
			} else {
				emlLog = scheduledEmailLogDao.create(email.getCompany(), email.getProject(),
						userDao.findById(email.getParticipant().getUserId()), email.getScheduledEmailText(), status,
						email.getSchedule(), email.getStoreSubject(), email.getStoreText());

				MimeMessage msg = createMimeMessage(email.getSendSubject(), email.getFromAddress(),
						email.getReplyToAddress(), email.getParticipant().getEmail(), email.getSendText(),
						TextUtils.getPlainText(email.getSendText()), emlLog.getEmailLogId());
				if (msg == null){
					ScheduledEmailLogStatus errorStatus = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);
					emlLog.setStatus(errorStatus);
					emlLog.setErrorMessage("Failed to create Mime Message.  No Email was sent.  This could indicate invalid email format for recipient or sender address.");
					scheduledEmailLogDao.update(emlLog);
					createMsgSuccess = false;
					log.debug("Failed to create Mime Message.  From Address: {}, To Address: {}, Reply To: {}", new Object[]{email.getFromAddress(), email.getParticipant().getEmail(), email.getReplyToAddress()});
				}else{
					messages.add(msg);
				}
			}

		}
		Boolean sendSuccess = sendMailMessages(messages);
		if (sendSuccess && createMsgSuccess){
			return true;
		}else{
			return false;
		}
	}

	private Boolean sendBatchMailJms(List<EmailEventObj> emails) {
		Connection connection = null;
		javax.jms.Session session = null;
		MessageProducer producer = null;

		// Get the JNDI Context
		try {
			Context jndiContext = new InitialContext();

			// Create the Connection Factory
			ConnectionFactory connectionFactory = null;
			connectionFactory = (ConnectionFactory) jndiContext.lookup(CONNECTION_FACTORY);

			connection = connectionFactory.createConnection();

			// Create the session
			session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);

			// Create new queue
			Queue queue = (Queue) jndiContext.lookup(CONNECTION_QUEUE);

			// Create Message Producer
			producer = session.createProducer(queue);

			// Send the messages
			boolean returnValue = true;
			for (EmailEventObj email : emails) {
				ScheduledEmailLogStatus status = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
						ScheduledEmailLogStatus.STATUS_SENDING_EMAIL, 1);

				ScheduledEmailLog emlLog = scheduledEmailLogDao.create(email.getCompany(), email.getProject(),
						userDao.findById(email.getParticipant().getUserId()), email.getScheduledEmailText(), status,
						email.getSchedule(), email.getStoreSubject(), email.getStoreText());

				MapMessage mapMessage = session.createMapMessage();
				mapMessage.setString("subject", email.getSendSubject());
				mapMessage.setString("fromAddress", email.getFromAddress());
				mapMessage.setString("replyToAddress", email.getReplyToAddress());
				mapMessage.setString("toAddress", email.getParticipant().getEmail());
				mapMessage.setString("htmlMessage", email.getSendText());
				mapMessage.setString("plainTextMessage", TextUtils.getPlainText(email.getSendText()));
				mapMessage.setInt("emailLogId", emlLog.getEmailLogId());
				try {
					producer.send(mapMessage);
				} catch (JMSException e) {

					log.debug("JMSException caught while attempting to add email to queue. {}", email.getParticipant()
							.getEmail());
					ScheduledEmailLogStatus failStatus = scheduledEmailLogStatusDao.findStatusByCodeAndLangId(
							ScheduledEmailLogStatus.STATUS_UNKNOWN_ERROR, 1);
					emlLog.setStatus(failStatus);
					scheduledEmailLogDao.update(emlLog);
					returnValue = false;
				}

				log.debug("Email added to queue: {}", email.getParticipant().getEmail());
			}
			return returnValue;

		} catch (NamingException e) {
			log.error("ERROR:  NamingException detected in sendMailJms(): {}", e.getMessage());
			e.printStackTrace();
			return false;

		} catch (JMSException e) {
			log.error("ERROR:  JMSException detected in sendMailJms(): {}", e.getMessage());
			e.printStackTrace();
			return false;

		} finally {
			try {
				if (connection != null) {
					connection.close();
				}

			} catch (JMSException e) {
				log.error("Error closing JMS connection: {}", e.getMessage());
				e.printStackTrace();

			}
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				log.error("Error closing jms session: {}", e.getMessage());
			}
			try {
				if (producer != null) {
					producer.close();
				}
			} catch (JMSException e) {
				log.error("Error closing jms producer: {}", e.getMessage());
			}
		}
	}

	private Boolean sendMailJms(String subject, String fromAddress, String replyToAddress, String toAddress,
			String message, Integer emailLogId) {
		Connection connection = null;
		javax.jms.Session session = null;
		MessageProducer producer = null;

		// Get the JNDI Context
		try {
			Context jndiContext = new InitialContext();

			// Create the Connection Factory
			ConnectionFactory connectionFactory = null;
			connectionFactory = (ConnectionFactory) jndiContext.lookup(CONNECTION_FACTORY);
			connection = connectionFactory.createConnection();

			// Create the session
			session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);

			// Create new queue
			Queue queue = (Queue) jndiContext.lookup(CONNECTION_QUEUE);

			// Create Message Producer
			producer = session.createProducer(queue);

			// Send the message
			MapMessage mapMessage = session.createMapMessage();
			mapMessage.setString("subject", subject);
			mapMessage.setString("fromAddress", fromAddress);
			mapMessage.setString("replyToAddress", replyToAddress);
			mapMessage.setString("toAddress", toAddress);
			mapMessage.setString("htmlMessage", message);
			mapMessage.setString("plainTextMessage", TextUtils.getPlainText(message));
			mapMessage.setInt("emailLogId", emailLogId);
			producer.send(mapMessage);

			log.debug("Email added to queue: {}", toAddress);
			return true;

		} catch (NamingException e) {
			log.error("ERROR:  NamingException detected in sendMailJms(): {}", e.getMessage());
			e.printStackTrace();
			return false;
		} catch (JMSException e) {
			log.error("ERROR:  JMSException detected in sendMailJms(): {}", e.getMessage());
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}

			} catch (JMSException e) {
				log.error("Error closing JMS connection: {}", e.getMessage());
				e.printStackTrace();

			}
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				log.error("Error closing jms session: {}", e.getMessage());
			}
			try {
				if (producer != null) {
					producer.close();
				}
			} catch (JMSException e) {
				log.error("Error closing jms producer: {}", e.getMessage());
			}
		}
	}
}
