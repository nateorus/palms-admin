package com.pdinh.mail;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.EmailRuleScheduleDao;
import com.pdinh.data.ms.dao.EmailRuleScheduleStatusDao;
import com.pdinh.data.ms.dao.EmailRuleToProcessDao;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleRelative;
import com.pdinh.persistence.ms.entity.EmailRuleScheduleStatus;
import com.pdinh.persistence.ms.entity.EmailRuleToProcess;
import com.pdinh.persistence.ms.entity.Project;

@Stateless
@LocalBean
public class RelativeScheduleDateUpdater implements RelativeScheduleDateUpdaterLocal{

	private static final Logger log = LoggerFactory.getLogger(RelativeScheduleDateUpdater.class);
	
	@EJB
	private EmailRuleScheduleDao emailRuleScheduleDao;
	
	@EJB
	private EmailRuleToProcessDao emailRuleToProcessDao;
	
	@EJB
	private EmailRuleScheduleStatusDao emailRuleScheduleStatusDao;
	/**
	 * This is used to update any relative email rule schedules when project due date changes.
	 */
	@Override
	public void updateRelativeEmailScheduleDatesForProject(Date newDate,
			Date oldDate, int projectId) {
		List<EmailRuleSchedule> schedules = emailRuleScheduleDao.findRelativeActiveSchedulesByProject(projectId);
		if (schedules.size() > 0){
			//Date oldDate = new Date(p.getDueDate().getTime());
			//Date newDate = new Date(project.getDueDate().getTime());
			
			long diffInDays = getDateDiff(oldDate, newDate, TimeUnit.DAYS);
			//Current time
			Date now = new Date();
			log.debug("due date changed.  Difference in days: {}", diffInDays);
			
			log.debug("Found {} Schedules", schedules.size());
			for (EmailRuleSchedule schedule : schedules){
				EmailRuleScheduleRelative relative = (EmailRuleScheduleRelative)schedule;
				log.trace("Relative To Date: {}", relative.getRelativeToDate());
				Calendar cal = new GregorianCalendar();
				cal.setTime(new Date(relative.getRelativeToDate().getTime()));
				
				cal.add(Calendar.DAY_OF_MONTH, Integer.valueOf(Long.toString(diffInDays)));
				
				relative.setRelativeToDate(new Timestamp(cal.getTime().getTime()));
				log.trace("New Relative To Date: {}", relative.getRelativeToDate());
				//This is the date/time the rule would be processed
				Date relativeDate = this.calculateRelativeDate(relative.getDaysAfter(), 
						relative.getDays(), new Date(cal.getTime().getTime()));
				//Get the item from the queue
				EmailRuleToProcess ruleToProcess = emailRuleToProcessDao.findEmailRuleToProcessByScheduleId(relative.getId());
				
				if (relativeDate.before(now)){
					log.trace("Rule would execute in past.  Setting error status and deleting from queue");
					//rule would execute in past.  set to error and clear from queue
					relative.setComplete(true);
					relative.setEmailRuleScheduleStatus(emailRuleScheduleStatusDao.findStatusByCode(EmailRuleScheduleStatus.NOT_PROCESSED));
					emailRuleToProcessDao.delete(ruleToProcess);
				}else{
					//Update the send date for the item in the queue
					ruleToProcess.setSendDate(new Timestamp(relativeDate.getTime()));
					emailRuleToProcessDao.update(ruleToProcess);
				}
				//update the schedule
				emailRuleScheduleDao.update(relative);
			}
		}
		
	}
	
	private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit){
		long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	
	private static int getDays(boolean daysAfter, int days) {
		return (daysAfter) ? days :  (days *= -1);
	}

	
	private Date calculateRelativeDate(boolean daysAfter, int days, Date dueDate) {
		//Calendar cal = Calendar.getInstance();
		Calendar cal = new GregorianCalendar();
		days = getDays(daysAfter, days);
		
		if(dueDate != null) {
			//cal.setTimeInMillis(dueDate.getTime());
			cal.setTime(dueDate);
			cal.add(Calendar.DATE, days);
		}
		
		return cal.getTime();
	}
/**
 * This returns the earliest date that would be valid for all current relative email rule
 * schedules for this project.  This is not currently used.
 */
	@Override
	public Date getEarliestAllowedDueDate(int projectId) {
		List<EmailRuleSchedule> schedules = emailRuleScheduleDao.findRelativeActiveSchedulesByProject(projectId);
		Date now = new Date();
		int maxDaysBefore = 0;
		log.debug("Returned {} Schedules", schedules.size());
		for (EmailRuleSchedule schedule : schedules){
			
			EmailRuleScheduleRelative rsched = (EmailRuleScheduleRelative) schedule;
			log.debug("Days After: {}", rsched.getDaysAfter());
			if (!(rsched.getDaysAfter())){
				log.debug("Days Before: {}", rsched.getDays());
				if (maxDaysBefore < rsched.getDays()){
					maxDaysBefore = rsched.getDays();
				}
			}
			
		}
		Calendar cal = new GregorianCalendar();
		cal.setTime(now);
		cal.add(Calendar.DATE, maxDaysBefore);
		log.debug("Returning Date:  {}", cal.getTime());
		return cal.getTime();
	}	

}
