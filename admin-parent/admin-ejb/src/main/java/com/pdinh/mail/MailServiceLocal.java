package com.pdinh.mail;

import java.util.List;

import javax.ejb.Local;

import com.pdinh.presentation.domain.EmailEventObj;

@Local
public interface MailServiceLocal {
	public Boolean sendMessage(String subject, String toAddress, String message);

	public Boolean sendMessage(String subject, String fromAddress, String toAddress, String message);

	public Boolean sendMessage(String subject, String fromAddress, String toAddress, String ccAddress,
			String bccAddress, String message);

	public Boolean sendMessage(String subject, String fromAddress, List<String> toAddresses, List<String> ccAddresses,
			List<String> bccAddresses, String message);

	public Boolean sendJMSEmailMessage(String subject, String fromAddress, String replyToAddress, String toAddress,
			String message, Integer emailLogId);

	public Boolean sendJMSEmailBatch(List<EmailEventObj> emails);

	public Boolean sendEmailBatch(List<EmailEventObj> emails);

	public Boolean sendBatchMail(List<EmailEventObj> emails);
}
