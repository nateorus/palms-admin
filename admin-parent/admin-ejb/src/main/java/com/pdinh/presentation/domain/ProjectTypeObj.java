package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProjectTypeObj implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int projectTypeId;
	private String name;
	private String description;
	private String code;
	private Boolean targetLevelVisible;
	private Boolean urlGeneratorVisible;
	private int targetLevelGroupId;
	private String reportContentManifestRef;
	private Boolean allowReportDownloadVisible;
	private List<InstrumentSelectionObj> courses = new ArrayList<InstrumentSelectionObj>();
			
	public ProjectTypeObj() {
		super();
	}
}
