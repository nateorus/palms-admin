package com.pdinh.presentation.helper;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.EmailValidator;

import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.presentation.domain.AddUserResultObj;
import com.pdinh.presentation.domain.ErrorObj;
import com.pdinh.presentation.domain.ParticipantObj;

public class ParticipantValidator {

	public static final String FAIL_STATUS = "Failed";
	public static final String SUCCESS_STATUS = "Success";
	public static final Integer VALIDATION_ERROR = -1;

	private UserDao userDao;
	public static final String[] NAME_CHAR_BLACKLIST = { "--", "@@", "<", ">", "%", ";", "(", ")", "&", "+", "\\", "\"" };

	public ParticipantValidator(UserDao userDao) {
		// TODO Auto-generated constructor stub
		this.userDao = userDao;
	}

	/* */
	public AddUserResultObj validateUserObj(User user, Boolean isEdit) {
		// TODO: refactor all this to work properly
		ParticipantObj participantObj = new ParticipantObj(user.getUsersId(), user.getFirstname(), user.getLastname(),
				user.getEmail(), user.getUsername());
		// TODO: add back into validation until bulk uploader is fixed
		// definitely before may 16th, 2013
		// participantObj.setOfficeLocationId(user.getOfficeLocation().getId());
		AddUserResultObj result = new AddUserResultObj(participantObj);
		result.setStatus(SUCCESS_STATUS);
		result.setSuccess(true);
		return result;
	}

	/* */

	public AddUserResultObj validateUserObj(User user) {
		ParticipantObj participantObj = new ParticipantObj(user.getUsersId(), user.getFirstname(), user.getLastname(),
				user.getEmail(), user.getUsername());

		if (user.getOfficeLocation() != null) {
			participantObj.setOfficeLocationId(user.getOfficeLocation().getId());
		}

		AddUserResultObj result = new AddUserResultObj(participantObj);
		result.setStatus(SUCCESS_STATUS);
		result.setSuccess(true);
		List<User> existingUserList;
		if (user.getEmail() == null || user.getEmail() == "") {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC008", "No email address provided");
			result.getErrors().add(error);
		}
		if (user.getEmail() != null && !isValidEmail(user.getEmail())) {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC012", "Email address is invalid");
			result.getErrors().add(error);
		}
		// TODO: add back into validation until bulk uploader is fixed
		// definitely before may 16th, 2013
		/* -- NW 2/27/15 -- no longer care about office location
		if (user.getOfficeLocation() != null && user.getOfficeLocation().getId() == VALIDATION_ERROR) {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC017", "Office Location selection is invalid");
			result.getErrors().add(error);
		}
		*/
		try {
			existingUserList = userDao.findByEmailAndCompany(user.getEmail(), user.getCompany().getCompanyId());
			// TMO/DW if the email exists IN COMPANY and the user.id is
			// different, the email is in use
			for (User eUser : existingUserList) {
				if (eUser != null && eUser.getUsersId() != user.getUsersId()) {
					result.setStatus(FAIL_STATUS);
					result.setSuccess(false);
					ErrorObj error = new ErrorObj("IMEC007", "User with this email exists in system");
					result.getErrors().add(error);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (user.getUsername() == null || user.getUsername() == "") {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC009", "No logon ID provided");
			result.getErrors().add(error);
		}
		if (user.getUsername() != null && !isValidUsername(user.getUsername())) {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC011",
					"Logon ID can contain letters A-Z, letters 0-9, \".\", \"_\", \"-\", and must be between 4-50 characters long");
			result.getErrors().add(error);
		}
		try {
			existingUserList = userDao.findByUsername(user.getUsername(), false);
			// if the username exists and the user.id is different, the username
			// is in use
			for (User eUser : existingUserList) {
				if (eUser != null && eUser.getUsersId() != user.getUsersId()) {
					result.setStatus(FAIL_STATUS);
					result.setSuccess(false);
					ErrorObj error = new ErrorObj("IMEC010", "User with this logon ID exists in system");
					result.getErrors().add(error);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Boolean for bad chars in either name
		Boolean namesContainBadChars = false;
		// check firstname
		if (user.getFirstname() == null || user.getFirstname() == "") {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC005", "No First name provided");
			result.getErrors().add(error);
		} else {
			for (String badChar : NAME_CHAR_BLACKLIST) {
				if (user.getFirstname().indexOf(badChar) != -1) {
					result.setStatus(FAIL_STATUS);
					result.setSuccess(false);
					ErrorObj error = new ErrorObj("IMEC013", "First name contains illegal characters");
					result.getErrors().add(error);
					namesContainBadChars = true;
				}
			}
		}
		// check lastname
		if (user.getLastname() == null || user.getLastname() == "") {
			result.setStatus(FAIL_STATUS);
			result.setSuccess(false);
			ErrorObj error = new ErrorObj("IMEC006", "No Last name provided");
			result.getErrors().add(error);
		} else {
			for (String badChar : NAME_CHAR_BLACKLIST) {
				if (user.getLastname().indexOf(badChar) != -1) {
					result.setStatus(FAIL_STATUS);
					result.setSuccess(false);
					ErrorObj error = new ErrorObj("IMEC014", "Last name contains illegal characters");
					result.getErrors().add(error);
					namesContainBadChars = true;
				}
			}
		}

		if (namesContainBadChars) {
			String blackList = "";
			for (int i = 0; i < NAME_CHAR_BLACKLIST.length; i++) {
				blackList += "\"" + NAME_CHAR_BLACKLIST[i] + "\"";
				if (i < NAME_CHAR_BLACKLIST.length - 1) {
					blackList += ", ";
				}
			}
			ErrorObj error = new ErrorObj("IMEC015", "Names cannot contain the following characters: " + blackList);
			result.getErrors().add(error);
		}

		// password
		// Skip password validation for users who have already set their own
		// password
		// Password field is NULL in that case which is expected -- nw
		if (user.getLoginStatus() < 2) {
			if (user.getPassword() == null || user.getPassword() == "") {
				result.setStatus(FAIL_STATUS);
				result.setSuccess(false);
				ErrorObj error = new ErrorObj("IMEC005", "No Password provided");
				result.getErrors().add(error);
			} else {
				if (user.getCompany().getEnforceSecurePassword() == 1) {
					if (!validatePassword(user.getPassword())) {
						result.setStatus(FAIL_STATUS);
						result.setSuccess(false);
						ErrorObj error = new ErrorObj("IMEC002", "Password not secure");
						result.getErrors().add(error);
					}
				} else {
					if (user.getPassword().length() < 4) {
						result.setStatus(FAIL_STATUS);
						result.setSuccess(false);
						ErrorObj error = new ErrorObj("IMEC003", "Password too short");
						result.getErrors().add(error);
					}
					if (user.getPassword().length() > 50) {
						result.setStatus(FAIL_STATUS);
						result.setSuccess(false);
						ErrorObj error = new ErrorObj("IMEC004", "Password too long");
						result.getErrors().add(error);
					}
				}
			}
		}

		return result;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	private boolean isValidUsername(String username) {
		boolean isValid = true;
		try {
			isValid = username.length() >= 4 && username.length() <= 50;
		} catch (Exception e) {
			isValid = false;
		}
		String okUsernameChars = "1234567890abcdefghijklmnopqrstuvwxyz._-";
		for (int i = 0; i < username.length(); i++) {
			if (okUsernameChars.indexOf(username.toLowerCase().charAt(i)) == -1) {
				isValid = false;
				break;
			}
		}
		return isValid;
	}

	private boolean isValidEmail(String email) {
		boolean isValid = true;
		isValid = EmailValidator.getInstance().isValid(email);
		return isValid;
	}

	public static boolean validatePassword(String password) {
		Pattern p;
		Matcher m;
		int testsPassed = 0;
		int testsRequired = 3;
		if (password.length() < 8) {
			return false;
		}
		if (password.length() > 50) {
			return false;
		}
		p = Pattern.compile(".??[A-Z]");
		m = p.matcher(password);
		while (m.find()) {
			testsPassed++;
			break;
		}
		p = Pattern.compile(".??[a-z]");
		m = p.matcher(password);
		while (m.find()) {
			testsPassed++;
			break;
		}
		p = Pattern.compile(".??[0-9]");
		m = p.matcher(password);
		while (m.find()) {
			testsPassed++;
			break;
		}
		if (testsPassed >= testsRequired) {
			return true;
		}
		return false;
	}

}
