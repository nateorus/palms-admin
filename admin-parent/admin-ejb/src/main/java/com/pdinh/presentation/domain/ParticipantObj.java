package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;

public class ParticipantObj implements Serializable {

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(ParticipantObj.class);
	private static final long serialVersionUID = 1L;

	private Integer userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private ProjectObj project;
	private Map<String, CourseStatusObj> courseStatuses = new HashMap<String, CourseStatusObj>();

	private Integer userType;
	private String languageId;
	private String reportLanguageId;

	private String optional1;
	private String optional2;
	private String optional3;
	private String supervisorName;
	private String supervisorEmail;

	private boolean coachFlag;

	private List<NoteObj> notes;
	private List<DocumentObj> documents;
	// attribute for norms
	private final List<CourseNormsObj> courseNorms = new ArrayList<CourseNormsObj>();

	private Integer languageIdCHQ;
	private Integer languageIdFEX;
	private Integer officeLocationId;

	// TMO added for bulk import
	private String password;
	private String employeeId;

	private Timestamp dateAdded;
	private Timestamp initialEmailDate;
	private Timestamp reminderEmailDate;

	private CoachingPlanObj coachingPlanObj;
	private String participantExperienceUrl;
	private List<ExternalSubjectDataObj> consultants;
	private int rowKey;
	private Map<String, String> courseVersions = new LinkedHashMap<String, String>();
	private List<ConsentHistoryObj> consentHistory = new ArrayList<ConsentHistoryObj>();

	public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
	}

	public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName,
			Timestamp dateAdded) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.dateAdded = dateAdded;
	}

	public ParticipantObj() {
		super();
	}

	public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName,
			String optional1, String optional2, String optional3, String supervisorName, String supervisorEmail) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.setOptional1(optional1);
		this.setOptional2(optional2);
		this.setOptional3(optional3);
		this.setSupervisorName(supervisorName);
		this.setSupervisorEmail(supervisorEmail);

	}

	public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName,
			String languageId) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.languageId = languageId;
	}

	public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName,
			String langPref, String reportLangPref) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.languageId = langPref;
		this.reportLanguageId = reportLangPref;
	}

	public ParticipantObj(String firstName, String lastName, String email, String userName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
	}

	public ParticipantObj(Integer userId2, String firstName2, String lastName2, String email2, String username2,
			Timestamp dateAdded2, boolean coachFlag) {
		this.userId = userId2;
		this.firstName = firstName2;
		this.lastName = lastName2;
		this.email = email2;
		this.userName = username2;
		this.dateAdded = dateAdded2;
		this.coachFlag = coachFlag;
	}

	public ParticipantObj(Integer userId, String firstName, String lastName, String email, String userName,
			String optional1, String optional2, String optional3, String supervisorName, String supervisorEmail,
			boolean coachFlag) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.setOptional1(optional1);
		this.setOptional2(optional2);
		this.setOptional3(optional3);
		this.setSupervisorName(supervisorName);
		this.setSupervisorEmail(supervisorEmail);
		this.setCoachFlag(coachFlag);

	}

	public String courseVersionsToString() {
		StringBuffer string = new StringBuffer();
		for (Map.Entry<String, String> entry : courseVersions.entrySet()) {
			string.append(entry.getKey());
			string.append(":");
			string.append(entry.getValue());
			string.append("|");
		}

		return StringUtils.removeEnd(string.toString(), "|");
	}

	/*
	 * setter/getter
	 */
	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getOptional1() {
		return optional1;
	}

	public void setOptional1(String optional1) {
		this.optional1 = optional1;
	}

	public String getOptional2() {
		return optional2;
	}

	public void setOptional2(String optional2) {
		this.optional2 = optional2;
	}

	public String getOptional3() {
		return optional3;
	}

	public void setOptional3(String optional3) {
		this.optional3 = optional3;
	}

	public String getSupervisorName() {
		return supervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	public String getSupervisorEmail() {
		return supervisorEmail;
	}

	public void setSupervisorEmail(String supervisorEmail) {
		this.supervisorEmail = supervisorEmail;
	}

	public List<NoteObj> getNotes() {
		return notes;
	}

	public void setNotes(List<NoteObj> notes) {
		this.notes = notes;
	}

	public List<DocumentObj> getDocuments() {
		return documents;
	}

	public void setDocuments(List<DocumentObj> documents) {
		this.documents = documents;
	}

	public Integer getLanguageIdCHQ() {
		return languageIdCHQ;
	}

	public void setLanguageIdCHQ(Integer languageIdCHQ) {
		this.languageIdCHQ = languageIdCHQ;
	}

	public Integer getLanguageIdFEX() {
		return languageIdFEX;
	}

	public void setLanguageIdFEX(Integer languageIdFEX) {
		this.languageIdFEX = languageIdFEX;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Map<String, CourseStatusObj> getCourseStatuses() {
		return courseStatuses;
	}

	public void setCourseStatuses(Map<String, CourseStatusObj> courseStatuses) {
		this.courseStatuses = courseStatuses;
	}

	public void setReportLanguageId(String string) {
		this.reportLanguageId = string;
	}

	public String getReportLanguageId() {
		return reportLanguageId;
	}

	@Override
	public String toString() {
		return lastName + ", " + firstName + " (" + email + ")";
	}

	public void setProject(ProjectObj project) {
		this.project = project;
	}

	public ProjectObj getProject() {
		return project;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Timestamp getDateAdded() {
		return dateAdded;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setInitialEmailDate(Timestamp initialEmailDate) {
		this.initialEmailDate = initialEmailDate;
	}

	public Timestamp getInitialEmailDate() {
		return initialEmailDate;
	}

	public void setReminderEmailDate(Timestamp reminderEmailDate) {
		this.reminderEmailDate = reminderEmailDate;
	}

	public Timestamp getReminderEmailDate() {
		return reminderEmailDate;
	}

	public CourseStatusObj findCourseStatusObjectByCourseCode(String courseCode) {
		CourseStatusObj courseStatus = null;
		for (int i = 0; i < courseStatuses.size(); i++) {
			if (courseStatuses.get(i).getCourseCode().equals(courseCode)) {
				courseStatus = courseStatuses.get(i);
				break;
			}
		}
		return courseStatus;
	}

	public boolean isCoachFlag() {
		return coachFlag;
	}

	public boolean getCoachFlag() {
		return isCoachFlag();
	}

	public void setCoachFlag(boolean coachFlag) {
		this.coachFlag = coachFlag;
	}

	public CoachingPlanObj getCoachingPlanObj() {
		return coachingPlanObj;
	}

	public void setCoachingPlanObj(CoachingPlanObj coachingPlanObj) {
		this.coachingPlanObj = coachingPlanObj;
	}

	public Integer getOfficeLocationId() {
		return officeLocationId;
	}

	public void setOfficeLocationId(Integer officeLocationId) {
		this.officeLocationId = officeLocationId;
	}

	public List<CourseNormsObj> getCourseNorms() {
		return courseNorms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipantObj other = (ParticipantObj) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	public String getParticipantExperienceUrl() {
		return participantExperienceUrl;
	}

	public void setParticipantExperienceUrl(String participantExperienceUrl) {
		this.participantExperienceUrl = participantExperienceUrl;
	}

	public List<ExternalSubjectDataObj> getConsultants() {
		return consultants;
	}

	public void setConsultants(List<ExternalSubjectDataObj> consultants) {
		this.consultants = consultants;
	}

	public String consultantsToString() {
		String finalList = "";

		for (ExternalSubjectDataObj consultant : getConsultants()) {
			finalList += consultant.getLastname() + ", " + consultant.getFirstname() + "; ";
		}

		return finalList;
	}

	public int getRowKey() {
		return rowKey;
	}

	public void setRowKey(int rowKey) {
		this.rowKey = rowKey;
	}

	public Map<String, String> getCourseVersions() {
		return courseVersions;
	}

	public void setCourseVersions(Map<String, String> courseVersions) {
		this.courseVersions = courseVersions;
	}

	public List<ConsentHistoryObj> getConsentHistory() {
		return consentHistory;
	}

	public void setConsentHistory(List<ConsentHistoryObj> consentHistory) {
		this.consentHistory = consentHistory;
	}

}
