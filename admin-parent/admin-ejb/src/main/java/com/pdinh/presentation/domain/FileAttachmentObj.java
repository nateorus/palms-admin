package com.pdinh.presentation.domain;

import java.io.InputStream;
import java.sql.Timestamp;

public class FileAttachmentObj {

	private Integer id;
	private String name;
	private String path;
	private Timestamp dateUploaded;
	private String extAdminId;
	private InputStream newFileInputStream;
	private String url;

	public FileAttachmentObj() {
	}

	public FileAttachmentObj(Integer id, String name, String path, Timestamp dateUploaded, String extAdminId) {

		this.id = id;
		this.name = name;
		this.path = path;
		this.dateUploaded = dateUploaded;
		this.extAdminId = extAdminId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Timestamp getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Timestamp dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public String getExtAdminId() {
		return extAdminId;
	}

	public void setExtAdminId(String extAdminId) {
		this.extAdminId = extAdminId;
	}

	public InputStream getNewFileInputStream() {
		return newFileInputStream;
	}

	public void setNewFileInputStream(InputStream newFileInputStream) {
		this.newFileInputStream = newFileInputStream;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
