package com.pdinh.presentation.domain;

import java.io.Serializable;

public class EmailRuleCriteriaObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer value;
    private String label;
    private Integer id;
    
    public EmailRuleCriteriaObj() {}

    public EmailRuleCriteriaObj(Integer value, String label) {
    	this.value = value;
    	this.label = label;
    }
    
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
