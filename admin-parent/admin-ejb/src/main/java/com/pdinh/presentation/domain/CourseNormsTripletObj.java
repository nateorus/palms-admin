package com.pdinh.presentation.domain;

import java.io.Serializable;

public class CourseNormsTripletObj implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CourseNormsObj course;

	private NormGroupObj selectedGenNormGroup;
	private NormGroupObj selectedSpecNormGroup;

	public CourseNormsObj getCourse() {
		return course;
	}

	public void setCourse(CourseNormsObj course) {
		this.course = course;
	}

	public NormGroupObj getSelectedGenNormGroup() {
		return selectedGenNormGroup;
	}

	public void setSelectedGenNormGroup(NormGroupObj selectedGenNormGroup) {
		this.selectedGenNormGroup = selectedGenNormGroup;
	}

	public NormGroupObj getSelectedSpecNormGroup() {
		return selectedSpecNormGroup;
	}

	public void setSelectedSpecNormGroup(NormGroupObj selectedSpecNormGroup) {
		this.selectedSpecNormGroup = selectedSpecNormGroup;
	}

	@Override
	public String toString() {
		return "CourseNormsTripletObj [course=" + course.getAbbv() + ", selectedGenNormGroup="
				+ selectedGenNormGroup.getValue() + ", selectedSpecNormGroup=" + selectedSpecNormGroup.getValue() + "]";
	}

}
