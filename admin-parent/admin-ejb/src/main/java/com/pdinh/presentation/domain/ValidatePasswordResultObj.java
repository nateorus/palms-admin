package com.pdinh.presentation.domain;

import java.util.List;

public class ValidatePasswordResultObj extends ResultObj {

	private List<ErrorObj> errors;

	public List<ErrorObj> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorObj> errors) {
		this.errors = errors;
	}
}
