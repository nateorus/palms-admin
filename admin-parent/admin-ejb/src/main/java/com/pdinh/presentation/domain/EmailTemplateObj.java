package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.List;

public class EmailTemplateObj implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String title;
	private String style;
	private String type;
	private int isTemplate;
	private boolean active;
	private List<EmailTextObj> emailTexts;
	private List<ListItemObj> languages;
	private String level;    // SYSTEM, CLIENT, PROJECT
	private String fromKeyCode;
	private String customFrom;
	private String customReplyTo;
	private String constantStyleLabel;
	private String emailTypeProdLabel;
	
	public EmailTemplateObj () {}

	public EmailTemplateObj(int id, String title, String style, String type, int isTemplate, String level){
		this.id = id;
		this.title = title;
		this.style = style;
		this.type = type;
		this.isTemplate = isTemplate;
		this.level = level;
	}
	
	public EmailTemplateObj(int id, String title, String style, String type, int isTemplate, String level, String keyCode, 
													String customFrom, String customReplyTo, String constantStyleLabel, String emailTypeProdLabel){
		this.id = id;
		this.title = title;
		this.style = style;
		this.type = type;
		this.isTemplate = isTemplate;
		this.level = level;
		this.fromKeyCode = keyCode;
		this.customFrom = customFrom;
		this.customReplyTo = customReplyTo;
		this.constantStyleLabel = constantStyleLabel;
		this.emailTypeProdLabel = emailTypeProdLabel;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public int getIsTemplate() {
		return isTemplate;
	}
	public void setIsTemplate(int isTemplate) {
		this.isTemplate = isTemplate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<EmailTextObj> getEmailText() {
		return emailTexts;
	}
	public void setEmailText(List<EmailTextObj> emailText) {
		emailTexts = emailText;
	}
	public List<ListItemObj> getLanguages() {
		return languages;
	}
	public void setLanguages(List<ListItemObj> languages) {
		this.languages = languages;
	}

	public String getLevel(){
		return this.level;
	}
	public void setLevel(String level){
		this.level = level;
	}

	public String getFromKeyCode() {
		return fromKeyCode;
	}

	public void setFromKeyCode(String fromKeyCode) {
		this.fromKeyCode = fromKeyCode;
	}

	public String getCustomFrom() {
		return customFrom;
	}

	public void setCustomFrom(String customFrom) {
		this.customFrom = customFrom;
	}

	public String getCustomReplyTo() {
		return customReplyTo;
	}

	public void setCustomReplyTo(String customReplyTo) {
		this.customReplyTo = customReplyTo;
	}

	public String getConstantStyleLabel() {
		return constantStyleLabel;
	}

	public void setConstantStyleLabel(String constantStyleLabel) {
		this.constantStyleLabel = constantStyleLabel;
	}

	public String getEmailTypeProdLabel() {
		return emailTypeProdLabel;
	}

	public void setEmailTypeProdLabel(String emailTypeProdLabel) {
		this.emailTypeProdLabel = emailTypeProdLabel;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
