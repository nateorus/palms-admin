package com.pdinh.presentation.domain;



public class EmailTranslationObj{

	private String code;
	private String name;
	private boolean present;

	public void setPresent(boolean present) {
		this.present = present;
	}

	public boolean getPresent() {
		return present;
	}

	public EmailTranslationObj(String code, String name, boolean present) {
		super();
		this.code = code;
		this.name = name;
		this.present = present;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
