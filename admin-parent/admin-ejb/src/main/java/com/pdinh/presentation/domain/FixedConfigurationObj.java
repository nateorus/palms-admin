package com.pdinh.presentation.domain;

public class FixedConfigurationObj extends GenericObj {

	private static final long serialVersionUID = 1L;

	public FixedConfigurationObj(int id, String name, String code) {
		super(id, name, code);
	}
}
