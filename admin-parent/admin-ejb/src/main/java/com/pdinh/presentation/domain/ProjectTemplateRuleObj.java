package com.pdinh.presentation.domain;

import java.util.ArrayList;

public class ProjectTemplateRuleObj {
	private boolean enabled;
	private boolean visible;
	private boolean selected;
	private boolean enabledWithActiveProject;
	private boolean required;
	private String defaultValue;
	private ArrayList<String> list;

	ProjectTemplateRuleObj() {
	}

	public ProjectTemplateRuleObj(boolean enabled, boolean visible, boolean selected, boolean enabledWithActiveProject,
			boolean required, String defaultValue, ArrayList<String> list) {
		this.enabled = enabled;
		this.visible = visible;
		this.selected = selected;
		this.enabledWithActiveProject = enabledWithActiveProject;
		this.required = required;
		this.defaultValue = defaultValue;
		this.list = list;
	}

	public boolean enabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean visible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean selected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean enabledWithActiveProject() {
		return enabledWithActiveProject;
	}

	public void setEnabledWithActiveProject(boolean enabledWithActiveProject) {
		this.enabledWithActiveProject = enabledWithActiveProject;
	}

	public boolean required() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String defaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public ArrayList<String> list() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}
}
