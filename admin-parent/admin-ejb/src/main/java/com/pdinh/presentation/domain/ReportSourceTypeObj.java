package com.pdinh.presentation.domain;

import java.util.ArrayList;
import java.util.List;

import com.pdinh.persistence.ms.entity.ReportSourceType;

public class ReportSourceTypeObj {
	private List<ReportTypeObj> reportTypes;
	private ReportSourceType reportSourceType;

	public ReportSourceTypeObj(ReportSourceType reportSourceType) {
		this.reportSourceType = reportSourceType;
		this.reportTypes = new ArrayList<ReportTypeObj>();
	}

	public List<ReportTypeObj> getReportTypes() {
		return reportTypes;
	}

	// public void setReportTypes(List<ReportTypeObj> reportTypes) {
	// this.reportTypes = reportTypes;
	// }

	public ReportSourceType getReportSourceType() {
		return reportSourceType;
	}

	// public void setReportSourceType(ReportSourceType reportSourceType) {
	// this.reportSourceType = reportSourceType;
	// }

	public void addReportType(ReportTypeObj rto) {
		this.reportTypes.add(rto);
	}
}
