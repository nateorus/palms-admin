package com.pdinh.presentation.domain;

import java.io.Serializable;

/**
 * Presentation DTO for Target level types
 */
public class TargetLevelTypeObj implements Serializable {
	private static final long serialVersionUID = 1L;
	private int targetLevelTypeId;
	private String name;
	private String code;
	
	public TargetLevelTypeObj(int targetLevelTypeId, String name, String code) {
		this.targetLevelTypeId = targetLevelTypeId;
		this.name = name;
		this.code = code;
	}

	public int getTargetLevelTypeId() {
		return targetLevelTypeId;
	}

	public void setTargetLevelTypeId(int targetLevelTypeId) {
		this.targetLevelTypeId = targetLevelTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
