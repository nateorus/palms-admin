package com.pdinh.presentation.domain;

public class ErrorObj extends ResultObj{

	private String errorCode;
	private String errorMsg;
	
	
	
	public ErrorObj(String errorCode, String errorMsg) {
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
		//since this is an error result, success is false.
		this.setSuccess(false);
	}
	
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	
	
}
