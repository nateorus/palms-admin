package com.pdinh.presentation.domain;

import java.io.Serializable;

public class ConsentTextObj implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String text;
	private String languageName;
	private String languageCode;
	private String constant;

	public ConsentTextObj() {
		super();
	}

	public ConsentTextObj(int id, String text, String languageName, String languageCode, String constant) {
		super();
		this.id = id;
		this.text = text;
		this.setLanguageName(languageName);
		this.setLanguageCode(languageCode);
		this.constant = constant;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(String constant) {
		this.constant = constant;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}


}
