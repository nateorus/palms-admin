package com.pdinh.presentation.domain;

public class ParticipantDocumentObj extends DocumentObj {

	private static final long serialVersionUID = 1L;
	
	public ParticipantDocumentObj() {
	}
	
	public ParticipantDocumentObj(int documentId, String description,
		Boolean isIntegrationGrid, int type, String typeDisplay,
		FileAttachmentObj fileAttachment, boolean newDocument) {
		
		super(documentId, description, isIntegrationGrid, type, typeDisplay,
			fileAttachment, newDocument);		
	}	
}
