package com.pdinh.presentation.domain;

import java.io.Serializable;

/**
 * Document Will probably need to be more generic and extended into two separate
 * items: Project and Participant documents. Each has different "owners" and a
 * different selection of types. Project Doc Types: Success Profile; Job
 * Description; Assessment Briefing Exchange (ABE); Pre-Call Notes; Other.
 * Participant Doc Types:
 */
public abstract class DocumentObj implements Serializable {
	private static final long serialVersionUID = 1L;

	protected int documentId;
	protected String description;
	protected String shortDescription;
	protected Boolean isIntegrationGrid;
	protected int type;
	protected String typeDisplay;
	protected FileAttachmentObj fileAttachment;
	protected boolean newDocument = true;

	public DocumentObj() {
	}

	public DocumentObj(int documentId, String description, Boolean isIntegrationGrid, int type, String typeDisplay,
			FileAttachmentObj fileAttachment, Boolean newDocument) {

		this.documentId = documentId;
		this.description = description;
		this.isIntegrationGrid = isIntegrationGrid;
		this.type = type;
		this.typeDisplay = typeDisplay;
		this.fileAttachment = fileAttachment;
		this.newDocument = newDocument;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsIntegrationGrid() {
		return isIntegrationGrid;
	}

	public void setIsIntegrationGrid(Boolean isIntegrationGrid) {
		this.isIntegrationGrid = isIntegrationGrid;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTypeDisplay() {
		return typeDisplay;
	}

	public void setTypeDisplay(String typeDisplay) {
		this.typeDisplay = typeDisplay;
	}

	public FileAttachmentObj getFileAttachment() {
		return fileAttachment;
	}

	public void setFileAttachment(FileAttachmentObj fileAttachment) {
		this.fileAttachment = fileAttachment;
	}

	public boolean isNewDocument() {
		return newDocument;
	}

	public void setNewDocument(boolean newDocument) {
		this.newDocument = newDocument;
	}

	public String getShortDescription() {
		if (shortDescription != null) {
			if (shortDescription.length() > 100) {
				shortDescription = shortDescription.substring(0, 100) + "...";
			}
		}
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

}
