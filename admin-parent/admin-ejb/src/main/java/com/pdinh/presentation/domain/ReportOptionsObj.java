/**
 * Copyright (c) 2013 Korn/Ferry International
 * All rights reserved.
 */
package com.pdinh.presentation.domain;

/**
 * This is a key/value pair implementation for reporting options
 */
public class ReportOptionsObj {

	//
	// Staics variables
	//

	// Report Option (RO) constants
	public static final String RO_NAMES = "names";
	public static final String RO_IDS = "ids";

	public static final String RO_POT_READI = "Potential by Readiness";
	public static final String RO_DLCI_POT = "dLCI by Potential";

	//
	// Instance variables
	//
	private String name;
	private String code;

	//
	// Constructors
	//
	public ReportOptionsObj(String code, String name) {
		this.code = code;
		this.name = name;
	}

	//
	// Instance methods
	//

	// ---------------------------------------------
	public String getName() {
		return this.name;
	}

	public void setName(String value) {
		this.name = value;
	}

	// ---------------------------------------------
	public String getCode() {
		return this.code;
	}

	public void setCode(String value) {
		this.code = value;
	}

}
