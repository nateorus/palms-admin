package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FixedDateObj extends EmailRuleScheduleObj implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(FixedDateObj.class);
    private Date date;
    private String type = "fixed";
    private TimeZone timezone;
    private String displayDate;
    
    public FixedDateObj () {}
    
    public FixedDateObj (Date date){
    	this.date = date;
    }
    
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public TimeZone getTimezone() {
		return timezone;
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}


	

	public String getDisplayDate() {
		TimeZone zone;
		if (!(this.getTimezone() == null)){
			zone = this.getTimezone();
		}else{
			log.debug("Timezone is null.. using default");
			zone = TimeZone.getDefault();
		}
		
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(this.date);
		
		SimpleDateFormat dateFormatZone = new SimpleDateFormat("yyyy-MMM-dd hh:mm a zzzzz");
		dateFormatZone.setTimeZone(zone);
		return dateFormatZone.format(calendar.getTime());
	}

	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}

}