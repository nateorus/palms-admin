package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.pdinh.auth.data.obj.ExternalSubjectDataObj;

public class ProjectObj implements Serializable {
	private static final long serialVersionUID = 1L;

	private int projectId;
	private String name;
	private String code;
	private String type;
	private String typeCode;
	private int typeId;
	private String targetLevel;
	private int targetLevelIndex;
	private String extAdminId;
	private int participantCount;
	private boolean participantsAdded;
	private Timestamp dateAdded;
	private Timestamp dueDate;
	private String consultants;
	private String clientName;
	private int clientId;
	private String clientMgmtOffice;
	private String scoring;
	private ExternalSubjectDataObj assignedPmPc;
	private ExternalSubjectDataObj internalScheduler;
	private int assignedPmPcId;
	private int internalSchedulerId;
	private boolean deliveryExist;
	private String chqLanguageCode;

	private FixedConfigurationObj configuration;
	private int configurationId;
	private int scoringMethodId;

	private String statusName;
	private boolean billable;
	private int noneBillableTypeId;
	private boolean includeClientLogonIdPassword;

	public ProjectObj() {
	}

	public ProjectObj(int projectId, String name) {
		this.projectId = projectId;
		this.name = name;
	}

	public ProjectObj(int projectId, String name, String type, int typeId) {
		this.projectId = projectId;
		this.name = name;
		this.type = type;
		this.typeId = typeId;
	}

	public ProjectObj(int projectId, String name, String code, String type, int typeId, String targetLevel,
			String extAdminId, int participantCount, Timestamp dateAdded, Timestamp dueDate) {
		this.projectId = projectId;
		this.code = code;
		this.name = name;
		this.type = type;
		this.typeId = typeId;
		this.targetLevel = targetLevel;
		this.extAdminId = extAdminId;
		this.participantCount = participantCount;
		this.dateAdded = dateAdded;
		this.dueDate = dueDate;
	}

	public ProjectObj(int projectId, String name, String code, String type, int typeId, String targetLevel,
			String extAdminId, int participantCount, Timestamp dateAdded, Timestamp dueDate, String consultants) {
		this.projectId = projectId;
		this.code = code;
		this.name = name;
		this.type = type;
		this.typeId = typeId;
		this.targetLevel = targetLevel;
		this.extAdminId = extAdminId;
		this.participantCount = participantCount;
		this.dateAdded = dateAdded;
		this.dueDate = dueDate;
		this.consultants = consultants;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setParticipantCount(int participantCount) {
		this.participantCount = participantCount;
	}

	public int getParticipantCount() {
		return participantCount;
	}

	@Override
	public String toString() {
		return getName();
	}

	public void setExtAdminId(String extAdminId) {
		this.extAdminId = extAdminId;
	}

	public String getExtAdminId() {
		return extAdminId;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Timestamp getDateAdded() {
		return dateAdded;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	/*
		public String getFromEmail() {
			return fromEmail;
		}

		public void setFromEmail(String fromEmail) {
			this.fromEmail = fromEmail;
		}
	*/
	public Timestamp getDueDate() {
		return dueDate;
	}

	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	public String getConsultants() {
		return consultants;
	}

	public void setConsultants(String consultants) {
		this.consultants = consultants;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientMgmtOffice() {
		return clientMgmtOffice;
	}

	public void setClientMgmtOffice(String clientMgmtOffice) {
		this.clientMgmtOffice = clientMgmtOffice;
	}

	public String getScoring() {
		return scoring;
	}

	public void setScoring(String scoring) {
		this.scoring = scoring;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public int getScoringMethodId() {
		return scoringMethodId;
	}

	public void setScoringMethodId(int scoringMethodId) {
		this.scoringMethodId = scoringMethodId;
	}

	public boolean isDeliveryExist() {
		return deliveryExist;
	}

	public void setDeliveryExist(boolean deliveryExist) {
		this.deliveryExist = deliveryExist;
	}

	public String getChqLanguageCode() {
		return chqLanguageCode;
	}

	public void setChqLanguageCode(String chqLanguage) {
		this.chqLanguageCode = chqLanguage;
	}

	public ExternalSubjectDataObj getAssignedPmPc() {
		return assignedPmPc;
	}

	public void setAssignedPmPc(ExternalSubjectDataObj assignedPmPc) {
		this.assignedPmPc = assignedPmPc;
	}

	public ExternalSubjectDataObj getInternalScheduler() {
		return internalScheduler;
	}

	public void setInternalScheduler(ExternalSubjectDataObj internalScheduler) {
		this.internalScheduler = internalScheduler;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public FixedConfigurationObj getConfiguration() {
		return configuration;
	}

	public void setConfiguration(FixedConfigurationObj configuration) {
		this.configuration = configuration;
	}

	public int getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(int configurationId) {
		this.configurationId = configurationId;
	}

	public boolean isParticipantsAdded() {
		return participantsAdded;
	}

	public void setParticipantsAdded(boolean participantsAdded) {
		this.participantsAdded = participantsAdded;
	}

	public int getAssignedPmPcId() {
		return assignedPmPcId;
	}

	public void setAssignedPmPcId(int assignedPmPcId) {
		this.assignedPmPcId = assignedPmPcId;
	}

	public int getInternalSchedulerId() {
		return internalSchedulerId;
	}

	public void setInternalSchedulerId(int internalSchedulerId) {
		this.internalSchedulerId = internalSchedulerId;
	}

	public int getNoneBillableTypeId() {
		return noneBillableTypeId;
	}

	public void setNoneBillableTypeId(int noneBillableTypeId) {
		this.noneBillableTypeId = noneBillableTypeId;
	}

	public boolean isBillable() {
		return billable;
	}

	public void setBillable(boolean billable) {
		this.billable = billable;
	}

	public int getTargetLevelIndex() {
		return targetLevelIndex;
	}

	public void setTargetLevelIndex(int targetLevelIndex) {
		this.targetLevelIndex = targetLevelIndex;
	}

	public boolean isIncludeClientLogonIdPassword() {
		return includeClientLogonIdPassword;
	}

	public void setIncludeClientLogonIdPassword(boolean includeClientLogonIdPassword) {
		this.includeClientLogonIdPassword = includeClientLogonIdPassword;
	}

}
