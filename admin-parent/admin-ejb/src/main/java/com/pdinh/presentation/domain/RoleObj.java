package com.pdinh.presentation.domain;

import java.io.Serializable;

public class RoleObj implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String label;
	private String name;
	private String description;
	
	public RoleObj() {}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
