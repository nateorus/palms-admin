package com.pdinh.presentation.domain;

import java.io.Serializable;

public class SuperUserObj implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer userId;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private Integer companyId;
	
	public SuperUserObj(Integer userId, String firstName, String lastName, String username, String password) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}
	
	public String toString() {
		return lastName + ", " + firstName + " (" + username + ")";
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
}
