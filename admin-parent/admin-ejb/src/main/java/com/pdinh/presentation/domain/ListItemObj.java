package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.lang3.builder.CompareToBuilder;

public class ListItemObj implements Comparable<ListItemObj>, Serializable {

	private static final long serialVersionUID = 4530608646472965483L;
	private Integer type;
	private String id;
	private String name;

	public ListItemObj() {
	}

	public ListItemObj(Integer type, String name) {
		this.type = type;
		this.name = name;
		this.id = null;
	}

	public ListItemObj(Integer type, String name, String id) {
		this.type = type;
		this.name = name;
		this.id = id;
	}

	public ListItemObj(String name, String id) {
		this.type = 0;
		this.name = name;
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Override
	public int compareTo(ListItemObj o) {
		return Comparators.NAME.compare(this, o);
	}

	public static class Comparators {

		public static Comparator<ListItemObj> NAME = new Comparator<ListItemObj>() {
			@Override
			public int compare(ListItemObj o1, ListItemObj o2) {
				return new CompareToBuilder().append(o1.getName().toUpperCase(), o2.getName().toUpperCase())
						.toComparison();
			}
		};
	}

}
