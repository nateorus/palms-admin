package com.pdinh.presentation.domain;

public class UpdateReportRequestResultObj extends ResultObj {

	public UpdateReportRequestResultObj(String status){
		this.setStatus(status);
		this.setSuccess(true);
	}
}
