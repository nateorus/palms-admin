package com.pdinh.presentation.domain;

import java.io.Serializable;

public class NoteObj implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String NO_AUTHOR = "<Author not available>";

	String text;
	String date;
	String author;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public NoteObj() {
		this.text = "";
	}

	public NoteObj(String text) {
		this.text = text;
	}

	public NoteObj(String text, String authorId, String date) {
		this.text = text;
		this.author = authorId;
		this.date = date;
	}

	public String toString() {
		return text;
	}

}
