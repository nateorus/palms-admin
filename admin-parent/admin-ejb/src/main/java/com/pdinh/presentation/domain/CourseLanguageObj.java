package com.pdinh.presentation.domain;

import java.util.List;

public class CourseLanguageObj {
	private String courseAbbv;
	private int courseId;
	private List<LanguageObj> courseLangs;
	
	public CourseLanguageObj(String courseAbbv, int courseId, List<LanguageObj> courseLangs){
		this.setCourseAbbv(courseAbbv);
		this.setCourseId(courseId);
		this.setCourseLangs(courseLangs);
	}

	public String getCourseAbbv() {
		return courseAbbv;
	}

	public void setCourseAbbv(String courseAbbv) {
		this.courseAbbv = courseAbbv;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public List<LanguageObj> getCourseLangs() {
		return courseLangs;
	}

	public void setCourseLangs(List<LanguageObj> courseLangs) {
		this.courseLangs = courseLangs;
	}
}
