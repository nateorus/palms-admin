package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Date;

public class RecurringDateObj extends EmailRuleScheduleObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int option;
    private Date startDate;
    private Date endDate;
    private String type = "recurring";

    public RecurringDateObj () {}

    public RecurringDateObj (int option, Date startDate, Date endDate) {
    	this.option = option;
    	this.startDate = startDate;
    	this.endDate = endDate;
    }
    
	public int getOption() {
		return option;
	}
	public void setOption(int option) {
		this.option = option;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}
}