package com.pdinh.presentation.domain;

import java.util.ArrayList;
import java.util.List;

import com.pdinh.persistence.ms.entity.User;

public class AddUserResultObj extends ResultObj{

	private ParticipantObj user;
	private List<ErrorObj> errors;

	public AddUserResultObj(ParticipantObj user) {
		this.user = user;
		this.errors = new ArrayList<ErrorObj>();
	}
	public ParticipantObj getUser() {
		return user;
	}
	public void setUser(ParticipantObj user) {
		this.user = user;
	}
	public List<ErrorObj> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorObj> errors) {
		this.errors = errors;
	}

	
	
}
