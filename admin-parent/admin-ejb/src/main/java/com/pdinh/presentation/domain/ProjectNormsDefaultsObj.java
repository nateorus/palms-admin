package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pdinh.data.instrumentnorms.Course;

public class ProjectNormsDefaultsObj implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer projectId;

	private List<CourseNormsTripletObj> triplets = new ArrayList<CourseNormsTripletObj>();;

	/*
	 * util fn
	 */

	public CourseNormsTripletObj getTripletByCourse(Course c) {
		for (CourseNormsTripletObj t : triplets) {
			t.getCourse().getAbbv().equals(c.getAbbv());
			return t;
		}
		return null;
	}

	/*
	 * setter/getters
	 */
	public List<CourseNormsTripletObj> getTriplets() {
		return triplets;
	}

	public void setTriplet(List<CourseNormsTripletObj> triplet) {
		this.triplets = triplet;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer project) {
		this.projectId = project;
	}

}
