package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ScoringMethodObj implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private int scoringMethodId;

	public static List<ScoringMethodObj> scoringMethodDb = new ArrayList<ScoringMethodObj>();
	static {
		scoringMethodDb.add(new ScoringMethodObj(1, "Standard"));
		scoringMethodDb.add(new ScoringMethodObj(2, "Excel"));
		scoringMethodDb.add(new ScoringMethodObj(3, "Paper"));
	}

	public ScoringMethodObj() {

	}

	public ScoringMethodObj(int id, String name) {
		this.scoringMethodId = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScoringMethodId() {
		return scoringMethodId;
	}

	public void setScoringMethodId(int scoringMethodId) {
		this.scoringMethodId = scoringMethodId;
	}

	public static String getScoringMethodName(int scoringMethodId) {
		String method = "";

		for (ScoringMethodObj scoringMethod : scoringMethodDb) {
			if (scoringMethod.scoringMethodId == scoringMethodId) {
				method = scoringMethod.getName();
				break;
			}
		}
		return method;
	}

}
