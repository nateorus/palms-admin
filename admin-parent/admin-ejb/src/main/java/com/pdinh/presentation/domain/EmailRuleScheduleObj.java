package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Date;

public class EmailRuleScheduleObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Integer ruleId;
    private Date scheduledDate;
    private EmailRuleScheduleStatusObj currentStatus;
    private boolean complete;
    private boolean error;
    
    public EmailRuleScheduleObj () {}
    
    public EmailRuleScheduleObj (Integer ruleId, Date scheduledDate, EmailRuleScheduleStatusObj currentStatus, boolean complete, boolean error) {
    	this.ruleId = ruleId;
    	this.scheduledDate = scheduledDate;
    	this.currentStatus = currentStatus;
    	this.complete = complete;
    	this.error = error;
    }
    
	public Date getScheduledDate() {
		return scheduledDate;
	}
	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}
	
	public EmailRuleScheduleStatusObj getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(EmailRuleScheduleStatusObj currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Integer getRuleId() {
		return ruleId;
	}
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
}
