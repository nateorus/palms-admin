package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.CompareToBuilder;

import com.pdinh.persistence.ms.entity.ReportType;

public class ReportTypeObj implements Comparable<ReportTypeObj>, Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String code;
	private boolean selected;
	private boolean reportLanguageVisible;
	private boolean participantLanguageVisible;
	private Integer targetLevelGroupId;
	private int reportSourceTypeId;
	private boolean groupReport;
	private String contentType;

	// Default rpt options to false... controls multiple fields
	private boolean rptIdNameOptVisible = false;
	private boolean rptTitleVisible = false;

	private List<TargetLevelTypeObj> targetLevelTypes = new ArrayList<TargetLevelTypeObj>();
	private List<ReportOptionsObj> rptIdNameList = new ArrayList<ReportOptionsObj>();

	private String languageCode;
	private String targetLevelOverride = "";
	private int targetLevelOverrideIndex;

	private String rptIdNameOpt = ReportOptionsObj.RO_NAMES; // Default to names
	private String rptTitle = "";

	// Analytics visibility
	private boolean analyticsNameOptVisible = false;
	private boolean analyticsShowLciOptVisible = false;
	private boolean analyticsLabelsOptVisible = false;
	private boolean analyticsSortOptVisible = false;

	// Analytics selected options
	private boolean analyticsNameOpt = true;
	private boolean analyticsShowLciOpt = true;
	private String analyticsLabelsOpt;
	private String curSortGroupId;
	private String analyticsSortIdOpt;

	// Analytics selection data
	private List<ReportOptionsObj> analytics16bLabelsList = new ArrayList<ReportOptionsObj>();
	private List<ReportOptionsObj> analyticsSortGroups = new ArrayList<ReportOptionsObj>();
	private List<ReportOptionsObj> analyticsSortItems = new ArrayList<ReportOptionsObj>();

	private Map<String, ArrayList<ReportOptionsObj>> sortOptions = new HashMap<String, ArrayList<ReportOptionsObj>>();

	public ReportTypeObj() {
		super();
	}

	public ReportTypeObj(String name, String code, boolean selected, boolean reportLanguageVisible,
			boolean participantLanguageVisible) {
		super();
		this.name = name;
		this.code = code;
		this.selected = selected;
		this.reportLanguageVisible = reportLanguageVisible;
		this.participantLanguageVisible = participantLanguageVisible;
		this.languageCode = "en";
	}

	public ReportTypeObj(ReportTypeObj rto) {
		super();
		this.name = rto.name;
		this.code = rto.code;
		this.selected = rto.selected;
		this.reportLanguageVisible = rto.reportLanguageVisible;
		this.participantLanguageVisible = rto.participantLanguageVisible;
		this.languageCode = rto.languageCode;
		this.targetLevelOverride = rto.targetLevelOverride;
	}

	public ReportTypeObj(String name, String code, boolean reportLanguageVisible, boolean participantLanguageVisible,
			Integer targetLevelGroupId, int reportSourceTypeId, boolean groupReport, String contentType) {
		super();
		this.name = name;
		this.code = code;
		this.reportLanguageVisible = reportLanguageVisible;
		this.participantLanguageVisible = participantLanguageVisible;
		this.targetLevelGroupId = targetLevelGroupId;
		this.reportSourceTypeId = reportSourceTypeId;
		this.groupReport = groupReport;
		this.contentType = contentType;
	}

	public static ReportTypeObj transformReportType(ReportType reportType) {
		return new ReportTypeObj(reportType.getName(), reportType.getCode(), reportType.getReportLanguageVisible(),
				reportType.getParticipantLanguageVisible(), (reportType.getTargetLevelGroup() == null) ? null
						: reportType.getTargetLevelGroup().getTargetLevelGroupId(),
				(reportType.getReportSourceType() == null) ? null : reportType.getReportSourceType()
						.getReportSourceTypeId(), reportType.isGroupReport(), reportType.getContentType());
	}

	public void handleGroupChange() {
		analyticsSortItems = sortOptions.get(curSortGroupId);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean getSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getTargetLevelOverride() {
		return targetLevelOverride;
	}

	public void setTargetLevelOverride(String targetLevelOverride) {
		this.targetLevelOverride = targetLevelOverride;
	}

	public String getRptIdNameOpt() {
		return this.rptIdNameOpt;
	}

	public void setRptIdNameOpt(String valuee) {
		this.rptIdNameOpt = valuee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isReportLanguageVisible() {
		return reportLanguageVisible;
	}

	public boolean isParticipantLanguageVisible() {
		return participantLanguageVisible;
	}

	public boolean isRptIdNameOptVisible() {
		return rptIdNameOptVisible;
	}

	public boolean isRptTitleVisible() {
		return rptTitleVisible;
	}

	public void setReportLanguageVisible(boolean reportLanguageVisible) {
		this.reportLanguageVisible = reportLanguageVisible;
	}

	public void setParticipantLanguageVisible(boolean participantLanguageVisible) {
		this.participantLanguageVisible = participantLanguageVisible;
	}

	public void setRptIdNameOptVisible(boolean value) {
		this.rptIdNameOptVisible = value;
	}

	public void setRptTitleVisible(boolean value) {
		this.rptTitleVisible = value;
	}

	public List<TargetLevelTypeObj> getTargetLevelTypes() {
		return targetLevelTypes;
	}

	public void setTargetLevelTypes(List<TargetLevelTypeObj> targetLevelTypes) {
		this.targetLevelTypes = targetLevelTypes;
	}

	public List<ReportOptionsObj> getRptIdNameList() {
		return this.rptIdNameList;
	}

	public void setRptIdNameList(List<ReportOptionsObj> value) {
		this.rptIdNameList = value;
	}

	public String getRptTitle() {
		return this.rptTitle;
	}

	public void setRptTitle(String value) {
		this.rptTitle = value;
	}

	public boolean isTargetLevelOverrideVisible() {
		return (targetLevelTypes.size() > 0);
	}

	public int getReportSourceTypeId() {
		return reportSourceTypeId;
	}

	public void setReportSourceTypeId(int reportSourceTypeId) {
		this.reportSourceTypeId = reportSourceTypeId;
	}

	public void setAnalyticsNameOptVisible(boolean value) {
		this.analyticsNameOptVisible = value;
	}

	public boolean isAnalyticsNameOptVisible() {
		return this.analyticsNameOptVisible;
	}

	public void setAnalyticsShowLciOptVisible(boolean value) {
		this.analyticsShowLciOptVisible = value;
	}

	public boolean isAnalyticsShowLciOptVisible() {
		return this.analyticsShowLciOptVisible;
	}

	public void setAnalyticsLabelsOptVisible(boolean value) {
		this.analyticsLabelsOptVisible = value;
	}

	public boolean isAnalyticsLabelsOptVisible() {
		return this.analyticsLabelsOptVisible;
	}

	public void setAnalyticsSortOptVisible(boolean value) {
		this.analyticsSortOptVisible = value;
	}

	public boolean isAnalyticsSortOptVisible() {
		return this.analyticsSortOptVisible;
	}

	public void setAnalyticsNameOpt(boolean value) {
		this.analyticsNameOpt = value;
	}

	public boolean getAnalyticsNameOpt() {
		return this.analyticsNameOpt;
	}

	public void setAnalyticsShowLciOpt(boolean value) {
		this.analyticsShowLciOpt = value;
	}

	public boolean getAnalyticsShowLciOpt() {
		return this.analyticsShowLciOpt;
	}

	public void setAnalyticsLabelsOpt(String value) {
		this.analyticsLabelsOpt = value;
	}

	public String getAnalyticsLabelsOpt() {
		return this.analyticsLabelsOpt;
	}

	public void setCurSortGroupId(String value) {
		this.curSortGroupId = value;
	}

	public String getCurSortGroupId() {
		return this.curSortGroupId;
	}

	public void setAnalyticsSortIdOpt(String value) {
		this.analyticsSortIdOpt = value;
	}

	public String getAnalyticsSortIdOpt() {
		return this.analyticsSortIdOpt;
	}

	public List<ReportOptionsObj> getAnalytics16bLabelsList() {
		return this.analytics16bLabelsList;
	}

	public void setAnalytics16bLabelsList(List<ReportOptionsObj> value) {
		this.analytics16bLabelsList = value;
	}

	public List<ReportOptionsObj> getAnalyticsSortGroups() {
		return this.analyticsSortGroups;
	}

	public void setAnalyticsSortGroups(List<ReportOptionsObj> value) {
		this.analyticsSortGroups = value;
	}

	public List<ReportOptionsObj> getAnalyticsSortItems() {
		return this.analyticsSortItems;
	}

	public void setAnalyticsSortItems(List<ReportOptionsObj> value) {
		this.analyticsSortItems = value;
	}

	public Map<String, ArrayList<ReportOptionsObj>> getSortOptions() {
		return this.sortOptions;
	}

	public void setSortOptions(Map<String, ArrayList<ReportOptionsObj>> value) {
		this.sortOptions = value;
	}

	public int getTargetLevelOverrideIndex() {
		return targetLevelOverrideIndex;
	}

	public void setTargetLevelOverrideIndex(int targetLevelOverrideIndex) {
		this.targetLevelOverrideIndex = targetLevelOverrideIndex;
	}

	@Override
	public int compareTo(ReportTypeObj o) {
		return Comparators.NAME.compare(this, o);
	}

	public Integer getTargetLevelGroupId() {
		return targetLevelGroupId;
	}

	public void setTargetLevelGroupId(Integer targetLevelGroupId) {
		this.targetLevelGroupId = targetLevelGroupId;
	}

	public boolean isGroupReport() {
		return groupReport;
	}

	public void setGroupReport(boolean groupReport) {
		this.groupReport = groupReport;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public static class Comparators {

		public static Comparator<ReportTypeObj> NAME = new Comparator<ReportTypeObj>() {
			@Override
			public int compare(ReportTypeObj o1, ReportTypeObj o2) {
				return new CompareToBuilder().append(o1.getName(), o2.getName()).toComparison();
			}
		};
	}
}