package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NonBillableTypeObj implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private int id;

	public static List<NonBillableTypeObj> nonBillableTypes = new ArrayList<NonBillableTypeObj>();

	static {
		nonBillableTypes.add(new NonBillableTypeObj(1, "Business Development"));
		nonBillableTypes.add(new NonBillableTypeObj(2, "Pilot"));
		nonBillableTypes.add(new NonBillableTypeObj(3, "Internal Use"));
		nonBillableTypes.add(new NonBillableTypeObj(4, "Certification"));
	}

	public NonBillableTypeObj() {
	}

	public NonBillableTypeObj(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static String getTypeName(int id) {
		String typeName = "";
		for (NonBillableTypeObj type : nonBillableTypes) {
			if (type.id == id) {
				typeName = type.getName();
				break;
			}
		}
		return typeName;
	}

}
