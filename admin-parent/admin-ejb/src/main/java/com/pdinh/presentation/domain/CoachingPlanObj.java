package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Date;

public class CoachingPlanObj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int coachingPlanId;
	private int projectId;
	private int participantId;
	private int coachId;
	private String coachFirstname;
	private String coachLastname;
	private Date startDate;
	private Date endDate;
	private Date lastModifiedDate;
	private int coachingPlanStatusTypeId;
	private String coachingPlanDocument;
	private String typeText;
	private int targetLevelTypeId;
	private String targetLevelTypeLabel;
	private int timeElapsed;
	private String completionStatusLabel;

	public CoachingPlanObj() {

	}

	public int getCoachingPlanId() {
		return coachingPlanId;
	}

	public void setCoachingPlanId(int coachingPlanId) {
		this.coachingPlanId = coachingPlanId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getCoachingPlanStatusTypeId() {
		return coachingPlanStatusTypeId;
	}

	public void setCoachingPlanStatusTypeId(int coachingPlanStatusTypeId) {
		this.coachingPlanStatusTypeId = coachingPlanStatusTypeId;
	}

	public String getCoachingPlanDocument() {
		return coachingPlanDocument;
	}

	public void setCoachingPlanDocument(String coachingPlanDocument) {
		this.coachingPlanDocument = coachingPlanDocument;
	}

	public String getTypeText() {
		return typeText;
	}

	public void setTypeText(String typeText) {
		this.typeText = typeText;
	}

	public int getTargetLevelTypeId() {
		return targetLevelTypeId;
	}

	public void setTargetLevelTypeId(int targetLevelTypeId) {
		this.targetLevelTypeId = targetLevelTypeId;
	}

	public String getTargetLevelTypeLabel() {
		return targetLevelTypeLabel;
	}

	public int getTimeElapsed() {
		return timeElapsed;
	}

	public void setTimeElapsed(int timeElapsed) {
		this.timeElapsed = timeElapsed;
	}

	public String getCompletionStatusLabel() {
		return completionStatusLabel;
	}

	public void setCompletionStatusLabel(String completionStatus) {
		this.completionStatusLabel = completionStatus;
	}

	public void setTargetLevelTypeLabel(String targetLevelTypeName) {
		this.targetLevelTypeLabel = targetLevelTypeName;
	}

	public int getParticipantId() {
		return participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public int getCoachId() {
		return coachId;
	}

	public void setCoachId(int coachId) {
		this.coachId = coachId;
	}

	public String getCoachFirstname() {
		return coachFirstname;
	}

	public void setCoachFirstname(String coachFirstname) {
		this.coachFirstname = coachFirstname;
	}

	public String getCoachLastname() {
		return coachLastname;
	}

	public void setCoachLastname(String coachLastname) {
		this.coachLastname = coachLastname;
	}
}
