package com.pdinh.presentation.domain;

import java.io.Serializable;

public class ClientObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int clientId;
    private String description;
    private String name;
    private String lang;
    private int assessmentProductCount;
    private int projectCount;
    private int participantCount;
    
    public ClientObj(int clientId, String description, String name, String lang, int assessmentProductCount, int projectCount, int participantCount) {
        this.clientId = clientId;
        this.description = description;
        this.name = name;
        this.lang = lang;
        this.assessmentProductCount = assessmentProductCount;
        this.projectCount = projectCount;
        this.participantCount = participantCount;
    }
    
    public ClientObj() {}
    
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
    public int getClientId() {
        return clientId;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }
    public int getProjectCount() {
        return projectCount;
    }
    public void setParticipantCount(int participantCount) {
        this.participantCount = participantCount;
    }
    public int getParticipantCount() {
        return participantCount;
    }

	public void setAssessmentProductCount(int assessmentProductCount) {
		this.assessmentProductCount = assessmentProductCount;
	}

	public int getAssessmentProductCount() {
		return assessmentProductCount;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getLang() {
		return lang;
	}

}
