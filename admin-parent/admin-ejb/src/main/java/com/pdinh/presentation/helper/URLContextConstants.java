package com.pdinh.presentation.helper;

public class URLContextConstants {

	public static final String REPORT_REST_CONTEXT = "/reportrest/jaxrs";
	public static final String OXCART_WEB_CONTEXT = "/pdi-web";
	public static final String TINCAN_ADMIN_REST_CONTEXT = "/tincanadminrest/jaxrs";
	public static final String ADMIN_REST_CONTEXT = "/adminrest/jaxrs";
	public static final String IMPORTPOLLER_REST_CONTEXT = "/importpollerrest/jaxrs";
}
