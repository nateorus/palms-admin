package com.pdinh.presentation.domain;

import java.io.Serializable;

public class LanguageObj implements Serializable {

	private static final long serialVersionUID = 1L;
	private String code;
	private int id;
	private String name;
	private String transName;
	private Short activeAssessment;
	private Short activeDevelopment;
	private int listOrder;
	private String transNameUnicode;

	public LanguageObj() {
		super();
	}

	public LanguageObj(String code, int id, String name, String transName) {
		super();
		this.code = code;
		this.id = id;
		this.name = name;
		this.transName = transName;
	}

	public LanguageObj(String code, String transName) {
		super();
		this.code = code;
		this.transName = transName;
	}

	public LanguageObj(String code, String name, String transName) {
		super();
		this.code = code;
		this.name = name;
		this.transName = transName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}

	public Short getActiveAssessment() {
		return activeAssessment;
	}

	public void setActiveAssessment(Short activeAssessment) {
		this.activeAssessment = activeAssessment;
	}

	public Short getActiveDevelopment() {
		return activeDevelopment;
	}

	public void setActiveDevelopment(Short activeDevelopment) {
		this.activeDevelopment = activeDevelopment;
	}

	public int getListOrder() {
		return listOrder;
	}

	public void setListOrder(int listOrder) {
		this.listOrder = listOrder;
	}

	public String getTransNameUnicode() {
		return transNameUnicode;
	}

	public void setTransNameUnicode(String transNameUnicode) {
		this.transNameUnicode = transNameUnicode;
	}

}
