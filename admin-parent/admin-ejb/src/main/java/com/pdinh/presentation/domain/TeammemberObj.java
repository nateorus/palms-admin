package com.pdinh.presentation.domain;

import java.io.Serializable;

public class TeammemberObj implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	private String roleName;
	private String firstName;
	private String lastName;
	private String email;
	private String username;
	
	public TeammemberObj () {}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLongName() {
		return this.firstName + " " + this.lastName + "(" + this.email + ")";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
