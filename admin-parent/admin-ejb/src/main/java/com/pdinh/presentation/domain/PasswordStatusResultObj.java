package com.pdinh.presentation.domain;

public class PasswordStatusResultObj extends ResultObj {

	boolean expired = false;
	boolean locked = false;
	public static final String STATUS_ACCOUNT_LOCKED = "ACCOUNT_LOCKED";
	public static final String STATUS_PASSWORD_EXPIRED = "PASSWORD_EXPIRED";
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	
}
