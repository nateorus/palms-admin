package com.pdinh.presentation.domain;

import java.io.Serializable;

public class GenericObj implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String code;
	private String value;
	private String description;

	public GenericObj() {
	}

	public GenericObj(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public GenericObj(int id, String name, String code) {
		this.id = id;
		this.name = name;
		this.code = code;
	}

	public GenericObj(int id, String name, String code, String description) {
		this.id = id;
		this.name = name;
		this.code = code;
		this.description = description;
	}

	public GenericObj(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
