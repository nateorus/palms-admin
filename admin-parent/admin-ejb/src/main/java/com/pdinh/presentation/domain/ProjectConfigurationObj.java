package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProjectConfigurationObj implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private int projectConfigurationId;
	
	public static List<ProjectConfigurationObj> configsDb = new ArrayList<ProjectConfigurationObj>();
	static {
		configsDb.add(new ProjectConfigurationObj(1,"FC01 - MLL People Leader"));
		configsDb.add(new ProjectConfigurationObj(2,"FC02 - MLL Matrix Leader"));
		configsDb.add(new ProjectConfigurationObj(3,"FC03 - MLL Operational Leader"));
		configsDb.add(new ProjectConfigurationObj(4,"FC04 - MLL Strategic Leader"));
		configsDb.add(new ProjectConfigurationObj(5,"FC05 - MLL Legacy"));
		configsDb.add(new ProjectConfigurationObj(6,"FC06 - BUL"));
		configsDb.add(new ProjectConfigurationObj(7,"FC07 - BUL"));
		configsDb.add(new ProjectConfigurationObj(8,"FC08 - BUL"));
		configsDb.add(new ProjectConfigurationObj(9,"FC09 - BUL"));
		configsDb.add(new ProjectConfigurationObj(10,"FC10 - BUL"));
		configsDb.add(new ProjectConfigurationObj(11,"FC11 - BUL"));
		configsDb.add(new ProjectConfigurationObj(12,"FC12 - BUL"));
	}
	
	public ProjectConfigurationObj() {
		
	}
	
	public ProjectConfigurationObj(int id, String name) {
		this.setProjectConfigurationId(id);
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getProjectConfigurationId() {
		return projectConfigurationId;
	}

	public void setProjectConfigurationId(int projectConfigurationId) {
		this.projectConfigurationId = projectConfigurationId;
	}

}
