package com.pdinh.presentation.domain;

import java.io.Serializable;

import com.pdinh.persistence.ms.entity.OfficeLocation;

public class OfficeLocationObj implements Serializable {
	private static final long serialVersionUID = -1579538729931464351L;

	private Integer officeLocationId;
	private String location;

	public OfficeLocationObj() {
		super();
		location = "";
	}

	public OfficeLocationObj(Integer id2, String location2) {
		officeLocationId = id2;
		location = location2;
	}

	public OfficeLocationObj(OfficeLocation o) {
		officeLocationId = o.getId();
		location = o.getLocation();
	}

	public Integer getOfficeLocationId() {
		return this.officeLocationId;
	}

	public void setOfficeLocationId(Integer id) {
		this.officeLocationId = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}