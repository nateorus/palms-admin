package com.pdinh.presentation.domain;


public class ProjectDocumentObj extends DocumentObj {
		
	private static final long serialVersionUID = 1L;

	public ProjectDocumentObj() {
	}

	public ProjectDocumentObj(int documentId, String description,
		boolean isIntegrationGrid, int type, String typeDisplay,
		FileAttachmentObj fileAttachment, boolean newDocument) {
		
		super(documentId, description, isIntegrationGrid, type, typeDisplay,
			fileAttachment, newDocument);		
	}

}
