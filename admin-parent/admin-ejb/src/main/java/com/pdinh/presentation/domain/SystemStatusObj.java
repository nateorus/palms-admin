package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SystemStatusObj implements Serializable {

	private static final long serialVersionUID = 2507742369518153159L;
	public static final String APPLICATION_REPORT_CENTER = "REPORT_CENTER";
	public static final String APPLICATION_BULK_UPLOAD = "BULK_UPLOAD";

	private boolean running;
	private String systemComponentName;
	private Date statusChangeTime;
	private String application;
	private Map<String, Map<String, String>> details;
	private boolean detailsVisible;

	public SystemStatusObj() {
	}


	public SystemStatusObj(boolean running, String systemComponentName, Date statusChangeTime, String application) {
		super();
		this.running = running;
		this.systemComponentName = systemComponentName;
		this.statusChangeTime = statusChangeTime;
		this.application = application;
	}

	public SystemStatusObj(boolean running, String systemComponentName, Date statusChangeTime, String application,
			Map<String, Map<String, String>> details) {
		super();
		this.running = running;
		this.systemComponentName = systemComponentName;
		this.statusChangeTime = statusChangeTime;
		this.application = application;
		this.details = details;
		this.detailsVisible = false;
	}

	public String getSystemComponentName() {
		return systemComponentName;
	}

	public void setSystemComponentName(String systemComponentName) {
		this.systemComponentName = systemComponentName;
	}

	public Date getStatusChangeTime() {
		return statusChangeTime;
	}

	public void setStatusChangeTime(Date statusChangeTime) {
		this.statusChangeTime = statusChangeTime;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getApplicationLabel() {
		String label = "";
		if (application.equals(APPLICATION_BULK_UPLOAD)) {
			label = "Bulk Upload";
		} else if (application.equals(APPLICATION_REPORT_CENTER)) {
			label = "Report Center";
		}
		return label;
	}

	public boolean isRunning() {
		return running;
	}


	public void setRunning(boolean running) {
		this.running = running;
	}

	public static boolean convertStatus(String status) {
		if (status.equalsIgnoreCase("OK") || status.equalsIgnoreCase("ON")) {
			return true;
		}
		return false;
	}

	public List<ArrayList<GenericObj>> getStatusDetails() {
		List<ArrayList<GenericObj>> records = new ArrayList<ArrayList<GenericObj>>();
		ArrayList<GenericObj> record;

		// System.out.println("systemComponentName = " + systemComponentName);
		// System.out.println("getDetails() = " + getDetails());
		// System.out.println("===============================================================");

		if (getDetails() != null) {
			for (Map.Entry<String, Map<String, String>> entry1 : getDetails().entrySet()) {
				record = new ArrayList<GenericObj>();
				for (Map.Entry<String, String> entry2 : entry1.getValue().entrySet()) {
					record.add(new GenericObj(entry2.getKey(), entry2.getValue()));
				}
				records.add(record);
			}
		}

		return records;
	}

	public Map<String, Map<String, String>> getDetails() {
		return details;
	}


	public void setDetails(Map<String, Map<String, String>> details) {
		this.details = details;
	}


	public boolean isDetailsVisible() {
		return detailsVisible;
	}


	public void setDetailsVisible(boolean detailsVisible) {
		this.detailsVisible = detailsVisible;
	}


}
