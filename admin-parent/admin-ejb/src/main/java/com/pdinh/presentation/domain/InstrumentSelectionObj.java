package com.pdinh.presentation.domain;

public class InstrumentSelectionObj {
	private Integer id;
	private String code;
	private String group;
	private String name;
	private boolean enabled;
	private boolean selected;
	private boolean visible;
	private int type;
	private String selectionType;
	private String objectName;
	private int downloadReportTypeId;

	public InstrumentSelectionObj() {
	}

	public InstrumentSelectionObj(Integer id, String code, String name, boolean enabled, boolean selected,
			boolean visible) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.enabled = enabled;
		this.selected = selected;
		this.visible = visible;
	}

	public InstrumentSelectionObj(Integer id, String code, String group, String name, boolean enabled,
			boolean selected, boolean visible) {
		this.id = id;
		this.code = code;
		this.group = group;
		this.name = name;
		this.enabled = enabled;
		this.selected = selected;
		this.visible = visible;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getGroup() {
		return group;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(String selectionType) {
		this.selectionType = selectionType;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public int getDownloadReportTypeId() {
		return downloadReportTypeId;
	}

	public void setDownloadReportTypeId(int downloadReportTypeId) {
		this.downloadReportTypeId = downloadReportTypeId;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
