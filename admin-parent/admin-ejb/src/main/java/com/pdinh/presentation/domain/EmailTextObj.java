package com.pdinh.presentation.domain;

import java.io.Serializable;

public class EmailTextObj implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String subject;
	private String emailText;
	private String langCode;
	
	public EmailTextObj() {}
	
	public EmailTextObj(Integer id, String subject, String emailText, String langCode) {
		this.id = id;
		this.subject = subject;
		this.emailText = emailText;
		this.langCode = langCode;
	}
	public EmailTextObj( String subject, String emailText, String langCode) {
		this.subject = subject;
		this.emailText = emailText;
		this.langCode = langCode;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getEmailText() {
		return emailText;
	}
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}
	public String getLangCode() {
		return langCode;
	}
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	
	
}
