package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pdinh.persistence.ms.entity.ProjectReport;
import com.pdinh.persistence.ms.entity.ProjectReportContent;
import com.pdinh.persistence.ms.entity.ReportType;

public class ProjectReportObj implements Comparable<ProjectReportObj>, Serializable {
	
	private static final long serialVersionUID = 1L;
	private int projectReportId;
	private int projectTypeId;
	private ReportType reportType;
	private boolean allowAutogenerate;
	private boolean allowUserDownload;
	private boolean allowAutogenerateEnabled;
	private boolean allowUserDownloadEnabled;
	private boolean allowAutogenerateVisible;
	private boolean allowUserDownloadVisible;
	private boolean selected;
	private int sequenceOrder;
	private String selectionType;
	private String objectName;
	private String code;
	private int contentRule;
	//private List<Integer> courseIds;
	private Map<Integer, Boolean[]> courseIdMap = new HashMap<Integer, Boolean[]>();
	
	public ProjectReportObj(){
		
	}
	public ProjectReportObj(ProjectReport pReport){
		this.setProjectReportId(pReport.getId());
		
		this.setProjectTypeId(pReport.getProjectTypeId());
		this.setReportType(pReport.getReportType());
		this.setAllowAutogenerate(pReport.isAllowAutogenerate());
		this.setAllowUserDownload(pReport.isAllowUserDownload());
		this.setAllowAutogenerateEnabled(pReport.isAllowAutogenerateEnabled());
		this.setAllowUserDownloadEnabled(pReport.isAllowUserDownloadEnabled());
		this.setAllowAutogenerateVisible(pReport.isAllowAutogenerateVisible());
		this.setAllowUserDownloadVisible(pReport.isAllowUserDownloadVisible());
		this.setSelected(pReport.isSelected());
		this.setSequenceOrder(pReport.getSequenceOrder());
		this.setSelectionType(pReport.getSelectionType());
		this.setObjectName(pReport.getObjectName());
		this.setCode(pReport.getCode());
		this.setContentRule(pReport.getContentRule());
		//this.setCourseIds(new ArrayList<Integer>());
		this.setCourseIdMap(new HashMap<Integer, Boolean[]>());
		for (ProjectReportContent c : pReport.getProjectReportContent()){
			
			this.getCourseIdMap().put(c.getCourse(), new Boolean[]{c.isDisplayDownloadLink(), c.isRequired()});
		}
	}
	public ProjectReport createEntityFromObj(){
		ProjectReport pReport = new ProjectReport();
		pReport.setTemplate(false);
		pReport.setProjectTypeId(this.getProjectTypeId());
		pReport.setReportType(this.reportType);
		pReport.setAllowAutogenerate(this.allowAutogenerate);
		pReport.setAllowAutogenerateEnabled(this.allowAutogenerateEnabled);
		pReport.setAllowUserDownload(this.allowUserDownload);
		pReport.setAllowUserDownloadEnabled(this.allowUserDownloadEnabled);
		pReport.setAllowAutogenerateVisible(this.allowAutogenerateVisible);
		pReport.setAllowUserDownloadVisible(this.allowUserDownloadVisible);
		pReport.setSelected(this.selected);
		pReport.setSequenceOrder(this.sequenceOrder);
		pReport.setSelectionType(this.selectionType);
		pReport.setObjectName(this.objectName);
		pReport.setCode(this.code);
		pReport.setContentRule(this.contentRule);
		pReport.setProjectReportContent(new ArrayList<ProjectReportContent>());
		
		for (Object i : this.getCourseIdMap().keySet()){
			
			ProjectReportContent prc = new ProjectReportContent();
			prc.setCourse((Integer)i);
			prc.setProjectReport(pReport);
			prc.setDisplayDownloadLink(this.getCourseIdMap().get(i)[0]);
			prc.setRequired(this.getCourseIdMap().get(i)[1]);
			pReport.getProjectReportContent().add(prc);
		}
		return pReport;
	}
	public ProjectReport updateEntityFromObj (ProjectReport pReport){
		pReport.setTemplate(false);
		pReport.setProjectTypeId(this.getProjectTypeId());
		pReport.setReportType(this.reportType);
		pReport.setAllowAutogenerate(this.allowAutogenerate);
		pReport.setAllowAutogenerateEnabled(this.allowAutogenerateEnabled);
		pReport.setAllowUserDownload(this.allowUserDownload);
		pReport.setAllowUserDownloadEnabled(this.allowUserDownloadEnabled);
		pReport.setAllowAutogenerateVisible(this.allowAutogenerateVisible);
		pReport.setAllowUserDownloadVisible(this.allowUserDownloadVisible);
		pReport.setSelected(this.selected);
		pReport.setSequenceOrder(this.sequenceOrder);
		pReport.setSelectionType(this.selectionType);
		pReport.setObjectName(this.objectName);
		pReport.setCode(this.code);
		pReport.setContentRule(this.contentRule);
		pReport.setProjectReportContent(new ArrayList<ProjectReportContent>());
		for (Object i : this.getCourseIdMap().keySet()){
			
			ProjectReportContent prc = new ProjectReportContent();
			prc.setCourse((Integer)i);
			prc.setProjectReport(pReport);
			prc.setDisplayDownloadLink(this.getCourseIdMap().get(i)[0]);
			prc.setRequired(this.getCourseIdMap().get(i)[1]);
			pReport.getProjectReportContent().add(prc);
		}
		/*
		for (Integer i : this.getCourseIds()){
			ProjectReportContent prc = new ProjectReportContent();
			prc.setCourse(i);
			prc.setProjectReport(pReport);
			pReport.getProjectReportContent().add(prc);
		}
		*/
		return pReport;
	}
	public int getProjectReportId() {
		return projectReportId;
	}
	public void setProjectReportId(int projectReportId) {
		this.projectReportId = projectReportId;
	}
	public int getProjectTypeId() {
		return projectTypeId;
	}
	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	public ReportType getReportType() {
		return reportType;
	}
	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}
	public boolean isAllowAutogenerate() {
		return allowAutogenerate;
	}
	public void setAllowAutogenerate(boolean allowAutogenerate) {
		this.allowAutogenerate = allowAutogenerate;
	}
	public boolean isAllowUserDownload() {
		return allowUserDownload;
	}
	public void setAllowUserDownload(boolean allowUserDownload) {
		this.allowUserDownload = allowUserDownload;
	}
	public boolean isAllowAutogenerateEnabled() {
		return allowAutogenerateEnabled;
	}
	public void setAllowAutogenerateEnabled(boolean allowAutogenerateEnabled) {
		this.allowAutogenerateEnabled = allowAutogenerateEnabled;
	}
	public boolean isAllowUserDownloadEnabled() {
		return allowUserDownloadEnabled;
	}
	public void setAllowUserDownloadEnabled(boolean allowUserDownloadEnabled) {
		this.allowUserDownloadEnabled = allowUserDownloadEnabled;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public int getSequenceOrder() {
		return sequenceOrder;
	}
	public void setSequenceOrder(int sequenceOrder) {
		this.sequenceOrder = sequenceOrder;
	}
	public String getSelectionType() {
		return selectionType;
	}
	public void setSelectionType(String selectionType) {
		this.selectionType = selectionType;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
/*
	public List<Integer> getCourseIds() {
		return courseIds;
	}
	public void setCourseIds(List<Integer> courseIds) {
		this.courseIds = courseIds;
	}
	*/
	public int getContentRule() {
		return contentRule;
	}
	public void setContentRule(int contentRule) {
		this.contentRule = contentRule;
	}
	@Override
	public int compareTo(ProjectReportObj o) {
		if (this.sequenceOrder > o.sequenceOrder){
			return 1;
		}else if (this.sequenceOrder < o.sequenceOrder){
			return -1;
		}
		return 0;
	}
	public boolean isAllowAutogenerateVisible() {
		return allowAutogenerateVisible;
	}
	public void setAllowAutogenerateVisible(boolean allowAutogenerateVisible) {
		this.allowAutogenerateVisible = allowAutogenerateVisible;
	}
	public boolean isAllowUserDownloadVisible() {
		return allowUserDownloadVisible;
	}
	public void setAllowUserDownloadVisible(boolean allowUserDownloadVisible) {
		this.allowUserDownloadVisible = allowUserDownloadVisible;
	}
	public Map<Integer, Boolean[]> getCourseIdMap() {
		return courseIdMap;
	}

	public void setCourseIdMap(Map<Integer, Boolean[]> courseIdMap) {
		this.courseIdMap = courseIdMap;
	}
}
