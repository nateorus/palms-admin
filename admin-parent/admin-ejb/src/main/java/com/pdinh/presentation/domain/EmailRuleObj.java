package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.List;

public class EmailRuleObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int id;
    private String name;
    private String active;
    private String language;
    private Boolean completed;
    private EmailTemplateObj template;
    private List<EmailRuleScheduleObj> schedules; 
    private Boolean error;
    
    public EmailRuleObj() {}

    public EmailRuleObj(int id, String name, String active, EmailTemplateObj template, 
    		String language, Boolean completed, Boolean error, List<EmailRuleScheduleObj> schedules) {
    	this.id = id;
    	this.name = name;
    	this.active = active;
    	this.template = template;
    	this.language = language;
    	this.completed = completed;
    	this.error = error;
    	this.schedules = schedules;
    }
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EmailTemplateObj getTemplate() {
		return template;
	}

	public void setTemplate(EmailTemplateObj template) {
		this.template = template;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Boolean getError() {
		return error;
	}

	public void setError(Boolean errors) {
		this.error = errors;
	}

	public List<EmailRuleScheduleObj> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<EmailRuleScheduleObj> schedules) {
		this.schedules = schedules;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}


}
