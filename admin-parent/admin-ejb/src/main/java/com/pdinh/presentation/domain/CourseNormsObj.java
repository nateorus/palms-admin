package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CourseNormsObj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String abbv;
	private List<NormGroupObj> normGroups = new ArrayList<NormGroupObj>();
	private String courseName;

	/*
	 * Util functions
	 */
	public NormGroupObj getOnlyOption(String type) {
		List<NormGroupObj> norms = getNormGroupByType(type);
		if (norms.size() == 1) {
			return norms.get(0);
		} else
			return null;
	}

	public NormGroupObj getSelectedNormByType(String type) {
		List<NormGroupObj> norms = getNormGroupByType(type);
		for (NormGroupObj ng : norms) {
			if (ng.isSelected())
				return ng;
		}
		return null;
	}

	public List<NormGroupObj> getNormGroupByType(String type) {
		List<NormGroupObj> result = new ArrayList<NormGroupObj>();
		if (normGroups != null && normGroups.size() > 0) {
			for (NormGroupObj normGroup : normGroups) {
				if (normGroup.getType().equals(type)) {
					result.add(normGroup);
				}
			}

		}
		return result;
	}

	public boolean isSpecPopNormListEmpty() {
		return getNormGroupByType(NormGroupObj.SPEC_POP).size() == 0;
	}

	/*public NormGroupTypeObj getNormGroupTypeByType(String type) {
		if (normGroupTypes != null && normGroupTypes.size() > 0) {
			for (NormGroupTypeObj ngt : normGroupTypes) {
				if (ngt.getType().equals(type)) {
					return ngt;
				}
			}
		}
		return null;
	}
	*/
	/*
	 * setter/getter
	 */
	public String getAbbv() {
		return abbv;
	}

	public void setAbbv(String abbv) {
		this.abbv = abbv;
	}

	public List<NormGroupObj> getNormGroups() {
		return normGroups;
	}

	public void setNormGroups(List<NormGroupObj> normGroups) {
		this.normGroups = normGroups;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String couseName) {
		this.courseName = couseName;
	}

	@Override
	public String toString() {
		return "CourseNormsObj [abbv=" + abbv + ", normGroups=" + normGroups + ", courseName=" + courseName + "]";
	}

}
