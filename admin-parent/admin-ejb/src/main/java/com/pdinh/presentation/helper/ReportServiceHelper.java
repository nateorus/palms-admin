package com.pdinh.presentation.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.singleton.ReportingSingleton;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.exception.RCMGenerationException;
import com.pdinh.exception.ServerUnavailableException;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.TargetLevelType;
import com.pdinh.presentation.domain.ParticipantObj;
import com.pdinh.presentation.domain.ReportSourceTypeObj;
import com.pdinh.presentation.domain.ReportTypeObj;
import com.pdinh.reporting.generation.CoachingPlanDetailedExtract;
import com.pdinh.reporting.generation.Override;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.ReportParticipant;
import com.pdinh.reporting.generation.ReportRequest;
import com.pdinh.reporting.generation.Reports;

@LocalBean
@Stateless
public class ReportServiceHelper {

	final static Logger logger = LoggerFactory.getLogger(ReportServiceHelper.class);

	@EJB
	private ProjectDao projectDao;

	@EJB
	private ReportingSingleton reportingSingleton;

	private Properties configProperties;

	private List<Map<String, String>> reportSourceUrls;

	public static Participants createParticipantsObject(List<ParticipantObj> participantObjs) {
		Participants participants = new Participants();

		for (ParticipantObj participantObj : participantObjs) {
			ReportParticipant rp = new ReportParticipant(participantObj.getUserId().toString(), participantObj
					.getProject().getProjectId() + "", participantObj.getProject().getName());
			if (participantObj.getCourseVersions().size() > 0){
				rp.setCourseVersionMap(participantObj.courseVersionsToString());
			}
			rp.setLangCode(participantObj.getReportLanguageId());
			rp.setDefaultProjectTargetLevelIndex(participantObj.getProject().getTargetLevelIndex());
			participants.getParticipants().add(rp);
		}

		return participants;
	}

	/**
	 * gePalmsUrls runs through the list of selected PALMS reports and generates
	 * the appropriate URLS and parameter data for each of them.
	 * 
	 * @param ppts
	 *            - A List of report participant objects
	 * @param rstai
	 *            - A holder for the reports requested for a particular data
	 *            source type
	 * @param reportSourceUrls
	 *            - The generated parameter lists (including the URL)
	 */
	public void genPalmsUrls(Participants ppts, ReportSourceTypeObj rstai, String projectId) throws PalmsException {
		String xmlParamVal = null;
		String xmlParamName = null;
		// System.out.println("HEY KEITH AHHEWRE@LKR$J@ = " + projectId);
		// loop through the requested reports in the PALMS type, generating the
		// appropriate values in the urlMap
		Map<String, String> urlMap = null;
		for (ReportTypeObj rto : rstai.getReportTypes()) {
			urlMap = new HashMap<String, String>();
			if (rto.getCode().equals(ReportType.COACHING_PLAN_SUMMARY)) {
				// urlMap = new HashMap<String, String>();
				xmlParamVal = null;
				xmlParamName = null;

				String ppParamVal = generationParticipantsXml(ppts);
				

				String url = configProperties.getProperty("reportserverBaseUrlCoaching") + "/CoachingSummaryRcmServlet";
				String ppParamName = "participants";
				Map<String, String> params = new HashMap<String, String>();
				params.put(ppParamName, ppParamVal);
				String rcm = "";
				try {
					logger.debug("Getting RCM from URL: {}", url);
					rcm = getRcm(url, params);
				} catch (PalmsException e) {
					// logger.error("Caught ServerUnavailableException.  Unable to generate Coaching Plan Summary RCM");
					// throw new ServerUnavailableException(url,
					// "The Coaching Plan Server was unavailable.  Cannot create RCM.");
					throw e;

				}
				xmlParamVal = rcm;
				xmlParamName = rstai.getReportSourceType().getXmlParamName();
				urlMap.put(xmlParamName, xmlParamVal);
				urlMap.put("url", configProperties.getProperty(rstai.getReportSourceType().getUrlRef())
						+ "/GenerateReportFromRcm");
				urlMap.put("type", ReportType.COACHING_PLAN_SUMMARY);
				reportSourceUrls.add(urlMap);
			} else if (ReportType.LVA_MLL_REPORTS_TYPES.contains(rto.getCode())) {

				// This works because these are not group reports. We'll have to
				// pass participants in a different fashion for that
				String url = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReport";
				// This works because the code is defined the same as the type
				String type = rto.getCode();

				// loop through the ppts, making the urls as we go
				for (ReportParticipant ppt : ppts.getParticipants()) {
					// New urlMap needed so we get a new one per ppt
					urlMap = new HashMap<String, String>();
					urlMap.put("url", url);
					urlMap.put("pid", ppt.getParticipantId());
					urlMap.put("type", type);
					// Phase ID of 0 is a cue to the report generator to select
					// the "highest" phase for reporting
					urlMap.put("phase", "0");
					reportSourceUrls.add(urlMap);
				} // End of ppt for loop
			} else if (rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_SUMMARY)
					|| rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_COACHING)
					|| rto.getCode().equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
				String rptUrl = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReportFromRcm";
				String type = rto.getCode();
				String rcm;
				for (ReportParticipant ppt : ppts.getParticipants()) {
					// New urlMap needed so we get a new one per ppt
					urlMap = new HashMap<String, String>();
					rcm = "";
					String langCode = (ppt.getLangCode() == null) ? rto.getLanguageCode() : ppt.getLangCode();
					String pptId = ppt.getParticipantId();

					// get the RCM
					String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") + "/ViaEdgeRcmServlet";
					Map<String, String> params = new HashMap<String, String>();
					params.put("pptId", pptId);
					params.put("langCode", langCode);
					params.put("type", rto.getCode());
					try {
						rcm = getRcm(rcmGenUrl, params);
					} catch (PalmsException e) {
						// logger.error("Caught ServerUnavailableException.  Unable to generate viaEdge Individual RCM for ppt {}",
						// pptId);
						// throw new ServerUnavailableException(rcmGenUrl,
						// "The Report Server was unavailable.  Cannot create RCM.");
						throw e;
					}

					// Need to do the rpt URL gen now
					urlMap.put("url", rptUrl);
					urlMap.put("type", type);
					urlMap.put("pid", pptId);
					urlMap.put("rcm", rcm);
					reportSourceUrls.add(urlMap);
				} // End of ppt loop
			} else if (rto.getCode().equals(ReportType.VIAEDGE_GROUP_FEEDBACK)
					|| rto.getCode().equals(ReportType.VIAEDGE_GROUP_EXTRACT)) {
				urlMap = new HashMap<String, String>();
				// Make an xml list of ppts
				String pptList = generationParticipantsXml(ppts);
				// System.out.println("RTO Code: {}" + rto.getCode());
				// Make the RCM
				String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") + "/ViaEdgeRcmServlet";
				Map<String, String> params = new HashMap<String, String>();
				params.put("pptList", pptList);
				params.put("langCode", rto.getLanguageCode());
				params.put("type", rto.getCode());
				params.put("pptInfoType", rto.getRptIdNameOpt());
				String rTitle = rto.getCode().equals(ReportType.VIAEDGE_GROUP_FEEDBACK) ? rto.getRptTitle() : "";
				// Escape title
				rTitle = StringEscapeUtils.escapeHtml4(rTitle);
				params.put("rptTitle", rTitle);

				String rcm = null;
				try {
					rcm = getRcm(rcmGenUrl, params);
				} catch (PalmsException e) {
					// logger.error("Caught ServerUnavailableException.  Unable to generate viaEdge Group feedback RCM");
					// throw new ServerUnavailableException(rcmGenUrl,
					// "The Report Server was unavailable.  Cannot create RCM.");
					throw e;
				}
				// System.out.println("GOT RCM: {}" + rcm);

				// Generate the report or extract
				String proj;
				if (projectId == null || projectId.length() == 0) {
					proj = "NA";
				} else {
					proj = "" + projectId;
				}

				String urlString = configProperties.getProperty("reportserverBaseUrl");
				urlString += rto.getCode().equals(ReportType.VIAEDGE_GROUP_EXTRACT) ? "/ViaEdgeExtractServlet"
						: "/GenerateReportFromRcm";
				urlMap.put("url", urlString);
				urlMap.put("type", rto.getCode());
				urlMap.put("proj", proj); // Added for report name
				urlMap.put("rcm", rcm);
				reportSourceUrls.add(urlMap);
			} else if (rto.getCode().equals(ReportType.KFP_NARRATIVE)
					|| rto.getCode().equals(ReportType.KFP_INDIVIDUAL) || rto.getCode().equals(ReportType.ALP_IND_V2)) {
				// System.out.println("HEY KEITH = " + projectId);
				String rptUrl = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReportFromRcm";
				String type = rto.getCode();
				String rcm;
				for (ReportParticipant ppt : ppts.getParticipants()) {
					// New urlMap needed so we get a new one per ppt
					urlMap = new HashMap<String, String>();
					rcm = "";
					String langCode = (ppt.getLangCode() == null || ppt.getLangCode() == "") ? rto.getLanguageCode()
							: ppt.getLangCode();
					String pptId = ppt.getParticipantId();

					Integer intproj;
					Integer inttgtRole;
					String tgtRole;
					String strtgtRole;

					// get the project ID by participant and NOT from
					// sessionBean
					// because there is a problem when using the clipboard
					// across multiple projects
					// The session bean simply stores the last projectID
					// System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@ppt.getProjectId() = "
					// + ppt.getProjectId());
					String strproj = (ppt.getProjectId() == null || ppt.getProjectId() == "") ? projectId : ppt
							.getProjectId();
					try {
						intproj = Integer.parseInt(strproj);
					} catch (NumberFormatException nfe) {
						throw new PalmsException(PalmsErrorCode.CANNOT_GEN_URL.getMessage(),
								PalmsErrorCode.CANNOT_GEN_URL.getCode(), "Invalid project id: " + strproj);
					}
					Project proj = projectDao.findById(intproj);

					// get the TargetRoleIndex for the Project default
					inttgtRole = proj.getTargetLevelType().getTargetLevelIndex();

					// get the TargetRoleIndex for the Override if one was
					// selected
					tgtRole = rto.getTargetLevelOverride();

					if (tgtRole != null) {
						TargetLevelType TLT = reportingSingleton.getTargetLevelTypeByCode(tgtRole);

						if (TLT != null)
							inttgtRole = TLT.getTargetLevelIndex();
					}

					strtgtRole = "" + inttgtRole;

					// get the RCM
					String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") + "/KfapRcmServlet";
					Map<String, String> params = new HashMap<String, String>();
					params.put("pid", pptId);
					params.put("langCode", langCode);
					params.put("type", rto.getCode());
					params.put("proj", strproj);
					params.put("targ", strtgtRole);
					try {
						rcm = getRcm(rcmGenUrl, params);
					} catch (PalmsException e) {
						// TODO: handle exception
						logger.warn("Unable to generate KFAP RCM for ppt " + pptId);
						throw e;
					}

					// Need to do the rpt URL gen now
					urlMap.put("url", rptUrl);
					urlMap.put("type", type);
					urlMap.put("pid", pptId);
					urlMap.put("rcm", rcm);
					reportSourceUrls.add(urlMap);
				}
			} else if (rto.getCode().equals(ReportType.KFP_SLATE)
					|| rto.getCode().equals(ReportType.ALP_TALENT_GRID_V2)) {
				String rptUrl = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReportFromRcm";
				urlMap = new HashMap<String, String>();
				// Make an xml list of ppts
				String pptList = generationParticipantsXml(ppts);
				logger.trace("Participants XML with course versions: {}", pptList);
				String strproj;
				Integer intproj;
				Integer inttgtRole = null;
				String tgtRole;
				String strtgtRole;

				if (projectId == null || projectId.length() == 0) {
					intproj = 0;
				} else {
					try {
						intproj = Integer.parseInt(projectId);
					} catch (NumberFormatException nfe) {
						intproj = 0;
					}
				}
				strproj = "" + intproj;

				Project proj = projectDao.findById(intproj);

				if (proj == null) {
					if (rto.getTargetLevelOverride() == null){
					
						throw new PalmsException(PalmsErrorCode.CANNOT_GEN_URL.getMessage(),
								PalmsErrorCode.CANNOT_GEN_URL.getCode(), "The project could not be resolved and no Target Level Override was provided."
										+ intproj);
					}
									
				}else{

					// get the TargetRoleIndex for the Project default
					inttgtRole = proj.getTargetLevelType().getTargetLevelIndex();
				}

				// get the TargetRoleIndex for the Override if one was
				// selected
				tgtRole = rto.getTargetLevelOverride();

				if (tgtRole != null) {
					TargetLevelType TLT = reportingSingleton.getTargetLevelTypeByCode(tgtRole);

					if (TLT != null)
						inttgtRole = TLT.getTargetLevelIndex();
				}

				strtgtRole = "" + inttgtRole;

				// Make the RCM
				String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") + "/KfapRcmServlet";
				Map<String, String> params = new HashMap<String, String>();
				params.put("pptList", pptList);
				params.put("langCode", rto.getLanguageCode());
				params.put("type", rto.getCode());
				//params.put("proj", strproj);
				params.put("targ", strtgtRole);
				String rTitle = rto.getRptTitle();
				// Escape title
				rTitle = StringEscapeUtils.escapeHtml4(rTitle);
				params.put("rptTitle", rTitle);

				String rcm = null;
				try {
					rcm = getRcm(rcmGenUrl, params);
				} catch (PalmsException e) {
					// TODO: handle exception
					logger.warn("Unable to generate KFAP Group RCM");
					throw e;
				}

				urlMap.put("url", rptUrl);
				urlMap.put("type", rto.getCode());
				urlMap.put("rcm", rcm);
				reportSourceUrls.add(urlMap);
			} else {
				// System.out.println("Error processing PALMS report.  Invalid report name = "
				// + rto.getName());
				throw new PalmsException("Invalid report name: " + rto.getCode(), PalmsErrorCode.CANNOT_GEN_URL.getCode());
			}
		} // End of for loop

		return;
	}

	/**
	 * genOxcartUrls - Create a request per ppt per report. Essentially this
	 * takes the aggregation of reports out of the Oxcart realm and brings it to
	 * a common spot (here).
	 * 
	 * @param ppts
	 *            - A List of the select ppts in ParticipantObj objects
	 * @param rstai
	 *            - A ReportSourceTypeArrayItem object containing information
	 *            about the type and a list of reports to be generated
	 * @param reportSourceUrls
	 *            - A list of Maps that contain information about the outgoing
	 *            report requests
	 */
	public void genOxcartUrls(List<ParticipantObj> ppts, ReportSourceTypeObj rstai) {

		Map<String, String> urlMap = null;
		// loop through the selected reports
		for (ReportTypeObj rto : rstai.getReportTypes()) {
			// Create a list of one rto
			ReportTypeObj[] selectedReportTypes = new ReportTypeObj[1];
			selectedReportTypes[0] = rto;
			// handle group report types (single report for multiple ppts
			// TODO: add these report types in ReportType
			if (rto.getCode().equals(ReportType.TLT_GROUP_DETAIL_REPORT)
					|| rto.getCode().equals(ReportType.TLT_EXTRACT_REPORT)
					|| rto.getCode().equals(ReportType.TLT_EXTRACT_REPORT_RAW)
					|| rto.getCode().equals(ReportType.ABYD_EXTRACT)
					|| rto.getCode().equals(ReportType.ABYD_ANALYTICS_EXTRACT)
					|| rto.getCode().equals(ReportType.ABYD_RAW_EXTRACT)) {
				urlMap = new HashMap<String, String>();
				String paramXml = generationReportXml(createReportsObject(ppts, selectedReportTypes));

				// // Testing kludge - replaces generated parameter string with
				// // a test one
				// paramXml =
				// "<Reports><ReportRequest code=\"ABYD_ANALYTICS_EXTRACT\" langCode=\"en\" sortId=\"2\" showName=\"true\" showLci=\"true\" axisLabels=\"dLCI by Potential\"><Participants><Participant participantId=\"367974\" projectId=\"370\"/><Participant participantId=\"367714\" projectId=\"370\"/><Participant participantId=\"367961\" projectId=\"432\"/><Participant participantId=\"368013\" projectId=\"468\"/></Participants></ReportRequest></Reports>";
				// int kdb = 0;

				String xmlParamName = rstai.getReportSourceType().getXmlParamName();
				urlMap.put("url", configProperties.getProperty(rstai.getReportSourceType().getUrlRef()));
				urlMap.put(xmlParamName, paramXml);
				reportSourceUrls.add(urlMap);
			} else {
				// Handle multiple ppts in individual reports (multiple reports
				// per invocation possible)
				// Loop through the ppts
				for (ParticipantObj ppt : ppts) {
					urlMap = new HashMap<String, String>();
					// Create a list of 1 ppt
					List<ParticipantObj> participants = new ArrayList<ParticipantObj>();
					participants.add(ppt);
					String paramXml = generationReportXml(createReportsObject(participants, selectedReportTypes));
					String xmlParamName = rstai.getReportSourceType().getXmlParamName();
					urlMap.put("url", configProperties.getProperty(rstai.getReportSourceType().getUrlRef()));
					urlMap.put(xmlParamName, paramXml);
					reportSourceUrls.add(urlMap);
				} // End of ppt for loop
			} // End of else on if <group report type>
		} // End of RTO for loop

		return;
	}

	public static String generationReportXml(Reports reports) {
		// Generate the xml
		String xml = "";
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(Reports.class);
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			marshaller.marshal(reports, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	public static String generationParticipantsXml(Participants participants) {
		String xml = "";
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(Participants.class);
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			marshaller.marshal(participants, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	public static String generationCoachingPlanDetailedExtractXml(
			CoachingPlanDetailedExtract coachingPlanDetailedExtract) {
		String xml = "";
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(CoachingPlanDetailedExtract.class);
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			marshaller.marshal(coachingPlanDetailedExtract, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	/**
	 * genCoachingExtractUrls - Runs through the list of selected coaching
	 * extract reports and generates the url data for them.
	 * 
	 * @param ppts
	 *            - A List of the select ppts in ParticipantObj objects
	 * @param rstai
	 *            - A ReportSourceTypeArrayItem object containing information
	 *            about the type and a list of reports to be generated
	 * @param reportSourceUrls
	 *            - A list of Maps that contain information about the outgoing
	 *            report requests (effective output)
	 */
	public void genCoachingExtractUrls(List<ParticipantObj> participants, ReportSourceTypeObj rstai) {
		// single report request - all of the participants are extracted to the
		// single generated report
		Map<String, String> urlMap = new HashMap<String, String>();
		String parmName = rstai.getReportSourceType().getXmlParamName();
		String parmVal = generationCoachingPlanDetailedExtractXml(createCoachingPlanDetailedExtractObject(participants));
		// .createCoachingPlanDetailedExtractObject(participants,
		// selectedReportTypes));
		urlMap.put("url", configProperties.getProperty(rstai.getReportSourceType().getUrlRef())
				+ "/CoachingPlanExtractServlet");

		urlMap.put(parmName, parmVal);
		reportSourceUrls.add(urlMap);

		return;
	}

	/**
	 * genHybridUrls - Creates URL parameters for Hybrid reports. Actually
	 * creates two separate sets of parameters in the same map with unique key
	 * names that are decoded at the generation of the actual reports to call
	 * the separate reports.
	 * 
	 * @param ppts
	 *            - A List of the select ppts in ParticipantObj objects
	 * @param rstai
	 *            - A ReportSourceTypeArrayItem object containing information
	 *            about the type and a list of reports to be generated
	 * @param reportSourceUrls
	 *            - A list of Maps that contain information about the outgoing
	 *            report requests (effective output)
	 */
	public void genHybridUrls(List<ParticipantObj> participants, ReportSourceTypeObj rstai) {
		// loop through the requested reports in the PALMS type, generating the
		// appropriate values in the urlMap
		Map<String, String> urlMap = null;
		for (ReportTypeObj rto : rstai.getReportTypes()) {
			// if (rto.getCode().equals(ReportType.VIAEDGE_TLT_COACHING)
			// || rto.getCode().equals(ReportType.VIAEDGE_TLT_FEEDBACK)) {
			if (rto.getCode().equals(ReportType.TLT_LAG_INDIVIDUAL_SUMMARY)) {
				// Loop through the participants
				for (ParticipantObj ppt : participants) {
					urlMap = new HashMap<String, String>();
					urlMap.put("hybrid", "yes");
					urlMap.put("hybridType", rto.getCode());
					urlMap.put("hybridPpt", "" + ppt.getUserId());

					// Figure out the language to use
					// Use ppt, but if null use the rpt. RPT has a full list and
					// no "xxx Default" so that should do it. And make SURE you
					// use it for both reports
					String langCode = ppt.getReportLanguageId() == null ? rto.getLanguageCode() : ppt
							.getReportLanguageId();

					/*
					 * Set up parms for the viaEDGE report
					 */
					String prefix = "H2_";
					// Create a ReportParticipant object
					ReportParticipant rp = new ReportParticipant(ppt.getUserId().toString(), ppt.getProject()
							.getProjectId() + "", ppt.getProject().getName());
					rp.setLangCode(langCode);
					String pptId = rp.getParticipantId();
					// // This works so long as there are only two valid types
					// // (coaching and feedback)
					// String rcmType =
					// rto.getCode().equals(ReportType.VIAEDGE_TLT_COACHING) ?
					// ReportType.VIAEDGE_INDIVIDUAL_COACHING
					// : ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;

					// This works because the feedback report is the only type
					// allowed in the TLT/viaEDGE hybrid report
					// String rcmType = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
					String rcmType = ReportType.HYBRID_RPT;
					// Set up to fetch the RCM
					String rcm = "";
					String rcmGenUrl = configProperties.getProperty("reportserverBaseUrl") + "/ViaEdgeRcmServlet";
					Map<String, String> params = new HashMap<String, String>();
					params.put("pptId", pptId);
					params.put("langCode", rp.getLangCode());
					params.put("type", rcmType);
					try {
						rcm = getRcm(rcmGenUrl, params);
					} catch (Exception e) {
						// TODO: handle exception
						logger.warn("Unable to generate viaEdge Individual RCM for ppt " + pptId);
						continue;
					}

					// Now set up the actual report generation parms (part 1)
					String rptUrl = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReportFromRcm";
					String type = rcmType;

					urlMap.put(prefix + "url", rptUrl);
					urlMap.put(prefix + "type", type);
					urlMap.put(prefix + "pid", pptId);
					urlMap.put(prefix + "rcm", rcm);
					// Dummy argument for the report subtype... only here on
					// hybrid viaEdge reports
					urlMap.put(prefix + "hybrid", "hybrid");

					/*
					 * Set up params for the TLT report
					 */
					prefix = "H1_";
					// Create a list of 1 ppt
					List<ParticipantObj> hppts = new ArrayList<ParticipantObj>();
					hppts.add(ppt);
					// Create a list of one report type, modified to be a TLT
					// Individual Summary report
					ReportTypeObj newRto = new ReportTypeObj(rto);
					newRto.setName(""); // wipe the hybrid name
					newRto.setCode(ReportType.TLT_INDIVIDUAL_SUMMARY_REPORT);
					newRto.setLanguageCode(langCode);
					ReportTypeObj[] selectedReportTypes = new ReportTypeObj[1];
					selectedReportTypes[0] = newRto;
					// Create the Reports object with the single ReportRequest
					// object in it. This is the TLT report that needs its
					// visual aspects changed, so set the combinedReport flag
					Reports rpts = createReportsObject(hppts, selectedReportTypes);
					rpts.getReportRequests().get(0).setHybridReport("YES");
					String oxcartXml = generationReportXml(rpts);

					urlMap.put(prefix + "xml", oxcartXml);
					urlMap.put(prefix + "url", configProperties.getProperty("reportserverBaseUrlOxcart"));
					// Fix above so following works (2nd half of the url ref)
					// configProperties.getProperty(rstai.getReportSourceType().getUrlRef()));

					reportSourceUrls.add(urlMap);
				} // End of participant loop
			} // End if code is VIAEDGE_TLT_COACHING or VIAEDGE_TLT_FEEDBACK
		} // end of report types loop

		return;
	}

	public static CoachingPlanDetailedExtract createCoachingPlanDetailedExtractObject(
			List<ParticipantObj> participantObjs) {
		CoachingPlanDetailedExtract cpde = new CoachingPlanDetailedExtract();
		Participants participants = new Participants();

		for (ParticipantObj participantObj : participantObjs) {
			ReportParticipant rp = new ReportParticipant(participantObj.getUserId().toString(), participantObj
					.getProject().getProjectId() + "", participantObj.getProject().getName());
			participants.getParticipants().add(rp);
		}
		cpde.setParticipants(participants);
		return cpde;
	}

	/**
	 * createReportsObject - Creates the Reports object (essentially a List of
	 * ReportRequest Objects)for an Oxcart report request
	 * 
	 * @param participants
	 * @param reportTypes
	 * @return
	 */
	public static Reports createReportsObject(List<ParticipantObj> participants, ReportTypeObj[] reportTypes) {

		Reports reports = new Reports();

		// create list of participant report objects
		List<ReportParticipant> allReportParticipantsNoLangCodes = new ArrayList<ReportParticipant>();

		for (ParticipantObj participantObj : participants) {
			ReportParticipant rp = new ReportParticipant(participantObj.getUserId().toString(), participantObj
					.getProject().getProjectId() + "", participantObj.getProject().getName());
			allReportParticipantsNoLangCodes.add(rp);
		}

		for (int i = 0; i < reportTypes.length; i++) {
			ReportTypeObj reportType = reportTypes[i];

			ReportRequest rr = new ReportRequest();
			rr.setCode(reportType.getCode());

			if (reportType.isParticipantLanguageVisible() == false) {
				rr.setLangCode(reportType.getLanguageCode());
			}

			// use participants with lang code if report allows it
			if (reportType.isParticipantLanguageVisible()) {

				List<ReportParticipant> allReportParticipantsWithLangCodes = new ArrayList<ReportParticipant>();

				for (ParticipantObj participantObj : participants) {

					ReportParticipant rpWithLangCode = new ReportParticipant(participantObj.getUserId().toString(),
							participantObj.getProject().getProjectId() + "", participantObj.getProject().getName());

					// if participant language is not specified default to the
					// report language
					if (participantObj.getReportLanguageId() != null
							&& !participantObj.getReportLanguageId().equals("")) {
						rpWithLangCode.setLangCode(participantObj.getReportLanguageId());
					} else {
						rpWithLangCode.setLangCode(reportType.getLanguageCode());
					}
					allReportParticipantsWithLangCodes.add(rpWithLangCode);
				}
				rr.getReportParticipants().addAll(allReportParticipantsWithLangCodes);
			} else {
				rr.getReportParticipants().addAll(allReportParticipantsNoLangCodes);
			}

			if (reportType.getTargetLevelOverride() != null && !reportType.getTargetLevelOverride().equals("")) {
				rr.getOverrides().add(new Override("TARGET_LEVEL", reportType.getTargetLevelOverride()));
			}

			// Do analytics stuff
			if (reportType.isAnalyticsNameOptVisible()) {
				rr.setShowName(reportType.getAnalyticsNameOpt());
			}
			if (reportType.isAnalyticsShowLciOptVisible()) {
				rr.setShowLci(reportType.getAnalyticsShowLciOpt());
			}
			if (reportType.isAnalyticsLabelsOptVisible()) {
				rr.setAxisLabels(reportType.getAnalyticsLabelsOpt());
			}
			if (reportType.isAnalyticsSortOptVisible()) {
				rr.setSortId(reportType.getAnalyticsSortIdOpt());
			}

			reports.getReportRequests().add(rr);
		}
		return reports;
	}

	/**
	 * getRcm - Call the appropriate servlet to generate and return the
	 * appropriate RCM as an XML string.
	 * 
	 * @param strUrl
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static String getRcm(String strUrl, Map<String, String> params) throws PalmsException {
		String rcm = "";
		StringBuffer sb = new StringBuffer();
		HttpURLConnection conn = null;
		URL url = null;
		BufferedReader reader = null;
		// System.out.println("in getRCM with url:  {}" + strUrl);

		try {
			url = new URL(strUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setDoOutput(true);
			StringBuffer urlParameters = new StringBuffer();
			Iterator<Entry<String, String>> it = params.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> pair = it.next();
				// System.out.println("Got param named : {}" + pair.getKey());
				urlParameters.append(pair.getKey() + "=" + URLEncoder.encode(pair.getValue(), "UTF-8"));
				if (it.hasNext()) {
					urlParameters.append("&");
				}
			}
			// System.out.println("urlParameters.toString() = " +
			// urlParameters.toString());

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(urlParameters.toString());
			writer.flush();
			String line;
			// Call servlet to get rcm filled
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			rcm = sb.toString();
			logger.trace("Got RCM response: {}", rcm);
			if (conn.getHeaderField("Error") != null){
				logger.debug("Found Error Header: {}", conn.getHeaderField("Error"));
				throw new RCMGenerationException(conn.getHeaderField("Error"));
			}
			writer.close();
			reader.close();

		} catch (IOException e) {
			if (e instanceof FileNotFoundException) {
				logger.error("Caught FileNotFoundException while getting RCM: {}, {}", e.getMessage(), e.getClass());
				ServerUnavailableException sue = new ServerUnavailableException(strUrl,
						"Unable to get RCM.  Server resource unavailable");
				sue.setErrorCode(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getCode());
				throw sue;
			} else {
				logger.error("Caught IOException while getting RCM: {}, {}", e.getMessage(), e.getClass());
				RCMGenerationException rge = new RCMGenerationException(e.getMessage());
				rge.setErrorCode(PalmsErrorCode.RCM_GEN_FAILED.getCode());
				throw rge;
			}
		} finally {

			try {
				if (!(reader == null)) {
					reader.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return rcm;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public List<Map<String, String>> getReportSourceUrls() {
		return reportSourceUrls;
	}

	public void setReportSourceUrls(List<Map<String, String>> reportSourceUrls) {
		this.reportSourceUrls = reportSourceUrls;
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}
}
