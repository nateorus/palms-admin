package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class RelativeDateObj extends EmailRuleScheduleObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int days;
    private Boolean daysAfter;
	private Date relativeToDate;
	private String type = "relative";
	private TimeZone timezone;
	private Date time;
	private String relativeToDateDisplayString;

    public RelativeDateObj () {}
    
    public RelativeDateObj (int days, Boolean daysAfter, Date relativeToDate) {
    	this.days = days;
    	this.daysAfter = daysAfter;
    	this.relativeToDate = relativeToDate;
    }
    
    public int getDays() {
		return days;
	}
	
    public void setDays(int days) {
		this.days = days;
	}
	
	public Date getRelativeToDate() {
		return relativeToDate;
	}

	public void setRelativeToDate(Date relativeToDate) {
		this.relativeToDate = relativeToDate;
	}

	public String getType() {
		return type;
	}

	public Boolean getDaysAfter() {
		return daysAfter;
	}

	public void setDaysAfter(Boolean daysAfter) {
		this.daysAfter = daysAfter;
	}

	public TimeZone getTimezone() {
		return timezone;
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getRelativeToDateDisplayString() {
		TimeZone zone;
		if (!(this.getTimezone() == null)){
			zone = this.getTimezone();
		}else{
			
			zone = TimeZone.getDefault();
		}
		
		Calendar calendar = new GregorianCalendar();
		
		calendar.setTime(this.getRelativeToDate());
		
		
		SimpleDateFormat dateFormatZone = new SimpleDateFormat("yyyy-MMM-dd hh:mm a zzzzz");
		dateFormatZone.setTimeZone(zone);
		return dateFormatZone.format(calendar.getTime());
	}

	public void setRelativeToDateDisplayString(
			String relativeToDateDisplayString) {
		this.relativeToDateDisplayString = relativeToDateDisplayString;
	}

}