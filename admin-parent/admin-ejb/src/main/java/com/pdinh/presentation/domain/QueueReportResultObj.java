package com.pdinh.presentation.domain;

public class QueueReportResultObj extends ResultObj {


	private String reportRequestKey;

	
	public QueueReportResultObj (String reportRequestKey){
		this.setReportRequestKey(reportRequestKey);
		
		this.setSuccess(true);
	}



	public String getReportRequestKey() {
		return reportRequestKey;
	}

	public void setReportRequestKey(String reportRequestKey) {
		this.reportRequestKey = reportRequestKey;
	}


}
