package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pdinh.persistence.ms.entity.Company;
import com.pdinh.persistence.ms.entity.EmailRuleSchedule;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ScheduledEmailText;

public class EmailEventObj implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String projectName = "";
	private String clientName = "";
	private String eventDueDate = "";
	private String participantUrl = "";
	private String eventDaysRemain = "";
	private String raterName = "";
	private String fromAddress = "";
	private String replyToAddress = "";
	private String recipientEmailAddress = "";
	private String recipientFirstLastName = "";
	private String recipientLastFirstName = "";
	private String ccEmailAddress = null;
	private String bccEmailAddress = null;
	private List<String> participantsInEvent = new ArrayList<String>();
	private ParticipantObj participant;
	private String passwordResetURLAssmtPortal = "";
	private String passwordResetURLClientPortal = "";
	private String siteUrlAP = "";
	private String siteUrlCP = "";
	private Company company;
	private Project project;
	private EmailRuleSchedule schedule;
	private String sendText;
	private String storeText;
	private String sendSubject;
	private String storeSubject;
	private ScheduledEmailText scheduledEmailText;
	private boolean error = false;
	private String errorText = "";
	private String lrmProjectName = "";
	private String lrmSchedulerNote = "";
	private String lrmUrl = "";
	private String senderFirstName = "";
	private String senderLastName = "";
	private String senderEmail = "";
	private String senderPhone = "";
	private String requestorFirstName = "";
	private String requestorLastName = "";
	private String requestorEmail = "";
	private String requestorPhone = "";
	private String tokenString = "";

	public String getStoreText() {
		return storeText;
	}

	public void setStoreText(String storeText) {
		this.storeText = storeText;
	}

	public String getSendSubject() {
		return sendSubject;
	}

	public void setSendSubject(String sendSubject) {
		this.sendSubject = sendSubject;
	}

	public String getStoreSubject() {
		return storeSubject;
	}

	public void setStoreSubject(String storeSubject) {
		this.storeSubject = storeSubject;
	}

	private int emailLogId;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getEventDueDate() {
		return eventDueDate;
	}

	public void setEventDueDate(String eventDueDate) {
		this.eventDueDate = eventDueDate;
	}

	public String getParticipantUrl() {
		return participantUrl;
	}

	public void setParticipantUrl(String participantUrl) {
		this.participantUrl = participantUrl;
	}

	public String getEventDaysRemain() {
		return eventDaysRemain;
	}

	public void setEventDaysRemain(String eventDaysRemain) {
		this.eventDaysRemain = eventDaysRemain;
	}

	public String getRaterName() {
		return raterName;
	}

	public void setRaterName(String raterName) {
		this.raterName = raterName;
	}

	public List<String> getParticipantsInEvent() {
		return participantsInEvent;
	}

	public void setParticipantsInEvent(List<String> participantsInEvent) {
		this.participantsInEvent = participantsInEvent;
	}

	public ParticipantObj getParticipant() {
		return participant;
	}

	public void setParticipant(ParticipantObj participant) {
		this.participant = participant;
	}

	public String getPasswordResetURLAssmtPortal() {
		return passwordResetURLAssmtPortal;
	}

	public void setPasswordResetURLAssmtPortal(String passwordResetURLAssmtPortal) {
		this.passwordResetURLAssmtPortal = passwordResetURLAssmtPortal;
	}

	public String getPasswordResetURLClientPortal() {
		return passwordResetURLClientPortal;
	}

	public void setPasswordResetURLClientPortal(String passwordResetURLClientPortal) {
		this.passwordResetURLClientPortal = passwordResetURLClientPortal;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public EmailRuleSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(EmailRuleSchedule schedule) {
		this.schedule = schedule;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getReplyToAddress() {
		return replyToAddress;
	}

	public void setReplyToAddress(String replyToAddress) {
		this.replyToAddress = replyToAddress;
	}

	public String getSiteUrlAP() {
		return siteUrlAP;
	}

	public void setSiteUrlAP(String siteUrlAP) {
		this.siteUrlAP = siteUrlAP;
	}

	public String getSiteUrlCP() {
		return siteUrlCP;
	}

	public void setSiteUrlCP(String siteUrlCP) {
		this.siteUrlCP = siteUrlCP;
	}

	public String getSendText() {
		return sendText;
	}

	public void setSendText(String sendText) {
		this.sendText = sendText;
	}

	public int getEmailLogId() {
		return emailLogId;
	}

	public void setEmailLogId(int emailLogId) {
		this.emailLogId = emailLogId;
	}

	public ScheduledEmailText getScheduledEmailText() {
		return scheduledEmailText;
	}

	public void setScheduledEmailText(ScheduledEmailText scheduledEmailText) {
		this.scheduledEmailText = scheduledEmailText;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public String getRecipientEmailAddress() {
		return recipientEmailAddress;
	}

	public void setRecipientEmailAddress(String recipientEmailAddress) {
		this.recipientEmailAddress = recipientEmailAddress;
	}

	public String getLrmProjectName() {
		return lrmProjectName;
	}

	public void setLrmProjectName(String lrmProjectName) {
		this.lrmProjectName = lrmProjectName;
	}

	public String getLrmSchedulerNote() {
		return lrmSchedulerNote;
	}

	public void setLrmSchedulerNote(String lrmSchedulerNote) {
		this.lrmSchedulerNote = lrmSchedulerNote;
	}

	public String getLrmUrl() {
		return lrmUrl;
	}

	public void setLrmUrl(String lrmUrl) {
		this.lrmUrl = lrmUrl;
	}

	public String getRecipientFirstLastName() {
		return recipientFirstLastName;
	}

	public void setRecipientFirstLastName(String recipientFirstLastName) {
		this.recipientFirstLastName = recipientFirstLastName;
	}

	public String getRecipientLastFirstName() {
		return recipientLastFirstName;
	}

	public void setRecipientLastFirstName(String recipientLastFirstName) {
		this.recipientLastFirstName = recipientLastFirstName;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderPhone() {
		return senderPhone;
	}

	public void setSenderPhone(String senderPhone) {
		this.senderPhone = senderPhone;
	}

	public String getRequestorPhone() {
		return requestorPhone;
	}

	public void setRequestorPhone(String requestorPhone) {
		this.requestorPhone = requestorPhone;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestorLastName() {
		return requestorLastName;
	}

	public void setRequestorLastName(String requestorLastName) {
		this.requestorLastName = requestorLastName;
	}

	public String getRequestorFirstName() {
		return requestorFirstName;
	}

	public void setRequestorFirstName(String requestorFirstName) {
		this.requestorFirstName = requestorFirstName;
	}

	public String getCcEmailAddress() {
		return ccEmailAddress;
	}

	public void setCcEmailAddress(String ccEmailAddress) {
		this.ccEmailAddress = ccEmailAddress;
	}

	public String getBccEmailAddress() {
		return bccEmailAddress;
	}

	public void setBccEmailAddress(String bccEmailAddress) {
		this.bccEmailAddress = bccEmailAddress;
	}

	public String getTokenString() {
		return tokenString;
	}

	public void setTokenString(String tokenString) {
		this.tokenString = tokenString;
	}

}
