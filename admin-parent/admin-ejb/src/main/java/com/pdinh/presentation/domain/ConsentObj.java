package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ConsentObj implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private int clientId;
	private double version;
	private boolean major;
	private boolean custom;
	private Date created;
	private List<ConsentTextObj> texts;
	private List<ConsentTextObj> currentTexts;
	private List<LanguageObj> availableInLanguages;

	public ConsentObj() {
		super();
	}

	public ConsentObj(int id, int clientId, double version, boolean major, boolean custom, Date created,
			List<ConsentTextObj> texts) {
		super();
		this.id = id;
		this.clientId = clientId;
		this.version = version;
		this.major = major;
		this.custom = custom;
		this.created = created;
		this.texts = texts;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}

	public boolean isMajor() {
		return major;
	}

	public void setMajor(boolean major) {
		this.major = major;
	}

	public boolean isCustom() {
		return custom;
	}

	public void setCustom(boolean custom) {
		this.custom = custom;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}


	public List<ConsentTextObj> getTexts() {
		return texts;
	}


	public void setTexts(List<ConsentTextObj> texts) {
		this.texts = texts;
	}

	public List<LanguageObj> getAvailableInLanguages() {
		return availableInLanguages;
	}

	public void setAvailableInLanguages(List<LanguageObj> availableInLanguages) {
		this.availableInLanguages = availableInLanguages;
	}

	public List<ConsentTextObj> getCurrentTexts() {
		return currentTexts;
	}

	public void setCurrentTexts(List<ConsentTextObj> currentTexts) {
		this.currentTexts = currentTexts;
	}

}
