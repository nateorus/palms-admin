package com.pdinh.presentation.domain;

import java.io.Serializable;

public class CourseStatusObj implements Serializable {
	private static final long serialVersionUID = 1L;
	private String courseCode;
	private Integer status = 0;
	private Boolean activated = false;
	
	public CourseStatusObj() {
	}
	
	public CourseStatusObj(String courseCode) {
		this.courseCode = courseCode;
		this.status = 0;
		this.activated = false;
	}
	
	public CourseStatusObj(String courseCode, Integer status) {
		this.courseCode = courseCode;
		this.status = status;
		if (getStatus() == 0) {
			activated = false;
		} else {
			activated = true;
		}
	}
	
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Boolean getActivated() {
		return activated;
	}
	public String toString() {
		if (getStatus() == 0) {
			return "Not Activated";
		} else if (getStatus() == 1) {
			return "Not Started";
		} else if (getStatus() == 2) {
			return "Started";
		} else if (getStatus() == 3) {
			return "Completed";
		} else {
			return "n/a";
		}
	}

}
