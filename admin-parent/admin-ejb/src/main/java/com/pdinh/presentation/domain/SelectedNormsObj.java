package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SelectedNormsObj implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ProjectNormsDefaultsObj projectNormsDefaults;

	private List<ParticipantObj> participants = new ArrayList<ParticipantObj>();

	public ProjectNormsDefaultsObj getProjectNormsDefaults() {
		return projectNormsDefaults;
	}

	public void setProjectNormsDefaults(ProjectNormsDefaultsObj projectNormsDefaults) {
		this.projectNormsDefaults = projectNormsDefaults;
	}

	public List<ParticipantObj> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ParticipantObj> participants) {
		this.participants = participants;
	}

}
