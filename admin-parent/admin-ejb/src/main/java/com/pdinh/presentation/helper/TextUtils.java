package com.pdinh.presentation.helper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextUtils {
	final static Logger log = LoggerFactory.getLogger(TextUtils.class);

	public static String getPlainText(String html) {
		Document doc = Jsoup.parse(html);
		StringBuffer buffer = new StringBuffer();
		for (Element element : doc.select("p")) {
			buffer.append(element.text());
			buffer.append("\n\n");
		}
		return buffer.toString();
	}

	public static boolean isHtml(String source) {
		Document doc = Jsoup.parse(source);
		if (doc.children().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public static String removeEmptyHtmlTags(String html) {
		Document doc = Jsoup.parse(html);
		doc.select(":containsOwn(\u00a0)").remove();
		for (Element element : doc.select("*")) {
			if (!element.hasText() && element.isBlock()) {
				element.remove();
			}
		}
		return doc.body().html();
	}

	// TODO: AV: Needs to be redone kinda a hack.
	public static String getHtmlText(String text) {

		if (text != null) {
			if (!isHtml(text)) {
				text = text.trim();
				// remove whitespace including tabs
				text = text.replaceAll("^[ \t]+|[ \t]+$. ", "");
				// replace newline breaks with paragraph tags
				text = text.replaceAll("\r\n?|\n", "</p><p>");
				text = "<p>" + removeEmptyHtmlTags(text) + "</p>";
				
				log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> text 2 = {}", text);
			} else {
				log.trace("Found HTML text: {}", text);
			}
		} else {
			log.error("value is null in getHtmlText() method, doing nothing.");
		}

		return text;
	}

}
