package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Date;

public class EmailLogObj implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private ParticipantObj participant;
    private EmailRuleObj emailRule;
    private EmailTemplateObj emailTemplate;
    private String langCode;
    private String status;
    private Date date;
    private String sentEmailSubject;
    private String sentEmailBody;
    
    public EmailLogObj() {}
    
    public EmailLogObj (Integer id, ParticipantObj participant, EmailRuleObj emailRule, String status, Date date) {
    	this.id = id;
    	this.participant = participant;
    	this.emailRule = emailRule;
    	this.status = status;
    	this.date = date;
    }
    
    public EmailLogObj (Integer id, ParticipantObj participant, String langCode, EmailTemplateObj emailTemplate, String status, Date date) {
    	this.id = id;
    	this.participant = participant;
    	this.langCode = langCode;
    	this.emailTemplate = emailTemplate;
    	this.status = status;
    	this.date = date;
    }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParticipantObj getParticipant() {
		return participant;
	}

	public void setParticipant(ParticipantObj participant) {
		this.participant = participant;
	}

	public EmailRuleObj getEmailRule() {
		return emailRule;
	}

	public void setEmailRule(EmailRuleObj emailRule) {
		this.emailRule = emailRule;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public EmailTemplateObj getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(EmailTemplateObj emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getSentEmailSubject() {
		return sentEmailSubject;
	}

	public void setSentEmailSubject(String sentEmailSubject) {
		this.sentEmailSubject = sentEmailSubject;
	}

	public String getSentEmailBody() {
		return sentEmailBody;
	}

	public void setSentEmailBody(String sentEmailBody) {
		this.sentEmailBody = sentEmailBody;
	}
    
}