package com.pdinh.presentation.domain;

import java.io.Serializable;
import java.util.Date;

public class ConsentHistoryObj implements Serializable {

	private static final long serialVersionUID = 1L;

	private int consentLogId;
	private int usersId;
	private int consentId;
	private int consentTextId;
	private double consentVersion;
	private boolean accepted;
	private String languageCode;
	private String consentTextLanguage;
	private Date date;

	public ConsentHistoryObj() {
		super();
	}

	public ConsentHistoryObj(int consentLogId, int usersId, int consentId, int consentTextId, double consentVersion,
			boolean accepted, String languageCode, String consentTextLanguage, Date date) {
		super();
		this.consentLogId = consentLogId;
		this.usersId = usersId;
		this.consentId = consentId;
		this.consentTextId = consentTextId;
		this.consentVersion = consentVersion;
		this.accepted = accepted;
		this.languageCode = languageCode;
		this.consentTextLanguage = consentTextLanguage;
		this.date = date;
	}

	public int getConsentLogId() {
		return consentLogId;
	}

	public void setConsentLogId(int consentLogId) {
		this.consentLogId = consentLogId;
	}

	public int getUsersId() {
		return usersId;
	}

	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public int getConsentId() {
		return consentId;
	}

	public void setConsentId(int consentId) {
		this.consentId = consentId;
	}

	public int getConsentTextId() {
		return consentTextId;
	}

	public void setConsentTextId(int consentTextId) {
		this.consentTextId = consentTextId;
	}

	public double getConsentVersion() {
		return consentVersion;
	}

	public void setConsentVersion(double consentVersion) {
		this.consentVersion = consentVersion;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getConsentTextLanguage() {
		return consentTextLanguage;
	}

	public void setConsentTextLanguage(String consentTextLanguage) {
		this.consentTextLanguage = consentTextLanguage;
	}

}
